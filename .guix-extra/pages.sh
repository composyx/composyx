#!/usr/bin/env bash

set -ex

# to avoid a lock during fetching compose branch in parallel
if [[ "$USER" = "gitlab-compose" ]]; then
    export XDG_CACHE_HOME=/tmp/guix-$$
fi

if [ -v CI_COMMIT_BRANCH ]; then
    export PUBLISHDIR="$CI_COMMIT_BRANCH"
else
    export PUBLISHDIR="local"
fi

export ENVGUIX="emacs emacs-org emacs-org-ref emacs-htmlize git texlive python-pygments imagemagick clang"
cat ./.guix-extra/channels-fixed.scm
export GUIXCMD="guix time-machine -C ./.guix-extra/channels-fixed.scm -- shell --pure"

# Tangle source code
${GUIXCMD} ${ENVGUIX} -- emacs --batch --no-init-file --load publish.el --eval '(org-publish "generate-source-code")'
# Generate pdf
${GUIXCMD} ${ENVGUIX} -- emacs --batch --no-init-file --load publish.el --eval '(org-publish "generate-pdf")'

# Pre-treatment for htmls
cd ./doc/html/ && ./make_doc.sh && cd ../..

# HTML export
${GUIXCMD} --preserve=PUBLISHDIR ${ENVGUIX} -- emacs --batch --no-init-file --load publish.el --eval '(org-publish "site")'

# Set README.org as homepage
if [[ -f ./public/README.html ]]; then mv ./public/README.html ./public/index.html; fi

# Add an index to link to the different branch pages available
cp ./doc/html/index_to_branches.org ./public/index_to_branches.org
cd ./public && guix time-machine -C ../.guix-extra/channels-fixed.scm -- shell --pure ${ENVGUIX} -- emacs --batch -l org index_to_branches.org -f org-html-export-to-html && cd ..

# clean tmp
rm -rf /tmp/guix-$$
