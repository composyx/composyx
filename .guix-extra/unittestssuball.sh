#!/usr/bin/env bash

set -ex

# to avoid a lock during fetching compose branch in parallel
export XDG_CACHE_HOME=/tmp/guix-$$

rm -rf ./.guix-extra/composyx-publish-timestamps*
./tangleorg.sh

export OMPI_MCA_rmaps_base_oversubscribe=1
#NB: qrmumps off because sources are not free (and also wrapper TOFIX for new version)
cmake -B build\
      -D CMAKE_BUILD_TYPE=Debug \
      -D CMAKE_CXX_FLAGS="--coverage" \
      -D COMPOSYX_USE_ARMADILLO=ON \
      -D COMPOSYX_USE_EIGEN=ON \
      -D COMPOSYX_USE_PASTIX=ON \
      -D COMPOSYX_USE_MUMPS=ON \
      -D COMPOSYX_USE_QRMUMPS=OFF \
      -D COMPOSYX_USE_FABULOUS=ON \
      -D COMPOSYX_USE_SZ_COMPRESSOR=ON \
      -D COMPOSYX_USE_ZFP_COMPRESSOR=ON \
      -D COMPOSYX_USE_ARPACK=ON \
      -D COMPOSYX_USE_PADDLE=ON \
      -D COMPOSYX_USE_TLAPACK=ON \
      -D COMPOSYX_USE_CHAMELEON=ON \
      -D COMPOSYX_C_DRIVER=ON \
      -D COMPOSYX_Fortran_DRIVER=ON \
      -D COMPOSYX_COMPILE_EXAMPLES=ON \
      -D COMPOSYX_COMPILE_TESTS=ON \
      -D COMPOSYX_COMPILE_BENCH=ON \
      -D CMAKE_INSTALL_PREFIX=$PWD/install \
      -D CMAKE_VERBOSE_MAKEFILE=ON

cmake --build build --verbose -j4
ctest --test-dir build --output-junit junit.xml --output-on-failure
NFAILURES=$(cat build/junit.xml | grep failures | awk -F '"' '{print $2}')
if [[ "$NFAILURES" -gt 0 ]]; then
    exit 1
fi
gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root .

# clean tmp
rm -rf /tmp/guix-$$
