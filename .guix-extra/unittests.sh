#!/usr/bin/env bash

set -ex

cat .guix-extra/channels-fixed.scm
git submodule update --init --recursive
guix time-machine -C .guix-extra/channels-fixed.scm -- shell --pure -m .guix-extra/unittests-manifest.scm -- .guix-extra/unittestssub.sh
