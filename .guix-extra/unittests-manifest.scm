;; What follows is a "manifest" equivalent to the command line you gave.
;; You can store it in a file that you may then pass to any 'guix' command
;; that accepts a '--manifest' (or '-m') option.

;; guix shell --pure clang emacs emacs-org python-gcovr -D composyx --export-manifest

(concatenate-manifests
  (list (specifications->manifest
          (list "clang" "emacs" "emacs-org" "python-gcovr"))
        (package->development-manifest
          (specification->package "composyx"))))
