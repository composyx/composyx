#!/usr/bin/env bash

set -ex

# to avoid a lock during fetching compose branch in parallel
export XDG_CACHE_HOME=/tmp/guix-$$

cat .guix-extra/channels-fixed.scm
git submodule update --init --recursive
guix time-machine -C .guix-extra/channels-fixed.scm -- shell --pure -m .guix-extra/unittestsall-manifest.scm -- .guix-extra/unittestssuball.sh

# clean tmp
rm -rf /tmp/guix-$$
