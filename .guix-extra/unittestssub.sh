#!/usr/bin/env bash

set -ex

# to avoid a lock during fetching compose branch in parallel
export XDG_CACHE_HOME=/tmp/guix-$$

rm -rf ./.guix-extra/composyx-publish-timestamps*
./tangleorg.sh

export OMPI_MCA_rmaps_base_oversubscribe=1
cmake -B build \
      -D CMAKE_BUILD_TYPE=Debug \
      -D CMAKE_CXX_FLAGS="--coverage" \
      -D COMPOSYX_DEV_WARNINGS=ON \
      -D COMPOSYX_USE_FABULOUS=ON \
      -D COMPOSYX_USE_CHAMELEON=ON \
      -D CMAKE_INSTALL_PREFIX=$PWD/install \
      -D CMAKE_VERBOSE_MAKEFILE=ON

cmake --build build --verbose -j4
ctest --test-dir build --output-junit junit.xml --output-on-failure
NFAILURES=$(cat build/junit.xml | grep failures | awk -F '"' '{print $2}')
if [[ "$NFAILURES" -gt 0 ]]; then
    exit 1
fi
gcovr --xml-pretty --exclude-unreachable-branches --print-summary -o coverage.xml --root .

# clean tmp
rm -rf /tmp/guix-$$
