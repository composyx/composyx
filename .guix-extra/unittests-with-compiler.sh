#!/usr/bin/env bash

set -ex

COMP_TYPE=$1
COMP_VER=$2

echo ">>> Test with Compiler: $COMP_TYPE version $2"

if [[ $COMP_TYPE = "gcc" ]]; then
    COMP_TOOLCHAIN="gcc-toolchain"
    COMP_CMAKE_C="gcc"
    COMP_CMAKE_CXX="g++"
elif [[ $COMP_TYPE = "clang" ]]; then
    COMP_TOOLCHAIN="clang-toolchain"
    COMP_CMAKE_C="clang"
    COMP_CMAKE_CXX="clang++"
else
    echo "Usage: $0 [gcc|clang] version"
    exit 0
fi

COMP_TOOLCHAIN="${COMP_TOOLCHAIN}@${COMP_VER}"

guix build composyx --with-source=composyx=$PWD \
     --with-configure-flag=composyx="-DCMAKE_C_COMPILER=${COMP_CMAKE_C}" \
     --with-configure-flag=composyx="-DCMAKE_CXX_COMPILER=${COMP_CMAKE_CXX}" \
     --with-configure-flag=composyx="-DCOMPOSYX_DEV_WARNINGS=ON" \
     --with-c-toolchain=composyx=${COMP_TOOLCHAIN}
