;; What follows is a "manifest" equivalent to the command line you gave.
;; You can store it in a file that you may then pass to any 'guix' command
;; that accepts a '--manifest' (or '-m') option.

;; guix shell --pure armadillo clang eigen emacs emacs-org python-gcovr sz-compressor zlib zfp -D composyx --export-manifest

(concatenate-manifests
  (list (specifications->manifest
          (list "armadillo"
                "clang"
                "eigen"
                "emacs"
                "emacs-org"
                "python-gcovr"
                "sz-compressor"
                "zlib"
                "zfp"))
        (package->development-manifest
          (specification->package "composyx"))))
