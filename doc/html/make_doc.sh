#!/bin/bash

echo "------------------------------------------"
echo "Pre-treatment for HTML output: make_doc.sh"
echo "------------------------------------------"

mkdir -p public
cd public

# Get org-mode files in org/composyx
INC_DIR="../../../org/composyx"
ORGFILES=`(cd ${INC_DIR} && find . | grep org$)`

#THEME="https://fniessen.github.io/org-html-themes/setup/theme-readtheorg.setup"

# add_header orgfile
function add_header {
    for f in '#+TITLE: Composyx' \
                 '#+AUTHOR: Concace' \
                 '#+DATE: {{{time(%d/%m/%Y at %H:%M:%S)}}}' \
                 '' \
                 '#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://concace.gitlabpages.inria.fr/inria-org-html-themes/readtheorginria/css/htmlize.css"/>' \
                 '#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://concace.gitlabpages.inria.fr/inria-org-html-themes/readtheorginria/css/readtheorginria.css"/>' \
                 '#+HTML_HEAD: <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>' \
                 '#+HTML_HEAD: <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>' \
                 '#+HTML_HEAD: <script type="text/javascript" src="https://concace.gitlabpages.inria.fr/inria-org-html-themes/lib/js/jquery.stickytableheaders.min.js"></script>' \
                 '#+HTML_HEAD: <script type="text/javascript" src="https://concace.gitlabpages.inria.fr/inria-org-html-themes/readtheorginria/js/readtheorginria.js"></script>' \
                 ''
    do
        echo $f >> tmp
    done
    cat $1 >> tmp
    mv tmp $1
}

# Replace {{{link(a,b,c)}}} by [[b][c]]
function replace_macro {
    sed -i -E "s/\{\{\{link\(\s*([^,]+),\s*([^,]+),\s*([^,]+)\)}}}/[[file:\2][\3]]/g" $1
}

# Index file (from the full documentation index)
cp ../../fulldoc/documentation_full.org index.org

# Figures
mkdir figures
cp ../../figures/*svg figures

# Remove inclusion (because use html links instead)
sed -i '/#+INCLUDE:/d' index.org
sed -i '/#+TITLE:/d' index.org
sed -i '/#+AUTHOR:/d' index.org
# Add html header
sed -i "1i#+INCLUDE: \"..\/..\/header.org\"" index.org
# Remove the tag '# HTML' used to identify HTML links
sed -i 's/# HTML //g' index.org
# Replace macro links
replace_macro index.org
# Add the theme
add_header index.org

# post_treat_org input_org_path local_file
function post_treat_org {
    INPUT_ORG_PATH=$1
    LOCAL_FILE=$2

    echo "Treating $LOCAL_FILE"
    # Create local directory
    DIR=$(dirname "${LOCAL_FILE}")
    mkdir -p $DIR

    # Get folder depth for relative links
    DEPTH=`awk -F'/' '{print NF-1}' <<< "${DIR}"`
    BACKDIR=""
    for i in `seq 1 $DEPTH`
    do
        BACKDIR="../$BACKDIR"
    done

    # Copy org file and tangle it into html
    cp ${INPUT_ORG_PATH} ${LOCAL_FILE}

    # Add html header
    sed -i "1i#+INCLUDE: \"${BACKDIR}..\/..\/header.org\"" ${LOCAL_FILE}
    # Add theme
    add_header ${LOCAL_FILE}
    # Add link to index.html
    sed -i "1iBack to [[file:${BACKDIR}index.html][index]]." ${LOCAL_FILE}
    # Replace macro links
    replace_macro ${LOCAL_FILE}
    # Remove latex labels
    sed -i '/label:/d' ${LOCAL_FILE}
}

for file in ${ORGFILES}
do
    post_treat_org ${INC_DIR}/${file} ${file}
done

if [ -f "../../fulldoc/documentation_full.pdf" ]; then
    cp ../../fulldoc/documentation_full.pdf .
fi

mkdir tutorial
post_treat_org ../../tutorial/tuto.org ./tutorial/tuto.org
post_treat_org ../../tutorial/gmres.org ./tutorial/gmres.org
post_treat_org ../../tutorial/operators.org ./tutorial/operators.org
cp ../../../src/driver/C/driver.org ./tutorial/driver.org
post_treat_org ../../tutorial/f_c_drivers.org ./tutorial/f_c_drivers.org

WEEKLY_BENCH_DIR=/home/gitlab-compose/weekly_mpp_results

mkdir bench

touch ./bench/lastruntime.txt
touch ./bench/commit.txt
touch ./bench/channels.txt
touch ./bench/results.csv

if [ -d "$WEEKLY_BENCH_DIR" ]; then
    cp ${WEEKLY_BENCH_DIR}/*png ./bench
    cp ${WEEKLY_BENCH_DIR}/*csv ./bench
    cp ${WEEKLY_BENCH_DIR}/lastruntime.txt ./bench
    cp ${WEEKLY_BENCH_DIR}/commit.txt ./bench
    cp ${WEEKLY_BENCH_DIR}/channels.txt ./bench
fi

post_treat_org "../../../bench/plafrim/weekly_results.org" "./bench/weekly_results.org"
post_treat_org "../../../bench/plafrim/bench_plafrim.org" "./bench/bench_plafrim.org"
#post_treat_org "../../../bench/fabulous/test_fabulous_Block.org" "./bench/fabulous/test_fabulous_Block.org"
cp -r ../../../bench/fabulous/img ./bench/fabulous/
