#pragma once
#include "composyx/interfaces/basic_concepts.hpp"
#include "composyx/Context.hpp"
#include "composyx/loc_data/DenseMatrix.hpp"
#include "composyx/loc_data/SparseMatrixCOO.hpp"
#include "composyx/kernel/spkernel.hpp"
