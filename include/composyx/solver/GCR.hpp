// [[file:../../../org/composyx/solver/GCR.org::*Header][Header:1]]
#pragma once

#include "composyx/utils/Arithmetic.hpp"
#include "composyx/utils/MatrixProperties.hpp"
#include "composyx/utils/Error.hpp"
#include "composyx/utils/Macros.hpp"
#include "composyx/solver/IterativeSolver.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/solver/GCR.org::*Base class][Base class:1]]
template <CPX_LinearOperator Matrix, CPX_Vector Vector,
          class Precond = Identity<Matrix, Vector>>
class GCR : public IterativeSolver<Matrix, Vector, Precond> {
  // Base class:1 ends here

  // [[file:../../../org/composyx/solver/GCR.org::*Attributes][Attributes:1]]
private:
  using Scalar = typename IterativeSolver<Matrix, Vector, Precond>::scalar_type;
  using Real = typename IterativeSolver<Matrix, Vector, Precond>::real_type;

  using IterativeSolver<Matrix, Vector, Precond>::use_preconditioner;
  using IterativeSolver<Matrix, Vector, Precond>::_A;
  using IterativeSolver<Matrix, Vector, Precond>::_B;
  using IterativeSolver<Matrix, Vector, Precond>::_X;
  using IterativeSolver<Matrix, Vector, Precond>::_R;
  using IterativeSolver<Matrix, Vector, Precond>::_M;
  using IterativeSolver<Matrix, Vector, Precond>::_norm;
  using IterativeSolver<Matrix, Vector, Precond>::_tol_sq;
  using IterativeSolver<Matrix, Vector, Precond>::_max_iter;
  using IterativeSolver<Matrix, Vector, Precond>::_check_true_residual;
  using IterativeSolver<Matrix, Vector, Precond>::_always_true_residual;
  using IterativeSolver<Matrix, Vector, Precond>::_residual_sq;
  using IterativeSolver<Matrix, Vector, Precond>::_verbose;
  using IterativeSolver<Matrix, Vector, Precond>::_convergence_achieved;

public:
  using scalar_type = Scalar;
  using vector_type = Vector;
  using matrix_type = Matrix;
  using real_type = Real;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/solver/GCR.org::*C++ code][C++ code:1]]
  int iterative_solve() override {
    Timer<TIMER_ITERATION> t0("GCR iteration 0");

    const Matrix& A = *_A;
    const Vector& B = *_B;
    Vector& X = *_X;
    Vector& R = _R;

    this->setup_initial_stop_crit("GCR");

    R = B - A * X;
    if (_convergence_achieved())
      return 0;

    t0.stop();

    Vector Mr = R;
    std::vector<Vector> MP, AMP;

    for (int iter = 0; iter < _max_iter; ++iter) {
      Timer<TIMER_ITERATION> t("GCR iteration");

      // Apply preconditioner if any
      if (_verbose)
        std::cout << iter + 1 << " -\t";
      Mr = _M * R;

      MP.push_back(Mr);
      AMP.push_back(A * Mr);

      //Orthonormalization
      for (auto j = 0; j < iter; ++j) {
        Scalar alpha = dot(AMP[iter], AMP[j]);
        MP[iter] -= alpha * MP[j];
        AMP[iter] -= alpha * AMP[j];
      }

      Real normAMPk_inv = Real{1.0} / _norm(AMP[iter]);
      MP[iter] *= normAMPk_inv;
      AMP[iter] *= normAMPk_inv;

      Scalar rAMPk = dot(R, AMP[iter]);

      X += rAMPk * MP[iter];
      R -= rAMPk * AMP[iter];
      if (_convergence_achieved())
        return iter + 1;
    }

    return -1;
  }
  // C++ code:1 ends here

  // [[file:../../../org/composyx/solver/GCR.org::*Constructors][Constructors:1]]
public:
  GCR() : IterativeSolver<Matrix, Vector, Precond>() {}

  GCR(const Matrix& A, bool verb = false)
      : IterativeSolver<Matrix, Vector, Precond>(A, verb) {}
}; // class GCR
// Constructors:1 ends here

// [[file:../../../org/composyx/solver/GCR.org::*Traits][Traits:1]]
// Set traits
template <typename Matrix, typename Vector, typename Precond>
struct is_solver_direct<GCR<Matrix, Vector, Precond>> : public std::false_type {
};
template <typename Matrix, typename Vector, typename Precond>
struct is_solver_iterative<GCR<Matrix, Vector, Precond>>
    : public std::true_type {};
template <typename Matrix, typename Vector, typename Precond>
struct is_matrix_free<GCR<Matrix, Vector, Precond>> : public std::true_type {};

template <CPX_LinearOperator Matrix, CPX_Vector Vector, class Precond>
struct vector_type<GCR<Matrix, Vector, Precond>> : public std::true_type {
  using type = typename GCR<Matrix, Vector, Precond>::vector_type;
};

template <CPX_LinearOperator Matrix, CPX_Vector Vector, class Precond>
struct scalar_type<GCR<Matrix, Vector, Precond>> : public std::true_type {
  using type = typename GCR<Matrix, Vector, Precond>::scalar_type;
};
// Traits:1 ends here

// [[file:../../../org/composyx/solver/GCR.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
