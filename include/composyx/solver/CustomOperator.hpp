// [[file:../../../org/composyx/solver/CustomOperator.org::*Header][Header:1]]
#pragma once

#include <functional>

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/solver/CustomOperator.org::*Class][Class:1]]
template <typename Operand> class CustomOperator {
  using ApplyType = std::function<Operand(const Operand&)>;

  static Operand default_apply(const Operand& in) { return in; }

  ApplyType apply_fct = CustomOperator::default_apply;

public:
  using vector_type = Operand;
  using apply_function_type = ApplyType;

  CustomOperator() {}
  CustomOperator(ApplyType f) : apply_fct{f} {}
  void setup(auto) {}
  Operand apply(const Operand& B) { return apply_fct(B); }

  void set_apply_fct(ApplyType afct) { apply_fct = afct; }
}; //class CustomOperator

template <typename Operand>
[[nodiscard]]
Operand operator*(CustomOperator<Operand>& op, const Operand& rhs) {
  return op.apply(rhs);
}
// Class:1 ends here

// [[file:../../../org/composyx/solver/CustomOperator.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
