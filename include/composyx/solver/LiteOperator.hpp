// [[file:../../../org/composyx/solver/LiteOperator.org::*Header][Header:1]]
#pragma once

#include "composyx/solver/LinearOperator.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/solver/LiteOperator.org::*Class][Class:1]]
template <typename Operator, typename Operand>
class LiteOperator : public LinearOperator<Operator, Operand> {

  using SetupType = std::function<void(const Operator&)>;
  using ApplyType = std::function<Operand(const Operand&)>;

  static void default_setup(const Operator&) {}
  static Operand default_apply(const Operand& in) { return in; }

  SetupType setup_fct = LiteOperator::default_setup;
  ApplyType apply_fct = LiteOperator::default_apply;

public:
  using matrix_type = Operator;
  using operator_type = Operator;
  using vector_type = Operand;
  using apply_function_type = ApplyType;
  using setup_function_type = SetupType;

  LiteOperator() {}
  LiteOperator(ApplyType f) : apply_fct{f} {}
  void setup(const Operator& A) { setup_fct(A); }
  Operand apply(const Operand& B) { return apply_fct(B); }

  void set_setup_fct(SetupType sfct) { setup_fct = sfct; }
  void set_apply_fct(ApplyType afct) { apply_fct = afct; }
}; //class LiteOperator
// Class:1 ends here

// [[file:../../../org/composyx/solver/LiteOperator.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
