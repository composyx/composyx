// [[file:../../../org/composyx/solver/SchurSolver.org::*Header][Header:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>
#include <type_traits>
#include <cmath>

#include "composyx/utils/Arithmetic.hpp"
#include "composyx/utils/MatrixProperties.hpp"
#include "composyx/utils/Error.hpp"
#include "composyx/utils/Macros.hpp"
#include "composyx/solver/LinearOperator.hpp"
#include "composyx/solver/IterativeSolver.hpp"
#include "composyx/solver/ImplicitSchur.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/solver/SchurSolver.org::*Parameters][Parameters:1]]
namespace parameters {
CREATE_STRUCT(schurlist)
CREATE_STRUCT(setup_solver_K)
CREATE_STRUCT(stop_crit_on_b)
} // end namespace parameters
// Parameters:1 ends here

// [[file:../../../org/composyx/solver/SchurSolver.org::*Attributes][Attributes:1]]
template <CPX_Matrix Matrix, CPX_Vector Vector, typename SolverK,
          typename SolverS>
class SchurSolverBase : public LinearOperator<Matrix, Vector> {

private:
  using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;
  using Real = typename arithmetic_real<Scalar>::type;
  using SchurOperator = typename SolverS::matrix_type;

public:
  using scalar_type = Scalar;
  using vector_type = Vector;
  using real_type = Real;
  using schur_type = SchurOperator;
  using solver_on_K = SolverK;
  using solver_on_S = SolverS;

protected:
  const Matrix* _K = nullptr; // Input matrix
  Matrix _K_copied;

  std::unique_ptr<std::vector<int>> _schurlist;
  std::unique_ptr<SchurOperator> _S;
  SolverS _solver_S;

  bool _verbose = false;
  bool _stop_crit_on_b = true;
  bool _copy_K = false;
  std::function<void(SolverK&)> _setup_solver_K = [](SolverK&) {};
  // Attributes:1 ends here

  // [[file:../../../org/composyx/solver/SchurSolver.org::*Implementation][Implementation:1]]
  // Get the Schur Operator _S from input matrix K
  virtual void get_schur_complement(const Matrix& K) = 0;

  // Get ftg, the RHS associated with the Schur complement system S Ug = ftg
  virtual Vector get_schur_rhs(const Vector& B) = 0;

  // Get the full solution U from the solution Ug
  virtual void get_schur_solution(const Vector& Ug, Vector& U) = 0;

  void reset_schur() { _S.reset(nullptr); }

  void hybrid_schur_solve(const Vector& B, Vector& U) {
    Timer<TIMER_SOLVER> t("Hybrid solve");

    if (_S == nullptr) {
      if (_verbose)
        std::cout << "Schur solver: computing Schur complement\n";
      get_schur_complement(*_K);
      COMPOSYX_ASSERT(_S != nullptr,
                      "SchurSolver: error, Schur complement not computed");
      _solver_S.setup(*_S);
    }

    // Compute ftg = Bg - Kig Kii^-1 Bi
    if (_verbose)
      std::cout << "Schur solver: computing new right hand side\n";
    Vector ftg = get_schur_rhs(B);

    // Stopping criterion to use: beta = ||b||
    if constexpr (is_solver_iterative<SolverS>::value) {
      if (_stop_crit_on_b) {
        Real nb = std::sqrt(std::real(dot(B, B)));
        if (nb != Real{0}) { // Avoid 0 division
          if (_verbose)
            std::cout << "Schur solver: iterative criterion ||Sy - f|| / ||b|| "
                         "< tolerance ; with ||b|| = "
                      << nb << '\n';
          _solver_S.setup(parameters::stopping_crit_denom<Real>{nb});
        }
      }
    }

    // Find Ug solving S Ug = ftg
    if (_verbose)
      std::cout << "Schur solver: starting iterative solve\n";
    Vector Ug = _solver_S * ftg;

    //Compute Ui = Kii^-1 (Bi - Kig Xg), and get U = (Ui, Ug)^T
    if (_verbose)
      std::cout << "Schur solver: backward substitution\n";
    get_schur_solution(Ug, U);
  }
  // Implementation:1 ends here

  // [[file:../../../org/composyx/solver/SchurSolver.org::*Setup functions][Setup functions:1]]
public:
  SchurSolverBase() {}

  SchurSolverBase(const Matrix& A, bool verb = false)
      : _K{&A}, _verbose{verb} {}

private:
  void setup_A(const Matrix* Aptr) {
    if (!_copy_K) {
      _K = Aptr;
      reset_schur();
      return;
    }

    if constexpr (std::is_copy_assignable<Matrix>::value) {
      if (Aptr == &_K_copied)
        return;
      _K_copied = *Aptr;
      _K = &_K_copied;
      reset_schur();
    } else { // Not copy assignable
      COMPOSYX_WARNING(
          "SchurSolver::setup_A operator cannot be copied, taking a pointer");
      _K = Aptr;
    }
  }

  void _setup(const parameters::A<Matrix>& v) { setup_A(&(v.value)); }
  void _setup(const parameters::A<const Matrix>& v) { setup_A(&(v.value)); }
  void _setup(const parameters::schurlist<std::vector<int>>& v) {
    _schurlist = std::make_unique<std::vector<int>>(v.value);
    reset_schur();
  }
  void _setup(const parameters::schurlist<const std::vector<int>>& v) {
    _schurlist = std::make_unique<std::vector<int>>(v.value);
    reset_schur();
  }
  void _setup(const parameters::stop_crit_on_b<bool> v) {
    _stop_crit_on_b = v.value;
  }
  void _setup(const parameters::verbose<bool> v) { _verbose = v.value; }
  void _setup(const parameters::copy_A<bool>& v) { _copy_K = v.value; }
  void
  _setup(const parameters::setup_solver_K<std::function<void(SolverK&)>>& v) {
    _setup_solver_K = v.value;
  }

  void setup() {}

public:
  void setup(const Matrix& A) { setup_A(&A); }

  // Variadic template -> each parameter is a call to the _setup function
  template <typename... Types> void setup(const Types&... args) noexcept {
    (void(_setup(args)), ...);
  }

  template <typename... Types>
  void setup(const Matrix& A, const Types&... args) {
    setup_A(&A);
    setup(args...);
  }
  // Setup functions:1 ends here

  // [[file:../../../org/composyx/solver/SchurSolver.org::*Apply / solve][Apply / solve:1]]
  void apply(const Vector& B, Vector& X) { this->hybrid_schur_solve(B, X); }

  Vector apply(const Vector& B) {
    Vector X = Scalar{0.0} * B; // Create X same size as B
    apply(B, X);
    return X;
  }

  Vector solve(const Vector& B) { return apply(B); }
  void solve(const Vector& B, Vector& X) { apply(B, X); }
  // Apply / solve:1 ends here

  // [[file:../../../org/composyx/solver/SchurSolver.org::*Getters][Getters:1]]
  [[nodiscard]] SolverS& get_solver_S() { return _solver_S; }
  [[nodiscard]] const Matrix* get_input_matrix() const { return _K; }

  [[nodiscard]] int get_n_iter() const {
    if constexpr (is_solver_iterative<SolverS>::value) {
      return _solver_S.get_n_iter();
    }
    return 0;
  }

  [[nodiscard]] Real get_residual() const {
    if constexpr (is_solver_iterative<SolverS>::value) {
      return _solver_S.get_residual();
    }
    return Real{0};
  }

  [[nodiscard]] const SchurOperator& get_schur() {
    if (_S == nullptr) {
      if (_verbose)
        std::cout << "Schur solver: computing Schur complement\n";
      get_schur_complement(*_K);
      COMPOSYX_ASSERT(_S != nullptr,
                      "SchurSolver: error, Schur complement not computed");
      _solver_S.setup(*_S);
    }
    return *_S;
  }
  // Getters:1 ends here

  // [[file:../../../org/composyx/solver/SchurSolver.org::*Display][Display:1]]
  void display(const std::string& name = "",
               std::ostream& out = std::cout) const {
    if (!name.empty())
      out << "Schur solver name: " << name << '\n';

    if (_K) {
      out << "Input matrix K (" << n_rows(*_K) << ", " << n_cols(*_K) << ")"
          << '\n';
    } else {
      out << "Input matrix K: unset" << '\n';
    }

    _solver_S.display("Solver on S", out);

    if (_schurlist) {
      composyx::display<std::vector<int>>(*_schurlist, "Schur list", out);
    } else {
      out << "Schur list: unset" << '\n';
    }

    out << '\n';
  }

  friend std::ostream& operator<<(std::ostream& out, const SchurSolverBase& s) {
    s.display("", out);
    return out;
  }

}; // class SchurSolverBase
// Display:1 ends here

// [[file:../../../org/composyx/solver/SchurSolver.org::*SchurSolver concrete sequential class][SchurSolver concrete sequential class:1]]
template <CPX_Matrix Matrix, CPX_Vector Vector, typename SolverK,
          typename SolverS>
class SchurSolver : public SchurSolverBase<Matrix, Vector, SolverK, SolverS> {

private:
  using BaseT = SchurSolverBase<Matrix, Vector, SolverK, SolverS>;
  using SchurOperator = typename BaseT::schur_type;

  using BaseT::_S;
  using BaseT::_schurlist;
  using BaseT::_setup_solver_K;

  constexpr const static bool is_implicit =
      std::is_same<ImplicitSchur<Matrix, Vector, SolverK>,
                   SchurOperator>::value;

  SolverK _solver_K;

  template <typename S = SolverS>
  typename std::enable_if<is_solver_iterative<S>::value, void>::type
  setup_precond() {
    using Precond = typename SolverS::precond_type;
    using SchurPcdType = typename Precond::matrix_type;
    using SchurPcdVectType = typename Precond::vector_type;
    if constexpr (!std::is_same<Precond, Identity<SchurPcdType,
                                                  SchurPcdVectType>>::value) {
      auto fct_setup_pcd = [&](const SchurOperator& Simpl, Precond& M) {
        M.setup(const_cast<SchurOperator&>(Simpl).get_dense_schur());
      };
      BaseT::_solver_S.setup(
          parameters::setup_pcd<
              std::function<void(const SchurOperator&, Precond&)>>{
              fct_setup_pcd});
    }
  }

  void get_schur_complement(const Matrix& K) {
    COMPOSYX_ASSERT(_schurlist != nullptr,
                    "Schur solver: schurlist must be set before solve!");
    Timer<TIMER_SOLVER> t("Compute Schur");

    if constexpr (is_implicit) {
      _S = std::make_unique<SchurOperator>();
      _S->setup(K);
      _S->setup_schurlist(*_schurlist);
      _S->split_matrix();
      _setup_solver_K(_S->get_solver_K());
      if constexpr (is_solver_iterative<SolverS>::value)
        setup_precond();
    } else { // Explicit
      _solver_K.setup(K);
      _setup_solver_K(_solver_K);
      _S = std::make_unique<SchurOperator>(_solver_K.get_schur(*_schurlist));
    }
  }

  Vector get_schur_rhs(const Vector& B) {
    Timer<TIMER_SOLVER> t("Schur b2f");
    if constexpr (is_implicit) {
      return _S->compute_rhs(B);
    } else { //Explicit
      return _solver_K.b2f(B);
    }
  }

  void get_schur_solution(const Vector& Ug, Vector& U) {
    Timer<TIMER_SOLVER> t("Schur y2x");
    if constexpr (is_implicit) {
      U = _S->get_solution(Ug);
    } else { //Explicit
      U = _solver_K.y2x(Ug);
    }
  }

public:
  [[nodiscard]] SolverK& get_solver_K() { return _solver_K; }

}; // class SchurSolver

template <typename Matrix, typename Vector, typename SolverK, typename SolverS>
struct is_matrix_free<SchurSolver<Matrix, Vector, SolverK, SolverS>>
    : public std::false_type {};

} // namespace composyx
// SchurSolver concrete sequential class:1 ends here
