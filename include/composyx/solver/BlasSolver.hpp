// [[file:../../../org/composyx/solver/BlasSolver.org::*Header][Header:1]]
#pragma once

#include <lapack.hh>
#include <blas.hh>

#include "composyx/interfaces/linalg_concepts.hpp"
namespace composyx {
template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
class BlasSolver;
}

#include "composyx/loc_data/DenseMatrix.hpp"
#include "composyx/solver/LinearOperator.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/solver/BlasSolver.org::*Class][Class:1]]
template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
class BlasSolver : public LinearOperator<Matrix, Vector> {
  // Class:1 ends here

  // [[file:../../../org/composyx/solver/BlasSolver.org::*Attributes][Attributes:1]]
private:
  using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;
  using Real = typename LinearOperator<Matrix, Vector>::real_type;
  using Complex = typename std::conditional<is_complex<Scalar>::value, Scalar,
                                            std::complex<Real>>::type;

public:
  using matrix_type = Matrix;
  using vector_type = Vector;
  using scalar_type = Scalar;

private:
  Matrix _A;
  std::vector<Blas_int> _ipiv;
  std::vector<Scalar> _tau;
  bool _is_setup = false;
  bool _is_facto = false;
  bool _is_facto_qr = false;
  bool _is_facto_lq = false;
  lapack::Uplo _facto_uplo = lapack::Uplo::Lower;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/solver/BlasSolver.org::*Constructors][Constructors:1]]
public:
  BlasSolver() {}
  BlasSolver(const Matrix& A) {
    _A = A;
    _is_setup = true;
  }
  BlasSolver(Matrix&& A) {
    _A = std::move(A);
    _is_setup = true;
  }
  // Constructors:1 ends here

  // [[file:../../../org/composyx/solver/BlasSolver.org::*Move operations][Move operations:1]]
public:
  BlasSolver(const BlasSolver&) = delete;
  BlasSolver& operator=(const BlasSolver&) = delete;

  BlasSolver(BlasSolver&&) = default;
  BlasSolver& operator=(BlasSolver&&) = default;
  // Move operations:1 ends here

  // [[file:../../../org/composyx/solver/BlasSolver.org::*Setup][Setup:1]]
  void setup(const Matrix& A) {
    _A = A;
    _is_setup = true;
    _is_facto = false;
    _is_facto_qr = false;
    _is_facto_lq = false;
  }
  void setup(Matrix&& A) {
    _A = std::move(A);
    _is_setup = true;
    _is_facto = false;
    _is_facto_qr = false;
    _is_facto_lq = false;
  }
  // Setup:1 ends here

  // [[file:../../../org/composyx/solver/BlasSolver.org::*Factorization general][Factorization general:1]]
  void factorize() {
    COMPOSYX_ASSERT(_is_setup == true,
                    "BlasSolver::factorize: calling factorize before setup(A)");
    Timer<TIMER_SOLVER> t("Dense facto");
    Blas_int M = static_cast<Blas_int>(n_rows(_A));
    Blas_int N = static_cast<Blas_int>(n_cols(_A));
    Blas_int lda = static_cast<Blas_int>(get_leading_dim(_A));

    if (M == 0 || N == 0)
      return;

    Blas_int err = 0;
    std::string routine_name;

    if (M > N) { // Rectangular -> least squares -> QR
      _tau = std::vector<Scalar>(std::min(M, N));
      err = lapack::geqrf(M, N, get_ptr(_A), lda, &_tau[0]);
      _is_facto_qr = true;
      routine_name = std::string("geqrf");
    } else if (M < N) { // Rectangular -> minimum norm -> LQ
      _tau = std::vector<Scalar>(std::min(M, N));
      err = lapack::gelqf(M, N, get_ptr(_A), lda, &_tau[0]);
      _is_facto_lq = true;
      routine_name = std::string("gelqf");
    } else if (is_general(_A)) { // Square general -> LU
      _ipiv = std::vector<Blas_int>(std::min(M, N));
      err = lapack::getrf(M, N, get_ptr(_A), lda, &_ipiv[0]);
      routine_name = std::string("getrf");
    } else {
      _facto_uplo =
          is_storage_upper(_A) ? lapack::Uplo::Upper : lapack::Uplo::Lower;
      if (is_spd_or_hpd(_A)) { // Square SPD -> LLT / LLH
        err = lapack::potrf(_facto_uplo, N, get_ptr(_A), lda);
        routine_name = std::string("potrf");
      } else { // Square symmetric -> LDLT
        _ipiv = std::vector<Blas_int>(N);
        err = lapack::sytrf(_facto_uplo, N, get_ptr(_A), lda, &_ipiv[0]);
        routine_name = std::string("sytrf");
      }
    }

    if (err != 0) {
      std::string message("Error: lapack ");
      message += routine_name;
      message += std::string(" returned with value: ") + std::to_string(err);
      COMPOSYX_ASSERT(err == 0, message);
    }

    _is_facto = true;
  }
  // Factorization general:1 ends here

  // [[file:../../../org/composyx/solver/BlasSolver.org::*Factorization QR/LQ][Factorization QR/LQ:1]]
  void factorizeQR() {
    COMPOSYX_ASSERT(
        _is_setup == true,
        "BlasSolver::factorizeQR: calling factorize before setup(A)");
    Timer<TIMER_SOLVER> t("Dense facto QR");
    Blas_int M = static_cast<Blas_int>(n_rows(_A));
    Blas_int N = static_cast<Blas_int>(n_cols(_A));
    Blas_int lda = static_cast<Blas_int>(get_leading_dim(_A));

    if (M == 0 || N == 0)
      return;

    Blas_int err = 0;

    _tau = std::vector<Scalar>(std::min(M, N));
    err = lapack::geqrf(M, N, get_ptr(_A), lda, &_tau[0]);

    if (err != 0) {
      std::string message("Error: lapack geqrf returned with value: ");
      COMPOSYX_ASSERT(err == 0, message + std::to_string(err));
    }

    _is_facto = true;
    _is_facto_qr = true;
  }

  void factorizeLQ() {
    COMPOSYX_ASSERT(
        _is_setup == true,
        "BlasSolver::factorizeLQ: calling factorize before setup(A)");
    Timer<TIMER_SOLVER> t("Dense facto LQ");
    Blas_int M = static_cast<Blas_int>(n_rows(_A));
    Blas_int N = static_cast<Blas_int>(n_cols(_A));
    Blas_int lda = static_cast<Blas_int>(get_leading_dim(_A));

    if (M == 0 || N == 0)
      return;

    Blas_int err = 0;

    _tau = std::vector<Scalar>(std::min(M, N));
    err = lapack::gelqf(M, N, get_ptr(_A), lda, &_tau[0]);

    if (err != 0) {
      std::string message("Error: lapack gelqf returned with value: ");
      COMPOSYX_ASSERT(err == 0, message + std::to_string(err));
    }

    _is_facto = true;
    _is_facto_lq = true;
  }

  void multiplyQ(Matrix& C, const char side = 'L', const char trans = 'N') {
    Timer<TIMER_SOLVER> t("Dense matrix multiplyQ");
    bool is_facto_qr_or_lq = (_is_facto_qr || _is_facto_lq);
    COMPOSYX_ASSERT(is_facto_qr_or_lq,
                    "BlasSolver::multiplyQ: use factorizeQR() or factorizeLQ() "
                    "before calling multiplyQ");
    Blas_int M = static_cast<Blas_int>(n_rows(C));
    Blas_int N = static_cast<Blas_int>(n_cols(C));
    Blas_int Mq = static_cast<Blas_int>(n_rows(_A));
    Blas_int K = static_cast<Blas_int>(n_cols(_A));
    Blas_int ldq = static_cast<Blas_int>(get_leading_dim(_A));
    Blas_int ldc = static_cast<Blas_int>(get_leading_dim(C));
    lapack::Side lside = side == 'L' ? lapack::Side::Left : lapack::Side::Right;
    lapack::Op ltrans;
    if (trans == 'N') {
      ltrans = lapack::Op::NoTrans;
    } else {
      if constexpr (is_complex<Scalar>::value) {
        ltrans = lapack::Op::ConjTrans;
      } else {
        ltrans = lapack::Op::Trans;
      }
    }

    if (M == 0 or N == 0 or K == 0) {
      C = Matrix();
      return;
    }

    if (side == 'L') {
      COMPOSYX_ASSERT(
          M == Mq, "BlasSolver::multiplyQ: C = Q[^*]C, nrows(C) != nrows(Q)");
      COMPOSYX_ASSERT(
          M == K, "BlasSolver::multiplyQ: C = Q[^*]C, nrows(C) != ncols(Q)");
    } else {
      COMPOSYX_ASSERT(
          N == Mq, "BlasSolver::multiplyQ: C = CQ[^*], ncols(C) != nrows(Q)");
      COMPOSYX_ASSERT(
          N == K, "BlasSolver::multiplyQ: C = CQ[^*], ncols(C) != ncols(Q)");
    }

    Blas_int err = 0;

    auto err_occured = [](const std::string& fct_name, Blas_int ierr) {
      if (ierr != 0) {
        std::cerr << "Error: blas/lapack " << fct_name
                  << " returned with value: " << ierr << '\n';
        return true;
      }
      return false;
    };

    if (_is_facto_qr) {
      if constexpr (is_complex<Scalar>::value) {
        // Compute Q[^*]B or BQ[^*]
        err = lapack::unmqr(lside, ltrans, M, N, K, get_ptr(_A), ldq, &_tau[0],
                            get_ptr(C), ldc);

        if (err_occured("unmqr", err))
          return;
      } else {
        // Compute Q[^T]B or BQ[^T]
        err = lapack::ormqr(lside, ltrans, M, N, K, get_ptr(_A), ldq, &_tau[0],
                            get_ptr(C), ldc);

        if (err_occured("ormqr", err))
          return;
      }
    } else if (_is_facto_lq) {
      if constexpr (is_complex<Scalar>::value) {
        // Compute Q[^*]B or BQ[^*]
        err = lapack::unmlq(lside, ltrans, M, N, K, get_ptr(_A), ldq, &_tau[0],
                            get_ptr(C), ldc);

        err_occured("unmlq", err);
      } else {
        // Compute Q[^T]B or BQ[^T]
        err = lapack::ormlq(lside, ltrans, M, N, K, get_ptr(_A), ldq, &_tau[0],
                            get_ptr(C), ldc);

        err_occured("ormlq", err);
      }
    }
  }

  void matrixQ(Matrix& Q) {
    Timer<TIMER_SOLVER> t("Dense matrix QR, assemble matrix Q");
    bool is_facto_qr_or_lq = (_is_facto_qr || _is_facto_lq);
    COMPOSYX_ASSERT(is_facto_qr_or_lq == true,
                    "BlasSolver::matrixQ: use factorizeQR() or factorizeLQ() "
                    "before calling matrixQ");
    Blas_int M = static_cast<Blas_int>(n_rows(Q));
    Blas_int N = static_cast<Blas_int>(n_cols(Q));
    Blas_int K = static_cast<Blas_int>(std::min(n_rows(Q), n_cols(Q)));
    Blas_int ldq = static_cast<Blas_int>(get_leading_dim(Q));

    if (M == 0 or N == 0) {
      Q = Matrix();
      return;
    }

    Blas_int err = 0;

    auto err_occured = [](const std::string& fct_name, Blas_int ierr) {
      if (ierr != 0) {
        std::cerr << "Error: blas/lapack " << fct_name
                  << " returned with value: " << ierr << '\n';
        return true;
      }
      return false;
    };

    if (_is_facto_qr) {
      if constexpr (is_complex<Scalar>::value) {
        err = lapack::ungqr(M, N, K, get_ptr(Q), ldq, &_tau[0]);
        if (err_occured("ungqr", err))
          return;
      } else {
        err = lapack::orgqr(M, N, K, get_ptr(Q), ldq, &_tau[0]);
        if (err_occured("orgqr", err))
          return;
      }
    } else if (_is_facto_lq) {
      if constexpr (is_complex<Scalar>::value) {
        err = lapack::unglq(M, N, K, get_ptr(Q), ldq, &_tau[0]);
        err_occured("unglq", err);
      } else {
        err = lapack::orglq(M, N, K, get_ptr(Q), ldq, &_tau[0]);

        err_occured("orglq", err);
      }
    }
  }

  Matrix matrixQ() {
    Matrix Q = _A;
    matrixQ(Q);
    return Q;
  }

  void matrixR(Matrix& R) {
    size_t M = n_rows(_A);
    size_t N = n_cols(_A);
    size_t K = std::min(n_rows(_A), n_cols(_A));

    if (M == 0 or N == 0) {
      R = Matrix();
      return;
    }

    if (!_is_facto) {
      factorizeQR();
    }

    Timer<TIMER_SOLVER> t("Dense matrix QR, assemble matrix R");

    // copy values of R, upper part of the matrix QR
    for (size_t j = 0; j < K; ++j) {
      for (size_t i = 0; i <= j; ++i) {
        R(i, j) = _A(i, j);
      }
    }
  }

  Matrix matrixR() {
    if (!_is_facto) {
      factorizeQR();
    }
    size_t k = std::min(n_rows(_A), n_cols(_A));
    Matrix R = Matrix(k, n_cols(_A));
    R.set_property(composyx::MatrixStorage::upper);
    matrixR(R);
    return R;
  }

  void matrixL(Matrix& L) {
    size_t M = n_rows(_A);
    size_t N = n_cols(_A);
    size_t K = std::min(n_rows(_A), n_cols(_A));

    if (M == 0 or N == 0) {
      L = Matrix();
      return;
    }

    if (!_is_facto) {
      factorizeLQ();
    }

    Timer<TIMER_SOLVER> t("Dense matrix LQ, assemble matrix L");

    // copy values of L, lower part of the matrix LQ
    for (size_t j = 0; j < K; ++j) {
      for (size_t i = j; i < K; ++i) {
        L(i, j) = _A(i, j);
      }
    }
  }

  Matrix matrixL() {
    if (!_is_facto) {
      factorizeLQ();
    }
    size_t k = std::min(n_rows(_A), n_cols(_A));
    Matrix L = Matrix(n_rows(_A), k);
    L.set_property(composyx::MatrixStorage::lower);
    matrixL(L);
    return L;
  }
  // Factorization QR/LQ:1 ends here

  // [[file:../../../org/composyx/solver/BlasSolver.org::*Solve][Solve:1]]
  void apply(const Vector& B, Vector& X) {
    if (!_is_facto) {
      factorize();
    }

    Timer<TIMER_SOLVER> t("Dense solve");

    Blas_int M = static_cast<Blas_int>(n_rows(_A));
    Blas_int N = static_cast<Blas_int>(n_cols(_A));
    Blas_int lda = static_cast<Blas_int>(get_leading_dim(_A));
    Blas_int nrhs = static_cast<Blas_int>(n_cols(B));
    Blas_int ldb = static_cast<Blas_int>(n_rows(B));

    if (M == 0 or N == 0) {
      X = Vector();
      return;
    }

    Blas_int err = 0;

    auto err_occured = [](const std::string& fct_name, Blas_int ierr) {
      if (ierr != 0) {
        std::cerr << "Error: blas/lapack " << fct_name
                  << " returned with value: " << ierr << '\n';
        return true;
      }
      return false;
    };

    if (M > N) {
      Vector QB = B;
      Blas_int ldqb = static_cast<Blas_int>(get_leading_dim(QB));
      Blas_int K = std::min(M, N);

      if constexpr (is_complex<Scalar>::value) {
        // Compute Q^*b
        err = lapack::unmqr(lapack::Side::Left, lapack::Op::ConjTrans, M, 1, K,
                            get_ptr(_A), lda, &_tau[0], get_ptr(QB), ldqb);

        if (err_occured("unmqr", err))
          return;
      } else {
        // Compute Q^T b
        err = lapack::ormqr(lapack::Side::Left, lapack::Op::Trans, M, 1, K,
                            get_ptr(_A), lda, &_tau[0], get_ptr(QB), ldqb);

        if (err_occured("ormqr", err))
          return;
      }
      X = QB.get_block_copy(0, 0, N, 1);
      auto ldx = N;

      // TRSM: solve R x = (Q^*b)
      Scalar alpha{1};
      blas::trsm(blas::Layout::ColMajor, blas::Side::Left, blas::Uplo::Upper,
                 blas::Op::NoTrans, blas::Diag::NonUnit, N, 1, alpha,
                 get_ptr(_A), lda, get_ptr(X), ldx);
    } else if (M < N) {
      // TRSM: solve L y = b
      X = Vector(N);
      X.get_block_view(0, 0, M, 1) = B;
      Scalar* Y_ptr = get_ptr(X);
      const auto ldy = M;
      Scalar alpha{1};
      blas::trsm(blas::Layout::ColMajor, blas::Side::Left, blas::Uplo::Lower,
                 blas::Op::NoTrans, blas::Diag::NonUnit, M, 1, alpha,
                 get_ptr(_A), lda, Y_ptr, ldy);

      const auto ldx = N;
      if constexpr (is_complex<Scalar>::value) {
        // Compute X = Q^* Y
        err = lapack::unmlq(lapack::Side::Left, lapack::Op::ConjTrans, N, 1, M,
                            get_ptr(_A), lda, &_tau[0], get_ptr(X), ldx);

        err_occured("unmlq", err);
      } else {
        // Compute X = Q^T Y
        err = lapack::ormlq(lapack::Side::Left, lapack::Op::Trans, N, 1, M,
                            get_ptr(_A), lda, &_tau[0], get_ptr(X), ldx);

        err_occured("ormlq", err);
      }
    } else { // A is square
      X = B;
      if (is_general(_A)) {
        lapack::Op trans = lapack::Op::NoTrans;
        err = lapack::getrs(trans, N, nrhs, get_ptr(_A), lda, &_ipiv[0],
                            get_ptr(X), ldb);
        err_occured("getrs", err);
      } else {
        if (is_spd(_A)) {
          err = lapack::potrs(_facto_uplo, N, nrhs, get_ptr(_A), lda,
                              get_ptr(X), ldb);
          err_occured("potrs", err);
        } else {
          err = lapack::sytrs(_facto_uplo, N, nrhs, get_ptr(_A), lda, &_ipiv[0],
                              get_ptr(X), ldb);
          err_occured("sytrs", err);
        }
      }
    }
  }

  void solve(const Vector& B, Vector& X) {
    X = B;
    apply(B, X);
  }

  Vector apply(const Vector& B) {
    Vector X = B; // Create X same size as B
    apply(B, X);
    return X;
  }

  Vector solve(const Vector& B) { return apply(B); }
  // Solve:1 ends here

  // [[file:../../../org/composyx/solver/BlasSolver.org::*Triangular solve][Triangular solve:1]]
  void triangular_solve(const Vector& B, Vector& X, MatrixStorage uplo,
                        bool transposed = false) {
    if (!_is_facto) {
      factorize();
    }
    Timer<TIMER_SOLVER> t("Dense triangular solve");

    const auto nrhs = n_cols(B);
    blas::Uplo b_uplo =
        (uplo == MatrixStorage::upper) ? blas::Uplo::Upper : blas::Uplo::Lower;
    if (is_sym_or_herm(_A)) {
      if (_facto_uplo == lapack::Uplo::Lower) {
        if (b_uplo == blas::Uplo::Upper)
          COMPOSYX_WARNING("BlasSolver::triangular_solve asked for upper part "
                           "on sym/herm matrix factorized lower");
        b_uplo = blas::Uplo::Lower;
      } else {
        if (b_uplo == blas::Uplo::Lower)
          COMPOSYX_WARNING("BlasSolver::triangular_solve asked for lower part "
                           "on sym/herm matrix factorized upper");
        b_uplo = blas::Uplo::Upper;
      }
    }

    blas::Op b_trans = transposed ? blas::Op::Trans : blas::Op::NoTrans;
    if (transposed and is_complex<Scalar>::value)
      b_trans = blas::Op::ConjTrans;
    const blas::Diag b_diag = blas::Diag::NonUnit;
    Blas_int N = static_cast<Blas_int>(n_rows(_A));
    const Scalar* A = get_ptr(_A);
    Blas_int lda = static_cast<Blas_int>(get_leading_dim(_A));
    Blas_int incx = 1;

    X = B;
    Scalar* x = get_ptr(X);

    if (nrhs == 1) {
      blas::trsv(blas::Layout::ColMajor, b_uplo, b_trans, b_diag, N, A, lda, x,
                 incx);
    } else {
      blas::Side b_side = blas::Side::Left;
      Blas_int M = N;
      N = static_cast<Blas_int>(nrhs);
      Scalar alpha{1};
      Blas_int ldb = static_cast<Blas_int>(get_leading_dim(X));
      blas::trsm(blas::Layout::ColMajor, b_side, b_uplo, b_trans, b_diag, M, N,
                 alpha, A, lda, x, ldb);
    }
  }

  [[nodiscard]] Vector triangular_solve(const Vector& B, MatrixStorage uplo,
                                        bool transposed = false) {
    Vector X = B; // Create X same size as B
    triangular_solve(B, X, uplo, transposed);
    return X;
  }
  // Triangular solve:1 ends here

  // [[file:../../../org/composyx/solver/BlasSolver.org::*Display][Display:1]]
  void display(const std::string& name = "",
               std::ostream& out = std::cout) const {
    if (!name.empty())
      out << "Blas solver name: " << name << '\n';
    out << "On matrix: \n";
    _A.display("", out);
    out << "_is_setup: " << _is_setup << '\n';
    out << "_is_facto: " << _is_facto << '\n';
  }
  friend std::ostream& operator<<(std::ostream& out, const BlasSolver& bs) {
    bs.display("", out);
    return out;
  }
  // Display:1 ends here

  // [[file:../../../org/composyx/solver/BlasSolver.org::*Traits][Traits:1]]
}; //class BlasSolver
// Traits:1 ends here

// [[file:../../../org/composyx/solver/BlasSolver.org::*Traits][Traits:2]]
// Set traits
template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
struct is_solver_direct<BlasSolver<Matrix, Vector>> : public std::true_type {};
template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
struct is_solver_iterative<BlasSolver<Matrix, Vector>>
    : public std::false_type {};
template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
struct is_matrix_free<BlasSolver<Matrix, Vector>> : public std::false_type {};

template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
struct has_triangular_solve<BlasSolver<Matrix, Vector>>
    : public std::true_type {};

template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
struct vector_type<BlasSolver<Matrix, Vector>> : public std::true_type {
  using type = typename BlasSolver<Matrix, Vector>::vector_type;
};

template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
struct scalar_type<BlasSolver<Matrix, Vector>> : public std::true_type {
  using type = typename BlasSolver<Matrix, Vector>::scalar_type;
};
// Traits:2 ends here

// [[file:../../../org/composyx/solver/BlasSolver.org::*Check SPD / HPD with factorization][Check SPD / HPD with factorization:1]]
template <CPX_DenseMatrix Matrix> bool check_matrix_spd_hpd(Matrix A) {
  using Scalar = typename Matrix::value_type;
  Blas_int M = static_cast<Blas_int>(n_rows(A));
  Blas_int N = static_cast<Blas_int>(n_cols(A));
  Blas_int lda = static_cast<Blas_int>(get_leading_dim(A));

  if (M != N)
    return false;
  if (M == 0 || N == 0)
    return true;

  Blas_int err = 0;
  if (is_storage_full(A)) {
    for (Blas_int j = 0; j < N; ++j) {
      for (Blas_int i = j; i < M; ++i) {
        if (std::abs(A(i, j) - conj(A(j, i))) >
            arithmetic_tolerance<Scalar>::value)
          return false;
      }
    }
  }

  lapack::Uplo uplo =
      is_storage_upper(A) ? lapack::Uplo::Upper : lapack::Uplo::Lower;
  try {
    err = lapack::potrf(uplo, N, get_ptr(A), lda);
  } catch (...) {
    err = 1;
  }

  return (err == 0);
}
// Check SPD / HPD with factorization:1 ends here

// [[file:../../../org/composyx/solver/BlasSolver.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
