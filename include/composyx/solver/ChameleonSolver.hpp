// [[file:../../../org/composyx/solver/ChameleonSolver.org::*Header][Header:1]]
#pragma once

#include <chameleon.h>

#include "composyx/utils/Chameleon.hpp"
#include "composyx/interfaces/linalg_concepts.hpp"
namespace composyx {
template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
class ChameleonSolver;
}

#include "composyx/loc_data/DenseMatrix.hpp"
#include "composyx/solver/LinearOperator.hpp"
#include "composyx/utils/Error.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/solver/ChameleonSolver.org::*Class][Class:1]]
template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
class ChameleonSolver : public LinearOperator<Matrix, Vector> {
  // Class:1 ends here

  // [[file:../../../org/composyx/solver/ChameleonSolver.org::*Attributes][Attributes:1]]
private:
  using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;
  using Real = typename LinearOperator<Matrix, Vector>::real_type;
  using Complex = typename std::conditional<is_complex<Scalar>::value, Scalar,
                                            std::complex<Real>>::type;

public:
  using matrix_type = Matrix;
  using vector_type = Vector;
  using scalar_type = Scalar;

private:
  Matrix _A;
  CHAM_desc_t* _chamA = nullptr;
  //CHAM_ipiv_t* _chamIPIV = nullptr;
  CHAM_desc_t* _chamT = nullptr;
  bool _is_setup = false;
  bool _is_facto = false;
  bool _is_facto_qr = false;
  bool _is_facto_lq = false;
  cham_uplo_t _facto_uplo =
      ChamLower; // storage location if symmetric/triangular matrix
  int _translation = ChamInPlace;
  int _nb = 320;
  /* some operations are not available in chameleon for now, rely on lapack for them (getrf, sytrf) */
  std::vector<Blas_int> _ipiv;
  lapack::Uplo _facto_uplo_lap = lapack::Uplo::Lower;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/solver/ChameleonSolver.org::*Constructors][Constructors:1]]
public:
  ChameleonSolver() {}
  ChameleonSolver(const Matrix& A) { setup(A); }
  ChameleonSolver(Matrix&& A) { setup(A); }
  ~ChameleonSolver() {
    if (_chamT != nullptr) {
      CHAMELEON_Desc_Destroy(&_chamT);
    }
    //if (_chamIPIV != nullptr){
    //  CHAMELEON_Ipiv_Destroy( &_chamIPIV );
    //}
    if (_chamA != nullptr) {
      CHAMELEON_Desc_Destroy(&_chamA);
    }
  }
  // Constructors:1 ends here

  // [[file:../../../org/composyx/solver/ChameleonSolver.org::*Move operations][Move operations:1]]
public:
  ChameleonSolver(const ChameleonSolver&) = delete;
  ChameleonSolver& operator=(const ChameleonSolver&) = delete;

  ChameleonSolver(ChameleonSolver&&) = default;
  ChameleonSolver& operator=(ChameleonSolver&&) = default;
  // Move operations:1 ends here

  // [[file:../../../org/composyx/solver/ChameleonSolver.org::*Setup][Setup:1]]
private:
  inline cham_flttype_t getType() {
    if constexpr (is_real<Scalar>::value) {
      return is_precision_double<Scalar>::value ? ChamRealDouble
                                                : ChamRealFloat;
    } else {
      return is_precision_double<Scalar>::value ? ChamComplexDouble
                                                : ChamComplexFloat;
    }
  }

public:
  void setup(const Matrix& A) {
    _A = A;
    _is_setup = true;
    _is_facto = false;
    _is_facto_qr = false;
    _is_facto_lq = false;
    CHAMELEON_Get(CHAMELEON_TRANSLATION_MODE, &_translation);
    CHAMELEON_Get(CHAMELEON_TILE_SIZE, &_nb);
  }
  void setup(Matrix&& A) {
    _A = std::move(A);
    _is_setup = true;
    _is_facto = false;
    _is_facto_qr = false;
    _is_facto_lq = false;
  }
  // Setup:1 ends here

  // [[file:../../../org/composyx/solver/ChameleonSolver.org::*Factorization general][Factorization general:1]]
public:
  void factorize() {
    COMPOSYX_ASSERT(
        _is_setup == true,
        "ChameleonSolver::factorize: calling factorize before setup(A)");
    Timer<TIMER_SOLVER> t("Dense facto");
    int m = static_cast<int>(n_rows(_A));
    int n = static_cast<int>(n_cols(_A));
    int lda = static_cast<int>(get_leading_dim(_A));
    Blas_int mab = static_cast<Blas_int>(n_rows(_A));
    Blas_int nab = static_cast<Blas_int>(n_cols(_A));
    Blas_int ldab = static_cast<Blas_int>(get_leading_dim(_A));

    if (m == 0 || n == 0)
      return;

    int err = 0;

    if (_translation == ChamOutOfPlace) {
      /* Initialize specific chameleon structures wrapping data tiled format */
      if (_chamA != nullptr) {
        CHAMELEON_Desc_Destroy(&_chamA);
      }
      err =
          CHAMELEON_Desc_Create(&_chamA, CHAMELEON_MAT_ALLOC_TILE, getType(),
                                _nb, _nb, _nb * _nb, lda, n, 0, 0, m, n, 1, 1);
      if (err != 0) {
        std::string message("Error: ChameleonSolver::factorize "
                            "CHAMELEON_Desc_Create on _chamA ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
      err = chameleon::lap_to_cham(_A, _chamA);
      if (err != 0) {
        std::string message(
            "Error: ChameleonSolver::factorize lap_to_cham on _A -> _chamA ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
    }
    if (m != n) { // Rectangular
      if (_chamT != nullptr) {
        CHAMELEON_Desc_Destroy(&_chamT);
      }
      err = CHAMELEON_Alloc_Workspace_zgels(m, n, &_chamT, 1, 1);
      if (err != 0) {
        std::string message("Error: ChameleonSolver::factorize "
                            "CHAMELEON_Alloc_Workspace_zgels on _chamT ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
    } else if (_A.is_general()) { // Square general -> LU
      // TODO: use getrf (with partial pivoting as soon as it is supported in chameleon)
      //err = CHAMELEON_Ipiv_Create( &_chamIPIV, _chamA, NULL );
      //if(err != 0){
      //  std::string message("Error: CHAMELEON_Ipiv_Create _chamIPIV ");
      //  message += std::string(" returned with value: ") + std::to_string(err);
      //  COMPOSYX_ASSERT(err == 0, message);
      //}
    }

    /* Call the fine algorithm depending on the matrix properties */
    if (m > n) { // Rectangular -> least squares -> QR
      if (_translation == ChamOutOfPlace) {
        err = chameleon::geqrf_tile(_chamA, _chamT);
      } else {
        err = chameleon::geqrf(m, n, get_ptr(_A), lda, _chamT);
      }
    } else if (m < n) { // Rectangular -> minimum norm -> LQ
      if (_translation == ChamOutOfPlace) {
        err = chameleon::gelqf_tile(_chamA, _chamT);
      } else {
        err = chameleon::gelqf(m, n, get_ptr(_A), lda, _chamT);
      }
    } else if (_A.is_general()) { // Square general -> LU
      _ipiv = std::vector<Blas_int>(std::min(mab, nab));
      err = lapack::getrf(mab, nab, get_ptr(_A), ldab, &_ipiv[0]);
      // TODO: use getrf (with partial pivoting as soon as it is supported in chameleon)
      //if (_translation == ChamOutOfPlace){
      //  err = getrf_tile();
      //}else{
      //  err = getrf(m, n, get_ptr(_A), lda, &_ipiv[0]);
      //}
    } else {
      _facto_uplo = _A.is_storage_upper() ? ChamUpper : ChamLower;
      _facto_uplo_lap =
          _A.is_storage_upper() ? lapack::Uplo::Upper : lapack::Uplo::Lower;
      // TODO: is it supposed to work if A is full ? If yes -> consider the case UpperLower.
      if (_A.is_spd()) { // Square SPD -> LLT
        if (_translation == ChamOutOfPlace) {
          err = chameleon::potrf_tile(_facto_uplo, _chamA);
        } else {
          err = chameleon::potrf(_facto_uplo, n, get_ptr(_A), lda);
        }
      } else { // Square symmetric -> LDLT
        if constexpr (is_real<Scalar>::value) {
          COMPOSYX_WARNING("ChameleonSolver::sytrf makes sense only for "
                           "complex matrix, fallback lapack::sytrf");
          _ipiv = std::vector<Blas_int>(nab);
          err =
              lapack::sytrf(_facto_uplo_lap, nab, get_ptr(_A), ldab, &_ipiv[0]);
        } else {
          if (_translation == ChamOutOfPlace) {
            err = chameleon::sytrf_tile(_facto_uplo, _chamA);
          } else {
            err = chameleon::sytrf(_facto_uplo, n, get_ptr(_A), lda);
          }
        }
      }
    }
    if (err != 0) {
      std::string message("Error: ChameleonSolver::factorize ");
      if (m > n) {
        message += std::string("geqrf");
      } else if (m < n) {
        message += std::string("gelqf");
      } else if (_A.is_general()) {
        message += std::string("getrf");
      } else if (_A.is_spd()) {
        message += std::string("potrf");
      } else {
        message += std::string("sytrf");
      }
      message += std::string(" returned with value: ") + std::to_string(err);
      COMPOSYX_ASSERT(err == 0, message);
    }

    _is_facto = true;
  }
  // Factorization general:1 ends here

  // [[file:../../../org/composyx/solver/ChameleonSolver.org::*Factorization QR/LQ][Factorization QR/LQ:1]]
  void factorizeQR() {
    COMPOSYX_ASSERT(
        _is_setup == true,
        "ChameleonSolver::factorizeQR: calling factorize before setup(A)");
    Timer<TIMER_SOLVER> t("Dense facto QR");
    int m = static_cast<int>(n_rows(_A));
    int n = static_cast<int>(n_cols(_A));
    int lda = static_cast<int>(get_leading_dim(_A));
    int err = 0;

    if (m == 0 || n == 0)
      return;

    /* Convert (copy) the Lapack matrix to a Chameleon tile matrix */
    if (_translation == ChamOutOfPlace) {
      /* Initialize specific chameleon structures wrapping data tiled format */
      if (_chamA != nullptr) {
        CHAMELEON_Desc_Destroy(&_chamA);
      }
      err =
          CHAMELEON_Desc_Create(&_chamA, CHAMELEON_MAT_ALLOC_TILE, getType(),
                                _nb, _nb, _nb * _nb, lda, n, 0, 0, m, n, 1, 1);
      if (err != 0) {
        std::string message("Error: ChameleonSolver::factorizeQR "
                            "CHAMELEON_Desc_Create on _chamA ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
      err = chameleon::lap_to_cham(_A, _chamA);
      if (err != 0) {
        std::string message(
            "Error: ChameleonSolver::factorizeQR lap_to_cham on _A -> _chamA ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
    }

    /* Initialize specific chameleon structures wrapping data tiled format for tau */
    if (_chamT != nullptr) {
      CHAMELEON_Desc_Destroy(&_chamT);
    }
    err = CHAMELEON_Alloc_Workspace_zgels(m, n, &_chamT, 1, 1);
    if (err != 0) {
      std::string message("Error: ChameleonSolver::factorizeQR "
                          "CHAMELEON_Alloc_Workspace_zgels on _chamT ");
      message += std::string(" returned with value: ") + std::to_string(err);
      COMPOSYX_ASSERT(err == 0, message);
    }

    /* Call chameleon factorization */
    if (_translation == ChamOutOfPlace) {
      err = chameleon::geqrf_tile(_chamA, _chamT);
    } else {
      err = chameleon::geqrf(m, n, get_ptr(_A), lda, _chamT);
    }
    if (err != 0) {
      std::string message(
          "Error: ChameleonSolver::factorizeQR geqrf returned with value: ");
      COMPOSYX_ASSERT(err == 0, message + std::to_string(err));
    }

    // /* Convert (copy) the Chameleon tiled matrix A (QR) into Lapack format */
    //if (_translation == ChamOutOfPlace){
    // err = chameleon::cham_to_lap(_chamA, _A);
    // if(err != 0){
    //   std::string message("Error: ChameleonSolver::factorizeQR cham_to_lap on chamA -> A ");
    //   message += std::string(" returned with value: ") + std::to_string(err);
    //   COMPOSYX_ASSERT(err == 0, message);
    // }
    // }

    _is_facto = true;
    _is_facto_qr = true;
  }

  void factorizeLQ() {
    COMPOSYX_ASSERT(
        _is_setup == true,
        "ChameleonSolver::factorizeLQ: calling factorize before setup(A)");
    Timer<TIMER_SOLVER> t("Dense facto LQ");
    int m = static_cast<int>(n_rows(_A));
    int n = static_cast<int>(n_cols(_A));
    int lda = static_cast<int>(get_leading_dim(_A));
    int err = 0;

    if (m == 0 || n == 0)
      return;

    /* Convert (copy) the Lapack matrix to a Chameleon tile matrix */
    if (_translation == ChamOutOfPlace) {
      /* Initialize specific chameleon structures wrapping data tiled format */
      if (_chamA != nullptr) {
        CHAMELEON_Desc_Destroy(&_chamA);
      }
      err =
          CHAMELEON_Desc_Create(&_chamA, CHAMELEON_MAT_ALLOC_TILE, getType(),
                                _nb, _nb, _nb * _nb, lda, n, 0, 0, m, n, 1, 1);
      if (err != 0) {
        std::string message("Error: ChameleonSolver::factorizeLQ "
                            "CHAMELEON_Desc_Create on _chamA ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
      err = chameleon::lap_to_cham(_A, _chamA);
      if (err != 0) {
        std::string message(
            "Error: ChameleonSolver::factorizeLQ lap_to_cham on _A -> _chamA ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
    }

    /* Initialize specific chameleon structures wrapping data tiled format for tau */
    if (_chamT != nullptr) {
      CHAMELEON_Desc_Destroy(&_chamT);
    }
    err = CHAMELEON_Alloc_Workspace_zgels(m, n, &_chamT, 1, 1);
    if (err != 0) {
      std::string message("Error: ChameleonSolver::factorizeLQ "
                          "CHAMELEON_Alloc_Workspace_zgels on _chamT ");
      message += std::string(" returned with value: ") + std::to_string(err);
      COMPOSYX_ASSERT(err == 0, message);
    }

    /* Call chameleon factorization */
    if (_translation == ChamOutOfPlace) {
      err = chameleon::gelqf_tile(_chamA, _chamT);
    } else {
      err = chameleon::gelqf(m, n, get_ptr(_A), lda, _chamT);
    }
    if (err != 0) {
      std::string message(
          "Error: ChameleonSolver::factorizeLQ gelqf returned with value: ");
      COMPOSYX_ASSERT(err == 0, message + std::to_string(err));
    }

    // /* Convert (copy) the Chameleon tiled matrix A (LQ) into Lapack format */
    //if (_translation == ChamOutOfPlace){
    // err = chameleon::cham_to_lap(_chamA, _A);
    // if(err != 0){
    //   std::string message("Error: ChameleonSolver::factorizeLQ cham_to_lap on chamA -> A ");
    //   message += std::string(" returned with value: ") + std::to_string(err);
    //   COMPOSYX_ASSERT(err == 0, message);
    // }
    // }

    _is_facto = true;
    _is_facto_lq = true;
  }

  void multiplyQ(Matrix& C, const char side = 'L', const char trans = 'N') {
    Timer<TIMER_SOLVER> t("Dense matrix multiplyQ");
    bool is_facto_qr_or_lq = (_is_facto_qr || _is_facto_lq);
    COMPOSYX_ASSERT(is_facto_qr_or_lq,
                    "Error: ChameleonSolver::multiplyQ use factorizeQR() or "
                    "factorizeLQ() before calling multiplyQ");
    int m = static_cast<int>(n_rows(C));
    int n = static_cast<int>(n_cols(C));
    int mq = static_cast<int>(n_rows(_A));
    int k = static_cast<int>(n_cols(_A));
    int ldq = static_cast<int>(get_leading_dim(_A));
    int ldc = static_cast<int>(get_leading_dim(C));
    cham_side_t cside = side == 'L' ? ChamLeft : ChamRight;
    cham_trans_t ctrans;
    if (trans == 'N') {
      ctrans = ChamNoTrans;
    } else {
      if constexpr (is_complex<Scalar>::value) {
        ctrans = ChamConjTrans;
      } else {
        ctrans = ChamTrans;
      }
    }
    int err = 0;

    if (m == 0 or n == 0 or k == 0) {
      C = Matrix();
      return;
    }

    if (side == 'L') {
      COMPOSYX_ASSERT(
          m == mq,
          "Error: ChameleonSolver::multiplyQ C = Q[^*]C, nrows(C) != nrows(Q)");
      COMPOSYX_ASSERT(
          m == k,
          "Error: ChameleonSolver::multiplyQ C = Q[^*]C, nrows(C) != ncols(Q)");
    } else {
      COMPOSYX_ASSERT(
          n == mq,
          "Error: ChameleonSolver::multiplyQ C = CQ[^*], ncols(C) != nrows(Q)");
      COMPOSYX_ASSERT(
          n == k,
          "Error: ChameleonSolver::multiplyQ C = CQ[^*], ncols(C) != ncols(Q)");
    }

    /* Initialize specific chameleon structures wrapping data tiled format */
    CHAM_desc_t* chamC;
    if (_translation == ChamOutOfPlace) {
      err =
          CHAMELEON_Desc_Create(&chamC, CHAMELEON_MAT_ALLOC_TILE, getType(),
                                _nb, _nb, _nb * _nb, ldc, n, 0, 0, m, n, 1, 1);
      if (err != 0) {
        std::string message("Error: ChameleonSolver::multiplyQ "
                            "CHAMELEON_Desc_Create on chamC ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
      /* Convert (copy) the Lapack matrix to a Chameleon tile matrix */
      err = chameleon::lap_to_cham(C, chamC);
      if (err != 0) {
        std::string message(
            "Error: ChameleonSolver::multiplyQ lap_to_cham on C -> chamC ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
    }

    auto err_occured = [](const std::string& fct_name, int ierr) {
      if (ierr != 0) {
        std::cerr << "Error: chameleon " << fct_name
                  << " returned with value: " << ierr << '\n';
        return true;
      }
      return false;
    };

    if (_is_facto_qr) {
      if (_translation == ChamOutOfPlace) {
        err = chameleon::mqr_tile(cside, ctrans, _chamA, _chamT, chamC);
      } else {
        err = chameleon::mqr(cside, ctrans, m, n, k, get_ptr(_A), ldq, _chamT,
                             get_ptr(C), ldc);
      }
      if (err_occured("mqr", err))
        return;
    } else if (_is_facto_lq) {
      if (_translation == ChamOutOfPlace) {
        err = chameleon::mlq_tile(cside, ctrans, _chamA, _chamT, chamC);
      } else {
        err = chameleon::mlq(cside, ctrans, m, n, k, get_ptr(_A), ldq, _chamT,
                             get_ptr(C), ldc);
      }
      if (err_occured("mlq", err))
        return;
    }

    /* Convert (copy) the Chameleon tiled matrix C into Lapack format */
    if (_translation == ChamOutOfPlace) {
      err = chameleon::cham_to_lap(chamC, C);
      if (err != 0) {
        std::string message(
            "Error: ChameleonSolver::multiplyQ cham_to_lap on chamC -> C ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
      CHAMELEON_Desc_Destroy(&chamC);
    }
  }

  void matrixQ(Matrix& Q) {
    Timer<TIMER_SOLVER> t("Dense matrix QR, assemble matrix Q");
    bool is_facto_qr_or_lq = (_is_facto_qr || _is_facto_lq);
    COMPOSYX_ASSERT(is_facto_qr_or_lq == true,
                    "ChameleonSolver::matrixQ: use factorizeQR() or "
                    "factorizeLQ() before calling matrixQ");
    int m = static_cast<int>(n_rows(Q));
    int n = static_cast<int>(n_cols(Q));
    int ldq = static_cast<int>(get_leading_dim(Q));
    int ma = static_cast<int>(n_rows(_A));
    int na = static_cast<int>(n_cols(_A));
    int lda = static_cast<int>(get_leading_dim(_A));
    COMPOSYX_ASSERT(m == ma, "ChameleonSolver::matrixQ: nrows Q != nrows A");
    COMPOSYX_ASSERT(n == na, "ChameleonSolver::matrixQ: ncols Q != ncols A");
    int err = 0;

    if (m == 0 or n == 0) {
      Q = Matrix();
      return;
    }

    CHAM_desc_t* chamQ;
    if (_translation == ChamOutOfPlace) {
      /* Initialize specific chameleon structures wrapping data tiled format */
      err =
          CHAMELEON_Desc_Create(&chamQ, CHAMELEON_MAT_ALLOC_TILE, getType(),
                                _nb, _nb, _nb * _nb, ldq, n, 0, 0, m, n, 1, 1);
      if (err != 0) {
        std::string message("Error: ChameleonSolver::multiplyQ "
                            "CHAMELEON_Desc_Create on chamC ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
    }

    auto err_occured = [](const std::string& fct_name, int ierr) {
      if (ierr != 0) {
        std::cerr << "Error: ChameleonSolver::matrixQ " << fct_name
                  << " returned with value: " << ierr << '\n';
        return true;
      }
      return false;
    };

    if (_is_facto_qr) {
      if (_translation == ChamOutOfPlace) {
        err = chameleon::gqr_tile(_chamA, _chamT, chamQ);
      } else {
        err = chameleon::gqr(ma, na, na, get_ptr(_A), lda, _chamT, get_ptr(Q),
                             ldq);
      }
      if (err_occured("gqr", err))
        return;
    } else if (_is_facto_lq) {
      if (_translation == ChamOutOfPlace) {
        err = chameleon::glq_tile(_chamA, _chamT, chamQ);
      } else {
        err = chameleon::glq(ma, na, ma, get_ptr(_A), lda, _chamT, get_ptr(Q),
                             ldq);
      }
      if (err_occured("mlq", err))
        return;
    }

    if (_translation == ChamOutOfPlace) {
      err = chameleon::cham_to_lap(chamQ, Q);
      if (err != 0) {
        std::string message(
            "Error: ChameleonSolver::matrixQ cham_to_lap on chamQ -> Q ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
      CHAMELEON_Desc_Destroy(&chamQ);
    }
  }

  Matrix matrixQ() {
    Matrix Q = Matrix(n_rows(_A), n_cols(_A));
    matrixQ(Q);
    return Q;
  }

  void matrixR(Matrix& R) {
    int m = static_cast<int>(n_rows(_A));
    int n = static_cast<int>(n_cols(_A));
    int lda = static_cast<int>(get_leading_dim(_A));
    int k = static_cast<int>(n_rows(R));
    int nr = static_cast<int>(n_cols(R));
    int ldr = static_cast<int>(get_leading_dim(R));
    COMPOSYX_ASSERT(n == nr, "ChameleonSolver::matrixR: ncols R != ncols A");
    COMPOSYX_ASSERT(k == nr, "ChameleonSolver::matrixR: nrows R != ncols R");

    if (m == 0 or n == 0) {
      R = Matrix();
      return;
    }

    if (!_is_facto) {
      factorizeQR();
    }

    Timer<TIMER_SOLVER> t("Dense matrix QR, assemble matrix R");

    int err;

    if (_translation == ChamOutOfPlace) {
      CHAM_desc_t* chamR;
      err =
          CHAMELEON_Desc_Create(&chamR, CHAMELEON_MAT_ALLOC_TILE, getType(),
                                _nb, _nb, _nb * _nb, ldr, k, 0, 0, k, k, 1, 1);
      if (err != 0) {
        std::string message(
            "Error: ChameleonSolver::matrixR CHAMELEON_Desc_Create on chamR ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }

      err = chameleon::copy_tile(ChamUpper, _chamA, chamR);
      if (err != 0) {
        std::string message(
            "Error: ChameleonSolver::matrixR copy _chamA -> chamR ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }

      /* Convert (copy) the Chameleon tiled matrix R into Lapack format */
      err = chameleon::cham_to_lap(chamR, R);
      if (err != 0) {
        std::string message(
            "Error: ChameleonSolver::matrixR cham_to_lap on chamR -> R ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
      CHAMELEON_Desc_Destroy(&chamR);
    } else {
      err =
          chameleon::copy(ChamUpper, k, nr, get_ptr(_A), lda, get_ptr(R), ldr);
      if (err != 0) {
        std::string message("Error: ChameleonSolver::matrixR copy _A -> R ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
    }
  }

  Matrix matrixR() {
    if (!_is_facto) {
      factorizeQR();
    }
    size_t k = std::min(n_rows(_A), n_cols(_A));
    Matrix R = Matrix(k, n_cols(_A));
    R.set_property(composyx::MatrixStorage::upper);
    matrixR(R);
    return R;
  }

  void matrixL(Matrix& L) {
    int m = static_cast<int>(n_rows(_A));
    int n = static_cast<int>(n_cols(_A));
    int lda = static_cast<int>(get_leading_dim(_A));
    int ml = static_cast<int>(n_rows(L));
    int k = static_cast<int>(n_cols(L));
    int ldl = static_cast<int>(get_leading_dim(L));
    COMPOSYX_ASSERT(ml == m, "ChameleonSolver::matrixL: nrows L != nrows A");
    COMPOSYX_ASSERT(ml == k, "ChameleonSolver::matrixL: nrows L != ncols L");

    if (m == 0 or n == 0) {
      L = Matrix();
      return;
    }

    if (!_is_facto) {
      factorizeLQ();
    }

    Timer<TIMER_SOLVER> t("Dense matrix LQ, assemble matrix L");

    int err;
    if (_translation == ChamOutOfPlace) {
      CHAM_desc_t* chamL;
      err =
          CHAMELEON_Desc_Create(&chamL, CHAMELEON_MAT_ALLOC_TILE, getType(),
                                _nb, _nb, _nb * _nb, ldl, k, 0, 0, ml, k, 1, 1);
      if (err != 0) {
        std::string message(
            "Error: ChameleonSolver::matrixL CHAMELEON_Desc_Create on chamL ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }

      err = chameleon::copy_tile(ChamLower, _chamA, chamL);
      if (err != 0) {
        std::string message(
            "Error: ChameleonSolver::matrixL copy _chamA -> chamL ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }

      /* Convert (copy) the Chameleon tiled matrix L into Lapack format */
      err = chameleon::cham_to_lap(chamL, L);
      if (err != 0) {
        std::string message(
            "Error: ChameleonSolver::matrixL cham_to_lap on chamL -> L ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
      CHAMELEON_Desc_Destroy(&chamL);
    } else {
      err =
          chameleon::copy(ChamLower, ml, k, get_ptr(_A), lda, get_ptr(L), ldl);
      if (err != 0) {
        std::string message("Error: ChameleonSolver::matrixL copy _A -> L ");
        message += std::string(" returned with value: ") + std::to_string(err);
        COMPOSYX_ASSERT(err == 0, message);
      }
    }
  }

  Matrix matrixL() {
    if (!_is_facto) {
      factorizeLQ();
    }
    size_t k = std::min(n_rows(_A), n_cols(_A));
    Matrix L = Matrix(n_rows(_A), k);
    L.set_property(composyx::MatrixStorage::lower);
    matrixL(L);
    return L;
  }
  // Factorization QR/LQ:1 ends here

  // [[file:../../../org/composyx/solver/ChameleonSolver.org::*Solve][Solve:1]]
  void apply(const Vector& B, Vector& X) {
    if (!_is_facto) {
      factorize();
    }

    Timer<TIMER_SOLVER> t("Dense solve");

    int m = static_cast<int>(n_rows(_A));
    int n = static_cast<int>(n_cols(_A));
    int lda = static_cast<int>(get_leading_dim(_A));
    int nrhs = static_cast<int>(n_cols(B));

    if (m == 0 or n == 0) {
      X = Vector();
      return;
    }

    int err = 0;

    auto err_occured_cham = [](const std::string& fct_name, int ierr) {
      if (ierr != 0) {
        std::cerr << "Error: chameleon " << fct_name
                  << " returned with value: " << ierr << '\n';
        return true;
      }
      return false;
    };

    auto err_occured_lap = [](const std::string& fct_name, Blas_int ierr) {
      if (ierr != 0) {
        std::cerr << "Error: blas/lapack " << fct_name
                  << " returned with value: " << ierr << '\n';
        return true;
      }
      return false;
    };

    if (m > n) {
      // Compute Q^T * B and solve R * X = (Q^T * B)
      Vector QB = B;
      if (_translation == ChamOutOfPlace) {
        CHAM_desc_t* chamQB;
        err = CHAMELEON_Desc_Create(&chamQB, get_ptr(QB), getType(), _nb, _nb,
                                    _nb * _nb, m, nrhs, 0, 0, m, nrhs, 1, 1);
        err_occured_cham("CHAMELEON_Desc_Create on chamQB", err);
        err = chameleon::geqrs_tile(_chamA, _chamT, chamQB);
        CHAMELEON_Desc_Destroy(&chamQB);
      } else {
        err = chameleon::geqrs(m, n, nrhs, get_ptr(_A), lda, _chamT,
                               get_ptr(QB), m);
      }
      err_occured_cham("geqrs", err);
      X = QB.get_block_copy(0, 0, n, 1);
    } else if (m < n) {
      // Solve L * Y = B and Compute X = Q^T * Y
      X = Vector(n);
      X.get_block_view(0, 0, m, 1) = B;
      if (_translation == ChamOutOfPlace) {
        CHAM_desc_t* chamX;
        err = CHAMELEON_Desc_Create(&chamX, get_ptr(X), getType(), _nb, _nb,
                                    _nb * _nb, n, nrhs, 0, 0, n, nrhs, 1, 1);
        err = chameleon::gelqs_tile(_chamA, _chamT, chamX);
        err_occured_cham("gelqs", err);
        CHAMELEON_Desc_Destroy(&chamX);
      } else {
        err = chameleon::gelqs(m, n, nrhs, get_ptr(_A), lda, _chamT, get_ptr(X),
                               n);
      }
    } else { // A is square
      Blas_int Nb = static_cast<Blas_int>(n);
      Blas_int LDAb = static_cast<Blas_int>(lda);
      Blas_int NRHSb = static_cast<Blas_int>(nrhs);
      Blas_int LDXb = static_cast<Blas_int>(get_leading_dim(B));

      X = B;
      if (_A.is_general()) {
        lapack::Op trans = lapack::Op::NoTrans;
        err = lapack::getrs(trans, Nb, NRHSb, get_ptr(_A), LDAb, &_ipiv[0],
                            get_ptr(X), LDXb);
        err_occured_lap("getrs", err);
        // TODO: use getrf (with partial pivoting as soon as it is supported in chameleon)
        //err = getrs_nopiv();
        //err_occured("getrs", err);
      } else {
        if (_A.is_spd()) {
          if (_translation == ChamOutOfPlace) {
            CHAM_desc_t* chamX;
            err =
                CHAMELEON_Desc_Create(&chamX, get_ptr(X), getType(), _nb, _nb,
                                      _nb * _nb, n, nrhs, 0, 0, n, nrhs, 1, 1);
            err_occured_cham("CHAMELEON_Desc_Create on chamX", err);
            err = chameleon::potrs_tile(_facto_uplo, _chamA, chamX);
            err_occured_cham("potrs", err);
            CHAMELEON_Desc_Destroy(&chamX);
          } else {
            err = chameleon::potrs(_facto_uplo, n, nrhs, get_ptr(_A), lda,
                                   get_ptr(X), n);
          }
        } else {
          if constexpr (is_real<Scalar>::value) {
            COMPOSYX_WARNING("ChameleonSolver::sytrs makes sense only for "
                             "complex matrix, fallback lapack::sytrs");
            err = lapack::sytrs(_facto_uplo_lap, Nb, NRHSb, get_ptr(_A), LDAb,
                                &_ipiv[0], get_ptr(X), LDXb);
            err_occured_lap("sytrs", err);
          } else {
            if (_translation == ChamOutOfPlace) {
              CHAM_desc_t* chamX;
              err = CHAMELEON_Desc_Create(&chamX, get_ptr(X), getType(), _nb,
                                          _nb, _nb * _nb, n, nrhs, 0, 0, n,
                                          nrhs, 1, 1);
              err_occured_cham("CHAMELEON_Desc_Create on chamX", err);
              err = chameleon::sytrs_tile(_facto_uplo, _chamA, chamX);
              err_occured_cham("sytrs", err);
              CHAMELEON_Desc_Destroy(&chamX);
            } else {
              err = chameleon::sytrs(_facto_uplo, n, nrhs, get_ptr(_A), lda,
                                     get_ptr(X), n);
            }
          }
        }
      }
    }
  }

  void solve(const Vector& B, Vector& X) {
    X = B;
    apply(B, X);
  }

  Vector apply(const Vector& B) {
    Vector X = B; // Create X same size as B
    apply(B, X);
    return X;
  }

  Vector solve(const Vector& B) { return apply(B); }
  // Solve:1 ends here

  // [[file:../../../org/composyx/solver/ChameleonSolver.org::*Triangular solve][Triangular solve:1]]
  void triangular_solve(const Vector& B, Vector& X, MatrixStorage uplo,
                        bool transposed = false) {
    if (!_is_facto) {
      factorize();
    }
    Timer<TIMER_SOLVER> t("Dense triangular solve");

    int n = static_cast<int>(n_cols(_A));
    int lda = static_cast<int>(get_leading_dim(_A));
    int nrhs = static_cast<int>(n_cols(B));
    int ldx = static_cast<int>(get_leading_dim(X));
    cham_side_t c_side = ChamLeft;
    cham_uplo_t c_uplo = (uplo == MatrixStorage::upper) ? ChamUpper : ChamLower;
    if (_A.is_sym_or_herm()) {
      if (_facto_uplo == ChamLower) {
        if (c_uplo == ChamUpper)
          COMPOSYX_WARNING("ChameleonSolver::triangular_solve asked for upper "
                           "part on sym/herm matrix factorized lower");
        c_uplo = ChamLower;
      } else {
        if (c_uplo == ChamLower)
          COMPOSYX_WARNING("ChameleonSolver::triangular_solve asked for lower "
                           "part on sym/herm matrix factorized upper");
        c_uplo = ChamUpper;
      }
    }
    cham_trans_t c_trans = transposed ? ChamTrans : ChamNoTrans;
    if (transposed and is_complex<Scalar>::value)
      c_trans = ChamConjTrans;
    const cham_diag_t c_diag = ChamNonUnit;

    int err = 0;

    auto err_occured_cham = [](const std::string& fct_name, int ierr) {
      if (ierr != 0) {
        std::cerr << "Error: ChameleonSolver::triangular_solve " << fct_name
                  << " returned with value: " << ierr << '\n';
        return true;
      }
      return false;
    };

    X = B;

    Scalar alpha{1};
    if (_translation == ChamOutOfPlace) {
      CHAM_desc_t* chamX;
      err = CHAMELEON_Desc_Create(&chamX, get_ptr(X), getType(), _nb, _nb,
                                  _nb * _nb, ldx, nrhs, 0, 0, n, nrhs, 1, 1);
      err_occured_cham("CHAMELEON_Desc_Create on chamX", err);
      err = chameleon::trsm_tile(c_side, c_uplo, c_trans, c_diag, alpha, _chamA,
                                 chamX);
      err_occured_cham("trsm", err);
      err = CHAMELEON_Desc_Destroy(&chamX);
      err_occured_cham("CHAMELEON_Desc_Destroy on chamX", err);
    } else {
      err = chameleon::trsm(c_side, c_uplo, c_trans, c_diag, n, nrhs, alpha,
                            get_ptr(_A), lda, get_ptr(X), ldx);
    }
  }

  Vector triangular_solve(const Vector& B, MatrixStorage uplo,
                          bool transposed = false) {
    Vector X = B; // Create X same size as B
    triangular_solve(B, X, uplo, transposed);
    return X;
  }
  // Triangular solve:1 ends here

  // [[file:../../../org/composyx/solver/ChameleonSolver.org::*Set some Chameleon parameters (threads, tile size)][Set some Chameleon parameters (threads, tile size):1]]
  template <CPX_Integral Tint> void set_n_tile(Tint n_tile) {
    int nb = static_cast<int>(n_tile);
    CHAMELEON_Set(CHAMELEON_TILE_SIZE, nb);
    _nb = nb;
  }

  void set_translation(cham_translation_t translation) {
    CHAMELEON_Set(CHAMELEON_TRANSLATION_MODE, translation);
    _translation = translation;
  }
  // Set some Chameleon parameters (threads, tile size):1 ends here

  // [[file:../../../org/composyx/solver/ChameleonSolver.org::*Display][Display:1]]
  void display(const std::string& name = "",
               std::ostream& out = std::cout) const {
    if (!name.empty())
      out << "Chameleon solver name: " << name << '\n';
    out << "On matrix: \n";
    _A.display("", out);
    out << "_is_setup: " << _is_setup << '\n';
    out << "_is_facto: " << _is_facto << '\n';
  }

  friend std::ostream& operator<<(std::ostream& out, const ChameleonSolver& s) {
    s.display("", out);
    return out;
  }
  // Display:1 ends here

  // [[file:../../../org/composyx/solver/ChameleonSolver.org::*Traits][Traits:1]]
}; //class ChameleonSolver
// Traits:1 ends here

// [[file:../../../org/composyx/solver/ChameleonSolver.org::*Traits][Traits:2]]
// Set traits
template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
struct is_solver_direct<ChameleonSolver<Matrix, Vector>>
    : public std::true_type {};
template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
struct is_solver_iterative<ChameleonSolver<Matrix, Vector>>
    : public std::false_type {};
template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
struct is_matrix_free<ChameleonSolver<Matrix, Vector>>
    : public std::false_type {};

template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
struct has_triangular_solve<ChameleonSolver<Matrix, Vector>>
    : public std::true_type {};

template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
struct vector_type<ChameleonSolver<Matrix, Vector>> : public std::true_type {
  using type = typename ChameleonSolver<Matrix, Vector>::vector_type;
};

template <CPX_DenseMatrix Matrix, CPX_Vector_Single_Or_Multiple Vector>
struct scalar_type<ChameleonSolver<Matrix, Vector>> : public std::true_type {
  using type = typename ChameleonSolver<Matrix, Vector>::scalar_type;
};
// Traits:2 ends here

// [[file:../../../org/composyx/solver/ChameleonSolver.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
