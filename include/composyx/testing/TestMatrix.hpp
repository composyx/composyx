// [[file:../../../org/composyx/testing/TestMatrix.org::*Header][Header:1]]
#pragma once
#include <list>

#ifdef COMPOSYX_USE_EIGEN
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <composyx/wrappers/Eigen/Eigen.hpp>
#include <composyx/wrappers/Eigen/Eigen_converter.hpp>
#endif

#include <random>

#ifdef COMPOSYX_USE_ARMA
#include <armadillo>
#include <composyx/wrappers/armadillo/Armadillo.hpp>
#include <composyx/wrappers/armadillo/Armadillo_converter.hpp>
#endif

#include "composyx/utils/Arithmetic.hpp"
#include "composyx/utils/MatrixProperties.hpp"
#include "composyx/utils/Error.hpp"
#include "composyx/utils/Macros.hpp"
#include "composyx/loc_data/DenseMatrix.hpp"
#include "composyx/loc_data/SparseMatrixCOO.hpp"
#include "composyx/loc_data/SparseMatrixCSC.hpp"
#include "composyx/loc_data/SparseMatrixCSR.hpp"
#include "composyx/loc_data/Laplacian.hpp"

#ifndef COMPOSYX_NO_MPI
#include "composyx/part_data/PartMatrix.hpp"
#endif

namespace composyx {
namespace test_matrix {

using S_complex = std::complex<float>;
using Z_complex = std::complex<double>;
// Header:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Matrix][Matrix:1]]
template <typename Matrix, typename Scalar = scalar_type<Matrix>::type>
Matrix from_composyx_dense(const DenseMatrix<Scalar> dmat) {

  if constexpr (std::is_same_v<DenseMatrix<Scalar>, Matrix>) {
    return dmat;
  }
#ifdef COMPOSYX_USE_EIGEN
  else if constexpr (std::is_same_v<E_DenseMatrix<Scalar>, Matrix>) {
    return to_eigen_dense(dmat);
  } else if constexpr (std::is_same_v<E_SparseMatrix<Scalar>, Matrix>) {
    return to_eigen_sparse(dmat);
  }
#endif // COMPOSYX_USE_EIGEN

#ifdef COMPOSYX_USE_ARMA
  else if constexpr (std::is_same_v<A_DenseMatrix<Scalar>, Matrix>) {
    return to_armadillo_dense(dmat);
  } else if constexpr (std::is_same_v<A_SparseMatrix<Scalar>, Matrix>) {
    return to_armadillo_sparse(dmat);
  }
#endif // COMPOSYX_USE_ARMA

  else {
    return Matrix(dmat);
  }
}
// Matrix:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Vector][Vector:1]]
template <typename OutVect, typename Scalar = scalar_type<OutVect>::type>
OutVect from_composyx_dense(const Vector<Scalar> v) {

  if constexpr (std::is_same_v<Vector<Scalar>, OutVect>) {
    return v;
  }
#ifdef COMPOSYX_USE_EIGEN
  else if constexpr (std::is_same_v<E_Vector<Scalar>, OutVect>) {
    return to_eigen_dense(v);
  }
#endif // COMPOSYX_USE_EIGEN

#ifdef COMPOSYX_USE_ARMA
  else if constexpr (std::is_same_v<A_Vector<Scalar>, OutVect>) {
    return to_armadillo_dense(v);
  }
#endif // COMPOSYX_USE_ARMA
  else {
    //static_assert(false, "from_composyx_dense: unrecognized vector type");
    COMPOSYX_WARNING("from_composyx_dense: unrecognized vector type");
    return OutVect();
  }
}
// Vector:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Generic generator][Generic generator:1]]
template <typename Matrix, typename Scalar = scalar_type<Matrix>::type>
Matrix generate_matrix(size_t M, size_t N,
                       const MatrixProperties<Scalar>& properties,
                       double density = 1, bool fill_diagonal = false,
                       int seed = 1337) {

  if constexpr (std::is_same_v<Laplacian<Scalar>, Matrix>) {
    return Laplacian<Scalar>();
  } else {

    using Real = typename arithmetic_real<Scalar>::type;
    DenseMatrix<Scalar> out(M, N);

    if (density <= double{0} || density > double{1}) {
      COMPOSYX_WARNING("Generate matrix: density s must be 0 < s <= 1");
      density = 1;
    }

    if (properties.is_sym_or_herm()) {
      COMPOSYX_DIM_ASSERT(
          M, N,
          "Generate matrix symmetric or hermitian with non square matrix");
    }

    if (seed == -1) {
      std::random_device rd;
      seed = rd();
    }
    std::mt19937 gen(seed);
    std::uniform_real_distribution<> dis(Real{-1}, Real{1});

    auto generate_scalar = [&]() {
      if constexpr (is_complex<Scalar>::value) {
        Real re = dis(gen);
        Real im = dis(gen);
        return Scalar{re, im};
      } else {
        return dis(gen);
      }
    };

    // Full storage matrix
    if (properties.is_storage_full()) {

      if (properties.is_sym_or_herm()) {
        for (size_t j = 0; j < N; ++j) {
          for (size_t i = j; i < M; ++i) {
            out(i, j) = generate_scalar();
          }
        }

        // Deal with symmetry
        if (properties.is_symmetric()) {
          for (size_t j = 0; j < N; ++j) {
            for (size_t i = j + 1; i < M; ++i) {
              out(j, i) = out(i, j);
            }
          }
        }

        if (properties.is_hermitian()) {
          for (size_t j = 0; j < N; ++j) {
            for (size_t i = j + 1; i < M; ++i) {
              out(j, i) = conj<Scalar>(out(i, j));
            }
          }
        }
      }

      else {
        for (size_t j = 0; j < N; ++j) {
          for (size_t i = 0; i < M; ++i) {
            out(i, j) = generate_scalar();
          }
        }
      }
    }

    else { // Half storage
      if (properties.is_storage_upper()) {
        for (size_t j = 0; j < N; ++j) {
          for (size_t i = 0; i <= std::min(M - 1, j); ++i) {
            out(i, j) = generate_scalar();
          }
        }
      } else {
        for (size_t j = 0; j < N; ++j) {
          for (size_t i = j; i < M; ++i) {
            out(i, j) = generate_scalar();
          }
        }
      }
    }

    if (properties.is_spd()) {
      out *= out.t();
    } else if (properties.is_hpd()) {
      out *= out.h();
    }

    // For hermitian, make sure imaginary part is zero on diagonal
    else if (properties.is_hermitian()) {
      for (size_t k = 0; k < std::min(M, N); ++k) {
        out(k, k) *= conj(out(k, k));
      }
    }

    for (size_t k = 0; k < std::min(M, N); ++k) {
      out(k, k) *= Scalar{10.0};
    }

    // Put zeros to adjust for spasity
    if (density < 1) {

      // Generate a list of coefficients we can remove
      std::list<std::pair<int, int>> removable;
      if (properties.is_sym_or_herm()) {
        for (size_t j = 0; j < N; ++j) {
          for (size_t i = j; i < M; ++i) {
            if (i == j && fill_diagonal)
              continue;
            if (properties.is_storage_lower()) {
              removable.emplace_back(std::make_pair<int, int>(i, j));
            } else {
              removable.emplace_back(std::make_pair<int, int>(j, i));
            }
          }
        }
      } else {
        for (size_t j = 0; j < N; ++j) {
          for (size_t i = 0; i < M; ++i) {
            if (i == j && fill_diagonal)
              continue;
            removable.emplace_back(std::make_pair<int, int>(i, j));
          }
        }
      }

      // Randomly pick from the list coeffs to remove
      // Until exepected density is reached (or no element left)
      double current_density = 1;
      double nnz = double{0};
      const double n_coeffs = static_cast<double>(M * N);

      while (current_density > density && !removable.empty()) {
        std::uniform_int_distribution<int> select(0, removable.size() - 1);
        int k = select(gen);

        auto it = removable.begin();
        std::advance(it, k);
        const auto& [i, j] = *it;
        out(i, j) = Scalar{0};
        removable.erase(it);

        if (properties.is_sym_or_herm() && i != j) {
          if (properties.is_storage_full()) {
            out(j, i) = Scalar{0};
          }
          nnz += double{1};
        }

        nnz += double{1};
        current_density = (n_coeffs - nnz) / n_coeffs;
      }
    }

    out.copy_properties(properties);
    return from_composyx_dense<Matrix>(out);
  }
}
// Generic generator:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*General matrix][General matrix:2]]
template <typename Matrix, typename Scalar = scalar_type<Matrix>::type>
Matrix general_matrix_4_4() {
  DenseMatrix<Scalar> out;

  if constexpr (is_complex<Scalar>::value) {
    out = DenseMatrix<Scalar>({{3, 1},
                               {2, 1},
                               {0, 0},
                               {1, 0},
                               {1, 1},
                               {5, 1},
                               {0, 2},
                               {1, 0},
                               {2, 0},
                               {2, 1},
                               {6, 0},
                               {1, 0},
                               {1, 1},
                               {1, 1},
                               {1, 2},
                               {8, 2}},
                              4, 4, true);
  } else {
    out = DenseMatrix<Scalar>({3, 2, 0, 1, 1, 5, 0, 1, 2, 2, 6, 1, 1, 1, 1, 8},
                              4, 4, true);
  }

  return from_composyx_dense<Matrix>(out);
}
// General matrix:2 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*1D-laplacian][1D-laplacian:1]]
template <typename Matrix, typename Scalar = scalar_type<Matrix>::type>
Matrix laplacian_1D_matrix(size_t M) {
  if constexpr (std::is_same_v<Laplacian<Scalar>, Matrix>) {
    (void)M;
    return Laplacian<Scalar>();
  }

  else {
    DenseMatrix<Scalar> dense_lap(M, M);
    for (size_t i = 0; i < M - 1; ++i) {
      dense_lap(i, i) = 2.0;
      dense_lap(i, i + 1) = -1.0;
      dense_lap(i + 1, i) = -1.0;
    }
    dense_lap(M - 1, M - 1) = 2.0;
    dense_lap.set_spd(MatrixStorage::full);

    return from_composyx_dense<Matrix>(dense_lap);
  }
}
// 1D-laplacian:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Distributed topology][Distributed topology:1]]
#ifndef COMPOSYX_NO_MPI
// Distributed topology:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Distributed topology][Distributed topology:2]]
std::shared_ptr<Process> get_distr_process() {
  using Nei_map = std::map<int, std::vector<int>>;
  const int n_dofs = 4;

  // SD 1
  Nei_map NM0{{1, {2, 3}}, {2, {0, 3}}};

  // SD 2
  Nei_map NM1{{0, {1, 0}}, {2, {0, 3}}};

  // SD 3
  Nei_map NM2{{0, {2, 3}}, {1, {3, 0}}};

  std::vector<Subdomain> sd;
  sd.emplace_back(0, n_dofs, std::move(NM0), false);
  sd.emplace_back(1, n_dofs, std::move(NM1), false);
  sd.emplace_back(2, n_dofs, std::move(NM2), false);

  std::shared_ptr<Process> p = bind_subdomains(static_cast<int>(sd.size()));
  p->load_subdomains(sd);

  return p;
}
// Distributed topology:2 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Distributed general matrix][Distributed general matrix:1]]
template <class Scalar, class LocMatrix>
struct dist_general_matrix : public std::false_type {
  PartMatrix<LocMatrix> matrix;
  dist_general_matrix(std::shared_ptr<Process>) {
    COMPOSYX_ASSERT(false,
                    "dist_general_matrix created with wrong LocMatrix type");
  }
}; // dist_general_matrix
// Distributed general matrix:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Global matrix][Global matrix:1]]
template <class Scalar, class Matrix> Matrix dist_general_matrix_global() {}

template <>
DenseMatrix<double> dist_general_matrix_global<double, DenseMatrix<double>>() {
  return DenseMatrix<double>({15., -1., 0.,  -2., 0.,  0.,  -1., -2., 6.,  -2.,
                              -1., 0.,  0.,  0.,  0.,  -1., 14., -3., -2., -1.,
                              0.,  -4., -1., -4., 21., 0.,  -2., -1., 0.,  0.,
                              -1., 0.,  8.,  -1., 0.,  0.,  0.,  -1., -2., -3.,
                              14., -1., -2., 0.,  0.,  -1., 0.,  -2., 6.},
                             7, 7, true);
}

template <>
DenseMatrix<float> dist_general_matrix_global<float, DenseMatrix<float>>() {
  return dist_general_matrix_global<double, DenseMatrix<double>>()
      .cast<float>();
}

template <>
DenseMatrix<Z_complex>
dist_general_matrix_global<Z_complex, DenseMatrix<Z_complex>>() {
  return DenseMatrix<Z_complex>(
      {{15, 0},  {-2, 1}, {0, 0},  {-4, 3},  {0, 0},  {0, 0},   {-2, 3},
       {-1, -1}, {6, 1},  {-1, 0}, {-1, 0},  {0, 0},  {0, 0},   {0, 0},
       {0, 0},   {-2, 3}, {14, 1}, {-4, 0},  {-1, 0}, {-1, 0},  {0, 0},
       {-2, 0},  {-1, 1}, {-3, 1}, {21, -2}, {0, 0},  {-2, 2},  {-1, 1},
       {0, 0},   {0, 0},  {-2, 3}, {0, 0},   {8, 0},  {-3, 1},  {0, 0},
       {0, 0},   {0, 0},  {-1, 1}, {-2, 2},  {-1, 0}, {14, -1}, {-2, 1},
       {-1, 0},  {0, 0},  {0, 0},  {-1, 0},  {0, 0},  {-1, -1}, {6, 1}},
      7, 7);
}

template <>
DenseMatrix<S_complex>
dist_general_matrix_global<S_complex, DenseMatrix<S_complex>>() {
  return dist_general_matrix_global<Z_complex, DenseMatrix<Z_complex>>()
      .cast<S_complex>();
}

// Sparse matrices
#define TEST_MATRIX_DIST_GENRAL_GLOB(_matrixtype, _type)                       \
  template <>                                                                  \
  _matrixtype<_type> dist_general_matrix_global<_type, _matrixtype<_type>>() { \
    return _matrixtype<_type>(                                                 \
        dist_general_matrix_global<_type, DenseMatrix<_type>>());              \
  }
TEST_MATRIX_DIST_GENRAL_GLOB(SparseMatrixCOO, float)
TEST_MATRIX_DIST_GENRAL_GLOB(SparseMatrixCOO, double)
TEST_MATRIX_DIST_GENRAL_GLOB(SparseMatrixCOO, S_complex)
TEST_MATRIX_DIST_GENRAL_GLOB(SparseMatrixCOO, Z_complex)
TEST_MATRIX_DIST_GENRAL_GLOB(SparseMatrixCSC, float)
TEST_MATRIX_DIST_GENRAL_GLOB(SparseMatrixCSC, double)
TEST_MATRIX_DIST_GENRAL_GLOB(SparseMatrixCSC, S_complex)
TEST_MATRIX_DIST_GENRAL_GLOB(SparseMatrixCSC, Z_complex)
TEST_MATRIX_DIST_GENRAL_GLOB(SparseMatrixCSR, float)
TEST_MATRIX_DIST_GENRAL_GLOB(SparseMatrixCSR, double)
TEST_MATRIX_DIST_GENRAL_GLOB(SparseMatrixCSR, S_complex)
TEST_MATRIX_DIST_GENRAL_GLOB(SparseMatrixCSR, Z_complex)
// Global matrix:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*With local dense matrix][With local dense matrix:1]]
template <class Scalar>
struct dist_general_matrix<Scalar, DenseMatrix<Scalar>> {

  PartMatrix<DenseMatrix<Scalar>> matrix;

  dist_general_matrix(std::shared_ptr<Process> p) {
    //NB column-major filling
    DenseMatrix<Scalar> m(
        {7, -2, 0, -1, -1, 6, -1, -1, 0, -2, 8, -3, -1, -1, -1, 7}, 4, 4);

    // Add imaginary part for complex
    if constexpr (is_complex<Scalar>::value) {
      DenseMatrix<Scalar> m_img(
          {0, 1, 0, 2, -1, 1, 0, 0, 0, 3, 0, 1, 0, 1, 0, -1}, 4, 4);
      m += (Scalar{0, 1} * m_img);
    }

    std::map<int, DenseMatrix<Scalar>> map;
    for (int k = 0; k < 3; ++k)
      if (p->owns_subdomain(k))
        map.emplace(k, m);

    matrix = PartMatrix<DenseMatrix<Scalar>>(p, std::move(map));
  }
}; // dist_general_matrix
// With local dense matrix:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*With local sparse matrix COO][With local sparse matrix COO:1]]
template <class Scalar>
struct dist_general_matrix<Scalar, SparseMatrixCOO<Scalar>> {
  using Mat = SparseMatrixCOO<Scalar>;
  PartMatrix<Mat> matrix;

  dist_general_matrix(std::shared_ptr<Process> p) {
    PartMatrix<DenseMatrix<Scalar>> m_dense =
        dist_general_matrix<Scalar, DenseMatrix<Scalar>>(p).matrix;
    auto convert_fct = [](const DenseMatrix<Scalar>& densemat) {
      return Mat(densemat);
    };
    matrix = m_dense.template convert<Mat>(convert_fct);
  }
}; // dist_general_matrix
// With local sparse matrix COO:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*With local sparse matrix CSC][With local sparse matrix CSC:1]]
template <class Scalar>
struct dist_general_matrix<Scalar, SparseMatrixCSC<Scalar>> {
  using Mat = SparseMatrixCSC<Scalar>;
  PartMatrix<Mat> matrix;

  dist_general_matrix(std::shared_ptr<Process> p) {
    PartMatrix<SparseMatrixCOO<Scalar>> m_coo =
        dist_general_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
    auto convert_fct = [](const SparseMatrixCOO<Scalar>& coomat) {
      return coomat.to_csc();
    };
    matrix = m_coo.template convert<Mat>(convert_fct);
  }
}; // dist_general_matrix
// With local sparse matrix CSC:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*With local sparse matrix CSR][With local sparse matrix CSR:1]]
template <class Scalar>
struct dist_general_matrix<Scalar, SparseMatrixCSR<Scalar>> {
  using Mat = SparseMatrixCSR<Scalar>;
  PartMatrix<Mat> matrix;

  dist_general_matrix(std::shared_ptr<Process> p) {
    PartMatrix<SparseMatrixCOO<Scalar>> m_coo =
        dist_general_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
    auto convert_fct = [](const SparseMatrixCOO<Scalar>& coomat) {
      return coomat.to_csr();
    };
    matrix = m_coo.template convert<Mat>(convert_fct);
  }
}; // dist_general_matrix
// With local sparse matrix CSR:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*With local dense matrix Eigen][With local dense matrix Eigen:1]]
#ifdef COMPOSYX_USE_EIGEN
template <class Scalar>
struct dist_general_matrix<
    Scalar, Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> {

  PartMatrix<Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> matrix;

  dist_general_matrix(std::shared_ptr<Process> p) {
    //NB column-major filling
    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> m(
        Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>(4, 4));
    m << 7, -2, 0, -1, -1, 6, -1, -1, 0, -2, 8, -3, -1, -1, -1, 7;

    // Add imaginary part for complex
    if constexpr (is_complex<Scalar>::value) {
      Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> m_img(
          Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>(4, 4));
      m_img << 0, 1, 0, 2, -1, 1, 0, 0, 0, 3, 0, 1, 0, 1, 0, -1;
      m += (Scalar{0, 1} * m_img);
    }

    std::map<int, Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> map;
    for (int k = 0; k < 3; ++k)
      if (p->owns_subdomain(k))
        map.emplace(k, m);

    matrix = PartMatrix<Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>>(
        p, std::move(map));
  }
}; // dist_general_matrix
// With local dense matrix Eigen:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*With local sparse matrix Eigen][With local sparse matrix Eigen:1]]
template <class Scalar>
struct dist_general_matrix<Scalar, Eigen::SparseMatrix<Scalar>> {

  PartMatrix<Eigen::SparseMatrix<Scalar>> matrix;

  dist_general_matrix(std::shared_ptr<Process> p) {
    std::vector<int> i{0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3};
    std::vector<int> j{0, 1, 3, 0, 1, 2, 3, 1, 2, 3, 0, 1, 2, 3};
    std::vector<Scalar> v{7, -2, -1, -1, 6, -1, -1, -2, 8, -3, -1, -1, -1, 7};
    Eigen::SparseMatrix<Scalar> m;
    build_matrix(m, 4, 4, static_cast<int>(i.size()), i.data(), j.data(),
                 v.data());

    // Add imaginary part for complex
    if constexpr (is_complex<Scalar>::value) {
      std::vector<int> i2{0, 0, 1, 1, 2, 2, 3, 3};
      std::vector<int> j2{1, 3, 0, 1, 1, 3, 1, 3};
      std::vector<Scalar> v2{1, 2, -1, 1, 3, 1, 1, -1};
      Eigen::SparseMatrix<Scalar> m_img;
      build_matrix(m_img, 4, 4, static_cast<int>(i2.size()), i2.data(),
                   j2.data(), v2.data());
      m += (Scalar{0, 1} * m_img);
    }

    std::map<int, Eigen::SparseMatrix<Scalar>> map;
    for (int k = 0; k < 3; ++k)
      if (p->owns_subdomain(k))
        map.emplace(k, m);

    matrix = PartMatrix<Eigen::SparseMatrix<Scalar>>(p, std::move(map));
  }
}; // dist_general_matrix
#endif
// With local sparse matrix Eigen:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*With local dense matrix Armadillo][With local dense matrix Armadillo:1]]
#ifdef COMPOSYX_USE_ARMA
template <class Scalar> struct dist_general_matrix<Scalar, arma::Mat<Scalar>> {

  PartMatrix<arma::Mat<Scalar>> matrix;

  dist_general_matrix(std::shared_ptr<Process> p) {
    //NB column-major filling
    arma::Mat<Scalar> m = {
        {7, -2, 0, -1, -1, 6, -1, -1, 0, -2, 8, -3, -1, -1, -1, 7}};
    m.reshape(4, 4);
    // Add imaginary part for complex
    if constexpr (is_complex<Scalar>::value) {
      arma::Mat<Scalar> m_img = {
          {0, 1, 0, 2, -1, 1, 0, 0, 0, 3, 0, 1, 0, 1, 0, -1}};
      m_img.reshape(4, 4);
      m += (Scalar{0, 1} * m_img);
    }

    std::map<int, arma::Mat<Scalar>> map;
    for (int k = 0; k < 3; ++k)
      if (p->owns_subdomain(k))
        map.emplace(k, m);

    matrix = PartMatrix<arma::Mat<Scalar>>(p, std::move(map));
  }
}; // dist_general_matrix
// With local dense matrix Armadillo:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*With local sparse matrix Armadillo][With local sparse matrix Armadillo:1]]
template <class Scalar>
struct dist_general_matrix<Scalar, arma::SpMat<Scalar>> {

  PartMatrix<arma::SpMat<Scalar>> matrix;

  dist_general_matrix(std::shared_ptr<Process> p) {
    std::vector<int> i{0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3};
    std::vector<int> j{0, 1, 3, 0, 1, 2, 3, 1, 2, 3, 0, 1, 2, 3};
    std::vector<Scalar> v{7, -2, -1, -1, 6, -1, -1, -2, 8, -3, -1, -1, -1, 7};
    arma::SpMat<Scalar> m;
    build_matrix(m, 4, 4, static_cast<int>(i.size()), i.data(), j.data(),
                 v.data());

    // Add imaginary part for complex
    if constexpr (is_complex<Scalar>::value) {
      std::vector<int> i2{0, 0, 1, 1, 2, 2, 3, 3};
      std::vector<int> j2{1, 3, 0, 1, 1, 3, 1, 3};
      std::vector<Scalar> v2{1, 2, -1, 1, 3, 1, 1, -1};
      arma::SpMat<Scalar> m_img;
      build_matrix(m_img, 4, 4, static_cast<int>(i2.size()), i2.data(),
                   j2.data(), v2.data());
      m += (Scalar{0, 1} * m_img);
    }

    std::map<int, arma::SpMat<Scalar>> map;
    for (int k = 0; k < 3; ++k)
      if (p->owns_subdomain(k))
        map.emplace(k, m);

    matrix = PartMatrix<arma::SpMat<Scalar>>(p, std::move(map));
  }
}; // dist_general_matrix
#endif
// With local sparse matrix Armadillo:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Dense][Dense:1]]
template <class Scalar, class LocMatrix>
struct dist_spd_matrix : public std::false_type {
  PartMatrix<LocMatrix> matrix;
  dist_spd_matrix(std::shared_ptr<Process>) {
    COMPOSYX_ASSERT(false, "dist_spd_matrix created with wrong LocMatrix type");
  }
}; // dist_spd_matrix

template <class Scalar> struct dist_spd_matrix<Scalar, DenseMatrix<Scalar>> {

  PartMatrix<DenseMatrix<Scalar>> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p) {
    DenseMatrix<Scalar> m(
        {3, -1, 0, -1, -1, 4, -1, -1, 0, -1, 3, -1, -1, -1, -1, 4}, 4, 4);
    m.set_spd(MatrixStorage::full);

    std::map<int, DenseMatrix<Scalar>> map;
    for (int k = 0; k < 3; ++k)
      if (p->owns_subdomain(k))
        map.emplace(k, m);

    matrix = PartMatrix<DenseMatrix<Scalar>>(p, std::move(map));
  }
}; // dist_spd_matrix
// Dense:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Sparse COO][Sparse COO:1]]
template <class Scalar>
struct dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>> {
  using Mat = SparseMatrixCOO<Scalar>;
  PartMatrix<Mat> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p) {
    PartMatrix<DenseMatrix<Scalar>> m_dense =
        dist_spd_matrix<Scalar, DenseMatrix<Scalar>>(p).matrix;
    auto convert_fct = [](const DenseMatrix<Scalar>& densemat) {
      return Mat(densemat);
    };
    matrix = m_dense.template convert<Mat>(convert_fct);
  }
}; // dist_spd_matrix
// Sparse COO:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Sparse CSC][Sparse CSC:1]]
template <class Scalar>
struct dist_spd_matrix<Scalar, SparseMatrixCSC<Scalar>> {
  using Mat = SparseMatrixCSC<Scalar>;
  PartMatrix<Mat> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p) {
    PartMatrix<DenseMatrix<Scalar>> m_dense =
        dist_spd_matrix<Scalar, DenseMatrix<Scalar>>(p).matrix;
    auto convert_fct = [](const DenseMatrix<Scalar>& densemat) {
      return Mat(densemat);
    };
    matrix = m_dense.template convert<Mat>(convert_fct);
  }
}; // dist_spd_matrix
// Sparse CSC:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Sparse CSR][Sparse CSR:1]]
template <class Scalar>
struct dist_spd_matrix<Scalar, SparseMatrixCSR<Scalar>> {
  using Mat = SparseMatrixCSR<Scalar>;
  PartMatrix<Mat> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p) {
    PartMatrix<DenseMatrix<Scalar>> m_dense =
        dist_spd_matrix<Scalar, DenseMatrix<Scalar>>(p).matrix;
    auto convert_fct = [](const DenseMatrix<Scalar>& densemat) {
      return Mat(densemat);
    };
    matrix = m_dense.template convert<Mat>(convert_fct);
  }
}; // dist_spd_matrix
// Sparse CSR:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Dense Eigen][Dense Eigen:1]]
#ifdef COMPOSYX_USE_EIGEN
template <class Scalar>
struct dist_spd_matrix<Scalar,
                       Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> {

  PartMatrix<Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p) {
    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> m(
        Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>(4, 4));
    m << 3, -1, 0, -1, -1, 4, -1, -1, 0, -1, 3, -1, -1, -1, -1, 4;

    std::map<int, Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>> map;
    for (int k = 0; k < 3; ++k)
      if (p->owns_subdomain(k))
        map.emplace(k, m);

    matrix = PartMatrix<Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>>(
        p, std::move(map));
  }
}; // dist_spd_matrix
// Dense Eigen:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Sparse Eigen][Sparse Eigen:1]]
template <class Scalar>
struct dist_spd_matrix<Scalar, Eigen::SparseMatrix<Scalar>> {

  PartMatrix<Eigen::SparseMatrix<Scalar>> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p) {
    std::vector<int> i{0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3};
    std::vector<int> j{0, 1, 3, 0, 1, 2, 3, 1, 2, 3, 0, 1, 2, 3};
    std::vector<Scalar> v{3, -1, -1, -1, 4, -1, -1, -1, 3, -1, -1, -1, -1, 4};
    Eigen::SparseMatrix<Scalar> m;
    build_matrix(m, 4, 4, static_cast<int>(i.size()), i.data(), j.data(),
                 v.data());

    std::map<int, Eigen::SparseMatrix<Scalar>> map;
    for (int k = 0; k < 3; ++k)
      if (p->owns_subdomain(k))
        map.emplace(k, m);

    matrix = PartMatrix<Eigen::SparseMatrix<Scalar>>(p, std::move(map));
  }
}; // dist_general_matrix
#endif
// Sparse Eigen:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Dense Armadillo][Dense Armadillo:1]]
#ifdef COMPOSYX_USE_ARMA
template <class Scalar> struct dist_spd_matrix<Scalar, arma::Mat<Scalar>> {

  PartMatrix<arma::Mat<Scalar>> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p) {
    //NB column-major filling
    arma::Mat<Scalar> m = {
        {3, -1, 0, -1, -1, 4, -1, -1, 0, -1, 3, -1, -1, -1, -1, 4}};
    m.reshape(4, 4);
    std::map<int, arma::Mat<Scalar>> map;
    for (int k = 0; k < 3; ++k)
      if (p->owns_subdomain(k))
        map.emplace(k, m);

    matrix = PartMatrix<arma::Mat<Scalar>>(p, std::move(map));
  }
}; // dist_spd_matrix
// Dense Armadillo:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Sparse Armadillo][Sparse Armadillo:1]]
template <class Scalar> struct dist_spd_matrix<Scalar, arma::SpMat<Scalar>> {

  PartMatrix<arma::SpMat<Scalar>> matrix;

  dist_spd_matrix(std::shared_ptr<Process> p) {
    std::vector<int> i{0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3};
    std::vector<int> j{0, 1, 3, 0, 1, 2, 3, 1, 2, 3, 0, 1, 2, 3};
    std::vector<Scalar> v{3, -1, -1, -1, 4, -1, -1, -1, 3, -1, -1, -1, -1, 4};
    arma::SpMat<Scalar> m;
    build_matrix(m, 4, 4, static_cast<int>(i.size()), i.data(), j.data(),
                 v.data());

    std::map<int, arma::SpMat<Scalar>> map;
    for (int k = 0; k < 3; ++k)
      if (p->owns_subdomain(k))
        map.emplace(k, m);

    matrix = PartMatrix<arma::SpMat<Scalar>>(p, std::move(map));
  }
}; // dist_spd_matrix
#endif
// Sparse Armadillo:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Distributed vector][Distributed vector:1]]
template <class Scalar, class LocVector>
struct dist_vector : public std::false_type {
  PartMatrix<LocVector> vector;
  dist_vector(std::shared_ptr<Process>) {
    COMPOSYX_ASSERT(false, "dist_vector created with wrong LocVector type");
  }
}; // dist_general_matrix
// Distributed vector:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Composyx vector][Composyx vector:1]]
template <class Scalar> struct dist_vector<Scalar, Vector<Scalar>> {

  PartVector<Vector<Scalar>> vector;

  dist_vector(std::shared_ptr<Process> p) {

    std::map<int, Vector<Scalar>> map;
    if (p->owns_subdomain(0)) {
      map.emplace(0, Vector<Scalar>{{0.0, 1.0, 2.0, 3.0}, 4, 1});
    }
    if (p->owns_subdomain(1)) {
      map.emplace(1, Vector<Scalar>{{3.0, 2.0, 4.0, 5.0}, 4, 1});
    }
    if (p->owns_subdomain(2)) {
      map.emplace(2, Vector<Scalar>{{5.0, 6.0, 0.0, 3.0}, 4, 1});
    }

    vector = PartVector<Vector<Scalar>>(p, std::move(map));
  }
}; // struct dist_vector
template <class Scalar> struct dist_vector<Scalar, DenseMatrix<Scalar, -1>> {

  PartVector<DenseMatrix<Scalar, -1>> vector;

  dist_vector(std::shared_ptr<Process> p) {

    std::map<int, DenseMatrix<Scalar, -1>> map;
    if (p->owns_subdomain(0)) {
      map.emplace(0, DenseMatrix<Scalar, -1>{{0.0, 1.0, 2.0, 3.0}, 4, 1});
    }
    if (p->owns_subdomain(1)) {
      map.emplace(1, DenseMatrix<Scalar, -1>{{3.0, 2.0, 4.0, 5.0}, 4, 1});
    }
    if (p->owns_subdomain(2)) {
      map.emplace(2, DenseMatrix<Scalar, -1>{{5.0, 6.0, 0.0, 3.0}, 4, 1});
    }

    vector = PartVector<DenseMatrix<Scalar, -1>>(p, std::move(map));
  }
}; // struct dist_vector
// Composyx vector:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Eigen vector][Eigen vector:1]]
#ifdef COMPOSYX_USE_EIGEN
template <class Scalar>
struct dist_vector<Scalar, Eigen::Matrix<Scalar, Eigen::Dynamic, 1>> {

  PartVector<Eigen::Matrix<Scalar, Eigen::Dynamic, 1>> vector;
  dist_vector(std::shared_ptr<Process> p) {

    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> v0(4);
    v0 << 0.0, 1.0, 2.0, 3.0;
    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> v1(4);
    v1 << 3.0, 2.0, 4.0, 5.0;
    Eigen::Matrix<Scalar, Eigen::Dynamic, 1> v2(4);
    v2 << 5.0, 6.0, 0.0, 3.0;

    std::map<int, Eigen::Matrix<Scalar, Eigen::Dynamic, 1>> map;
    if (p->owns_subdomain(0))
      map.emplace(0, std::move(v0));
    if (p->owns_subdomain(1))
      map.emplace(1, std::move(v1));
    if (p->owns_subdomain(2))
      map.emplace(2, std::move(v2));

    vector =
        PartVector<Eigen::Matrix<Scalar, Eigen::Dynamic, 1>>(p, std::move(map));
  }
}; // struct dist_vector
#endif // COMPOSYX_USE_EIGEN
// Eigen vector:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Armadillo vector][Armadillo vector:1]]
#ifdef COMPOSYX_USE_ARMA
template <class Scalar> struct dist_vector<Scalar, arma::Col<Scalar>> {

  PartVector<arma::Col<Scalar>> vector;
  dist_vector(std::shared_ptr<Process> p) {

    std::map<int, arma::Col<Scalar>> map;
    if (p->owns_subdomain(0)) {
      map.emplace(0, arma::Col<Scalar>{0.0, 1.0, 2.0, 3.0});
    }
    if (p->owns_subdomain(1)) {
      map.emplace(1, arma::Col<Scalar>{3.0, 2.0, 4.0, 5.0});
    }
    if (p->owns_subdomain(2)) {
      map.emplace(2, arma::Col<Scalar>{5.0, 6.0, 0.0, 3.0});
    }

    vector = PartVector<arma::Col<Scalar>>(p, std::move(map));
  }
}; // struct dist_vector
#endif // COMPOSYX_USE_ARMA
// Armadillo vector:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Armadillo vector][Armadillo vector:2]]
#endif // COMPOSYX_NO_MPI
// Armadillo vector:2 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Simple range vector size 4][Simple range vector size 4:1]]
// (1, 2, 3, 4)^T for real
// ({1,1}, {2,2}, {3,-3}, {2,-1})^T for complex
template <typename Vect, typename Scalar = scalar_type<Vect>::type>
Vect simple_vector() {
  if constexpr (is_complex<Scalar>::value) {
    Vector<Scalar> v{{1, 1}, {2, 2}, {3, -3}, {2, -1}};
    return from_composyx_dense<Vect>(v);
  } else {
    Vector<Scalar> v{1, 2, 3, 4};
    return from_composyx_dense<Vect>(v);
  }
}
// Simple range vector size 4:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Fill dense matrix with random values][Fill dense matrix with random values:1]]
template <class Matrix> void fill_random(Matrix& m, int seed = -1) {
  using Scalar = typename Matrix::scalar_type;
  using Real = typename arithmetic_real<Scalar>::type;
  if (seed == -1) {
    std::random_device rd;
    seed = rd();
  }
  std::mt19937 gen(seed);
  std::uniform_real_distribution<> dis(Real{-1}, Real{1});

  for (size_t j = 0; j < n_cols(m); ++j) {
    for (size_t i = 0; i < n_rows(m); ++i) {
      if constexpr (is_complex<Scalar>::value) {
        Real re = dis(gen);
        Real im = dis(gen);
        m(i, j) = Scalar{re, im};
      } else {
        m(i, j) = dis(gen);
      }
    }
  }
}
// Fill dense matrix with random values:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Random matrices][Random matrices:1]]
template <class Scalar, class Matrix>
struct random_matrix : public std::false_type {
  Matrix matrix;
  random_matrix() {
    COMPOSYX_ASSERT(false, "random_matrix created with wrong matrix type");
  }
}; // random_matrix

template <class Scalar>
struct random_matrix<Scalar, DenseMatrix<Scalar>> : public std::true_type {
  DenseMatrix<Scalar> matrix;
  using Real = typename arithmetic_real<Scalar>::type;
  random_matrix(int nnz_max, Real min, Real max, int m, int n,
                bool upper = false) {
    matrix = DenseMatrix<Scalar>(m, n);

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(min, max);
    std::uniform_real_distribution<> dis_i(Real{0}, static_cast<Real>(m));
    std::uniform_real_distribution<> dis_j(Real{0}, static_cast<Real>(n));

    for (int k = 0; k < nnz_max; ++k) {
      int i = static_cast<int>(std::ceil(dis_i(gen) - 1));
      int j = static_cast<int>(std::ceil(dis_j(gen) - 1));
      Scalar val;
      if constexpr (is_complex<Scalar>::value) {
        Real re = dis(gen);
        Real im = dis(gen);
        val = Scalar{re, im};
      } else {
        val = dis(gen);
      }
      if (upper) {
        matrix(std::max(i, j), std::min(i, j)) += val;
      } else {
        matrix(i, j) += val;
      }
    }
    if (upper)
      matrix.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
  }
}; // random_matrix

template <class Scalar>
struct random_matrix<Scalar, SparseMatrixCOO<Scalar>> : public std::true_type {
  SparseMatrixCOO<Scalar> matrix;
  using Real = typename arithmetic_real<Scalar>::type;
  random_matrix(int nnz_max, Real min, Real max, int m, int n,
                bool upper = false) {
    DenseMatrix<Scalar> dmat = random_matrix<Scalar, DenseMatrix<Scalar>>(
                                   nnz_max, min, max, m, n, upper)
                                   .matrix;
    dmat.convert(matrix);
  }
}; // random_matrix

template <class Scalar>
struct random_matrix<Scalar, SparseMatrixCSC<Scalar>> : public std::true_type {
  SparseMatrixCSC<Scalar> matrix;
  using Real = typename arithmetic_real<Scalar>::type;
  random_matrix(int nnz_max, Real min, Real max, int m, int n,
                bool upper = false) {
    DenseMatrix<Scalar> dmat = random_matrix<Scalar, DenseMatrix<Scalar>>(
                                   nnz_max, min, max, m, n, upper)
                                   .matrix;
    dmat.convert(matrix);
  }
}; // random_matrix

template <class Scalar>
struct random_matrix<Scalar, SparseMatrixCSR<Scalar>> : public std::true_type {
  SparseMatrixCSR<Scalar> matrix;
  using Real = typename arithmetic_real<Scalar>::type;
  random_matrix(int nnz_max, Real min, Real max, int m, int n,
                bool upper = false) {
    DenseMatrix<Scalar> dmat = random_matrix<Scalar, DenseMatrix<Scalar>>(
                                   nnz_max, min, max, m, n, upper)
                                   .matrix;
    dmat.convert(matrix);
  }
}; // random_matrix

template <class Scalar>
struct random_matrix<Scalar, Vector<Scalar>> : public std::true_type {
  Vector<Scalar> vector;
  using Real = typename arithmetic_real<Scalar>::type;
  random_matrix(int nnz_max, Real min, Real max, int m) {
    DenseMatrix<Scalar> dmat =
        random_matrix<Scalar, DenseMatrix<Scalar>>(nnz_max, min, max, m, 1)
            .matrix;
    vector = Vector<Scalar>(dmat);
  }
}; // random_matrix
// Random matrices:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Random vector][Random vector:1]]
template <typename Vect, typename Scalar = scalar_type<Vect>::type,
          typename Real = typename arithmetic_real<Scalar>::type>
Vect random_vector(Real min, Real max, size_t size, int seed = -1) {
  Vector<Scalar> vect(size);

  if (seed == -1) {
    std::random_device rd;
    seed = rd();
  }
  std::mt19937 gen(seed);
  std::uniform_real_distribution<Real> dis(min, max);

  if constexpr (is_complex<Scalar>::value) {
    for (size_t i = 0; i < size; ++i)
      vect[i] = Scalar(dis(gen), dis(gen));
  } else {
    for (size_t i = 0; i < size; ++i)
      vect[i] = dis(gen);
  }

  return from_composyx_dense<Vect>(vect);
}
// Random vector:1 ends here

// [[file:../../../org/composyx/testing/TestMatrix.org::*Footer][Footer:1]]
} // namespace test_matrix
} // namespace composyx
// Footer:1 ends here
