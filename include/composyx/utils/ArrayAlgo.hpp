// [[file:../../../org/composyx/utils/ArrayAlgo.org::*Header][Header:1]]
#pragma once

#include <vector>
#include <algorithm>
#include <functional>
#include <ranges>
#include <numeric>
#include <span>

#include <composyx/interfaces/basic_concepts.hpp>
#include <composyx/utils/Error.hpp>

namespace composyx {

template <typename A>
concept CPX_Array = requires(A arr, int i) {
  { arr.size() } -> std::unsigned_integral;
  { arr[i] } -> std::convertible_to<typename A::value_type>;
  { A(i) } -> std::convertible_to<A>;
};
// Header:1 ends here

// [[file:../../../org/composyx/utils/ArrayAlgo.org::*Unique][Unique:1]]
template <CPX_Array A> void unique(A& a) {
  std::ranges::sort(a);
  auto ret = std::ranges::unique(a);
  a.erase(ret.begin(), ret.end());
}
// Unique:1 ends here

// [[file:../../../org/composyx/utils/ArrayAlgo.org::*Union with][Union with:1]]
template <CPX_Array A1, CPX_Array A2> void union_with(A1& A, const A2& B) {
  auto size_a = A.size();
  A.resize(size_a + B.size());
  std::copy(B.begin(), B.end(), A.begin() + size_a);
  unique(A);
}
// Union with:1 ends here

// [[file:../../../org/composyx/utils/ArrayAlgo.org::*Search sorted][Search sorted:1]]
template <CPX_Array A, typename T = typename A::value_type>
int searchsorted(const A& array, const T value) {
  auto l = std::lower_bound(array.begin(), array.end(), value);
  return l - array.begin();
}
// Search sorted:1 ends here

// [[file:../../../org/composyx/utils/ArrayAlgo.org::*Search sorted][Search sorted:2]]
template <CPX_Array A, CPX_IntArray I, typename T = typename A::value_type>
void searchsorted(const A& array, const A& values, I& out) {
  out = I(values.size());
  int k = 0;
  for (const T& v : values) {
    out[k++] = searchsorted(array, v);
  }
}

template <CPX_Array A, CPX_IntArray I, typename T = typename A::value_type>
I searchsorted(const A& array, const A& values) {
  I out(values.size());
  searchsorted(array, values, out);
  return out;
}
// Search sorted:2 ends here

// [[file:../../../org/composyx/utils/ArrayAlgo.org::*Generators][Generators:1]]
template <CPX_Array A, typename T = typename A::value_type>
void arange(A& array, const T begin, const T end, const T step = T{1}) {
  if (begin == end) {
    array = A();
    return;
  }
  COMPOSYX_ASSERT(step != T{0},
                  "arange(begin, end, step) with step=0 is invalid");
  if (step > T{0}) {
    COMPOSYX_ASSERT(
        end >= begin,
        "arange(begin, end, step) with step > 0 must have (end >= begin)");
  } else {
    COMPOSYX_ASSERT(
        end <= begin,
        "arange(begin, end, step) with step < 0 must have (end <= begin)");
  }
  size_t n_elts =
      std::ceil((static_cast<double>(end) - static_cast<double>(begin)) /
                static_cast<double>(step));
  if (array.size() != n_elts) {
    array = A(n_elts);
  }
  if (step == T{1}) {
    //std::ranges::iota(array, T{0}); // C++23
    std::iota(array.begin(), array.end(), begin);
  } else {
    T val = begin;
    std::ranges::generate(array, [&val, &step]() mutable {
      auto c = val;
      val += step;
      return c;
    });
  }
}

template <CPX_Array A, typename T = typename A::value_type>
A arange(const T begin, const T end, const T step = T{1}) {
  A a;
  arange<A, T>(a, begin, end, step);
  return a;
}

template <CPX_Array A, typename T = typename A::value_type>
void arange(A& array, const T end) {
  arange<A, T>(array, T{0}, end);
}

template <CPX_Array A, typename T = typename A::value_type>
A arange(const T end) {
  A a;
  arange<A, T>(a, T{0}, end);
  return a;
}

template <CPX_Array A, typename T = typename A::value_type>
void iota(A& array) {
  //std::ranges::iota(array, T{0}); // C++23
  std::iota(array.begin(), array.end(), T{0});
}
// Generators:1 ends here

// [[file:../../../org/composyx/utils/ArrayAlgo.org::*Getting indices to sort an array][Getting indices to sort an array:1]]
template <CPX_Array A, CPX_IntArray I>
void argsort(I& indices, const A& array) {
  using Tint = typename I::value_type;
  indices = I(array.size());
  iota(indices);
  auto compare = [&array](Tint lhs, Tint rhs) {
    return array[lhs] < array[rhs];
  };
  std::ranges::sort(indices, compare);
}

template <CPX_Array A, CPX_IntArray I = std::vector<int>>
I argsort(const A& array) {
  I out;
  argsort<A, I>(out, array);
  return out;
}

// In-place version
// idxs must alrdeady be a permutation vector of indices
template <CPX_Array A, CPX_IntArray I = std::vector<int>>
void inplace_argsort(I& idxs, const A& array, size_t first_idx,
                     size_t last_idx) {
  using Tint = typename I::value_type;
  using Tarr = typename A::value_type;

  COMPOSYX_ASSERT(last_idx >= first_idx, "inplace_argsort wrong parameter");
  COMPOSYX_ASSERT(array.size() >= last_idx, "inplace_argsort wrong parameter");
  COMPOSYX_ASSERT(idxs.size() >= last_idx, "inplace_argsort wrong parameter");
  size_t size = last_idx - first_idx;

  // Idea: sort with a vector of pair (value, idx) and return the array with indices
  std::vector<std::pair<Tarr, Tint>> with_idx(size);

  for (size_t k = 0; k < size; ++k) {
    with_idx[k].first = array[idxs[first_idx + k]];
    with_idx[k].second = idxs[first_idx + k];
  }

  auto less_first = [](const std::pair<Tarr, Tint>& a,
                       const std::pair<Tarr, Tint>& b) {
    return (a.first < b.first);
  };

  std::sort(with_idx.begin(), with_idx.end(), less_first);

  for (size_t k = 0; k < size; ++k) {
    idxs[first_idx + k] = with_idx[k].second;
  }
}
// Getting indices to sort an array:1 ends here

// [[file:../../../org/composyx/utils/ArrayAlgo.org::*Multilevel argsort][Multilevel argsort:1]]
template <CPX_IntArray I> void multilevel_argsort_aux(I&, size_t, size_t) {}

template <CPX_IntArray I, CPX_Array A, class... Types>
void multilevel_argsort_aux(I& idx, size_t begin, size_t end, const A& array,
                            Types&... args) {

  inplace_argsort(idx, array, begin, end);

  size_t k = begin;
  size_t l = begin + 1;
  for (; l < end; ++l) {
    if (array[idx[l]] != array[idx[k]]) {
      if (l - k > 1) {
        multilevel_argsort_aux(idx, k, l, args...);
      }
      k = l;
    }
  }
  if (l - k > 1)
    multilevel_argsort_aux(idx, k, l, args...);
}

template <CPX_IntArray I, CPX_Array A, class... Types>
I multilevel_argsort(const A& array, Types&... args) {
  I idx(array.size());
  for (size_t k = 0; k < array.size(); ++k)
    idx[k] = k;
  multilevel_argsort_aux(idx, 0, idx.size(), array, args...);
  return idx;
}
// Multilevel argsort:1 ends here

// [[file:../../../org/composyx/utils/ArrayAlgo.org::*Apply permutation][Apply permutation:1]]
template <CPX_IntArray I> void apply_permutation(const I&) {}

template <CPX_IntArray I, CPX_Array A, class... Types>
void apply_permutation(const I& perm, A& values, Types&... args) {
  COMPOSYX_DIM_ASSERT(perm.size(), values.size(),
                      "apply_permutation size(perm) != size(values)");
  A val_cpy = values;
  for (size_t k = 0; k < values.size(); ++k)
    values[k] = val_cpy[perm[k]];
  apply_permutation(perm, args...);
}
// Apply permutation:1 ends here

// [[file:../../../org/composyx/utils/ArrayAlgo.org::*Subarray extraction][Subarray extraction:1]]
template <CPX_IntArray I, CPX_Array A>
A subarray(const A& array, const I& indices) {
  A out(indices.size());
  for (size_t k = 0; k < indices.size(); ++k) {
    out[k] = array[indices[k]];
  }
  return out;
}
// Subarray extraction:1 ends here

// [[file:../../../org/composyx/utils/ArrayAlgo.org::*Check upper and lower bound][Check upper and lower bound:1]]
template <CPX_Array A, typename T = typename A::value_type>
[[nodiscard]] bool elts_lower_than(const A& array, const T val) {
  return (std::ranges::find_if(array, [&](const T& i) { return i >= val; }) ==
          std::end(array));
}

template <CPX_Array A, typename T = typename A::value_type>
[[nodiscard]] bool elts_greater_than(const A& array, const T val) {
  return (std::ranges::find_if(array, [&](const T& i) { return i <= val; }) ==
          std::end(array));
}
// Check upper and lower bound:1 ends here

// [[file:../../../org/composyx/utils/ArrayAlgo.org::*Display][Display:1]]
template <CPX_Array A>
void display(const A& a, const std::string& name = "",
             std::ostream& out = std::cout) {
  if (!name.empty())
    out << name << " :\n";
  for (auto v : a)
    out << v << ',';
  out << '\n';
}

template <CPX_Array A> std::ostream& operator<<(std::ostream& out, const A& a) {
  display(a, "", out);
  return out;
}
// Display:1 ends here

// [[file:../../../org/composyx/utils/ArrayAlgo.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
