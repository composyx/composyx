// [[file:../../../org/composyx/utils/Chameleon.org::*Header][Header:1]]
#pragma once

#include <chameleon.h>
#if !defined(COMPOSYX_NO_MPI)
#include <composyx/dist/MPI.hpp>
#endif
#include "composyx/interfaces/linalg_concepts.hpp"
#include "composyx/utils/Error.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/utils/Chameleon.org::*Class][Class:1]]
class Chameleon {
  // Class:1 ends here

  // [[file:../../../org/composyx/utils/Chameleon.org::*Attributes][Attributes:1]]
private:
  int _ncpus = -1;      // number of worker of type CPU used by Chameleon
  int _ngpus = 0;       // number of worker of type GPU used by Chameleon
  int _tile_size = 320; // size of the tiles (blocks), square sub-matrices
  int _rh = 4;          // reduction Householder size
  int _translation =
      ChamInPlace; // if conversion (i.e. copy) must be done between lapack array and chameleon tiles.
      // ChamInPlace means no conversion, we use lapack array as it is. ChamOutOfPlace means
      // copy from array to tiles.
  // Attributes:1 ends here

  // [[file:../../../org/composyx/utils/Chameleon.org::*Constructors][Constructors:1]]
public:
  Chameleon() {}
  ~Chameleon() { finalize(); }
  // Constructors:1 ends here

  // [[file:../../../org/composyx/utils/Chameleon.org::*Move operations][Move operations:1]]
public:
  Chameleon(const Chameleon&) = delete;
  Chameleon& operator=(const Chameleon&) = delete;

  Chameleon(Chameleon&&) = default;
  Chameleon& operator=(Chameleon&&) = default;
  // Move operations:1 ends here

  // [[file:../../../org/composyx/utils/Chameleon.org::*Initialize][Initialize:1]]
public:
  void setParam() {
    CHAMELEON_Enable(CHAMELEON_PROFILING_MODE);
    CHAMELEON_Disable(CHAMELEON_WARNINGS);
    CHAMELEON_Set(CHAMELEON_TRANSLATION_MODE, _translation);
    CHAMELEON_Set(CHAMELEON_TILE_SIZE, _tile_size);
    CHAMELEON_Set(CHAMELEON_HOUSEHOLDER_MODE, ChamTreeHouseholder);
    CHAMELEON_Set(CHAMELEON_HOUSEHOLDER_SIZE, _rh);
  }
  void initialize(int nc, int ng) {
    _ncpus = nc;
    _ngpus = ng;
    // if chameleon already initialized, finalize it
    finalize();
    // initialize chameleon
    int res = CHAMELEON_Init(nc, ng);
    if (res != 0) {
      COMPOSYX_WARNING("composyx::Chameleon::initialize CHAMELEON_Init failed "
                       "(may be in starpu initialization)");
    }
    // set parameters
    setParam();
    // to make execution traces if available
    RUNTIME_start_profiling();
    // prevent the runtime from polling tasks
    CHAMELEON_Pause();
  }
#if !defined(COMPOSYX_NO_MPI)
  void initialize(int nc, int ng, MPI_Comm comm) {
    _ncpus = nc;
    _ngpus = ng;
    // if chameleon already initialized, finalize it
    finalize();
    // initialize MPI and Chameleon
    int res = CHAMELEON_Init(_ncpus, _ngpus);
    if (res != 0) {
      COMPOSYX_WARNING("composyx::Chameleon::initialize CHAMELEON_Init failed "
                       "(may be in starpu initialization)");
    }
    // TODO: wait for a new chameleon release to use this function
    (void)comm; // To remove unused warning
    //CHAMELEON_InitParComm(_ncpus, _ngpus, 1, comm);
    // set parameters
    setParam();
    // to make execution traces if available
    RUNTIME_start_profiling();
    // prevent the runtime from polling tasks
    CHAMELEON_Pause();
  }
#endif
  // Initialize:1 ends here

  // [[file:../../../org/composyx/utils/Chameleon.org::*Finalize][Finalize:1]]
public:
  void finalize() {
    if (CHAMELEON_Initialized()) {
      RUNTIME_stop_profiling();
      CHAMELEON_Resume();
      CHAMELEON_Finalize();
    }
  }
  // Finalize:1 ends here

  // [[file:../../../org/composyx/utils/Chameleon.org::*Getters and Setters][Getters and Setters:1]]
public:
  int getNumCpus() { return _ncpus; }
  int getNumGpus() { return _ngpus; }
  int getTileSize() { return _tile_size; }
  void setNumCpus(int nc) { _ncpus = nc; }
  void setNumGpus(int ng) {
    COMPOSYX_ASSERT(
        ng >= 0, "Chameleon::setNumGpus: numbers of gpus must be chosen >= 0.");
    _ngpus = ng;
  }
  void setTileSize(int nb) {
    COMPOSYX_ASSERT(nb > 0, "Chameleon::setTileSize: tile size must be > 0.");
    _tile_size = nb;
    CHAMELEON_Set(CHAMELEON_TILE_SIZE, _tile_size);
  }
  void setTranslation(int translation) {
    COMPOSYX_ASSERT(
        (translation == ChamInPlace || translation == ChamOutOfPlace),
        "Chameleon::setTranslation: translation must be either ChamInPlace or "
        "ChamOutOfPlace.");
    _translation = translation;
    CHAMELEON_Set(CHAMELEON_TRANSLATION_MODE, _translation);
  }
  // Getters and Setters:1 ends here

  // [[file:../../../org/composyx/utils/Chameleon.org::*Getters and Setters][Getters and Setters:2]]
}; //class Chameleon
// Getters and Setters:2 ends here

// [[file:../../../org/composyx/utils/Chameleon.org::*Matrix conversions][Matrix conversions:1]]
namespace chameleon {

template <typename Scalar> cham_flttype_t getType(Scalar v) {
  (void)v;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      return ChamRealDouble;
    } else {
      return ChamRealFloat;
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      return ChamComplexDouble;
    } else {
      return ChamComplexFloat;
    }
  }
}

template <CPX_DenseMatrix Matrix,
          CPX_Scalar Scalar = typename Matrix::scalar_type>
static int lap_to_cham(const Matrix& inA, CHAM_desc_t* chamA) {
  CHAMELEON_Resume();
  int err = 0;
  cham_uplo_t uplo;
  if (inA.is_storage_full()) {
    uplo = ChamUpperLower;
  } else {
    uplo = inA.is_storage_upper() ? ChamUpper : ChamLower;
  }
  int lda = static_cast<int>(n_rows(inA));
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dLap2Desc(uplo, (Scalar*)get_ptr(inA), lda, chamA);
    } else {
      err = CHAMELEON_sLap2Desc(uplo, (Scalar*)get_ptr(inA), lda, chamA);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zLap2Desc(uplo, (Scalar*)get_ptr(inA), lda, chamA);
    } else {
      err = CHAMELEON_cLap2Desc(uplo, (Scalar*)get_ptr(inA), lda, chamA);
    }
  }
  CHAMELEON_Pause();
  return err;
}

template <CPX_DenseMatrix Matrix,
          CPX_Scalar Scalar = typename Matrix::scalar_type>
static int cham_to_lap(CHAM_desc_t* chamA, Matrix& outA) {
  CHAMELEON_Resume();
  int err = 0;
  cham_uplo_t uplo;
  if (outA.is_storage_full()) {
    uplo = ChamUpperLower;
  } else {
    uplo = outA.is_storage_upper() ? ChamUpper : ChamLower;
  }
  int lda = static_cast<int>(n_rows(outA));
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dDesc2Lap(uplo, chamA, get_ptr(outA), lda);
    } else {
      err = CHAMELEON_sDesc2Lap(uplo, chamA, get_ptr(outA), lda);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zDesc2Lap(uplo, chamA, get_ptr(outA), lda);
    } else {
      err = CHAMELEON_cDesc2Lap(uplo, chamA, get_ptr(outA), lda);
    }
  }
  CHAMELEON_Pause();
  return err;
}
// Matrix conversions:1 ends here

// [[file:../../../org/composyx/utils/Chameleon.org::*Matrix copy and fill with a value (laset)][Matrix copy and fill with a value (laset):1]]
template <typename Scalar>
static inline int copy(cham_uplo_t uplo, int m, int n, Scalar* A, int lda,
                       Scalar* B, int ldb) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dlacpy(uplo, m, n, A, lda, B, ldb);
    } else {
      err = CHAMELEON_slacpy(uplo, m, n, A, lda, B, ldb);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zlacpy(uplo, m, n, A, lda, B, ldb);
    } else {
      err = CHAMELEON_clacpy(uplo, m, n, A, lda, B, ldb);
    }
  }
  CHAMELEON_Pause();
  return err;
}
static inline int copy_tile(cham_uplo_t uplo, CHAM_desc_t* chamA,
                            CHAM_desc_t* chamB) {
  CHAMELEON_Resume();
  int err = 0;
  switch (chamA->dtyp) {
  case ChamRealFloat:
    err = CHAMELEON_slacpy_Tile(uplo, chamA, chamB);
    break;
  case ChamRealDouble:
    err = CHAMELEON_dlacpy_Tile(uplo, chamA, chamB);
    break;
  case ChamComplexFloat:
    err = CHAMELEON_clacpy_Tile(uplo, chamA, chamB);
    break;
  case ChamComplexDouble:
    err = CHAMELEON_zlacpy_Tile(uplo, chamA, chamB);
    break;
  default:
    COMPOSYX_ASSERT(false, "Chameleon::copy_tile: data type not supported, "
                           "must be float, double, complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
    break;
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static inline int laset(cham_uplo_t uplo, int m, int n, Scalar alpha,
                        Scalar beta, Scalar* A, int lda) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dlaset(uplo, m, n, alpha, beta, A, lda);
    } else {
      err = CHAMELEON_slaset(uplo, m, n, alpha, beta, A, lda);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zlaset(uplo, m, n, alpha, beta, A, lda);
    } else {
      err = CHAMELEON_claset(uplo, m, n, alpha, beta, A, lda);
    }
  }
  CHAMELEON_Pause();
  return err;
}
template <typename Scalar>
static inline int laset_tile(cham_uplo_t uplo, Scalar alpha, Scalar beta,
                             CHAM_desc_t* chamA) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dlaset_Tile(uplo, alpha, beta, chamA);
    } else {
      err = CHAMELEON_slaset_Tile(uplo, alpha, beta, chamA);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zlaset_Tile(uplo, alpha, beta, chamA);
    } else {
      err = CHAMELEON_claset_Tile(uplo, alpha, beta, chamA);
    }
  }
  CHAMELEON_Pause();
  return err;
}
// Matrix copy and fill with a value (laset):1 ends here

// [[file:../../../org/composyx/utils/Chameleon.org::*Random matrix generation][Random matrix generation:1]]
template <typename Scalar>
static inline int plrnt(int m, int n, Scalar* lapA, int lda,
                        unsigned long long int seed) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dplrnt(m, n, lapA, lda, seed);
    } else {
      err = CHAMELEON_splrnt(m, n, lapA, lda, seed);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zplrnt(m, n, lapA, lda, seed);
    } else {
      err = CHAMELEON_cplrnt(m, n, lapA, lda, seed);
    }
  }
  CHAMELEON_Pause();
  return err;
}
static inline int plrnt_tile(CHAM_desc_t* chamA, unsigned long long int seed) {
  CHAMELEON_Resume();
  int err = 0;
  switch (chamA->dtyp) {
  case ChamRealFloat:
    err = CHAMELEON_splrnt_Tile(chamA, seed);
    break;
  case ChamRealDouble:
    err = CHAMELEON_dplrnt_Tile(chamA, seed);
    break;
  case ChamComplexFloat:
    err = CHAMELEON_cplrnt_Tile(chamA, seed);
    break;
  case ChamComplexDouble:
    err = CHAMELEON_zplrnt_Tile(chamA, seed);
    break;
  default:
    COMPOSYX_ASSERT(false, "Chameleon::plrnt_tile: data type not supported, "
                           "must be float, double, complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
    break;
  }
  CHAMELEON_Pause();
  return err;
}
// Random matrix generation:1 ends here

// [[file:../../../org/composyx/utils/Chameleon.org::*Multiplication][Multiplication:1]]
template <typename Scalar>
static int gemm(cham_trans_t transA, cham_trans_t transB, int m, int n, int k,
                Scalar lalpha, const Scalar* lapA, int lda, const Scalar* lapB,
                int ldb, Scalar lbeta, Scalar* lapC, int ldc) {
  CHAMELEON_Resume();
  int err = 0;
  Scalar* chamA = const_cast<Scalar*>(lapA);
  Scalar* chamB = const_cast<Scalar*>(lapB);
  Scalar* chamC = lapC;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dgemm(transA, transB, m, n, k, lalpha, chamA, lda, chamB,
                            ldb, lbeta, chamC, ldc);
    } else {
      err = CHAMELEON_sgemm(transA, transB, m, n, k, lalpha, chamA, lda, chamB,
                            ldb, lbeta, chamC, ldc);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zgemm(transA, transB, m, n, k, lalpha, chamA, lda, chamB,
                            ldb, lbeta, chamC, ldc);
    } else {
      err = CHAMELEON_cgemm(transA, transB, m, n, k, lalpha, chamA, lda, chamB,
                            ldb, lbeta, chamC, ldc);
    }
  }
  CHAMELEON_Pause();
  return err;
}
template <typename Scalar>
static int gemm_tile(cham_trans_t transA, cham_trans_t transB, Scalar lalpha,
                     CHAM_desc_t* chamA, CHAM_desc_t* chamB, Scalar lbeta,
                     CHAM_desc_t* chamC) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dgemm_Tile(transA, transB, lalpha, chamA, chamB, lbeta,
                                 chamC);
    } else {
      err = CHAMELEON_sgemm_Tile(transA, transB, lalpha, chamA, chamB, lbeta,
                                 chamC);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zgemm_Tile(transA, transB, lalpha, chamA, chamB, lbeta,
                                 chamC);
    } else {
      err = CHAMELEON_cgemm_Tile(transA, transB, lalpha, chamA, chamB, lbeta,
                                 chamC);
    }
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static int symm(cham_side_t side, cham_uplo_t uplo, int m, int n, Scalar lalpha,
                const Scalar* lapA, int lda, const Scalar* lapB, int ldb,
                Scalar lbeta, Scalar* lapC, int ldc) {
  CHAMELEON_Resume();
  int err = 0;
  Scalar* chamA = const_cast<Scalar*>(lapA);
  Scalar* chamB = const_cast<Scalar*>(lapB);
  Scalar* chamC = lapC;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dsymm(side, uplo, m, n, lalpha, chamA, lda, chamB, ldb,
                            lbeta, chamC, ldc);
    } else {
      err = CHAMELEON_ssymm(side, uplo, m, n, lalpha, chamA, lda, chamB, ldb,
                            lbeta, chamC, ldc);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zsymm(side, uplo, m, n, lalpha, chamA, lda, chamB, ldb,
                            lbeta, chamC, ldc);
    } else {
      err = CHAMELEON_csymm(side, uplo, m, n, lalpha, chamA, lda, chamB, ldb,
                            lbeta, chamC, ldc);
    }
  }
  CHAMELEON_Pause();
  return err;
}
template <typename Scalar>
static int symm_tile(cham_side_t side, cham_uplo_t uplo, Scalar lalpha,
                     CHAM_desc_t* chamA, CHAM_desc_t* chamB, Scalar lbeta,
                     CHAM_desc_t* chamC) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err =
          CHAMELEON_dsymm_Tile(side, uplo, lalpha, chamA, chamB, lbeta, chamC);
    } else {
      err =
          CHAMELEON_ssymm_Tile(side, uplo, lalpha, chamA, chamB, lbeta, chamC);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err =
          CHAMELEON_zsymm_Tile(side, uplo, lalpha, chamA, chamB, lbeta, chamC);
    } else {
      err =
          CHAMELEON_csymm_Tile(side, uplo, lalpha, chamA, chamB, lbeta, chamC);
    }
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static int hemm(cham_side_t side, cham_uplo_t uplo, int m, int n, Scalar lalpha,
                const Scalar* lapA, int lda, const Scalar* lapB, int ldb,
                Scalar lbeta, Scalar* lapC, int ldc) {
  CHAMELEON_Resume();
  int err = 0;
  Scalar* chamA = const_cast<Scalar*>(lapA);
  Scalar* chamB = const_cast<Scalar*>(lapB);
  Scalar* chamC = lapC;
  if constexpr (is_complex<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zhemm(side, uplo, m, n, lalpha, chamA, lda, chamB, ldb,
                            lbeta, chamC, ldc);
    } else {
      err = CHAMELEON_chemm(side, uplo, m, n, lalpha, chamA, lda, chamB, ldb,
                            lbeta, chamC, ldc);
    }
  } else {
    COMPOSYX_ASSERT(false, "Chameleon::hemm: data type not supported, must be "
                           "complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
  }
  CHAMELEON_Pause();
  return err;
}
template <typename Scalar>
static int hemm_tile(cham_side_t side, cham_uplo_t uplo, Scalar lalpha,
                     CHAM_desc_t* chamA, CHAM_desc_t* chamB, Scalar lbeta,
                     CHAM_desc_t* chamC) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_complex<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err =
          CHAMELEON_zhemm_Tile(side, uplo, lalpha, chamA, chamB, lbeta, chamC);
    } else {
      err =
          CHAMELEON_chemm_Tile(side, uplo, lalpha, chamA, chamB, lbeta, chamC);
    }
  } else {
    COMPOSYX_ASSERT(false, "Chameleon::hemm_tile: data type not supported, "
                           "must be complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static int trmm(cham_side_t side, cham_uplo_t uplo, cham_trans_t trans,
                cham_diag_t diag, int n, int nrhs, Scalar lalpha,
                const Scalar* lapA, int lda, Scalar* lapB, int ldb) {
  CHAMELEON_Resume();
  int err = 0;
  Scalar* chamA = const_cast<Scalar*>(lapA);
  Scalar* chamB = lapB;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dtrmm(side, uplo, trans, diag, n, nrhs, lalpha, chamA,
                            lda, chamB, ldb);
    } else {
      err = CHAMELEON_strmm(side, uplo, trans, diag, n, nrhs, lalpha, chamA,
                            lda, chamB, ldb);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_ztrmm(side, uplo, trans, diag, n, nrhs, lalpha, chamA,
                            lda, chamB, ldb);
    } else {
      err = CHAMELEON_ctrmm(side, uplo, trans, diag, n, nrhs, lalpha, chamA,
                            lda, chamB, ldb);
    }
  }
  CHAMELEON_Pause();
  return err;
}
template <typename Scalar>
static int trmm_tile(cham_side_t side, cham_uplo_t uplo, cham_trans_t trans,
                     cham_diag_t diag, Scalar lalpha, CHAM_desc_t* chamA,
                     CHAM_desc_t* chamB) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dtrmm_Tile(side, uplo, trans, diag, lalpha, chamA, chamB);
    } else {
      err = CHAMELEON_strmm_Tile(side, uplo, trans, diag, lalpha, chamA, chamB);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_ztrmm_Tile(side, uplo, trans, diag, lalpha, chamA, chamB);
    } else {
      err = CHAMELEON_ctrmm_Tile(side, uplo, trans, diag, lalpha, chamA, chamB);
    }
  }
  CHAMELEON_Pause();
  return err;
}
// Multiplication:1 ends here

// [[file:../../../org/composyx/utils/Chameleon.org::*Factorization][Factorization:1]]
template <typename Scalar>
static int geqrf(int m, int n, Scalar* A, int lda, CHAM_desc_t* descT) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dgeqrf(m, n, A, lda, descT);
    } else {
      err = CHAMELEON_sgeqrf(m, n, A, lda, descT);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zgeqrf(m, n, A, lda, descT);
    } else {
      err = CHAMELEON_cgeqrf(m, n, A, lda, descT);
    }
  }
  CHAMELEON_Pause();
  return err;
}
static inline int geqrf_tile(CHAM_desc_t* chamA, CHAM_desc_t* chamT) {
  CHAMELEON_Resume();
  int err = 0;
  switch (chamA->dtyp) {
  case ChamRealFloat:
    err = CHAMELEON_sgeqrf_Tile(chamA, chamT);
    break;
  case ChamRealDouble:
    err = CHAMELEON_dgeqrf_Tile(chamA, chamT);
    break;
  case ChamComplexFloat:
    err = CHAMELEON_cgeqrf_Tile(chamA, chamT);
    break;
  case ChamComplexDouble:
    err = CHAMELEON_zgeqrf_Tile(chamA, chamT);
    break;
  default:
    COMPOSYX_ASSERT(false, "Chameleon::geqrf_tile: data type not supported, "
                           "must be float, double, complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
    break;
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static int geqrs(int m, int n, int nrhs, Scalar* A, int lda, CHAM_desc_t* descT,
                 Scalar* B, int ldb) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dgeqrs(m, n, nrhs, A, lda, descT, B, ldb);
    } else {
      err = CHAMELEON_sgeqrs(m, n, nrhs, A, lda, descT, B, ldb);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zgeqrs(m, n, nrhs, A, lda, descT, B, ldb);
    } else {
      err = CHAMELEON_cgeqrs(m, n, nrhs, A, lda, descT, B, ldb);
    }
  }
  CHAMELEON_Pause();
  return err;
}
static inline int geqrs_tile(CHAM_desc_t* chamA, CHAM_desc_t* chamT,
                             CHAM_desc_t* chamX) {
  CHAMELEON_Resume();
  int err = 0;
  switch (chamA->dtyp) {
  case ChamRealFloat:
    err = CHAMELEON_sgeqrs_Tile(chamA, chamT, chamX);
    break;
  case ChamRealDouble:
    err = CHAMELEON_dgeqrs_Tile(chamA, chamT, chamX);
    break;
  case ChamComplexFloat:
    err = CHAMELEON_cgeqrs_Tile(chamA, chamT, chamX);
    break;
  case ChamComplexDouble:
    err = CHAMELEON_zgeqrs_Tile(chamA, chamT, chamX);
    break;
  default:
    COMPOSYX_ASSERT(false, "Chameleon::geqrs_tile: data type not supported, "
                           "must be float, double, complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
    break;
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static int mqr(cham_side_t side, cham_trans_t trans, int m, int n, int k,
               Scalar* A, int lda, CHAM_desc_t* descT, Scalar* C, int ldc) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dormqr(side, trans, m, n, k, A, lda, descT, C, ldc);
    } else {
      err = CHAMELEON_sormqr(side, trans, m, n, k, A, lda, descT, C, ldc);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zunmqr(side, trans, m, n, k, A, lda, descT, C, ldc);
    } else {
      err = CHAMELEON_cunmqr(side, trans, m, n, k, A, lda, descT, C, ldc);
    }
  }
  CHAMELEON_Pause();
  return err;
}
static inline int mqr_tile(cham_side_t side, cham_trans_t trans,
                           CHAM_desc_t* chamA, CHAM_desc_t* chamT,
                           CHAM_desc_t* chamC) {
  CHAMELEON_Resume();
  int err = 0;
  switch (chamA->dtyp) {
  case ChamRealFloat:
    err = CHAMELEON_sormqr_Tile(side, trans, chamA, chamT, chamC);
    break;
  case ChamRealDouble:
    err = CHAMELEON_dormqr_Tile(side, trans, chamA, chamT, chamC);
    break;
  case ChamComplexFloat:
    err = CHAMELEON_cunmqr_Tile(side, trans, chamA, chamT, chamC);
    break;
  case ChamComplexDouble:
    err = CHAMELEON_zunmqr_Tile(side, trans, chamA, chamT, chamC);
    break;
  default:
    COMPOSYX_ASSERT(false, "Chameleon::mqr_tile: data type not supported, must "
                           "be float, double, complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
    break;
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static int gqr(int m, int n, int k, Scalar* A, int lda, CHAM_desc_t* descT,
               Scalar* Q, int ldq) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dorgqr(m, n, k, A, lda, descT, Q, ldq);
    } else {
      err = CHAMELEON_sorgqr(m, n, k, A, lda, descT, Q, ldq);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zungqr(m, n, k, A, lda, descT, Q, ldq);
    } else {
      err = CHAMELEON_cungqr(m, n, k, A, lda, descT, Q, ldq);
    }
  }
  CHAMELEON_Pause();
  return err;
}
static inline int gqr_tile(CHAM_desc_t* chamA, CHAM_desc_t* chamT,
                           CHAM_desc_t* chamQ) {
  CHAMELEON_Resume();
  int err = 0;
  switch (chamA->dtyp) {
  case ChamRealFloat:
    err = CHAMELEON_sorgqr_Tile(chamA, chamT, chamQ);
    break;
  case ChamRealDouble:
    err = CHAMELEON_dorgqr_Tile(chamA, chamT, chamQ);
    break;
  case ChamComplexFloat:
    err = CHAMELEON_cungqr_Tile(chamA, chamT, chamQ);
    break;
  case ChamComplexDouble:
    err = CHAMELEON_zungqr_Tile(chamA, chamT, chamQ);
    break;
  default:
    COMPOSYX_ASSERT(false, "Chameleon::gqr_tile: data type not supported, must "
                           "be float, double, complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
    break;
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static int gelqf(int m, int n, Scalar* A, int lda, CHAM_desc_t* descT) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dgelqf(m, n, A, lda, descT);
    } else {
      err = CHAMELEON_sgelqf(m, n, A, lda, descT);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zgelqf(m, n, A, lda, descT);
    } else {
      err = CHAMELEON_cgelqf(m, n, A, lda, descT);
    }
  }
  CHAMELEON_Pause();
  return err;
}
static inline int gelqf_tile(CHAM_desc_t* chamA, CHAM_desc_t* chamT) {
  CHAMELEON_Resume();
  int err = 0;
  switch (chamA->dtyp) {
  case ChamRealFloat:
    err = CHAMELEON_sgelqf_Tile(chamA, chamT);
    break;
  case ChamRealDouble:
    err = CHAMELEON_dgelqf_Tile(chamA, chamT);
    break;
  case ChamComplexFloat:
    err = CHAMELEON_cgelqf_Tile(chamA, chamT);
    break;
  case ChamComplexDouble:
    err = CHAMELEON_zgelqf_Tile(chamA, chamT);
    break;
  default:
    COMPOSYX_ASSERT(false, "Chameleon::gelqf_tile: data type not supported, "
                           "must be float, double, complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
    break;
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static int gelqs(int m, int n, int nrhs, Scalar* A, int lda, CHAM_desc_t* descT,
                 Scalar* B, int ldb) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dgelqs(m, n, nrhs, A, lda, descT, B, ldb);
    } else {
      err = CHAMELEON_sgelqs(m, n, nrhs, A, lda, descT, B, ldb);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zgelqs(m, n, nrhs, A, lda, descT, B, ldb);
    } else {
      err = CHAMELEON_cgelqs(m, n, nrhs, A, lda, descT, B, ldb);
    }
  }
  CHAMELEON_Pause();
  return err;
}
static inline int gelqs_tile(CHAM_desc_t* chamA, CHAM_desc_t* chamT,
                             CHAM_desc_t* chamX) {
  CHAMELEON_Resume();
  int err = 0;
  switch (chamA->dtyp) {
  case ChamRealFloat:
    err = CHAMELEON_sgelqs_Tile(chamA, chamT, chamX);
    break;
  case ChamRealDouble:
    err = CHAMELEON_dgelqs_Tile(chamA, chamT, chamX);
    break;
  case ChamComplexFloat:
    err = CHAMELEON_cgelqs_Tile(chamA, chamT, chamX);
    break;
  case ChamComplexDouble:
    err = CHAMELEON_zgelqs_Tile(chamA, chamT, chamX);
    break;
  default:
    COMPOSYX_ASSERT(false, "Chameleon::gelqs_tile: data type not supported, "
                           "must be float, double, complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
    break;
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static int mlq(cham_side_t side, cham_trans_t trans, int m, int n, int k,
               Scalar* A, int lda, CHAM_desc_t* descT, Scalar* C, int ldc) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dormlq(side, trans, m, n, k, A, lda, descT, C, ldc);
    } else {
      err = CHAMELEON_sormlq(side, trans, m, n, k, A, lda, descT, C, ldc);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zunmlq(side, trans, m, n, k, A, lda, descT, C, ldc);
    } else {
      err = CHAMELEON_cunmlq(side, trans, m, n, k, A, lda, descT, C, ldc);
    }
  }
  CHAMELEON_Pause();
  return err;
}
static inline int mlq_tile(cham_side_t side, cham_trans_t trans,
                           CHAM_desc_t* chamA, CHAM_desc_t* chamT,
                           CHAM_desc_t* chamC) {
  CHAMELEON_Resume();
  int err = 0;
  switch (chamA->dtyp) {
  case ChamRealFloat:
    err = CHAMELEON_sormlq_Tile(side, trans, chamA, chamT, chamC);
    break;
  case ChamRealDouble:
    err = CHAMELEON_dormlq_Tile(side, trans, chamA, chamT, chamC);
    break;
  case ChamComplexFloat:
    err = CHAMELEON_cunmlq_Tile(side, trans, chamA, chamT, chamC);
    break;
  case ChamComplexDouble:
    err = CHAMELEON_zunmlq_Tile(side, trans, chamA, chamT, chamC);
    break;
  default:
    COMPOSYX_ASSERT(false, "Chameleon::mlq_tile: data type not supported, must "
                           "be float, double, complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
    break;
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static int glq(int m, int n, int k, Scalar* A, int lda, CHAM_desc_t* descT,
               Scalar* Q, int ldq) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dorglq(m, n, k, A, lda, descT, Q, ldq);
    } else {
      err = CHAMELEON_sorglq(m, n, k, A, lda, descT, Q, ldq);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zunglq(m, n, k, A, lda, descT, Q, ldq);
    } else {
      err = CHAMELEON_cunglq(m, n, k, A, lda, descT, Q, ldq);
    }
  }
  CHAMELEON_Pause();
  return err;
}
static inline int glq_tile(CHAM_desc_t* chamA, CHAM_desc_t* chamT,
                           CHAM_desc_t* chamQ) {
  CHAMELEON_Resume();
  int err = 0;
  switch (chamA->dtyp) {
  case ChamRealFloat:
    err = CHAMELEON_sorglq_Tile(chamA, chamT, chamQ);
    break;
  case ChamRealDouble:
    err = CHAMELEON_dorglq_Tile(chamA, chamT, chamQ);
    break;
  case ChamComplexFloat:
    err = CHAMELEON_cunglq_Tile(chamA, chamT, chamQ);
    break;
  case ChamComplexDouble:
    err = CHAMELEON_zunglq_Tile(chamA, chamT, chamQ);
    break;
  default:
    COMPOSYX_ASSERT(false, "Chameleon::glq_tile: data type not supported, must "
                           "be float, double, complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
    break;
  }
  CHAMELEON_Pause();
  return err;
}

//template<typename Scalar>
//static int getrf(int m, int n, Scalar *A, int lda, int *ipiv){
//  CHAMELEON_Resume();
//  int err = 0;
//  if constexpr(is_real<Scalar>::value) {
//    if constexpr(is_precision_double<Scalar>::value) {
//      err = CHAMELEON_dgetrf(m, n, A, lda, ipiv);
//    } else {
//      err = CHAMELEON_sgetrf(m, n, A, lda, ipiv);
//    }
//  } else {
//    if constexpr(is_precision_double<Scalar>::value) {
//      err = CHAMELEON_zgetrf(m, n, A, lda, ipiv);
//    } else {
//      err = CHAMELEON_cgetrf(m, n, A, lda, ipiv);
//    }
//  }
//  CHAMELEON_Pause();
//  return err;
//}
//static inline int getrf_tile(CHAM_desc_t *chamA, CHAM_desc_t *chamIPIV){
//  CHAMELEON_Resume();
//  int err = 0;
//  switch (chamA->dtyp) {
//  case ChamRealFloat:
//    err = CHAMELEON_sgetrf_Tile( chamA, chamIPIV );
//    break;
//  case ChamRealDouble:
//    err = CHAMELEON_dgetrf_Tile( chamA, chamIPIV );
//    break;
//  case ChamComplexFloat:
//    err = CHAMELEON_cgetrf_Tile( chamA, chamIPIV );
//    break;
//  case ChamComplexDouble:
//    err = CHAMELEON_zgetrf_Tile( chamA, chamIPIV );
//    break;
//  default:
//    COMPOSYX_ASSERT(false, "Chameleon::getrf_tile: data type not supported, must be float, double, complex, double complex");
//    err = CHAMELEON_ERR_NOT_SUPPORTED;
//    break;
//  }
//  CHAMELEON_Pause();
//  return err;
//}

//template<typename Scalar>
//static int getrf_nopiv(int m, int n, Scalar *A, int lda){
//  CHAMELEON_Resume();
//  int err = 0;
//  if constexpr(is_real<Scalar>::value) {
//    if constexpr(is_precision_double<Scalar>::value) {
//      err = CHAMELEON_dgetrf(m, n, A, lda);
//    } else {
//      err = CHAMELEON_sgetrf(m, n, A, lda);
//    }
//  } else {
//    if constexpr(is_precision_double<Scalar>::value) {
//      err = CHAMELEON_zgetrf(m, n, A, lda);
//    } else {
//      err = CHAMELEON_cgetrf(m, n, A, lda);
//    }
//  }
//  CHAMELEON_Pause();
//  return err;
//}
//static inline int getrf_nopiv_tile(CHAM_desc_t *chamA){
//  CHAMELEON_Resume();
//  int err = 0;
//  switch (chamA->dtyp) {
//  case ChamRealFloat:
//    err = CHAMELEON_sgetrf_nopiv_Tile( chamA );
//    break;
//  case ChamRealDouble:
//    err = CHAMELEON_dgetrf_nopiv_Tile( chamA );
//    break;
//  case ChamComplexFloat:
//    err = CHAMELEON_cgetrf_nopiv_Tile( chamA );
//    break;
//  case ChamComplexDouble:
//    err = CHAMELEON_zgetrf_nopiv_Tile( chamA );
//    break;
//  default:
//    COMPOSYX_ASSERT(false, "Chameleon::getrf_nopiv_tile: data type not supported, must be float, double, complex, double complex");
//    err = CHAMELEON_ERR_NOT_SUPPORTED;
//  }
//  CHAMELEON_Pause();
//  return err;
//}

//template<typename Scalar>
//static int getrs_nopiv(cham_trans_t trans, int m, int n, Scalar *A, int lda, Scalar *B, int ldb){
//  CHAMELEON_Resume();
//  int err = 0;
//  if constexpr(is_real<Scalar>::value) {
//    if constexpr(is_precision_double<Scalar>::value) {
//      err = CHAMELEON_dgetrs(trans, m, n, A, lda, B, ldb);
//    } else {
//      err = CHAMELEON_sgetrs(trans, m, n, A, lda, B, ldb);
//    }
//  } else {
//    if constexpr(is_precision_double<Scalar>::value) {
//      err = CHAMELEON_zgetrs(trans, m, n, A, lda, B, ldb);
//    } else {
//      err = CHAMELEON_cgetrs(trans, m, n, A, lda, B, ldb);
//    }
//  }
//  CHAMELEON_Pause();
//  return err;
//}
//static inline int getrs_nopiv_tile( CHAM_desc_t *chamA, CHAM_desc_t* chamX ){
//  CHAMELEON_Resume();
//  int err = 0;
//  switch (chamA->dtyp) {
//  case ChamRealFloat:
//    err = CHAMELEON_sgetrs_nopiv_Tile( chamA, chamX );
//    break;
//  case ChamRealDouble:
//    err = CHAMELEON_dgetrs_nopiv_Tile( chamA, chamX );
//    break;
//  case ChamComplexFloat:
//    err = CHAMELEON_cgetrs_nopiv_Tile( chamA, chamX );
//    break;
//  case ChamComplexDouble:
//    err = CHAMELEON_zgetrs_nopiv_Tile( chamA, chamX );
//    break;
//  default:
//    COMPOSYX_ASSERT(false, "Chameleon::getrs_nopiv_tile: data type not supported, must be float, double, complex, double complex");
//    err = CHAMELEON_ERR_NOT_SUPPORTED;
//    break;
//  }
//  CHAMELEON_Pause();
//  return err;
//}

template <typename Scalar>
static int potrf(cham_uplo_t uplo, int n, Scalar* A, int lda) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dpotrf(uplo, n, A, lda);
    } else {
      err = CHAMELEON_spotrf(uplo, n, A, lda);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zpotrf(uplo, n, A, lda);
    } else {
      err = CHAMELEON_cpotrf(uplo, n, A, lda);
    }
  }
  CHAMELEON_Pause();
  return err;
}
static inline int potrf_tile(cham_uplo_t uplo, CHAM_desc_t* chamA) {
  CHAMELEON_Resume();
  int err = 0;
  switch (chamA->dtyp) {
  case ChamRealFloat:
    err = CHAMELEON_spotrf_Tile(uplo, chamA);
    break;
  case ChamRealDouble:
    err = CHAMELEON_dpotrf_Tile(uplo, chamA);
    break;
  case ChamComplexFloat:
    err = CHAMELEON_cpotrf_Tile(uplo, chamA);
    break;
  case ChamComplexDouble:
    err = CHAMELEON_zpotrf_Tile(uplo, chamA);
    break;
  default:
    COMPOSYX_ASSERT(false, "Chameleon::potrf_tile: data type not supported, "
                           "must be float, double, complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
    break;
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static int potrs(cham_uplo_t uplo, int n, int nrhs, Scalar* A, int lda,
                 Scalar* B, int ldb) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dpotrs(uplo, n, nrhs, A, lda, B, ldb);
    } else {
      err = CHAMELEON_spotrs(uplo, n, nrhs, A, lda, B, ldb);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zpotrs(uplo, n, nrhs, A, lda, B, ldb);
    } else {
      err = CHAMELEON_cpotrs(uplo, n, nrhs, A, lda, B, ldb);
    }
  }
  CHAMELEON_Pause();
  return err;
}
static inline int potrs_tile(cham_uplo_t uplo, CHAM_desc_t* chamA,
                             CHAM_desc_t* chamX) {
  CHAMELEON_Resume();
  int err = 0;
  switch (chamA->dtyp) {
  case ChamRealFloat:
    err = CHAMELEON_spotrs_Tile(uplo, chamA, chamX);
    break;
  case ChamRealDouble:
    err = CHAMELEON_dpotrs_Tile(uplo, chamA, chamX);
    break;
  case ChamComplexFloat:
    err = CHAMELEON_cpotrs_Tile(uplo, chamA, chamX);
    break;
  case ChamComplexDouble:
    err = CHAMELEON_zpotrs_Tile(uplo, chamA, chamX);
    break;
  default:
    COMPOSYX_ASSERT(false, "Chameleon::potrs_tile: data type not supported, "
                           "must be float, double, complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
    break;
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static int sytrf(cham_uplo_t uplo, int n, Scalar* A, int lda) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_complex<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zsytrf(uplo, n, A, lda);
    } else {
      err = CHAMELEON_csytrf(uplo, n, A, lda);
    }
  } else {
    COMPOSYX_ASSERT(false, "Chameleon::sytrf: data type not supported, must be "
                           "complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
  }
  CHAMELEON_Pause();
  return err;
}
static inline int sytrf_tile(cham_uplo_t uplo, CHAM_desc_t* chamA) {
  CHAMELEON_Resume();
  int err = 0;
  switch (chamA->dtyp) {
  case ChamRealFloat:
    COMPOSYX_ASSERT(
        false, "ChameleonSolver::sytrf makes sense only for complex matrix");
    break;
  case ChamRealDouble:
    COMPOSYX_ASSERT(
        false, "ChameleonSolver::sytrf makes sense only for complex matrix");
    break;
  case ChamComplexFloat:
    err = CHAMELEON_csytrf_Tile(uplo, chamA);
    break;
  case ChamComplexDouble:
    err = CHAMELEON_zsytrf_Tile(uplo, chamA);
    break;
  default:
    COMPOSYX_ASSERT(false, "Chameleon::sytrf_tile: data type not supported, "
                           "must be complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
    break;
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static inline int sytrs(cham_uplo_t uplo, int n, int nrhs, Scalar* A, int lda,
                        Scalar* B, int ldb) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_complex<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_zsytrs(uplo, n, nrhs, A, lda, B, ldb);
    } else {
      err = CHAMELEON_csytrs(uplo, n, nrhs, A, lda, B, ldb);
    }
  } else {
    COMPOSYX_ASSERT(false, "Chameleon::sytrs: data type not supported, must be "
                           "complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
  }
  CHAMELEON_Pause();
  return err;
}
static inline int sytrs_tile(cham_uplo_t uplo, CHAM_desc_t* chamA,
                             CHAM_desc_t* chamX) {
  CHAMELEON_Resume();
  int err = 0;
  switch (chamA->dtyp) {
  case ChamRealFloat:
    COMPOSYX_ASSERT(
        false, "ChameleonSolver::sytrs makes sense only for complex matrix");
    break;
  case ChamRealDouble:
    COMPOSYX_ASSERT(
        false, "ChameleonSolver::sytrs makes sense only for complex matrix");
    break;
  case ChamComplexFloat:
    err = CHAMELEON_csytrs_Tile(uplo, chamA, chamX);
    break;
  case ChamComplexDouble:
    err = CHAMELEON_zsytrs_Tile(uplo, chamA, chamX);
    break;
  default:
    COMPOSYX_ASSERT(false, "Chameleon::sytrs_tile: data type not supported, "
                           "must be complex, double complex");
    err = CHAMELEON_ERR_NOT_SUPPORTED;
    break;
  }
  CHAMELEON_Pause();
  return err;
}

template <typename Scalar>
static int trsm(cham_side_t side, cham_uplo_t uplo, cham_trans_t trans,
                cham_diag_t diag, int m, int n, Scalar alpha, Scalar* A,
                int lda, Scalar* B, int ldb) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err =
          CHAMELEON_dtrsm(side, uplo, trans, diag, m, n, alpha, A, lda, B, ldb);
    } else {
      err =
          CHAMELEON_strsm(side, uplo, trans, diag, m, n, alpha, A, lda, B, ldb);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err =
          CHAMELEON_ztrsm(side, uplo, trans, diag, m, n, alpha, A, lda, B, ldb);
    } else {
      err =
          CHAMELEON_ctrsm(side, uplo, trans, diag, m, n, alpha, A, lda, B, ldb);
    }
  }
  CHAMELEON_Pause();
  return err;
}
template <typename Scalar>
static inline int trsm_tile(cham_side_t side, cham_uplo_t uplo,
                            cham_trans_t trans, cham_diag_t diag, Scalar alpha,
                            CHAM_desc_t* chamA, CHAM_desc_t* chamX) {
  CHAMELEON_Resume();
  int err = 0;
  if constexpr (is_real<Scalar>::value) {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_dtrsm_Tile(side, uplo, trans, diag, alpha, chamA, chamX);
    } else {
      err = CHAMELEON_strsm_Tile(side, uplo, trans, diag, alpha, chamA, chamX);
    }
  } else {
    if constexpr (is_precision_double<Scalar>::value) {
      err = CHAMELEON_ztrsm_Tile(side, uplo, trans, diag, alpha, chamA, chamX);
    } else {
      err = CHAMELEON_ctrsm_Tile(side, uplo, trans, diag, alpha, chamA, chamX);
    }
  }
  CHAMELEON_Pause();
  return err;
}
// Factorization:1 ends here

// [[file:../../../org/composyx/utils/Chameleon.org::*Footer][Footer:1]]
} // namespace chameleon
// Footer:1 ends here

// [[file:../../../org/composyx/utils/Chameleon.org::*Footer][Footer:2]]
} // namespace composyx
// Footer:2 ends here
