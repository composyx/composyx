// [[file:../../../org/composyx/utils/SZ_compressor.org::*Header][Header:1]]
#pragma once

#include <sz.h>
#include "Arithmetic.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/utils/SZ_compressor.org::*Compression modes][Compression modes:1]]
enum class SZ_CompressionMode : int { POINTWISE, NORMWISE };
// Compression modes:1 ends here

// [[file:../../../org/composyx/utils/SZ_compressor.org::*Initialize / finalize][Initialize / finalize:1]]
inline void SZ_compressor_init() { SZ_Init(NULL); }
inline void SZ_compressor_finalize() { SZ_Finalize(); }
// Initialize / finalize:1 ends here

// [[file:../../../org/composyx/utils/SZ_compressor.org::*SZ compressor class][SZ compressor class:1]]
template <CPX_Scalar Scalar, SZ_CompressionMode ComprMode> class SZ_compressor {
private:
  size_t _n_elts = 0;
  size_t _compressed_bytes = 0;

  // SZ allocates with malloc, we need to deallocate with free
  struct _Deleter_with_free {
    void operator()(unsigned char* ptr) { free(ptr); }
  };
  std::unique_ptr<unsigned char, _Deleter_with_free> _compressed;

public:
  using scalar_type = Scalar;
  using value_type = Scalar;
  using compression_param_type = double;
  constexpr static const SZ_CompressionMode compression_mode = ComprMode;

  SZ_compressor() {}

  SZ_compressor(std::span<const Scalar> data, double zeta) {
    compress(data, zeta);
  }
  SZ_compressor(Scalar* data, int size, double zeta) {
    _compress(data, size, zeta);
  }

  // Copy is not OK
  SZ_compressor(const SZ_compressor&) = delete;
  SZ_compressor& operator=(const SZ_compressor&) = delete;

  // Move is OK
private:
  void _move(SZ_compressor&& other) {
    _n_elts = std::exchange(other._n_elts, 0);
    _compressed_bytes = std::exchange(other._compressed_bytes, 0);
    _compressed = std::move(other._compressed);
  }

public:
  SZ_compressor(SZ_compressor&& other) { _move(std::move(other)); }

  SZ_compressor& operator=(SZ_compressor&& other) {
    _move(std::move(other));
    return *this;
  }

private:
  [[nodiscard]] size_t _n_reals() const {
    constexpr const size_t real_per_scalar = is_complex<Scalar>::value ? 2 : 1;
    return _n_elts * real_per_scalar;
  }

  [[nodiscard]] static constexpr int _get_datatype() {
    if constexpr (is_precision_double<Scalar>::value) {
      return SZ_DOUBLE;
    } else {
      return SZ_FLOAT;
    }
  }

  bool _too_small_to_compress() const {
    return _n_elts * sizeof(Scalar) <= 1024;
  }

  void _compress(const Scalar* data, int size, double zeta) {
    _n_elts = size;
    unsigned char* ptr = nullptr;

    constexpr const int mode =
        (ComprMode == SZ_CompressionMode::POINTWISE) ? PW_REL : NORM;

    if (_too_small_to_compress()) {
      _compressed_bytes = _n_elts * sizeof(Scalar);
      ptr = (unsigned char*)malloc(_compressed_bytes);
      std::memcpy(ptr, data, _compressed_bytes);
    } else {
      ptr = SZ_compress_args(_get_datatype(), (void*)data, &_compressed_bytes,
                             mode, zeta, zeta, zeta, 0, 0, 0, 0, _n_reals());
    }

    _compressed.reset(ptr);
  }

public:
  void compress(std::span<const Scalar> values, double zeta) {
    _compress(values.data(), values.size(), zeta);
  }

  void compress(const Scalar* data, size_t size, double zeta) {
    _compress(data, size, zeta);
  }

  void decompress(Scalar* ptr) const {
    if (_too_small_to_compress()) {
      std::memcpy(ptr, _compressed.get(), _compressed_bytes);
    } else {
      SZ_decompress_args(_get_datatype(), _compressed.get(), _compressed_bytes,
                         ptr, 0, 0, 0, 0, _n_reals());
    }
  }

  std::vector<Scalar> decompress() const {
    std::vector<Scalar> out_vector(_n_elts);
    decompress(out_vector.data());
    return out_vector;
  }

  void decompress_and_free(Scalar* ptr) {
    decompress(ptr);
    _compressed.reset();
  }

  std::vector<Scalar> decompress_and_free() {
    std::vector<Scalar> out_vector(_n_elts);
    decompress_and_free(out_vector.data());
    return out_vector;
  }

  [[nodiscard]] double get_ratio() const {
    if (_compressed_bytes == 0)
      return 0;

    const int input_bytes = sizeof(Scalar) * _n_elts;
    return static_cast<double>(input_bytes) /
           static_cast<double>(_compressed_bytes);
  }

  [[nodiscard]] size_t get_compressed_bytes() const {
    return _compressed_bytes;
  }

  [[nodiscard]] size_t get_n_elts() const { return _n_elts; }
}; // class SZ_compressor
} // namespace composyx
// SZ compressor class:1 ends here
