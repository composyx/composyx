// [[file:../../../org/composyx/utils/ZFP_compressor.org::*Header][Header:1]]
#pragma once

#include <zfp.hpp>
#include <zfp/array1.hpp>

#include "Arithmetic.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/utils/ZFP_compressor.org::*Compression modes][Compression modes:1]]
enum class ZFP_CompressionMode : int { ACCURACY, PRECISION, RATE };
// Compression modes:1 ends here

// [[file:../../../org/composyx/utils/ZFP_compressor.org::*ZFP compressor class][ZFP compressor class:1]]
template <CPX_Scalar Scalar, ZFP_CompressionMode ComprMode>
class ZFP_compressor {
private:
  zfp_stream* _stream = nullptr;
  bitstream* _bit_stream = nullptr;
  std::vector<char> _compressed;
  size_t _n_elts = 0;
  size_t _compressed_bytes = 0;

  using Real = typename arithmetic_real<Scalar>::type;
  // Compression parameter is an integer for ZFP_PRECISION or ZFP_RATE, otherwise double
  using ComprParam =
      std::conditional_t<ComprMode == ZFP_CompressionMode::PRECISION ||
                             ComprMode == ZFP_CompressionMode::RATE,
                         unsigned int, double>;

public:
  using Scalar_type = Scalar;
  using value_type = Scalar;
  using compression_param_type = ComprParam;
  constexpr static const ZFP_CompressionMode compression_mode = ComprMode;

  ZFP_compressor() { _stream = zfp_stream_open(NULL); }

  ZFP_compressor(std::span<const Scalar> data, ComprParam param) {
    _stream = zfp_stream_open(NULL);
    compress(data, param);
  }

  ZFP_compressor(Scalar* data, int size, ComprParam param) {
    _stream = zfp_stream_open(NULL);
    std::span<Scalar> spdata(data, data + size);
    compress(spdata, param);
  }

  // Avoid implicit conversion (double/int)
  template <typename T> ZFP_compressor(std::span<const Scalar>, T) = delete;
  template <typename T> ZFP_compressor(Scalar*, int, T) = delete;

  ~ZFP_compressor() {
    if (_bit_stream)
      stream_close(_bit_stream);
    if (_stream)
      zfp_stream_close(_stream);
  }

  // Copy is not OK
  ZFP_compressor(const ZFP_compressor&) = delete;
  ZFP_compressor& operator=(const ZFP_compressor&) = delete;

  // Move is OK
private:
  void _move(ZFP_compressor&& other) {
    if (_stream)
      zfp_stream_close(_stream);
    _stream = std::exchange(other._stream, nullptr);
    if (_bit_stream)
      stream_close(_bit_stream);
    _bit_stream = std::exchange(other._bit_stream, nullptr);
    _n_elts = std::exchange(other._n_elts, 0);
    _compressed_bytes = std::exchange(other._compressed_bytes, 0);
    _compressed = std::move(other._compressed);
  }

public:
  ZFP_compressor(ZFP_compressor&& other) { _move(std::move(other)); }

  ZFP_compressor& operator=(ZFP_compressor&& other) {
    _move(std::move(other));
    return *this;
  }

private:
  [[nodiscard]] size_t _n_reals() const {
    constexpr const size_t real_per_scalar = is_complex<Scalar>::value ? 2 : 1;
    return _n_elts * real_per_scalar;
  }

  [[nodiscard]] static constexpr zfp_type _get_datatype() {
    if constexpr (is_precision_double<Scalar>::value) {
      return zfp_type_double;
    } else {
      return zfp_type_float;
    }
  }
  // ZFP compressor class:1 ends here

  // [[file:../../../org/composyx/utils/ZFP_compressor.org::*Fixed-accuracy compression][Fixed-accuracy compression:1]]
  double _set_accuracy(double tolerance) {
    return zfp_stream_set_accuracy(_stream, tolerance);
  }
  // Fixed-accuracy compression:1 ends here

  // [[file:../../../org/composyx/utils/ZFP_compressor.org::*Fixed-precision compression][Fixed-precision compression:1]]
  unsigned int _set_precision(unsigned int precision) {
    return zfp_stream_set_precision(_stream, precision);
  }
  // Fixed-precision compression:1 ends here

  // [[file:../../../org/composyx/utils/ZFP_compressor.org::*Fixed-rate compression][Fixed-rate compression:1]]
  double _set_rate(unsigned int rate) {
    return zfp_stream_set_rate(_stream, rate, _get_datatype(), 1, zfp_false);
  }
  // Fixed-rate compression:1 ends here

  // [[file:../../../org/composyx/utils/ZFP_compressor.org::*Compression][Compression:1]]
private:
  void _compress(const Scalar* data, size_t size, ComprParam param) {
    _n_elts = size;

    if constexpr (ComprMode == ZFP_CompressionMode::ACCURACY) {
      _set_accuracy(param);
    } else if constexpr (ComprMode == ZFP_CompressionMode::PRECISION) {
      _set_precision(param);
    } else if constexpr (ComprMode == ZFP_CompressionMode::RATE) {
      _set_rate(param);
    }

    zfp_field* field = zfp_field_1d((void*)data, _get_datatype(), _n_reals());

    // Get a maximal size for the buffer
    size_t max_bufsize = zfp_stream_maximum_size(_stream, field);

    // Allocate buffer and attach a "bitstream" to it
    _compressed = std::vector<char>(max_bufsize);
    _bit_stream = stream_open(_compressed.data(), max_bufsize);
    zfp_stream_set_bit_stream(_stream, _bit_stream);

    // Write metadata in the header of the bitsteam (return size of the header)
    _compressed_bytes = zfp_write_header(_stream, field, ZFP_HEADER_MAGIC);

    // rewind stream to beginning
    zfp_stream_rewind(_stream);
    // return value is byte size of compressed stream
    _compressed_bytes += zfp_compress(_stream, field);
    _compressed.resize(_compressed_bytes);

    zfp_field_free(field);
  }

public:
  void compress(std::span<const Scalar> values, ComprParam param) {
    _compress(values.data(), values.size(), param);
  }

  template <typename T> void compress(std::span<const Scalar>, T) = delete;
  // Compression:1 ends here

  // [[file:../../../org/composyx/utils/ZFP_compressor.org::*Decompress][Decompress:1]]
  void decompress(Scalar* ptr) const {
    zfp_field* re_field = zfp_field_1d(ptr, _get_datatype(), _n_reals());

    // read header
    zfp_read_header(_stream, re_field, ZFP_HEADER_MAGIC); // read metadata

    zfp_stream_rewind(_stream); // rewind stream to beginning
    zfp_decompress(_stream, re_field);

    zfp_field_free(re_field);
  }

  std::vector<Scalar> decompress() const {
    std::vector<Scalar> out_vector(_n_elts);
    decompress(out_vector.data());
    return out_vector;
  }

  void decompress_and_free(Scalar* ptr) {
    decompress(ptr);
    _compressed.clear();
    _n_elts = 0;
  }

  std::vector<Scalar> decompress_and_free() {
    std::vector<Scalar> out_vector(_n_elts);
    decompress_and_free(out_vector.data());
    return out_vector;
  }
  // Decompress:1 ends here

  // [[file:../../../org/composyx/utils/ZFP_compressor.org::*Getters][Getters:1]]
  [[nodiscard]] double get_ratio() const {
    if (_compressed_bytes == 0) {
      return 0;
    }

    const double input_bytes = static_cast<double>(sizeof(Scalar) * _n_elts);
    return (input_bytes / static_cast<double>(_compressed_bytes));
  }

  [[nodiscard]] size_t get_compressed_bytes() const {
    return _compressed_bytes;
  }

  [[nodiscard]] size_t get_n_elts() const { return _n_elts; }
}; // class ZFP_compressor
} // namespace composyx
// Getters:1 ends here
