// [[file:../../../org/composyx/utils/FortranArray.org::*Header][Header:1]]
#pragma once

#include <vector>
#include <stdexcept>
#include "Error.hpp"
#include "composyx/interfaces/basic_concepts.hpp"
#include "composyx/interfaces/Common.hpp"
// Header:1 ends here

// [[file:../../../org/composyx/utils/FortranArray.org::*Attributes][Attributes:1]]
namespace composyx {
template <typename T> class FortranArray {
private:
  size_t _size;
  T* _data;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/utils/FortranArray.org::*Constructors][Constructors:1]]
public:
  FortranArray() : _size{0}, _data{nullptr} {}
  FortranArray(size_t s, T* d) : _size{s}, _data{d} {}

  void set_data(size_t s, T* d) {
    _size = s;
    _data = d;
  }

  // No copy because using weak pointer
  FortranArray(const FortranArray&) = delete;
  FortranArray& operator=(const FortranArray& other) = delete;

  // Move allowed to take the pointer
  FortranArray(FortranArray&& other) {
    _size = std::exchange(other._size, 0);
    _data = std::exchange(other._data, nullptr);
  }
  FortranArray& operator=(FortranArray&& other) {
    if (&other == this)
      return *this;
    _size = std::exchange(other._size, 0);
    _data = std::exchange(other._data, nullptr);
    return *this;
  }
  // Constructors:1 ends here

  // [[file:../../../org/composyx/utils/FortranArray.org::*Indexing][Indexing:1]]
  // [] operator, no boundary checking
  template <CPX_Integral Tint> T& operator[](Tint idx) {
    return _data[idx - 1];
  }
  template <CPX_Integral Tint> const T& operator[](Tint idx) const {
    return _data[idx - 1];
  }
  // Indexing:1 ends here

  // [[file:../../../org/composyx/utils/FortranArray.org::*Indexing][Indexing:2]]
  // () operator, with boundary checking
  template <CPX_Integral Tint> T& operator()(Tint idx) {
    if (idx <= Tint{0}) {
      throw(std::out_of_range("Fortran array with index <= 0"));
    }
    if (idx > static_cast<Tint>(_size)) {
      throw(std::out_of_range("Fortran array with index > size"));
    }
    return _data[idx - 1];
  }
  template <CPX_Integral Tint> const T& operator()(Tint idx) const {
    if (idx <= Tint{0}) {
      throw(std::out_of_range("Fortran array with index <= 0"));
    }
    if (idx > static_cast<Tint>(_size)) {
      throw(std::out_of_range("Fortran array with index > size"));
    }
    return _data[idx - 1];
  }
  // Indexing:2 ends here

  // [[file:../../../org/composyx/utils/FortranArray.org::*Footer][Footer:1]]
}; // FortranArray
} // namespace composyx
// Footer:1 ends here
