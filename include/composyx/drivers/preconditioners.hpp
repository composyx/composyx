#pragma once

#include <composyx.hpp>
#include <composyx/precond/DiagonalPrecond.hpp>
#include <composyx/precond/AbstractSchwarz.hpp>
#include <composyx/precond/TwoLevelAbstractSchwarz.hpp>
#include <composyx/solver/BlasSolver.hpp>
#include <composyx/part_data/PartMatrix.hpp>
#if defined(COMPOSYX_USE_FABULOUS)
#include <composyx/precond/EigenDeflatedPcd.hpp>
#endif
#include <type_traits>

namespace composyx {

/*
 * Jan - 22: for some reason the conditional does not work with c++20
 * concepts...
 */

template <CPX_Matrix LocMatrix, CPX_Vector LocVector, typename SparseSolver>
auto mph_get_solver_type() {
#ifdef __cpp_concepts
  if constexpr (is_dense<LocMatrix>::value) {
    return BlasSolver<LocMatrix, LocVector>();
  } else {
    return SparseSolver();
  }
#else
  using Solver = typename std::conditional_t<is_dense<LocMatrix>::value,
                                             BlasSolver<LocMatrix, LocVector>,
                                             SparseSolver>;
  return Solver();
#endif //__cpp_concepts
}

#define CPX_ONE_LEVEL_AS(CLASS_NAME)                                           \
  using LocMatrix = typename Matrix::local_data;                               \
  using LocVector = typename Vector::local_data;                               \
  using Solver =                                                               \
      decltype(mph_get_solver_type<LocMatrix, LocVector, SparseSolver>());     \
  using Precond = CLASS_NAME<Matrix, Vector, Solver>;                          \
  return Precond();

#define CPX_TWO_LEVEL_AS_GENEO(CLASS_NAME)                                     \
  using LocMatrix = typename Matrix::local_data;                               \
  using LocVector = typename Vector::local_data;                               \
  using Solver =                                                               \
      decltype(mph_get_solver_type<LocMatrix, LocVector, SparseSolver>());     \
  using GlobSolver = CentralizedSolver<Matrix, Vector, Solver>;                \
  using M0_type = CoarseSolve<LocMatrix, LocVector, GlobSolver>;               \
  using M1_type = CLASS_NAME<Matrix, Vector, Solver>;                          \
  return TwoLevelAbstractSchwarz<Matrix, Vector, M0_type, M1_type, deflated>();

#define CPX_EIG_DEFL_AS(CLASS_NAME)                                            \
  using LocMatrix = typename Matrix::local_data;                               \
  using LocVector = typename Vector::local_data;                               \
  using Solver =                                                               \
      decltype(mph_get_solver_type<LocMatrix, LocVector, SparseSolver>());     \
  using M0_type = EigenDeflatedPcd<Matrix, Vector>;                            \
  using M1_type = CLASS_NAME<Matrix, Vector, Solver>;                          \
  return TwoLevelAbstractSchwarz<Matrix, Vector, M0_type, M1_type, true>();

template <CPX_Matrix Matrix, CPX_Vector Vector> auto precond_diagonal() {
  return DiagonalPrecond<Matrix, Vector>();
}

template <CPX_Matrix Matrix, CPX_Vector Vector, typename SparseSolver>
auto precond_additive_schwarz() {
  CPX_ONE_LEVEL_AS(AdditiveSchwarz)
}

template <CPX_Matrix Matrix, CPX_Vector Vector, typename SparseSolver>
auto precond_neumann_neumann() {
  CPX_ONE_LEVEL_AS(NeumannNeumann)
}

template <CPX_Matrix Matrix, CPX_Vector Vector, typename SparseSolver>
auto precond_robin_robin() {
  CPX_ONE_LEVEL_AS(RobinRobin)
}

template <CPX_Matrix Matrix, CPX_Vector Vector, typename SparseSolver,
          bool deflated = true>
auto precond_geneo_AS() {
  CPX_TWO_LEVEL_AS_GENEO(AdditiveSchwarz)
}

template <CPX_Matrix Matrix, CPX_Vector Vector, typename SparseSolver,
          bool deflated = true>
auto precond_geneo_NN() {
  CPX_TWO_LEVEL_AS_GENEO(NeumannNeumann)
}

template <CPX_Matrix Matrix, CPX_Vector Vector, typename SparseSolver,
          bool deflated = true>
auto precond_geneo_RR() {
  CPX_TWO_LEVEL_AS_GENEO(RobinRobin)
}

#if defined(COMPOSYX_USE_FABULOUS)
template <CPX_Matrix Matrix, CPX_Vector Vector, typename SparseSolver>
auto precond_defl_AS() {
  CPX_EIG_DEFL_AS(AdditiveSchwarz)
}

template <CPX_Matrix Matrix, CPX_Vector Vector, typename SparseSolver>
auto precond_defl_NN() {
  CPX_EIG_DEFL_AS(NeumannNeumann)
}

template <CPX_Matrix Matrix, CPX_Vector Vector, typename SparseSolver>
auto precond_defl_RR() {
  CPX_EIG_DEFL_AS(RobinRobin)
}
#endif

} // namespace composyx
