// [[file:../../../org/composyx/kernel/spkernel.org::*Header][Header:1]]
#pragma once

#include "composyx/kernel/sparse/composyx_sparse.hpp"

#if defined(COMPOSYX_USE_MKL_SPBLAS) && defined(COMPOSYX_USE_RSB_SPBLAS)
#error "Can not use mkl or librsb at the same time"
#endif

#if defined(COMPOSYX_USE_MKL_SPBLAS)
#include "composyx/kernel/sparse/mkl_sparse.hpp"
#endif

#if defined(COMPOSYX_USE_RSB_SPBLAS)
#include "composyx/kernel/sparse/rsb_sparse.hpp"
#endif

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/kernel/spkernel.org::*Information display][Information display:1]]
template <CPX_SparseMatrix SpMat>
inline void info_implicit_sparse_kernels(std::ostream& sout = std::cout) {

#if defined(COMPOSYX_USE_RSB_SPBLAS)
  if constexpr (std::is_same_v<typename SpMat::index_type, int>) {
    sout << "Sparse kernels used: RSB\n";
    return;
  }
#endif

#if defined(COMPOSYX_USE_MKL_SPBLAS)
  if constexpr (std::is_same_v<typename SpMat::index_type, int>) {
    sout << "Sparse kernels used: MKL\n";
    return;
  }
#endif

  sout << "Sparse kernels used: COMPOSYX\n";
}
// Information display:1 ends here

// [[file:../../../org/composyx/kernel/spkernel.org::*Multiplication operators][Multiplication operators:1]]
namespace sparse_kernel_impl {
template <Op opA, CPX_SparseMatrixIJV SpMat, CPX_DenseBlasMatrix DM,
          CPX_Scalar Scalar = DM::value_type>
inline void spmv(const SpMat& sp_mat, const DM& dm_in, DM& dm_out,
                 Scalar alpha = Scalar{1}, Scalar beta = Scalar{0}) {

#if defined(COMPOSYX_USE_RSB_SPBLAS)
  if constexpr (std::is_same_v<typename SpMat::index_type, int>) {
    rsb_sparse_matrix_dense_matrix_product<opA>(sp_mat, dm_in, dm_out, alpha,
                                                beta);
    return;
  }
#endif

#if defined(COMPOSYX_USE_MKL_SPBLAS)
  if constexpr (std::is_same_v<typename SpMat::index_type, int>) {
    mkl_sparse_matrix_dense_matrix_product<opA>(sp_mat, dm_in, dm_out, alpha,
                                                beta);
    return;
  }
#endif

  sparse_matrix_dense_matrix_product<opA>(sp_mat, dm_in, dm_out, alpha, beta);
}
} // namespace sparse_kernel_impl
// Multiplication operators:1 ends here

// [[file:../../../org/composyx/kernel/spkernel.org::*COO matrix multiplication][COO matrix multiplication:1]]
template <CPX_Scalar Scalar, CPX_Integral Index, CPX_DenseBlasMatrix DM, Op opA>
inline SpCOOMV<Scalar, Index, opA, DM>
operator*(const OpSpMatCOO<Scalar, Index, opA>& sp_mat, const DM& dm_in) {
  return SpCOOMV<Scalar, Index, opA, DM>(sp_mat.mat, dm_in);
}

template <CPX_Scalar Scalar, CPX_Integral Index, Op opA, typename Rhs>
inline Rhs SpCOOMV<Scalar, Index, opA, Rhs>::eval() const {
  debug_log_ET::log("SpCOOMV eval");
  const size_t M =
      (opA == Op::NoTrans) ? sp_mat.get_n_rows() : sp_mat.get_n_cols();
  const size_t N = rhs.get_n_cols();
  Rhs out(M, N);
  sparse_kernel_impl::spmv<opA>(sp_mat, rhs, out);
  return out;
}

template <CPX_Scalar Scalar, int NbCol>
template <CPX_Integral Index, Op opA>
inline DenseMatrix<Scalar, NbCol>& DenseMatrix<Scalar, NbCol>::operator=(
    const SpCOOMV<Scalar, Index, opA, DenseMatrix<Scalar, NbCol>>& spmv_coo) {
  debug_log_ET::log("DenseMatrix = SpCOOMV");
  const SparseMatrixCOO<Scalar, Index>& A = spmv_coo.sp_mat;
  const size_t M = (opA == Op::NoTrans) ? n_rows(A) : n_cols(A);
  const size_t N = n_cols(spmv_coo.rhs);

  // Special case: y = A * y, need a copy
  if (this == &(spmv_coo.rhs)) {
    const DenseMatrix dm_in(*this);
    if (M != this->get_n_rows()) {
      (*this) = DenseMatrix(M, N);
    }
    sparse_kernel_impl::spmv<opA>(A, dm_in, *this);
  } else {
    if (M != this->get_n_rows() or N != this->get_n_cols()) {
      (*this) = DenseMatrix(M, N);
    }
    sparse_kernel_impl::spmv<opA>(A, spmv_coo.rhs, *this);
  }
  return *this;
}

template <CPX_Scalar Scalar, CPX_Integral Index, CPX_DenseBlasMatrix DM>
inline SpCOOMV<Scalar, Index, Op::NoTrans, DM>
operator*(const SparseMatrixCOO<Scalar, Index>& sp_mat, const DM& dm_in) {
  return SpCOOMV<Scalar, Index, Op::NoTrans, DM>(sp_mat, dm_in);
}
// COO matrix multiplication:1 ends here

// [[file:../../../org/composyx/kernel/spkernel.org::*CSC matrix multiplication][CSC matrix multiplication:1]]
template <CPX_Scalar Scalar, CPX_Integral Index, CPX_DenseBlasMatrix DM, Op opA>
inline SpCSCMV<Scalar, Index, opA, DM>
operator*(const OpSpMatCSC<Scalar, Index, opA>& sp_mat, const DM& dm_in) {
  return SpCSCMV<Scalar, Index, opA, DM>(sp_mat.mat, dm_in);
}

template <CPX_Scalar Scalar, CPX_Integral Index, Op opA, typename Rhs>
inline Rhs SpCSCMV<Scalar, Index, opA, Rhs>::eval() const {
  debug_log_ET::log("SpCSCMV eval");
  const size_t M =
      (opA == Op::NoTrans) ? sp_mat.get_n_rows() : sp_mat.get_n_cols();
  const size_t N = rhs.get_n_cols();
  Rhs out(M, N);
  sparse_kernel_impl::spmv<opA>(sp_mat, rhs, out);
  return out;
}

template <CPX_Scalar Scalar, int NbCol>
template <CPX_Integral Index, Op opA>
inline DenseMatrix<Scalar, NbCol>& DenseMatrix<Scalar, NbCol>::operator=(
    const SpCSCMV<Scalar, Index, opA, DenseMatrix<Scalar, NbCol>>& spmv_coo) {
  debug_log_ET::log("DenseMatrix = SpCSCMV");
  const SparseMatrixCSC<Scalar, Index>& A = spmv_coo.sp_mat;
  const size_t M = (opA == Op::NoTrans) ? n_rows(A) : n_cols(A);
  const size_t N = n_cols(spmv_coo.rhs);

  // Special case: y = A * y, need a copy
  if (this == &(spmv_coo.rhs)) {
    const DenseMatrix dm_in(*this);
    if (M != this->get_n_rows()) {
      (*this) = DenseMatrix(M, N);
    }
    sparse_kernel_impl::spmv<opA>(A, dm_in, *this);
  } else {
    if (M != this->get_n_rows() or N != this->get_n_cols()) {
      (*this) = DenseMatrix(M, N);
    }
    sparse_kernel_impl::spmv<opA>(A, spmv_coo.rhs, *this);
  }
  return *this;
}

template <CPX_Scalar Scalar, CPX_Integral Index, CPX_DenseBlasMatrix DM>
inline SpCSCMV<Scalar, Index, Op::NoTrans, DM>
operator*(const SparseMatrixCSC<Scalar, Index>& sp_mat, const DM& dm_in) {
  return SpCSCMV<Scalar, Index, Op::NoTrans, DM>(sp_mat, dm_in);
}
// CSC matrix multiplication:1 ends here

// [[file:../../../org/composyx/kernel/spkernel.org::*CSR][CSR:1]]
template <CPX_Scalar Scalar, CPX_Integral Index, CPX_DenseBlasMatrix DM, Op opA>
inline SpCSRMV<Scalar, Index, opA, DM>
operator*(const OpSpMatCSR<Scalar, Index, opA>& sp_mat, const DM& dm_in) {
  return SpCSRMV<Scalar, Index, opA, DM>(sp_mat.mat, dm_in);
}

template <CPX_Scalar Scalar, CPX_Integral Index, Op opA, typename Rhs>
inline Rhs SpCSRMV<Scalar, Index, opA, Rhs>::eval() const {
  debug_log_ET::log("SpCSRMV eval");
  const size_t M =
      (opA == Op::NoTrans) ? sp_mat.get_n_rows() : sp_mat.get_n_cols();
  const size_t N = rhs.get_n_cols();
  Rhs out(M, N);
  sparse_kernel_impl::spmv<opA>(sp_mat, rhs, out);
  return out;
}

template <CPX_Scalar Scalar, int NbCol>
template <CPX_Integral Index, Op opA>
inline DenseMatrix<Scalar, NbCol>& DenseMatrix<Scalar, NbCol>::operator=(
    const SpCSRMV<Scalar, Index, opA, DenseMatrix<Scalar, NbCol>>& spmv_coo) {
  debug_log_ET::log("DenseMatrix = SpCSRMV");
  const SparseMatrixCSR<Scalar, Index>& A = spmv_coo.sp_mat;
  const size_t M = (opA == Op::NoTrans) ? n_rows(A) : n_cols(A);
  const size_t N = n_cols(spmv_coo.rhs);

  // Special case: y = A * y, need a copy
  if (this == &(spmv_coo.rhs)) {
    const DenseMatrix dm_in(*this);
    if (M != this->get_n_rows()) {
      (*this) = DenseMatrix(M, N);
    }
    sparse_kernel_impl::spmv<opA>(A, dm_in, *this);
  } else {
    if (M != this->get_n_rows() or N != this->get_n_cols()) {
      (*this) = DenseMatrix(M, N);
    }
    sparse_kernel_impl::spmv<opA>(A, spmv_coo.rhs, *this);
  }
  return *this;
}

template <CPX_Scalar Scalar, CPX_Integral Index, CPX_DenseBlasMatrix DM>
inline SpCSRMV<Scalar, Index, Op::NoTrans, DM>
operator*(const SparseMatrixCSR<Scalar, Index>& sp_mat, const DM& dm_in) {
  return SpCSRMV<Scalar, Index, Op::NoTrans, DM>(sp_mat, dm_in);
}
// CSR:1 ends here

// [[file:../../../org/composyx/kernel/spkernel.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
