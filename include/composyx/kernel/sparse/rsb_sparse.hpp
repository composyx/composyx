// [[file:../../../../org/composyx/include/composyx/kernel/sparse/rsb_sparse.hpp :comments link][No heading:1]]
#pragma once

#include "composyx/loc_data/SparseMatrixCOO.hpp"

#include <rsb.h>
#include <blas_sparse.h>

namespace composyx {

template <typename Scalar> struct rsb_spblas : public std::false_type {};

template <> struct rsb_spblas<float> {
  template <typename... Args> static auto start_create(Args... args) {
    return BLAS_suscr_begin(args...);
  }
  template <typename... Args> static auto insert_data(Args... args) {
    return BLAS_suscr_insert_entries(args...);
  }
  template <typename... Args> static auto end_create(Args... args) {
    return BLAS_suscr_end(args...);
  }
  template <typename... Args> static auto matvect(Args... args) {
    return BLAS_susmv(args...);
  }
  template <typename... Args> static auto matmat(Args... args) {
    return BLAS_susmm(args...);
  }
  using valtype = float;
};
template <> struct rsb_spblas<double> {
  template <typename... Args> static auto start_create(Args... args) {
    return BLAS_duscr_begin(args...);
  }
  template <typename... Args> static auto insert_data(Args... args) {
    return BLAS_duscr_insert_entries(args...);
  }
  template <typename... Args> static auto end_create(Args... args) {
    return BLAS_duscr_end(args...);
  }
  template <typename... Args> static auto matvect(Args... args) {
    return BLAS_dusmv(args...);
  }
  template <typename... Args> static auto matmat(Args... args) {
    return BLAS_dusmm(args...);
  }
  using valtype = double;
};
template <> struct rsb_spblas<std::complex<float>> {
  template <typename... Args> static auto start_create(Args... args) {
    return BLAS_cuscr_begin(args...);
  }
  template <typename... Args> static auto insert_data(Args... args) {
    return BLAS_cuscr_insert_entries(args...);
  }
  template <typename... Args> static auto end_create(Args... args) {
    return BLAS_cuscr_end(args...);
  }
  template <typename... Args>
  static auto matvect(enum blas_trans_type transA,
                      const std::complex<float> alpha, blas_sparse_matrix A,
                      const std::complex<float>* x, int incx,
                      std::complex<float>* y, int incy) {
    return BLAS_cusmv(transA, (const void*)&alpha, A, (const void*)x, incx,
                      (void*)y, incy);
  }
  template <typename... Args>
  static auto matmat(enum blas_order_type order, enum blas_trans_type transA,
                     int nrhs, const std::complex<float> alpha,
                     blas_sparse_matrix A, const std::complex<float>* b,
                     int ldb, std::complex<float>* c, int ldc) {
    return BLAS_cusmm(order, transA, nrhs, (const void*)&alpha, A,
                      (const void*)b, ldb, (void*)c, ldc);
  }
  using valtype = std::complex<float>;
};
template <> struct rsb_spblas<std::complex<double>> {
  template <typename... Args> static auto start_create(Args... args) {
    return BLAS_zuscr_begin(args...);
  }
  template <typename... Args> static auto insert_data(Args... args) {
    return BLAS_zuscr_insert_entries(args...);
  }
  template <typename... Args> static auto end_create(Args... args) {
    return BLAS_zuscr_end(args...);
  }
  template <typename... Args>
  static auto matvect(enum blas_trans_type transA,
                      const std::complex<double> alpha, blas_sparse_matrix A,
                      const std::complex<double>* x, int incx,
                      std::complex<double>* y, int incy) {
    return BLAS_zusmv(transA, (const void*)&alpha, A, (const void*)x, incx,
                      (void*)y, incy);
  }
  template <typename... Args>
  static auto matmat(enum blas_order_type order, enum blas_trans_type transA,
                     int nrhs, const std::complex<double> alpha,
                     blas_sparse_matrix A, const std::complex<double>* b,
                     int ldb, std::complex<double>* c, int ldc) {
    return BLAS_zusmm(order, transA, nrhs, (const void*)&alpha, A,
                      (const void*)b, ldb, (void*)c, ldc);
  }
  using valtype = std::complex<double>;
};

template <typename Matrix>
void describe_matrix(const Matrix& mat, blas_sparse_matrix& A) {
  int err = 0;
  if (mat.is_symmetric()) {
    if (mat.is_storage_upper()) {
      //err += BLAS_ussp(A, blas_upper);
      err += BLAS_ussp(A, blas_upper_symmetric);
    } else if (mat.is_storage_lower()) {
      //err += BLAS_ussp(A, blas_lower);
      err += BLAS_ussp(A, blas_lower_symmetric);
    }
  } else if (mat.is_hermitian()) {
    if (mat.is_storage_upper()) {
      //BLAS_ussp(A, blas_upper);
      err += BLAS_ussp(A, blas_upper_hermitian);
    } else if (mat.is_storage_lower()) {
      //BLAS_ussp(A, blas_lower);
      err += BLAS_ussp(A, blas_lower_hermitian);
    }
  }

  err += BLAS_ussp(A, blas_zero_base);
  err += BLAS_ussp(A, blas_non_unit_diag);
  COMPOSYX_ASSERT(err == 0, "Error in librsb set properties");
}

template <Op opA> constexpr inline auto rsb_trans_type() {
  if constexpr (opA == Op::NoTrans)
    return blas_no_trans;
  if constexpr (opA == Op::Trans)
    return blas_trans;
  if constexpr (opA == Op::ConjTrans)
    return blas_conj_trans;
}

template <Op opA, class SPMat, class DM,
          typename Scalar = SPMat::scalar_type> // SPMat is CSC or COO
inline void rsb_sparse_matrix_dense_matrix_product(const SPMat& sp_mat,
                                                   const DM& dm_in, DM& dm_out,
                                                   Scalar alpha, Scalar beta) {
  using Index = typename SPMat::index_type;
  using RSBSP = rsb_spblas<Scalar>;
  using RSBSCALAR = typename RSBSP::valtype;

  int m = n_rows(sp_mat);
  int n = n_cols(sp_mat);
  int nnz = sp_mat.get_nnz();
  const Index* i_ptr = sp_mat.get_i_ptr();
  const Index* j_ptr = sp_mat.get_j_ptr();
  const RSBSCALAR* v_ptr = (const RSBSCALAR*)sp_mat.get_v_ptr();

  blas_sparse_matrix A = RSBSP::start_create(m, n);
  COMPOSYX_ASSERT(A != blas_invalid_handle, "Error in librsb matrix creation");
  BLAS_ussp(A, blas_rsb_rep_coo);
  /*if constexpr(std::is_same_v<SPMat, SparseMatrixCOO<Scalar, Index>>){
	BLAS_ussp(A, blas_rsb_rep_coo);
	}
	else if(std::is_same_v<SPMat, SparseMatrixCSC<Scalar, Index>>){
	COMPOSYX_ASSERT(false, "CSC format not supported by librsb");
	}*/
  describe_matrix(sp_mat, A);

  int err = RSBSP::insert_data(A, nnz, v_ptr, i_ptr, j_ptr);
  COMPOSYX_ASSERT(err == 0, "Error in librsb insert data");
  COMPOSYX_ASSERT(RSBSP::end_create(A) != blas_invalid_handle,
                  "Error in librsb end matrix creation");

  const RSBSCALAR* in_v = (const RSBSCALAR*)(dm_in.get_ptr());
  RSBSCALAR* out_v = (RSBSCALAR*)(dm_out.get_ptr());
  const RSBSCALAR rsbalpha = static_cast<RSBSCALAR>(alpha);
  const int nrhs = dm_in.get_n_cols();

  // beta is not supported by internal routines
  dm_out *= beta;

  if (nrhs == 1) { // Matrix vector product
    err = RSBSP::matvect(rsb_trans_type<opA>(), alpha, A, in_v,
                         dm_in.get_increment(), out_v, dm_out.get_increment());
    COMPOSYX_ASSERT(err == 0, "Error in librsb matvect");
  } else { // Matrix matrix product
    err = RSBSP::matmat(blas_colmajor, rsb_trans_type<opA>(), nrhs, alpha, A,
                        in_v, dm_in.get_leading_dim(), out_v,
                        dm_out.get_leading_dim());
    COMPOSYX_ASSERT(err == 0, "Error in librsb matmat");
  }

  // free A
  COMPOSYX_ASSERT(BLAS_usds(A) == 0, "Error in librsb matrix destruction");
}

} // namespace composyx
// No heading:1 ends here
