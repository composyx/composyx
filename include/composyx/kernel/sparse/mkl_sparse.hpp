// [[file:../../../../org/composyx/include/composyx/kernel/sparse/mkl_sparse.hpp :comments link][No heading:1]]
#pragma once

#include "composyx/loc_data/SparseMatrixCOO.hpp"

#ifndef INTEL_MKL_VERSION
#ifndef COMPOSYX_USE_FABULOUS
#include <lapack.hh>
#include <mkl.h>
#else
#define FABULOUS_USE_MKL_SPARSE
#include <mkl_spblas.h>
#endif
#endif

namespace composyx {

template <typename Scalar> struct mkl_spblas : public std::false_type {};

template <> struct mkl_spblas<float> {
  template <typename... Args> static auto create_coomat(Args... args) {
    return mkl_sparse_s_create_coo(args...);
  }
  template <typename... Args> static auto create_cscmat(Args... args) {
    return mkl_sparse_s_create_csc(args...);
  }
  template <typename... Args> static auto create_csrmat(Args... args) {
    return mkl_sparse_s_create_csr(args...);
  }
  template <typename... Args> static auto matvect(Args... args) {
    return mkl_sparse_s_mv(args...);
  }
  template <typename... Args> static auto matmat(Args... args) {
    return mkl_sparse_s_mm(args...);
  }
  using valtype = float;
};
template <> struct mkl_spblas<double> {
  template <typename... Args> static auto create_coomat(Args... args) {
    return mkl_sparse_d_create_coo(args...);
  }
  template <typename... Args> static auto create_cscmat(Args... args) {
    return mkl_sparse_d_create_csc(args...);
  }
  template <typename... Args> static auto create_csrmat(Args... args) {
    return mkl_sparse_d_create_csr(args...);
  }
  template <typename... Args> static auto matvect(Args... args) {
    return mkl_sparse_d_mv(args...);
  }
  template <typename... Args> static auto matmat(Args... args) {
    return mkl_sparse_d_mm(args...);
  }
  using valtype = double;
};
template <> struct mkl_spblas<std::complex<float>> {
  template <typename... Args> static auto create_coomat(Args... args) {
    return mkl_sparse_c_create_coo(args...);
  }
  template <typename... Args> static auto create_cscmat(Args... args) {
    return mkl_sparse_c_create_csc(args...);
  }
  template <typename... Args> static auto create_csrmat(Args... args) {
    return mkl_sparse_c_create_csr(args...);
  }
  template <typename... Args> static auto matvect(Args... args) {
    return mkl_sparse_c_mv(args...);
  }
  template <typename... Args> static auto matmat(Args... args) {
    return mkl_sparse_c_mm(args...);
  }
  using valtype = MKL_Complex8;
};
template <> struct mkl_spblas<std::complex<double>> {
  template <typename... Args> static auto create_coomat(Args... args) {
    return mkl_sparse_z_create_coo(args...);
  }
  template <typename... Args> static auto create_cscmat(Args... args) {
    return mkl_sparse_z_create_csc(args...);
  }
  template <typename... Args> static auto create_csrmat(Args... args) {
    return mkl_sparse_z_create_csr(args...);
  }
  template <typename... Args> static auto matvect(Args... args) {
    return mkl_sparse_z_mv(args...);
  }
  template <typename... Args> static auto matmat(Args... args) {
    return mkl_sparse_z_mm(args...);
  }
  using valtype = MKL_Complex16;
};

template <typename Matrix> auto describe_matrix(const Matrix& mat) {
  struct matrix_descr desc;

  if (mat.is_symmetric()) {
    desc.type = SPARSE_MATRIX_TYPE_SYMMETRIC;
  } else if (mat.is_hermitian()) {
    desc.type = SPARSE_MATRIX_TYPE_HERMITIAN;
  } else {
    desc.type = SPARSE_MATRIX_TYPE_GENERAL;
  }

  if (mat.is_storage_upper()) {
    desc.mode = SPARSE_FILL_MODE_UPPER;
  } else if (mat.is_storage_lower()) {
    desc.mode = SPARSE_FILL_MODE_LOWER;
  } else {
    desc.mode = SPARSE_FILL_MODE_FULL;
  }

  desc.diag = SPARSE_DIAG_NON_UNIT;
  return desc;
}

template <Op opA> constexpr inline auto mkl_trans_type() {
  if constexpr (opA == Op::NoTrans)
    return SPARSE_OPERATION_NON_TRANSPOSE;
  if constexpr (opA == Op::Trans)
    return SPARSE_OPERATION_TRANSPOSE;
  if constexpr (opA == Op::ConjTrans)
    return SPARSE_OPERATION_CONJUGATE_TRANSPOSE;
}

template <Op opA, class SPMat, class DM,
          typename Scalar = SPMat::scalar_type> // SPMat is CSC, CSR or COO
inline void mkl_sparse_matrix_dense_matrix_product(const SPMat& sp_mat,
                                                   const DM& dm_in, DM& dm_out,
                                                   Scalar alpha, Scalar beta) {
  using Index = typename SPMat::index_type;
  using MKLSP = mkl_spblas<Scalar>;
  using MKLSCALAR = typename MKLSP::valtype;

  MKL_INT m = static_cast<MKL_INT>(n_rows(sp_mat));
  MKL_INT n = static_cast<MKL_INT>(n_cols(sp_mat));
  sparse_matrix_t matmkl;

  Index* i_ptr = const_cast<Index*>(sp_mat.get_i_ptr());
  Index* j_ptr = const_cast<Index*>(sp_mat.get_j_ptr());
  MKLSCALAR* v_ptr = (MKLSCALAR*)(sp_mat.get_v_ptr());

  if constexpr (std::is_same_v<SPMat, SparseMatrixCOO<Scalar, Index>>) {
    MKL_INT nnz = static_cast<MKL_INT>(n_nonzero(sp_mat));
    MKLSP::create_coomat(&matmkl, SPARSE_INDEX_BASE_ZERO, m, n, nnz, i_ptr,
                         j_ptr, v_ptr);
  } else if (std::is_same_v<SPMat, SparseMatrixCSC<Scalar, Index>>) {
    MKLSP::create_cscmat(&matmkl, SPARSE_INDEX_BASE_ZERO, m, n, j_ptr,
                         &(j_ptr[1]), i_ptr, v_ptr);
  } else if (std::is_same_v<SPMat, SparseMatrixCSR<Scalar, Index>>) {
    MKLSP::create_csrmat(&matmkl, SPARSE_INDEX_BASE_ZERO, m, n, i_ptr,
                         &(i_ptr[1]), j_ptr, v_ptr);
  }

  const struct matrix_descr desc = describe_matrix(sp_mat);

  const MKLSCALAR* in_v = (const MKLSCALAR*)(dm_in.get_ptr());
  MKLSCALAR* out_v = (MKLSCALAR*)(dm_out.get_ptr());
  const MKLSCALAR mklalpha = static_cast<MKLSCALAR>(alpha);
  const MKLSCALAR mklbeta = static_cast<MKLSCALAR>(beta);

  if (dm_in.get_n_cols() == 1) { // Matrix vector product
    MKLSP::matvect(mkl_trans_type<opA>(), mklalpha, matmkl, desc, in_v, mklbeta,
                   out_v);
  } else { // Matrix matrix product
    const MKL_INT ncol_out = static_cast<MKL_INT>(dm_in.get_n_cols());
    // Matrix prdouct function not supported with theses formats yet

    for (MKL_INT irhs = 0; irhs < ncol_out; irhs++) {
      const MKLSCALAR* in_v_irhs = (const MKLSCALAR*)(dm_in.get_ptr(0, irhs));
      out_v = (MKLSCALAR*)(dm_out.get_ptr(0, irhs));
      MKLSP::matvect(mkl_trans_type<opA>(), mklalpha, matmkl, desc, in_v_irhs,
                     mklbeta, out_v);
    }
  }
}
} // namespace composyx
// No heading:1 ends here
