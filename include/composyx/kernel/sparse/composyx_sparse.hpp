// [[file:../../../../org/composyx/kernel/sparse/composyx_sparse.org::*Header][Header:1]]
#pragma once

#include "composyx/loc_data/SparseMatrixCOO.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../../org/composyx/kernel/sparse/composyx_sparse.org::*Matrix product: COO Matrix with DenseMatrix (or vector)][Matrix product: COO Matrix with DenseMatrix (or vector):1]]
template <Op opA, class Scalar, class Index, class DM>
inline void
sparse_matrix_dense_matrix_product(const SparseMatrixCOO<Scalar, Index>& sp_mat,
                                   const DM& dm_in, DM& dm_out, Scalar alpha,
                                   Scalar beta) {

  Timer<10000> t{"kernel: COOxDense"};
  // Y(dense) <- A(sparse) * X(dense)
  // Dimensions:
  // (M x N) <- (M x K) * (K x N)

  const size_t M =
      (opA == Op::NoTrans) ? sp_mat.get_n_rows() : sp_mat.get_n_cols();
  const size_t K =
      (opA == Op::NoTrans) ? sp_mat.get_n_cols() : sp_mat.get_n_rows();
  const size_t N = n_cols(dm_in);

  COMPOSYX_DIM_ASSERT(M, n_rows(dm_out),
                      "Sp mat x dense mat: wrong M dimension");
  COMPOSYX_DIM_ASSERT(N, n_cols(dm_out),
                      "Sp mat x dense mat: wrong N dimension");
  COMPOSYX_DIM_ASSERT(K, n_rows(dm_in),
                      "Sp mat x dense mat: wrong K dimension");

  const Index* Ai =
      (opA == Op::NoTrans) ? sp_mat.get_i_ptr() : sp_mat.get_j_ptr();
  const Index* Aj =
      (opA == Op::NoTrans) ? sp_mat.get_j_ptr() : sp_mat.get_i_ptr();
  const Scalar* Av = sp_mat.get_v_ptr();
  const size_t nnz = sp_mat.get_nnz();

  const Scalar* X = get_ptr(dm_in);
  const size_t X_ld = get_leading_dim(dm_in);
  const size_t Y_ld = get_leading_dim(dm_out);
  Scalar* Y = get_ptr(dm_out);
  dm_out *= beta;

  // Full storage

  if (sp_mat.is_storage_full()) {
    for (size_t k = 0; k < nnz; ++k) {
      const auto& row = Ai[k];
      const auto& col = Aj[k];
      const auto& val = Av[k];

      for (size_t l = 0; l < N; ++l) {
        if constexpr (is_complex<Scalar>::value and opA == Op::ConjTrans) {
          Y[l * Y_ld + row] += alpha * conj(val) * X[l * X_ld + col];
        } else {
          Y[l * Y_ld + row] += alpha * val * X[l * X_ld + col];
        }
      }
    }
  } else {
    // Half storage
    for (size_t k = 0; k < nnz; ++k) {
      auto& row = Ai[k];
      auto& col = Aj[k];
      auto& val = Av[k];

      for (size_t l = 0; l < N; ++l) {
        if constexpr (is_complex<Scalar>::value and opA == Op::ConjTrans) {
          Y[l * Y_ld + row] += alpha * conj(val) * X[l * X_ld + col];
        } else {
          Y[l * Y_ld + row] += alpha * val * X[l * X_ld + col];
        }
      }

      if (row != col) {
        if (!sp_mat.is_hermitian()) {
          for (size_t l = 0; l < N; ++l) {
            if constexpr (is_complex<Scalar>::value and opA == Op::ConjTrans) {
              Y[l * Y_ld + col] += alpha * conj(val) * X[l * X_ld + row];
            } else {
              Y[l * Y_ld + col] += alpha * val * X[l * X_ld + row];
            }
          }
        } else {
          for (size_t l = 0; l < N; ++l) {
            if constexpr (is_complex<Scalar>::value and opA == Op::ConjTrans) {
              Y[l * Y_ld + col] += alpha * val * X[l * X_ld + row];
            } else {
              Y[l * Y_ld + col] += alpha * conj(val) * X[l * X_ld + row];
            }
          }
        }
      }
    }
  }
}
// Matrix product: COO Matrix with DenseMatrix (or vector):1 ends here

// [[file:../../../../org/composyx/kernel/sparse/composyx_sparse.org::*Matrix product: CSC Matrix with DenseMatrix (or vector)][Matrix product: CSC Matrix with DenseMatrix (or vector):1]]
template <Op opA, class Scalar, class Index, class DM>
inline void
sparse_matrix_dense_matrix_product(const SparseMatrixCSC<Scalar, Index>& sp_mat,
                                   const DM& dm_in, DM& dm_out, Scalar alpha,
                                   Scalar beta) {

  Timer<10000> t{"kernel: CSCxDense"};
  // Y(dense) <- A(sparse) * X(dense)
  // Dimensions:
  // (M x N) <- (M x K) * (K x N)

  const size_t M =
      (opA == Op::NoTrans) ? sp_mat.get_n_rows() : sp_mat.get_n_cols();
  const size_t K =
      (opA == Op::NoTrans) ? sp_mat.get_n_cols() : sp_mat.get_n_rows();
  const size_t N = n_cols(dm_in);

  COMPOSYX_ASSERT(M == n_rows(dm_out), "Sp mat x dense mat: wrong M dimension");
  COMPOSYX_ASSERT(N == n_cols(dm_out), "Sp mat x dense mat: wrong N dimension");
  COMPOSYX_ASSERT(K == n_rows(dm_in), "Sp mat x dense mat: wrong K dimension");

  const Index* Aia = sp_mat.get_i_ptr();
  const Index* Aja = sp_mat.get_j_ptr();
  const Scalar* Av = sp_mat.get_v_ptr();

  const Scalar* X = get_ptr(dm_in);
  const size_t X_ld = get_leading_dim(dm_in);
  const size_t Y_ld = get_leading_dim(dm_out);
  Scalar* Y = get_ptr(dm_out);
  dm_out *= beta;

  // Full storage

  if (sp_mat.is_storage_full()) {
    for (size_t k = 0; k < sp_mat.get_n_cols(); ++k) {
      Index col = static_cast<Index>(k);
      for (Index k2 = Aja[k]; k2 < Aja[k + 1]; ++k2) {
        auto& row = Aia[k2];
        auto& val = Av[k2];
        for (size_t l = 0; l < N; ++l) {
          if constexpr (is_complex<Scalar>::value and opA == Op::ConjTrans) {
            Y[l * Y_ld + col] += alpha * conj(val) * X[l * X_ld + row];
          } else if constexpr (opA == Op::Trans) {
            Y[l * Y_ld + col] += alpha * val * X[l * X_ld + row];
          } else {
            Y[l * Y_ld + row] += alpha * val * X[l * X_ld + col];
          }
        }
      }
    }
  } else {
    // Half storage
    for (size_t k = 0; k < sp_mat.get_n_cols(); ++k) {
      Index col = static_cast<Index>(k);
      for (Index k2 = Aja[k]; k2 < Aja[k + 1]; ++k2) {
        auto& row = Aia[k2];
        auto& val = Av[k2];
        for (size_t l = 0; l < N; ++l) {
          if constexpr (is_complex<Scalar>::value and opA == Op::ConjTrans) {
            Y[l * Y_ld + col] += alpha * conj(val) * X[l * X_ld + row];
          } else if constexpr (opA == Op::Trans) {
            Y[l * Y_ld + col] += alpha * val * X[l * X_ld + row];
          } else {
            Y[l * Y_ld + row] += alpha * val * X[l * X_ld + col];
          }
        }

        if (row != col) {
          if (!sp_mat.is_hermitian()) {
            for (size_t l = 0; l < N; ++l) {
              if constexpr (is_complex<Scalar>::value and
                            opA == Op::ConjTrans) {
                Y[l * Y_ld + row] += alpha * conj(val) * X[l * X_ld + col];
              } else if constexpr (opA == Op::Trans) {
                Y[l * Y_ld + row] += alpha * val * X[l * X_ld + col];
              } else {
                Y[l * Y_ld + col] += alpha * val * X[l * X_ld + row];
              }
            }
          } else {
            for (size_t l = 0; l < N; ++l) {
              if constexpr (is_complex<Scalar>::value and
                            opA == Op::ConjTrans) {
                Y[l * Y_ld + row] += alpha * val * X[l * X_ld + col];
              } else if constexpr (opA == Op::Trans) {
                Y[l * Y_ld + row] += alpha * conj(val) * X[l * X_ld + col];
              } else {
                Y[l * Y_ld + col] += alpha * conj(val) * X[l * X_ld + row];
              }
            }
          }
        }
      }
    }
  }
}
// Matrix product: CSC Matrix with DenseMatrix (or vector):1 ends here

// [[file:../../../../org/composyx/kernel/sparse/composyx_sparse.org::*Matrix product: CSR Matrix with DenseMatrix (or vector)][Matrix product: CSR Matrix with DenseMatrix (or vector):1]]
template <Op opA, class Scalar, class Index, class DM>
inline void
sparse_matrix_dense_matrix_product(const SparseMatrixCSR<Scalar, Index>& sp_mat,
                                   const DM& dm_in, DM& dm_out, Scalar alpha,
                                   Scalar beta) {

  Timer<10000> t{"kernel: CSRxDense"};
  // Y(dense) <- A(sparse) * X(dense)
  // Dimensions:
  // (M x N) <- (M x K) * (K x N)

  const size_t M =
      (opA == Op::NoTrans) ? sp_mat.get_n_rows() : sp_mat.get_n_cols();
  const size_t K =
      (opA == Op::NoTrans) ? sp_mat.get_n_cols() : sp_mat.get_n_rows();
  const size_t N = n_cols(dm_in);

  COMPOSYX_ASSERT(M == n_rows(dm_out), "Sp mat x dense mat: wrong M dimension");
  COMPOSYX_ASSERT(N == n_cols(dm_out), "Sp mat x dense mat: wrong N dimension");
  COMPOSYX_ASSERT(K == n_rows(dm_in), "Sp mat x dense mat: wrong K dimension");

  const Index* Aia = sp_mat.get_i_ptr();
  const Index* Aja = sp_mat.get_j_ptr();
  const Scalar* Av = sp_mat.get_v_ptr();

  const Scalar* X = get_ptr(dm_in);
  const size_t X_ld = get_leading_dim(dm_in);
  const size_t Y_ld = get_leading_dim(dm_out);
  Scalar* Y = get_ptr(dm_out);
  dm_out *= beta;

  // Full storage

  if (sp_mat.is_storage_full()) {
    for (size_t k = 0; k < sp_mat.get_n_rows(); ++k) {
      Index row = static_cast<Index>(k);
      for (Index k2 = Aia[k]; k2 < Aia[k + 1]; ++k2) {
        auto& col = Aja[k2];
        auto& val = Av[k2];
        for (size_t l = 0; l < N; ++l) {
          if constexpr (is_complex<Scalar>::value and opA == Op::ConjTrans) {
            Y[l * Y_ld + col] += alpha * conj(val) * X[l * X_ld + row];
          } else if constexpr (opA == Op::Trans) {
            Y[l * Y_ld + col] += alpha * val * X[l * X_ld + row];
          } else {
            Y[l * Y_ld + row] += alpha * val * X[l * X_ld + col];
          }
        }
      }
    }
  } else {
    // Half storage
    for (size_t k = 0; k < sp_mat.get_n_rows(); ++k) {
      Index row = static_cast<Index>(k);
      for (Index k2 = Aia[k]; k2 < Aia[k + 1]; ++k2) {
        auto& col = Aja[k2];
        auto& val = Av[k2];
        for (size_t l = 0; l < N; ++l) {
          if constexpr (is_complex<Scalar>::value and opA == Op::ConjTrans) {
            Y[l * Y_ld + col] += alpha * conj(val) * X[l * X_ld + row];
          } else if constexpr (opA == Op::Trans) {
            Y[l * Y_ld + col] += alpha * val * X[l * X_ld + row];
          } else {
            Y[l * Y_ld + row] += alpha * val * X[l * X_ld + col];
          }
        }

        if (row != col) {
          if (!sp_mat.is_hermitian()) {
            for (size_t l = 0; l < N; ++l) {
              if constexpr (is_complex<Scalar>::value and
                            opA == Op::ConjTrans) {
                Y[l * Y_ld + row] += alpha * conj(val) * X[l * X_ld + col];
              } else if constexpr (opA == Op::Trans) {
                Y[l * Y_ld + row] += alpha * val * X[l * X_ld + col];
              } else {
                Y[l * Y_ld + col] += alpha * val * X[l * X_ld + row];
              }
            }
          } else {
            for (size_t l = 0; l < N; ++l) {
              if constexpr (is_complex<Scalar>::value and
                            opA == Op::ConjTrans) {
                Y[l * Y_ld + row] += alpha * val * X[l * X_ld + col];
              } else if constexpr (opA == Op::Trans) {
                Y[l * Y_ld + row] += alpha * conj(val) * X[l * X_ld + col];
              } else {
                Y[l * Y_ld + col] += alpha * conj(val) * X[l * X_ld + row];
              }
            }
          }
        }
      }
    }
  }
}
// Matrix product: CSR Matrix with DenseMatrix (or vector):1 ends here

// [[file:../../../../org/composyx/kernel/sparse/composyx_sparse.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
