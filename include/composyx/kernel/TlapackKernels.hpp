// [[file:../../../org/composyx/kernel/TlapackKernels.org::*Header][Header:1]]
#pragma once
#include <cctype>

// Use span as a sliceable vector of int for tlapack
#include <span>

#include <tlapack/base/arrayTraits.hpp>
#include <tlapack/LegacyMatrix.hpp>

namespace tlapack {
template <typename T>
const std::span<T> slice(const std::span<T>& v, std::pair<int, int> idxs) {
  auto& [i, f] = idxs;
  return v.subspan(i, f - i);
}

template <typename T>
std::span<T> slice(std::span<T>& v, std::pair<int, int> idxs) {
  auto& [i, f] = idxs;
  return v.subspan(i, f - i);
}
} // namespace tlapack

#include <composyx/interfaces/linalg_concepts.hpp>

template <composyx::CPX_DenseBlasMatrix Mat>
constexpr auto legacy_matrix(const Mat& A) noexcept {
  using Scalar = typename Mat::value_type;
  return tlapack::LegacyMatrix<Scalar>{
      n_rows(A), n_cols(A), (const Scalar*)get_ptr(A), get_leading_dim(A)};
}

template <composyx::CPX_DenseBlasMatrix Mat>
constexpr auto legacy_matrix(Mat& A) noexcept {
  using Scalar = typename Mat::value_type;
  return tlapack::LegacyMatrix<Scalar>{n_rows(A), n_cols(A),
                                       (Scalar*)get_ptr(A), get_leading_dim(A)};
}

#include <tlapack.hpp>
#include <tlapack/lapack/gesvd.hpp>

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/kernel/TlapackKernels.org::*Concepts][Concepts:1]]
template <typename matrix>
concept TlapackKernelMatrix =
    tlapack::concepts::Matrix<matrix> && requires(matrix m) {
      { is_storage_full(m) } -> std::same_as<bool>;
      { is_storage_lower(m) } -> std::same_as<bool>;
      { is_storage_upper(m) } -> std::same_as<bool>;
      { is_symmetric(m) } -> std::same_as<bool>;
      { is_hermitian(m) } -> std::same_as<bool>;
    };
// Concepts:1 ends here

// [[file:../../../org/composyx/kernel/TlapackKernels.org::*Concepts][Concepts:2]]
struct tlapack_kernels {
  // Concepts:2 ends here

  // [[file:../../../org/composyx/kernel/TlapackKernels.org::*Blas 1][Blas 1:1]]
  // X <- alpha * X (vector or matrix)
  template <tlapack::concepts::Vector Vector, CPX_Scalar Scalar>
  static void scal(Vector& X, Scalar alpha) {
    tlapack::scal(alpha, X);
  }

  // Y <- alpha * X + Y
  template <tlapack::concepts::Vector Vector, CPX_Scalar Scalar>
  static void axpy(const Vector& X, Vector& Y, Scalar alpha) {
    tlapack::axpy(alpha, X, Y);
  }

  // Return X dot Y
  template <tlapack::concepts::Vector Vector>
  [[nodiscard]] static CPX_Scalar auto dot(const Vector& X, const Vector& Y) {
    return tlapack::dot(X, Y);
  }

  // Return X dot Y
  template <tlapack::concepts::Vector Vector>
  [[nodiscard]] static CPX_Scalar auto dotu(const Vector& X, const Vector& Y) {
    return tlapack::dotu(X, Y);
  }

  template <tlapack::concepts::Vector Vector>
  [[nodiscard]] static CPX_Real auto norm2_squared(const Vector& X) {
    return std::real(tlapack::dot(X, X));
  }

  template <tlapack::concepts::Vector Vector>
  [[nodiscard]] static CPX_Real auto norm2(const Vector& X) {
    return tlapack::nrm2(X);
  }
  // Blas 1:1 ends here

  // [[file:../../../org/composyx/kernel/TlapackKernels.org::*Blas 2][Blas 2:1]]
  // Y = alpha * op(A) * X + beta * Y
  template <TlapackKernelMatrix Matrix, tlapack::concepts::Vector Vector,
            CPX_Scalar Scalar = typename Vector::scalar_type>
  static void gemv(const Matrix& A, const Vector& X, Vector& Y, char opA = 'N',
                   Scalar alpha = 1, Scalar beta = 0) {
    const size_t M = tlapack::nrows(A);
    const size_t N = tlapack::ncols(A);
    opA = std::toupper(opA);

    const size_t opa_rows = (opA == 'N') ? M : N;
    const size_t opa_cols = (opA == 'N') ? N : M;

    COMPOSYX_DIM_ASSERT(
        tlapack::nrows(X), opa_cols,
        "TlapackSolver::gemv dimensions of A and X do not match");
    if (tlapack::nrows(Y) != opa_rows) {
      COMPOSYX_ASSERT(
          beta == Scalar{0},
          "TlapackSolver::gemv wrong dimensions for Y (and beta != 0)");
      Y = Vector(opa_rows); // Not captured by the concept of tlapack Vector
    }

    if (is_storage_full(A)) {
      tlapack::Op op = tlapack::Op::NoTrans;
      if (opA == 'T')
        op = tlapack::Op::Trans;
      if (opA == 'C')
        op = tlapack::Op::ConjTrans;
      tlapack::gemv(op, alpha, A, X, beta, Y);
    } else {
      tlapack::Uplo uplo =
          (is_storage_lower(A)) ? tlapack::Uplo::Lower : tlapack::Uplo::Upper;
      if (is_symmetric(A)) {
        tlapack::symv(uplo, alpha, A, X, beta, Y);
      } else if (is_hermitian(A)) {
        if constexpr (is_complex<Scalar>::value) {
          tlapack::hemv(uplo, alpha, A, X, beta, Y);
        }
      } else {
        COMPOSYX_ASSERT(
            false,
            "TlapackSolver::gemv matrix A is half stored but not sym/herm");
      }
    }
  }

  // A += alpha * x * y.t()
  template <TlapackKernelMatrix Matrix, tlapack::concepts::Vector Vect1,
            tlapack::concepts::Vector Vect2,
            CPX_Scalar Scalar = typename Matrix::scalar_type>
  static void geru(Matrix& A, const Vect1& X, const Vect2& Y,
                   Scalar alpha = 1) {
    tlapack::geru(alpha, X, Y, A);
  }

  // A += alpha * x * y.h()
  template <TlapackKernelMatrix Matrix, tlapack::concepts::Vector Vect1,
            tlapack::concepts::Vector Vect2,
            CPX_Scalar Scalar = typename Matrix::scalar_type>
  static void ger(Matrix& A, const Vect1& X, const Vect2& Y, Scalar alpha = 1) {
    tlapack::ger(alpha, X, Y, A);
  }
  // Blas 2:1 ends here

  // [[file:../../../org/composyx/kernel/TlapackKernels.org::*Blas 3][Blas 3:1]]
  // C <- alpha * opA(A) * opB(B) + beta * C
  template <TlapackKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type>
  static void gemm(const Matrix& A, const Matrix& B, Matrix& C, char opA = 'N',
                   char opB = 'N', Scalar alpha = 1, Scalar beta = 0) {
    opA = std::toupper(opA);
    opB = std::toupper(opB);
    const size_t opa_rows =
        (opA == 'N') ? tlapack::nrows(A) : tlapack::ncols(A);
    const size_t opa_cols =
        (opA == 'N') ? tlapack::ncols(A) : tlapack::nrows(A);
    const size_t opb_rows =
        (opB == 'N') ? tlapack::nrows(B) : tlapack::ncols(B);
    const size_t opb_cols =
        (opB == 'N') ? tlapack::ncols(B) : tlapack::nrows(B);

    COMPOSYX_DIM_ASSERT(
        opa_cols, opb_rows,
        "TlapackSolver::gemm dimensions of A and B do not match");
    if (tlapack::nrows(C) != opa_rows or tlapack::ncols(C) != opb_cols) {
      COMPOSYX_ASSERT(
          beta == Scalar{0},
          "TlapackSolver::gemm wrong dimensions for C (and beta != 0)");
      C = Matrix(opa_rows, opb_cols);
    }

    if (is_storage_full(A) and is_storage_full(B)) {
      auto op_blas = [](char op) {
        if (op == 'T')
          return tlapack::Op::Trans;
        if (op == 'C')
          return tlapack::Op::ConjTrans;
        return tlapack::Op::NoTrans;
      };

      tlapack::gemm(op_blas(opA), op_blas(opB), alpha, A, B, beta, C);
    } else {

      const Matrix& sym_mat = (is_storage_full(A)) ? B : A;
      const Matrix& full_mat = (is_storage_full(A)) ? A : B;
      tlapack::Side side =
          (is_storage_full(A)) ? tlapack::Side::Right : tlapack::Side::Left;
      tlapack::Uplo uplo = (is_storage_lower(sym_mat)) ? tlapack::Uplo::Lower
                                                       : tlapack::Uplo::Upper;

      if (is_symmetric(sym_mat)) {
        tlapack::symm(side, uplo, alpha, sym_mat, full_mat, beta, C);
      } else if (is_hermitian(sym_mat)) {
        if constexpr (is_complex<Scalar>::value) {
          tlapack::hemm(side, uplo, alpha, sym_mat, full_mat, beta, C);
        }
      } else {
        COMPOSYX_ASSERT(
            false,
            "TlapackSolver::gemm matrix is half stored but not sym/herm");
      }
    }
  }
  // Blas 3:1 ends here

  // [[file:../../../org/composyx/kernel/TlapackKernels.org::*Eigenvalue decomposition][Eigenvalue decomposition:1]]
  template <
      TlapackKernelMatrix Matrix,
      TlapackKernelMatrix ComplexMatrix = typename complex_type<Matrix>::type,
      CPX_Scalar Scalar = typename Matrix::scalar_type,
      CPX_Complex Complex = typename ComplexMatrix::scalar_type>
  [[nodiscard]] static std::pair<std::vector<Complex>, ComplexMatrix>
  eig(const Matrix&) {
    COMPOSYX_ASSERT(false, "Tlapack::eig not implemented");
    return std::pair<std::vector<Complex>, ComplexMatrix>();
  }

  template <
      TlapackKernelMatrix Matrix,
      TlapackKernelMatrix ComplexMatrix = typename complex_type<Matrix>::type,
      CPX_Scalar Scalar = typename Matrix::scalar_type,
      CPX_Complex Complex = typename ComplexMatrix::scalar_type>
  [[nodiscard]] static std::vector<Complex> eigvals(const Matrix&) {
    COMPOSYX_ASSERT(false, "Tlapack::eigvals not implemented");
    return std::vector<Complex>();
  }

  template <TlapackKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  eigh(const Matrix&) {
    COMPOSYX_ASSERT(false, "Tlapack::eigh not implemented");
    return std::pair<std::vector<Real>, Matrix>();
  }

  template <TlapackKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::vector<Real> eigvalsh(const Matrix&) {
    COMPOSYX_ASSERT(false, "Tlapack::eigvalsh not implemented");
    return std::vector<Real>();
  }

  template <TlapackKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, DenseMatrix<Scalar>>
  eigh_select(const Matrix&, int, int) {
    COMPOSYX_ASSERT(false, "Tlapack::eigh_select not implemented");
    return std::pair<std::vector<Real>, DenseMatrix<Scalar>>();
  }

  template <TlapackKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, DenseMatrix<Scalar>>
  eigh_smallest(const Matrix&, int) {
    COMPOSYX_ASSERT(false, "Tlapack::eigh_smallest not implemented");
    return std::pair<std::vector<Real>, DenseMatrix<Scalar>>();
  }

  template <TlapackKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, DenseMatrix<Scalar>>
  eigh_largest(const Matrix&, int) {
    COMPOSYX_ASSERT(false, "Tlapack::eigh_largest not implemented");
    return std::pair<std::vector<Real>, DenseMatrix<Scalar>>();
  }

  template <TlapackKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  gen_eigh_select(const Matrix&, const Matrix&, int, int) {
    COMPOSYX_ASSERT(false, "Tlapack::gen_eigh_select not implemented");
    return std::pair<std::vector<Real>, Matrix>();
  }

  template <TlapackKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  gen_eigh_smallest(const Matrix&, const Matrix&, int) {
    COMPOSYX_ASSERT(false, "Tlapack::gen_eigh_smallest not implemented");
    return std::pair<std::vector<Real>, Matrix>();
  }

  template <TlapackKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  gen_eigh_largest(const Matrix&, const Matrix&, int) {
    COMPOSYX_ASSERT(false, "Tlapack::gen_eigh_largest not implemented");
    return std::pair<std::vector<Real>, Matrix>();
  }

  template <TlapackKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::tuple<Matrix, std::vector<Real>, Matrix>
  _svd_gesvd(Matrix A, bool compute_u = true, bool compute_vt = true,
             bool reduced = true) {
    size_t M = n_rows(A);
    size_t N = n_cols(A);
    size_t K = std::min(M, N);

    // !!!! Seems like reduced is not implemented

    Matrix U;
    if (compute_u) {
      U = Matrix(M, M);
      // if(reduced){
      //   U = Matrix(M, K);
      // } else {
      //   U = Matrix(M, M);
      // }
    }

    Matrix Vt;
    if (compute_vt) {
      Vt = Matrix(N, N);
      //if(reduced){
      //  Vt = Matrix(K, N);
      //} else {
      //  Vt = Matrix(N, N);
      //}
    }

    std::vector<Real> svalues(K);
    std::span<Real> span_sv(svalues);

    auto tlpA = legacy_matrix(A);
    auto tlpU = legacy_matrix(U);
    auto tlpVt = legacy_matrix(Vt);

    tlapack::gesvd(compute_u, compute_vt, tlpA, span_sv, tlpU, tlpVt);

    // Truncate the result if reduced form is demanded
    if (reduced) {
      Matrix U_hat;
      Matrix Vt_hat;
      if (compute_u) {
        U_hat = Matrix(M, K);
        for (size_t j = 0; j < K; ++j) {
          for (size_t i = 0; i < M; ++i) {
            U_hat(i, j) = U(i, j);
          }
        }
      }
      if (compute_vt) {
        Vt_hat = Matrix(K, N);
        for (size_t j = 0; j < N; ++j) {
          for (size_t i = 0; i < K; ++i) {
            Vt_hat(i, j) = Vt(i, j);
          }
        }
      }
      return std::make_tuple(std::move(U_hat), std::move(svalues),
                             std::move(Vt_hat));
    }

    // Else return full SVD
    return std::make_tuple(std::move(U), std::move(svalues), std::move(Vt));
  }

  template <TlapackKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::tuple<Matrix, std::vector<Real>, Matrix>
  svd(const Matrix& A, bool reduced = true) {
    return _svd_gesvd<Matrix, Scalar, Real>(A, true, true, reduced);
  }

  // Returns U and sigmas only
  template <TlapackKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<Matrix, std::vector<Real>>
  svd_left_sv(const Matrix& A, bool reduced = true) {
    auto [U, sigmas, Vt] =
        _svd_gesvd<Matrix, Scalar, Real>(A, true, false, reduced);
    return std::move(std::make_pair(std::move(U), std::move(sigmas)));
  }

  // Returns sigmas and Vt only
  template <TlapackKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  svd_right_sv(const Matrix& A, bool reduced = true) {
    auto [U, sigmas, Vt] =
        _svd_gesvd<Matrix, Scalar, Real>(A, false, true, reduced);
    return std::move(std::make_pair(std::move(sigmas), std::move(Vt)));
  }

  // Returns only singular values
  template <TlapackKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::vector<Real> svdvals(const Matrix& A) {
    auto [U, sigmas, Vt] = _svd_gesvd<Matrix, Scalar, Real>(A, false, false);
    return sigmas;
  }
  // Eigenvalue decomposition:1 ends here

  // [[file:../../../org/composyx/kernel/TlapackKernels.org::*Footer][Footer:1]]
}; // struct tlapack_kernels
} // namespace composyx
// Footer:1 ends here
