// [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Header][Header:1]]
#pragma once
#include <cctype>

#include <chameleon.h>
#include "composyx/utils/Chameleon.hpp"
#include "composyx/kernel/BlasKernels.hpp"
#include "composyx/solver/ChameleonSolver.hpp"

namespace composyx {
struct chameleon_kernels : blas_kernels {
  // Header:1 ends here

  // [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Blas 3][Blas 3:1]]
  // C <- alpha * opA(A) * opB(B) + beta * C
  template <BlasKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type>
  static void gemm(const Matrix& A, const Matrix& B, Matrix& C, char opA = 'N',
                   char opB = 'N', Scalar alpha = 1, Scalar beta = 0) {

    int translation;
    CHAMELEON_Get(CHAMELEON_TRANSLATION_MODE, &translation);
    int ts;
    CHAMELEON_Get(CHAMELEON_TILE_SIZE, &ts);

    opA = std::toupper(opA);
    opB = std::toupper(opB);
    const int opa_rows = static_cast<int>((opA == 'N') ? n_rows(A) : n_cols(A));
    const int opa_cols = static_cast<int>((opA == 'N') ? n_cols(A) : n_rows(A));
    const int opb_rows = static_cast<int>((opB == 'N') ? n_rows(B) : n_cols(B));
    const int opb_cols = static_cast<int>((opB == 'N') ? n_cols(B) : n_rows(B));

    COMPOSYX_DIM_ASSERT(
        opa_cols, opb_rows,
        "Matrix matrix multiplication AB: nb cols(op(A)) != nb rows(op(B))");
    if (static_cast<int>(n_rows(C)) != opa_rows or
        static_cast<int>(n_cols(C)) != opb_cols) {
      COMPOSYX_ASSERT(
          beta == Scalar{0},
          "ChameleonKernels::gemm wrong dimensions for C (and beta != 0)");
      C = Matrix(opa_rows, opb_cols);
    }

    auto op_trans = [](char op) {
      if (op == 'T')
        return ChamTrans;
      if (op == 'C')
        return ChamConjTrans;
      return ChamNoTrans;
    };

    const int ldc = static_cast<int>(get_leading_dim(C));

    if (is_storage_full(A) and is_storage_full(B)) {

      const int ma = static_cast<int>(n_rows(A));
      const int na = static_cast<int>(n_cols(A));
      const int lda = static_cast<int>(get_leading_dim(A));
      const int mb = static_cast<int>(n_rows(B));
      const int nb = static_cast<int>(n_cols(B));
      const int ldb = static_cast<int>(get_leading_dim(B));

      if (translation == ChamOutOfPlace) {
        CHAM_desc_t* chamA;
        CHAMELEON_Desc_Create(&chamA, CHAMELEON_MAT_ALLOC_TILE,
                              chameleon::getType(alpha), ts, ts, ts * ts, lda,
                              na, 0, 0, ma, na, 1, 1);
        CHAM_desc_t* chamB;
        CHAMELEON_Desc_Create(&chamB, CHAMELEON_MAT_ALLOC_TILE,
                              chameleon::getType(alpha), ts, ts, ts * ts, ldb,
                              nb, 0, 0, mb, nb, 1, 1);
        CHAM_desc_t* chamC;
        CHAMELEON_Desc_Create(&chamC, CHAMELEON_MAT_ALLOC_TILE,
                              chameleon::getType(alpha), ts, ts, ts * ts, ldc,
                              opb_cols, 0, 0, opa_rows, opb_cols, 1, 1);

        chameleon::lap_to_cham(A, chamA);
        chameleon::lap_to_cham(B, chamB);
        chameleon::lap_to_cham(C, chamC);

        chameleon::gemm_tile(op_trans(opA), op_trans(opB), alpha, chamA, chamB,
                             beta, chamC);

        chameleon::cham_to_lap(chamC, C);

        CHAMELEON_Desc_Destroy(&chamA);
        CHAMELEON_Desc_Destroy(&chamB);
        CHAMELEON_Desc_Destroy(&chamC);
      } else {

        chameleon::gemm(op_trans(opA), op_trans(opB), opa_rows, opb_cols,
                        opa_cols, alpha, get_ptr(A), lda, get_ptr(B), ldb, beta,
                        get_ptr(C), ldc);
      }

    } else { // A or B half-stored

      cham_side_t side = (is_storage_full(B)) ? ChamLeft : ChamRight;
      const Matrix& mat_half = (is_storage_full(B)) ? A : B;
      const Matrix& mat_full = (is_storage_full(B)) ? B : A;

      const int ld_half = static_cast<int>(get_leading_dim(mat_half));
      const int ld_full = static_cast<int>(get_leading_dim(mat_full));

      cham_uplo_t uplo = (is_storage_lower(mat_half)) ? ChamLower : ChamUpper;
      const int M_half = static_cast<int>(n_rows(mat_half));
      const int M_full = static_cast<int>(n_rows(mat_full));
      const int N_full = static_cast<int>(n_cols(mat_full));
      const int M_C = static_cast<int>(n_rows(C));
      const int N_C = static_cast<int>(n_cols(C));

      if (translation == ChamOutOfPlace) {
        CHAM_desc_t* chamA;
        CHAMELEON_Desc_Create(&chamA, CHAMELEON_MAT_ALLOC_TILE,
                              chameleon::getType(alpha), ts, ts, ts * ts,
                              ld_half, M_half, 0, 0, M_half, M_half, 1, 1);
        CHAM_desc_t* chamB;
        CHAMELEON_Desc_Create(&chamB, CHAMELEON_MAT_ALLOC_TILE,
                              chameleon::getType(alpha), ts, ts, ts * ts,
                              ld_full, N_full, 0, 0, M_half, N_full, 1, 1);
        CHAM_desc_t* chamC;
        CHAMELEON_Desc_Create(&chamC, CHAMELEON_MAT_ALLOC_TILE,
                              chameleon::getType(alpha), ts, ts, ts * ts, ldc,
                              opb_cols, 0, 0, opa_rows, opb_cols, 1, 1);

        chameleon::lap_to_cham(mat_half, chamA);
        if (is_symmetric(mat_half) or is_hermitian(mat_half)) {
          chameleon::lap_to_cham(mat_full, chamB);
          chameleon::lap_to_cham(C, chamC);
        }

        if (is_symmetric(mat_half)) {

          chameleon::symm_tile(side, uplo, alpha, chamA, chamB, beta, chamC);

        } else if (is_hermitian(mat_half)) {

          chameleon::hemm_tile(side, uplo, alpha, chamA, chamB, beta, chamC);

        } else if (is_general(mat_half) and (M_full == M_C and N_full == N_C)) {

          char op_half = (is_storage_full(B)) ? opA : opB;
          chameleon::lap_to_cham(mat_full, chamC);

          chameleon::trmm_tile(side, uplo, op_trans(op_half), ChamNonUnit,
                               alpha, chamA, chamC);

        } else {
          COMPOSYX_ASSERT(
              false,
              "ChameleonKernels::gemm matrix is half stored but not sym/herm"
              "or not adapted for trmm (C and full matrix sizes must match)");
        }

        chameleon::cham_to_lap(chamC, C);

        CHAMELEON_Desc_Destroy(&chamA);
        CHAMELEON_Desc_Destroy(&chamB);
        CHAMELEON_Desc_Destroy(&chamC);

      } else {

        if (is_symmetric(mat_half)) {

          chameleon::symm(side, uplo, M_C, N_C, alpha, get_ptr(mat_half),
                          ld_half, get_ptr(mat_full), ld_full, beta, get_ptr(C),
                          ldc);

        } else if (is_hermitian(mat_half)) {

          chameleon::hemm(side, uplo, M_C, N_C, alpha, get_ptr(mat_half),
                          ld_half, get_ptr(mat_full), ld_full, beta, get_ptr(C),
                          ldc);

        } else if (is_general(mat_half) and (M_full == M_C and N_full == N_C)) {

          char op_half = (is_storage_full(B)) ? opA : opB;
          C = mat_full;

          chameleon::trmm(side, uplo, op_trans(op_half), ChamNonUnit, M_full,
                          N_full, alpha, get_ptr(mat_half), ld_half, get_ptr(C),
                          ldc);

        } else {
          COMPOSYX_ASSERT(
              false,
              "ChameleonKernels::gemm matrix is half stored but not sym/herm"
              "or not adapted for trmm (C and full matrix sizes must match)");
        }
      }
    }
  }
  // Blas 3:1 ends here

  // [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Randomized Singular and Eigen value decomposition][Randomized Singular and Eigen value decomposition:1]]
  // TODO: should be moved in TestMatrix.hpp
  template <BlasKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static Matrix random_matrix_normal(size_t m, size_t n) {

    int translation;
    CHAMELEON_Get(CHAMELEON_TRANSLATION_MODE, &translation);
    int ts;
    CHAMELEON_Get(CHAMELEON_TILE_SIZE, &ts);

    Matrix A = Matrix(m, n);

    unsigned long long int seed = rand() % 100 + 1;

    if (translation == ChamOutOfPlace) {
      Scalar alpha = 0;
      CHAM_desc_t* chamA;
      CHAMELEON_Desc_Create(&chamA, CHAMELEON_MAT_ALLOC_TILE,
                            chameleon::getType(alpha), ts, ts, ts * ts, m, n, 0,
                            0, m, n, 1, 1);

      chameleon::plrnt_tile(chamA, seed);

      chameleon::cham_to_lap(chamA, A);
      CHAMELEON_Desc_Destroy(&chamA);
    } else {

      chameleon::plrnt(m, n, get_ptr(A), m, seed);
    }

    return A;
  }
  // Randomized Singular and Eigen value decomposition:1 ends here

  // [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Randomized Singular and Eigen value decomposition][Randomized Singular and Eigen value decomposition:2]]
  template <BlasKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static Matrix
  _randomized_range_finder(const Matrix& A, const int l,
                           const int subspace_iter = 2) {
    size_t n = n_cols(A);

    // generate random matrix
    Matrix Omega = random_matrix_normal<Matrix>(n, l);

    // compute image of A
    Matrix Y(n_rows(A), l);
    gemm(A, Omega, Y);

    // QR factorization of Y
    ChameleonSolver<Matrix, Vector<Scalar>> solverQRy(std::move(Y));
    solverQRy.factorizeQR();
    Matrix Qy = solverQRy.matrixQ();

    Matrix Yp;
    ChameleonSolver<Matrix, Vector<Scalar>> solverQRyp;
    if (subspace_iter > 0) {
      Yp = Matrix(n, l);
    }

    // randomized subspace iterations, algorithm 4.4
    int iter = 1;
    while (iter <= subspace_iter) {
      // Y'= A^* Qy
      if (is_symmetric(A)) {
        gemm(A, Qy, Yp, 'N', 'N');
      } else {
        if constexpr (is_complex<Scalar>::value) {
          gemm(A, Qy, Yp, 'C', 'N');
        } else {
          gemm(A, Qy, Yp, 'T', 'N');
        }
      }
      // Q', R' = QR(Y')
      solverQRyp.setup(std::move(Yp));
      solverQRyp.factorizeQR();
      Qy = solverQRyp.matrixQ();

      // Y = A*Q
      gemm(A, Qy, Y);

      // Qy, Ry = QR(Y)
      solverQRy.setup(std::move(Y));
      solverQRy.factorizeQR();
      Qy = solverQRy.matrixQ();
      iter++;
    }

    return Qy;
  }
  // Randomized Singular and Eigen value decomposition:2 ends here

  // [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Randomized Singular and Eigen value decomposition][Randomized Singular and Eigen value decomposition:3]]
  template <BlasKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::tuple<Matrix, std::vector<Real>, Matrix>
  _svd_rsvd_direct(const Matrix& A, const Matrix& Q, bool compute_u = true,
                   bool compute_vt = true) {
    const size_t m = n_rows(A);
    const size_t n = n_cols(A);
    const size_t l = n_cols(Q);

    // B = Q^* A
    Matrix B = Matrix(l, n);
    if constexpr (is_complex<Scalar>::value) {
      gemm(Q, A, B, 'C', 'N');
    } else {
      gemm(Q, A, B, 'T', 'N');
    }

    // U_b, S_b, V_b^* = SVD(B), V_b = V
    std::vector<Real> S;
    Matrix Ub, Vt;
    setBlasThreads(getMaxBlasThreads());
    if (compute_u && compute_vt) {
      auto [Ui, Si, Vti] = svd(B);
      Ub = std::move(Ui);
      S = std::move(Si);
      Vt = std::move(Vti);
    } else if (compute_u) {
      auto [Ui, Si] = svd_left_sv(B);
      Ub = std::move(Ui);
      S = std::move(Si);
    } else if (compute_vt) {
      auto [Si, Vti] = svd_right_sv(B);
      S = std::move(Si);
      Vt = std::move(Vti);
    } else {
      auto Si = svdvals(B);
      S = std::move(Si);
    }
    setBlasThreads(1);

    // U = Q U_b
    Matrix U = Matrix(m, l);
    if (compute_u) {
      gemm(Q, Ub, U, 'N', 'N');
    }

    return std::make_tuple(std::move(U), std::move(S), std::move(Vt));
  }
  // Randomized Singular and Eigen value decomposition:3 ends here

  // [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Randomized Singular and Eigen value decomposition][Randomized Singular and Eigen value decomposition:4]]
  template <BlasKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::tuple<Matrix, std::vector<Real>, Matrix>
  _svd_rsvd_two_stages(const Matrix& A, const Matrix& Q, bool compute_u = true,
                       bool compute_vt = true) {
    const size_t m = n_rows(A);
    const size_t n = n_cols(A);
    const size_t l = n_cols(Q);

    // C = A^* Q (if A symmetric, C = A Q)
    Matrix C = Matrix(n, l);
    if (is_symmetric(A)) {
      gemm(A, Q, C, 'N', 'N');
    } else {
      if constexpr (is_complex<Scalar>::value) {
        gemm(A, Q, C, 'C', 'N');
      } else {
        gemm(A, Q, C, 'T', 'N');
      }
    }

    // Q_c, R_c = QR(C)
    ChameleonSolver<Matrix, Vector<Scalar>> solverQRc(C);
    solverQRc.factorizeQR();
    Matrix Qc = solverQRc.matrixQ();
    Matrix Rc = solverQRc.matrixR();

    // U_rc, S_rc, V_rc^* = SVD(R_c)
    std::vector<Real> S;
    Matrix Urc, Vrct;
    //std::cout << "Blas Num threads 1 =  " << getBlasThreads() << std::endl;
    setBlasThreads(getMaxBlasThreads());
    //std::cout << "Blas Num threads 2 =  " << getBlasThreads() << std::endl;
    if (compute_u && compute_vt) {
      auto [Ui, Si, Vti] = svd(Rc);
      Urc = std::move(Ui);
      S = std::move(Si);
      Vrct = std::move(Vti);
    } else if (compute_u) {
      auto [Si, Vti] = svd_right_sv(Rc);
      S = std::move(Si);
      Vrct = std::move(Vti);
    } else if (compute_vt) {
      auto [Ui, Si] = svd_left_sv(Rc);
      Urc = std::move(Ui);
      S = std::move(Si);
    } else {
      auto Si = svdvals(Rc);
      S = std::move(Si);
    }
    setBlasThreads(1);
    //std::cout << "Blas Num threads 3 =  " << getBlasThreads() << std::endl;

    // U = Q_y V_rc
    Matrix U = Matrix(m, l);
    if (compute_u) {
      if constexpr (is_complex<Scalar>::value) {
        gemm(Q, Vrct, U, 'N', 'C');
      } else {
        gemm(Q, Vrct, U, 'N', 'T');
      }
    }

    // V^* = U_rc^* Q_c^*
    Matrix Vt = Matrix(l, n);
    if (compute_vt) {
      if constexpr (is_complex<Scalar>::value) {
        gemm(Urc, Qc, Vt, 'C', 'C');
      } else {
        gemm(Urc, Qc, Vt, 'T', 'T');
      }
    }

    return std::make_tuple(std::move(U), std::move(S), std::move(Vt));
  }
  // Randomized Singular and Eigen value decomposition:4 ends here

  // [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Randomized Singular and Eigen value decomposition][Randomized Singular and Eigen value decomposition:5]]
  template <BlasKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  _eig_revd_direct(const Matrix& A, const Matrix& Q, bool compute_u = true) {
    bool is_A_hermitian_or_symmetric = (is_hermitian(A) || is_symmetric(A));
    COMPOSYX_ASSERT(
        is_A_hermitian_or_symmetric,
        "BlasKernels::_eigh_revd_direct: A must be hermitian or symmetric");
    const size_t m = n_rows(A);
    const size_t n = n_cols(A);
    COMPOSYX_ASSERT(m == n, "BlasKernels::_eigh_revd_direct: "
                            "nrows(A)!=ncols(A), A must be squared");
    const size_t l = n_cols(Q);

    // B = Q^* A Q
    Matrix AQ(m, l);
    gemm(A, Q, AQ);
    Matrix B = Matrix(l, l);
    if constexpr (is_complex<Scalar>::value) {
      gemm(Q, AQ, B, 'C', 'N');
      B.set_property(MatrixSymmetry::hermitian);
    } else {
      gemm(Q, AQ, B, 'T', 'N');
      B.set_property(MatrixSymmetry::symmetric);
    }

    // L, V = EVD(B)
    std::vector<Real> L;
    Matrix V;
    setBlasThreads(getMaxBlasThreads());
    if (compute_u) {
      auto [Li, Vi] = eigh(B);
      // Sorting eigen values and vectors in decreasing order
      std::sort(Li.begin(), Li.end(), std::greater<Real>());
      Matrix vector_vi;
      const size_t onecol = 1;
      for (int i(0); i < static_cast<int>(l / 2); i++) {
        int lastcol = static_cast<int>(l - 1 - i);
        vector_vi = Vi.get_block_copy(0, i, l, onecol);
        Vi.get_block_view(0, i, l, onecol) =
            Vi.get_block_copy(0, lastcol, l, onecol);
        Vi.get_block_view(0, lastcol, l, onecol) = vector_vi;
      }
      L = std::move(Li);
      V = std::move(Vi);
    } else {
      auto Li = eigvalsh(B);
      std::sort(Li.begin(), Li.end(), std::greater<Real>());
      L = std::move(Li);
    }
    setBlasThreads(1);

    // U = Q V
    Matrix U = Matrix(m, l);
    if (compute_u) {
      gemm(Q, V, U, 'N', 'N');
    }

    return std::make_pair(std::move(L), std::move(U));
  }
  // Randomized Singular and Eigen value decomposition:5 ends here

  // [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Randomized Singular and Eigen value decomposition][Randomized Singular and Eigen value decomposition:6]]
  template <BlasKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::tuple<Matrix, std::vector<Real>, Matrix>
  _svd_rsvd(const Matrix& A, const int rank, const int subspace_iter = 2,
            bool direct = true, bool compute_u = true, bool compute_vt = true) {
    size_t m = n_rows(A);
    size_t n = n_cols(A);
    size_t minmn = std::min(m, n);
    size_t r = static_cast<size_t>(rank);
    COMPOSYX_ASSERT(
        r <= minmn,
        "Error in _svd_rsvd: rank cannot be larger than min(M, N) of A");
    size_t oversampling = 5;
    size_t l = r + oversampling;
    if (l > minmn) {
      l = minmn;
    }

    // stage A: randomized range approximation of A
    Matrix Qy = _randomized_range_finder(A, l, subspace_iter);

    // stage B: factorizations
    std::vector<Real> Sr;
    Matrix Ur, Vtr;
    bool is_A_hermitian_or_symmetric = (is_hermitian(A) || is_symmetric(A));

    if (is_A_hermitian_or_symmetric) {

      // direct EVD on B=Q^{*}AQ
      auto [L, U] = _eig_revd_direct(A, Qy, compute_u);

      // S = S_b[0:r-1], part without oversampling
      L.resize(r);
      Sr = L;

      // U = Q_y U_b
      if (compute_u) {
        // Extract U without columns related to oversampling
        Ur = U.get_block_copy(0, 0, m, r);
        if (compute_vt) {
          Vtr = Ur.t();
        }
      }

    } else if (direct) {

      // direct SVD on B=Q^{*}A
      auto [U, S, Vt] = _svd_rsvd_direct(A, Qy, compute_u, compute_vt);

      // S = S_b[0:r-1], part without oversampling
      S.resize(r);
      Sr = S;

      // U = Q_y U_b
      if (compute_u) {
        // Extract U without columns related to oversampling
        Ur = U.get_block_copy(0, 0, m, r);
      }

      // V^* = V_b^*
      if (compute_vt) {
        // Extract Vt without rows related to oversampling
        Vtr = Vt.get_block_copy(0, 0, r, n);
      }

    } else {

      // two-stages SVD, C=A^{*}Q, Q_c, R_c = QR(C), SVD on R_c
      auto [U, S, Vt] = _svd_rsvd_two_stages(A, Qy, compute_u, compute_vt);

      // S = S_rc[0:r-1], part without oversampling
      S.resize(r);
      Sr = S;

      // U = Q_y V_rc
      if (compute_u) {
        // Extract U without columns related to oversampling
        Ur = U.get_block_copy(0, 0, m, r);
      }

      // V^* = U_rc^* Q_c^*
      if (compute_vt) {
        // Extract Vt without rows related to oversampling
        Vtr = Vt.get_block_copy(0, 0, r, n);
      }
    }

    return std::make_tuple(std::move(Ur), std::move(Sr), std::move(Vtr));
  }
  // Randomized Singular and Eigen value decomposition:6 ends here

  // [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Randomized Singular and Eigen value decomposition][Randomized Singular and Eigen value decomposition:7]]
  // By default, we return U, sigmas and V^*
  template <BlasKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::tuple<Matrix, std::vector<Real>, Matrix>
  rsvd(const Matrix& A, const int rank, const int subspace_iter = 2,
       bool direct = true) {
    return _svd_rsvd<Matrix, Scalar, Real>(A, rank, subspace_iter, direct, true,
                                           true);
  }

  // Returns U and sigmas only
  template <BlasKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<Matrix, std::vector<Real>>
  rsvd_left_sv(const Matrix& A, const int rank, const int subspace_iter = 2,
               bool direct = true) {
    auto [U, sigmas, Vt] = _svd_rsvd<Matrix, Scalar, Real>(
        A, rank, subspace_iter, direct, true, false);
    return std::make_pair(std::move(U), std::move(sigmas));
  }

  // Returns sigmas and Vt only
  template <BlasKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  rsvd_right_sv(const Matrix& A, const int rank, const int subspace_iter = 2,
                bool direct = true) {
    auto [U, sigmas, Vt] = _svd_rsvd<Matrix, Scalar, Real>(
        A, rank, subspace_iter, direct, false, true);
    return std::make_pair(std::move(sigmas), std::move(Vt));
  }

  // Returns only singular values
  template <BlasKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::vector<Real>
  rsvdvals(const Matrix& A, const int rank, const int subspace_iter = 2,
           bool direct = true) {
    auto [U, sigmas, Vt] = _svd_rsvd<Matrix, Scalar, Real>(
        A, rank, subspace_iter, direct, false, false);
    return sigmas;
  }
  // Randomized Singular and Eigen value decomposition:7 ends here

  // [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Randomized Singular and Eigen value decomposition][Randomized Singular and Eigen value decomposition:8]]
  // By default, we return lambdas, U
  template <BlasKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  revd(const Matrix& A, const int rank, const int subspace_iter = 2,
       bool direct = true) {
    bool is_A_hermitian_or_symmetric = (is_hermitian(A) || is_symmetric(A));
    COMPOSYX_ASSERT(is_A_hermitian_or_symmetric,
                    "BlasKernels::revd: A must be hermitian or symmetric");
    auto [U, lambdas, Vt] = _svd_rsvd<Matrix, Scalar, Real>(
        A, rank, subspace_iter, direct, true, false);
    return std::make_pair(std::move(lambdas), std::move(U));
  }

  // Returns only eigen values
  template <BlasKernelMatrix Matrix,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::vector<Real>
  revdvals(const Matrix& A, const int rank, const int subspace_iter = 2,
           bool direct = true) {
    bool is_A_hermitian_or_symmetric = (is_hermitian(A) || is_symmetric(A));
    COMPOSYX_ASSERT(is_A_hermitian_or_symmetric,
                    "BlasKernels::revdvals: A must be hermitian or symmetric");
    auto [U, lambdas, Vt] = _svd_rsvd<Matrix, Scalar, Real>(
        A, rank, subspace_iter, direct, false, false);
    return lambdas;
  }
  // Randomized Singular and Eigen value decomposition:8 ends here

  // [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Footer][Footer:1]]
}; // struct chameleon_kernels
} // namespace composyx
// Footer:1 ends here
