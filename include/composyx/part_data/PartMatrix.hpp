// [[file:../../../org/composyx/part_data/PartMatrix.org::*Header][Header:1]]
#pragma once

#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <type_traits>

#include "composyx/utils/Error.hpp"
#include "composyx/utils/ArrayAlgo.hpp"
#include "composyx/dist/Process.hpp"
#include "composyx/part_data/PartVector.hpp"
#include "composyx/part_data/PartBase.hpp"
#include "composyx/loc_data/SparseMatrixLIL.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/part_data/PartMatrix.org::*Attributes][Attributes:1]]
template <CPX_Matrix Matrix> class PartMatrix : public PartBase<Matrix> {

private:
  using Scalar = typename scalar_type<Matrix>::type;
  using Real = typename arithmetic_real<Scalar>::type;
  using Base = PartBase<Matrix>;
  using LocVect = typename vector_type<Matrix>::type;
  using LocDiag = typename diag_type<Matrix>::type;

  // Assemble vector after matrix vector product
  bool _mat_vect_assemble = true;

public:
  using scalar_type = Scalar;
  using real_type = Real;
  using local_type = Matrix;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Constructors][Constructors:1]]
public:
  // Minimal constructor
  explicit PartMatrix() {}

  explicit PartMatrix(std::shared_ptr<Process> proc, bool on_interface = false)
      : Base(proc, on_interface) {}

  // One subdomain constructors
  explicit PartMatrix(std::shared_ptr<Process> proc, const int sd_id,
                      const Matrix& loc_m, bool on_interface = false)
      : Base(proc, sd_id, loc_m, on_interface) {}

  explicit PartMatrix(std::shared_ptr<Process> proc, const int sd_id,
                      Matrix&& loc_m, bool on_interface = false)
      : Base(proc, sd_id, loc_m, on_interface) {}

  // Multiple subdomain constructors
  explicit PartMatrix(std::shared_ptr<Process> proc,
                      const std::map<int, Matrix>& loc_matrices,
                      bool on_interface = false)
      : Base(proc, loc_matrices, on_interface) {}

  explicit PartMatrix(std::shared_ptr<Process> proc,
                      std::map<int, Matrix>&& loc_matrices,
                      bool on_interface = false)
      : Base(proc, loc_matrices, on_interface) {}

  // Copy constructor
  PartMatrix(const PartMatrix& dm) : Base(dm) {}

  // Move constructor
  PartMatrix(PartMatrix&& dm) : Base(std::move(dm)) {}

  PartMatrix& operator=(const PartMatrix& dm) = default;
  PartMatrix& operator=(PartMatrix&& dm) = default;

  // Other matrix type conversion
  template <class OtherMat>
  PartMatrix(const PartBase<OtherMat>& mat)
      : Base(mat.get_proc(), mat.on_intrf()) {
    for (const auto& sd_id : this->_proc->get_sd_ids()) {
      this->_loc_data.insert_or_assign(sd_id,
                                       Matrix(mat.get_local_data(sd_id)));
    }
  }
  // Constructors:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Checking process is valid][Checking process is valid:1]]
  inline std::shared_ptr<Process> check_proc_ptr() const {
    COMPOSYX_ASSERT(this->_proc.get() != nullptr,
                    "PartMatrix::_proc is nullptr");
    return this->_proc;
  }
  // Checking process is valid:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Load values from a file][Load values from a file:1]]
public:
  void load_from_file(int id, const std::string& filename) {
    std::ifstream f{filename, std::ios::in};
    std::string line;
    std::vector<std::string> s;

    if (!f.is_open()) {
      COMPOSYX_ASSERT(f, filename + ": file not opened");
    }

    std::vector<int> i, j;
    std::vector<Scalar> values;
    size_t m, n, nnz;
    MatrixSymmetry sym;
    MatrixStorage stor;

    matrix_market::load<std::vector<int>, std::vector<Scalar>>(
        filename, i, j, values, m, n, nnz, sym, stor);

    Matrix mat;
    build_matrix(mat, m, n, nnz, &i[0], &j[0], &values[0]);
    mat.set_property(sym);
    mat.set_property(stor);

    this->_loc_data[id] = mat;
  }
  // Load values from a file:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Load matrix and DDM from files][Load matrix and DDM from files:1]]
  void load_matrix_and_ddm(const std::string& path) {
    this->get_proc()->load_subdomains(path);

    for (int sd_id : this->get_sd_ids()) {
      std::string fname = path;
      if (!(fname.empty()) && fname.back() != '/') {
        fname += '/';
      }
      fname += "composyx_local_matrix" + std::to_string(sd_id) + ".mtx";
      load_from_file(sd_id, fname);
    }
  }
  // Load matrix and DDM from files:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Dump local matrix to a matrix market file][Dump local matrix to a matrix market file:1]]
public:
  void dump_to_file(int id, const std::string& filename) const {
    SparseMatrixCOO<Scalar> mcoo;
    this->_loc_data.at(id).convert(mcoo);
    mcoo.dump_to_matrix_market(filename);
  }
  // Dump local matrix to a matrix market file:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Dump matrix and DDM to files][Dump matrix and DDM to files:1]]
  void dump_matrix_and_ddm(const std::string& path) const {
    this->get_proc()->dump_subdomains(path);

    for (int sd_id : this->get_sd_ids()) {
      std::string fname = path;
      if (!(fname.empty()) && fname.back() != '/') {
        fname += '/';
      }
      fname += "composyx_local_matrix" + std::to_string(sd_id) + ".mtx";
      dump_to_file(sd_id, fname);
    }
  }
  // Dump matrix and DDM to files:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Local matrix type conversion][Local matrix type conversion:1]]
  template <class OtherM>
  [[nodiscard]] PartMatrix<OtherM>
  convert(std::function<OtherM(const Matrix&)> convert_fct) const {
    std::shared_ptr<Process> p = check_proc_ptr();
    PartMatrix<OtherM> out_distmat(p, this->_on_interface);
    for (auto& [sd_id, my_mat] : this->_loc_data) {
      out_distmat.add_subdomain(sd_id, convert_fct(my_mat));
    }
    return out_distmat;
  }
// Local matrix type conversion:1 ends here

// [[file:../../../org/composyx/part_data/PartMatrix.org::*Operators][Operators:1]]
// Scalar multiplication
#if defined(COMPOSYX_USE_MULTITHREAD)
  PartMatrix& operator*=(const Scalar& scal) {
    this->_proc->parallel_run([&, this](int, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        this->_loc_data[sd_id] *= scal;
      }
    });
    return *this;
  }
#else
  PartMatrix& operator*=(const Scalar& scal) {
    for (auto& my_mats : this->_loc_data) {
      my_mats.second *= scal;
    }
    return *this;
  }
#endif
#if defined(COMPOSYX_USE_MULTITHREAD)
  template <typename OtherLocMat>
  PartMatrix& operator+=(const PartMatrix<OtherLocMat>& mat) {
    this->_proc->parallel_run([&, this](int, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        const auto& their_mat = mat.get_local_matrix(sd_id); // Same matrix ID
        this->_loc_data[sd_id] += their_mat;
      }
    });

    return *this;
  }
#else
  template <typename OtherLocMat>
  PartMatrix& operator+=(const PartMatrix<OtherLocMat>& mat) {
    for (auto& my_mats : this->_loc_data) {
      const int& my_id = my_mats.first;
      const auto& their_mat = mat.get_local_matrix(my_id); // Same matrix ID
      my_mats.second += their_mat;
    }

    return *this;
  }
#endif
#if defined(COMPOSYX_USE_MULTITHREAD)
  template <typename OtherLocMat>
  PartMatrix& operator-=(const PartMatrix<OtherLocMat>& mat) {
    this->_proc->parallel_run([&, this](int, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        const auto& their_mat = mat.get_local_matrix(sd_id); // Same matrix ID
        this->_loc_data[sd_id] -= their_mat;
      }
    });

    return *this;
  }
#else
  template <typename OtherLocMat>
  PartMatrix& operator-=(const PartMatrix<OtherLocMat>& mat) {
    for (auto& my_mats : this->_loc_data) {
      const int& my_id = my_mats.first;
      const auto& their_mat = mat.get_local_matrix(my_id); // Same matrix ID
      my_mats.second -= their_mat;
    }

    return *this;
  }
#endif
  // Matrix multiplication
  PartMatrix& operator*=(const PartMatrix& mat) {
    auto copy = *this;
    (*this) = copy.matprod(mat);
    return *this;
  }
// Operators:1 ends here

// [[file:../../../org/composyx/part_data/PartMatrix.org::*Norms][Norms:1]]
// Sum the local frobenius norms squared to get the sums of the terms squared
// then square root it to get the global norm
#if defined(COMPOSYX_USE_MULTITHREAD)
  [[nodiscard]] Real frobenius_norm() const {
    std::shared_ptr<Process> p = check_proc_ptr();
    std::vector<Real> sendbufs;
    this->_proc->parallel_run([&, this](int, std::vector<int>& sds) {
      size_t i = 0;
      for (const auto& sd_id : sds) {
        Real fnorm = this->_loc_data.at(sd_id).frobenius_norm();
        {
          const std::lock_guard<std::mutex> lock_map(this->_map_mutex);
          sendbufs.push_back(fnorm * fnorm);
        }

        i++;
      }
    });
    /*
  for(auto& my_mats : this->_loc_data){
    Real fnorm = my_mats.second.frobenius_norm();
    sendbufs.push_back(fnorm * fnorm);
  }
  */
    Real sum_of_squared = p->subdomain_allreduce(sendbufs);
    return std::sqrt(sum_of_squared);
  }
#else
  [[nodiscard]] Real frobenius_norm() const {
    std::shared_ptr<Process> p = check_proc_ptr();
    std::vector<Real> sendbufs;
    for (auto& my_mats : this->_loc_data) {
      Real fnorm = my_mats.second.frobenius_norm();
      sendbufs.push_back(fnorm * fnorm);
    }
    Real sum_of_squared = p->subdomain_allreduce(sendbufs);
    return std::sqrt(sum_of_squared);
  }
#endif
  [[nodiscard]] Real norm() const { return frobenius_norm(); }
  // Norms:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Global and local indexing][Global and local indexing:1]]
  void local_to_global() {
    std::shared_ptr<Process> p = check_proc_ptr();

    std::map<int, std::vector<int>> glob_idx =
        p->get_global_indices(this->_on_interface);
    const int n_glob = p->get_n_glob(this->_on_interface);

    for (auto& [sd_id, mat] : this->_loc_data) {
      mat.reindex(glob_idx[sd_id], n_glob);
    }
  }

  void global_to_local() {
    std::shared_ptr<Process> p = check_proc_ptr();

    std::map<int, std::vector<int>> glob_idx =
        p->get_global_indices(this->_on_interface);

    for (auto& [sd_id, mat] : this->_loc_data) {
      const int n_loc = p->get_n_dofs(sd_id, this->_on_interface);
      std::vector<int> range(n_loc);
      int k = 0;
      for (int& i : range)
        i = k++;
      mat.reindex(range, glob_idx[sd_id], n_loc);
    }
  }
  // Global and local indexing:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*From interface to local indexing][From interface to local indexing:1]]
  template <typename SparseType =
                std::conditional_t<is_sparse<Matrix>::value, Matrix,
                                   typename sparse_type<Matrix>::type>>
  [[nodiscard]] PartMatrix<SparseType> interface_to_local() const {

    std::shared_ptr<Process> p = check_proc_ptr();
    COMPOSYX_ASSERT(
        this->_on_interface,
        "PartMatrix: Calling interface_to_local on a non interface matrix.");
    const std::map<int, std::vector<int>> interfaces = p->get_interface();

    PartMatrix<SparseType> out_mat(p, false);

    for (const auto& mapintrf : interfaces) {
      const int sd_id = mapintrf.first;
      const std::vector<int>& intrf = mapintrf.second;
      const int n_i = p->get_n_dofs(sd_id);

      SparseType spmat = this->get_local_matrix(sd_id);
      reindex(spmat, intrf, n_i);
      out_mat.add_subdomain(sd_id, std::move(spmat));
    }

    return out_mat;
  }
  // From interface to local indexing:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Centralization][Centralization:1]]
  template <class M = Matrix>
  [[nodiscard]]
  typename std::enable_if<std::is_same<M, SparseMatrixCOO<Scalar>>::value,
                          SparseMatrixCOO<Scalar>>::type
  centralize(const int root = 0) const {
    std::shared_ptr<Process> p = check_proc_ptr();
    if (!p->is_master())
      return SparseMatrixCOO<Scalar>();
    PartMatrix copy = (*this);
    copy.local_to_global();

    const int n_sd_tot = p->get_n_sd_total();
    SparseMatrixCOO<Scalar> out_mat;

    std::vector<std::array<int, 3>> sendbufs(this->_loc_data.size());
    std::vector<std::array<MPI_Request, 4>> reqs(this->_loc_data.size());

    int k = 0;
    for (const auto& [sd_id, mat] : copy._loc_data) {
      mat.isend(sendbufs[k], root, sd_id, reqs[k], p->master_comm(), n_sd_tot);
      k++;
    }
    if (p->rank_master() == root) {
      int n_glob = p->get_n_glob(this->_on_interface);
      SparseMatrixLIL<Scalar> lil_mat(n_glob, n_glob);
      for (int sd_id = 0; sd_id < n_sd_tot; ++sd_id) {
        SparseMatrixCOO<Scalar> mat;
        mat.recv(MPI_ANY_SOURCE, sd_id, p->master_comm(), n_sd_tot);
        lil_mat.insert(mat);
      }
      out_mat = lil_mat.to_coo();
    }

    k = 0;
    for (const auto& my_mats : copy._loc_data) {
      my_mats.second.wait(reqs[k]);
      ++k;
    }

    for (const auto& [sd_id, mat] : copy._loc_data) {
      (void)sd_id;
      out_mat.copy_properties(mat);
      break;
    }

    MMPI::barrier(p->master_comm());

    return out_mat;
  }

  template <class M = Matrix>
  [[nodiscard]]
  typename std::enable_if<std::is_same<M, SparseMatrixCSC<Scalar>>::value,
                          SparseMatrixCSC<Scalar>>::type
  centralize(const int root = 0) const {
    auto convert_fct = [](const Matrix& cscmat) { return cscmat.to_coo(); };
    PartMatrix<SparseMatrixCOO<Scalar>> coomat =
        this->convert<SparseMatrixCOO<Scalar>>(convert_fct);
    SparseMatrixCOO<Scalar> ctr = coomat.centralize(root);
    return ctr.to_csc();
  }

  template <class M = Matrix>
  [[nodiscard]]
  typename std::enable_if<std::is_same<M, DenseMatrix<Scalar>>::value,
                          DenseMatrix<Scalar>>::type
  centralize(const int root = 0) const {
    std::shared_ptr<Process> p = check_proc_ptr();
    if (!p->is_master())
      return DenseMatrix<Scalar>();

    int n_glob = p->get_n_glob(this->_on_interface);

    // Create output matrix
    DenseMatrix<Scalar> out_mat;
    if (p->rank_master() == root) {
      out_mat = DenseMatrix<Scalar>(n_glob, n_glob);
    }

    // Get global indices
    std::map<int, std::vector<int>> glob_idx =
        p->get_global_indices(this->_on_interface);

    // Centralize column vectors one by one
    for (int idx_col = 0; idx_col < n_glob; ++idx_col) {
      PartVector<Vector<Scalar>> col(p, this->_on_interface);
      for (const auto& [sd_id, mat] : this->_loc_data) {
        const std::vector<int>& g_idx = glob_idx.at(sd_id);
        const int loc_size = n_rows(mat);

        auto loc_col_ite = std::ranges::find(g_idx, idx_col);
        col.add_subdomain(sd_id, Vector<Scalar>(loc_size));
        // Check if the column is owned by the process
        if (loc_col_ite != g_idx.end()) {
          int loc_col_idx = g_idx.size() - (g_idx.end() - loc_col_ite);
          std::memcpy(col.get_local_data(sd_id).get_ptr(),
                      mat.get_vect_ptr(loc_col_idx), loc_size * sizeof(Scalar));
        } else { // Otherwise let zeros
          col.get_local_data(sd_id) *= Scalar{0};
        }
      }

      col.assemble();
      Vector<Scalar> col_ctr = col.centralize(root);

      // Copy into the output matrix for root
      if (p->rank_master() == root) {
        Scalar* ptr_col = out_mat.get_vect_ptr(idx_col);
        for (int i = 0; i < n_glob; ++i) {
          ptr_col[i] = col_ctr[i];
        }
      }
    }

    for (const auto& [sd_id, mat] : this->_loc_data) {
      (void)sd_id;
      out_mat.copy_properties(mat);
      break;
    }

    return out_mat;
  }
  // Centralization:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Assembly][Assembly:1]]
  void assemble() {
    std::shared_ptr<Process> p = check_proc_ptr();
    // Get storage type of the first available matrix
    MatrixStorage storage = MatrixStorage::full;
    for (const auto& mat : this->_loc_data) {
      storage = mat.second.get_storage_type();
      break;
    }
    p->subdomain_assemble<Scalar, Matrix>(this->_loc_data, this->_on_interface,
                                          Reduction::twoways_sum, storage);
  }
  // Assembly:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Diagonal extraction][Diagonal extraction:1]]
  /*  [[nodiscard]] PartMatrix<LocDiag> diag() const {
    std::shared_ptr<Process> p = check_proc_ptr();
    PartMatrix<LocDiag> out(p, this->_on_interface);

    for(const auto& [sd_id, mat] : this->_loc_data){
      out.add_subdomain(sd_id, diagonal(mat));
    }

    return out;
  }*/

  [[nodiscard]] PartVector<LocVect> diag_vect() const {
    std::shared_ptr<Process> p = check_proc_ptr();
    PartVector<LocVect> out(p, this->_on_interface);

    for (const auto& [sd_id, mat] : this->_loc_data) {
      out.add_subdomain(sd_id, diagonal_as_vector(mat));
    }

    return out;
  }
  // Diagonal extraction:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Transposition][Transposition:1]]
  [[nodiscard]] PartMatrix t() const {
    PartMatrix out = *this;
    out.apply_on_data([](Matrix& m) { m = transpose(m); });
    return out;
  }

  [[nodiscard]] PartMatrix h() const {
    PartMatrix out = *this;
    out.apply_on_data([](Matrix& m) { m = adjoint(m); });
    return out;
  }
  // Transposition:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Cast][Cast:1]]
  template <CPX_Scalar OtherScalar> [[nodiscard]] auto cast() const {
    using CastedLocMatrix = decltype(Matrix().template cast<OtherScalar>());
    using CastedPartMatrix = PartMatrix<CastedLocMatrix>;
    CastedPartMatrix m_out;
    m_out.initialize(this->_proc);

    m_out.template apply_on_data<Matrix>(
        *this, [](CastedLocMatrix& out, const Matrix& loc) {
          out = loc.template cast<OtherScalar>();
        });

    return m_out;
  }
  // Cast:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Checking matrix is defined on the interface][Checking matrix is defined on the interface:1]]
  [[nodiscard]] bool check_is_on_interface() const {
    for (const auto& [sd_id, loc_mat] : this->_loc_data) {
      const size_t M = this->_proc->get_n_dofs(sd_id, true);
      if (M != n_rows(loc_mat))
        return false;
    }
    return true;
  }
  // Checking matrix is defined on the interface:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Checking matrix is defined on the subdomain][Checking matrix is defined on the subdomain:1]]
  [[nodiscard]] bool check_is_on_subdomain() const {
    for (const auto& [sd_id, loc_mat] : this->_loc_data) {
      const size_t M = this->_proc->get_n_dofs(sd_id);
      if (M != n_rows(loc_mat))
        return false;
    }
    return true;
  }
  // Checking matrix is defined on the subdomain:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Getters][Getters:1]]
  inline int get_n_rows() const {
    COMPOSYX_ASSERT(this->_proc.get() != nullptr,
                    "PartMatrix::_proc is nullptr");
    return this->_proc->get_n_glob();
  }
  inline int get_n_cols() const {
    COMPOSYX_ASSERT(this->_proc.get() != nullptr,
                    "PartMatrix::_proc is nullptr");
    return this->_proc->get_n_glob();
  }

  [[nodiscard]] Matrix& get_local_matrix(const int sd_id) {
    return this->_loc_data.at(sd_id);
  }

  [[nodiscard]] const Matrix& get_local_matrix(const int sd_id) const {
    return this->_loc_data.at(sd_id);
  }
  // Getters:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Getters for DenseMatrix][Getters for DenseMatrix:1]]
  template <CPX_Integral Tint>
  [[nodiscard]] PartVector<Vector<Scalar>> get_vect_view(Tint j = 0) {
    PartVector<Vector<Scalar>> out_vect(this->get_proc(), this->_on_interface);
    out_vect.template apply_on_data<Matrix>(
        *this, [j](Vector<Scalar>& v, Matrix& m) { v = m.get_vect_view(j); });
    return out_vect;
  }

  template <CPX_Integral Tint>
  [[nodiscard]] friend auto get_vect_view(PartMatrix& m, Tint j = 0) {
    return m.get_vect_view(j);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] PartMatrix get_block_view(Tint i, Tint j, size_t m, size_t n) {
    PartMatrix out_mat(this->get_proc(), this->_on_interface);
    out_mat.template apply_on_data<Matrix>(
        *this, [i, j, m, n](Matrix& out, Matrix& me) {
          out = me.get_block_view(i, j, m, n);
        });
    return out_mat;
  }

  template <CPX_Integral Tint>
  [[nodiscard]] PartMatrix get_rows_view(Tint i_beg, Tint i_end) {
    PartMatrix out_mat(this->get_proc(), this->_on_interface);
    out_mat.template apply_on_data<Matrix>(
        *this, [i_beg, i_end](Matrix& out, Matrix& me) {
          out = me.get_rows_view(i_beg, i_end);
        });
    return out_mat;
  }

  template <CPX_Integral Tint>
  [[nodiscard]] PartMatrix get_columns_view(Tint j_beg, Tint j_end) {
    PartMatrix out_mat(this->get_proc(), this->_on_interface);
    out_mat.template apply_on_data<Matrix>(
        *this, [j_beg, j_end](Matrix& out, Matrix& me) {
          out = me.get_columns_view(j_beg, j_end);
        });
    return out_mat;
  }

  template <CPX_Integral Tint>
  [[nodiscard]] friend auto get_columns_view(PartMatrix& m, Tint j_beg,
                                             Tint j_end) {
    return m.get_columns_view(j_beg, j_end);
  }

  [[nodiscard]] friend size_t get_storage_bytes(const PartMatrix& mat, int) {
    size_t bytes = 0;
    for (const int sd_id : mat.get_sd_ids()) {
      bytes += n_rows(mat.get_local_vector(sd_id)) * sizeof(Scalar);
    }
    return bytes;
  }
  // Getters for DenseMatrix:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Setters][Setters:1]]
  void set_mat_vect_assemble(bool v) { _mat_vect_assemble = v; }
  // Setters:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Generation][Generation:1]]
  template <class Diag>
  // Partitioned Vector or Diagonal type that can be indexed with loc_v(k)
  void from_diagonal(const PartBase<Diag>& to_diag,
                     Real tolerance = Real{1e-14}) {
    this->_proc = to_diag.get_proc();
    this->_on_interface = to_diag.on_intrf();

    for (int sd : to_diag.get_sd_ids()) {
      const auto& loc_v = to_diag.get_local_data(sd);
      const int M = size(loc_v);
      std::vector<int> i(M);
      std::vector<int> j(M);
      std::vector<Scalar> v(M);
      for (int k = 0; k < M; ++k) {
        i[k] = k;
        j[k] = k;
        if (std::abs(loc_v[k]) > tolerance) {
          v[k] = loc_v(k);
        } else {
          v[k] = Scalar{0};
        }
      }
      this->_loc_data[sd] = Matrix();
      build_matrix(this->_loc_data[sd], M, M, M, &i[0], &j[0], &v[0]);
    }
  }
  // Generation:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Generation][Generation:2]]
  void identity(const bool on_intrf = false, const bool assembled = false) {
    COMPOSYX_ASSERT(this->_proc != nullptr, "PartMatrix::_proc is nullptr");
    PartVector<Vector<Scalar>> ones((this->_proc), on_intrf);
    ones.set_to_ones();
    if (!assembled) {
      ones.assemble();
      ones.apply([](Scalar& s) { s = Scalar{1} / s; });
    }
    this->from_diagonal(ones);
    this->set_on_intrf(on_intrf);
  }
  // Generation:2 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Addition on interface][Addition on interface:1]]
  template <class OtherLocMat>
  void add_on_interface(const PartMatrix<OtherLocMat>& other) {
    COMPOSYX_ASSERT(
        other.on_intrf(),
        "PartMatrix: Calling add_on_interface with a non interface matrix.");
    if (this->_on_interface) {
      (*this) += other;
    } else {
      auto on_intf = other.interface_to_local();
      (*this) += on_intf;
    }
  }
  // Addition on interface:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Display functions][Display functions:1]]
  void display_centralized(const std::string& name = "",
                           std::ostream& out = std::cout,
                           const int root = 0) const {
    std::shared_ptr<Process> p = check_proc_ptr();
    auto sp_mat = centralize(root);
    if (p->rank_master() != root)
      return;

    if (!name.empty())
      out << name;
    if (this->_on_interface)
      out << " (on interface)";
    out << '\n';
    sp_mat.display("", out);
  }
  // Display functions:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Display functions][Display functions:2]]
  void display_local(const std::string& name = "",
                     std::ostream& out = std::cout) const {
    if (!name.empty())
      out << name;
    if (this->_on_interface)
      out << " (on interface)";
    out << '\n';
    for (const auto& mats : this->_loc_data) {
      mats.second.display("", out);
    }
  }

  void display(const std::string& name = "",
               std::ostream& out = std::cout) const {
    display_local(name, out);
  }

  friend std::ostream& operator<<(std::ostream& out, const PartMatrix& p) {
    p.display("", out);
    return out;
  }
// Display functions:2 ends here

// [[file:../../../org/composyx/part_data/PartMatrix.org::*Matrix vector product][Matrix vector product:1]]
#if defined(COMPOSYX_USE_MULTITHREAD)
  template <class Vect>
  [[nodiscard]] PartVector<Vect> matvect(const PartVector<Vect>& vect) const {
    std::shared_ptr<Process> p = check_proc_ptr();
    PartVector<Vect> out_distv(p, vect.on_intrf());
    this->_proc->parallel_run([&, this](int, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        const Vect& loc_v = vect.get_local_vector(sd_id); // Same vector ID
        out_distv.add_subdomain(sd_id, this->_loc_data.at(sd_id) * loc_v);
      }
    });
    if (_mat_vect_assemble) {
      out_distv.assemble();
    }
    return out_distv;
  }
#else
  template <class Vect>
  [[nodiscard]] PartVector<Vect> matvect(const PartVector<Vect>& vect) const {
    std::shared_ptr<Process> p = check_proc_ptr();
    PartVector<Vect> out_distv(p, vect.on_intrf());
    for (const auto& [sd_id, loc_m] : this->_loc_data) {
      const Vect& loc_v = vect.get_local_vector(sd_id);
      out_distv.add_subdomain(sd_id, loc_m * loc_v);
    }

    if (_mat_vect_assemble) {
      out_distv.assemble();
    }
    return out_distv;
  }
#endif

//A.gemv(X, Y, alpha=1, beta=0, op='N')
//performs Y = alpha * op(A) * X + beta * Y
#if defined(COMPOSYX_USE_MULTITHREAD)
  template <class Vect>
  void gemv(const PartVector<Vect>& X, PartVector<Vect>& Y,
            const Scalar alpha = Scalar{1.0}, const Scalar beta = Scalar{0.0},
            char opA = 'N') const {
    std::shared_ptr<Process> p = check_proc_ptr();

    COMPOSYX_ASSERT(X.is_on_proc(*p), "Error PartMatrix::gemv: PartVector X "
                                      "not bind on the correct process.");
    if (beta != Scalar{0.0}) {
      COMPOSYX_ASSERT(Y.is_on_proc(*p), "Error PartMatrix::gemv: PartVector Y "
                                        "not bind on the correct process.");
    } else if (!Y.is_on_proc(*p)) {
      Y = X * Scalar{0.0};
    }
    this->_proc->parallel_run([&, this](int, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        const Vect& loc_x = X.get_local_vector(sd_id);
        Vect& loc_y = Y.get_local_vector(sd_id);
        loc_y.gemv(this->loc_data.at(sd_id), loc_x, alpha, beta, opA);
      }
    });

    if (_mat_vect_assemble) {
      Y.assemble();
    }
  }
#else
  template <class Vect>
  void gemv(const PartVector<Vect>& X, PartVector<Vect>& Y,
            const Scalar alpha = Scalar{1.0}, const Scalar beta = Scalar{0.0},
            char opA = 'N') const {
    std::shared_ptr<Process> p = check_proc_ptr();

    COMPOSYX_ASSERT(X.is_on_proc(*p), "Error PartMatrix::gemv: PartVector X "
                                      "not bind on the correct process.");
    if (beta != Scalar{0.0}) {
      COMPOSYX_ASSERT(Y.is_on_proc(*p), "Error PartMatrix::gemv: PartVector Y "
                                        "not bind on the correct process.");
    } else if (!Y.is_on_proc(*p)) {
      Y = X * Scalar{0.0};
    }

    for (const auto& [sd_id, loc_a] : this->_loc_data) {
      const Vect& loc_x = X.get_local_vector(sd_id);
      Vect& loc_y = Y.get_local_vector(sd_id);
      loc_y.gemv(loc_a, loc_x, alpha, beta, opA);
    }

    if (_mat_vect_assemble) {
      Y.assemble();
    }
  }
#endif

// Matvect where RHS is duplicate (the same for every subdomain)
// Careful: does not assemble by default
#if defined(COMPOSYX_USE_MULTITHREAD)
  template <class Vect>
  [[nodiscard]] PartVector<Vect>
  matvect_dupl(const Vect& vect, const bool assemble = false) const {
    std::shared_ptr<Process> p = check_proc_ptr();
    PartVector<Vect> out_distv(p, this->on_intrf());
    this->_proc->parallel_run([&, this](int, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        out_distv.add_subdomain(sd_id, this->_loc_data.at(sd_id) * vect);
      }
    });

    if (assemble) {
      out_distv.assemble();
    }
    return out_distv;
  }
#else
  template <class Vect>
  [[nodiscard]] PartVector<Vect>
  matvect_dupl(const Vect& vect, const bool assemble = false) const {
    std::shared_ptr<Process> p = check_proc_ptr();
    PartVector<Vect> out_distv(p, this->on_intrf());
    for (const auto& [sd_id, loc_m] : this->_loc_data) {
      out_distv.add_subdomain(sd_id, loc_m * vect);
    }

    if (assemble) {
      out_distv.assemble();
    }
    return out_distv;
  }
#endif
// Matrix vector product:1 ends here

// [[file:../../../org/composyx/part_data/PartMatrix.org::*Matrix matrix product][Matrix matrix product:1]]
// PartMatrix<Matrix> *= PartMatrix<OtherMatrix>
// Requires implementation of: Matrix *= OtherMatrix
#if defined(COMPOSYX_USE_MULTITHREAD)
  template <class OtherMatrix = Matrix>
  PartMatrix& matprod(const PartMatrix<OtherMatrix>& other,
                      bool assemble = true) {
    this->_proc->parallel_run([&, this](int, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        const OtherMatrix& rhs_loc_m = other.get_local_matrix(sd_id);
        this->_loc_data[sd_id] *= rhs_loc_m;
      }
    });

    if (assemble) {
      this->assemble();
    }
    return *this;
  }
#else
  template <class OtherMatrix = Matrix>
  PartMatrix& matprod(const PartMatrix<OtherMatrix>& other,
                      bool assemble = true) {
    for (auto& [sd_id, loc_m] : this->_loc_data) {
      const OtherMatrix& rhs_loc_m = other.get_local_matrix(sd_id);
      loc_m *= rhs_loc_m;
    }

    if (assemble) {
      this->assemble();
    }
    return *this;
  }
#endif
// PartMatrix<OtherMatrix> = PartMatrix<Matrix> * PartMatrix<OtherMatrix>
// Requires implementation of: OtherMatrix = Matrix * OtherMatrix
// Especially useful with diagonal matrix multiplication
#if defined(COMPOSYX_USE_MULTITHREAD)
  template <class OtherMatrix = Matrix>
  PartMatrix<OtherMatrix> matprod_left(PartMatrix<OtherMatrix> other,
                                       bool assemble = true) const {
    this->_proc->parallel_run([&, this](int, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        OtherMatrix& left_m = other.get_local_matrix(sd_id);
        left_m = this->_loc_data.at(sd_id) * left_m;
      }
    });
    if (assemble) {
      other.assemble();
    }
    return other;
  }
#else
  template <class OtherMatrix = Matrix>
  PartMatrix<OtherMatrix> matprod_left(PartMatrix<OtherMatrix> other,
                                       bool assemble = true) const {
    for (const auto& [sd_id, loc_m] : this->_loc_data) {
      OtherMatrix& left_m = other.get_local_matrix(sd_id);
      left_m = loc_m * left_m;
    }

    if (assemble) {
      other.assemble();
    }
    return other;
  }
#endif
  // Matrix matrix product:1 ends here

  // [[file:../../../org/composyx/part_data/PartMatrix.org::*Out of class operators][Out of class operators:1]]
}; // class PartMatrix
// Out of class operators:1 ends here

// [[file:../../../org/composyx/part_data/PartMatrix.org::*Out of class operators][Out of class operators:2]]
template <CPX_Matrix LocMatrix, CPX_Scalar Scalar,
          typename = std::enable_if_t<std::is_same<
              Scalar, typename scalar_type<LocMatrix>::type>::value>>
[[nodiscard]]
PartMatrix<LocMatrix> operator*(PartMatrix<LocMatrix> mat, const Scalar s) {
  mat *= s;
  return mat;
}

template <CPX_Matrix LocMatrix, CPX_Scalar Scalar,
          typename = std::enable_if_t<std::is_same<
              Scalar, typename scalar_type<LocMatrix>::type>::value>>
[[nodiscard]]
PartMatrix<LocMatrix> operator*(Scalar s, const PartMatrix<LocMatrix>& mat) {
  return mat * s;
}

template <CPX_Matrix LocMatrix, CPX_Vector LocVect>
  requires requires(LocMatrix m, LocVect v) {
    { m* v } -> std::convertible_to<LocVect>;
  }
[[nodiscard]] PartVector<LocVect> operator*(const PartMatrix<LocMatrix>& mat,
                                            const PartVector<LocVect>& vect) {
  return mat.template matvect<LocVect>(vect);
}

template <CPX_Matrix LocMatrix,
          CPX_Vector LocVect = typename vector_type<LocMatrix>::type>
  requires requires(LocMatrix m, LocVect v) {
    { m* v } -> std::convertible_to<LocVect>;
  }
[[nodiscard]] PartVector<LocVect> operator*(const PartMatrix<LocMatrix>& mat,
                                            const LocVect& vect) {
  return mat.matvect_dupl(vect);
}

template <CPX_Matrix LocMatrix, CPX_Matrix OtherMatrix = LocMatrix>
[[nodiscard]]
PartMatrix<LocMatrix> operator+(PartMatrix<LocMatrix> A,
                                const PartMatrix<OtherMatrix>& B) {
  A += B;
  return A;
}

template <CPX_Matrix LocMatrix, CPX_Matrix OtherMatrix = LocMatrix>
[[nodiscard]]
PartMatrix<LocMatrix> operator-(PartMatrix<LocMatrix> A,
                                const PartMatrix<OtherMatrix>& B) {
  A -= B;
  return A;
}

template <CPX_Matrix LocMatrix, CPX_Matrix OtherMatrix = LocMatrix>
[[nodiscard]]
PartMatrix<LocMatrix> operator*(PartMatrix<LocMatrix> A,
                                const PartMatrix<OtherMatrix>& B) {
  return A.template matprod<OtherMatrix>(B);
}
// Out of class operators:2 ends here

// [[file:../../../org/composyx/part_data/PartMatrix.org::*Load subdomains, matrices and vectors from matrix market files][Load subdomains, matrices and vectors from matrix market files:1]]
template <typename Matrix, typename Scalar = typename Matrix::scalar_type>
void load_subdomains_and_data(std::string path_to_part, const int n_subdomains,
                              std::shared_ptr<Process> p, PartMatrix<Matrix>& A,
                              PartVector<Scalar>& RHS) {
  p->load_subdomains(path_to_part);

  if (!(path_to_part.empty()) && path_to_part.back() != '/')
    path_to_part += '/';

  // Right hand side vector
  COMPOSYX_ASSERT(RHS.is_on_proc(*p),
                  "Error: PartVector not bind on the correct process.");
  for (int sd_id = 0; sd_id < n_subdomains; ++sd_id) {
    if (p->owns_subdomain(sd_id)) {
      std::string fullpath = path_to_part + "composyx_local_rhs" +
                             std::to_string(sd_id + 1) + ".mtx";
      RHS.load_from_file(sd_id, fullpath);
    }
  }

  // Input matrix
  COMPOSYX_ASSERT(A.is_on_proc(*p),
                  "Error: PartMatrix not bind on the correct process.");
  for (int sd_id = 0; sd_id < n_subdomains; ++sd_id) {
    if (p->owns_subdomain(sd_id)) {
      std::string fullpath = path_to_part + "composyx_local_matrix" +
                             std::to_string(sd_id + 1) + ".mtx";
      A.load_from_file(sd_id, fullpath);
    }
  }
}
// Load subdomains, matrices and vectors from matrix market files:1 ends here

// [[file:../../../org/composyx/part_data/PartMatrix.org::*Dump subdomains, matrices and vectors to matrix market files][Dump subdomains, matrices and vectors to matrix market files:1]]
template <typename Matrix, typename Scalar = typename Matrix::scalar_type>
void dump_subdomains_and_data(std::string path_to_part, const int n_subdomains,
                              std::shared_ptr<Process> p, PartMatrix<Matrix>& A,
                              PartVector<Scalar>& RHS) {
  p->load_subdomains(path_to_part);

  if (!(path_to_part.empty()) && path_to_part.back() != '/')
    path_to_part += '/';

  // Right hand side vector
  COMPOSYX_ASSERT(RHS.is_on_proc(*p),
                  "Error: PartVector not bind on the correct process.");
  for (int sd_id = 0; sd_id < n_subdomains; ++sd_id) {
    if (p->owns_subdomain(sd_id)) {
      std::string fullpath = path_to_part + "composyx_local_rhs" +
                             std::to_string(sd_id + 1) + ".mtx";
      RHS.dump_to_file(sd_id, fullpath);
    }
  }

  // Input matrix
  COMPOSYX_ASSERT(A.is_on_proc(*p),
                  "Error: PartMatrix not bind on the correct process.");
  for (int sd_id = 0; sd_id < n_subdomains; ++sd_id) {
    if (p->owns_subdomain(sd_id)) {
      std::string fullpath = path_to_part + "composyx_local_matrix" +
                             std::to_string(sd_id + 1) + ".mtx";
      A.dump_to_file(sd_id, fullpath);
    }
  }
}
// Dump subdomains, matrices and vectors to matrix market files:1 ends here

// [[file:../../../org/composyx/part_data/PartMatrix.org::*Bâton loader][Bâton loader:1]]
template <typename Matrix, typename Scalar = typename Matrix::scalar_type>
void load_baton_subdomains(std::string path_to_part, const int n_subdomains,
                           std::shared_ptr<Process> p, PartMatrix<Matrix>& A,
                           PartVector<Scalar>& RHS) {
  p->load_baton_subdomains(path_to_part);

  if (!(path_to_part.empty()) && path_to_part.back() != '/')
    path_to_part += '/';

  // Right hand side vector
  COMPOSYX_ASSERT(RHS.is_on_proc(*p),
                  "Error: PartVector not bind on the correct process.");
  COMPOSYX_ASSERT(A.is_on_proc(*p),
                  "Error: PartMatrix not bind on the correct process.");
#if defined(COMPOSYX_USE_MULTITHREAD)
  p->parallel_run([&](int, std::vector<int>& sds) {
    for (const auto& sd_id : sds) {
      if (p->owns_subdomain(sd_id)) {
        std::string piece_to_load("middle");
        if (sd_id == 0)
          piece_to_load = std::string("first");
        if (sd_id == n_subdomains - 1)
          piece_to_load = std::string("last");

        std::string fullpath = path_to_part + std::string("baton_rhs_") +
                               piece_to_load + std::string(".mtx");
        RHS.load_from_file(sd_id, fullpath);

        fullpath = path_to_part + std::string("baton_matrix_") + piece_to_load +
                   std::string(".mtx");

        A.load_from_file(sd_id, fullpath);
      }
    }
  });
#else
  for (int sd_id = 0; sd_id < n_subdomains; ++sd_id) {
    if (p->owns_subdomain(sd_id)) {

      std::string piece_to_load("middle");
      if (sd_id == 0)
        piece_to_load = std::string("first");
      if (sd_id == n_subdomains - 1)
        piece_to_load = std::string("last");

      std::string fullpath = path_to_part + std::string("baton_rhs_") +
                             piece_to_load + std::string(".mtx");
      RHS.load_from_file(sd_id, fullpath);

      fullpath = path_to_part + std::string("baton_matrix_") + piece_to_load +
                 std::string(".mtx");
      A.load_from_file(sd_id, fullpath);
    }
  }
#endif
}
// Bâton loader:1 ends here

// [[file:../../../org/composyx/part_data/PartMatrix.org::*GEMV functions][GEMV functions:1]]
// Y <- alpha op(A) X + beta Y
// Y and X part vectors, A part matrix
template <typename Matrix, typename Vect, typename Scalar>
void gemv(const PartMatrix<Matrix>& A, const PartVector<Vect>& X,
          PartVector<Vect>& Y, const Scalar alpha = Scalar{1.0},
          const Scalar beta = Scalar{0.0}, char opA = 'N') {
  A.gemv(X, Y, alpha, beta, opA);
}
// GEMV functions:1 ends here

// [[file:../../../org/composyx/part_data/PartMatrix.org::*Interface functions][Interface functions:1]]
template <typename Matrix>
[[nodiscard]] inline int n_rows(const PartMatrix<Matrix>& mat) {
  return mat.get_n_rows();
}

template <typename Matrix>
[[nodiscard]] inline int n_cols(const PartMatrix<Matrix>& mat) {
  return mat.get_n_cols();
}

template <typename Matrix>
[[nodiscard]] inline decltype(auto) diagonal(const PartMatrix<Matrix>& mat) {
  return mat.diag();
}

template <typename Matrix>
[[nodiscard]] inline decltype(auto)
diagonal_as_vector(const PartMatrix<Matrix>& mat) {
  return mat.diag_vect();
}

template <typename Matrix>
[[nodiscard]] PartMatrix<Matrix> transpose(const PartMatrix<Matrix>& mat) {
  return mat.t();
}

template <typename Matrix>
[[nodiscard]] PartMatrix<Matrix> adjoint(const PartMatrix<Matrix>& mat) {
  return mat.h();
}
// Interface functions:1 ends here

// [[file:../../../org/composyx/part_data/PartMatrix.org::*Traits][Traits:1]]
template <typename Matrix>
struct is_distributed<PartMatrix<Matrix>> : public std::true_type {};

template <typename Matrix>
struct vector_type<PartMatrix<Matrix>> : public std::true_type {
  using loc_vect_type = typename vector_type<Matrix>::type;
  using type = PartVector<loc_vect_type>;
};

template <typename Matrix>
struct scalar_type<PartMatrix<Matrix>> : public std::true_type {
  using type = typename scalar_type<Matrix>::type;
};

template <typename Matrix>
struct dense_type<PartMatrix<Matrix>> : public std::true_type {
  using type = PartMatrix<typename dense_type<Matrix>::type>;
};

template <typename Matrix>
struct sparse_type<PartMatrix<Matrix>> : public std::true_type {
  using type = PartMatrix<typename sparse_type<Matrix>::type>;
};

template <typename Matrix>
struct local_type<PartMatrix<Matrix>> : public std::true_type {
  using type = Matrix;
};
// Traits:1 ends here

// [[file:../../../org/composyx/part_data/PartMatrix.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
