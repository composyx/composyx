// [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Header][Header:1]]
#pragma once
#include <map>
#include <random>
#include <set>
#include <ios>
#include <iomanip>
#include "composyx/loc_data/DenseMatrix.hpp"
#if defined(COMPOSYX_USE_CHAMELEON)
#include "composyx/utils/Chameleon.hpp"
#endif

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Concepts][Concepts:1]]
template <typename F>
concept DataMapFunction = requires(F f, int i, int j) {
  { f(i, j) } -> std::convertible_to<int>;
};

template <typename F>
concept TaskMapFunction = requires(F f, int i, int j, int k) {
  { f(i, j, k) } -> std::convertible_to<int>;
};
// Concepts:1 ends here

// [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Canonical datamap][Canonical datamap:1]]
namespace canonical_datamap {
struct OneD_row {
  OneD_row(const int nprocs) : NP(nprocs) {
    COMPOSYX_ASSERT(nprocs > 0, "canonical_datamap 1D row with nprocs <= 0");
    COMPOSYX_ASSERT(nprocs == MMPI::size(),
                    "canonical_datamap 1D row with nprocs != MMPI::size()");
  }
  auto operator()(int i, int) const { return i % NP; }

private:
  int NP;

public:
  auto getNP() { return NP; }
};

struct OneD_col {
  OneD_col(const int nprocs) : NP(nprocs) {
    COMPOSYX_ASSERT(nprocs > 0, "canonical_datamap 1D column with nprocs <= 0");
    COMPOSYX_ASSERT(nprocs == MMPI::size(),
                    "canonical_datamap 1D column with nprocs != MMPI::size()");
  }
  auto operator()(int, int j) const { return j % NP; }

private:
  int NP;

public:
  auto getNP() { return NP; }
};

struct TwoD_block_cyclic {
  TwoD_block_cyclic(const int p, const int q) : P(p), Q(q) {
    COMPOSYX_ASSERT(P > 0,
                    "canonical_datamap 2D block cyclic grid (PxQ) with P <= 0");
    COMPOSYX_ASSERT(Q > 0,
                    "canonical_datamap 2D block cyclic grid (PxQ) with Q <= 0");
    COMPOSYX_ASSERT(P * Q == MMPI::size(),
                    "canonical_datamap 2D block cyclic grid (PxQ) with P*Q != "
                    "MMPI::size()");
  }
  auto operator()(int i, int j) const { return (i % P) * Q + (j % Q); }

private:
  int P;
  int Q;

public:
  auto getP() { return P; }
  auto getQ() { return Q; }
};

class pseudorandom {
  int nprocs;
  int seed;
  int M;

public:
  pseudorandom(int lnprocs, int lseed = 1337, int lM = 10000)
      : nprocs{lnprocs}, seed{lseed}, M{lM} {}

  int operator()(int i, int j) {
    std::mt19937 gen(seed + j * M + i);
    std::uniform_int_distribution<int> dis(0, nprocs - 1);
    return dis(gen);
  }
};
} // namespace canonical_datamap
// Canonical datamap:1 ends here

// [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*A distribution matrix][A distribution matrix:1]]
using std::make_pair;

class DistributionMatrix {
  // A basic column-major int matrix
  std::vector<int> _matrix;
  bool boundary_check = false;
  size_t m;
  size_t n;

  template <CPX_Integral I1, CPX_Integral I2 = I1>
  void check_boundaries(I1 i, I2 j) const {
    if (static_cast<size_t>(i) >= m) {
      std::cerr << "i: " << i << ", M: " << m << '\n';
      COMPOSYX_ASSERT(static_cast<size_t>(i) < m,
                      "DistributionMatrix(i, j): i out of bounds");
    }
    if (static_cast<size_t>(j) >= n) {
      std::cerr << "j: " << j << ", N: " << n << '\n';
      COMPOSYX_ASSERT(static_cast<size_t>(j) < n,
                      "DistributionMatrix(i, j): j out of bounds");
    }
  }

public:
  DistributionMatrix(size_t mm, size_t nn) : m{mm}, n{nn} {
    _matrix = std::vector<int>(m * n);
  }
  DistributionMatrix(const DistributionMatrix&) = default;
  DistributionMatrix(DistributionMatrix&&) = default;
  DistributionMatrix() : DistributionMatrix(0, 0) {}

  DistributionMatrix& operator=(const DistributionMatrix&) = default;
  DistributionMatrix& operator=(DistributionMatrix&&) = default;

  template <CPX_Integral I1, CPX_Integral I2 = I1>
  [[nodiscard]]
  int& operator()(I1 i, I2 j) {
    if (boundary_check) {
      check_boundaries(i, j);
    }
    return _matrix[j * m + i];
  }
  template <CPX_Integral I1, CPX_Integral I2 = I1>
  [[nodiscard]]
  const int& operator()(I1 i, I2 j) const {
    if (boundary_check) {
      check_boundaries(i, j);
    }
    return _matrix[j * m + i];
  }

  size_t n_rows() const { return m; }
  size_t n_cols() const { return n; }
  void set_boundary_check(bool b) { boundary_check = b; }
};
// A distribution matrix:1 ends here

// [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*A datamap class][A datamap class:1]]
template <DataMapFunction MapFunc> class Datamap {
  std::shared_ptr<MapFunc> _datamap;
  int _offset_row = 0;
  int _offset_col = 0;
  int _dim_row;
  int _dim_col;

public:
  Datamap(std::shared_ptr<MapFunc> dmap, int dim_row, int dim_col, int i = 0,
          int j = 0)
      : _datamap{dmap}, _offset_row{i}, _offset_col{j}, _dim_row{dim_row},
        _dim_col{dim_col} {}

  int operator()(int i, int j) const {
    int ioff = i + _offset_row;
    int joff = j + _offset_col;
    COMPOSYX_ASSERT(ioff >= 0, "Datamap::(i,j) i < 0");
    COMPOSYX_ASSERT(joff >= 0, "Datamap::(i,j) j < 0");
    COMPOSYX_ASSERT(ioff < _dim_row, "Datamap::(i,j) i >= dim_row");
    COMPOSYX_ASSERT(joff < _dim_col, "Datamap::(i,j) j >= dim_col");
    return (*_datamap)(ioff, joff);
  }

  Datamap(const Datamap&) = default;
  Datamap(Datamap&&) = default;
  Datamap& operator=(const Datamap&) = default;
  Datamap& operator=(Datamap&&) = default;

  MapFunc get_func() const { return *_datamap; }

  friend Datamap create_datamap_with_offset(Datamap d, int off_row,
                                            int off_col) {
    d._offset_row += off_row;
    d._offset_col += off_col;
    return d;
  }

  int get_dim_row() const { return _dim_row; }
  int get_dim_col() const { return _dim_col; }
};
// A datamap class:1 ends here

// [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Canonical taskmaps][Canonical taskmaps:1]]
namespace canonical_taskmap {
template <DataMapFunction MapFunc>
[[nodiscard]]
auto A_stationary(const Datamap<MapFunc>& d) {
  return [d](int i, int, int k) { return d(i, k); };
}

template <DataMapFunction MapFunc>
[[nodiscard]]
auto B_stationary(const Datamap<MapFunc>& d) {
  return [d](int, int j, int k) { return d(j, k); };
}

template <DataMapFunction MapFunc>
[[nodiscard]]
auto C_stationary(const Datamap<MapFunc>& d) {
  return [d](int i, int j, int) { return d(i, j); };
}

class pseudorandom {
  int nprocs;
  int seed;
  int M;
  int N;

public:
  pseudorandom(int lnprocs, int lseed = 1337, int lM = 1000, int lN = 1000)
      : nprocs{lnprocs}, seed{lseed}, M{lM}, N{lN} {}

  int operator()(int i, int j, int k) {
    std::mt19937 gen(seed + k * M * N + j * M + i);
    std::uniform_int_distribution<int> dis(0, nprocs - 1);
    return dis(gen);
  }
};
} // namespace canonical_taskmap
// Canonical taskmaps:1 ends here

// [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Class definition and attributes][Class definition and attributes:1]]
enum class gemm_algo { serialize_gemm, dist_gemm, chameleon_gemm };

template <typename LocMatrix, DataMapFunction MapFunc> class PartDenseMatrix {

  MPI_Comm _comm;
  int _rank;
  // Matrices indexed locally
  std::vector<LocMatrix> _local_matrices;
  // Global indices of the matrices (local index -> global position (i, j))
  std::vector<std::pair<int, int>> _loc2glob;
  // Local indices of the matrices (global position (i, j) -> local index)
  std::map<std::pair<int, int>, int> _glob2loc;
  // Datamap: (global position (i, j) -> MPI rank owning)
  Datamap<MapFunc> _datamap;

  // Cuts format: [0, cut_1, cut_2, ..., glob_size]
  std::vector<size_t> _cuts_rows;
  std::vector<size_t> _cuts_cols;

  using Scalar = typename LocMatrix::value_type;
  using Real = typename arithmetic_real<Scalar>::type;

public:
  using scalar_type = Scalar;
  using real_type = Real;
  using local_type = LocMatrix;
  using data_map_type = MapFunc;
  static inline gemm_algo _gemm_algo = gemm_algo::serialize_gemm;

private:
  void _compute_cuts() {

    std::array<size_t, 2> max_ij{0, 0};
    for (const auto& [pos, _] : _glob2loc) {
      max_ij[0] = std::max(max_ij[0], static_cast<size_t>(pos.first));
      max_ij[1] = std::max(max_ij[1], static_cast<size_t>(pos.second));
    }
    std::array<size_t, 2> ncuts;
    MMPI::allreduce(max_ij.data(), ncuts.data(), 2, MPI_MAX, _comm);

    std::vector<size_t> dims_i(ncuts[0] + 1);
    std::vector<size_t> dims_j(ncuts[1] + 1);

    for (const auto& [pos, loc_idx] : _glob2loc) {
      const auto& [ib, jb] = pos;
      if (ib == 0)
        dims_j[jb] = n_cols(_local_matrices[loc_idx]);
      if (jb == 0)
        dims_i[ib] = n_rows(_local_matrices[loc_idx]);
    }

    _cuts_rows = std::vector<size_t>(ncuts[0] + 2);
    _cuts_cols = std::vector<size_t>(ncuts[1] + 2);
    _cuts_rows[0] = 0;
    _cuts_cols[0] = 0;

    MMPI::allreduce(dims_i.data(), &_cuts_rows[1], ncuts[0] + 1, MPI_MAX,
                    _comm);
    MMPI::allreduce(dims_j.data(), &_cuts_cols[1], ncuts[1] + 1, MPI_MAX,
                    _comm);
    for (size_t k = 0; k <= ncuts[0]; ++k)
      _cuts_rows[k + 1] += _cuts_rows[k];
    for (size_t k = 0; k <= ncuts[1]; ++k)
      _cuts_cols[k + 1] += _cuts_cols[k];
  }

public:
  PartDenseMatrix(MPI_Comm comm,
                  const std::map<std::pair<int, int>, LocMatrix>& mat_map,
                  Datamap<MapFunc> datamap)
      : _comm{comm}, _datamap{datamap} {
    _rank = MMPI::rank(comm);
    int k = 0;
    for (const auto& [pos, mat] : mat_map) {
      _local_matrices.push_back(mat);
      _loc2glob.push_back(pos);
      _glob2loc[pos] = k++;
    }
    _compute_cuts();
  }

  PartDenseMatrix(MPI_Comm comm,
                  std::map<std::pair<int, int>, LocMatrix>&& mat_map,
                  Datamap<MapFunc> datamap)
      : _comm{comm}, _datamap{datamap} {
    _rank = MMPI::rank(comm);
    size_t n_loc_mat = mat_map.size();
    _local_matrices = std::vector<LocMatrix>(n_loc_mat);
    _loc2glob = std::vector<std::pair<int, int>>(n_loc_mat);

    int k = 0;
    for (auto&& [pos, mat] : mat_map) {
      _local_matrices.at(k) = std::move(mat);
      _loc2glob.at(k) = std::move(pos);
      _glob2loc[pos] = k++;
    }
    _compute_cuts();
  }

  PartDenseMatrix(const PartDenseMatrix&) = default;
  PartDenseMatrix(PartDenseMatrix&&) = default;
  PartDenseMatrix& operator=(const PartDenseMatrix&) = default;
  PartDenseMatrix& operator=(PartDenseMatrix&&) = default;
  // Class definition and attributes:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*With dense matrices][With dense matrices:1]]
private:
  // To send blocks A and B
  // is_A == true: A(i, k) -> i1 = i, i2 = k, i3 = j
  // is_A == false = 2: B(k, j) -> i1 = k, i2 = j, i3 = i
  // dim_size: size of the other dimension (in gemm): N for A and M for B
  template <TaskMapFunction TMapFunc, typename TagFunc>
  void isend_block(const bool is_A, TMapFunc ijk_taskmap, const int dim_size,
                   std::vector<MPI_Request>& reqs, TagFunc& tag) const {
    const int rank = MMPI::rank(_comm);
    using namespace std::placeholders;
    std::function<int(int, int, int)> tm = ijk_taskmap;

    if (is_A) {
      tm = std::bind(ijk_taskmap, _1, _3, _2);
    } else {
      tm = std::bind(ijk_taskmap, _3, _2, _1);
    }

    // Isend matrix M (i1, i2) -> t (j1, j2, j3)
    for (auto& [pos, loc_idx] : _glob2loc) {
      const auto& [i1, i2] = pos;
      auto& lm = _local_matrices.at(loc_idx);
      std::set<int> sent_to;

      for (int i3 = 0; i3 < dim_size; i3++) {
        // isend to taskmap(i, j, k)
        int dest = tm(i1, i2, i3);
        if (dest == rank)
          continue; // Avoid self send
        if (sent_to.contains(dest))
          continue; // Avoid duplicate send
        int count = static_cast<int>(n_rows(lm) * n_cols(lm));
        auto& req_ref = reqs.emplace_back();
        MMPI::isend(get_ptr(lm), count, tag(i1, i2), dest, req_ref, _comm);
        sent_to.insert(dest);
      }
    }
  }

public:
  template <TaskMapFunction TMapFunc, typename LM1, DataMapFunction D1,
            typename LM2, DataMapFunction D2>
  friend void dense_dist_gemm(const PartDenseMatrix<LM1, D1>& A,
                              const PartDenseMatrix<LM2, D2>& B,
                              PartDenseMatrix& C, TMapFunc taskmap,
                              Scalar alpha = Scalar{1},
                              Scalar beta = Scalar{0}) {
    COMPOSYX_ASSERT(A.get_comm() == B.get_comm(),
                    "PartDenseMatrix::dense_dist_gemm A and B must have same "
                    "MPI communicator");
    COMPOSYX_ASSERT(A.get_comm() == C.get_comm(),
                    "PartDenseMatrix::dense_dist_gemm A and C must have same "
                    "MPI communicator");
    const MPI_Comm& comm = A.get_comm();
    const int rank = MMPI::rank(comm);

    size_t M_blocks = C.get_n_rows_block();
    size_t N_blocks = C.get_n_cols_block();
    size_t K_blocks = A.get_n_cols_block();

    COMPOSYX_DIM_ASSERT(
        M_blocks, A.get_n_rows_block(),
        "PartDenseMatrix::dense_dist_gemm rows bloc (A) != rows bloc (C)");
    COMPOSYX_DIM_ASSERT(
        K_blocks, B.get_n_rows_block(),
        "PartDenseMatrix::dense_dist_gemm cols bloc (A) != rows bloc (B)");
    COMPOSYX_DIM_ASSERT(
        N_blocks, B.get_n_cols_block(),
        "PartDenseMatrix::dense_dist_gemm cols bloc (C) != cols bloc (B)");

    auto tag_A = [&](int i, int k) { return i * K_blocks + k + 1; };
    const int offset_A = tag_A(M_blocks, K_blocks);
    auto tag_B = [&](int k, int j) { return offset_A + k * N_blocks + j; };
    const int offset_B = tag_B(K_blocks, N_blocks);
    auto tag_C = [&](int i, int j, int k) {
      return offset_B + i * N_blocks * K_blocks + j * K_blocks + k;
    };
    // This might overflow for a tag int 32 ???

    C *= beta;

    std::vector<MPI_Request> reqs_A;
    A.isend_block(true, taskmap, N_blocks, reqs_A, tag_A);
    std::vector<MPI_Request> reqs_B;
    B.isend_block(false, taskmap, M_blocks, reqs_B, tag_B);

    // Receive map: (i, k) -> (Block_Aik)
    // (same for Bkj)
    using MapBlocks = std::map<std::pair<int, int>, LocMatrix>;
    MapBlocks Aik_received;
    MapBlocks Bkj_received;

    auto get_block = [rank, comm](int i1, int i2, int tag,
                                  MapBlocks& mat_received,
                                  const PartDenseMatrix& mat) {
      LocMatrix* mat_ptr = nullptr;
      int src = mat.datamap(i1, i2);

      if (src == rank) {
        mat_ptr = const_cast<LocMatrix*>(&(mat.get_local_matrix(i1, i2)));
      } else {
        auto i1i2 = make_pair(i1, i2);
        if (mat_received.contains(i1i2)) {
          mat_ptr = &mat_received.at(i1i2);
        } else {
          int nrows_mat = mat.get_blocksize_row(i1);
          int ncols_mat = mat.get_blocksize_col(i2);
          auto [aik_ite, _] =
              mat_received.emplace(i1i2, LocMatrix(nrows_mat, ncols_mat));
          mat_ptr = &((*aik_ite).second);
          MMPI::recv(get_ptr(*mat_ptr), nrows_mat * ncols_mat, tag, src, comm);
        }
      }

      return mat_ptr;
    };

    MMPI::barrier(comm);

    // The temporary matrices to send must be stored in an array
    // where pointer addresses remain constant
    // i.d. we can't use simply use Cijk_blocks_to_send.emplace_back
    size_t n_C_send = 0;
    for (int i = 0; i < static_cast<int>(M_blocks); i++) {
      for (int j = 0; j < static_cast<int>(N_blocks); j++) {
        if (C.datamap(i, j) != rank) {
          for (int k = 0; k < static_cast<int>(K_blocks); k++) {
            if (taskmap(i, j, k) == rank) {
              n_C_send++;
            }
          }
        }
      }
    }

    std::vector<MPI_Request> reqs_C(n_C_send);
    std::vector<LocMatrix> Cijk_blocks_to_send(n_C_send);
    int idx_C_send = 0;

    // Task loop: receive blocks A(i, k) and B(k, j), multiply and send to C(i, j)
    for (int i = 0; i < static_cast<int>(M_blocks); i++) {
      for (int j = 0; j < static_cast<int>(N_blocks); j++) {
        for (int k = 0; k < static_cast<int>(K_blocks); k++) {
          if (taskmap(i, j, k) == rank) {
            // Recv A(i, k)
            LocMatrix* Aik_ptr = get_block(i, k, tag_A(i, k), Aik_received, A);

            // Recv B(k, j)
            LocMatrix* Bkj_ptr = get_block(k, j, tag_B(k, j), Bkj_received, B);

            // Send to C(i, j)
            int dest_C = C.datamap(i, j);
            if (dest_C == rank) {
              auto& Cij = C.get_local_matrix(i, j);
              Cij += (alpha * (*Aik_ptr) * (*Bkj_ptr));
            } else {
              auto& req_ref = reqs_C.at(idx_C_send);
              auto& aCijk = Cijk_blocks_to_send.at(idx_C_send);
              idx_C_send++;

              aCijk = alpha * (*Aik_ptr) * (*Bkj_ptr);
              int count = static_cast<int>(n_rows(aCijk) * n_cols(aCijk));
              MMPI::isend(get_ptr(aCijk), count, tag_C(i, j, k), dest_C,
                          req_ref, comm);
            }
          }
        } // i
      } // j
    } // k

    MMPI::waitall(reqs_A);
    reqs_A.clear();
    MMPI::waitall(reqs_B);
    reqs_B.clear();

    // Recv aCijk blocks and sum on Cij
    for (auto& [pos, loc_idx] : C.get_glob2loc()) {
      const auto& [i, j] = pos;
      auto& Cij = C._local_matrices.at(loc_idx);
      int nrows_Cij = C.get_blocksize_row(i);
      int ncols_Cij = C.get_blocksize_col(j);
      int count = nrows_Cij * ncols_Cij;

      LocMatrix aCijk(nrows_Cij, ncols_Cij);

      for (int k = 0; k < static_cast<int>(K_blocks); k++) {
        int src = taskmap(i, j, k);
        if (src == rank)
          continue;

        MMPI::recv(get_ptr(aCijk), count, tag_C(i, j, k), src, comm);
        Cij += aCijk;
      }
    }

    MMPI::waitall(reqs_C);
  }
  // With dense matrices:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Gemm with serialization][Gemm with serialization:1]]
private:
  template <typename LM>
  static int serialize_send_buf(const LM& Bloc, int idx1, int idx2,
                                std::vector<char>& send_buffer) {
    // Serialized data
    std::vector<char> ser_data = Bloc.serialize();
    // Metadata : idx1, idx2
    std::array<int, 2> meta_data{idx1, idx2};

    send_buffer = std::vector<char>(sizeof(meta_data) + ser_data.size());

    std::memcpy(send_buffer.data(), meta_data.data(), sizeof(meta_data));
    std::memcpy(send_buffer.data() + sizeof(meta_data), ser_data.data(),
                ser_data.size());

    int count = static_cast<int>(send_buffer.size());

    return count;
  }

  // Return tuple: block indices (idx1, idx2)
  template <typename LM>
  static auto deserialize_received_buf(std::vector<char>& recv_buf, LM& Block,
                                       MPI_Status& s) {
    int count = MMPI::get_count<char>(s);
    std::array<int, 2> meta_data{-1, -1};
    std::vector<char> ser_data(count - sizeof(meta_data));

    std::memcpy(meta_data.data(), recv_buf.data(), sizeof(meta_data));
    std::memcpy(ser_data.data(), recv_buf.data() + sizeof(meta_data),
                ser_data.size());
    Block.deserialize(ser_data);
    auto& [idx1, idx2] = meta_data;
    return std::make_pair(idx1, idx2);
  }

public:
  template <TaskMapFunction TMapFunc, typename LMA, DataMapFunction DA,
            typename LMB, DataMapFunction DB>
  friend void serialize_gemm(const PartDenseMatrix<LMA, DA>& A,
                             const PartDenseMatrix<LMB, DB>& B,
                             PartDenseMatrix& C, TMapFunc taskmap,
                             Scalar alpha = Scalar{1},
                             Scalar beta = Scalar{0}) {
    COMPOSYX_ASSERT(A.get_comm() == B.get_comm(),
                    "PartDenseMatrix::serialize_gemm A and B must have same "
                    "MPI communicator");
    COMPOSYX_ASSERT(A.get_comm() == C.get_comm(),
                    "PartDenseMatrix::serialize_gemm A and C must have same "
                    "MPI communicator");
    const MPI_Comm comm = A.get_comm();
    const int rank = MMPI::rank(comm);

    using LMC = LocMatrix;

    size_t M_blocks = C.get_n_rows_block();
    size_t N_blocks = C.get_n_cols_block();
    size_t K_blocks = A.get_n_cols_block();

    COMPOSYX_DIM_ASSERT(
        M_blocks, A.get_n_rows_block(),
        "PartDenseMatrix::serialize_gemm rows bloc (A) != rows bloc (C)");
    COMPOSYX_DIM_ASSERT(
        K_blocks, B.get_n_rows_block(),
        "PartDenseMatrix::serialize_gemm cols bloc (A) != rows bloc (B)");
    COMPOSYX_DIM_ASSERT(
        N_blocks, B.get_n_cols_block(),
        "PartDenseMatrix::serialize_gemm cols bloc (C) != cols bloc (B)");

    C *= beta;

    const int tag_A = 33;
    const int tag_B = 35;
    const int tag_C = 37;

    std::map<std::pair<int, int>, std::vector<char>> Aik_blocks_to_send;
    std::vector<MPI_Request> reqs_A;

    int max_size_loc = 0;

    // Isend A -> taskmap(i, j, k)
    for (auto& [pos, _] : A.get_glob2loc()) {
      const auto& [i, k] = pos;
      const LMA& Aik = A.get_local_matrix(i, k);
      std::set<int> sent_to;
      int count = -1;

      for (int j = 0; j < static_cast<int>(N_blocks); j++) {
        // isend to taskmap(i, j, k)
        int dest = taskmap(i, j, k);
        if (dest == rank)
          continue; // Avoid self send
        if (sent_to.contains(dest))
          continue; // Avoid duplicate send

        if (!Aik_blocks_to_send.contains(pos)) {
          auto [pair_ptr, __] =
              Aik_blocks_to_send.emplace(pos, std::vector<char>());
          count = serialize_send_buf(Aik, i, k, pair_ptr->second);
          max_size_loc = std::max(max_size_loc, count);
        }

        auto& req_ref = reqs_A.emplace_back();
        MMPI::isend(Aik_blocks_to_send.at(pos).data(), count, tag_A, dest,
                    req_ref, comm);
        sent_to.insert(dest);
      }
    }

    std::map<std::pair<int, int>, std::vector<char>> Bkj_blocks_to_send;
    std::vector<MPI_Request> reqs_B;

    // Isend B -> taskmap(i, j, k)
    for (auto& [pos, _] : B.get_glob2loc()) {
      const auto& [k, j] = pos;
      const LMB& Bkj = B.get_local_matrix(k, j);
      std::set<int> sent_to;
      int count = -1;

      for (int i = 0; i < static_cast<int>(M_blocks); i++) {
        // isend to taskmap(i, j, k)
        int dest = taskmap(i, j, k);
        if (dest == rank)
          continue; // Avoid self send
        if (sent_to.contains(dest))
          continue; // Avoid duplicate send

        if (!Bkj_blocks_to_send.contains(pos)) {
          auto [pair_ptr, __] =
              Bkj_blocks_to_send.emplace(pos, std::vector<char>());
          count = serialize_send_buf(Bkj, k, j, pair_ptr->second);
          max_size_loc = std::max(max_size_loc, count);
        }

        auto& req_ref = reqs_B.emplace_back();
        MMPI::isend(Bkj_blocks_to_send.at(pos).data(), count, tag_B, dest,
                    req_ref, comm);
        sent_to.insert(dest);
      }
    }

    // Get max message size
    auto get_max_size = [&](int& size_loc) {
      int m = 0;
      MMPI::allreduce(&size_loc, &m, 1, MPI_MAX, comm);
      return m;
    };
    const int max_size = get_max_size(max_size_loc);

    // Task loop:
    // 1. part: count receptions to be made for Aik and Bjk
    int n_recv_A = 0;
    int n_recv_B = 0;

    std::map<std::pair<int, int>, LMA> Aik_received;
    std::map<std::pair<int, int>, LMB> Bkj_received;

    for (int i = 0; i < static_cast<int>(M_blocks); i++) {
      for (int j = 0; j < static_cast<int>(N_blocks); j++) {
        for (int k = 0; k < static_cast<int>(K_blocks); k++) {
          if (taskmap(i, j, k) == rank) {
            // Recv A(i, k)
            int src_A = A.datamap(i, k);

            if (src_A != rank) {
              auto ik = make_pair(i, k);
              if (!Aik_received.contains(ik)) {
                Aik_received.emplace(ik, LocMatrix());
                n_recv_A++;
              }
            }

            // Recv B(k, j)
            int src_B = B.datamap(k, j);
            if (src_B != rank) {
              auto kj = make_pair(k, j);
              if (!Bkj_received.contains(kj)) {
                Bkj_received.emplace(kj, LMB());
                n_recv_B++;
              }
            }
          }
        } // i
      } // j
    } // k

    // Task loop
    // 2. Receive (out of order) Aik and Bkj
    std::vector<char> recv_buf(max_size);
    for (int rab = 0; rab < (n_recv_A + n_recv_B); ++rab) {

      MPI_Status s = MMPI::recv(recv_buf.data(), max_size, MPI_ANY_TAG,
                                MPI_ANY_SOURCE, comm);
      int tag = MMPI::get_tag(s);

      if (tag == tag_A) {
        LMA Block;
        auto idx_pair = deserialize_received_buf(recv_buf, Block, s);
        Aik_received.at(idx_pair) = Block;
      } else if (tag == tag_B) {
        LMB Block;
        auto idx_pair = deserialize_received_buf(recv_buf, Block, s);
        Bkj_received.at(idx_pair) = Block;
      }
    }

    // Some cleaning
    recv_buf.clear();
    MMPI::waitall(reqs_A);
    reqs_A.clear();
    Aik_blocks_to_send.clear();
    MMPI::waitall(reqs_B);
    reqs_B.clear();
    Bkj_blocks_to_send.clear();

    // Now send Cijk to C datamap(i, j)
    size_t n_C_send = 0;
    for (int i = 0; i < static_cast<int>(M_blocks); i++) {
      for (int j = 0; j < static_cast<int>(N_blocks); j++) {
        if (C.datamap(i, j) != rank) {
          for (int k = 0; k < static_cast<int>(K_blocks); k++) {
            if (taskmap(i, j, k) == rank) {
              n_C_send++;
            }
          }
        }
      }
    }

    std::vector<MPI_Request> reqs_C(n_C_send);
    std::vector<std::vector<char>> Cijk_blocks_to_send(n_C_send);
    int max_size_C_loc = 0;
    int idx_send_buffer = 0;

    for (int i = 0; i < static_cast<int>(M_blocks); i++) {
      for (int j = 0; j < static_cast<int>(N_blocks); j++) {
        for (int k = 0; k < static_cast<int>(K_blocks); k++) {
          if (taskmap(i, j, k) == rank) {
            LMA* Aik_ptr = nullptr;
            LMB* Bkj_ptr = nullptr;

            int src_A = A.datamap(i, k);
            int src_B = B.datamap(k, j);
            int dest_C = C.datamap(i, j);

            if (src_A == rank) {
              Aik_ptr = const_cast<LMA*>(&A.get_local_matrix(i, k));
            } else {
              Aik_ptr = &Aik_received.at(make_pair(i, k));
            }

            if (src_B == rank) {
              Bkj_ptr = const_cast<LMB*>(&B.get_local_matrix(k, j));
            } else {
              Bkj_ptr = &Bkj_received.at(make_pair(k, j));
            }

            // Send to C(i, j)
            if (dest_C == rank) {
              auto& Cij = C.get_local_matrix(i, j);
              Cij += (alpha * (*Aik_ptr) * (*Bkj_ptr));
            } else {
              LMC Cijk = alpha * (*Aik_ptr) * (*Bkj_ptr);

              auto& data_to_send = Cijk_blocks_to_send.at(idx_send_buffer);
              int count = serialize_send_buf(Cijk, i, j, data_to_send);
              MMPI::isend(data_to_send.data(), count, tag_C, dest_C,
                          reqs_C.at(idx_send_buffer), comm);
              idx_send_buffer++;

              max_size_C_loc = std::max(count, max_size_C_loc);
            }
          }
        } // i
      } // j
    } // k

    // Last step: receive and sum Cijk on Cij
    // Recv Cijk blocks and sum on Cij

    // Count recpections
    int n_recv_C = 0;
    for (auto& [pos, _] : C.get_glob2loc()) {
      const auto& [i, j] = pos;
      for (int k = 0; k < static_cast<int>(K_blocks); k++) {
        int src = taskmap(i, j, k);
        if (src != rank)
          n_recv_C++;
      }
    }

    // Receive and sum
    const int max_size_C = get_max_size(max_size_C_loc);
    recv_buf = std::vector<char>(max_size_C);
    for (int rc = 0; rc < n_recv_C; ++rc) {
      MPI_Status s =
          MMPI::recv(recv_buf.data(), max_size_C, tag_C, MPI_ANY_SOURCE, comm);
      LMC Cijk;
      auto [i, j] = deserialize_received_buf(recv_buf, Cijk, s);
      auto& Cij = C.get_local_matrix(i, j);
      Cij += Cijk;
    }

    MMPI::waitall(reqs_C);
  }
// Gemm with serialization:1 ends here

// [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*GEMM with Chameleon][GEMM with Chameleon:1]]
#if defined(COMPOSYX_USE_CHAMELEON) && defined(CHAMELEON_USE_MPI)
private:
  static void* chameleon_getaddr(const CHAM_desc_t* desc, int ib, int jb) {
    using PartMat = PartDenseMatrix<local_type, data_map_type>;
    PartMat* partmat = (PartMat*)desc->mat;
    return (void*)get_ptr(partmat->get_local_matrix(ib, jb));
  }

public:
  template <TaskMapFunction TMapFunc, typename LMA, DataMapFunction DA,
            typename LMB, DataMapFunction DB>
  friend void
  chameleon_gemm(const PartDenseMatrix<LMA, DA>& A,
                 const PartDenseMatrix<LMB, DB>& B, PartDenseMatrix& C,
                 [[maybe_unused]] TMapFunc taskmap, Scalar alpha = Scalar{1},
                 Scalar beta = Scalar{0}) {
    COMPOSYX_ASSERT(A.get_comm() == B.get_comm(),
                    "PartDenseMatrix::chameleon_gemm A and B must have same "
                    "MPI communicator");
    COMPOSYX_ASSERT(A.get_comm() == C.get_comm(),
                    "PartDenseMatrix::chameleon_gemm A and C must have same "
                    "MPI communicator");

    CHAM_desc_t* chamA = NULL;
    CHAM_desc_t* chamB = NULL;
    CHAM_desc_t* chamC = NULL;
    int NB = A.get_blocksize_row(0);
    int M = A.glob_m();
    int K = A.glob_n();
    int N = C.glob_n();
    int P = 0;
    int Q = 0;
    /* check that data mapping is 2D block cyclic for all 3 matrices */
    if constexpr (std::is_same<DA,
                               canonical_datamap::TwoD_block_cyclic>::value and
                  std::is_same<DB,
                               canonical_datamap::TwoD_block_cyclic>::value and
                  std::is_same<decltype(C.get_datamap().get_func()),
                               canonical_datamap::TwoD_block_cyclic>::value) {
      P = A.get_datamap().get_func().getP();
      Q = A.get_datamap().get_func().getQ();
    } else {
      COMPOSYX_ASSERT(false, "PartDenseMatrix::chameleon_gemm datamap function "
                             "is not of type TwoD_block_cyclic");
    }
    CHAMELEON_Desc_Create_User(&chamA, (void*)&A, chameleon::getType(alpha), NB,
                               NB, NB * NB, M, K, 0, 0, M, K, P, Q,
                               chameleon_getaddr, NULL, NULL);
    CHAMELEON_Desc_Create_User(&chamB, (void*)&B, chameleon::getType(alpha), NB,
                               NB, NB * NB, K, N, 0, 0, K, N, P, Q,
                               chameleon_getaddr, NULL, NULL);
    CHAMELEON_Desc_Create_User(&chamC, (void*)&C, chameleon::getType(alpha), NB,
                               NB, NB * NB, M, N, 0, 0, M, N, P, Q,
                               chameleon_getaddr, NULL, NULL);
    chameleon::gemm_tile(ChamNoTrans, ChamNoTrans, alpha, chamA, chamB, beta,
                         chamC);
    CHAMELEON_Desc_Destroy(&chamC);
    CHAMELEON_Desc_Destroy(&chamB);
    CHAMELEON_Desc_Destroy(&chamA);
  }
#endif // defined(COMPOSYX_USE_CHAMELEON) && defined(CHAMELEON_USE_MPI)
  // GEMM with Chameleon:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Gemm flops][Gemm flops:1]]
  [[nodiscard]]
  friend long int flops_gemm(const PartDenseMatrix& A,
                             const PartDenseMatrix& B) {
    size_t M = A.glob_m();
    size_t N = B.glob_n();
    size_t K = A.glob_n();
    // (K mult + (K - 1) add) * M * N
    return static_cast<long int>(M) * static_cast<long int>(N) *
           static_cast<long int>(2 * K - 1);
  }
  // Gemm flops:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Norm][Norm:1]]
  [[nodiscard]]
  Real norm() const {
    Real norm_sq = Real{0};
    Real norm_sq_loc = Real{0};
    for (const auto& lm : _local_matrices) {
      const auto locnorm = lm.norm();
      norm_sq_loc += (locnorm * locnorm);
    }

    MMPI::allreduce(&norm_sq_loc, &norm_sq, 1, MPI_SUM, _comm);
    return std::sqrt(norm_sq);
  }
  // Norm:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Addition][Addition:1]]
  friend void axpy(const PartDenseMatrix& X, PartDenseMatrix& Y,
                   Scalar alpha = Scalar{1}) {

    const size_t X_row_blocks = X.get_n_rows_block();

    auto tag_X = [&](int i, int j) { return j * X_row_blocks + i + 1; };

    COMPOSYX_DIM_ASSERT(X_row_blocks, Y.get_n_rows_block(),
                        "PartDenseMatrix::axpy rows bloc (X) != rows bloc (Y)");
    COMPOSYX_DIM_ASSERT(X.get_n_cols_block(), Y.get_n_cols_block(),
                        "PartDenseMatrix::axpy cols bloc (X) != cols bloc (Y)");

    const int rank = Y._rank;
    MPI_Comm comm = Y.get_comm();

    // Send X blocks to Y location (if different)
    std::vector<MPI_Request> reqs_X;
    for (const auto& [pos, loc_idx] : X.get_glob2loc()) {
      const auto& [ib, jb] = pos;
      if (Y.datamap(ib, jb) != rank) {
        const auto& Xloc = X._local_matrices.at(loc_idx);
        const int count = static_cast<int>(n_rows(Xloc) * n_cols(Xloc));
        auto& req_ref = reqs_X.emplace_back();
        const int tag = tag_X(ib, jb);
        const int dest = Y.datamap(ib, jb);
        MMPI::isend(get_ptr(Xloc), count, tag, dest, req_ref, comm);
      }
    }

    // Receive X blocks and sums on Y
    for (const auto& [pos, loc_idx] : Y.get_glob2loc()) {
      auto& Yloc = Y._local_matrices.at(loc_idx);
      const auto& [ib, jb] = pos;
      const LocMatrix* Xloc_ptr;
      LocMatrix recv_buf;
      if (X.datamap(ib, jb) == rank) {
        Xloc_ptr = &(X.get_local_matrix(ib, jb));
      } else {
        recv_buf = LocMatrix(n_rows(Yloc), n_cols(Yloc));
        const int size = static_cast<int>(n_rows(Yloc) * n_cols(Yloc));
        const int tag = tag_X(ib, jb);
        const int src = X.datamap(ib, jb);
        MMPI::recv(get_ptr(recv_buf), size, tag, src, comm);
        Xloc_ptr = &recv_buf;
      }
      Yloc += alpha * (*Xloc_ptr);
    }

    MMPI::waitall(reqs_X);
  }
  // Addition:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Compute a distribution consistent for matrix product][Compute a distribution consistent for matrix product:1]]
  template <typename LMA, DataMapFunction DA, DataMapFunction DC,
            typename LMC = LocMatrix>
  friend PartDenseMatrix<LMC, DC>
  compute_matmul_distribution(const PartDenseMatrix<LMA, DA>& A,
                              const PartDenseMatrix& B,
                              const Datamap<DC>& c_datamap) {
    size_t M_blocks = A.get_n_rows_block();
    size_t N_blocks = B.get_n_cols_block();
    size_t K_blocks = A.get_n_cols_block();

    COMPOSYX_DIM_ASSERT(K_blocks, B.get_n_rows_block(),
                        "PartDenseMatrix::compute_matmul_distribution cols "
                        "bloc (A) != rows bloc (B)");

    int rank = MMPI::rank(A.get_comm());
    std::map<std::pair<int, int>, LMC> mat_map;

    for (int i = 0; i < static_cast<int>(M_blocks); ++i) {
      for (int j = 0; j < static_cast<int>(N_blocks); ++j) {
        int proc = c_datamap(i, j);
        if (proc == rank) {
          int nrows_Ai = A.get_blocksize_row(i);
          int ncols_Bj = B.get_blocksize_col(j);
          mat_map.emplace(make_pair(i, j), LMC(nrows_Ai, ncols_Bj));
        }
      }
    }

    return PartDenseMatrix<LMC, DC>(A.get_comm(), std::move(mat_map),
                                    c_datamap);
  }
  // Compute a distribution consistent for matrix product:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Compute a vector distribution for a matrix vector product][Compute a vector distribution for a matrix vector product:1]]
  template <DataMapFunction DX = MapFunc, typename LMX = LocMatrix>
  friend auto compute_matvect_distribution(const PartDenseMatrix& A) {
    std::map<std::pair<int, int>, LMX> mat_map;
    int rank = MMPI::rank(A.get_comm());

    // We choose as data datamap the transpose of A's one
    //using namespace std::placeholders;
    //auto A_tr_datamap = std::bind(A._datamap, _2, _1);
    auto A_tr_datamap_ptr = std::make_shared<decltype(A._datamap.get_func())>(
        A._datamap.get_func());
    Datamap x_datamap(A_tr_datamap_ptr, A.get_n_cols_block(), 1);

    for (int i = 0; i < static_cast<int>(A.get_n_cols_block()); ++i) {
      int proc = x_datamap(i, 0);
      if (proc == rank) {
        int nrows_Xi = A.get_blocksize_col(i);
        mat_map.emplace(make_pair(i, 0), LMX(nrows_Xi, 1));
      }
    }

    return PartDenseMatrix(A.get_comm(), std::move(mat_map), x_datamap);
  }
  // Compute a vector distribution for a matrix vector product:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Operators][Operators:1]]
  PartDenseMatrix& operator+=(const PartDenseMatrix& mat) {
    axpy(mat, *this);
    return *this;
  }

  PartDenseMatrix& operator-=(const PartDenseMatrix& mat) {
    axpy(mat, *this, Scalar{-1});
    return *this;
  }

  PartDenseMatrix& operator*=(Scalar alpha) {
    if (alpha == Scalar{1})
      return *this;
    for (auto& loc_mat : _local_matrices)
      loc_mat *= alpha;
    return *this;
  }

  PartDenseMatrix& operator/=(Scalar alpha) {
    COMPOSYX_ASSERT(alpha != Scalar{0},
                    "PartDenseMatrix /= scalar division by 0");
    Scalar inv = Scalar{1.0 / alpha};
    (*this) *= inv;
    return *this;
  }

  friend PartDenseMatrix operator*(PartDenseMatrix A, Scalar alpha) {
    A *= alpha;
    return A;
  }

  friend PartDenseMatrix operator*(Scalar alpha, PartDenseMatrix A) {
    A *= alpha;
    return A;
  }

  friend PartDenseMatrix operator+(PartDenseMatrix m1,
                                   const PartDenseMatrix& m2) {
    m1 += m2;
    return m1;
  }

  friend PartDenseMatrix operator-(PartDenseMatrix m1,
                                   const PartDenseMatrix& m2) {
    m1 -= m2;
    return m1;
  }

  template <typename LMA, DataMapFunction DA>
  friend PartDenseMatrix operator*(const PartDenseMatrix<LMA, DA>& A,
                                   const PartDenseMatrix& B) {
    PartDenseMatrix C = compute_matmul_distribution(A, B, A.get_datamap());

    size_t dimA = A.glob_m() * A.glob_n();
    size_t dimB = B.glob_m() * B.glob_n();
    size_t dimC = C.glob_m() * C.glob_n();
    dimC *= 2; // Add an advantage to C-stat in dimensions are similar

    size_t dim_max = std::max(dimA, std::max(dimB, dimC));

    std::function<int(int, int, int)> taskmap =
        canonical_taskmap::C_stationary(C.get_datamap());
    if (dim_max == dimA) {
      taskmap = canonical_taskmap::A_stationary(A.get_datamap());
    } else if (dim_max == dimB) {
      taskmap = canonical_taskmap::B_stationary(B.get_datamap());
    }

    if (A._gemm_algo == gemm_algo::serialize_gemm) {
      serialize_gemm(A, B, C, taskmap);
    } else if (A._gemm_algo == gemm_algo::dist_gemm) {
      dense_dist_gemm(A, B, C, taskmap);
    } else if (A._gemm_algo == gemm_algo::chameleon_gemm) {
#if defined(COMPOSYX_USE_CHAMELEON) && defined(CHAMELEON_USE_MPI)
      chameleon_gemm(A, B, C, taskmap);
#else
      COMPOSYX_ASSERT(false,
                      "PartDenseMatrix::operator*: chameleon_gemm only "
                      "available if composyx "
                      "configured with Chameleon (COMPOSYX_USE_CHAMELEON) and "
                      "Chameleon configured with MPI "
                      "(CHAMELEON_USE_MPI)");
#endif
    }
    return C;
  }
  // Operators:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Datamap output][Datamap output:1]]
  std::string datamap_str(char sep = ',') const {
    std::stringstream ssout;
    for (size_t i = 0; i < get_n_rows_block(); ++i) {
      for (size_t j = 0; j < get_n_cols_block(); ++j) {
        ssout << _datamap(i, j) << sep;
      }
      ssout << '\n';
    }
    return ssout.str();
  }
  // Datamap output:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Block sizes output][Block sizes output:1]]
  std::string blocksizes_row_str(char sep = ',') const {
    std::stringstream ssout;
    for (size_t k = 0; k < _cuts_rows.size() - 1; ++k)
      ssout << (_cuts_rows[k + 1] - _cuts_rows[k]) << sep;
    return ssout.str();
  }

  std::string blocksizes_col_str(char sep = ',') const {
    std::stringstream ssout;
    for (size_t k = 0; k < _cuts_cols.size() - 1; ++k)
      ssout << (_cuts_cols[k + 1] - _cuts_cols[k]) << sep;
    return ssout.str();
  }
  // Block sizes output:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Check consistency][Check consistency:1]]
  bool check_consistency() const {
    const int nprocs = MMPI::size(_comm);
    const int rank = MMPI::rank(_comm);

    int next_in_ring = (rank + 1) % nprocs;
    int prev_in_ring = (rank + nprocs - 1) % nprocs;

    auto check_array = [&](const std::vector<size_t>& arr) {
      std::vector<size_t> recv_buf(arr.size());
      int count = static_cast<int>(arr.size());
      MPI_Request req;

      MMPI::isend(arr.data(), count, 1, next_in_ring, req, _comm);
      MMPI::recv(recv_buf.data(), count, 1, prev_in_ring, _comm);

      MPI_Status stat;
      MMPI::wait(req, stat);
      return (recv_buf == arr);
    };

    if (!check_array(_cuts_rows)) {
      std::cerr << "Inconsistent cuts_rows\n";
      return false;
    }
    if (!check_array(_cuts_cols)) {
      std::cerr << "Inconsistent cuts_cols\n";
      return false;
    }
    std::vector<size_t> check_datamap(get_n_rows_block() * get_n_cols_block());
    int k = 0;
    for (size_t i = 0; i < get_n_rows_block(); ++i) {
      for (size_t j = 0; j < get_n_cols_block(); ++j) {
        check_datamap[k++] = static_cast<size_t>(_datamap(i, j));
      }
    }

    if (!check_array(check_datamap)) {
      std::cerr << "Inconsistent datamap\n";
      return false;
    }

    return true;
  }
  // Check consistency:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Display][Display:1]]
  void display(const std::string& name = "",
               std::ostream& out = std::cout) const {
    if (!name.empty())
      out << name << '\n';

    out << "Global matrix dimensions (" << glob_m() << ',' << glob_n() << ")\n";

    for (size_t k = 0; k < _local_matrices.size(); ++k) {
      out << "Global position: (" << _loc2glob[k].first << " , "
          << _loc2glob[k].second << ")\n";
      std::string s("Local matrix ");
      s += std::to_string(k);
      composyx::display(_local_matrices[k], s, out);
    }

    out << "Global to local indexing:\n";
    for (const auto& [pos, loc_idx] : _glob2loc) {
      out << '(' << pos.first << ',' << pos.second << "): " << loc_idx << '\n';
    }
    out << '\n';

    out << "Global to proc map:\n";
    out << datamap_str('\t');
    out << '\n';
  }

  friend std::ostream& operator<<(std::ostream& out, const PartDenseMatrix& p) {
    p.display("", out);
    return out;
  }
  // Display:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Centralization][Centralization:1]]
  LocMatrix centralize(int root) const {
    LocMatrix glob_matrix;

    if (_rank == root) {
      glob_matrix = LocMatrix(glob_m(), glob_n());
    }

    const auto n_block_row = get_n_rows_block();
    auto tag_block = [&n_block_row](int i, int j) {
      return j * n_block_row + i + 1;
    };

    std::vector<MPI_Request> reqs;

    for (const auto& [iblock, jblock] : _loc2glob) {
      auto& locmat = get_local_matrix(iblock, jblock);
      int nrows = get_blocksize_row(iblock);
      int ncols = get_blocksize_col(jblock);
      auto& req = reqs.emplace_back();
      int tag = tag_block(iblock, jblock);
      MMPI::isend(get_ptr(locmat), nrows * ncols, tag, root, req, _comm);
    }

    if (_rank == root) {
      for (size_t iblock = 0; iblock < get_n_rows_block(); iblock++) {
        for (size_t jblock = 0; jblock < get_n_cols_block(); jblock++) {
          int tag = tag_block(iblock, jblock);
          int nrows = get_blocksize_row(iblock);
          int ncols = get_blocksize_col(jblock);
          int source = _datamap(iblock, jblock);
          LocMatrix received_matrix(nrows, ncols);
          MMPI::recv(get_ptr(received_matrix), nrows * ncols, tag, source,
                     _comm);
          glob_matrix.get_block_view(_cuts_rows.at(iblock),
                                     _cuts_cols.at(jblock), nrows, ncols) =
              received_matrix;
        }
      }
    }

    MMPI::waitall(reqs);

    return glob_matrix;
  }
  // Centralization:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Centralize and display][Centralize and display:1]]
  void display_centralized(int root, const std::string& name = "",
                           std::ostream& out = std::cout) const {
    LocMatrix glob_matrix = centralize(root);

    if (_rank != root) {
      return;
    }

    if (!name.empty())
      out << name << '\n';

    constexpr const int textwidth = (is_complex<Scalar>::value) ? 23 : 10;
    const std::string textline(textwidth + 1, '-');

    auto line_separator = [&]() {
      out << '|';
      int cidx = 1;
      for (size_t j = 0; j < glob_n(); ++j) {
        if (_cuts_cols[cidx] <= j) {
          out << '+';
          cidx++;
        }
        out << textline;
      }
      out << "|\n";
    };

    int row_block_idx = 1;
    int col_block_idx = 0;

    for (size_t i = 0; i < glob_m(); ++i) {
      if (_cuts_rows[row_block_idx] <= i) {
        line_separator();
        row_block_idx++;
      }

      for (size_t j = 0; j < glob_n(); ++j) {
        if (_cuts_cols[col_block_idx] <= j) {
          out << '|';
          col_block_idx++;
        }
        out << std::setw(textwidth) << std::setprecision(3) << std::scientific
            << glob_matrix(i, j) << ' ';
      }
      col_block_idx = 0;
      out << "|\n";
    }

    out << "\nData map:\n";

    for (size_t iblock = 0; iblock < get_n_rows_block(); ++iblock) {
      for (size_t jblock = 0; jblock < get_n_cols_block(); ++jblock) {
        out << std::setw(3) << _datamap(iblock, jblock) << ' ';
      }
      out << '\n';
    }
    out << '\n';
  }
  // Centralize and display:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Get block index from global index][Get block index from global index:1]]
  template <CPX_Integral Tint>
  [[nodiscard]]
  int get_block_index_row(Tint i) const {
    auto upper_idx = std::ranges::upper_bound(_cuts_rows, i);
    return std::ranges::distance(_cuts_rows.begin(), upper_idx) - 1;
  }

  template <CPX_Integral Tint>
  [[nodiscard]]
  int get_block_index_col(Tint j) const {
    auto upper_idx = std::ranges::upper_bound(_cuts_cols, j);
    return std::ranges::distance(_cuts_cols.begin(), upper_idx) - 1;
  }

  template <CPX_Integral Tint>
  [[nodiscard]]
  std::pair<int, int> get_block_indices(Tint i, Tint j) const {
    return std::make_pair(get_block_index_row(i), get_block_index_col(j));
  }
  // Get block index from global index:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Extract a row or column][Extract a row or column:1]]
  template <CPX_Integral Tint,
            DataMapFunction DMap = std::function<int(int, int)>>
  [[nodiscard]] PartDenseMatrix get_col(Tint j) const { // , size_t nb_col = 1
    std::map<std::pair<int, int>, LocMatrix> mat_map;

    int jblock = get_block_index_col(j);
    int locmat_j = j - _cuts_cols[jblock];

    for (size_t iblock = 0; iblock < get_n_rows_block(); ++iblock) {
      if (_datamap(iblock, jblock) == _rank) {
        LocMatrix lm(
            get_local_matrix(iblock, jblock).get_vect_view(locmat_j).copy());
        mat_map.emplace(std::make_pair(iblock, 0), std::move(lm));
      }
    }

    return PartDenseMatrix(
        _comm, std::move(mat_map),
        create_datamap_with_offset(this->get_datamap(), 0, jblock));
  }

  template <CPX_Integral Tint>
  [[nodiscard]] PartDenseMatrix get_row(Tint i) const {
    std::map<std::pair<int, int>, LocMatrix> mat_map;

    // Find the block (row)
    int iblock = get_block_index_row(i);
    int locmat_i = i - _cuts_rows[iblock];

    for (size_t jblock = 0; jblock < get_n_cols_block(); ++jblock) {
      if (_datamap(iblock, jblock) == _rank) {
        LocMatrix lm(get_local_matrix(iblock, jblock).get_row(locmat_i).copy());
        mat_map.emplace(std::make_pair(0, jblock), std::move(lm));
      }
    }

    return PartDenseMatrix(
        _comm, std::move(mat_map),
        create_datamap_with_offset(this->get_datamap(), iblock, 0));
  }
  // Extract a row or column:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Extract a block][Extract a block:1]]
  template <CPX_Integral Tint>
  [[nodiscard]] PartDenseMatrix get_block(Tint i, Tint j, size_t m,
                                          size_t n) const {
    std::map<std::pair<int, int>, LocMatrix> mat_map;

    COMPOSYX_ASSERT(i + m <= glob_m(),
                    "PartDenseMatrix::get_block: i + M > nb rows");
    COMPOSYX_ASSERT(j + n <= glob_n(),
                    "PartDenseMatrix::get_block: j + N > nb cols");

    auto [ib_beg, jb_beg] = get_block_indices(i, j);
    auto [ib_end, jb_end] = get_block_indices(i + m - 1, j + n - 1);

    for (int iblock = ib_beg; iblock <= ib_end; ++iblock) {
      for (int jblock = jb_beg; jblock <= jb_end; ++jblock) {
        //dm(iblock - ib_beg, jblock - jb_beg) = _datamap(iblock, jblock);

        if (_datamap(iblock, jblock) == _rank) {
          LocMatrix lm = get_local_matrix(iblock, jblock);

          int lm_i_beg = (iblock == ib_beg) ? (i - _cuts_rows[ib_beg]) : 0;
          size_t lm_i_m = (iblock == ib_end)
                              ? (i + m - _cuts_rows[ib_end] - lm_i_beg)
                              : n_rows(lm) - lm_i_beg;
          int lm_j_beg = (jblock == jb_beg) ? (j - _cuts_cols[jb_beg]) : 0;
          size_t lm_j_n = (jblock == jb_end)
                              ? (j + n - _cuts_cols[jb_end] - lm_j_beg)
                              : n_cols(lm) - lm_j_beg;

          auto pos = std::make_pair(iblock - ib_beg, jblock - jb_beg);
          auto block = lm.get_block_copy(lm_i_beg, lm_j_beg, lm_i_m, lm_j_n);
          mat_map.emplace(pos, std::move(block));
        }
      }
    }

    return PartDenseMatrix(
        _comm, std::move(mat_map),
        create_datamap_with_offset(this->get_datamap(), ib_beg, jb_beg));
  }
  // Extract a block:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Getters][Getters:1]]
  size_t get_n_rows_block() const { return _cuts_rows.size() - 1; }
  size_t get_n_cols_block() const { return _cuts_cols.size() - 1; }
  size_t glob_m() const { return _cuts_rows.back(); }
  size_t glob_n() const { return _cuts_cols.back(); }
  int get_blocksize_row(int i) const {
    return _cuts_rows.at(i + 1) - _cuts_rows.at(i);
  }
  int get_blocksize_col(int j) const {
    return _cuts_cols.at(j + 1) - _cuts_cols.at(j);
  }
  LocMatrix& get_local_matrix(int i, int j) {
    return _local_matrices.at(_glob2loc.at(make_pair(i, j)));
  }
  const LocMatrix& get_local_matrix(int i, int j) const {
    return _local_matrices.at(_glob2loc.at(make_pair(i, j)));
  }
  Datamap<MapFunc> get_datamap() const { return _datamap; }
  int datamap(int i, int j) const { return _datamap(i, j); }
  MPI_Comm get_comm() const { return _comm; }
  const std::map<std::pair<int, int>, int>& get_glob2loc() const {
    return _glob2loc;
  }
  // Getters:1 ends here

  // [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*End of class][End of class:1]]
}; // class PartDenseMatrix
// End of class:1 ends here

// [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
