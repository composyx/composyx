// [[file:../../../org/composyx/part_data/PartOperator.org::*Header][Header:1]]
#pragma once

#include <string>
#include <vector>
#include <map>

namespace composyx {
template <CPX_LinearOperator, CPX_Matrix, CPX_Vector> class PartOperator;
}

#include "composyx/part_data/PartBase.hpp"
#include "composyx/part_data/PartVector.hpp"
#include "composyx/part_data/PartMatrix.hpp"
#include "composyx/utils/Error.hpp"
#include "composyx/dist/Process.hpp"
#include "composyx/solver/LinearOperator.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/part_data/PartOperator.org::*Attributes][Attributes:1]]
template <CPX_LinearOperator Operator,
          CPX_Matrix LocMatrix = typename Operator::matrix_type,
          CPX_Vector LocVector = typename Operator::vector_type>
class PartOperator
    : public PartBase<Operator>,
      public LinearOperator<PartMatrix<LocMatrix>, PartVector<LocVector>> {

private:
  using Matrix = PartMatrix<LocMatrix>;
  using Vector = PartVector<LocVector>;
  using Base = PartBase<Operator>;

public:
  using local_operator = Operator;
  using local_type = Operator;
  using matrix_type = Matrix;
  using vector_type = Vector;
  using scalar_type = typename Matrix::scalar_type;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/part_data/PartOperator.org::*Constructors][Constructors:1]]
  explicit PartOperator() : Base() {}

  explicit PartOperator(std::shared_ptr<Process> proc,
                        bool on_interface = false)
      : Base(proc, on_interface) {}

  //Ideally, delete it only if std::is_copy_assignable<Operator>::value is false
  PartOperator& operator=(const PartOperator&) = delete;
  // Constructors:1 ends here

  // [[file:../../../org/composyx/part_data/PartOperator.org::*Setup][Setup:1]]
  void setup(const Matrix& A) {
    this->initialize(A.get_proc());
    auto setup_operator = [](Operator& op, const LocMatrix& mat) {
      op.setup(mat);
    };
    this->template apply_on_data<LocMatrix>(A, setup_operator);
  }
  // Setup:1 ends here

  // [[file:../../../org/composyx/part_data/PartOperator.org::*Apply][Apply:1]]
  Vector apply(const Vector& B) {
    auto apply_operator = [](Operator& op, const LocVector& vect) {
      return op.apply(vect);
    };
    auto X = Vector(B.get_proc(), B.on_intrf());
    this->template apply_on_data<LocVector, LocVector>(B, X, apply_operator);
    X.assemble();
    return X;
  }
  // Apply:1 ends here

  // [[file:../../../org/composyx/part_data/PartOperator.org::*Traits][Traits:1]]
}; // class PartOperator
// Traits:1 ends here

// [[file:../../../org/composyx/part_data/PartOperator.org::*Traits][Traits:2]]
template <CPX_LinearOperator Operator, CPX_Matrix LocMatrix,
          CPX_Vector LocVector>
struct vector_type<PartOperator<Operator, LocMatrix, LocVector>>
    : public std::true_type {
  using type =
      typename PartOperator<Operator, LocMatrix, LocVector>::vector_type;
};

template <CPX_LinearOperator Operator, CPX_Matrix LocMatrix,
          CPX_Vector LocVector>
struct scalar_type<PartOperator<Operator, LocMatrix, LocVector>>
    : public std::true_type {
  using type =
      typename PartOperator<Operator, LocMatrix, LocVector>::scalar_type;
};
// Traits:2 ends here

// [[file:../../../org/composyx/part_data/PartOperator.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
