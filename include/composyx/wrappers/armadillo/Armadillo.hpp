// [[file:../../../../org/composyx/wrappers/armadillo/Armadillo.org::*Armidillo][Armidillo:1]]
#pragma once

#include <armadillo>
#include <composyx/interfaces/linalg_concepts.hpp>

namespace composyx {
using std::size_t;
// Some aliasing
template <CPX_Scalar Scalar> using A_Vector = arma::Col<Scalar>;

template <CPX_Scalar Scalar> using A_DenseMatrix = arma::Mat<Scalar>;

template <CPX_Scalar Scalar> using A_SparseMatrix = arma::SpMat<Scalar>;

// Armadillo vectors
template <CPX_Scalar Scalar>
[[nodiscard]] Scalar dot(const A_Vector<Scalar>& v1,
                         const A_Vector<Scalar>& v2) {
  if constexpr (is_complex<Scalar>::value) {
    return arma::cdot(v1, v2);
  } else {
    return arma::dot(v1, v2);
  }
}

template <CPX_Scalar Scalar>
[[nodiscard]] size_t size(const A_Vector<Scalar>& v) {
  return static_cast<size_t>(v.size());
}

template <CPX_Scalar Scalar>
[[nodiscard]] Scalar* get_ptr(A_Vector<Scalar>& v) {
  return v.memptr();
}

template <CPX_Scalar Scalar>
[[nodiscard]] const Scalar* get_ptr(const A_Vector<Scalar>& v) {
  return v.memptr();
}

template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_rows(const A_Vector<Scalar>& v) {
  return static_cast<size_t>(v.size());
}

template <CPX_Scalar Scalar>
void display(const A_Vector<Scalar>& v, const std::string& name = "",
             std::ostream& out = std::cout) {
  if (!name.empty())
    out << name << '\n';
  out << " (arma::Col)\n\n" << v;
  out << '\n';
}

template <CPX_Scalar Scalar>
[[nodiscard]] size_t get_leading_dim(const A_Vector<Scalar>& v) {
  return static_cast<size_t>(v.size());
}

// Armadillo dense matrix
template <CPX_Scalar Scalar>
[[nodiscard]] Scalar* get_ptr(A_DenseMatrix<Scalar>& m) {
  return m.memptr();
}

template <CPX_Scalar Scalar>
[[nodiscard]] const Scalar* get_ptr(const A_DenseMatrix<Scalar>& m) {
  return m.memptr();
}

template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_rows(const A_DenseMatrix<Scalar>& m) {
  return static_cast<size_t>(m.n_rows);
}

template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_cols(const A_DenseMatrix<Scalar>& m) {
  return static_cast<size_t>(m.n_cols);
}
template <CPX_Scalar Scalar>
[[nodiscard]] A_SparseMatrix<Scalar> diagonal(const A_DenseMatrix<Scalar>& m) {
  A_SparseMatrix<Scalar> out(m.diag());
  return out;
}

template <CPX_Scalar Scalar>
[[nodiscard]] A_Vector<Scalar>
diagonal_as_vector(const A_DenseMatrix<Scalar>& m) {
  return m.diag();
}

template <CPX_Scalar Scalar>
[[nodiscard]] A_DenseMatrix<Scalar> transpose(const A_DenseMatrix<Scalar>& m) {
  if constexpr (is_complex<Scalar>::value) {
    return m.st();
  } else {
    return m.t();
  }
}

template <CPX_Scalar Scalar>
[[nodiscard]] A_DenseMatrix<Scalar> adjoint(const A_DenseMatrix<Scalar>& m) {
  return m.t();
}

template <CPX_Scalar Scalar>
struct vector_type<A_DenseMatrix<Scalar>> : public std::true_type {
  using type = A_Vector<Scalar>;
};

template <CPX_Scalar Scalar>
void build_matrix(A_DenseMatrix<Scalar>& mat, const size_t M, const size_t N,
                  const size_t nnz, const int* i, const int* j, const Scalar* v,
                  bool fill_symmetry = false) {
  mat = A_DenseMatrix<Scalar>(M, N);
  mat *= Scalar{0};
  for (size_t k = 0; k < nnz; ++k) {
    mat(i[k], j[k]) = v[k];
    if (fill_symmetry && (i[k] != j[k])) {
      mat(j[k], i[k]) = v[k];
    }
  }
}

template <CPX_Scalar Scalar>
struct is_dense<A_DenseMatrix<Scalar>> : public std::true_type {};

// Armadillo sparse matrix
template <CPX_Scalar Scalar>
[[nodiscard]] int n_nonzero(const A_SparseMatrix<Scalar>& m) {
  return m.n_nonzero;
}

template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_rows(const A_SparseMatrix<Scalar>& m) {
  return static_cast<size_t>(m.n_rows);
}

template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_cols(const A_SparseMatrix<Scalar>& m) {
  return static_cast<size_t>(m.n_cols);
}

template <CPX_Scalar Scalar>
[[nodiscard]] A_SparseMatrix<Scalar> diagonal(const A_SparseMatrix<Scalar>& m) {
  A_SparseMatrix<Scalar> out(m.diag());
  return out;
}

template <CPX_Scalar Scalar>
[[nodiscard]] A_Vector<Scalar>
diagonal_as_vector(const A_SparseMatrix<Scalar>& m) {
  A_Vector<Scalar> v(m.diag());
  return v;
}

template <CPX_Scalar Scalar>
[[nodiscard]] A_SparseMatrix<Scalar> adjoint(const A_SparseMatrix<Scalar>& m) {
  return m.t();
}
// Matrix building

template <CPX_Scalar Scalar>
void build_matrix(A_SparseMatrix<Scalar>& mat, const int M, const int N,
                  const int nnz, const int* i, const int* j, const Scalar* v,
                  const bool fill_symmetry = false) {

  int true_nnz = 0;
  if (fill_symmetry) {
    for (int k = 0; k < nnz; ++k) {
      true_nnz++;
      if (i[k] != j[k])
        true_nnz++;
    }
  } else {
    true_nnz = nnz;
  }

  arma::umat locations(2, true_nnz);
  A_Vector<Scalar> vals(true_nnz);

  int idx = 0;
  for (int k = 0; k < nnz; ++k) {
    locations(0, idx) = i[k];
    locations(1, idx) = j[k];
    vals[idx] = v[k];
    idx++;

    if (i[k] != j[k] && fill_symmetry) {
      locations(0, idx) = j[k];
      locations(1, idx) = i[k];
      vals[idx] = v[k];
      idx++;
    }
  }

  mat = A_SparseMatrix<Scalar>(locations, vals, M, N);
}

template <CPX_Scalar Scalar>
void build_dense_matrix(A_DenseMatrix<Scalar>& mat, const int M, const int N,
                        Scalar* values) {
  mat = A_DenseMatrix<Scalar>(M, N);
  std::memcpy(get_ptr(mat), values, M * N * sizeof(Scalar));
}

template <CPX_Scalar Scalar>
struct is_sparse<A_SparseMatrix<Scalar>> : public std::true_type {};

template <CPX_Scalar Scalar>
struct vector_type<A_SparseMatrix<Scalar>> : public std::true_type {
  using type = A_Vector<Scalar>;
};

template <CPX_Scalar Scalar>
struct dense_type<A_SparseMatrix<Scalar>> : public std::true_type {
  using type = A_DenseMatrix<Scalar>;
};

template <CPX_Scalar Scalar>
struct sparse_type<A_DenseMatrix<Scalar>> : public std::true_type {
  using type = A_SparseMatrix<Scalar>;
};

// No diagonal type ?

template <CPX_Scalar Scalar>
struct diag_type<A_DenseMatrix<Scalar>> : public std::true_type {
  using type = A_SparseMatrix<Scalar>;
};

template <CPX_Scalar Scalar>
struct diag_type<A_SparseMatrix<Scalar>> : public std::true_type {
  using type = A_SparseMatrix<Scalar>;
};

// Scalar types
template <CPX_Scalar Scalar>
struct scalar_type<A_DenseMatrix<Scalar>> : public std::true_type {
  using type = Scalar;
};

template <CPX_Scalar Scalar>
struct scalar_type<A_SparseMatrix<Scalar>> : public std::true_type {
  using type = Scalar;
};

template <CPX_Scalar Scalar>
struct scalar_type<A_Vector<Scalar>> : public std::true_type {
  using type = Scalar;
};
} // namespace composyx
// Armidillo:1 ends here
