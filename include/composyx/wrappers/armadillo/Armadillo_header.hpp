// [[file:../../../../org/composyx/include/composyx/wrappers/armadillo/Armadillo_header.hpp :comments link][No heading:1]]
#pragma once

#include <armadillo>
#include <composyx/interfaces/basic_concepts.hpp>

namespace composyx {
// Some aliasing
template <CPX_Scalar Scalar> using A_Vector = arma::Col<Scalar>;

template <CPX_Scalar Scalar> using A_DenseMatrix = arma::Mat<Scalar>;

template <CPX_Scalar Scalar> using A_SparseMatrix = arma::SpMat<Scalar>;

// Vector
template <CPX_Scalar Scalar>
[[nodiscard]] Scalar dot(const A_Vector<Scalar>&, const A_Vector<Scalar>&);

template <CPX_Scalar Scalar> [[nodiscard]] size_t size(const A_Vector<Scalar>&);

template <CPX_Scalar Scalar> [[nodiscard]] Scalar* get_ptr(A_Vector<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] const Scalar* get_ptr(const A_Vector<Scalar>&);

template <CPX_Scalar Scalar> [[nodiscard]] size_t size(const A_Vector<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_rows(const A_Vector<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] constexpr size_t n_cols(const A_Vector<Scalar>&) {
  return size_t{1};
}

template <CPX_Scalar Scalar>
[[nodiscard]] constexpr size_t get_increment(const A_Vector<Scalar>&) {
  return size_t{1};
}

template <CPX_Scalar Scalar>
[[nodiscard]] constexpr size_t get_increment(A_Vector<Scalar>&) {
  return size_t{1};
}

// Dense matrix
template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_rows(const A_DenseMatrix<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_cols(const A_DenseMatrix<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] A_Vector<Scalar> diagonal_as_vector(const A_DenseMatrix<Scalar>&);

// Sparse matrix
template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_rows(const A_SparseMatrix<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_cols(const A_SparseMatrix<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] A_Vector<Scalar>
diagonal_as_vector(const A_SparseMatrix<Scalar>&);
} // namespace composyx
// No heading:1 ends here
