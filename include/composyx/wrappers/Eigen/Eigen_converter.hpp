// [[file:../../../../org/composyx/wrappers/Eigen/Eigen_converter.org::*Eigen converter][Eigen converter:1]]
#pragma once

#include <composyx/loc_data/DenseMatrix.hpp>
#include <composyx/wrappers/Eigen/Eigen.hpp>

namespace composyx {
template <typename Scalar>
inline DenseMatrix<Scalar>
to_composyx_dense(const E_DenseMatrix<Scalar>& e_mat) {
  DenseMatrix<Scalar> c_mat(e_mat.rows(), e_mat.cols());
  for (size_t i = 0; i < n_rows(c_mat); ++i) {
    for (size_t j = 0; j < n_cols(c_mat); ++j) {
      c_mat(i, j) = e_mat(i, j);
    }
  }

  return c_mat;
}

template <typename Scalar>
inline Vector<Scalar> to_composyx_dense(const E_Vector<Scalar>& e_vect) {
  Vector<Scalar> c_vect(e_vect.size());
  for (size_t i = 0; i < n_rows(c_vect); ++i) {
    c_vect[i] = e_vect[i];
  }
  return c_vect;
}

template <typename Scalar>
inline E_DenseMatrix<Scalar> to_eigen_dense(const DenseMatrix<Scalar>& c_mat) {
  E_DenseMatrix<Scalar> e_mat(n_rows(c_mat), n_cols(c_mat));
  for (size_t i = 0; i < n_rows(c_mat); ++i) {
    for (size_t j = 0; j < n_cols(c_mat); ++j) {
      e_mat(i, j) = c_mat(i, j);
    }
  }

  return e_mat;
}

template <typename Scalar>
inline E_Vector<Scalar> to_eigen_dense(const Vector<Scalar>& c_vect) {
  E_Vector<Scalar> e_vect(n_rows(c_vect));
  for (size_t i = 0; i < n_rows(c_vect); ++i) {
    e_vect[i] = c_vect[i];
  }
  return e_vect;
}

template <typename Scalar>
inline E_SparseMatrix<Scalar>
to_eigen_sparse(const DenseMatrix<Scalar>& c_mat) {
  std::vector<int> ai;
  std::vector<int> aj;
  std::vector<Scalar> av;
  for (size_t i = 0; i < n_rows(c_mat); ++i) {
    for (size_t j = 0; j < n_cols(c_mat); ++j) {
      if (c_mat(i, j) != Scalar{0}) {
        ai.push_back(i);
        aj.push_back(j);
        av.push_back(c_mat(i, j));
      }
    }
  }

  E_SparseMatrix<Scalar> e_mat(n_rows(c_mat), n_cols(c_mat));
  build_matrix(e_mat, n_rows(c_mat), n_cols(c_mat), static_cast<int>(ai.size()),
               ai.data(), aj.data(), av.data());

  return e_mat;
}
} // namespace composyx
// Eigen converter:1 ends here
