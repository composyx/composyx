// [[file:../../../../org/composyx/wrappers/Eigen/Eigen.org::*Header][Header:1]]
#pragma once

#include <composyx/wrappers/Eigen/EigenMatrix.hpp>

namespace composyx {
// Header:1 ends here

// [[file:../../../../org/composyx/wrappers/Eigen/Eigen.org::*Traits][Traits:1]]
template <CPX_Scalar Scalar>
struct vector_type<E_DenseMatrix<Scalar>> : public std::true_type {
  using type = E_Vector<Scalar>;
};

// Sparse
template <CPX_Scalar Scalar>
struct vector_type<E_SparseMatrix<Scalar>> : public std::true_type {
  using type = E_Vector<Scalar>;
};
// Traits:1 ends here

// [[file:../../../../org/composyx/wrappers/Eigen/Eigen.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
