// [[file:../../../../org/composyx/wrappers/Eigen/Eigen_header.org::*Header][Header:1]]
#pragma once

#include <Eigen/Core>
#include <Eigen/Sparse>
#include <composyx/interfaces/basic_concepts.hpp>

namespace composyx {
// Some aliasing
template <CPX_Scalar Scalar>
using E_Vector = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;

template <CPX_Scalar Scalar>
using E_DenseMatrix = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;

template <CPX_Scalar Scalar>
using E_DiagMatrix = Eigen::DiagonalMatrix<Scalar, Eigen::Dynamic>;

template <CPX_Scalar Scalar> using E_SparseMatrix = Eigen::SparseMatrix<Scalar>;

// Vector
template <CPX_Scalar Scalar>
[[nodiscard]] Scalar dot(const E_Vector<Scalar>&, const E_Vector<Scalar>&);

template <CPX_Scalar Scalar> [[nodiscard]] size_t size(const E_Vector<Scalar>&);

template <CPX_Scalar Scalar> [[nodiscard]] Scalar* get_ptr(E_Vector<Scalar>&);

template <CPX_Scalar Scalar> [[nodiscard]] size_t size(const E_Vector<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_rows(const E_Vector<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] constexpr size_t n_cols(const E_Vector<Scalar>&) {
  return size_t{1};
}

template <CPX_Scalar Scalar>
[[nodiscard]] constexpr size_t get_increment(const E_Vector<Scalar>&) {
  return size_t{1};
}

template <CPX_Scalar Scalar>
[[nodiscard]] constexpr size_t get_increment(E_Vector<Scalar>&) {
  return size_t{1};
}

template <CPX_Scalar Scalar> [[nodiscard]] Scalar* get_ptr(E_Vector<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] const Scalar* get_ptr(const E_Vector<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] size_t get_leading_dim(const E_Vector<Scalar>&);

// Diagonal matrix
template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_rows(const E_DiagMatrix<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_cols(const E_DiagMatrix<Scalar>&);

// Dense matrix
template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_rows(const E_DenseMatrix<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_cols(const E_DenseMatrix<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] E_Vector<Scalar> diagonal_as_vector(const E_DenseMatrix<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] size_t get_leading_dim(const E_DenseMatrix<Scalar>&);

// Sparse matrix
template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_rows(const E_SparseMatrix<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] size_t n_cols(const E_SparseMatrix<Scalar>&);

template <CPX_Scalar Scalar>
[[nodiscard]] E_Vector<Scalar>
diagonal_as_vector(const E_SparseMatrix<Scalar>&);
} // namespace composyx
// Header:1 ends here
