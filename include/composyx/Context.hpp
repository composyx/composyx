// [[file:../../org/composyx/Context.org::*Header][Header:1]]
#pragma once

#if !defined(COMPOSYX_NO_MPI)
#include <mpi.h>
#endif

#if defined(COMPOSYX_USE_MKL)
#include <mkl_service.h>
#elif defined(COMPOSYX_USE_OPENBLAS)
#include <cblas.h>
#elif defined(COMPOSYX_USE_BLIS)
#include <blis.h>
#endif

#if defined(COMPOSYX_USE_CHAMELEON)
#include "composyx/utils/Chameleon.hpp"
#endif

namespace composyx {
// Header:1 ends here

// [[file:../../org/composyx/Context.org::*Structure][Structure:1]]
struct Context {

  int blas_max_threads = 0;
  int n_cpus = -1;
  int n_gpus = 0;
  bool is_initialized = false;

#if !defined(COMPOSYX_NO_MPI)
  MPI_Comm comm_composyx = MPI_COMM_WORLD;
#endif

#if defined(COMPOSYX_USE_CHAMELEON)
  Chameleon chameleon;
#endif
};

#ifndef COMPOSYX_STATIC_VARIABLES_ALREADY_DECLARED
Context context;
// Structure:1 ends here

// [[file:../../org/composyx/Context.org::*Update parameters][Update parameters:1]]
int getMaxBlasThreads() { return context.blas_max_threads; }
int getBlasThreads() {
  int nt = -1;
#if defined(COMPOSYX_USE_MKL)
  nt = mkl_get_max_threads();
#elif defined(COMPOSYX_USE_OPENBLAS)
  nt = openblas_get_num_threads();
#elif defined(COMPOSYX_USE_BLIS)
  nt = bli_thread_get_num_threads();
#endif
  return nt;
}
int getNumCpus() { return context.n_cpus; }
int getNumGpus() { return context.n_gpus; }

void setBlasThreads(int nt) {
#if defined(COMPOSYX_USE_MKL)
  mkl_set_num_threads(nt);
#elif defined(COMPOSYX_USE_OPENBLAS)
  openblas_set_num_threads(nt);
#elif defined(COMPOSYX_USE_BLIS)
  bli_thread_set_num_threads(nt);
#else
  (void)nt;
#endif
}
void setNumCpus(int nc) {
  context.n_cpus = nc;
  setBlasThreads(nc);
}
void setNumGpus(int ng) { context.n_gpus = ng; }
#if defined(COMPOSYX_USE_CHAMELEON)
void setChameleonParameters(int nb = 320, int translation = ChamInPlace) {
  context.chameleon.setTileSize(nb);
  context.chameleon.setTranslation(translation);
}
#endif
// Update parameters:1 ends here

// [[file:../../org/composyx/Context.org::*Initalization][Initalization:1]]
void initialize(int nc = -1, int ng = 0) {
  if (nc > 0) {
    setNumCpus(nc);
  }
  if (ng > 0) {
    setNumGpus(ng);
  }
  context.blas_max_threads = getBlasThreads();
#if defined(COMPOSYX_USE_CHAMELEON)
#if defined(COMPOSYX_NO_MPI)
  context.chameleon.initialize(context.n_cpus, context.n_gpus);
#else
  context.chameleon.initialize(context.n_cpus, context.n_gpus,
                               context.comm_composyx);
#endif
  setBlasThreads(1);
#endif
  context.n_cpus = context.blas_max_threads;
  context.is_initialized = true;
}
// Initalization:1 ends here

// [[file:../../org/composyx/Context.org::*Finalization][Finalization:1]]
void finalize() {
  if (context.is_initialized) {
#if defined(COMPOSYX_USE_CHAMELEON)
    context.chameleon.finalize();
    setBlasThreads(context.blas_max_threads);
#endif // defined(COMPOSYX_USE_CHAMELEON)
  }
  context.is_initialized = false;
}
// Finalization:1 ends here

// [[file:../../org/composyx/Context.org::*Footer][Footer:1]]
#endif // COMPOSYX_STATIC_VARIABLES_ALREADY_DECLARED
} // namespace composyx
// Footer:1 ends here
