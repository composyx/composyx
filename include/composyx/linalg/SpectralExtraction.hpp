// [[file:../../../org/composyx/linalg/SpectralExtraction.org::*Header][Header:1]]
#pragma once

#include "composyx/interfaces/linalg_concepts.hpp"

#ifdef COMPOSYX_NO_MPI
#include "composyx/loc_data/DenseMatrix.hpp"
#else
#include "composyx/part_data/PartMatrix.hpp"
#endif

#if defined(COMPOSYX_USE_LAPACKPP)
using COMPOSYX_BLAS = composyx::blas_kernels;
#elif defined(COMPOSYX_USE_CHAMELEON)
using COMPOSYX_BLAS = composyx::chameleon_kernels;
#else
using COMPOSYX_BLAS = composyx::tlapack_kernels;
#endif

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/linalg/SpectralExtraction.org::*Class declaration][Class declaration:1]]
template <CPX_LinearOperator Matrix, CPX_Vector Vect> class SpectralExtraction {

private:
  using Scalar = typename Vect::scalar_type;
  using Real = typename Vect::real_type;
#ifndef COMPOSYX_NO_MPI
  using DenseVect =
      std::conditional_t<is_distributed<Vect>::value,
                         PartVector<DenseMatrix<Scalar>>, DenseMatrix<Scalar>>;
#else
  using DenseVect = DenseMatrix<Scalar>;
#endif

public:
  using scalar_type = Scalar;
  using vector_type = Vect;

  using matrix_type = Matrix;
  using real_type = Real;
  // Class declaration:1 ends here

  // [[file:../../../org/composyx/linalg/SpectralExtraction.org::*Attributes][Attributes:1]]
private:
  static constexpr const Real _threshold_SVD =
      is_precision_double<Scalar>::value ? 5e-14 : 1e-6;
  /*const*/ int _solve_deflation_size;
  /*const*/ int _deflation_increment;
  int _ivec;
  DenseVect* _V0_buffer; // buffer for the deflation space
  int*
      _current_deflation_size; // V0 = _V0_buffer[:, 0, _current_deflation_size]
  DenseVect _V;                // cantidate vectors for deflation buffer
  Vect _w;                     // A*zj at restart

  // FIXME _T should be sparse
  DenseMatrix<Scalar> _T;    // Lanczos' tridiagonal(ish) matrix
  DenseMatrix<Scalar> _Tm_1; // Upper left m-1 x m-1 block of T

  // Eigen vector storage for T_m and T_{m-1}
  DenseMatrix<Scalar> _Y_Y1; // [_Y, _Ym_1]
  DenseMatrix<Scalar> _Y;
  DenseMatrix<Scalar> _Ym_1;

  // Eigen / singular values storage
  std::vector<Real> _Sigma{}; // Singular values of Y_Y1

  // Fig 3.1 : 11.4
  // Store in LoY the eigenvectors of T_m associated wit its smallest eigenvalues
  DenseMatrix<Scalar> _U; //
  // For faster _T reset
  //int _init_V0 = false;
  int _old_rank_Y;
  bool _just_restarted = false;
  Scalar _rho;
  Scalar _alpha;
  Vect* _z;
  bool _is_setup = false;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/linalg/SpectralExtraction.org::*Constructor][Constructor:1]]
public:
  SpectralExtraction() {}
  SpectralExtraction(const int solve_deflation_size,
                     const int deflation_increment, int* current_deflation_size,
                     DenseVect* V0_buffer, Vect* z) {
    setup(solve_deflation_size, deflation_increment, current_deflation_size,
          V0_buffer, z);
  }

  void setup(const int solve_deflation_size, const int deflation_increment,
             int* current_deflation_size, DenseVect* V0_buffer, Vect* z) {
    //if(_is_setup) return;
    int nev2 = deflation_increment;
    int sds2 = solve_deflation_size;
    int max_deflation_size = V0_buffer->get_n_cols();

    if (max_deflation_size < deflation_increment) {
      COMPOSYX_WARNING(
          "SpectralExtraction: deflation size smaller than the increment,"
          "the increment is set to the deflation size.");
      nev2 = max_deflation_size;
    }

    if (sds2 <= nev2 * 2) {
      COMPOSYX_WARNING(
          "SpectralExtraction: deflation size for the solve is smaller than "
          "twice the increment,"
          "deflation size for the solve is set to twice the increment.");
      sds2 = nev2 * 2 + 1;
    }

    _solve_deflation_size = sds2;
    _deflation_increment = nev2;
    _ivec = 0;
    _V0_buffer = V0_buffer;
    _current_deflation_size = current_deflation_size;
    if constexpr (is_distributed<Vect>::value) {
      _V = build_partvector_multicol(*V0_buffer, _solve_deflation_size);
    } else {
      _V = DenseVect(n_rows(*V0_buffer), _solve_deflation_size);
    }

    if constexpr (is_distributed<Vect>::value) {
      _w = build_partvector_multicol(*V0_buffer, /*ncols*/ 1);
    } else {
      _w = Vect(n_rows(*V0_buffer), /*nrhs*/ 1);
    }

    _T = DenseMatrix<Scalar>(_solve_deflation_size, _solve_deflation_size);
    _T.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
    _Tm_1 = _T.get_block_view(0, 0, _solve_deflation_size - 1,
                              _solve_deflation_size - 1);
    _Tm_1.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);

    // Y
    _Y_Y1 =
        DenseMatrix<Scalar>(_solve_deflation_size, _deflation_increment * 2);
    _Y = _Y_Y1.get_vect_view(0, _deflation_increment);
    _Ym_1 =
        _Y_Y1.get_block_view(0, _deflation_increment, _solve_deflation_size - 1,
                             _deflation_increment);
    _U = DenseMatrix<Scalar>(
        _solve_deflation_size,
        std::min(_solve_deflation_size, _deflation_increment * 2));

    // Helper matrix
    _old_rank_Y = 0;
    // variables
    _just_restarted = false;
    _rho = 0.0;
    _alpha = 0.0;
    _z = z;
    _is_setup = true;
  }

  void setup(DenseVect* V0_buffer, Vect* z) {
    _V0_buffer = V0_buffer;
    _z = z;
    _w = Vect(*V0_buffer, /*nrhs*/ 1);
    _V = DenseVect(*V0_buffer, _solve_deflation_size);
  }
  // Constructor:1 ends here

  // [[file:../../../org/composyx/linalg/SpectralExtraction.org::*First iteration][First iteration:1]]
private:
  void _clear() {
    _V *= Scalar{0.0};
    _T *= Scalar{0.0};
    _w *= Scalar{0};
    _just_restarted = false;
    _rho = 0.0;
    _alpha = 0.0;
    _ivec = 0;
    _old_rank_Y = 0;
  }

public:
  void first_iteration(const Real rz) {
    Timer<TIMER_ITERATION> t("SpectralExtraction iteration 0");
    _clear();
    _V.get_vect_view(_ivec, /*nrhs*/ 1) = (*_z) / std::sqrt(rz);
  }
  // First iteration:1 ends here

  // [[file:../../../org/composyx/linalg/SpectralExtraction.org::*Update deflation space][Update deflation space:1]]
private:
  void _update_deflation() {
    Timer<TIMER_ITERATION> t("SpectralExtraction update_deflation");

    // Compute the eigenvectors of T_m - Fig 3.1: 11.3
    std::tie(std::ignore, _Y) =
        COMPOSYX_BLAS::eigh_smallest(_T, _deflation_increment);

    // Compute the eigenvectors of T_{m-1} - Fig 3.1: 11.3
    std::tie(std::ignore, _Ym_1) =
        COMPOSYX_BLAS::eigh_smallest(_Tm_1, _deflation_increment);

    // Compute the rank of the Y_Y1
    std::tie(_U, _Sigma) = COMPOSYX_BLAS::svd_left_sv(_Y_Y1);

    int rank_Y = std::ranges::count_if(
        _Sigma, [=](Real sig) { return sig > _threshold_SVD; });

    DenseMatrix<Scalar> Q =
        _U.get_block_view(0, 0, _solve_deflation_size, rank_Y);
    DenseMatrix<Scalar> H =
        dot_block(Q, static_cast<DenseMatrix<Scalar>>(_T * Q));
    H.set_property(MatrixSymmetry::symmetric, MatrixStorage::full);
    auto [D, Z] = COMPOSYX_BLAS::eigh_smallest(H, rank_Y);

    // Reset T
    if (_old_rank_Y == 0)
      for (int i = 0; i < rank_Y; i++)
        _T(i, i + 1) = Scalar{0.0};
    else
      for (int i = 0; i < _old_rank_Y; i++) {
        _T(i, i + 1) = Scalar{0.0};
        _T(i, _old_rank_Y) = Scalar{0.0};
      }

    // Update T for the restart - Fig 3.1: 11.8
    for (int i = 0; i < rank_Y; i++)
      _T(i, i) = D[i];
    auto Vy = _V.get_vect_view(0, rank_Y);
    auto QZ = static_cast<DenseMatrix<Scalar>>(Q * Z);
    //Vy = _V * static_cast<DenseMatrix<Scalar>>(Q * Z);
    apply_on_data([&](DenseMatrix<Scalar>& Vy_loc,
                      DenseMatrix<Scalar>& V_loc) { Vy_loc = V_loc * QZ; },
                  Vy, _V);
    _old_rank_Y = rank_Y;
    _ivec = rank_Y;
  }
  // Update deflation space:1 ends here

  // [[file:../../../org/composyx/linalg/SpectralExtraction.org::*Iteration][Iteration:1]]
public:
  /*
  void iteration(const Vect &Ap, const Vect&, const Vect&, const Scalar alpha, const Real rz) {
  Timer<TIMER_ITERATION> t("SpectralExtraction iteration");
  _Ap = &Ap;
  _alpha = alpha;
  _rho = std::sqrt(rz);
  }*/
  // Iteration:1 ends here

  // [[file:../../../org/composyx/linalg/SpectralExtraction.org::*Iteration end][Iteration end:1]]
  void iteration_end(const Vect& Ap, const Scalar alpha, const Scalar beta,
                     const Real rz) {
    Timer<TIMER_ITERATION> t("SpectralExtraction iteration_end");
    _alpha = alpha;
    _rho = std::sqrt(rz);
    _T(_ivec, _ivec) += Real{1.0} / _alpha;
    if (_just_restarted) {
      _w += Ap; // update w - Fig 3.1: 11.8, t part
      _T.get_block_view(0, _ivec, _ivec, 1) =
          dot_block(_V.get_vect_view(0, _ivec), _w) / _rho;
      _just_restarted = false;
    }
    _rho = std::sqrt(rz);
    if (_ivec == _solve_deflation_size - 1) {
      _update_deflation();
      _w = -beta * Ap; // setup w - Fig 3.1: 11.8, -beta * t_prec part
      _just_restarted = true;
    } else {
      _ivec += 1;
      _T(_ivec - 1, _ivec) = -std::sqrt(beta) / _alpha;
    }
    _V.get_vect_view(_ivec, /*nrhs*/ 1) = (*_z) / _rho;
    _T(_ivec, _ivec) = beta / _alpha;
  }
  // Iteration end:1 ends here

  // [[file:../../../org/composyx/linalg/SpectralExtraction.org::*Finalize][Finalize:1]]
  void finalize() {
    Timer<TIMER_ITERATION> t("SpectralExtraction finalize");
    //if(!_just_restarted) {
    if (_ivec <= _deflation_increment) {
      COMPOSYX_WARNING("Too few CG iterations only Lanczos vectors are return, "
                       "that should not be used for deflation");
      return;
    } else {
      // No deflation has been computed
      //if(_old_rank_Y == 0) _update_deflation();
    }
    //}
    if ((*_current_deflation_size) + _deflation_increment <=
        static_cast<int>(_V0_buffer->get_n_cols()) /*max_deflation_size*/) {
      _V0_buffer->get_vect_view(*_current_deflation_size,
                                _deflation_increment) =
          _V.get_vect_view(0, _deflation_increment);
      (*_current_deflation_size) += _deflation_increment;
    }
  }
}; // class SpectralExtraction
// Finalize:1 ends here

// [[file:../../../org/composyx/linalg/SpectralExtraction.org::*Traits][Traits:1]]
template <typename T>
struct has_spectral_extraction : public std::false_type {};
// Traits:1 ends here

// [[file:../../../org/composyx/linalg/SpectralExtraction.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
