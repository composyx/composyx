// [[file:../../../org/composyx/linalg/EigenArpackSolver.org::*Header][Header:1]]
#pragma once

#include <lapack.hh>
#include <blas.hh>
#define _COMPLEX_H 1
// Dirty but for some reason, arpack includes "complex.h"
// This define should avoid this include
#include <arpack.hpp>
#include <debug_c.hpp>

#include <exception>
#include "composyx/utils/Arithmetic.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/linalg/EigenArpackSolver.org::*Exception when convergence fails][Exception when convergence fails:1]]
struct ArpackNotConverged : public std::exception {
  const char* what() const throw() { return "Arpack convergence failed"; }
};
// Exception when convergence fails:1 ends here

// [[file:../../../org/composyx/linalg/EigenArpackSolver.org::*Class][Class:1]]
template <CPX_DenseMatrix OutMatrix, CPX_Matrix Matrix, CPX_Vector Vector,
          typename Solver, int force_use_mode = -1>
struct EigenArpackSolver {

  using Real = typename Vector::real_type;
  using Scalar = typename scalar_type<Matrix>::type;
  // Class:1 ends here

  // [[file:../../../org/composyx/linalg/EigenArpackSolver.org::*Generalized eigen problem][Generalized eigen problem:1]]
  [[nodiscard]] static std::pair<std::vector<Real>, OutMatrix>
  gen_eigh_smallest(const Matrix& A, const Matrix& B, int n_v, Real tol = -1) {
    Timer<TIMER_EVD> t("arpack_GEVD_pb");

    static_assert(is_dense<OutMatrix>::value,
                  "Arpack - generalized EVD only returns dense matrix");

    if (n_rows(A) <= 1)
      return std::make_pair(std::vector<Real>(), OutMatrix());

    COMPOSYX_ASSERT(A.is_sym_or_herm(),
                    "arpack_generalized_eigen_smallest: A.U = Lam.B.x only "
                    "implemented with A sym or herm");
    COMPOSYX_ASSERT(B.is_spd_or_hpd(),
                    "arpack_generalized_eigen_smallest: A.U = Lam.B.x only "
                    "implemented with B spd or hpd");

    // Choosing mode: MODE = 1 is supposed to be more efficient but it requires to be able to triangular solves separately
    // Cannot use MODE = 1 work with sparse at the moment (for instance MUMPS doesn't give access to triangular solve)

    constexpr auto mode_to_use = []() {
      if constexpr (force_use_mode == 1 || force_use_mode == 2)
        return static_cast<a_int>(force_use_mode);
#if __cplusplus > 201703L // C++20 version
      if constexpr (requires(Solver r_s, Vector r_v1, Vector r_v2) {
                      r_s.triangular_solve(r_v1, r_v2, MatrixStorage::lower,
                                           true);
                    }) {
        return 1;
      }
#else // C++ 17 version
      if constexpr (has_triangular_solve<Solver>::value) {
        return 1;
      }
#endif
      return 2;
    };

    constexpr const a_int MODE = mode_to_use();
    //std::cout << "MODE: " << MODE << '\n';
    //debug_c(6, -6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

    if (static_cast<int>(n_rows(A)) < n_v) {
      std::string warn_mess("Asking for ");
      warn_mess += std::to_string(n_v) +
                   std::string(" eigenvectors in matrix of size ") +
                   std::to_string(n_rows(A));
      COMPOSYX_WARNING(warn_mess);
      n_v = n_rows(A);
    }

    COMPOSYX_ASSERT((A.is_symmetric() || A.is_hermitian()),
                    "Eigen solve : only implemented for sym/herm matrices");

    a_int ido = 0; // Instruction to perfrom (reverse communication)
    constexpr const arpack::bmat bmat =
        (MODE == 1) ? arpack::bmat::identity : arpack::bmat::generalized;
    const a_int N = static_cast<a_int>(n_rows(A));
    const a_int nev = std::min(N - 1, static_cast<a_int>(n_v));
    if (tol <= 0) {
      tol = is_precision_double<Scalar>::value ? 1e-14 : 1e-6;
    } else {
      if (!is_precision_double<Scalar>::value && tol < 1e-8) {
        COMPOSYX_WARNING(
            "arpack gen. evd : tolerance lower than 1e-8 in single precision");
      }
    }

    Vector resid(N);
    const a_int ncv = std::min(2 * nev + 1, N); // Muse be in [2 * nev, N]
    std::vector<Scalar> V(ncv * N);
    const a_int ldv = N;

    std::array<a_int, 11> iparam{};
    iparam[0] = 1;      // Shift provided by arpack
    iparam[2] = 10 * N; // Max iter
    iparam[3] = 1;      // Only 1 supported
    iparam[4] = 0;      // Number of ev found by arpack.

    //If M can be factored into a Cholesky factorization M = LL`
    //then Mode = 2 should not be selected.  Instead one should use
    //Mode = 1 with  OP = inv(L)*A*inv(L`)

    // Mode 1:  A*x = lambda*x, A symmetric
    //          ===> OP = A  and  B = I.
    // Mode 2:  A*x = lambda*M*x, A symmetric, M symmetric positive definite
    //          ===> OP = inv[M]*A  and  B = M.

    iparam[6] = MODE;

    std::array<a_int, 14>
        ipntr{}; // Not sure why 14 (see arpack-ng/TESTS/icb_arpack_cpp.cpp)

    std::vector<Scalar> workd(3 * N);
    const a_int lworkl = ncv * (3 * ncv + 8); // Must be >= NCV**2 + 8*NCV
    std::vector<Scalar> workl(lworkl);
    a_int info = 0;
    // Factorize B with Cholesky
    Solver solver(B);

    // SEUPD extra parameters
    const a_int rvec = 1; // Get eigenvectors

    OutMatrix U(N, nev);
    std::vector<Scalar> d_array(nev);
    Scalar* z_ptr = get_ptr(U);
    Scalar* d_ptr = d_array.data();
    const a_int ldz = get_leading_dim(U);
    Scalar sigma = Scalar{0};
    std::vector<a_int> select(ncv); // Used as workspace

    const size_t Vsize = static_cast<size_t>(N);

    while (ido != 99) {
      arpack::saupd(ido, bmat, N, arpack::which::smallest_magnitude, nev, tol,
                    get_ptr(resid), ncv, V.data(), ldv, iparam.data(),
                    ipntr.data(), workd.data(), workl.data(), lworkl, info);

      if (info != 0) {
        std::string warn("arpack::saupd returned with info = ");
        warn += std::to_string(info);
        COMPOSYX_WARNING(warn);
      }
      //std::cerr << "Returned IDO: " << ido << '\n';

      Vector X(DenseData<Scalar, 1>(Vsize, &(workd[ipntr[0] - 1])), true);
      Vector Y(DenseData<Scalar, 1>(Vsize, &(workd[ipntr[1] - 1])), true);

      if (ido == 99) {
        break;
      }

      if constexpr (MODE == 1) {
        // With B = L . L^T :
        // Compute Y <- L^{-1} . A . L^{-T} . X
        solver.triangular_solve(X, Y, MatrixStorage::lower, true);
        X = A * Y;
        solver.triangular_solve(X, Y, MatrixStorage::lower, false);
      } else if constexpr (MODE == 2) {
        if (ido == 1 or ido == -1) {
          X = A * X;
          solver.apply(X, Y);
        } else if (ido == 2) {
          Y = B * X;
        }
      }
    }

    // Check we found enough eigenvalues
    if (iparam[4] < nev)
      throw ArpackNotConverged();

    arpack::seupd(rvec, arpack::howmny::ritz_vectors, select.data(), d_ptr,
                  z_ptr, ldz,
                  sigma, // Here we must copy parameters passed to saupd
                  bmat, N, arpack::which::smallest_magnitude, nev, tol,
                  get_ptr(resid), ncv, V.data(), ldv, iparam.data(),
                  ipntr.data(), workd.data(), workl.data(), lworkl, info);
    if (info != 0) {
      std::string warn("arpack::seupd returned with info = ");
      warn += std::to_string(info);
      COMPOSYX_WARNING(warn);
    }

    std::vector<Real> lambda(nev);
    for (a_int k = 0; k < nev; ++k) {
      lambda[k] = static_cast<Real>(d_array[k]);
    }

    //std::cout << "Lambdas:\n";
    //for(auto l : lambda) std::cout << l << ", ";
    //std::cout << '\n';

    // See remark 3 of dsaupd.f
    // After convergence, an approximate
    // eigenvector z of the original problem is recovered by solving
    // L`z = x  where x is a Ritz vector of OP.
    if constexpr (MODE == 1) {
      for (a_int j = 0; j < nev; ++j) {
        auto Uj = U.get_vect_view(j);
        auto tmp = U.get_vect(j);
        solver.triangular_solve(tmp, Uj, MatrixStorage::lower, true);
      }
    }

    constexpr const Real prune_tol =
        is_precision_double<Scalar>::value ? 1e-15 : 5e-7;
    for (size_t j = 0; j < n_cols(U); ++j) {
      for (size_t i = 0; i < n_rows(U); ++i) {
        if (std::abs(U(i, j)) < prune_tol)
          U(i, j) = Scalar{0};
      }
    }

    return std::make_pair(lambda, U);
  }
  // Generalized eigen problem:1 ends here

  // [[file:../../../org/composyx/linalg/EigenArpackSolver.org::*Footer][Footer:1]]
}; // struct EigenArpackSolver
} // namespace composyx
// Footer:1 ends here
