// [[file:../../../org/composyx/linalg/MatrixNorm.org::*Approximate matrix norm][Approximate matrix norm:1]]
#pragma once

#include "../utils/Arithmetic.hpp"
#include <random>

namespace composyx {

template <typename Matrix, typename Vector = typename vector_type<Matrix>::type,
          typename Scalar = typename scalar_type<Matrix>::type,
          typename Real = typename arithmetic_real<Scalar>::type>
Real approximate_mat_norm(const Matrix& A, size_t nb_rand_vect = 100,
                          int seed = 0) {

  if (seed == 0) {
    std::random_device rd;
    seed = rd();
  }

  std::mt19937 gen(seed);
  std::uniform_real_distribution<> dis(double{-1}, double{1});

  auto random_scalar = [&]() {
    if constexpr (is_complex<Scalar>::value) {
      Real re = static_cast<Real>(dis(gen));
      Real im = static_cast<Real>(dis(gen));
      return Scalar{re, im};
    } else {
      return static_cast<Scalar>(dis(gen));
    }
  };

  auto M = n_rows(A);

  Real approx_norm_sq = Real{0};
  for (size_t k = 0; k < nb_rand_vect; ++k) {
    Vector u(M);
    Scalar sum_squares = Scalar{0};

    for (int i = 0; i < static_cast<int>(M); ++i) {
      u[i] = random_scalar();
      sum_squares += conj(u[i]) * u[i];
    }

    Scalar inv_norm = Scalar{1.0} / Scalar{std::sqrt(std::real(sum_squares))};

    for (int i = 0; i < static_cast<int>(M); ++i) {
      u[i] *= inv_norm;
    }

    Vector mv = A * u;
    approx_norm_sq = std::max(approx_norm_sq, std::real(dot(mv, mv)));
  }

  return std::sqrt(approx_norm_sq);
}

} // end namespace composyx
// Approximate matrix norm:1 ends here
