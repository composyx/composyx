// [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Header][Header:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>

namespace composyx {
template <CPX_Scalar, CPX_Integral = int> class SparseMatrixCOO;
template <class, class> struct COOIterator;

template <CPX_Scalar, CPX_Integral, Op> struct OpSpMatCOO;

template <Op op, CPX_Scalar Scalar, CPX_Integral Index>
inline void apply_op(SparseMatrixCOO<Scalar, Index>& mat) {
  if constexpr (op == Op::NoTrans) {
    return;
  } else if constexpr (op == Op::Trans) {
    mat.transpose();
  } else if constexpr (op == Op::ConjTrans) {
    mat.conj_transpose();
  }
}
} // namespace composyx

#include "composyx/loc_data/SparseMatrixBase.hpp"
#include "composyx/loc_data/SparseMatrixLIL.hpp"
#include "composyx/loc_data/SparseMatrixCSC.hpp"

#ifndef COMPOSYX_NO_MPI
#include "composyx/dist/MPI.hpp"
#endif

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Attributes][Attributes:1]]
template <CPX_Scalar Scalar, CPX_Integral Index>
class SparseMatrixCOO : public SparseMatrixBase<Scalar, Index> {
public:
  using local_type = SparseMatrixCOO<Scalar, Index>;

private:
  using BaseT = SparseMatrixBase<Scalar, Index>;

  using Real = typename BaseT::Real;
  using Idx_arr = typename BaseT::Idx_arr;
  using Scal_arr = typename BaseT::Scal_arr;

  using BaseT::_m;
  using BaseT::_n;
  using BaseT::_nnz;

  using BaseT::_i;
  using BaseT::_j;
  using BaseT::_v;

  bool _sorted = false;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Check consistency for attributes][Check consistency for attributes:1]]
  // Check that values in i < M, in j < N
  void check_values_range() {
    COMPOSYX_ASSERT(elts_lower_than(_i, static_cast<Index>(_m)),
                    "CSC: value in i >= m");
    COMPOSYX_ASSERT(elts_lower_than(_j, static_cast<Index>(_n)),
                    "CSC: value in i >= m");
  }
  // Check consistency for attributes:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Check dimensions][Check dimensions:1]]
  virtual void check_dimensions() {
    COMPOSYX_DIM_ASSERT(
        _i.size(), _nnz,
        "SparseMatrixCOO:: size(i) shoule be the number of non-zero elements");
    COMPOSYX_DIM_ASSERT(
        _j.size(), _nnz,
        "SparseMatrixCOO:: size(j) shoule be the number of non-zero elements");
    COMPOSYX_DIM_ASSERT(
        _v.size(), _nnz,
        "SparseMatrixCOO:: size(v) shoule be the number of non-zero elements");
  }
  // Check dimensions:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Order indices][Order indices:1]]
public:
  // Check that values in i < M, in j < N
  void order_indices() {
    if (_sorted)
      return;
    std::vector<size_t> idx = multilevel_argsort<std::vector<size_t>>(_j, _i);
    apply_permutation(idx, _i, _j, _v);
    _sorted = true;
  }
  // Order indices:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Constructors][Constructors:1]]
public:
  explicit SparseMatrixCOO() : BaseT() {}
  explicit SparseMatrixCOO(const size_t m, const size_t n) : BaseT() {
    _m = m;
    _n = n;
  }

  explicit SparseMatrixCOO(const size_t m, const size_t n, const size_t nnz,
                           const Index* i, const Index* j, const Scalar* v)
      : BaseT{m,
              n,
              nnz,
              std::span(i, nnz),
              std::span(j, nnz),
              std::span(v, nnz)} {
    check_values_range();
  }

  explicit SparseMatrixCOO(const size_t m, const size_t n, const size_t nnz,
                           const Idx_arr& i, const Idx_arr& j,
                           const Scal_arr& v)
      : BaseT{m, n, nnz, i, j, v} {
    check_dimensions();
    check_values_range();
  }

  explicit SparseMatrixCOO(const size_t m, const size_t n, const size_t nnz,
                           Idx_arr&& i, Idx_arr&& j, Scal_arr&& v)
      : BaseT{m, n, nnz, std::move(i), std::move(j), std::move(v)} {
    check_dimensions();
    check_values_range();
  }

  // Initialize with a triplet of vectors ({i}, {j}, {v})
  // Guess m and n as maximums of i and j (if not given)
  // To be used for small matrices (quick testing)
  explicit SparseMatrixCOO(std::initializer_list<Index> i,
                           std::initializer_list<Index> j,
                           std::initializer_list<Scalar> v, const size_t m = 0,
                           const size_t n = 0) {
    _m = m;
    _n = n;
    _nnz = i.size();
    COMPOSYX_ASSERT(_nnz == j.size(),
                    "Initialize sparse matrix with size(i) != size(j)");
    COMPOSYX_ASSERT(_nnz == v.size(),
                    "Initialize sparse matrix with size(i) != size(v)");
    // Auto-detect matrix dimensions with max (when not given by the user)
    if (_m == 0 && _nnz != 0) {
      for (auto k : i) {
        _m = std::max(k, static_cast<Index>(_m));
      }
      ++_m; // Because starting at 0
    }
    if (_n == 0 && _nnz != 0) {
      for (auto k : j) {
        _n = std::max(k, static_cast<Index>(_n));
      }
      ++_n; // Because starting at 0
    }
    _i = Idx_arr(i.begin(), i.end());
    _j = Idx_arr(j.begin(), j.end());
    _v = Scal_arr(v.begin(), v.end());

    check_values_range();
  }

  // From a dense matrix
  template <typename DMat>
    requires requires(DMat d, int i, int j) {
      { n_rows(d) } -> std::integral;
      { n_cols(d) } -> std::integral;
      { d(i, j) } -> std::convertible_to<Scalar>;
    }
  SparseMatrixCOO(const DMat& dm, Real drop = 1e-15) {
    _m = n_rows(dm);
    _n = n_cols(dm);
    _nnz = 0;

    // Count nnz
    if (dm.is_storage_full()) {
      for (size_t j = 0; j < _n; ++j) {
        for (size_t i = 0; i < _m; ++i) {
          if (std::abs(dm(i, j)) > drop)
            ++_nnz;
        }
      }
    } else if (dm.is_storage_lower()) {
      for (size_t j = 0; j < _n; ++j) {
        for (size_t i = j; i < _m; ++i) {
          if (std::abs(dm(i, j)) > drop)
            ++_nnz;
        }
      }
    } else {
      for (size_t j = 0; j < _n; ++j) {
        for (size_t i = 0; i <= std::min(_m - 1, j); ++i) {
          if (std::abs(dm(i, j)) > drop)
            ++_nnz;
        }
      }
    }

    // Create and fill ijv
    _i = Idx_arr(_nnz);
    _j = Idx_arr(_nnz);
    _v = Scal_arr(_nnz);
    auto k = 0;

    auto add_ijv = [&](size_t i, size_t j) {
      if (std::abs(dm(i, j)) > drop) {
        _i[k] = i;
        _j[k] = j;
        _v[k] = dm(i, j);
        k++;
      }
    };

    // Count nnz
    if (dm.is_storage_full()) {
      for (size_t j = 0; j < _n; ++j) {
        for (size_t i = 0; i < _m; ++i) {
          add_ijv(i, j);
        }
      }
    } else if (dm.is_storage_lower()) {
      for (size_t j = 0; j < _n; ++j) {
        for (size_t i = j; i < _m; ++i) {
          add_ijv(i, j);
        }
      }
    } else {
      for (size_t j = 0; j < _n; ++j) {
        for (size_t i = 0; i <= std::min(_m - 1, j); ++i) {
          add_ijv(i, j);
        }
      }
    }

    this->copy_properties(dm);
  }

  SparseMatrixCOO(const SparseMatrixCSC<Scalar, Index>& csc_mat) {
    this->from_csc(csc_mat);
  }
  // Constructors:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Apply function on IJV][Apply function on IJV:1]]
  template <CPX_FuncIJV<Index, Scalar> Func> void foreach_ijv(Func f) const {
    for (Index k = 0; k < static_cast<Index>(_nnz); ++k) {
      f(_i[k], _j[k], _v[k]);
    }
  }

  template <CPX_FuncIJV<Index, Scalar> Func>
  void foreach_ijv_max_i(Func f, size_t max_i) const {
    const Index mi = static_cast<Index>(max_i);
    for (Index k = 0; k < static_cast<Index>(_nnz); ++k) {
      if (_i[k] >= mi)
        continue;
      f(_i[k], _j[k], _v[k]);
    }
  }

  template <CPX_FuncIJV<Index, Scalar> Func>
  void foreach_ijv_max_j(Func f, size_t max_j) const {
    const Index mj = static_cast<Index>(max_j);
    for (Index k = 0; k < static_cast<Index>(_nnz); ++k) {
      if (_j[k] >= mj)
        continue;
      f(_i[k], _j[k], _v[k]);
    }
  }
  // Apply function on IJV:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Factories][Factories:1]]
  static SparseMatrixCOO eye(const size_t M, const size_t N) {
    size_t nnz = std::min(M, N);
    Scal_arr v(nnz);
    std::ranges::fill(v, Scalar{1});
    SparseMatrixCOO out(M, N, nnz, arange<Idx_arr>(nnz), arange<Idx_arr>(nnz),
                        std::move(v));
    return out;
  }

  static SparseMatrixCOO identity(const size_t M) {
    return SparseMatrixCOO::eye(M, M);
  }

  static SparseMatrixCOO from_matrix_market(const std::string& filename) {
    MatrixSymmetry sym;
    MatrixStorage stor;
    size_t m, n, nnz;
    Idx_arr i, j;
    Scal_arr v;
    matrix_market::load(filename, i, j, v, m, n, nnz, sym, stor);
    SparseMatrixCOO out(m, n, nnz, std::move(i), std::move(j), std::move(v));
    out.set_property(sym);
    out.set_property(stor);
    return out;
  }
  // Factories:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Generic SparseMatrix methods][Generic SparseMatrix methods:1]]
  [[nodiscard]] DenseMatrix<Scalar> to_dense() const {
    return sparse_matrix_to_dense(*this);
  }

  [[nodiscard]] DiagonalMatrix<Scalar> diag() const {
    return sparse_matrix_diag(*this);
  }

  [[nodiscard]] Vector<Scalar> diag_vect() const {
    return sparse_matrix_diag_vect(*this);
  }

  SparseMatrixCOO& operator*=(const SparseMatrixCOO& other) {
    sparse_matrix_spmm(*this, other);
    return *this;
  }

  SparseMatrixCOO& operator*=(const SparseMatrixCSR<Scalar, Index>& other) {
    sparse_matrix_spmm(*this, other);
    return *this;
  }

  SparseMatrixCOO& operator*=(const SparseMatrixCSC<Scalar, Index>& other) {
    sparse_matrix_spmm(*this, other);
    return *this;
  }

  void right_diagmm(const DiagonalMatrix<Scalar>& other) {
    sparse_matrix_right_diagmm(*this, other);
  }

  void left_diagmm(const DiagonalMatrix<Scalar>& other) {
    sparse_matrix_left_diagmm(*this, other);
  }

  COMPOSYX_SPARSE_MATRIX_CAST_FUNCTION(SparseMatrixCOO)
  // Generic SparseMatrix methods:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Conversion functions][Conversion functions:1]]
  // Conversion
  template <typename InIndex = Index>
  void from_csc(const SparseMatrixCSC<Scalar, InIndex>& mat) {
    _m = n_rows(mat);
    _n = n_cols(mat);
    _nnz = n_nonzero(mat);

    _i = Idx_arr(_nnz);
    _j = Idx_arr(_nnz);
    _v = Scal_arr(_nnz);

    const InIndex* i = mat.get_i_ptr();
    const InIndex* j = mat.get_j_ptr();
    const Scalar* v = mat.get_v_ptr();

    for (size_t k = 0; k < _n; ++k) {
      for (Index k2 = j[k]; k2 < j[k + 1]; ++k2) {
        _i[k2] = static_cast<Index>(i[k2]);
        _j[k2] = static_cast<Index>(k);
        _v[k2] = v[k2];
      }
    }
    _sorted = true;
    this->copy_properties(mat);
  }

  template <typename InIndex = Index>
  void from_csr(const SparseMatrixCSR<Scalar, InIndex>& mat) {
    _m = n_rows(mat);
    _n = n_cols(mat);
    _nnz = n_nonzero(mat);

    _i = Idx_arr(_nnz);
    _j = Idx_arr(_nnz);
    _v = Scal_arr(_nnz);

    const InIndex* i = mat.get_i_ptr();
    const InIndex* j = mat.get_j_ptr();
    const Scalar* v = mat.get_v_ptr();

    for (size_t k = 0; k < _m; ++k) {
      for (Index k2 = i[k]; k2 < i[k + 1]; ++k2) {
        _i[k2] = static_cast<Index>(k);
        _j[k2] = static_cast<Index>(j[k2]);
        _v[k2] = v[k2];
      }
    }
    _sorted = false;
    this->copy_properties(mat);
  }

  template <typename InIndex = Index>
  void from_csr(std::span<const InIndex> csri, std::span<const InIndex> csrj,
                std::span<const Scalar> csrv, size_t csrm, size_t csrn) {
    _m = csrm;
    _n = csrn;
    _nnz = csrv.size();

    _i = Idx_arr(_nnz);
    _j = Idx_arr(_nnz);
    _v = Scal_arr(_nnz);

    const InIndex* i = csri.data();
    const InIndex* j = csrj.data();
    const Scalar* v = csrv.data();

    for (size_t k = 0; k < _m; ++k) {
      for (Index k2 = i[k]; k2 < i[k + 1]; ++k2) {
        _i[k2] = static_cast<Index>(k);
        _j[k2] = static_cast<Index>(j[k2]);
        _v[k2] = v[k2];
      }
    }
  }

  // Conversion
  void from_lil(const SparseMatrixLIL<Scalar, Index>& lilmat) {
    (*this) = std::move(lilmat.to_coo());
  }

  SparseMatrixCSC<Scalar, Index> to_csc() const {
    SparseMatrixCSC<Scalar, Index> out;
    out.from_coo(*this);
    return out;
  }

  SparseMatrixCSR<Scalar, Index> to_csr() const {
    SparseMatrixCSR<Scalar, Index> out;
    out.from_coo(*this);
    return out;
  }

  template <typename OutIndex = Index>
  SparseMatrixCOO<Scalar, OutIndex> to_coo() const {
    if constexpr (std::is_same<Index, OutIndex>::value) {
      return *this;
    } else {
      std::vector<OutIndex> iarr(_nnz);
      std::vector<OutIndex> jarr(_nnz);
      std::vector<Scalar> varr(_nnz);
      for (size_t k = 0; k < _nnz; ++k) {
        iarr[k] = static_cast<OutIndex>(_i[k]);
        jarr[k] = static_cast<OutIndex>(_j[k]);
        varr[k] = _v[k];
      }
      auto outmat = SparseMatrixCOO<Scalar, OutIndex>(
          _m, _n, _nnz, std::move(iarr), std::move(jarr), std::move(varr));
      outmat.copy_properties(*this);
      return outmat;
    }
  }

  void convert(SparseMatrixCOO& out_mat) const { out_mat = *this; }
  void convert(SparseMatrixCSC<Scalar>& out_mat) const {
    out_mat = this->to_csc();
  }
  void convert(SparseMatrixCSR<Scalar>& out_mat) const {
    out_mat = this->to_csr();
  }
  void convert(DenseMatrix<Scalar>& out_mat) const {
    out_mat = this->to_dense();
  }
  // Conversion functions:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Load matrix from a matrix market file][Load matrix from a matrix market file:1]]
  void from_matrix_market_file(const std::string& filename) {
    MatrixSymmetry sym;
    MatrixStorage stor;
    matrix_market::load(filename, _i, _j, _v, _m, _n, _nnz, sym, stor);
    this->set_property(sym);
    this->set_property(stor);
    check_values_range();
  }
  // Load matrix from a matrix market file:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Dump to matrix market file][Dump to matrix market file:1]]
  void dump_to_matrix_market(const std::string& filename) const {
    matrix_market::dump<Idx_arr, Scal_arr>(filename, _i, _j, _v, _m, _n,
                                           this->get_symmetry(),
                                           this->get_storage_type());
  }
  // Dump to matrix market file:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Comparison functions][Comparison functions:1]]
  bool operator==(const SparseMatrixCOO& spmat2) const {

    SparseMatrixCOO *sp1, *sp2;
    SparseMatrixCOO sp_data1, sp_data2;

    // Copy if not sorted
    if (this->_sorted) {
      sp1 = const_cast<SparseMatrixCOO*>(this);
    } else {
      sp_data1 = *this;
      sp1 = &sp_data1;
      sp1->order_indices();
    }

    if (spmat2._sorted) {
      sp2 = const_cast<SparseMatrixCOO*>(&spmat2);
    } else {
      sp_data2 = spmat2;
      sp2 = &sp_data2;
      sp2->order_indices();
    }

    if (sp1->_m != sp2->_m)
      return false;
    if (sp1->_n != sp2->_n)
      return false;
    if (sp1->_nnz != sp2->_nnz)
      return false;

    if (sp1->_i != sp2->_i)
      return false;
    if (sp1->_j != sp2->_j)
      return false;
    if (sp1->_v != sp2->_v)
      return false;

    return true;
  }

  bool operator!=(const SparseMatrixCOO& spmat2) const {
    return !(*this == spmat2);
  }
  // Comparison functions:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Comparison functions][Comparison functions:2]]
  bool equals(const SparseMatrixCOO& spmat2) {
    this->order_indices();
    return (*this == spmat2);
  }

  bool equals(SparseMatrixCOO& spmat2) {
    this->order_indices();
    spmat2.order_indices();
    return (*this == spmat2);
  }
  // Comparison functions:2 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Re-indexing][Re-indexing:1]]
  template <CPX_IntArray I> void reindex(const I& new_indices, int new_m = -1) {
    if (new_m != -1) {
      _m = new_m;
      _n = new_m;
    }

    for (size_t k = 0; k < _nnz; ++k) {
      _i[k] = new_indices[_i[k]];
      _j[k] = new_indices[_j[k]];
    }
    correct_storage();
  }

  template <CPX_IntArray I>
  void reindex(const I& new_indices, const I& old_indices, int new_m = -1) {
    if (new_m != -1) {
      _m = new_m;
      _n = new_m;
    }

    for (size_t k = 0; k < _nnz; ++k) {
      for (size_t k2 = 0; k2 < old_indices.size(); ++k2) {
        if (_i[k] == old_indices[k2]) {
          _i[k] = new_indices[k2];
          break;
        }
      }
    }

    for (size_t k = 0; k < _nnz; ++k) {
      for (size_t k2 = 0; k2 < old_indices.size(); ++k2) {
        if (_j[k] == old_indices[k2]) {
          _j[k] = new_indices[k2];
          break;
        }
      }
    }
    correct_storage();
  }

  void correct_storage() {
    if (this->is_storage_lower()) {
      for (size_t k = 0; k < _nnz; ++k) {
        if (_i[k] < _j[k])
          std::swap(_i[k], _j[k]);
      }
      _sorted = false;
    } else if (this->is_storage_upper()) {
      for (size_t k = 0; k < _nnz; ++k) {
        if (_i[k] > _j[k])
          std::swap(_i[k], _j[k]);
      }
      _sorted = false;
    }
  }
  // Re-indexing:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Fill matrix from half storage to full storage][Fill matrix from half storage to full storage:1]]
  void fill_half_to_full_storage() {
    if (this->is_storage_full()) {
      return;
    }
    if (this->is_general()) {
      return;
    }

    int new_nnz = 0;
    for (size_t k = 0; k < _nnz; ++k) {
      new_nnz++;
      if (_i[k] != _j[k])
        new_nnz++;
    }

    Idx_arr ii(new_nnz);
    Idx_arr jj(new_nnz);
    Scal_arr vv(new_nnz);

    int idx = 0;
    for (size_t k = 0; k < _nnz; ++k) {
      ii[idx] = _i[k];
      jj[idx] = _j[k];
      vv[idx] = _v[k];
      idx++;
      if (_i[k] != _j[k]) {
        ii[idx] = _j[k];
        jj[idx] = _i[k];
        if constexpr (is_complex<Scalar>::value) {
          if (this->is_hermitian()) {
            vv[idx] = conj<Scalar>(_v[k]);
          } else {
            vv[idx] = _v[k];
          }
        } else {
          vv[idx] = _v[k];
        }
        idx++;
      }
    }

    this->set_property(MatrixStorage::full);
    _nnz = new_nnz;
    _i = std::move(ii);
    _j = std::move(jj);
    _v = std::move(vv);
  }
  // Fill matrix from half storage to full storage:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Make storage half][Make storage half:1]]
  void to_storage_half(MatrixStorage storage = MatrixStorage::lower) {
    COMPOSYX_ASSERT(
        this->is_symmetric() || this->is_hermitian(),
        "SparseMatrixCOO: asking for half storage with non symmetric matrix");
    if (this->get_storage_type() == storage) {
      return;
    }
    if ((this->get_storage_type() == MatrixStorage::lower &&
         storage == MatrixStorage::upper) ||
        (this->get_storage_type() == MatrixStorage::upper &&
         storage == MatrixStorage::lower)) {
      auto sym = this->get_symmetry();
      this->set_property(MatrixStorage::full, MatrixSymmetry::general);
      (*this) = this->t();
      this->set_property(sym, storage);
      return;
    }

    auto indices_to_keep = arange<std::vector<size_t>>(_i.size());

    if (storage == MatrixStorage::lower) {
      auto e = std::ranges::remove_if(
          indices_to_keep, [this](size_t k) { return _i[k] < _j[k]; });
      indices_to_keep.erase(e.begin(), e.end());
    } else {
      auto e = std::ranges::remove_if(
          indices_to_keep, [this](size_t k) { return _i[k] > _j[k]; });
      indices_to_keep.erase(e.begin(), e.end());
    }

    _i = subarray(_i, indices_to_keep);
    _j = subarray(_j, indices_to_keep);
    _v = subarray(_v, indices_to_keep);
    _nnz = _i.size();
    this->set_property(storage);
  }
  // Make storage half:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Element accessor][Element accessor:1]]
private:
  bool _search_sorted_ijv(const Index i, const Index j,
                          Index& value_idx) const {
    Index j_idx = searchsorted(_j, j);
    for (Index k = j_idx; k < static_cast<Index>(_i.size()); ++k) {
      if (_j[k] != j)
        break;
      if (_i[k] == i) {
        value_idx = k;
        return true;
      }
    }
    return false;
  }

  void coeff_swap(Index& i, Index& j) const {
    if ((this->is_storage_lower() && (i < j)) or
        (this->is_storage_upper() && (i > j))) {
      std::swap(i, j);
    }
  }

public:
  // Element accessor (access is linear in nnz if not sorted !!!)
  [[nodiscard]] Scalar coeff(Index i, Index j) const {
    coeff_swap(i, j);
    if (_sorted) {
      Index v_idx;
      if (_search_sorted_ijv(i, j, v_idx))
        return _v[v_idx];
    } else {
      for (size_t k = 0; k < _nnz; ++k) {
        if (_i[k] == i && _j[k] == j)
          return _v[k];
      }
    }
    return Scalar{0};
  }

  [[nodiscard]] Scalar coeff(Index i, Index j) {
    coeff_swap(i, j);
    if (!_sorted)
      order_indices();
    Index v_idx;
    if (_search_sorted_ijv(i, j, v_idx))
      return _v[v_idx];
    return Scalar{0};
  }

  // Element accessor (access is linear in nnz if not sorted !!!)
  [[nodiscard]] const Scalar& coeffRef(Index i, Index j) const {
    coeff_swap(i, j);
    if (_sorted) {
      Index v_idx;
      if (_search_sorted_ijv(i, j, v_idx))
        return _v[v_idx];
      throw ComposyxAccessZeroValue(
          "const SparseMatrixCOO(i,j) does not point to a non zero value");
    } else {
      for (size_t k = 0; k < _nnz; ++k) {
        if (_i[k] == i && _j[k] == j)
          return _v[k];
      }
      throw ComposyxAccessZeroValue(
          "const SparseMatrixCOO(i,j) does not point to a non zero value");
    }
    return _v[0];
  }

  [[nodiscard]] Scalar& coeffRef(Index i, Index j) {
    coeff_swap(i, j);
    if (!_sorted)
      order_indices();
    Index v_idx;
    if (_search_sorted_ijv(i, j, v_idx))
      return _v[v_idx];
    throw ComposyxAccessZeroValue(
        "SparseMatrixCOO(i,j) does not point to a non zero value");
    return _v[0];
  }

  [[nodiscard]] const Scalar& operator()(Index i, Index j) const {
    return this->coeffRef(i, j);
  }
  [[nodiscard]] Scalar& operator()(Index i, Index j) {
    return this->coeffRef(i, j);
  }
  // Element accessor:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Memory cost][Memory cost:1]]
  inline size_t memcost() {
    constexpr size_t elt_size = (2 * sizeof(Index) + sizeof(Scalar));
    return elt_size * _nnz;
  }
  // Memory cost:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Serialization][Serialization:1]]
  std::vector<char> serialize() const {
    // M, N, values
    char prop = this->properties_serialize();
    int M = static_cast<int>(_m);
    int N = static_cast<int>(_n);
    int NNZ = static_cast<int>(_nnz);
    std::vector<char> buffer(1 + (3 + 2 * _nnz) * sizeof(int) +
                             _nnz * sizeof(Scalar));
    int cnt = 0;

    std::memcpy(&buffer[cnt], &prop, sizeof(char));
    cnt += sizeof(char);
    std::memcpy(&buffer[cnt], &M, sizeof(int));
    cnt += sizeof(int);
    std::memcpy(&buffer[cnt], &N, sizeof(int));
    cnt += sizeof(int);
    std::memcpy(&buffer[cnt], &NNZ, sizeof(int));
    cnt += sizeof(int);
    std::memcpy(&buffer[cnt], &_i[0], _nnz * sizeof(int));
    cnt += _nnz * sizeof(int);
    std::memcpy(&buffer[cnt], &_j[0], _nnz * sizeof(int));
    cnt += _nnz * sizeof(int);
    std::memcpy(&buffer[cnt], &_v[0], _nnz * sizeof(Scalar));

    return buffer;
  }

  void deserialize(const std::vector<char>& buffer) {
    int M, N, NNZ;
    int cnt = 0;
    char prop;
    std::memcpy(&prop, &buffer[cnt], sizeof(char));
    cnt += sizeof(char);
    std::memcpy(&M, &buffer[cnt], sizeof(int));
    cnt += sizeof(int);
    std::memcpy(&N, &buffer[cnt], sizeof(int));
    cnt += sizeof(int);
    std::memcpy(&NNZ, &buffer[cnt], sizeof(int));
    cnt += sizeof(int);
    this->properties_deserialize(prop);
    _m = static_cast<size_t>(M);
    _n = static_cast<size_t>(N);
    _nnz = static_cast<size_t>(NNZ);
    _i = Idx_arr(_nnz);
    _j = Idx_arr(_nnz);
    _v = Scal_arr(_nnz);

    std::memcpy(&_i[0], &buffer[cnt], _nnz * sizeof(int));
    cnt += _nnz * sizeof(int);
    std::memcpy(&_j[0], &buffer[cnt], _nnz * sizeof(int));
    cnt += _nnz * sizeof(int);
    std::memcpy(&_v[0], &buffer[cnt], _nnz * sizeof(Scalar));

    _sorted = false;
  }
  // Serialization:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Operators][Operators:1]]
  // Scalar multiplication
  using SparseMatrixBase<Scalar, Index>::operator*=;

  template <CPX_IJV_SparseMatrix OtherMatrix>
  SparseMatrixCOO& operator+=(const OtherMatrix& other) {
    sparse_matrix_addition(*this, other);
    return *this;
  }

  template <CPX_IJV_SparseMatrix OtherMatrix>
  SparseMatrixCOO& operator-=(const OtherMatrix& other) {
    sparse_matrix_addition(*this, other, Scalar{-1});
    return *this;
  }

  SparseMatrixCOO& operator+=(const DiagonalMatrix<Scalar>& other) {
    sparse_matrix_diagonal_addition(*this, other);
    return *this;
  }

  SparseMatrixCOO& operator-=(const DiagonalMatrix<Scalar>& other) {
    sparse_matrix_diagonal_addition(*this, other, Scalar{-1});
    return *this;
  }

  SparseMatrixCOO& operator*=(const DiagonalMatrix<Scalar>& other) {
    this->right_diagmm(other);
    return *this;
  }
  // Operators:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Display function][Display function:1]]
  // Pretty printing
  void display(const std::string& name = "",
               std::ostream& out = std::cout) const {
    if (!name.empty())
      out << name << '\n';
    out << "m: " << _m << " "
        << "n: " << _n << " "
        << "nnz: " << _nnz << '\n'
        << this->properties_str() << '\n'
        << '\n';

    out << "i\tj\tv" << '\n' << '\n';
    for (size_t k = 0; k < _nnz; ++k) {
      out << _i[k] << "\t" << _j[k] << "\t" << _v[k] << '\n';
    }
    out << '\n';
  }

  friend std::ostream& operator<<(std::ostream& out,
                                  const SparseMatrixCOO& coom) {
    coom.display("", out);
    return out;
  }
  // Display function:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Transposition][Transposition:1]]
  void transpose() {
    if (this->is_symmetric())
      return;
    std::swap(_i, _j);
    std::swap(_m, _n);
    _sorted = false;
  }

  void conj_transpose() {
    if (this->is_hermitian())
      return;
    this->transpose();
    if constexpr (is_complex<Scalar>::value) {
      std::ranges::for_each(_v, [](Scalar& s) { s = conj<Scalar>(s); });
    }
  }

  [[nodiscard]] auto t() const {
    return OpSpMatCOO<Scalar, Index, Op::Trans>(*this);
  }

  [[nodiscard]] auto h() const {
    return OpSpMatCOO<Scalar, Index, Op::ConjTrans>(*this);
  }

  template <Op op>
  SparseMatrixCOO(const OpSpMatCOO<Scalar, Index, op>& opmat)
      : SparseMatrixCOO(opmat.mat) {
    debug_log_ET::log("SparseMatrixCOO = OpSpMatCOO");
    apply_op<op>(*this);
  }

  template <Op op>
  SparseMatrixCOO& operator=(const OpSpMatCOO<Scalar, Index, op>& opmat) {
    debug_log_ET::log("SparseMatrixCOO = OpSpMatCOO");
    if (this != &(opmat.mat))
      *this = opmat.mat;
    apply_op<op>(*this);
    return *this;
  }
// Transposition:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Send and receive a matrix][Send and receive a matrix:1]]
// Send / Recv
#ifndef COMPOSYX_NO_MPI
  // Sendbuf of size 3, reqs of size 4
  void isend(std::array<int, 3>& sendbuf, const int dest, const int matrix_tag,
             std::array<MPI_Request, 4>& reqs,
             const MPI_Comm comm = MPI_COMM_WORLD,
             const int n_matrices = 1) const {
    sendbuf[0] = _m;
    sendbuf[1] = _n;
    sendbuf[2] = _nnz;
    MMPI::isend<int>(&sendbuf[0], 3, matrix_tag, dest, reqs[0], comm);
    MMPI::isend<Index>(this->get_i_ptr(), static_cast<int>(_nnz),
                       matrix_tag + n_matrices, dest, reqs[1], comm);
    MMPI::isend<Index>(this->get_j_ptr(), static_cast<int>(_nnz),
                       matrix_tag + 2 * n_matrices, dest, reqs[2], comm);
    MMPI::isend<Scalar>(this->get_v_ptr(), static_cast<int>(_nnz),
                        matrix_tag + 3 * n_matrices, dest, reqs[3], comm);
  }

  void recv(int source, int matrix_tag, const MPI_Comm comm = MPI_COMM_WORLD,
            const int n_matrices = 1) {
    int buf[3];
    MMPI::recv<int>(buf, 3, matrix_tag, source, comm);
    _m = buf[0];
    _n = buf[1];
    _nnz = buf[2];
    _i = std::vector<Index>(_nnz);
    _j = std::vector<Index>(_nnz);
    _v = std::vector<Scalar>(_nnz);
    MMPI::recv<Index>(&_i[0], static_cast<int>(_nnz), matrix_tag + n_matrices,
                      source, comm);
    MMPI::recv<Index>(&_j[0], static_cast<int>(_nnz),
                      matrix_tag + 2 * n_matrices, source, comm);
    MMPI::recv<Scalar>(&_v[0], static_cast<int>(_nnz),
                       matrix_tag + 3 * n_matrices, source, comm);
  }

  // reqs of size 4
  void wait(std::array<MPI_Request, 4> reqs) const {
    MPI_Status stat[4];
    MMPI::waitall(4, &reqs[0], &stat[0]);
  }
#endif
}; // class SparseMatrixCOO
// Send and receive a matrix:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Out of class operators][Out of class operators:1]]
// Scalar multiplication
template <class Scalar>
inline SparseMatrixCOO<Scalar> operator*(SparseMatrixCOO<Scalar> sp_mat,
                                         const Scalar& scal) {
  sp_mat *= scal;
  return sp_mat;
}

template <class Scalar>
inline SparseMatrixCOO<Scalar>
operator*(Scalar scal, const SparseMatrixCOO<Scalar>& sp_mat) {
  return sp_mat * scal;
}

template <class Scalar>
inline SparseMatrixCOO<Scalar> operator/(SparseMatrixCOO<Scalar> sp_mat,
                                         const Scalar& scal) {
  sp_mat /= scal;
  return sp_mat;
}

template <CPX_IJV_SparseMatrix OtherMatrix,
          CPX_Scalar Scalar = OtherMatrix::scalar_type>
inline SparseMatrixCOO<Scalar> operator+(SparseMatrixCOO<Scalar> sp_mat,
                                         const OtherMatrix& other) {
  sp_mat += other;
  return sp_mat;
}

template <CPX_IJV_SparseMatrix OtherMatrix,
          CPX_Scalar Scalar = OtherMatrix::scalar_type>
inline SparseMatrixCOO<Scalar> operator-(SparseMatrixCOO<Scalar> sp_mat,
                                         const OtherMatrix& other) {
  sp_mat -= other;
  return sp_mat;
}

template <CPX_IJV_SparseMatrix OtherMatrix,
          CPX_Scalar Scalar = OtherMatrix::scalar_type>
inline SparseMatrixCOO<Scalar> operator*(SparseMatrixCOO<Scalar> sp_mat_l,
                                         const OtherMatrix& sp_mat_r) {
  sp_mat_l *= sp_mat_r;
  return sp_mat_l;
}
// Out of class operators:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Matrix inverse operator][Matrix inverse operator:1]]
template <CPX_Scalar Scalar, CPX_Integral Index>
auto operator~(const SparseMatrixCOO<Scalar, Index>& A) {
  return COMPOSYX_SPARSE_SOLVER(A);
}

template <CPX_Scalar Scalar, CPX_Integral Index>
Vector<Scalar> operator%(const SparseMatrixCOO<Scalar, Index>& A,
                         const Vector<Scalar>& b) {
  return ~A * b;
}
// Matrix inverse operator:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Transposed or adjoint sparse matrix structure][Transposed or adjoint sparse matrix structure:1]]
template <CPX_Scalar Scalar, CPX_Integral Index, Op op> struct OpSpMatCOO {
  using Mat = SparseMatrixCOO<Scalar, Index>;
  const Mat& mat;

  OpSpMatCOO(const Mat& m) : mat{m} { debug_log_ET::log("OpSpMatCOO"); }

  auto eval() const {
    if constexpr (op == Op::NoTrans) {
      return mat;
    } else if constexpr (op == Op::Trans) {
      Mat emat(mat);
      emat.transpose();
      return emat;
    } else if constexpr (op == Op::ConjTrans) {
      Mat emat(mat);
      emat.conj_transpose();
      return emat;
    }
  }

  auto t() const {
    // A.h().t()
    static_assert(op != Op::ConjTrans, "Unsupported A.h().t() expression");

    if constexpr (op == Op::NoTrans) {
      return OpSpMatCOO<Scalar, Index, Op::Trans>(mat);
    } else {
      return OpSpMatCOO<Scalar, Index, Op::NoTrans>(mat);
    }
  }

  auto h() const {
    // A.t().h()
    static_assert(op != Op::Trans, "Unsupported A.t().h() expression");

    if constexpr (op == Op::NoTrans) {
      return OpSpMatCOO<Scalar, Index, Op::ConjTrans>(mat);
    } else {
      return OpSpMatCOO<Scalar, Index, Op::NoTrans>(mat);
    }
  }
};
// Transposed or adjoint sparse matrix structure:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*SpMV structure][SpMV structure:1]]
template <CPX_Scalar Scalar, CPX_Integral Index, Op opA, typename Rhs>
struct SpCOOMV {
  const SparseMatrixCOO<Scalar, Index>& sp_mat;
  const Rhs& rhs;

  SpCOOMV(const SparseMatrixCOO<Scalar, Index>& m, const Rhs& r)
      : sp_mat{m}, rhs{r} {
    debug_log_ET::log("SpCOOMV");
  }

  inline Rhs eval() const;

  operator Rhs() { return eval(); }
};

template <CPX_Scalar Scalar, CPX_Integral Index, Op opA, typename Rhs>
Rhs operator*(Scalar s, const SpCOOMV<Scalar, Index, opA, Rhs>& spmv) {
  return s * spmv.eval();
}
// SpMV structure:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Traits][Traits:1]]
template <typename Scalar, CPX_Integral Index>
struct scalar_type<SparseMatrixCOO<Scalar, Index>> : public std::true_type {
  using type = Scalar;
};

template <typename Scalar, CPX_Integral Index>
struct vector_type<SparseMatrixCOO<Scalar, Index>> : public std::true_type {
  using type = Vector<Scalar>;
};

template <typename Scalar, CPX_Integral Index>
struct dense_type<SparseMatrixCOO<Scalar, Index>> : public std::true_type {
  using type = DenseMatrix<Scalar>;
};

template <typename Scalar, CPX_Integral Index>
struct sparse_type<SparseMatrixCOO<Scalar, Index>> : public std::true_type {
  using type = SparseMatrixCOO<Scalar, Index>;
};

template <typename Scalar, CPX_Integral Index>
struct diag_type<SparseMatrixCOO<Scalar, Index>> : public std::true_type {
  using type = DiagonalMatrix<Scalar>;
};

template <typename Scalar, CPX_Integral Index>
struct is_sparse<SparseMatrixCOO<Scalar, Index>> : public std::true_type {};
// Traits:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Interface functions][Interface functions:1]]
template <typename Scalar, CPX_Integral Index = int>
SparseMatrixCOO<Scalar, Index>
transpose(const SparseMatrixCOO<Scalar, Index>& mat) {
  SparseMatrixCOO<Scalar, Index> tr = mat.t();
  return tr;
}

template <typename Scalar, CPX_Integral Index = int>
SparseMatrixCOO<Scalar, Index>
adjoint(const SparseMatrixCOO<Scalar, Index>& mat) {
  SparseMatrixCOO<Scalar, Index> adjmat = mat.h();
  return adjmat;
}

template <typename Scalar, CPX_Integral Index = int>
void build_matrix(SparseMatrixCOO<Scalar, Index>& mat, const int M, const int N,
                  const int nnz, int* i, int* j, Scalar* v,
                  const bool fill_symmetry = false) {
  mat = SparseMatrixCOO<Scalar, Index>(M, N, nnz, i, j, v);
  if (fill_symmetry) {
    mat.fill_half_to_full_storage();
  }
}

template <CPX_Scalar Scalar, CPX_IntArray I>
void reindex(SparseMatrixCOO<Scalar>& mat, const I& new_indices,
             int new_m = -1) {
  mat.reindex(new_indices, new_m);
}
// Interface functions:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Construction with CSR format][Construction with CSR format:1]]
template <CPX_Scalar Scalar, CPX_Integral Index>
SparseMatrixCOO<Scalar, Index>
COO_from_CSR(std::span<const Index> csri, std::span<const Index> csrj,
             std::span<const Scalar> csrv, size_t csrm, size_t csrn) {
  SparseMatrixCOO<Scalar, Index> coomat;
  coomat.from_csr(csri, csrj, csrv, csrm, csrn);
  return coomat;
}

template <CPX_Scalar Scalar, CPX_Integral Index>
SparseMatrixCOO<Scalar, Index>
COO_from_CSR(std::initializer_list<Index> i, std::initializer_list<Index> j,
             std::initializer_list<Scalar> v, const size_t m, const size_t n) {
  return COO_from_CSR<Scalar, Index>(std::span(i.begin(), i.end()),
                                     std::span(j.begin(), j.end()),
                                     std::span(v.begin(), v.end()), m, n);
}
// Construction with CSR format:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
