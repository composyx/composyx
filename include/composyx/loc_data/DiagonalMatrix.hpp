// [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Header][Header:1]]
#pragma once

namespace composyx {
template <class> class DiagonalMatrix;
}

#include "composyx/utils/Arithmetic.hpp"
#include "composyx/utils/ArrayAlgo.hpp"
#include "composyx/utils/Error.hpp"
#include "composyx/utils/MatrixProperties.hpp"
#include <composyx/interfaces/basic_concepts.hpp>
// To remove
#include "composyx/loc_data/SparseMatrixCSC.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Attributes][Attributes:1]]
template <class Scalar> class DiagonalMatrix : public MatrixProperties<Scalar> {

public:
  using scalar_type = Scalar;
  using real_type = typename arithmetic_real<Scalar>::type;

private:
  using Real = real_type;
  using DataArray = std::vector<Scalar>;

  size_t _m{0};
  size_t _n{0};
  DataArray _elts;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Constructors][Constructors:1]]
public:
  explicit DiagonalMatrix(const size_t m, const size_t n) : _m{m}, _n{n} {
    _elts = DataArray(std::min(_m, _n));
  }

  explicit DiagonalMatrix(const size_t m) : DiagonalMatrix(m, m) {}

  explicit DiagonalMatrix() : DiagonalMatrix(0, 0) {}

  explicit DiagonalMatrix(const size_t m, const size_t n, Scalar* vect)
      : _m{m}, _n{n} {
    _elts = DataArray(std::min(_m, _n));
    for (size_t k = 0; k < _elts.size(); ++k)
      _elts[k] = vect[k];
  }

  template <CPX_Arrayof<Scalar> A>
  explicit DiagonalMatrix(const size_t m, const size_t n, const A& a)
      : _m{m}, _n{n} {
    COMPOSYX_ASSERT(a.size() >= std::min(_m, _n),
                    "DiagonalMatrix initialized with too few values");
    _elts = DataArray(std::min(_m, _n));
    for (size_t k = 0; k < _elts.size(); ++k)
      _elts[k] = a[k];
  }

  template <CPX_Arrayof<Scalar> A>
  explicit DiagonalMatrix(const size_t m, const size_t n, A&& a)
      : _m{m}, _n{n} {
    COMPOSYX_ASSERT(a.size() >= std::min(_m, _n),
                    "DiagonalMatrix initialized with too few values");
    _elts = std::move(a);
  }

  template <CPX_Arrayof<Scalar> A>
  explicit DiagonalMatrix(const A& a) : _m{a.size()}, _n{a.size()} {
    _elts = DataArray(_m);
    for (size_t k = 0; k < _m; ++k)
      _elts[k] = a[k];
  }

  template <CPX_Arrayof<Scalar> A>
  explicit DiagonalMatrix(A&& a)
      : _m{a.size()}, _n{a.size()}, _elts{std::move(a)} {}

  explicit DiagonalMatrix(std::initializer_list<Scalar> l, const size_t m = 0,
                          const size_t n = 0) {
    size_t diagsize = l.size();
    if (m == 0) {
      _m = diagsize;
      _n = diagsize;
    } else if (n == 0) {
      _m = m;
      _n = m;
    } else {
      _m = m;
      _n = n;
    }
    COMPOSYX_ASSERT(
        diagsize == std::min(_m, _n),
        "DiagonalMatrix list initialization: size must be min(m, n)");
    _elts = DataArray(l);
  }

  DiagonalMatrix(const DiagonalMatrix& mat) = default;
  DiagonalMatrix(DiagonalMatrix&& mat)
      : _m{std::exchange(mat._m, 0)}, _n{std::exchange(mat._n, 0)},
        _elts{std::move(mat._elts)} {
    this->copy_properties(mat);
    mat.set_default_properties();
  }

  // Copy and move assignment operator
  DiagonalMatrix& operator=(DiagonalMatrix copy) {
    copy.swap(*this);
    return *this;
  }
  // Constructors:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Swap function][Swap function:1]]
  void swap(DiagonalMatrix& other) {
    std::swap(_m, other._m);
    std::swap(_n, other._n);
    std::swap(_elts, other._elts);
  }

  friend void swap(DiagonalMatrix& m1, DiagonalMatrix& m2) { m1.swap(m2); }
  // Swap function:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Comparison operator][Comparison operator:1]]
  bool operator==(const DiagonalMatrix& mat2) const {
    if (_m != mat2._m)
      return false;
    if (_n != mat2._n)
      return false;
    if (_elts != mat2._elts)
      return false;
    return true;
  }

  bool operator!=(const DiagonalMatrix& mat2) const { return !(*this == mat2); }
  // Comparison operator:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Casting][Casting:1]]
  template <class OtherScalar>
  [[nodiscard]] DiagonalMatrix<OtherScalar> cast() const {
    DiagonalMatrix<OtherScalar> mat(_m, _n);
    copy_properties<Scalar, OtherScalar>(*this, mat);
    OtherScalar* ptr = mat.get_ptr();
    for (size_t i = 0; i < std::min(_m, _n); ++i) {
      ptr[i] = static_cast<OtherScalar>(_elts[i]);
    }
    return mat;
  }
  // Casting:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Identity][Identity:1]]
  static DiagonalMatrix identity(const size_t M) {
    DiagonalMatrix id(M);
    std::ranges::fill(id._elts, Scalar{1});
    return id;
  }
  // Identity:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Inverse][Inverse:1]]
  void inverse() {
    std::ranges::for_each(_elts, [](Scalar& s) {
      COMPOSYX_ASSERT(s != 0, "DiagonalMatrix::inverse coefficient is 0");
      s *= Scalar{1} / s;
    });
  }
  // Inverse:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Transposition][Transposition:1]]
  void transpose() { std::swap(_m, _n); }
  void conj_transpose() {
    std::swap(_m, _n);
    if constexpr (is_complex<Scalar>::value) {
      std::ranges::for_each(_elts, [](Scalar& s) { s = conj<Scalar>(s); });
    }
  }

  [[nodiscard]] DiagonalMatrix t() const {
    return DiagonalMatrix(_n, _m, _elts);
  }

  [[nodiscard]] DiagonalMatrix h() const {
    DiagonalMatrix out = (*this);
    out.conj_transpose();
    return out;
  }
  // Transposition:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Scalar multiplication and division][Scalar multiplication and division:1]]
  DiagonalMatrix& operator*=(const Scalar& scal) {
    std::ranges::for_each(_elts, [&scal](Scalar& s) { s *= scal; });
    return *this;
  }

  DiagonalMatrix& operator/=(const Scalar& scal) {
    COMPOSYX_ASSERT(scal != Scalar{0}, "SparseMatrix:: division by 0 !");
    const auto inv = Scalar{1.0} / scal;
    std::ranges::for_each(_elts, [&inv](Scalar& s) { s *= inv; });
    return *this;
  }
  // Scalar multiplication and division:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Diagonal matrix product][Diagonal matrix product:1]]
  // Diag *= Diag
  void matprod(const DiagonalMatrix& other) {
    COMPOSYX_ASSERT(other.get_n_cols() == this->get_n_rows(),
                    "DiagonalMatrix * DiagonalMatrix: wrong dimensions");
    for (size_t k = 0; k < std::min(_m, _n); ++k) {
      _elts[k] *= other._elts[k];
    }
  }

  // Square the matrix
  void square() {
    for (size_t k = 0; k < std::min(_m, _n); ++k) {
      _elts[k] *= _elts[k];
    }
  }

  DiagonalMatrix& operator*=(const DiagonalMatrix& other) {
    if (this == &other) {
      this->square();
    } else {
      this->matprod(other);
    }
    return *this;
  }
  // Diagonal matrix product:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Diagonal matrix addition][Diagonal matrix addition:1]]
  // Diag += Diag
  void add(const DiagonalMatrix& other) {
    COMPOSYX_ASSERT(other.get_n_rows() == this->get_n_rows(),
                    "DiagonalMatrix + DiagonalMatrix: wrong dimensions");
    COMPOSYX_ASSERT(other.get_n_cols() == this->get_n_cols(),
                    "DiagonalMatrix + DiagonalMatrix: wrong dimensions");
    for (size_t k = 0; k < std::min(_m, _n); ++k) {
      _elts[k] += other._elts[k];
    }
  }

  void substract(const DiagonalMatrix& other) {
    COMPOSYX_ASSERT(other.get_n_rows() == this->get_n_rows(),
                    "DiagonalMatrix + DiagonalMatrix: wrong dimensions");
    COMPOSYX_ASSERT(other.get_n_cols() == this->get_n_cols(),
                    "DiagonalMatrix + DiagonalMatrix: wrong dimensions");
    for (size_t k = 0; k < std::min(_m, _n); ++k) {
      _elts[k] += other._elts[k];
    }
  }

  DiagonalMatrix& operator+=(const DiagonalMatrix& other) {
    if (this == &other) {
      (*this) *= Scalar{2};
    } else {
      this->add(other);
    }
    return *this;
  }

  DiagonalMatrix& operator-=(const DiagonalMatrix& other) {
    if (this == &other) {
      std::ranges::fill(_elts, Scalar{0});
    } else {
      this->substract(other);
    }
    return *this;
  }
  // Diagonal matrix addition:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Getters][Getters:1]]
public:
  // Getters
  [[nodiscard]] inline size_t get_n_rows() const { return _m; }
  [[nodiscard]] inline size_t get_n_cols() const { return _n; }
  [[nodiscard]] inline size_t constexpr get_leading_dim() const { return 1; }

  [[nodiscard]] inline Scalar* get_ptr() { return &_elts[0]; }
  [[nodiscard]] inline const Scalar* get_ptr() const { return &_elts[0]; }
  template <CPX_Integral Tint>
  [[nodiscard]] inline Scalar* get_ptr(Tint i, Tint j) {
    COMPOSYX_ASSERT(i == j, "DiagonalMatrix(i, j) with i != j");
    return &_elts[i];
  }
  template <CPX_Integral Tint>
  [[nodiscard]] inline const Scalar* get_ptr(Tint i, Tint j) const {
    COMPOSYX_ASSERT(i == j, "DiagonalMatrix(i, j) with i != j");
    return &_elts[i];
  }
  template <CPX_Integral Tint> [[nodiscard]] inline Scalar* get_ptr(Tint i) {
    return &_elts[i];
  }
  template <CPX_Integral Tint>
  [[nodiscard]] inline const Scalar* get_ptr(Tint i) const {
    return &_elts[i];
  }
  [[nodiscard]] inline DataArray get_array() const { return _elts; }
  // Getters:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Indexing][Indexing:1]]
  // Element accessor
  template <CPX_Integral Tint>
  [[nodiscard]] inline Scalar& operator()(const Tint i, const Tint j) {
    COMPOSYX_ASSERT(i == j, "DiagonalMatrix(i, j) with i != j");
    return _elts[i];
  }
  template <CPX_Integral Tint>
  [[nodiscard]] inline const Scalar& operator()(const Tint i,
                                                const Tint j) const {
    COMPOSYX_ASSERT(i == j, "DiagonalMatrix(i, j) with i != j");
    return _elts[i];
  }
  template <CPX_Integral Tint>
  [[nodiscard]] inline Scalar& operator()(const Tint i) {
    return _elts[i];
  }
  template <CPX_Integral Tint>
  [[nodiscard]] inline const Scalar& operator()(const Tint i) const {
    return _elts[i];
  }
  // Indexing:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Memory cost][Memory cost:1]]
  [[nodiscard]] inline size_t memcost() const {
    return std::min(_m, _n) * sizeof(Scalar);
  }
  // Memory cost:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Display function][Display function:1]]
  // Pretty printing
  void display(const std::string& name = "",
               std::ostream& out = std::cout) const {
    if (!name.empty())
      out << name << '\n';
    out << "m: " << _m << " "
        << "n: " << _n << " " << '\n'
        << "Diagonal matrix\n"
        << this->properties_str() << '\n'
        << '\n';

    for (size_t i = 0; i < _m; ++i) {
      for (size_t j = 0; j < _n; ++j) {
        if (i == j) {
          out << _elts[i] << "\t";
        } else {
          out << "-" << "\t";
        }
      }
      out << '\n';
    }
    out << '\n';
  }

  friend std::ostream& operator<<(std::ostream& out, const DiagonalMatrix& dm) {
    dm.display("", out);
    return out;
  }
  // Display function:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Frobenius norm][Frobenius norm:1]]
  [[nodiscard]] Real frobenius_norm() const {
    Real norm{0.0};
    for (size_t k = 0; k < std::min(_m, _n); ++k) {
      norm += std::norm(_elts[k]);
    }
    return std::sqrt(norm);
  }

  Real norm() const { return frobenius_norm(); }
  // Frobenius norm:1 ends here

  // [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Binary operators][Binary operators:1]]
}; // class DiagonalMatrix
// Binary operators:1 ends here

// [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Binary operators][Binary operators:2]]
// Scalar multiplication
template <CPX_Scalar Scalar>
inline DiagonalMatrix<Scalar> operator*(DiagonalMatrix<Scalar> diag,
                                        const Scalar scal) {
  diag *= scal;
  return diag;
}

template <CPX_Scalar Scalar>
inline DiagonalMatrix<Scalar> operator*(const Scalar scal,
                                        DiagonalMatrix<Scalar> diag) {
  diag *= scal;
  return diag;
}

template <CPX_Scalar Scalar>
inline DiagonalMatrix<Scalar> operator/(DiagonalMatrix<Scalar> diag,
                                        const Scalar scal) {
  diag /= scal;
  return diag;
}

// Diagonal addition / substraction
template <CPX_Scalar Scalar, typename OtherMatrix>
[[nodiscard]] inline OtherMatrix operator+(const DiagonalMatrix<Scalar> diag,
                                           OtherMatrix other) {
  other += diag;
  return other;
}

template <CPX_Scalar Scalar, typename OtherMatrix>
[[nodiscard]] inline OtherMatrix operator+(OtherMatrix other,
                                           const DiagonalMatrix<Scalar> diag) {
  other += diag;
  return other;
}

template <CPX_Scalar Scalar, typename OtherMatrix>
[[nodiscard]] inline OtherMatrix operator-(const DiagonalMatrix<Scalar> diag,
                                           OtherMatrix other) {
  other -= diag;
  other *= Scalar{-1};
  return other;
};

template <CPX_Scalar Scalar, typename OtherMatrix>
[[nodiscard]] inline OtherMatrix operator-(OtherMatrix other,
                                           const DiagonalMatrix<Scalar> diag) {
  other -= diag;
  return other;
}

// Mat <- Mat * Diagonal
template <CPX_Scalar Scalar, typename OtherMatrix>
[[nodiscard]] inline OtherMatrix operator*(OtherMatrix mat,
                                           const DiagonalMatrix<Scalar>& diag) {
  mat.right_diagmm(diag);
  return mat;
}

// Mat <- Diagonal * Mat
template <CPX_Scalar Scalar, typename OtherMatrix>
[[nodiscard]] inline OtherMatrix operator*(const DiagonalMatrix<Scalar>& diag,
                                           OtherMatrix mat) {
  mat.left_diagmm(diag);
  return mat;
}
// Binary operators:2 ends here

// [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Pseudo-inverse operators][Pseudo-inverse operators:1]]
template <typename Scalar>
DiagonalMatrix<Scalar> operator~(const DiagonalMatrix<Scalar>& D) {
  DiagonalMatrix<Scalar> Dinv = D;
  D.inverse();
  return D;
}

template <typename Scalar>
Vector<Scalar> operator%(const DiagonalMatrix<Scalar>& D,
                         const Vector<Scalar>& b) {
  return ~D * b;
}
// Pseudo-inverse operators:1 ends here

// [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Traits][Traits:1]]
template <typename Scalar>
struct vector_type<DiagonalMatrix<Scalar>> : public std::true_type {
  using type = Vector<Scalar>;
};

template <typename Scalar>
struct sparse_type<DiagonalMatrix<Scalar>> : public std::true_type {
  using type = SparseMatrixCSC<Scalar>;
};

template <typename Scalar>
struct scalar_type<DiagonalMatrix<Scalar>> : public std::true_type {
  using type = Scalar;
};

template <typename Scalar>
struct is_dense<DiagonalMatrix<Scalar>> : public std::true_type {};
// Traits:1 ends here

// [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Container functions][Container functions:1]]
// Container functions
template <class Scalar> Scalar* begin(DiagonalMatrix<Scalar>& v) {
  return v.get_ptr();
}
template <class Scalar> const Scalar* begin(const DiagonalMatrix<Scalar>& v) {
  return v.get_ptr();
}

template <class Scalar> Scalar* end(DiagonalMatrix<Scalar>& v) {
  return v.get_ptr() + v.get_n_rows();
}
template <class Scalar> const Scalar* end(const DiagonalMatrix<Scalar>& v) {
  return v.get_ptr() + v.get_n_rows();
}
// Container functions:1 ends here

// [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Interface functions][Interface functions:1]]
template <typename Scalar>
[[nodiscard]] inline size_t n_rows(const DiagonalMatrix<Scalar>& mat) {
  return mat.get_n_rows();
}

template <typename Scalar>
[[nodiscard]] inline size_t n_cols(const DiagonalMatrix<Scalar>& mat) {
  return mat.get_n_cols();
}

template <typename Scalar>
[[nodiscard]] inline DiagonalMatrix<Scalar>
diagonal(const DiagonalMatrix<Scalar>& mat) {
  return mat;
}

template <typename Scalar>
[[nodiscard]] inline Scalar* get_ptr(DiagonalMatrix<Scalar>& mat) {
  return mat.get_ptr();
}

template <typename Scalar>
[[nodiscard]] inline const Scalar* get_ptr(const DiagonalMatrix<Scalar>& mat) {
  return mat.get_ptr();
}

template <class Scalar>
[[nodiscard]] DiagonalMatrix<Scalar>
tranpose(const DiagonalMatrix<Scalar>& mat) {
  return mat.t();
}

template <class Scalar>
[[nodiscard]] DiagonalMatrix<Scalar>
adjoint(const DiagonalMatrix<Scalar>& mat) {
  return mat.h();
}

template <class Scalar>
void display(const DiagonalMatrix<Scalar>& v, const std::string& name = "",
             std::ostream& out = std::cout) {
  v.display(name, out);
}

template <typename Scalar>
void build_matrix(DiagonalMatrix<Scalar>& mat, const int M, const int N,
                  const int nnz, int* i, int* j, Scalar* v, const bool) {
  mat = DiagonalMatrix<Scalar>(M, N);
  COMPOSYX_ASSERT(M == nnz, "build_matrix(Diagonal matrix) with nnz != M");
  COMPOSYX_ASSERT(M == nnz, "build_matrix(Diagonal matrix) with nnz != N");
  for (int k = 0; k < nnz; ++k) {
    COMPOSYX_ASSERT(
        i[k] == j[k],
        "build_matrix(Diagonal matrix) with extradiagonal elements");
    mat(i[k]) = v[k];
  }
}
// Interface functions:1 ends here

// [[file:../../../org/composyx/loc_data/DiagonalMatrix.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
