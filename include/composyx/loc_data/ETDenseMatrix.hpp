// [[file:../../../org/composyx/loc_data/ETDenseMatrix.org::*Header][Header:1]]
#pragma once

namespace composyx {

namespace debug_log_ET {
#ifndef COMPOSYX_STATIC_VARIABLES_ALREADY_DECLARED
bool activated_log = false;
#endif // COMPOSYX_STATIC_VARIABLES_ALREADY_DECLARED

#if defined(EXPRESSION_TEMPLATE_LOG)
inline void log(const std::string& s) {
  if (activated_log)
    std::cout << " !!! ET > " << s << '\n';
}
#else
inline void log(const std::string&) {}
#endif

} // namespace debug_log_ET

namespace dense_ET {
// Header:1 ends here

// [[file:../../../org/composyx/loc_data/ETDenseMatrix.org::*Scaled vector][Scaled vector:1]]
// Scalar * vector
template <CPX_Scalar Scalar, Op op = Op::NoTrans> struct ScalOpVect {
  using Vect = Vector<Scalar>;
  const Vect& vect;
  const Scalar scal;

  ScalOpVect(const Vect& vv, const Scalar s) : vect{vv}, scal{s} {
    debug_log_ET::log("ScalOpVect");
  }

  auto t() const {
    // x.h().t()
    static_assert(op != Op::ConjTrans, "Unsupported x.h().t() expression");

    if constexpr (op == Op::NoTrans) {
      return ScalOpVect<Scalar, Op::Trans>(vect, scal);
    } else {
      return ScalOpVect<Scalar, Op::NoTrans>(vect, scal);
    }
  }

  auto h() const {
    // x.t().h()
    static_assert(op != Op::Trans, "Unsupported x.t().h() expression");

    if constexpr (op == Op::NoTrans) {
      return ScalOpVect<Scalar, Op::ConjTrans>(vect, scal);
    } else {
      return ScalOpVect<Scalar, Op::NoTrans>(vect, scal);
    }
  }

  auto eval() const {
    if constexpr (op == Op::NoTrans) {
      return Vect(scal * vect);
    } else {
      DenseMatrix<Scalar, -1> res(vect);
      apply_op<op>(res);
      return res * scal;
    }
  }
};
// Scaled vector:1 ends here

// [[file:../../../org/composyx/loc_data/ETDenseMatrix.org::*Scaled matrix][Scaled matrix:1]]
// Scalar * matrix
template <CPX_Scalar Scalar, int NbCol, Op op = Op::NoTrans> struct ScalOpMat {
  using Mat = DenseMatrix<Scalar, NbCol>;
  const Mat& mat;
  const Scalar scal;
  ScalOpMat(const Mat& mm, const Scalar s) : mat{mm}, scal{s} {
    debug_log_ET::log("ScalOpMat");
  }

  auto t() const {
    // A.h().t()
    static_assert(op != Op::ConjTrans, "Unsupported A.h().t() expression");

    if constexpr (op == Op::NoTrans) {
      return ScalOpMat<Scalar, NbCol, Op::Trans>(mat, scal);
    } else {
      return ScalOpMat<Scalar, NbCol, Op::NoTrans>(mat, scal);
    }
  }

  auto h() const {
    // A.t().h()
    static_assert(op != Op::Trans, "Unsupported A.t().h() expression");

    if constexpr (op == Op::NoTrans) {
      return ScalOpMat<Scalar, NbCol, Op::ConjTrans>(mat, scal);
    } else {
      return ScalOpMat<Scalar, NbCol, Op::NoTrans>(mat, scal);
    }
  }

  auto eval() const {
    if constexpr (op == Op::NoTrans) {
      return Mat(scal * mat);
    } else {
      DenseMatrix<Scalar, -1> res(mat);
      apply_op<op>(res);
      return res * scal;
    }
  }
};
// Scaled matrix:1 ends here

// [[file:../../../org/composyx/loc_data/ETDenseMatrix.org::*Matrix vector product][Matrix vector product:1]]
template <CPX_Scalar Scalar, int NbCol, Op opA = Op::NoTrans> struct MatVect {
  using ScMat = ScalOpMat<Scalar, NbCol, opA>;
  using Vect = Vector<Scalar>;

  ScMat sm;
  const Vect& v;

  //mat * vect
  //alpha * (mat * vect)
  MatVect(const ScMat& mm, const Vect& vv) : sm{mm}, v{vv} {
    debug_log_ET::log("MatVect");
  }

  auto eval() const {
    DenseMatrix<Scalar, NbCol> smmat = sm.eval();
    Vect y = v;
    y.gemv(sm.eval(), y);
    return y;
  }
};
// Matrix vector product:1 ends here

// [[file:../../../org/composyx/loc_data/ETDenseMatrix.org::*GEMV-like][GEMV-like:1]]
template <CPX_Scalar Scalar, int NbCol, Op opA> struct ETgemv {
  // alpha * A * X + beta * Y
  using MatV = MatVect<Scalar, NbCol, opA>;
  using Vect = ScalOpVect<Scalar, Op::NoTrans>;

  MatV Ax;
  Vect y;

  ETgemv(const MatV& ax, const Vect& yy) : Ax{ax}, y{yy} {
    debug_log_ET::log("ETgemv");
  }

  auto eval() const { return Ax.eval() + y.eval(); }
};
// GEMV-like:1 ends here

// [[file:../../../org/composyx/loc_data/ETDenseMatrix.org::*Matrix matrix product][Matrix matrix product:1]]
template <CPX_Scalar Scalar, int NbColA, int NbColB, Op opA = Op::NoTrans,
          Op opB = Op::NoTrans>
struct MatMat {
  using ScMat = ScalOpMat<Scalar, NbColA, opA>;
  using Mat = DenseMatrix<Scalar, NbColB>;

  ScMat sm_A;
  const Mat& mat_B;

  //mat * mat
  //alpha * (mat * mat)
  MatMat(const ScMat& smA, const Mat& mB) : sm_A{smA}, mat_B{mB} {
    debug_log_ET::log("MatMat");
  }

  auto eval() const {
    const size_t M = (opA == Op::NoTrans) ? n_rows(sm_A.mat) : n_cols(sm_A.mat);
    const size_t N = (opB == Op::NoTrans) ? n_cols(mat_B) : n_rows(mat_B);
    if constexpr (opB == Op::NoTrans) {
      DenseMatrix<Scalar, NbColB> C(M, N);
      C.gemm(sm_A.mat, mat_B, opA, opB, sm_A.scal, Scalar{0});
      return C;
    } else {
      DenseMatrix<Scalar, -1> C(M, N);
      C.gemm(sm_A.mat, mat_B, opA, opB, sm_A.scal, Scalar{0});
      return C;
    }
  }
};
// Matrix matrix product:1 ends here

// [[file:../../../org/composyx/loc_data/ETDenseMatrix.org::*GEMM-like][GEMM-like:1]]
template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC,
          Op opA = Op::NoTrans, Op opB = Op::NoTrans, Op opC = Op::NoTrans>
struct ETgemm {

  using MatM = MatMat<Scalar, NbColA, NbColB, opA, opB>;
  using ScMat = ScalOpMat<Scalar, NbColC, opC>;

  MatM alpha_AB;
  ScMat beta_C;

  ETgemm(const MatM& aAB, const ScMat& bC) : alpha_AB{aAB}, beta_C{bC} {
    debug_log_ET::log("ETgemm");
  }

  auto eval() const {
    DenseMatrix<Scalar, NbColC> C_out(beta_C.mat);
    apply_op<opC>(C_out);
    C_out.gemm(alpha_AB.sm_A.mat, alpha_AB.mat_B, opA, opB, alpha_AB.sm_A.scal,
               beta_C.scal);
    return C_out;
  }
};
// GEMM-like:1 ends here

// [[file:../../../org/composyx/loc_data/ETDenseMatrix.org::*Vector arithmetic][Vector arithmetic:1]]
template <CPX_Scalar Scalar, Op op>
ScalOpVect<Scalar, op> operator*(const ScalOpVect<Scalar, op>& sv, Scalar s) {
  return ScalOpVect<Scalar, op>(sv.vect, sv.scal * s);
}

template <CPX_Scalar Scalar, Op op>
ScalOpVect<Scalar, op> operator*(Scalar s, const ScalOpVect<Scalar, op>& sv) {
  return ScalOpVect<Scalar, op>(sv.vect, sv.scal * s);
}

// Dot product
// Real
// alpha = x.t() * y
template <CPX_Scalar Scalar>
Scalar operator*(ScalOpVect<Scalar, Op::Trans> v1, const Vector<Scalar>& v2) {
  if (v1.scal == Scalar{1}) {
    return v1.vect.dotu(v2);
  } else {
    Vector<Scalar> sv1 = v1.scal * v1.vect;
    return sv1.dotu(v2);
  }
}

template <CPX_Scalar Scalar>
Scalar operator*(const ScalOpVect<Scalar, Op::Trans>& v1,
                 const ScalOpVect<Scalar, Op::NoTrans>& v2) {
  if (v2.scal == Scalar{1}) {
    return v1 * v2.vect;
  }
  Vector<Scalar> sv2 = v2.eval();
  return v1 * sv2;
}

// Hermitian
// alpha = x.h() * y
template <CPX_Scalar Scalar>
Scalar operator*(ScalOpVect<Scalar, Op::ConjTrans> v1,
                 const Vector<Scalar>& v2) {
  if (v1.scal == Scalar{1}) {
    return v1.vect.dot(v2);
  } else {
    Vector<Scalar> sv1 = v1.scal * v1.vect;
    return sv1.dot(v2);
  }
}

template <CPX_Scalar Scalar>
Scalar operator*(const ScalOpVect<Scalar, Op::ConjTrans>& v1,
                 const ScalOpVect<Scalar, Op::NoTrans>& v2) {
  if (v2.scal == Scalar{1}) {
    return v1 * v2.vect;
  }
  Vector<Scalar> sv2 = v2.eval();
  return v1 * sv2;
}

// Outer product
// A = x * y.t()
template <CPX_Scalar Scalar>
DenseMatrix<Scalar, -1> operator*(const Vector<Scalar>& v1,
                                  const ScalOpVect<Scalar, Op::Trans>& v2) {
  DenseMatrix<Scalar, -1> A(n_rows(v1), n_rows(v2.vect));
  A.geru(v1, v2.vect, v2.scal);
  return A;
}

template <CPX_Scalar Scalar>
DenseMatrix<Scalar, -1> operator*(const ScalOpVect<Scalar, Op::NoTrans>& v1,
                                  const ScalOpVect<Scalar, Op::Trans>& v2) {
  DenseMatrix<Scalar, -1> A(n_rows(v1.vect), n_rows(v2.vect));
  A.geru(v1, v2.vect, v2.scal);
  return A;
}

// Hermitian
template <CPX_Scalar Scalar>
DenseMatrix<Scalar, -1> operator*(const Vector<Scalar>& v1,
                                  const ScalOpVect<Scalar, Op::ConjTrans>& v2) {
  DenseMatrix<Scalar, -1> A(n_rows(v1), n_rows(v2.vect));
  A.ger(v1, v2.vect, v2.scal);
  return A;
}

template <CPX_Scalar Scalar>
DenseMatrix<Scalar, -1> operator*(const ScalOpVect<Scalar, Op::NoTrans>& v1,
                                  const ScalOpVect<Scalar, Op::ConjTrans>& v2) {
  DenseMatrix<Scalar, -1> A(n_rows(v1.vect), n_rows(v2.vect));
  A.ger(v1.vect, v2.vect, v1.scal * v2.scal);
  return A;
}

// Vector addition
template <CPX_Scalar Scalar>
DenseMatrix<Scalar, 1> operator+(const ScalOpVect<Scalar, Op::NoTrans> vx,
                                 const ScalOpVect<Scalar, Op::NoTrans> vy) {
  DenseMatrix<Scalar, 1> res = vx.vect;
  res += vy.vect;
  return res;
}

// Vector substraction
template <CPX_Scalar Scalar>
DenseMatrix<Scalar, 1> operator-(const ScalOpVect<Scalar, Op::NoTrans> vx,
                                 const ScalOpVect<Scalar, Op::NoTrans> vy) {
  DenseMatrix<Scalar, 1> res = vx.vect;
  res -= vy.vect;
  return res;
}
// Vector arithmetic:1 ends here

// [[file:../../../org/composyx/loc_data/ETDenseMatrix.org::*Matrix scalar and additive arithmetic][Matrix scalar and additive arithmetic:1]]
template <CPX_Scalar Scalar, int NbCol, Op op>
ScalOpMat<Scalar, NbCol, op> operator*(const ScalOpMat<Scalar, NbCol, op>& sm,
                                       Scalar s) {
  return ScalOpMat<Scalar, NbCol, op>(sm.mat, sm.scal * s);
}

template <CPX_Scalar Scalar, int NbCol, Op op>
ScalOpMat<Scalar, NbCol, op> operator*(Scalar s,
                                       const ScalOpMat<Scalar, NbCol, op>& sm) {
  return ScalOpMat<Scalar, NbCol, op>(sm.mat, sm.scal * s);
}

// Matrix addition
template <CPX_Scalar Scalar, int NbColA, int NbColB, Op opA, Op opB>
DenseMatrix<Scalar, -1> operator+(const ScalOpMat<Scalar, NbColA, opA> A,
                                  const ScalOpMat<Scalar, NbColB, opB> B) {
  DenseMatrix<Scalar, -1> res(A.mat);
  apply_op<opA>(res);
  res *= A.scal;
  if constexpr (opB == Op::NoTrans) {
    if (B.scal == Scalar{1}) {
      res += B.mat;
      return res;
    };
  }

  DenseMatrix<Scalar, -1> tmpB(B);
  apply_op<opA>(tmpB);
  tmpB *= B.scal;
  res += tmpB;

  return res;
}

// Matrix substraction
template <CPX_Scalar Scalar, int NbColA, int NbColB, Op opA, Op opB>
DenseMatrix<Scalar, -1> operator-(const ScalOpMat<Scalar, NbColA, opA> A,
                                  const ScalOpMat<Scalar, NbColB, opB> B) {
  return A + ScalOpMat<Scalar, NbColB, opB>(B.mat, B.scal * Scalar{-1});
}
// Matrix scalar and additive arithmetic:1 ends here

// [[file:../../../org/composyx/loc_data/ETDenseMatrix.org::*Footer][Footer:1]]
} // namespace dense_ET
} // namespace composyx
// Footer:1 ends here
