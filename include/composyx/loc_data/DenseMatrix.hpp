// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Header][Header:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <string>
#include <algorithm>

namespace composyx {
template <CPX_Scalar Scalar, int NbCol = -1> class DenseMatrix;

template <CPX_Scalar Scalar> using Vector = DenseMatrix<Scalar, 1>;

template <CPX_Scalar Scalar, int NbCol>
void display(const DenseMatrix<Scalar, NbCol>&, const std::string& n = "",
             std::ostream& o = std::cout);

template <CPX_Scalar Scalar, int NbCol>
size_t size(const DenseMatrix<Scalar, NbCol>& mat);

template <CPX_Scalar Scalar>
constexpr size_t n_cols(const DenseMatrix<Scalar, 1>&);

template <CPX_Scalar Scalar, int NbCol>
size_t n_rows(const DenseMatrix<Scalar, NbCol>& mat);

template <CPX_Scalar Scalar, int NbCol>
size_t n_cols(const DenseMatrix<Scalar, NbCol>& mat);

template <CPX_Scalar Scalar, int NbCol>
Scalar* get_ptr(DenseMatrix<Scalar, NbCol>& mat);

template <CPX_Scalar Scalar, int NbCol>
const Scalar* get_ptr(const DenseMatrix<Scalar, NbCol>& mat);

template <CPX_Scalar Scalar, int NbCol> struct DenseMatrixIterator;

enum class Op : char { NoTrans = 'N', Trans = 'T', ConjTrans = 'C' };

template <Op op, CPX_Scalar Scalar, int NbCol>
inline void apply_op(DenseMatrix<Scalar, NbCol>& mat) {
  if constexpr (op == Op::NoTrans) {
    return;
  } else if constexpr (op == Op::Trans) {
    mat.transpose();
  } else if constexpr (op == Op::ConjTrans) {
    mat.conj_transpose();
  }
}

[[nodiscard]] inline char op_char(Op op) {
  if (op == Op::Trans)
    return 'T';
  if (op == Op::ConjTrans)
    return 'C';
  return 'N';
}
} // namespace composyx

#include "composyx/loc_data/ETDenseMatrix.hpp"
#include "composyx/loc_data/DiagonalMatrix.hpp"
#include "composyx/loc_data/SparseMatrixCSC.hpp"
#include "composyx/loc_data/SparseMatrixCSR.hpp"
#include "composyx/utils/Arithmetic.hpp"
#include "composyx/utils/MatrixProperties.hpp"
#include "composyx/utils/ArrayAlgo.hpp"
#include "composyx/utils/Error.hpp"

#include "composyx/interfaces/implicit_dense_kernels.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Attributes][Attributes:1]]
template <CPX_Scalar Scalar, int NbCol>
class DenseMatrix : public MatrixProperties<Scalar> {

public:
  using scalar_type = Scalar;
  using local_type = Scalar;
  using value_type = Scalar;
  using element_type = Scalar;
  using real_type = typename arithmetic_real<Scalar>::type;
  using index_type = size_t;
  using size_type = size_t;
  using rank_type = size_t;
  static const int static_nb_col = NbCol;

private:
  using Real = real_type;
  using Data = DenseData<Scalar, NbCol>;

  Data _data;
  bool _fixed_ptr = false;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Classic constructor][Classic constructor:1]]
public:
  // Constructors
  explicit DenseMatrix() : DenseMatrix(0, 0) {}
  explicit DenseMatrix(const size_t m, const size_t n = 1)
      : _data{Data(m, n)} {}
  explicit DenseMatrix(const size_t m, const size_t n, Scalar* ptr,
                       bool fixed_ptr = false) {
    if (fixed_ptr) {
      _data = Data(m, n, ptr, m);
    } else {
      _data = Data(m, n);
      std::memcpy(_data.get_ptr(), ptr, m * n * sizeof(Scalar));
    }
    _fixed_ptr = fixed_ptr;
  }

  explicit DenseMatrix(std::initializer_list<Scalar> l, const size_t m,
                       const size_t n = 1, const bool row_major = false)
      : _data(l, row_major ? n : m, row_major ? m : n), _fixed_ptr{false} {
    if (row_major) {
      _data.transpose();
    }
  }

  explicit DenseMatrix(std::initializer_list<Scalar> l) : _data(l) {
    static_assert(NbCol > 0, "DenseMatrix:: bracket initialization: static "
                             "NbCol or dimension (m, n) must be specified");
  }

  // Copy constructor
  DenseMatrix(const DenseMatrix& mat) : MatrixProperties<Scalar>(mat) {
    _fixed_ptr = false;
    _data.make_deepcopy_of(mat._data);
  }

  // Move constructor
  DenseMatrix(DenseMatrix&& mat) : _data(std::move(mat._data)) {
    if (&mat == this)
      return;
    _fixed_ptr = std::exchange(mat._fixed_ptr, false);
    this->copy_properties(mat);
    mat.set_default_properties();
  }

  DenseMatrix(const Data& data, bool fixed_ptr = false) {
    if (fixed_ptr) {
      _data = data.shallowcopy();
    } else {
      _data = data.deepcopy();
    }
    _fixed_ptr = fixed_ptr;
  }

  DenseMatrix(Data&& data, bool fixed_ptr = false) {
    _data = std::move(data);
    _fixed_ptr = fixed_ptr;
  }

  // Copying matrices with other NbCol
  template <int OtherNbCol>
  DenseMatrix(const DenseMatrix<Scalar, OtherNbCol>& mat) {
    if (mat.is_ptr_fixed()) {
      _fixed_ptr = true;
      _data.make_shallowcopy_of(mat.get_array());
    } else {
      _data.make_deepcopy_of(mat.get_array());
    }
    this->copy_properties(mat);
  }

  template <int OtherNbCol> DenseMatrix(DenseMatrix<Scalar, OtherNbCol>&& mat) {
    this->copy_properties(mat);
    _fixed_ptr = mat.is_ptr_fixed();
    _data = mat.move_array();
  }
  // Classic constructor:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Assignment operators][Assignment operators:1]]
private:
  template <int OtherNbCol>
  DenseMatrix& _assignment_lvalue(const DenseMatrix<Scalar, OtherNbCol>& mat) {
    if constexpr (OtherNbCol == NbCol) {
      if (&mat == this)
        return *this;
    }
    if (_fixed_ptr) {
      COMPOSYX_DIM_ASSERT(
          get_n_rows(), mat.get_n_rows(),
          "DenseMatrix with fixed ptr: assignement with different nb rows");
      COMPOSYX_DIM_ASSERT(
          get_n_cols(), mat.get_n_cols(),
          "DenseMatrix with fixed ptr: assignement with different nb cols");
      _data.copy_values(mat.get_array());
    } else {
      _data.make_deepcopy_of(mat.get_array());
    }
    this->copy_properties(mat);
    return *this;
  }

  template <int OtherNbCol>
  DenseMatrix& _assignment_rvalue(DenseMatrix<Scalar, OtherNbCol>&& mat) {
    if constexpr (OtherNbCol == NbCol) {
      if (&mat == this)
        return *this;
    }
    if (_fixed_ptr) {
      COMPOSYX_DIM_ASSERT(
          get_n_rows(), mat.get_n_rows(),
          "DenseMatrix with fixed ptr: assignement with different nb rows");
      COMPOSYX_DIM_ASSERT(
          get_n_cols(), mat.get_n_cols(),
          "DenseMatrix with fixed ptr: assignement with different nb cols");
      _data.copy_values(mat.get_array());
    } else {
      if (mat.is_ptr_fixed()) {
        _data.make_shallowcopy_of(mat.get_array());
        _fixed_ptr = true;
      } else {
        _data = mat.move_array();
      }
    }
    this->copy_properties(mat);
    mat.clear();
    return *this;
  }

public:
  template <int OtherNbCol>
  DenseMatrix& operator=(const DenseMatrix<Scalar, OtherNbCol>& mat) {
    return _assignment_lvalue<OtherNbCol>(mat);
  }

  template <int OtherNbCol>
  DenseMatrix& operator=(DenseMatrix<Scalar, OtherNbCol>&& mat) {
    return _assignment_rvalue<OtherNbCol>(std::move(mat));
  }

  DenseMatrix& operator=(const DenseMatrix& mat) {
    return _assignment_lvalue<NbCol>(mat);
  }

  DenseMatrix& operator=(DenseMatrix&& mat) {
    return _assignment_rvalue<NbCol>(std::move(mat));
  }
  // Assignment operators:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Comparison operators][Comparison operators:1]]
  template <int OtherNbCol>
  bool operator==(const DenseMatrix<Scalar, OtherNbCol>& mat2) const {
    return _data == mat2.get_array();
  }

  template <int OtherNbCol>
  bool operator!=(const DenseMatrix<Scalar, OtherNbCol>& mat2) const {
    return _data != mat2.get_array();
  }
  // Comparison operators:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Casting][Casting:1]]
  // Cast operations
  template <class OtherScalar>
  [[nodiscard]] DenseMatrix<OtherScalar, NbCol> cast() const {
    DenseMatrix<OtherScalar, NbCol> casted(get_n_rows(), get_n_cols());
    copy_properties<Scalar, OtherScalar>(*this, casted);
    for (size_t j = 0; j < get_n_cols(); ++j) {
      for (size_t i = 0; i < get_n_rows(); ++i) {
        casted(i, j) = static_cast<OtherScalar>(_data(i, j));
      }
    }
    return casted;
  }
  // Casting:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Conversion functions][Conversion functions:1]]
  DenseMatrix to_dense() const { return *this; };
  void convert(DenseMatrix& out_mat) const { out_mat = *this; }
  void convert(SparseMatrixCSC<Scalar>& out_mat) const {
    out_mat = SparseMatrixCSC<Scalar>(*this);
  }
  void convert(SparseMatrixCSR<Scalar>& out_mat) const {
    out_mat = SparseMatrixCSR<Scalar>(*this);
  }
  void convert(SparseMatrixCOO<Scalar>& out_mat) const {
    out_mat = SparseMatrixCOO<Scalar>(*this);
  }
  // Conversion functions:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Indexing][Indexing:1]]
  // Element accessor
  template <CPX_Integral Tint, CPX_Integral Sint>
  [[nodiscard]] inline Scalar& operator()(const Tint i, const Sint j) {
    return _data(i, j);
  }
  template <CPX_Integral Tint, CPX_Integral Sint>
  [[nodiscard]] inline const Scalar& operator()(const Tint i,
                                                const Sint j) const {
    return _data(i, j);
  }
  template <CPX_Integral Tint, CPX_Integral Sint>
  [[nodiscard]] inline Scalar& at(const Tint i, const Sint j) {
    return _data.at(i, j);
  }
  template <CPX_Integral Tint, CPX_Integral Sint>
  [[nodiscard]] inline const Scalar& at(const Tint i, const Sint j) const {
    return _data.at(i, j);
  }

  template <CPX_Integral Tint, CPX_Integral Sint>
  [[nodiscard]] inline Scalar coeff(const Tint i, const Sint j) const {
    if (this->is_storage_full())
      return _data(i, j);

    Scalar val = _data(i, j);

    if (this->is_storage_lower() && j > i) {
      val = _data(j, i);
    } else if (this->is_storage_upper() && i > j) {
      val = _data(j, i);
    }

    if constexpr (is_complex<Scalar>::value) {
      if (this->is_storage_lower() && j > i) {
        val = conj(val);
      } else if (this->is_storage_upper() && i > j) {
        val = conj(val);
      }
    }

    return val;
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline const Scalar& operator()(const Tint i) const {
    static_assert(NbCol == 1,
                  "DenseMatrix(i): only available with static NbCol = 1.");
    return _data[i];
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline Scalar& operator()(const Tint i) {
    static_assert(NbCol == 1,
                  "DenseMatrix(i): only available with static NbCol = 1.");
    return _data[i];
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline const Scalar& operator[](const Tint i) const {
    static_assert(NbCol == 1,
                  "DenseMatrix[i]: only available with static NbCol = 1.");
    return _data[i];
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline Scalar& operator[](const Tint i) {
    static_assert(NbCol == 1,
                  "DenseMatrix[i]: only available with static NbCol = 1.");
    return _data[i];
  }

  template <CPX_Integral Tint> [[nodiscard]] inline Scalar& at(const Tint i) {
    return _data.at(i);
  }
  template <CPX_Integral Tint>
  [[nodiscard]] inline const Scalar& at(const Tint i) const {
    return _data.at(i);
  }
  // Indexing:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Diagonal extraction][Diagonal extraction:1]]
  [[nodiscard]] DiagonalMatrix<Scalar> diag() const {
    const size_t k = std::min(get_n_rows(), get_n_cols());
    DiagonalMatrix<Scalar> diag(get_n_rows(), get_n_cols());
    for (size_t i = 0; i < k; ++i) {
      diag(i) = (*this)(i, i);
    }
    return diag;
  }

  [[nodiscard]] Vector<Scalar> diag_vect() const {
    const size_t k = std::min(get_n_rows(), get_n_cols());
    Vector<Scalar> diag(k);
    for (size_t i = 0; i < k; ++i) {
      diag(i) = (*this)(i, i);
    }
    return diag;
  }
  // Diagonal extraction:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Re-indexing][Re-indexing:1]]
  template <CPX_IntArray I, CPX_Integral Index = typename I::value_type>
  void reindex(const I& new_indices, int new_m = -1) {
    SparseMatrixCOO<Scalar, Index> s;
    this->convert(s);
    s.reindex(new_indices, new_m);
    (*this) = s.to_dense();
  }

  template <CPX_IntArray I, CPX_Integral Index = typename I::value_type>
  void reindex(const I& new_indices, const I& old_indices, int new_m = -1) {
    SparseMatrixCOO<Scalar, Index> s;
    this->convert(s);
    s.reindex(new_indices, old_indices, new_m);
    (*this) = s.to_dense();
  }
  // Re-indexing:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Transposition][Transposition:1]]
  void transpose() { this->_data.transpose(); }

  void conj_transpose() {
    this->_data.transpose();
    if constexpr (is_complex<Scalar>::value) {
      for (size_t j = 0; j < get_n_cols(); ++j) {
        for (size_t i = 0; i < get_n_rows(); ++i) {
          _data(i, j) = conj(_data(i, j));
        }
      }
    }
  }

  // Transposition temporary views (expression templates)
  [[nodiscard]] auto t() const {
    if constexpr (NbCol == 1) {
      return dense_ET::ScalOpVect<Scalar, Op::Trans>(*this, Scalar{1});
    } else {
      return dense_ET::ScalOpMat<Scalar, NbCol, Op::Trans>(*this, Scalar{1});
    }
  }

  [[nodiscard]] auto h() const {
    if constexpr (NbCol == 1) {
      return dense_ET::ScalOpVect<Scalar, Op::ConjTrans>(*this, Scalar{1});
    } else {
      return dense_ET::ScalOpMat<Scalar, NbCol, Op::ConjTrans>(*this,
                                                               Scalar{1});
    }
  }
  // Transposition:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Change storage][Change storage:1]]
  void to_storage_full() {
    if (this->is_storage_full())
      return;

    if (this->is_storage_lower()) {
      for (size_t j = 0; j < this->get_n_cols(); ++j) {
        for (size_t i = j + 1; i < this->get_n_rows(); ++i) {
          if (this->is_hermitian()) {
            (*this)(j, i) = conj((*this)(i, j));
          } else if (this->is_symmetric()) {
            (*this)(j, i) = (*this)(i, j);
          }
        }
      }
    }

    else if (this->is_storage_upper()) {
      if (this->is_sym_or_herm()) {
        for (size_t i = 0; i < this->get_n_rows(); ++i) {
          for (size_t j = i + 1; j < this->get_n_cols(); ++j) {
            if (this->is_hermitian()) {
              (*this)(j, i) = conj((*this)(i, j));
            } else if (this->is_symmetric()) {
              (*this)(j, i) = (*this)(i, j);
            }
          }
        }
      }
    }

    this->set_property(MatrixStorage::full);
  }

  void break_symmetry() {
    this->to_storage_full();
    this->set_property(MatrixSymmetry::general);
  }
  // Change storage:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix vector product][Matrix vector product:1]]
  // Matrix vector multiplication
  // Y.gemv(A, X, alpha=1, beta=0)
  // performs Y = alpha * op(A) * X + beta * Y
  template <int OtherNbCol>
  void gemv(const DenseMatrix<Scalar, OtherNbCol>& A,
            const DenseMatrix<Scalar, NbCol>& X, const Op opA = Op::NoTrans,
            const Scalar alpha = Scalar{1.0}, const Scalar beta = Scalar{0.0}) {
    if (&X == this) {
      auto Xcpy = X;
      COMPOSYX_BLAS::gemv(A, Xcpy, *this, op_char(opA), alpha, beta);
    } else {
      COMPOSYX_BLAS::gemv(A, X, *this, op_char(opA), alpha, beta);
    }
  }
  // Matrix vector product:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix matrix product][Matrix matrix product:1]]
  // Matrix multiplication
  // C.gemm(A, B, opA, opB, alpha=1, beta=0)
  // performs C = alpha * opA(A) * opB(B) + beta * C
  void gemm(const DenseMatrix& A, const DenseMatrix& B, const Op opA,
            const Op opB, const Scalar alpha = Scalar{1.0},
            const Scalar beta = Scalar{0.0}) {
    this->break_symmetry();
    if (B.is_sym_or_herm() and !B.is_storage_full()) {
      DenseMatrix Bcpy = B;
      Bcpy.break_symmetry();
      COMPOSYX_BLAS::gemm(A, Bcpy, *this, op_char(opA), op_char(opB), alpha,
                          beta);
    } else {
      COMPOSYX_BLAS::gemm(A, B, *this, op_char(opA), op_char(opB), alpha, beta);
    }
  }
  // Matrix matrix product:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Diagonal matrix matrix product][Diagonal matrix matrix product:1]]
  // A.left_diagmm(D) performs
  // A <- D * A
  //    (M, K) * (K, N)
  // Careful: break symmetry
  void left_diagmm(const DiagonalMatrix<Scalar>& D) {
    const Blas_int M = static_cast<Blas_int>(D.get_n_rows());
    const Blas_int N = static_cast<Blas_int>(this->get_n_cols());
    const Blas_int K = static_cast<Blas_int>(D.get_n_cols());

    COMPOSYX_DIM_ASSERT(this->get_n_rows(), K,
                        "(DiagonalMatrix) x (DenseMatrix) multiplication with "
                        "wrong dimensions");

    this->break_symmetry();
    // Should scale each row of the dense matrix by the diagonal coefficient
    const Scalar* diag_coeff = D.get_ptr();
    for (int i = 0; i < std::min(M, K); ++i) {
      auto rowi = this->get_row_view(i);
      COMPOSYX_BLAS::scal(rowi, diag_coeff[i]);
    }

    // If dimensions are changing
    if (D.get_n_cols() != D.get_n_rows()) {
      Data newdata(M, N);
      for (Blas_int j = 0; j < N; ++j) {
        for (Blas_int i = 0; i < std::min(M, K); ++i) {
          newdata(i, j) = _data(i, j);
        }
      }
      _data = std::move(newdata);
    }
  }

  // A.right_diagmm(D) performs
  // A <- A * D
  //    (M, K) * (K, N)
  // Careful: break symmetry
  void right_diagmm(const DiagonalMatrix<Scalar>& D) {
    const Blas_int M = static_cast<Blas_int>(this->get_n_rows());
    const Blas_int N = static_cast<Blas_int>(D.get_n_cols());
    const Blas_int K = static_cast<Blas_int>(this->get_n_cols());

    COMPOSYX_DIM_ASSERT(D.get_n_rows(), K,
                        "(DenseMatrix) x (DiagonalMatrix) multiplication with "
                        "wrong dimensions");

    this->break_symmetry();
    // Should scale each colums of the dense matrix by the diagonal coefficient
    const Scalar* diag_coeff = D.get_ptr();
    for (int j = 0; j < std::min(K, N); ++j) {
      auto colj = this->get_vect_view(j);
      COMPOSYX_BLAS::scal(colj, diag_coeff[j]);
    }

    // If dimensions are changing
    if (D.get_n_cols() != D.get_n_rows()) {
      Data newdata(M, N);
      for (Blas_int j = 0; j < std::min(K, N); ++j) {
        for (Blas_int i = 0; i < M; ++i) {
          newdata(i, j) = _data(i, j);
        }
      }
      _data = std::move(newdata);
    }
  }
  // Diagonal matrix matrix product:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Diagonal matrix vector product][Diagonal matrix vector product:1]]
  void diagmv(const DiagonalMatrix<Scalar>& D) {
    COMPOSYX_ASSERT(
        D.get_n_cols() == this->get_n_rows(),
        "Diagonal matrix vector multiplication with wrong dimensions");
    const Scalar* D_data = D.get_ptr();
    for (size_t k = 0; k < D.get_n_cols(); ++k) {
      this->_data[k] *= D_data[k];
    }
  }
  // Diagonal matrix vector product:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix addition][Matrix addition:1]]
  // Matrix addition
  void add(const DenseMatrix& mat, Scalar alpha = Scalar{1.0}) {

    if (this->get_storage_type() != mat.get_storage_type()) {
      this->to_storage_full();
      if (!mat.is_storage_full()) {
        DenseMatrix mat_cpy_full(mat);
        mat_cpy_full.to_storage_full();
        _data.add(mat_cpy_full._data, alpha);
      } else {
        _data.add(mat._data, alpha);
      }
    } else {
      _data.add(mat._data, alpha);
    }
  }

  template <CPX_Integral Index>
  void add(const SparseMatrixCSC<Scalar, Index>& mat,
           Scalar alpha = Scalar{1.0}) {
    mat.foreach_ijv(
        [&](Index i, Index j, Scalar v) { (*this)(i, j) += alpha * v; });
  }

  template <CPX_Integral Index>
  void add(const SparseMatrixCOO<Scalar, Index>& mat,
           Scalar alpha = Scalar{1.0}) {
    mat.foreach_ijv(
        [&](Index i, Index j, Scalar v) { (*this)(i, j) += alpha * v; });
  }

  template <CPX_Integral Index>
  void add(const SparseMatrixCSR<Scalar, Index>& mat,
           Scalar alpha = Scalar{1.0}) {
    mat.foreach_ijv(
        [&](Index i, Index j, Scalar v) { (*this)(i, j) += alpha * v; });
  }
  // Matrix addition:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Diagonal addition][Diagonal addition:1]]
  // Matrix addition
  void add(const DiagonalMatrix<Scalar>& mat, Scalar alpha = Scalar{1.0}) {
    const size_t M = mat.get_n_rows();
    const size_t N = mat.get_n_cols();
    COMPOSYX_ASSERT(get_n_rows() == M,
                    "Matrix + diagonal matrix addition with different M");
    COMPOSYX_ASSERT(get_n_cols() == N,
                    "Matrix + diagonal matrix addition with different N");
    const Scalar* diag = mat.get_ptr();
    for (size_t k = 0; k < std::min(M, N); ++k) {
      (*this)(k, k) += alpha * diag[k];
    }
  }
  // Diagonal addition:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Getters][Getters:1]]
public:
  // Getters
  [[nodiscard]] inline size_t get_n_rows() const { return _data.get_n_rows(); }
  [[nodiscard]] inline size_t get_n_cols() const { return _data.get_n_cols(); }
  [[nodiscard]] inline size_t get_nb_row() const { return _data.get_n_rows(); }
  [[nodiscard]] inline size_t get_nb_col() const { return _data.get_n_cols(); }
  [[nodiscard]] inline size_t get_leading_dim() const {
    return _data.get_leading_dim();
  }
  [[nodiscard]] inline size_t get_increment() const {
    return _data.get_increment();
  }
  [[nodiscard]] inline size_t size() const {
    return _data.get_n_rows() * _data.get_n_cols();
  }

  // MD-span like
  [[nodiscard]] consteval inline size_t rank() const { return 2; }

  template <CPX_Integral Tint>
  [[nodiscard]] inline size_t extent(Tint dim) const {
    if (dim == 0) {
      return _data.get_n_rows();
    } else if (dim == 1) {
      return _data.get_n_cols();
    }

    throw std::range_error("DenseMatrix::extent out of range (must be 0 or 1)");
    return 0;
  }

  [[nodiscard]] inline Scalar* get_ptr() { return _data.get_ptr(); }
  [[nodiscard]] inline const Scalar* get_ptr() const { return _data.get_ptr(); }
  [[nodiscard]] inline Scalar* data() { return _data.get_ptr(); }
  [[nodiscard]] inline const Scalar* data() const { return _data.get_ptr(); }

  template <CPX_Integral Tint>
  [[nodiscard]] inline Scalar* get_ptr(Tint i, Tint j) {
    return &_data(i, j);
  }
  template <CPX_Integral Tint>
  [[nodiscard]] inline const Scalar* get_ptr(Tint i, Tint j) const {
    return &_data(i, j);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline Scalar* get_vect_ptr(Tint j = 0) {
    return _data.get_vect_ptr(j);
  }
  template <CPX_Integral Tint>
  [[nodiscard]] inline const Scalar* get_vect_ptr(Tint j = 0) const {
    return _data.get_vect_ptr(j);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline const DenseMatrix<Scalar, 1> get_vect(Tint j = 0) const {
    return DenseMatrix<Scalar, 1>(get_n_rows(), 1, get_vect_ptr(j));
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline DenseMatrix<Scalar, 1> get_vect(Tint j = 0) {
    return DenseMatrix<Scalar, 1>(get_n_rows(), 1, get_vect_ptr(j));
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline DenseMatrix<Scalar, 1> get_col(Tint j = 0) {
    return get_vect(j);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline DenseMatrix<Scalar, 1> get_vect_view(Tint j = 0) {
    COMPOSYX_ASSERT(j <= static_cast<Tint>(get_n_cols()),
                    "DenseMatrix::get_vect_view: j > N");
    return DenseMatrix<Scalar, 1>(
        DenseData<Scalar, 1>(get_n_rows(), 1, get_vect_ptr(j)), true);
  }
  template <CPX_Integral Tint>
  [[nodiscard]] friend auto get_vect_view(DenseMatrix<Scalar, NbCol>& dm,
                                          Tint j = 0) {
    return dm.get_vect_view(j);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline const DenseMatrix<Scalar, 1>
  get_vect_view(Tint j = 0) const {
    COMPOSYX_ASSERT(j <= static_cast<Tint>(get_n_cols()),
                    "DenseMatrix::get_vect_view: j > N");
    return DenseMatrix<Scalar, 1>(
        DenseData<Scalar, 1>(get_n_rows(), 1,
                             const_cast<Scalar*>(get_vect_ptr(j))),
        true);
  }
  template <CPX_Integral Tint>
  [[nodiscard]] friend auto get_vect_view(const DenseMatrix<Scalar, NbCol>& dm,
                                          Tint j = 0) {
    return dm.get_vect_view(j);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline DenseMatrix<Scalar, -1> get_vect_view(Tint j,
                                                             size_t nb_col) {
    COMPOSYX_ASSERT(static_cast<size_t>(j) + nb_col <= get_n_cols(),
                    "DenseMatrix::get_vect_view: j + nb_col > N");
    return DenseMatrix<Scalar, -1>(
        DenseData<Scalar, -1>(this->get_n_rows(), nb_col, this->get_ptr(0, j)),
        true);
  }
  template <CPX_Integral Tint>
  [[nodiscard]] friend auto get_vect_view(DenseMatrix<Scalar, NbCol>& dm,
                                          Tint j, size_t nb_col) {
    return dm.get_vect_view(j, nb_col);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline DenseMatrix<Scalar, -1> get_vect(Tint j, size_t nb_col) {
    COMPOSYX_ASSERT(static_cast<size_t>(j) + nb_col <= get_n_cols(),
                    "DenseMatrix::get_vect_view: j + nb_col > N");
    return DenseMatrix<Scalar, -1>(
        DenseData<Scalar, -1>(this->get_n_rows(), nb_col, this->get_ptr(0, j)));
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline DenseMatrix<Scalar, 1> get_vect_view(Tint i, Tint j,
                                                            size_t m) {
    COMPOSYX_ASSERT(j <= static_cast<Tint>(get_n_cols()),
                    "DenseMatrix::get_vect_view: j > N");
    COMPOSYX_ASSERT(i + m <= get_n_rows(),
                    "DenseMatrix::get_vect_view: i + M > nb rows");
    return DenseMatrix<Scalar, 1>(DenseData<Scalar, 1>(m, this->get_ptr(i, j)),
                                  true);
  }
  template <CPX_Integral Tint>
  [[nodiscard]] friend auto get_vect_view(DenseMatrix<Scalar, NbCol>& dm,
                                          Tint i, Tint j, size_t m) {
    return dm.get_vect_view(i, j, m);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline const DenseMatrix<Scalar, 1>
  get_vect_view(Tint i, Tint j, size_t m) const {
    COMPOSYX_ASSERT(j <= static_cast<Tint>(get_n_cols()),
                    "DenseMatrix::get_vect_view: j > N");
    COMPOSYX_ASSERT(i + m <= get_n_rows(),
                    "DenseMatrix::get_vect_view: i + M > nb rows");
    return DenseMatrix<Scalar, 1>(
        DenseData<Scalar, 1>(m, const_cast<Scalar*>(this->get_ptr(i, j))),
        true);
  }
  template <CPX_Integral Tint>
  [[nodiscard]] friend auto get_vect_view(const DenseMatrix<Scalar, NbCol>& dm,
                                          Tint i, Tint j, size_t m) {
    return dm.get_vect_view(i, j, m);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline const DenseMatrix<Scalar, 1>
  get_col_view(Tint i, Tint j, size_t m) const {
    return get_vect_view(i, j, m);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline DenseMatrix<Scalar, 1> get_col_view(Tint i, Tint j,
                                                           size_t m) {
    return get_vect_view(i, j, m);
  }

  // Row and diag view: non-contiguous access, we need an increment
  template <CPX_Integral Tint>
  [[nodiscard]] inline const DenseMatrix<Scalar, 1>
  get_row_view(Tint i, Tint j, size_t n) const {
    COMPOSYX_ASSERT(i <= static_cast<Tint>(get_n_rows()),
                    "DenseMatrix::get_vect_view: i > M");
    COMPOSYX_ASSERT(j + n <= get_n_cols(),
                    "DenseMatrix::get_vect_view: i + N > nb cols");
    int inc = static_cast<int>(this->get_leading_dim());
    return DenseMatrix<Scalar, 1>(
        DenseData<Scalar, 1>(n, const_cast<Scalar*>(this->get_ptr(i, j)), 0,
                             inc),
        true);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline const DenseMatrix<Scalar, 1> get_row_view(Tint i) const {
    return get_row_view(i, 0, get_n_cols());
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline DenseMatrix<Scalar, -1> get_row(Tint i = 0) const {
    return get_row_view(i).copy().t();
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline DenseMatrix<Scalar, 1> get_row_view(Tint i, Tint j,
                                                           size_t n) {

    COMPOSYX_ASSERT(i <= static_cast<Tint>(get_n_rows()),
                    "DenseMatrix::get_vect_view: i > M");
    COMPOSYX_ASSERT(j + n <= get_n_cols(),
                    "DenseMatrix::get_vect_view: i + N > nb cols");
    int inc = static_cast<int>(this->get_leading_dim());
    return DenseMatrix<Scalar, 1>(
        DenseData<Scalar, 1>(n, this->get_ptr(i, j), 0, inc), true);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline const DenseMatrix<Scalar, 1> get_row_view(Tint i) {
    return get_row_view(i, 0, get_n_cols());
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline const DenseMatrix<Scalar, 1>
  get_diag_view(Tint i) const {
    COMPOSYX_ASSERT(i >= 0 || (-i < static_cast<Tint>(get_n_rows())),
                    "DenseMatrix::get_diag_view -index > nb rows");
    COMPOSYX_ASSERT(i <= 0 || (i < static_cast<Tint>(get_n_cols())),
                    "DenseMatrix::get_diag_view index > nb cols");
    const Scalar* ptr = (i >= 0) ? this->get_ptr(0, i) : this->get_ptr(-i, 0);
    size_t n_diag_elts = (i >= 0)
                             ? std::min(get_n_rows() + i, get_n_cols()) - i
                             : std::min(get_n_rows(), get_n_cols() - i) + i;
    int inc = static_cast<int>(this->get_leading_dim() + 1);
    return DenseMatrix<Scalar, 1>(
        DenseData<Scalar, 1>(n_diag_elts, const_cast<Scalar*>(ptr), 0, inc),
        true);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] inline DenseMatrix<Scalar, 1> get_diag_view(Tint i) {
    COMPOSYX_ASSERT(i >= 0 || (-i < static_cast<Tint>(get_n_rows())),
                    "DenseMatrix::get_diag_view -index > nb rows");
    COMPOSYX_ASSERT(i <= 0 || (i < static_cast<Tint>(get_n_cols())),
                    "DenseMatrix::get_diag_view index > nb cols");
    Scalar* ptr = (i >= 0) ? this->get_ptr(0, i) : this->get_ptr(-i, 0);
    size_t n_diag_elts = (i >= 0)
                             ? std::min(get_n_rows() + i, get_n_cols()) - i
                             : std::min(get_n_rows(), get_n_cols() - i) + i;
    int inc = static_cast<int>(this->get_leading_dim() + 1);
    return DenseMatrix<Scalar, 1>(
        DenseData<Scalar, 1>(n_diag_elts, ptr, 0, inc), true);
  }

  [[nodiscard]] inline const DenseMatrix<Scalar, 1> get_diag_view() const {
    return get_diag_view(0);
  }
  [[nodiscard]] inline DenseMatrix<Scalar, 1> get_diag_view() {
    return get_diag_view(0);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] DenseMatrix<Scalar> get_block_view(Tint i, Tint j, size_t m,
                                                   size_t n) {
    COMPOSYX_ASSERT(i >= 0, "DenseMatrix::get_block_view: i < 0");
    COMPOSYX_ASSERT(j >= 0, "DenseMatrix::get_block_view: j < 0");
    COMPOSYX_ASSERT(i + m <= get_n_rows(),
                    "DenseMatrix::get_block_view: i + M > nb rows");
    COMPOSYX_ASSERT(j + n <= get_n_cols(),
                    "DenseMatrix::get_block_view: j + N > nb rows");
    return DenseMatrix<Scalar>(
        DenseData<Scalar>(m, n, this->get_ptr(i, j), this->get_leading_dim()),
        true);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] const DenseMatrix<Scalar>
  get_block_view(Tint i, Tint j, size_t m, size_t n) const {
    COMPOSYX_ASSERT(i >= 0, "DenseMatrix::get_block_view: i < 0");
    COMPOSYX_ASSERT(j >= 0, "DenseMatrix::get_block_view: j < 0");
    COMPOSYX_ASSERT(i + m <= get_n_rows(),
                    "DenseMatrix::get_block_view: i + M > nb rows");
    COMPOSYX_ASSERT(j + n <= get_n_cols(),
                    "DenseMatrix::get_block_view: j + N > nb rows");
    return DenseMatrix<Scalar>(
        DenseData<Scalar>(m, n, const_cast<Scalar*>(this->get_ptr(i, j)),
                          this->get_leading_dim()),
        true);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] const DenseMatrix<Scalar> get_rows_view(Tint i_beg,
                                                        Tint i_end) const {
    return get_block_view(i_beg, 0, i_end - i_beg, get_n_cols());
  }

  template <CPX_Integral Tint>
  [[nodiscard]] const DenseMatrix<Scalar> get_columns_view(Tint j_beg,
                                                           Tint j_end) const {
    return get_block_view(0, j_beg, get_n_rows(), j_end - j_beg);
  }
  template <CPX_Integral Tint>
  [[nodiscard]] friend auto
  get_columns_view(const DenseMatrix<Scalar, NbCol>& dm, Tint j_beg,
                   Tint j_end) {
    return dm.get_columns_view(j_beg, j_end);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] DenseMatrix<Scalar> get_block_copy(Tint i, Tint j, size_t m,
                                                   size_t n) const {
    const DenseMatrix<Scalar> blk = this->get_block_view(i, j, m, n);
    return DenseMatrix<Scalar>(blk);
  }

  [[nodiscard]] inline const Data& get_array() const { return _data; }
  [[nodiscard]] Data&& move_array() { return std::move(_data); }
  [[nodiscard]] bool is_ptr_fixed() const { return _fixed_ptr; }
  void copy_data(const Data& data) { _data.make_deepcopy_of(data); }

  DenseMatrix copy() const { return DenseMatrix(*this); }

  const DenseMatrix view() const {
    return DenseMatrix<Scalar>(
        DenseData<Scalar>(get_n_rows(), get_n_cols(),
                          const_cast<Scalar*>(this->get_ptr()),
                          this->get_leading_dim()),
        true);
  }

  DenseMatrix view() {
    return DenseMatrix<Scalar>(
        DenseData<Scalar>(get_n_rows(), get_n_cols(),
                          const_cast<Scalar*>(this->get_ptr()),
                          this->get_leading_dim()),
        true);
  }
  // Getters:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Setters][Setters:1]]
  void clear() {
    _data = Data();
    _fixed_ptr = false;
  }

  template <CPX_Integral Tint>
  void set_vector(const DenseMatrix<Scalar, 1>& v, Tint j) {
    this->get_vect_view(j) = v;
  }

  template <CPX_Integral Tint>
  void set_vector(DenseMatrix<Scalar, 1>&& v, Tint j) {
    this->get_vect_view(j) = std::move(v);
  }
  // Setters:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Memory cost][Memory cost:1]]
  [[nodiscard]] inline size_t memcost() const {
    if (_data.is_view())
      return sizeof(Scalar*);
    return get_n_rows() * get_n_cols() * sizeof(Scalar);
  }
  // Memory cost:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Begin and end][Begin and end:1]]
  DenseMatrixIterator<Scalar, NbCol> begin() const {
    return DenseMatrixIterator<Scalar, NbCol>(*this);
  }

  DenseMatrixIterator<Scalar, NbCol> end() const {
    return DenseMatrixIterator<Scalar, NbCol>(*this).end();
  }
  // Begin and end:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Display function][Display function:1]]
  // Pretty printing
  void display(const std::string& name = "",
               std::ostream& out = std::cout) const {
    if (!name.empty())
      out << name << '\n';
    out << "m: " << get_n_rows() << " ";

    constexpr const int textwidth = (is_complex<Scalar>::value) ? 23 : 10;

    if constexpr (NbCol == 1) {
      out << "(vector)\n\n";
      for (size_t i = 0; i < get_n_rows(); ++i) {
        out << (*this)[i] << '\n';
      }
    } else {
      out << "n: " << get_n_cols() << " " << '\n' << this->properties_str();
      out << "\n\n";

      for (size_t i = 0; i < get_n_rows(); ++i) {
        for (size_t j = 0; j < get_n_cols(); ++j) {
          if (this->is_storage_lower() && j > i) {
            out << std::setw(textwidth) << '-' << ' ';
          } else if (this->is_storage_upper() && i > j) {
            out << std::setw(textwidth) << '-' << ' ';
          } else {
            out << std::setw(textwidth) << std::setprecision(3)
                << std::scientific << (*this)(i, j) << ' ';
          }
        }
        out << '\n';
      }
      out << '\n';
    }
    out << '\n';
  }
  // Display function:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Display in python][Display in python:1]]
  // Pretty printing
  void display_python(std::string name = "",
                      std::ostream& out = std::cout) const {
    if (name.empty())
      name = "A";
    out << name << " = np.array([\n";

    const int textwidth = 10;

    for (size_t i = 0; i < get_n_rows(); ++i) {

      if (i == 0) {
        out << "    [";
      } else {
        out << '[';
      }

      std::stringstream line;

      for (size_t j = 0; j < get_n_cols(); ++j) {
        Scalar val = this->coeff(i, j);

        if constexpr (is_complex<Scalar>::value) {
          line << std::setw(textwidth) << std::setprecision(3)
               << std::scientific << val.real();

          if (val.imag() > 0) {
            line << " + " << std::setw(textwidth) << std::setprecision(3)
                 << std::scientific << val.imag();
          } else {
            line << " - " << std::setw(textwidth) << std::setprecision(3)
                 << std::scientific << std::abs(val.imag());
          }

          line << "j, ";
        }

        else {
          line << std::setw(textwidth) << std::setprecision(3)
               << std::scientific << val << ", ";
        }
      }

      auto str = line.str();
      if (str.size() >= 2) {
        str.pop_back();
        str.pop_back();
      }
      //if constexpr(is_complex<Scalar>::value){
      //  if(str.size() >= 1){
      //	  str.pop_back();
      //  }
      //}

      out << str << ']';
      if (i < get_n_rows() - 1)
        out << ",\n    ";
    }

    out << "], dtype='" << arithmetic_numpy_name<Scalar>::name << "')\n\n";
  }
  // Display in python:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Operators][Operators:1]]
  // Scalar multiplication
  DenseMatrix& operator*=(const Scalar& scal) {
    if (scal != Scalar{1}) {
      _data *= scal;
    }
    return *this;
  }

  DenseMatrix& operator-() {
    (*this) *= Scalar{-1};
    return *this;
  }

  DenseMatrix& operator+=(const DenseMatrix<Scalar>& mat) {
    add(mat);
    return *this;
  }

  DenseMatrix& operator+=(const DiagonalMatrix<Scalar>& mat) {
    add(mat);
    return *this;
  }

  template <CPX_Integral Index>
  DenseMatrix& operator+=(const SparseMatrixCSC<Scalar, Index>& mat) {
    add(mat);
    return *this;
  }

  template <CPX_Integral Index>
  DenseMatrix& operator+=(const SparseMatrixCSR<Scalar, Index>& mat) {
    add(mat);
    return *this;
  }

  template <CPX_Integral Index>
  DenseMatrix& operator+=(const SparseMatrixCOO<Scalar, Index>& mat) {
    add(mat);
    return *this;
  }

  DenseMatrix& operator-=(const DenseMatrix& mat) {
    add(mat, Scalar{-1.0});
    return *this;
  }

  DenseMatrix& operator-=(const DiagonalMatrix<Scalar>& mat) {
    add(mat, Scalar{-1.0});
    return *this;
  }

  template <CPX_Integral Index>
  DenseMatrix& operator-=(const SparseMatrixCSC<Scalar, Index>& mat) {
    add(mat, Scalar{-1.0});
    return *this;
  }

  template <CPX_Integral Index>
  DenseMatrix& operator-=(const SparseMatrixCSR<Scalar, Index>& mat) {
    add(mat, Scalar{-1.0});
    return *this;
  }

  template <CPX_Integral Index>
  DenseMatrix& operator-=(const SparseMatrixCOO<Scalar, Index>& mat) {
    add(mat, Scalar{-1.0});
    return *this;
  }

  template <int OtherNbCol>
  DenseMatrix& operator*=(const DenseMatrix<Scalar, OtherNbCol>& B) {
    // We have to allocate a new matrix
    const DenseMatrix<Scalar> A = *this;
    // A *= A
    if (this == &B) {
      this->gemm(A, A, Op::NoTrans, Op::NoTrans, Scalar{1.0}, Scalar{0.0});
    } else {
      this->gemm(A, B, Op::NoTrans, Op::NoTrans, Scalar{1.0}, Scalar{0.0});
    }
    return *this;
  }

  template <Op opB, int OtherNbCol>
  DenseMatrix&
  operator*=(const dense_ET::ScalOpMat<Scalar, OtherNbCol, opB>& B) {
    // We have to allocate a new matrix
    const DenseMatrix<Scalar> A = *this;
    if (this == &(B.mat)) { // A *= A.t()
      this->gemm(A, A, Op::NoTrans, opB, B.scal, Scalar{0.0});
    } else {
      this->gemm(A, B.mat, Op::NoTrans, opB, B.scal, Scalar{0.0});
    }
    return *this;
  }

  DenseMatrix& operator*=(const DiagonalMatrix<Scalar>& D) {
    // We have to allocate a new matrix
    right_diagmm(D);
    return *this;
  }
  // Operators:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Frobenius norm][Frobenius norm:1]]
  [[nodiscard]] Real frobenius_norm() const {
    Real norm{0.0};
    for (size_t j = 0; j < get_n_cols(); ++j) {
      for (size_t i = 0; i < get_n_rows(); ++i) {
        norm += std::norm(_data(i, j));
      }
    }
    return std::sqrt(norm);
  }

  Real norm() const { return frobenius_norm(); }
  // Frobenius norm:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Dot product][Dot product:1]]
  template <int OtherNbCol>
  Scalar dot(const DenseMatrix<Scalar, OtherNbCol>& v2) const {
    COMPOSYX_DIM_ASSERT(
        get_n_rows(), v2.get_n_rows(),
        "DenseMatrix::dot: Scalar dot product different row numbers");
    if constexpr (NbCol != 1) {
      COMPOSYX_DIM_ASSERT(get_n_cols(), 1,
                          "DenseMatrix::dot: dot only works on 1 column");
    }
    if constexpr (OtherNbCol != 1) {
      COMPOSYX_DIM_ASSERT(v2.get_n_cols(), 1,
                          "DenseMatrix::dot: dot only works on 1 column");
    }
    return _data.dot(v2._data);
  }

  template <int OtherNbCol>
  Scalar dotu(const DenseMatrix<Scalar, OtherNbCol>& v2) const {
    COMPOSYX_DIM_ASSERT(
        get_n_rows(), v2.get_n_rows(),
        "DenseMatrix::dot: Scalar dot product different row numbers");
    if constexpr (NbCol != 1) {
      COMPOSYX_DIM_ASSERT(get_n_cols(), 1,
                          "DenseMatrix::dot: dot only works on 1 column");
    }
    if constexpr (OtherNbCol != 1) {
      COMPOSYX_DIM_ASSERT(v2.get_n_cols(), 1,
                          "DenseMatrix::dot: dot only works on 1 column");
    }
    return _data.dotu(v2._data);
  }

  template <int OtherNbCol>
  std::vector<Scalar>
  cwise_dot(const DenseMatrix<Scalar, OtherNbCol>& M2) const {
    COMPOSYX_DIM_ASSERT(
        get_n_rows(), M2.get_n_rows(),
        "DenseMatrix::dot: Matrix column-wise dot different row numbers");
    COMPOSYX_DIM_ASSERT(
        get_n_cols(), M2.get_n_cols(),
        "DenseMatrix::dot: Matrix column-wise dot different col numbers");
    std::vector<Scalar> v_out(get_n_cols());
    for (size_t j = 0; j < get_n_cols(); ++j) {
      auto xj = M2.get_vect_view(j);
      auto yj = this->get_vect_view(j);
      v_out[j] = yj.dot(xj);
    }
    return v_out;
  }

  template <int NbCol2 = NbCol>
  DenseMatrix<Scalar, NbCol>
  dot_block(const DenseMatrix<Scalar, NbCol2>& M2) const {
    COMPOSYX_DIM_ASSERT(get_n_rows(), M2.get_n_rows(),
                        "Matrix dot_block different row numbers");
    DenseMatrix<Scalar, NbCol> res(get_n_cols(), M2.get_n_cols());
    res.gemm(*this, M2, Op::Trans, Op::NoTrans);
    return res;
  }
  // Dot product:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Low-rank update][Low-rank update:1]]
  template <int OtherNbCol1, int OtherNbCol2>
  void geru(const DenseMatrix<Scalar, OtherNbCol1>& v1,
            const DenseMatrix<Scalar, OtherNbCol2>& v2, Scalar alpha = 1) {
    COMPOSYX_DIM_ASSERT(get_n_rows(), v1.get_n_rows(),
                        "DenseMatrix::geru: matrix row number do not match");
    COMPOSYX_DIM_ASSERT(get_n_cols(), v2.get_n_rows(),
                        "DenseMatrix::geru: matrix col number do not match");
    COMPOSYX_DIM_ASSERT(
        v1.get_n_cols(), v2.get_n_cols(),
        "DenseMatrix::geru: low-rank matrices dim do not match");
    COMPOSYX_BLAS::geru(*this, v1, v2, alpha);
  }

  template <int OtherNbCol1, int OtherNbCol2>
  void ger(const DenseMatrix<Scalar, OtherNbCol1>& v1,
           const DenseMatrix<Scalar, OtherNbCol2>& v2, Scalar alpha = 1) {
    COMPOSYX_DIM_ASSERT(get_n_rows(), v1.get_n_rows(),
                        "DenseMatrix::geru: matrix row number do not match");
    COMPOSYX_DIM_ASSERT(get_n_cols(), v2.get_n_rows(),
                        "DenseMatrix::geru: matrix col number do not match");
    COMPOSYX_DIM_ASSERT(
        v1.get_n_cols(), v2.get_n_cols(),
        "DenseMatrix::geru: low-rank matrices dim do not match");
    COMPOSYX_BLAS::ger(*this, v1, v2, alpha);
  }
  // Low-rank update:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Expression templates (matrix)][Expression templates (matrix):1]]
  // ScalOpMat
  template <Op op, int NbCol2 = NbCol>
  DenseMatrix(const dense_ET::ScalOpMat<Scalar, NbCol2, op>& sm)
      : DenseMatrix(sm.mat) {
    debug_log_ET::log("DenseMatrix(ScalOpMat)");
    (*this) *= sm.scal;
    apply_op<op>(*this);
  }

  template <Op op, int NbCol2 = NbCol>
  DenseMatrix& operator=(const dense_ET::ScalOpMat<Scalar, NbCol2, op>& sm) {
    debug_log_ET::log("DenseMatrix = ScalOpMat");
    if (this != &(sm.mat))
      *this = sm.mat;
    _data *= sm.scal;
    apply_op<op>(*this);
    return *this;
  }

  // MatMat
  template <int NbColA, int NbColB, Op opA, Op opB>
  DenseMatrix(const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB> matmat)
      : DenseMatrix{matmat.eval()} {}

  template <int NbColA, int NbColB, Op opA, Op opB>
  DenseMatrix&
  operator=(const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB> matmat) {
    (*this) = matmat.eval();
    return *this;
  }

  // ETgemm
  template <int NbColA, int NbColB, int NbColC, Op opA, Op opB, Op opC>
  DenseMatrix(
      const dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC, opA, opB, opC>
          etgemm)
      : DenseMatrix(etgemm.beta_C.mat) {
    debug_log_ET::log("DenseMatrix(ETgemm)");
    apply_op<opC>(*this);
    const auto& A = etgemm.alpha_AB.sm_A.mat;
    const auto& B = etgemm.alpha_AB.mat_B;
    Scalar alpha = etgemm.alpha_AB.sm_A.scal;
    Scalar beta = etgemm.beta_C.scal;
    this->gemm(A, B, opA, opB, alpha, beta);
  }

  template <int NbColA, int NbColB, int NbColC, Op opA, Op opB, Op opC>
  DenseMatrix& operator=(
      const dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC, opA, opB, opC>
          etgemm) {
    debug_log_ET::log("DenseMatrix = ETgemm");
    const auto& A = etgemm.alpha_AB.sm_A.mat;
    const auto& B = etgemm.alpha_AB.mat_B;
    const auto& C = etgemm.beta_C.mat;
    Scalar alpha = etgemm.alpha_AB.sm_A.scal;
    Scalar beta = etgemm.beta_C.scal;

    // GEMV: C <- alpha A B + beta C
    // If A == C or B == C, we need to make a copy

    if (this == &A) {
      auto A_copy = A;
      if (this != &C)
        (*this) = C;
      apply_op<opC>(*this);
      debug_log_ET::log(" -> gemm");
      if (this == &B) { // alpha C * C + beta C
        this->gemm(A_copy, A_copy, opA, opB, alpha, beta);
      } else {
        this->gemm(A_copy, B, opA, opB, alpha, beta);
      }
    } else if (this == &B) {
      auto B_copy = B;
      if (this != &C)
        (*this) = C;
      apply_op<opC>(*this);
      debug_log_ET::log(" -> gemm");
      this->gemm(A, B_copy, opA, opB, alpha, beta);
    } else {
      if (this != &C)
        (*this) = C;
      apply_op<opC>(*this);
      debug_log_ET::log(" -> gemm");
      this->gemm(A, B, opA, opB, alpha, beta);
    }
    return *this;
  }
  // Expression templates (matrix):1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Expression templates (vectors)][Expression templates (vectors):1]]
  template <int NbCol2, Op opA>
  DenseMatrix(const dense_ET::MatVect<Scalar, NbCol2, opA>& mv) {
    debug_log_ET::log("DenseMatrix(MatVect)");
    if constexpr (NbCol == 1) {
      debug_log_ET::log(" -> (NbCol = 1) gemv");
      this->gemv(mv.sm.mat, mv.v, opA, mv.sm.scal, Scalar{0});
    } else {
      if (mv.v.get_n_cols() == 1) {
        debug_log_ET::log(" -> gemv");
        this->gemv(mv.sm.mat, mv.v, opA, mv.sm.scal, Scalar{0});
      } else {
        debug_log_ET::log(" -> gemm");
        this->gemm(mv.sm.mat, mv.v, opA, Op::NoTrans, mv.sm.scal, Scalar{0});
      }
    }
  }

  template <int NbCol2, Op opA>
  DenseMatrix& operator=(const dense_ET::MatVect<Scalar, NbCol2, opA>& mv) {
    debug_log_ET::log("DenseMatrix = MatVect");
    if constexpr (NbCol == 1) {
      debug_log_ET::log(" -> (NbCol = 1) gemv");
      this->gemv(mv.sm.mat, mv.v, opA, mv.sm.scal, Scalar{0});
    } else {
      if (mv.v.get_n_cols() == 1) {
        debug_log_ET::log(" -> gemv");
        this->gemv(mv.sm.mat, mv.v, opA, mv.sm.scal, Scalar{0});
      } else {
        debug_log_ET::log(" -> gemm");
        this->gemm(mv.sm.mat, mv.v, opA, Op::NoTrans, mv.sm.scal, Scalar{0});
      }
    }
    return *this;
  }

  template <int NbCol2, Op opA>
  DenseMatrix(const dense_ET::ETgemv<Scalar, NbCol2, opA>& gmv)
      : DenseMatrix(gmv.y.vect) {
    debug_log_ET::log("DenseMatrix(ETgemv)");
    static_assert(
        NbCol == 1,
        "DenseMatrix(const ETgemv<Scalar>&): only available NbCol = 1");
    const auto& A = gmv.Ax.sm.mat;
    const auto& x = gmv.Ax.v;
    //const auto& y = gmv.y.vect;
    const Scalar& alpha = gmv.Ax.sm.scal;
    const Scalar& beta = gmv.y.scal;
    debug_log_ET::log(" -> gemv");
    this->gemv(A, x, opA, alpha, beta);
  }

  template <int NbCol2, Op opA>
  DenseMatrix& operator=(const dense_ET::ETgemv<Scalar, NbCol2, opA>& gmv) {
    debug_log_ET::log("DenseMatrix = ETgemv");
    static_assert(NbCol == 1, "DenseMatrix& operator=(const ETgemv<Scalar>&): "
                              "only available NbCol = 1");
    const auto& A = gmv.Ax.sm.mat;
    const auto& x = gmv.Ax.v;
    const auto& y = gmv.y.vect;
    const Scalar& alpha = gmv.Ax.sm.scal;
    const Scalar& beta = gmv.y.scal;

    // GEMV: y <- alpha A x + beta y
    // If y == x, we need to make a copy
    // y <- alpha A (copy(y)) + beta Y

    if (this == &x) {
      auto x_copy = x;
      if (this != &y)
        (*this) = y;
      debug_log_ET::log(" -> gemv");
      this->gemv(A, x_copy, opA, alpha, beta);
    } else { // y <- alpha A x + beta y
      if (this != &y)
        (*this) = y;
      debug_log_ET::log(" -> gemv");
      this->gemv(A, x, opA, alpha, beta);
    }
    return *this;
  }

  template <Op op>
  DenseMatrix(const dense_ET::ScalOpVect<Scalar, op>& sv)
      : DenseMatrix(sv.vect) {
    debug_log_ET::log("DenseMatrix(ScalOpVect)");
    (*this) *= sv.scal;
    apply_op<op>(*this);
  }

  template <Op op>
  DenseMatrix& operator=(const dense_ET::ScalOpVect<Scalar, op>& sv) {
    debug_log_ET::log("DenseMatrix = ScalOpVect");
    if (this != &(sv.vect))
      *this = sv.vect;
    _data *= sv.scal;
    apply_op<op>(*this);
    return *this;
  }
  // Expression templates (vectors):1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*COO][COO:1]]
  template <CPX_Integral Index, Op opA>
  inline DenseMatrix&
  operator=(const SpCOOMV<Scalar, Index, opA, DenseMatrix>& spmv_coo);

  template <CPX_Integral Index, Op opA>
  DenseMatrix(const SpCOOMV<Scalar, Index, opA, DenseMatrix>& spmv_coo)
      : DenseMatrix(spmv_coo.eval()) {}

  template <CPX_Integral Index, Op opA>
  inline friend DenseMatrix
  operator+(const DenseMatrix& dm,
            const SpCOOMV<Scalar, Index, opA, DenseMatrix>& spmv_coo) {
    return dm + spmv_coo.eval();
  }

  template <CPX_Integral Index, Op opA>
  inline friend DenseMatrix
  operator+(const SpCOOMV<Scalar, Index, opA, DenseMatrix>& spmv_coo,
            const DenseMatrix& dm) {
    return dm + spmv_coo.eval();
  }

  template <CPX_Integral Index, Op opA>
  inline friend DenseMatrix
  operator-(const DenseMatrix& dm,
            const SpCOOMV<Scalar, Index, opA, DenseMatrix>& spmv_coo) {
    return dm - spmv_coo.eval();
  }

  template <CPX_Integral Index, Op opA>
  inline friend DenseMatrix
  operator-(const SpCOOMV<Scalar, Index, opA, DenseMatrix>& spmv_coo,
            const DenseMatrix& dm) {
    return spmv_coo.eval() - dm;
  }
  // COO:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*CSC][CSC:1]]
  template <CPX_Integral Index, Op opA>
  inline DenseMatrix&
  operator=(const SpCSCMV<Scalar, Index, opA, DenseMatrix>& spmv_coo);

  template <CPX_Integral Index, Op opA>
  DenseMatrix(const SpCSCMV<Scalar, Index, opA, DenseMatrix>& spmv_coo)
      : DenseMatrix(spmv_coo.eval()) {}

  template <CPX_Integral Index, Op opA>
  inline friend DenseMatrix
  operator+(const DenseMatrix& dm,
            const SpCSCMV<Scalar, Index, opA, DenseMatrix>& spmv_coo) {
    return dm + spmv_coo.eval();
  }

  template <CPX_Integral Index, Op opA>
  inline friend DenseMatrix
  operator+(const SpCSCMV<Scalar, Index, opA, DenseMatrix>& spmv_coo,
            const DenseMatrix& dm) {
    return dm + spmv_coo.eval();
  }

  template <CPX_Integral Index, Op opA>
  inline friend DenseMatrix
  operator-(const DenseMatrix& dm,
            const SpCSCMV<Scalar, Index, opA, DenseMatrix>& spmv_coo) {
    return dm - spmv_coo.eval();
  }

  template <CPX_Integral Index, Op opA>
  inline friend DenseMatrix
  operator-(const SpCSCMV<Scalar, Index, opA, DenseMatrix>& spmv_coo,
            const DenseMatrix& dm) {
    return spmv_coo.eval() - dm;
  }
  // CSC:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*CSR][CSR:1]]
  template <CPX_Integral Index, Op opA>
  inline DenseMatrix&
  operator=(const SpCSRMV<Scalar, Index, opA, DenseMatrix>& spmv_coo);

  template <CPX_Integral Index, Op opA>
  DenseMatrix(const SpCSRMV<Scalar, Index, opA, DenseMatrix>& spmv_coo)
      : DenseMatrix(spmv_coo.eval()) {}

  template <CPX_Integral Index, Op opA>
  inline friend DenseMatrix
  operator+(const DenseMatrix& dm,
            const SpCSRMV<Scalar, Index, opA, DenseMatrix>& spmv_coo) {
    return dm + spmv_coo.eval();
  }

  template <CPX_Integral Index, Op opA>
  inline friend DenseMatrix
  operator+(const SpCSRMV<Scalar, Index, opA, DenseMatrix>& spmv_coo,
            const DenseMatrix& dm) {
    return dm + spmv_coo.eval();
  }

  template <CPX_Integral Index, Op opA>
  inline friend DenseMatrix
  operator-(const DenseMatrix& dm,
            const SpCSRMV<Scalar, Index, opA, DenseMatrix>& spmv_coo) {
    return dm - spmv_coo.eval();
  }

  template <CPX_Integral Index, Op opA>
  inline friend DenseMatrix
  operator-(const SpCSRMV<Scalar, Index, opA, DenseMatrix>& spmv_coo,
            const DenseMatrix& dm) {
    return spmv_coo.eval() - dm;
  }
  // CSR:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Serialization][Serialization:1]]
  std::vector<char> serialize() const {
    // M, N, values
    int M = static_cast<int>(_data.get_n_rows());
    int N = static_cast<int>(_data.get_n_cols());
    std::vector<char> buffer(2 * sizeof(int) + M * N * sizeof(Scalar));
    int cnt = 0;

    std::memcpy(&buffer[cnt], &M, sizeof(int));
    cnt += sizeof(int);
    std::memcpy(&buffer[cnt], &N, sizeof(int));
    cnt += sizeof(int);

    if (static_cast<int>(_data.get_leading_dim()) == M) {
      std::memcpy(&buffer[cnt], _data.get_ptr(), M * N * sizeof(Scalar));
      cnt += M * N * sizeof(Scalar);
    } else { // Copy vector by vector
      for (int j = 0; j < N; ++j) {
        std::memcpy(&buffer[cnt], _data.get_vect_ptr(j), M * sizeof(Scalar));
        cnt += M * sizeof(Scalar);
      }
    }

    return buffer;
  }

  void deserialize(const std::vector<char>& buffer) {
    int M, N;
    int cnt = 0;
    std::memcpy(&M, &buffer[cnt], sizeof(int));
    cnt += sizeof(int);
    std::memcpy(&N, &buffer[cnt], sizeof(int));
    cnt += sizeof(int);
    _data = Data(static_cast<size_t>(M), static_cast<size_t>(N));
    std::memcpy(_data.get_ptr(), &buffer[cnt], N * M * sizeof(Scalar));
    _fixed_ptr = false;
  }
  // Serialization:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Factories][Factories:1]]
  static DenseMatrix<Scalar, NbCol> full(const size_t M, const size_t N,
                                         const Scalar s) {
    DenseMatrix<Scalar, NbCol> A(M, N);
    for (size_t j = 0; j < N; ++j) {
      for (size_t i = 0; i < M; ++i) {
        A(i, j) = s;
      }
    }
    return A;
  }

  static DenseMatrix<Scalar, 1> ones(const size_t M) {
    return DenseMatrix<Scalar, 1>::full(M, 1, Scalar{1});
  }

  static DenseMatrix<Scalar, NbCol> ones(const size_t M, const size_t N) {
    return DenseMatrix<Scalar, NbCol>::full(M, N, Scalar{1});
  }

  static DenseMatrix<Scalar, NbCol> eye(const size_t M, const size_t N) {
    DenseMatrix<Scalar, NbCol> A(M, N);
    for (size_t i = 0; i < std::min(M, N); ++i) {
      A(i, i) = Scalar{1};
    }
    return A;
  }

  static DenseMatrix<Scalar, NbCol> identity(const size_t M) {
    return DenseMatrix<Scalar, NbCol>::eye(M, M);
  }

  static DenseMatrix<Scalar, NbCol>
  from_matrix_market(const std::string& filename) {
    MatrixSymmetry sym;
    MatrixStorage stor;
    size_t m, n, nnz;
    std::vector<int> i, j;
    std::vector<Scalar> v;
    matrix_market::load(filename, i, j, v, m, n, nnz, sym, stor);
    DenseMatrix<Scalar, NbCol> out(m, n);
    for (size_t k = 0; k < nnz; ++k) {
      out(i[k], j[k]) = v[k];
    }
    out.set_property(sym);
    out.set_property(stor);
    return out;
  }
  // Factories:1 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Factories][Factories:2]]
  static DenseMatrix<Scalar, NbCol> view(const size_t M, const size_t N,
                                         Scalar* ptr, const size_t ld) {
    return DenseMatrix<Scalar, NbCol>(DenseData<Scalar, NbCol>(M, N, ptr, ld),
                                      true);
  }

  template <CPX_DenseVector OtherType>
  static DenseMatrix<Scalar, 1> view_vector(OtherType& other) {
    auto M = composyx::size(other);
    Scalar* ptr = composyx::get_ptr(other);
    return DenseMatrix<Scalar, 1>::view(M, 1, ptr, M);
  }

  template <CPX_DenseMatrix OtherType>
  static DenseMatrix<Scalar, NbCol> view_matrix(OtherType& other) {
    auto M = n_rows(other);
    auto N = n_cols(other);
    Scalar* ptr = composyx::get_ptr(other);
    auto ld = composyx::get_leading_dim(other);
    return DenseMatrix<Scalar, NbCol>::view(M, N, ptr, ld);
  }

  template <typename OtherType>
  static DenseMatrix<Scalar, NbCol> view(OtherType& o) {
    if constexpr (NbCol == 1) {
      return DenseMatrix<Scalar, NbCol>::view_vector(o);
    } else {
      return DenseMatrix<Scalar, NbCol>::view_matrix(o);
    }
  }

  template <typename OtherType> void update_view(OtherType& other) {
    this->clear();
    (*this) = DenseMatrix::view(other);
  }
  // Factories:2 ends here

  // [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Factories][Factories:3]]
}; // class DenseMatrix
// Factories:3 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Scalar times vector / matrix with transposition operation][Scalar times vector / matrix with transposition operation:1]]
template <CPX_Scalar Scalar>
inline dense_ET::ScalOpVect<Scalar> operator*(const DenseMatrix<Scalar, 1>& v,
                                              Scalar s) {
  return dense_ET::ScalOpVect<Scalar>(v, s);
}

template <CPX_Scalar Scalar>
inline dense_ET::ScalOpVect<Scalar> operator*(Scalar s,
                                              const DenseMatrix<Scalar, 1>& v) {
  return dense_ET::ScalOpVect<Scalar>(v, s);
}

template <CPX_Scalar Scalar>
inline dense_ET::ScalOpVect<Scalar> operator/(const DenseMatrix<Scalar, 1>& v,
                                              Scalar s) {
  COMPOSYX_ASSERT(s != Scalar{0}, "Division by 0");
  return dense_ET::ScalOpVect<Scalar>(v, Scalar{1.0} / s);
}

template <CPX_Scalar Scalar, int NbCol>
inline dense_ET::ScalOpMat<Scalar, NbCol>
operator*(const DenseMatrix<Scalar, NbCol>& m, Scalar s) {
  return dense_ET::ScalOpMat<Scalar, NbCol>(m, s);
}

template <CPX_Scalar Scalar, int NbCol>
inline dense_ET::ScalOpMat<Scalar, NbCol>
operator*(Scalar s, const DenseMatrix<Scalar, NbCol>& m) {
  return dense_ET::ScalOpMat<Scalar, NbCol>(m, s);
}

template <CPX_Scalar Scalar, int NbCol>
inline dense_ET::ScalOpMat<Scalar, NbCol>
operator/(const DenseMatrix<Scalar, NbCol>& m, Scalar s) {
  COMPOSYX_ASSERT(s != Scalar{0}, "Division by 0");
  return dense_ET::ScalOpMat<Scalar, NbCol>(m, Scalar{1.0} / s);
}

template <CPX_Scalar Scalar>
DenseMatrix<Scalar, 1>
operator+(DenseMatrix<Scalar, 1> v1,
          const dense_ET::ScalOpVect<Scalar, Op::NoTrans>& vx) {
  COMPOSYX_BLAS::axpy(vx.vect, v1, vx.scal);
  return v1;
}

template <CPX_Scalar Scalar>
DenseMatrix<Scalar, 1>
operator+(const dense_ET::ScalOpVect<Scalar, Op::NoTrans>& vx,
          DenseMatrix<Scalar, 1> v1) {
  COMPOSYX_BLAS::axpy(vx.vect, v1, vx.scal);
  return v1;
}

template <CPX_Scalar Scalar>
DenseMatrix<Scalar, 1>
operator-(DenseMatrix<Scalar, 1> v1,
          const dense_ET::ScalOpVect<Scalar, Op::NoTrans>& vx) {
  COMPOSYX_BLAS::axpy(vx.vect, v1, vx.scal * Scalar{-1});
  return v1;
}

template <CPX_Scalar Scalar>
DenseMatrix<Scalar, 1>
operator-(const dense_ET::ScalOpVect<Scalar, Op::NoTrans>& vx,
          DenseMatrix<Scalar, 1> v1) {
  COMPOSYX_BLAS::axpy(vx.vect, v1, vx.scal * Scalar{-1});
  v1 *= Scalar{-1};
  return v1;
}

template <CPX_Scalar Scalar, int NbCol1, int NbCol2, Op opA>
DenseMatrix<Scalar, NbCol2>
operator+(const DenseMatrix<Scalar, NbCol1>& mat1,
          const dense_ET::ScalOpMat<Scalar, NbCol2, opA>& sc_mat) {
  DenseMatrix<Scalar, NbCol2> res(sc_mat.mat);
  apply_op<opA>(res);
  res *= sc_mat.scal;
  res += mat1;
  return res;
}

template <CPX_Scalar Scalar, int NbCol1, int NbCol2, Op opA>
DenseMatrix<Scalar, NbCol2>
operator+(const dense_ET::ScalOpMat<Scalar, NbCol2, opA>& sc_mat,
          const DenseMatrix<Scalar, NbCol1>& mat1) {
  DenseMatrix<Scalar, NbCol2> res(sc_mat.mat);
  apply_op<opA>(res);
  res *= sc_mat.scal;
  res += mat1;
  return res;
}

template <CPX_Scalar Scalar, int NbCol1, int NbCol2, Op opA>
DenseMatrix<Scalar, NbCol2>
operator-(const DenseMatrix<Scalar, NbCol1>& mat1,
          const dense_ET::ScalOpMat<Scalar, NbCol2, opA>& sc_mat) {
  DenseMatrix<Scalar, NbCol2> res(sc_mat.mat);
  apply_op<opA>(res);
  res *= (sc_mat.scal * Scalar{-1});
  res += mat1;
  return res;
  ;
}

template <CPX_Scalar Scalar, int NbCol1, int NbCol2, Op opA>
DenseMatrix<Scalar, NbCol2>
operator-(const dense_ET::ScalOpMat<Scalar, NbCol2, opA>& sc_mat,
          const DenseMatrix<Scalar, NbCol1>& mat1) {
  DenseMatrix<Scalar, NbCol2> res(sc_mat.mat);
  apply_op<opA>(res);
  res *= sc_mat.scal;
  res -= mat1;
  return res;
}
// Scalar times vector / matrix with transposition operation:1 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix vector product][Matrix vector product:1]]
// Matrix vector product
// Ax
template <CPX_Scalar Scalar, int NbCol>
inline auto operator*(const DenseMatrix<Scalar, NbCol>& m,
                      const DenseMatrix<Scalar, 1>& v) {
  return dense_ET::MatVect<Scalar, NbCol, Op::NoTrans>(
      dense_ET::ScalOpMat<Scalar, NbCol, Op::NoTrans>(m, Scalar{1}), v);
}
// Matrix vector product:1 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix vector product][Matrix vector product:2]]
// alpha * (Ax)
template <CPX_Scalar Scalar, int NbCol, Op opA>
inline auto operator*(const Scalar s,
                      const dense_ET::MatVect<Scalar, NbCol, opA>& mv) {
  return dense_ET::MatVect<Scalar, NbCol, opA>(mv.sm * s, mv.v);
}
// Matrix vector product:2 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix vector product][Matrix vector product:3]]
// (Ax) * alpha
template <CPX_Scalar Scalar, int NbCol, Op opA>
inline auto operator*(const dense_ET::MatVect<Scalar, NbCol, opA>& mv,
                      const Scalar s) {
  return dense_ET::MatVect<Scalar, NbCol, opA>(mv.sm * s, mv.v);
}
// Matrix vector product:3 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix vector product][Matrix vector product:4]]
// A * (beta * x)
template <CPX_Scalar Scalar, int NbCol>
inline auto operator*(const DenseMatrix<Scalar>& m,
                      const dense_ET::ScalOpVect<Scalar>& sv) {
  return dense_ET::MatVect<Scalar, NbCol, Op::NoTrans>(
      dense_ET::ScalOpMat<Scalar, NbCol, Op::NoTrans>(m, sv.scal), sv.vect);
}
// Matrix vector product:4 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix vector product][Matrix vector product:5]]
// (alpha * A) * x
template <CPX_Scalar Scalar, int NbCol, Op opA>
inline auto operator*(const dense_ET::ScalOpMat<Scalar, NbCol, opA>& sm,
                      const Vector<Scalar>& v) {
  return dense_ET::MatVect<Scalar, NbCol, opA>(sm, v);
}
// Matrix vector product:5 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix vector product][Matrix vector product:6]]
// (alpha * A) * (beta * x)
template <CPX_Scalar Scalar, int NbCol, Op opA>
inline auto operator*(const dense_ET::ScalOpMat<Scalar, NbCol, opA>& sm,
                      const dense_ET::ScalOpVect<Scalar, Op::NoTrans>& sv) {
  return dense_ET::MatVect<Scalar, NbCol, opA>(sm * sv.scal, sv.vect);
}
// Matrix vector product:6 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix vector product][Matrix vector product:7]]
template <CPX_Scalar Scalar, int NbColA, int NbColB, Op opA, Op opB>
inline DenseMatrix<Scalar, 1>
operator+(const dense_ET::MatVect<Scalar, NbColA, opA>& mv1,
          const dense_ET::MatVect<Scalar, NbColB, opB> mv2) {
  const auto& A = mv1.sm.mat;
  const auto& x = mv1.v;
  Scalar alpha = mv1.sm.scal;
  const auto& B = mv2.sm.mat;
  const auto& y = mv2.v;
  Scalar beta = mv2.sm.scal;
  DenseMatrix<Scalar, 1> out(x);
  debug_log_ET::log(" -> gemv (beta=0)");
  out.gemv(A, x, opA, alpha, Scalar{0});
  debug_log_ET::log(" -> gemv (beta=1)");
  out.gemv(B, y, opB, beta, Scalar{1});
  return out;
}

template <CPX_Scalar Scalar, int NbColA, int NbColB, Op opA, Op opB>
inline DenseMatrix<Scalar, 1>
operator-(const dense_ET::MatVect<Scalar, NbColA, opA>& mv1,
          const dense_ET::MatVect<Scalar, NbColB, opB> mv2) {
  return mv1 +
         MatVect<Scalar, NbColB, opB>(dense_ET::ScalOpMat<Scalar, NbColB, opA>(
                                          mv2.sm.mat, mv2.sm.scal * Scalar{-1}),
                                      mv2.v);
}
// Matrix vector product:7 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMV][GEMV:1]]
// GEMV expression template: alpha * M * X + beta * Y
// y + (alpha * Ax)
template <CPX_Scalar Scalar, int NbCol, Op opA>
inline auto operator+(const Vector<Scalar>& v2,
                      const dense_ET::MatVect<Scalar, NbCol, opA>& ax) {
  return dense_ET::ETgemv<Scalar, NbCol, opA>(
      ax, dense_ET::ScalOpVect<Scalar, Op::NoTrans>(v2, Scalar{1}));
}
// GEMV:1 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMV][GEMV:2]]
// (alpha * Ax) + y
template <CPX_Scalar Scalar, int NbCol, Op opA>
inline auto operator+(const dense_ET::MatVect<Scalar, NbCol, opA>& ax,
                      const Vector<Scalar>& v2) {
  return dense_ET::ETgemv<Scalar, NbCol, opA>(
      ax, dense_ET::ScalOpVect<Scalar, Op::NoTrans>(v2, Scalar{1}));
}
// GEMV:2 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMV][GEMV:3]]
// y - (alpha * Ax)
template <CPX_Scalar Scalar, int NbCol, Op opA>
inline auto operator-(const Vector<Scalar>& v2,
                      const dense_ET::MatVect<Scalar, NbCol, opA>& ax) {
  return dense_ET::ETgemv<Scalar, NbCol, opA>(
      ax * Scalar{-1},
      dense_ET::ScalOpVect<Scalar, Op::NoTrans>(v2, Scalar{1}));
}

// y - (alpha * Ax)
template <CPX_Scalar Scalar, int NbCol, int NbCol2, Op opA>
inline auto operator-(const DenseMatrix<Scalar, NbCol2>& v2,
                      const dense_ET::MatVect<Scalar, NbCol, opA>& ax) {
  return v2 - ax.eval();
}
// GEMV:3 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMV][GEMV:4]]
// (alpha * Ax) - y
template <CPX_Scalar Scalar, int NbCol, Op opA>
inline auto operator-(const dense_ET::MatVect<Scalar, NbCol, opA>& ax,
                      const Vector<Scalar>& v2) {
  return dense_ET::ETgemv<Scalar, NbCol, opA>(
      ax, dense_ET::ScalOpVect<Scalar, Op::NoTrans>(v2, Scalar{-1}));
}
// GEMV:4 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMV][GEMV:5]]
// (beta * y) + (alpha * Ax)
template <CPX_Scalar Scalar, int NbCol, Op opA>
inline auto operator+(const dense_ET::ScalOpVect<Scalar>& sv,
                      const dense_ET::MatVect<Scalar, NbCol, opA>& ax) {
  return dense_ET::ETgemv<Scalar, NbCol, opA>(ax, sv);
}
// GEMV:5 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMV][GEMV:6]]
// (alpha * Ax) + (beta * y)
template <CPX_Scalar Scalar, int NbCol, Op opA>
inline auto operator+(const dense_ET::MatVect<Scalar, NbCol, opA>& ax,
                      const dense_ET::ScalOpVect<Scalar>& sv) {
  return dense_ET::ETgemv<Scalar, NbCol, opA>(ax, sv);
}
// GEMV:6 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMV][GEMV:7]]
// (beta * y) - (alpha * Ax)
template <CPX_Scalar Scalar, int NbCol, Op opA>
inline auto operator-(const dense_ET::ScalOpVect<Scalar>& sv,
                      const dense_ET::MatVect<Scalar, NbCol, opA>& ax) {
  return dense_ET::ETgemv<Scalar, NbCol, opA>(ax * Scalar{-1}, sv);
}
// GEMV:7 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMV][GEMV:8]]
// (alpha * Ax) - (beta * y)
template <CPX_Scalar Scalar, int NbCol, Op opA>
inline auto operator-(const dense_ET::MatVect<Scalar, NbCol, opA>& ax,
                      const dense_ET::ScalOpVect<Scalar>& sv) {
  return dense_ET::ETgemv<Scalar, NbCol, opA>(ax, sv * Scalar{-1});
}
// GEMV:8 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix matrix product][Matrix matrix product:1]]
template <CPX_Scalar Scalar, int NbCol1, int NbCol2>
inline auto operator*(const DenseMatrix<Scalar, NbCol1>& A,
                      const DenseMatrix<Scalar, NbCol2>& B) {
  return dense_ET::MatMat(
      dense_ET::ScalOpMat<Scalar, NbCol1, Op::NoTrans>(A, Scalar{1}), B);
}
// Matrix matrix product:1 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix matrix product][Matrix matrix product:2]]
template <CPX_Scalar Scalar, int NbCol1, int NbCol2, Op opA>
inline auto operator*(const dense_ET::ScalOpMat<Scalar, NbCol1, opA>& scA,
                      const DenseMatrix<Scalar, NbCol2>& B) {
  return dense_ET::MatMat<Scalar, NbCol1, NbCol2, opA, Op::NoTrans>(scA, B);
}
// Matrix matrix product:2 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix matrix product][Matrix matrix product:3]]
template <CPX_Scalar Scalar, int NbCol1, int NbCol2, Op opB>
inline auto operator*(const DenseMatrix<Scalar, NbCol1>& A,
                      const dense_ET::ScalOpMat<Scalar, NbCol2, opB>& scB) {
  return dense_ET::MatMat<Scalar, NbCol1, NbCol2, Op::NoTrans, opB>(
      dense_ET::ScalOpMat<Scalar, NbCol1, Op::NoTrans>(A, scB.scal), scB.mat);
}
// Matrix matrix product:3 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix matrix product][Matrix matrix product:4]]
template <CPX_Scalar Scalar, int NbCol1, int NbCol2, Op opA, Op opB>
inline auto operator*(const dense_ET::ScalOpMat<Scalar, NbCol1, opA>& scA,
                      const dense_ET::ScalOpMat<Scalar, NbCol2, opB>& scB) {
  return dense_ET::MatMat<Scalar, NbCol1, NbCol2, opA, opB>(
      dense_ET::ScalOpMat<Scalar, NbCol1, opA>(scA.mat, scA.scal * scB.scal),
      scB.mat);
}
// Matrix matrix product:4 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix matrix product][Matrix matrix product:5]]
template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, Op opA, Op opB>
inline auto
operator*(const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB>& mm,
          const DenseMatrix<Scalar, NbColC>& C) {
  const auto& A = mm.sm_A.mat;
  const auto& B = mm.mat_B;
  Scalar alpha = mm.sm_A.scal;

  const size_t M = (opA == Op::NoTrans) ? n_rows(A) : n_cols(A);
  const size_t N = (opB == Op::NoTrans) ? n_cols(B) : n_rows(B);

  DenseMatrix<Scalar, -1> tmp(M, N);
  tmp.gemm(A, B, opA, opB, alpha, Scalar{0});
  DenseMatrix<Scalar, -1> res(M, n_cols(C));
  res.gemm(tmp, C, Op::NoTrans, Op::NoTrans, Scalar{1}, Scalar{0});

  return res;
}

template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, Op opA, Op opB>
inline auto
operator*(const DenseMatrix<Scalar, NbColC>& C,
          const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB>& mm) {
  const auto& A = mm.sm_A.mat;
  const auto& B = mm.mat_B;
  Scalar alpha = mm.sm_A.scal;

  const size_t M = (opA == Op::NoTrans) ? n_rows(A) : n_cols(A);
  const size_t N = (opB == Op::NoTrans) ? n_cols(B) : n_rows(B);

  DenseMatrix<Scalar, -1> tmp(M, N);
  tmp.gemm(A, B, opA, opB, alpha, Scalar{0});
  DenseMatrix<Scalar, -1> res(n_rows(C), N);
  res.gemm(C, tmp, Op::NoTrans, Op::NoTrans, Scalar{1}, Scalar{0});

  return res;
}
// Matrix matrix product:5 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix matrix product][Matrix matrix product:6]]
template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, Op opA, Op opB,
          Op opC>
inline auto
operator*(const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB>& mm,
          const dense_ET::ScalOpMat<Scalar, NbColC, opC>& scC) {
  const auto& A = mm.sm_A.mat;
  const auto& B = mm.mat_B;
  const auto& C = scC.mat;
  Scalar alpha = mm.sm_A.scal;
  Scalar beta = scC.scal;

  const size_t M = (opA == Op::NoTrans) ? n_rows(A) : n_cols(A);
  const size_t N = (opB == Op::NoTrans) ? n_cols(B) : n_rows(B);
  const size_t res_N = (opC == Op::NoTrans) ? n_cols(C) : n_rows(C);

  DenseMatrix<Scalar, -1> tmp(M, N);
  tmp.gemm(A, B, opA, opB, alpha, Scalar{0});
  DenseMatrix<Scalar, -1> res(M, res_N);
  res.gemm(tmp, C, Op::NoTrans, opC, beta, Scalar{0});
}

template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, Op opA, Op opB,
          Op opC>
inline auto
operator*(const dense_ET::ScalOpMat<Scalar, NbColC, opC>& scC,
          const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB>& mm) {
  const auto& A = mm.sm_A.mat;
  const auto& B = mm.mat_B;
  const auto& C = scC.mat;
  Scalar alpha = mm.sm_A.scal;
  Scalar beta = scC.scal;

  const size_t M = (opA == Op::NoTrans) ? n_rows(A) : n_cols(A);
  const size_t N = (opB == Op::NoTrans) ? n_cols(B) : n_rows(B);
  const size_t res_M = (opC == Op::NoTrans) ? n_rows(C) : n_cols(C);

  DenseMatrix<Scalar, -1> tmp(M, N);
  tmp.gemm(A, B, opA, opB, alpha, Scalar{0});
  DenseMatrix<Scalar, -1> res(res_M, N);
  res.gemm(C, tmp, opC, Op::NoTrans, beta, Scalar{0});
}
// Matrix matrix product:6 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMM][GEMM:1]]
template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, Op opA, Op opB>
inline auto
operator+(const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB>& mm,
          const DenseMatrix<Scalar, NbColC>& C) {
  return dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC, opA, opB,
                          Op::NoTrans>(
      mm, dense_ET::ScalOpMat<Scalar, NbColC, Op::NoTrans>(C, Scalar{1}));
}

template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, Op opA, Op opB>
inline auto
operator+(const DenseMatrix<Scalar, NbColC>& C,
          const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB>& mm) {
  return dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC, opA, opB,
                          Op::NoTrans>(
      mm, dense_ET::ScalOpMat<Scalar, NbColC, Op::NoTrans>(C, Scalar{1}));
}
// GEMM:1 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMM][GEMM:2]]
template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, Op opA, Op opB>
inline auto
operator-(const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB>& mm,
          const DenseMatrix<Scalar, NbColC>& C) {
  return dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC, opA, opB,
                          Op::NoTrans>(
      mm, dense_ET::ScalOpMat<Scalar, NbColC, Op::NoTrans>(C, Scalar{-1}));
}
// GEMM:2 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMM][GEMM:3]]
template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, Op opA, Op opB>
inline auto
operator-(const DenseMatrix<Scalar, NbColC>& C,
          const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB>& mm) {
  dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB> minus_mm(
      dense_ET::ScalOpMat<Scalar, NbColA, opA>(mm.sm_A.mat,
                                               mm.sm_A.scal * Scalar{-1}),
      mm.mat_B);
  dense_ET::ScalOpMat<Scalar, NbColC, Op::NoTrans> one_C(C, Scalar{1});
  return dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC, opA, opB,
                          Op::NoTrans>(minus_mm, one_C);
}
// GEMM:3 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMM][GEMM:4]]
template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, Op opA, Op opB,
          Op opC>
inline auto
operator+(const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB>& mm,
          const dense_ET::ScalOpMat<Scalar, NbColC, opC>& scC) {
  return dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC, opA, opB, opC>(mm,
                                                                         scC);
}

template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, Op opA, Op opB,
          Op opC>
inline auto
operator+(const dense_ET::ScalOpMat<Scalar, NbColC, opC>& scC,
          const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB>& mm) {
  return dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC, opA, opB, opC>(mm,
                                                                         scC);
}
// GEMM:4 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMM][GEMM:5]]
template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, Op opA, Op opB,
          Op opC>
inline auto
operator-(const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB>& mm,
          const dense_ET::ScalOpMat<Scalar, NbColC, opC>& scC) {
  return dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC, opA, opB, opC>(
      mm, Scalar{-1} * scC);
}
// GEMM:5 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMM][GEMM:6]]
template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, Op opA, Op opB,
          Op opC>
inline auto
operator-(const dense_ET::ScalOpMat<Scalar, NbColC, opC>& scC,
          const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB>& mm) {
  dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB> minus_mm(
      ScalOpMat<Scalar, NbColA, opA>(mm.sm_A.mat, mm.sm_A.scal * Scalar{-1}),
      mm.mat_B);
  return dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC, opA, opB, opC>(
      minus_mm, scC);
}
// GEMM:6 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMM][GEMM:7]]
template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, int NbColD,
          Op opA, Op opB, Op opC>
inline auto operator+(const dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC,
                                             opA, opB, opC>& etgemm,
                      const DenseMatrix<Scalar, NbColD>& D) {
  return etgemm.eval() + D;
}

template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, int NbColD,
          Op opA, Op opB, Op opC>
inline auto operator+(const DenseMatrix<Scalar, NbColD>& D,
                      const dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC,
                                             opA, opB, opC>& etgemm) {
  return D + etgemm.eval();
}

template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, int NbColD,
          Op opA, Op opB, Op opC>
inline auto operator-(const dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC,
                                             opA, opB, opC>& etgemm,
                      const DenseMatrix<Scalar, NbColD>& D) {
  return etgemm.eval() - D;
}

template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, int NbColD,
          Op opA, Op opB, Op opC>
inline auto operator-(const DenseMatrix<Scalar, NbColD>& D,
                      const dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC,
                                             opA, opB, opC>& etgemm) {
  return D - etgemm.eval();
}
// GEMM:7 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMM][GEMM:8]]
template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, int NbColD,
          Op opA, Op opB, Op opC, Op opD>
inline auto operator+(const dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC,
                                             opA, opB, opC>& etgemm,
                      const dense_ET::ScalOpMat<Scalar, NbColD, opD>& scD) {
  return etgemm.eval() + scD.eval();
}

template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, int NbColD,
          Op opA, Op opB, Op opC, Op opD>
inline auto operator+(const dense_ET::ScalOpMat<Scalar, NbColD, opD>& scD,
                      const dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC,
                                             opA, opB, opC>& etgemm) {
  return scD.eval() + etgemm.eval();
}

template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, int NbColD,
          Op opA, Op opB, Op opC, Op opD>
inline auto operator-(const dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC,
                                             opA, opB, opC>& etgemm,
                      const dense_ET::ScalOpMat<Scalar, NbColD, opD>& scD) {
  return etgemm.eval() - scD.eval();
}

template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, int NbColD,
          Op opA, Op opB, Op opC, Op opD>
inline auto operator-(const dense_ET::ScalOpMat<Scalar, NbColD, opD>& scD,
                      const dense_ET::ETgemm<Scalar, NbColA, NbColB, NbColC,
                                             opA, opB, opC>& etgemm) {
  return scD.eval() - etgemm.eval();
}
// GEMM:8 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMM][GEMM:9]]
template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, int NbColD,
          Op opA, Op opB, Op opC, Op opD>
inline auto
operator+(const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB>& mm1,
          const dense_ET::MatMat<Scalar, NbColC, NbColD, opC, opD>& mm2) {
  const auto& A = mm1.sm_A.mat;
  const auto& B = mm1.mat_B;
  const auto& C = mm2.sm_A.mat;
  const auto& D = mm2.mat_B;
  Scalar alpha = mm1.sm_A.scal;
  Scalar beta = mm2.sm_A.scal;

  const size_t M = (opA == Op::NoTrans) ? n_rows(A) : n_cols(A);
  const size_t N = (opB == Op::NoTrans) ? n_cols(B) : n_rows(B);

  DenseMatrix<Scalar, -1> res(M, N);
  res.gemm(A, B, opA, opB, alpha, Scalar{0});
  res.gemm(C, D, opC, opD, beta, Scalar{1});

  return res;
}

template <CPX_Scalar Scalar, int NbColA, int NbColB, int NbColC, int NbColD,
          Op opA, Op opB, Op opC, Op opD>
inline auto
operator-(const dense_ET::MatMat<Scalar, NbColA, NbColB, opA, opB>& mm1,
          const dense_ET::MatMat<Scalar, NbColC, NbColD, opC, opD>& mm2) {
  return mm1 + dense_ET::MatMat<Scalar, NbColC, NbColD, opC, opD>(
                   dense_ET::ScalOpMat<Scalar, NbColC, opC>(
                       mm2.sm_A.mat, Scalar{-1} * mm2.sm_A.scal),
                   mm2.mat_B);
}
// GEMM:9 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Others][Others:1]]
// Matrix addition
template <CPX_Scalar Scalar, int NbCol1, int NbCol2>
inline DenseMatrix<Scalar, NbCol1>
operator+(DenseMatrix<Scalar, NbCol1> A, const DenseMatrix<Scalar, NbCol2>& B) {
  A += B;
  return A;
}

template <CPX_Scalar Scalar, CPX_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol>
operator+(DenseMatrix<Scalar, NbCol> A,
          const SparseMatrixCSC<Scalar, Index>& B) {
  A += B;
  return A;
}

template <CPX_Scalar Scalar, CPX_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol>
operator+(DenseMatrix<Scalar, NbCol> A,
          const SparseMatrixCSR<Scalar, Index>& B) {
  A += B;
  return A;
}

template <CPX_Scalar Scalar, CPX_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol>
operator+(DenseMatrix<Scalar, NbCol> A,
          const SparseMatrixCOO<Scalar, Index>& B) {
  A += B;
  return A;
}

template <CPX_Scalar Scalar, CPX_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol>
operator+(const SparseMatrixCSC<Scalar, Index>& A,
          DenseMatrix<Scalar, NbCol> B) {
  B += A;
  return B;
}

template <CPX_Scalar Scalar, CPX_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol>
operator+(const SparseMatrixCSR<Scalar, Index>& A,
          DenseMatrix<Scalar, NbCol> B) {
  B += A;
  return B;
}

template <CPX_Scalar Scalar, CPX_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol>
operator+(const SparseMatrixCOO<Scalar, Index>& A,
          DenseMatrix<Scalar, NbCol> B) {
  B += A;
  return B;
}

// Matrix substraction
template <CPX_Scalar Scalar, int NbCol1, int NbCol2>
inline DenseMatrix<Scalar, NbCol1>
operator-(DenseMatrix<Scalar, NbCol1> A, const DenseMatrix<Scalar, NbCol2>& B) {
  A -= B;
  return A;
}

template <CPX_Scalar Scalar, CPX_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol>
operator-(DenseMatrix<Scalar, NbCol> A,
          const SparseMatrixCSC<Scalar, Index>& B) {
  A -= B;
  return B;
}

template <CPX_Scalar Scalar, CPX_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol>
operator-(DenseMatrix<Scalar, NbCol> A,
          const SparseMatrixCSR<Scalar, Index>& B) {
  A -= B;
  return B;
}

template <CPX_Scalar Scalar, CPX_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol>
operator-(DenseMatrix<Scalar, NbCol> A,
          const SparseMatrixCOO<Scalar, Index>& B) {
  A -= B;
  return B;
}

template <CPX_Scalar Scalar, CPX_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol>
operator-(const SparseMatrixCSC<Scalar, Index>& A,
          DenseMatrix<Scalar, NbCol> B) {
  B *= Scalar{-1};
  B += A;
  return B;
}

template <CPX_Scalar Scalar, CPX_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol>
operator-(const SparseMatrixCSR<Scalar, Index>& A,
          DenseMatrix<Scalar, NbCol> B) {
  B *= Scalar{-1};
  B += A;
  return B;
}

template <CPX_Scalar Scalar, CPX_Integral Index, int NbCol>
inline DenseMatrix<Scalar, NbCol>
operator-(const SparseMatrixCOO<Scalar, Index>& A,
          DenseMatrix<Scalar, NbCol> B) {
  B *= Scalar{-1};
  B += A;
  return B;
}
// Others:1 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*GEMV interface][GEMV interface:1]]
// Y <- alpha A X + beta Y
template <CPX_Scalar Scalar, int NbCol, int NbCol2, Op opA>
void gemv(const DenseMatrix<Scalar, NbCol>& A,
          const DenseMatrix<Scalar, NbCol2>& X, DenseMatrix<Scalar, NbCol2>& Y,
          const Scalar alpha = Scalar{1.0}, const Scalar beta = Scalar{0.0}) {
  Y.gemv(A, X, opA, alpha, beta);
}
// GEMV interface:1 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Traits][Traits:1]]
template <CPX_Scalar Scalar, int NbCol>
struct vector_type<DenseMatrix<Scalar, NbCol>> : public std::true_type {
  using type = Vector<Scalar>;
};

template <CPX_Scalar Scalar, int NbCol>
struct sparse_type<DenseMatrix<Scalar, NbCol>> : public std::true_type {
  using type = SparseMatrixCSC<Scalar>;
};

template <CPX_Scalar Scalar, int NbCol>
struct dense_type<DenseMatrix<Scalar, NbCol>> : public std::true_type {
  using type = DenseMatrix<Scalar, NbCol>;
};

template <CPX_Scalar Scalar, int NbCol>
struct diag_type<DenseMatrix<Scalar, NbCol>> : public std::true_type {
  using type = DiagonalMatrix<Scalar>;
};

template <CPX_Scalar Scalar, int NbCol>
struct scalar_type<DenseMatrix<Scalar, NbCol>> : public std::true_type {
  using type = Scalar;
};

template <typename Scalar, int NbCol>
struct is_dense<DenseMatrix<Scalar, NbCol>> : public std::true_type {};

template <CPX_Scalar Scalar, int NbCol>
struct real_type<DenseMatrix<Scalar, NbCol>> : public std::true_type {
  using Real = typename arithmetic_real<Scalar>::type;
  using type = DenseMatrix<Real, NbCol>;
};

template <CPX_Scalar Scalar, int NbCol>
struct complex_type<DenseMatrix<Scalar, NbCol>> : public std::true_type {
  using Complex = typename arithmetic_complex<Scalar>::type;
  using type = DenseMatrix<Complex, NbCol>;
};
// Traits:1 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Interface functions][Interface functions:1]]
template <CPX_Scalar Scalar, int NbCol>
[[nodiscard]] inline size_t size(const DenseMatrix<Scalar, NbCol>& mat) {
  return mat.get_n_rows() * mat.get_n_cols();
}

template <CPX_Scalar Scalar>
[[nodiscard]] constexpr size_t n_cols(const DenseMatrix<Scalar, 1>&) {
  return 1;
}

template <CPX_Scalar Scalar, int NbCol>
[[nodiscard]] inline size_t n_rows(const DenseMatrix<Scalar, NbCol>& mat) {
  return mat.get_n_rows();
}

template <CPX_Scalar Scalar, int NbCol>
[[nodiscard]] inline size_t n_cols(const DenseMatrix<Scalar, NbCol>& mat) {
  return mat.get_n_cols();
}

template <CPX_Scalar Scalar, int NbCol>
[[nodiscard]] inline size_t
get_leading_dim(const DenseMatrix<Scalar, NbCol>& mat) {
  return mat.get_leading_dim();
}

template <CPX_Scalar Scalar, int NbCol>
[[nodiscard]] inline size_t
get_increment(const DenseMatrix<Scalar, NbCol>& mat) {
  return mat.get_increment();
}

template <CPX_Scalar Scalar, int NbCol1, int NbCol2>
inline Scalar dot(const DenseMatrix<Scalar, NbCol1>& v1,
                  const DenseMatrix<Scalar, NbCol2>& v2) {
  return v1.dot(v2);
}

template <CPX_Scalar Scalar, int NbCol1, int NbCol2>
inline std::vector<Scalar> cwise_dot(const DenseMatrix<Scalar, NbCol1>& v1,
                                     const DenseMatrix<Scalar, NbCol2>& v2) {
  return v1.cwise_dot(v2);
}

template <CPX_Scalar Scalar, int NbCol1, int NbCol2>
inline DenseMatrix<Scalar, NbCol1>
dot_block(const DenseMatrix<Scalar, NbCol1>& v1,
          const DenseMatrix<Scalar, NbCol2>& v2) {
  COMPOSYX_DIM_ASSERT(v1.get_n_rows(), v2.get_n_rows(),
                      "Matrix dot_block1 different row numbers");
  return v1.dot_block(v2);
}

template <CPX_Scalar Scalar, int NbCol>
[[nodiscard]] inline DiagonalMatrix<Scalar>
diagonal(const DenseMatrix<Scalar, NbCol>& mat) {
  return mat.diag();
}

template <CPX_Scalar Scalar, int NbCol>
[[nodiscard]] inline Vector<Scalar>
diagonal_as_vector(const DenseMatrix<Scalar, NbCol>& mat) {
  return mat.diag_vect();
}

template <CPX_Scalar Scalar, int NbCol>
[[nodiscard]] inline Scalar* get_ptr(DenseMatrix<Scalar, NbCol>& mat) {
  return mat.get_ptr();
}

template <CPX_Scalar Scalar, int NbCol>
[[nodiscard]] inline const Scalar*
get_ptr(const DenseMatrix<Scalar, NbCol>& mat) {
  return mat.get_ptr();
}

template <CPX_Scalar Scalar, int NbCol>
[[nodiscard]] DenseMatrix<Scalar, NbCol>
transpose(const DenseMatrix<Scalar, NbCol>& mat) {
  return mat.t();
}

template <CPX_Scalar Scalar, int NbCol>
[[nodiscard]] DenseMatrix<Scalar, NbCol>
adjoint(const DenseMatrix<Scalar, NbCol>& mat) {
  return mat.h();
}

template <CPX_Scalar Scalar, int NbCol>
void display(const DenseMatrix<Scalar, NbCol>& v, const std::string& name,
             std::ostream& out) {
  v.display(name, out);
}

template <CPX_Scalar Scalar, int NbCol>
std::ostream& operator<<(std::ostream& out,
                         const DenseMatrix<Scalar, NbCol>& v) {
  v.display("", out);
  return out;
}

template <CPX_Scalar Scalar, int NbCol>
void build_matrix(DenseMatrix<Scalar, NbCol>& mat, const int M, const int N,
                  const int nnz, int* i, int* j, Scalar* v,
                  const bool fill_symmetry = false) {
  mat = DenseMatrix<Scalar, NbCol>(M, N);
  for (int k = 0; k < nnz; ++k) {
    mat(i[k], j[k]) = v[k];
    if (fill_symmetry && (i[k] != j[k])) {
      mat(j[k], i[k]) = v[k];
    }
  }
}

template <CPX_Scalar Scalar, int NbCol>
void build_dense_matrix(DenseMatrix<Scalar, NbCol>& mat, const int M,
                        const int N, Scalar* values) {
  mat = DenseMatrix<Scalar, NbCol>(M, N);
  std::memcpy(get_ptr(mat), values, M * N * sizeof(Scalar));
}

template <CPX_Scalar Scalar, int NbCol>
[[nodiscard]] size_t get_storage_bytes(const DenseMatrix<Scalar, NbCol>& mat,
                                       int) {
  return n_rows(mat) * sizeof(Scalar);
}
// Interface functions:1 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Matrix pseudo-inverse operator][Matrix pseudo-inverse operator:1]]
template <typename Scalar> auto operator~(const DenseMatrix<Scalar>& A) {
  return COMPOSYX_SOLVER_BLAS<DenseMatrix<Scalar>, Vector<Scalar>>(A);
}

template <typename Scalar>
Vector<Scalar> operator%(const DenseMatrix<Scalar>& A,
                         const Vector<Scalar>& b) {
  return ~A * b;
}
// Matrix pseudo-inverse operator:1 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Iterator][Iterator:1]]
template <CPX_Scalar Scalar, int NbCol> struct DenseMatrixIterator {
  size_t i;
  size_t j;
  const DenseMatrix<Scalar, NbCol>& mat;

  using value_type = Scalar;

  DenseMatrixIterator(const DenseMatrix<Scalar, NbCol>& m)
      : i{0}, j{0}, mat{m} {}

  DenseMatrixIterator(const DenseMatrixIterator&) = default;

  DenseMatrixIterator& operator++() {
    ++i;
    if (i == n_rows(mat)) {
      i = 0;
      ++j;
    }
    return *this;
  }

  Scalar operator*() const { return mat(i, j); }

  DenseMatrixIterator begin() { return DenseMatrixIterator(mat); }

  DenseMatrixIterator end() {
    DenseMatrixIterator d(mat);
    d.i = 0;
    d.j = n_cols(mat);
    return d;
  }

  bool operator!=(const DenseMatrixIterator& other) const {
    return (i != other.i) or (j != other.j);
  }

  bool operator==(const DenseMatrixIterator& other) const {
    return !((*this) != other);
  }
};
// Iterator:1 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Footer namespace][Footer namespace:1]]
} // namespace composyx
// Footer namespace:1 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Tlapack interface][Tlapack interface:1]]
#ifdef COMPOSYX_USE_TLAPACK
namespace tlapack {

template <typename Scalar, int NbCol>
composyx::size_t nrows(const composyx::DenseMatrix<Scalar, NbCol>& M) {
  return composyx::n_rows(M);
}

template <typename Scalar, int NbCol>
composyx::size_t ncols(const composyx::DenseMatrix<Scalar, NbCol>& M) {
  return composyx::n_cols(M);
}

template <typename Scalar, int NbCol>
composyx::size_t size(const composyx::DenseMatrix<Scalar, NbCol>& M) {
  return composyx::n_rows(M) * composyx::n_cols(M);
}

template <typename Scalar, int NbCol>
composyx::DenseMatrix<Scalar, NbCol>
slice(const composyx::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> fidx,
      std::pair<int, int> lidx) {
  auto& [fi, li] = fidx;
  auto& [fj, lj] = lidx;
  return M.get_block_view(fi, fj, li - fi, lj - fj);
}

template <typename Scalar, int NbCol>
composyx::DenseMatrix<Scalar, NbCol>
rows(const composyx::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> ridx) {
  auto& [fi, li] = ridx;
  return M.get_block_view(fi, 0, li - fi,
                          static_cast<int>(composyx::n_cols(M)));
}

template <typename Scalar, int NbCol>
composyx::DenseMatrix<Scalar, NbCol>
cols(const composyx::DenseMatrix<Scalar, NbCol>& M, std::pair<int, int> cidx) {
  auto& [fj, lj] = cidx;
  return M.get_block_view(0, fj, static_cast<int>(composyx::n_rows(M)),
                          lj - fj);
}

template <typename Scalar, int NbCol>
composyx::Vector<Scalar> row(const composyx::DenseMatrix<Scalar, NbCol>& M,
                             int i) {
  return M.get_row_view(i, 0, composyx::n_cols(M));
}

template <typename Scalar, int NbCol>
composyx::Vector<Scalar> col(const composyx::DenseMatrix<Scalar, NbCol>& M,
                             int j) {
  return M.get_vect_view(j);
}

template <typename Scalar, int NbCol>
composyx::Vector<Scalar> slice(const composyx::DenseMatrix<Scalar, NbCol>& M,
                               int i, std::pair<int, int> lidx) {
  auto& [fj, lj] = lidx;
  COMPOSYX_ASSERT(fj <= lj,
                  "composyx::DenseMatrix::slice with first index > last index");
  return M.get_row_view(i, fj, static_cast<size_t>(lj - fj));
}

template <typename Scalar, int NbCol>
composyx::Vector<Scalar> slice(const composyx::DenseMatrix<Scalar, NbCol>& M,
                               std::pair<int, int> lidx, int j) {
  auto& [fi, li] = lidx;
  COMPOSYX_ASSERT(fi <= li,
                  "composyx::DenseMatrix::slice with first index > last index");
  return M.get_col_view(fi, j, static_cast<size_t>(li - fi));
}

template <typename Scalar, int NbCol>
composyx::Vector<Scalar> diag(const composyx::DenseMatrix<Scalar, NbCol>& M) {
  return M.get_diag_view();
}

template <typename Scalar, int NbCol>
composyx::Vector<Scalar> diag(const composyx::DenseMatrix<Scalar, NbCol>& M,
                              int i) {
  return M.get_diag_view(i);
}

template <typename Scalar>
composyx::Vector<Scalar> slice(const composyx::DenseMatrix<Scalar, 1>& M,
                               std::pair<int, int> idxs) {
  auto& [fi, li] = idxs;
  return M.get_block_view(fi, 0, li - fi, 1);
}
} // namespace tlapack
#endif
// Tlapack interface:1 ends here

// [[file:../../../org/composyx/loc_data/DenseMatrix.org::*Operators with Diagonal Matrix][Operators with Diagonal Matrix:1]]
namespace composyx { // Vector <- Diagonal * Vector
template <class Scalar>
inline Vector<Scalar> operator*(const DiagonalMatrix<Scalar>& diag,
                                Vector<Scalar> vect) {
  vect.diagmv(diag);
  return vect;
}

// DenseMatrix <- Diagonal * DenseMatrix
template <class Scalar>
inline DenseMatrix<Scalar> operator*(const DiagonalMatrix<Scalar>& diag,
                                     DenseMatrix<Scalar> mat) {
  mat.left_diagmm(diag);
  return mat;
}

// DenseMatrix <- DenseMatrix * Diagonal
template <class Scalar>
inline DenseMatrix<Scalar> operator*(DenseMatrix<Scalar> mat,
                                     const DiagonalMatrix<Scalar>& diag) {
  mat.right_diagmm(diag);
  return mat;
}

// DenseMatrix <- Diagonal + DenseMatrix
template <class Scalar>
inline DenseMatrix<Scalar> operator+(DenseMatrix<Scalar> mat,
                                     const DiagonalMatrix<Scalar>& diag) {
  mat.add(diag);
  return mat;
}

// DenseMatrix <- DenseMatrix + Diagonal
template <class Scalar>
inline DenseMatrix<Scalar> operator+(const DiagonalMatrix<Scalar>& diag,
                                     DenseMatrix<Scalar> mat) {
  mat.add(diag);
  return mat;
}

// DenseMatrix <- DenseMatrix - Diagonal
template <class Scalar>
inline DenseMatrix<Scalar> operator-(DenseMatrix<Scalar> mat,
                                     const DiagonalMatrix<Scalar>& diag) {
  mat.add(diag, Scalar{-1.0});
  return mat;
}

// DenseMatrix <- Diagonal - DenseMatrix
template <class Scalar>
inline DenseMatrix<Scalar> operator-(const DiagonalMatrix<Scalar>& diag,
                                     DenseMatrix<Scalar> mat) {
  mat *= Scalar{-1};
  mat.add(diag);
  return mat;
}
} // namespace composyx
// Operators with Diagonal Matrix:1 ends here
