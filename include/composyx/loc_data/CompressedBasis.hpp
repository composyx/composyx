// [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Header][Header:1]]
#pragma once

#include "composyx/loc_data/DenseMatrix.hpp"
#include "composyx/loc_data/DiagonalMatrix.hpp"

#ifdef COMPOSYX_USE_SZ_COMPRESSOR
#include <composyx/utils/SZ_compressor.hpp>
#endif
#ifdef COMPOSYX_USE_ZFP_COMPRESSOR
#include <composyx/utils/ZFP_compressor.hpp>
#endif

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Attributes][Attributes:1]]
template <typename Compressor> class CompressedBasis {
private:
  using Scalar = typename Compressor::value_type;
  using ComprParam = typename Compressor::compression_param_type;

public:
  using local_operator = Compressor;
  using local_type = Compressor;
  using matrix_type = Compressor;
  using vector_type = DenseMatrix<Scalar, 1>;
  using scalar_type = Scalar;
  constexpr static const int compression_mode = Compressor::compression_mode;
  using compression_param_type = ComprParam;

private:
  using CprVect = std::vector<Compressor>;
  std::shared_ptr<CprVect> _vectors;
  size_t _n;

  ComprParam _param_compr;

  // When taking a view to the data, first element may be shifted
  size_t _first_elt = 0;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Constructors][Constructors:1]]
public:
  explicit CompressedBasis() : _n{0}, _param_compr{0} {}

  template <int NbCol>
  explicit CompressedBasis(const DenseMatrix<Scalar, NbCol>& mat,
                           ComprParam param_compr)
      : _n{n_cols(mat)}, _param_compr{param_compr} {
    _vectors = std::make_shared<CprVect>(n_cols(mat));
    for (size_t j = 0; j < _n; ++j) {
      Scalar* mat_ptr = const_cast<Scalar*>(mat.get_vect_ptr(j));
      _vectors->at(j) = _compress(mat_ptr, n_rows(mat));
    }
    _first_elt = 0;
  }
  template <int NbCol, typename T>
  explicit CompressedBasis(const DenseMatrix<Scalar, NbCol>&, T) = delete;

  explicit CompressedBasis(size_t n, ComprParam param_compr)
      : _n{n}, _param_compr{param_compr} {
    _vectors = std::make_shared<CprVect>(n);
    _first_elt = 0;
  }

  // Avoid implicit conversion (double/int)
  template <typename T> explicit CompressedBasis(size_t, T) = delete;

  template <CPX_Integral Tint>
  explicit CompressedBasis(const CompressedBasis& other, Tint j_beg,
                           Tint j_end) {
    COMPOSYX_ASSERT(j_end >= j_beg,
                    "CompressedBasis: index error makeing view");
    _n = j_end - j_beg;
    _first_elt = other._first_elt + j_beg;
    _vectors = other._vectors;
    _param_compr = other._param_compr;
  }
  // Constructors:1 ends here

  // [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Compression of a vector][Compression of a vector:1]]
private:
  constexpr inline Compressor _compress(Scalar* ptr, size_t size) {
    Compressor cpr;

    auto dataspan = std::span<Scalar>(ptr, ptr + size);
    cpr.compress(dataspan, _param_compr);

    return cpr;
  }
  // Compression of a vector:1 ends here

  // [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Access compressed vectors][Access compressed vectors:1]]
private:
  template <CPX_Integral Tint> Compressor& _cpr_vector(Tint j) {
    std::vector<Compressor>& vects = *(_vectors.get());
    return vects.at(_first_elt + j);
  }

  template <CPX_Integral Tint> const Compressor& _cpr_vector(Tint j) const {
    const std::vector<Compressor>& vects = *(_vectors.get());
    return vects.at(_first_elt + j);
  }

public:
  // Access compressed vectors:1 ends here

  // [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Decompress vectors][Decompress vectors:1]]
  template <CPX_Integral Tint>
  [[nodiscard]] Vector<Scalar> get_vect(Tint j = 0) const {
    const Compressor& compressed_V = _cpr_vector(j);
    Vector<Scalar> v_out(compressed_V.get_n_elts());
    compressed_V.decompress(get_ptr(v_out));
    return v_out;
  }

  template <CPX_Integral Tint>
  [[nodiscard]] Vector<Scalar> get_vect_view(Tint j = 0) const {
    return get_vect(j);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] friend Vector<Scalar> get_vect_view(const CompressedBasis& b,
                                                    Tint j = 0) {
    return b.get_vect_view(j);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] DenseMatrix<Scalar, -1> get_block_copy(Tint i, Tint j, size_t m,
                                                       size_t n) const {
    DenseMatrix<Scalar> m_out(m, n);
    for (Tint k = j; k < j + n; ++k) {
      Compressor& cpr_v = _cpr_vector(k);
      if (cpr_v.get_n_elts() == m) {
        cpr_v.decompress(m_out.get_vect_ptr(j));
      } else {
        std::vector<Scalar> dcp_v = cpr_v.decompress();
        std::memcpy(m_out.get_ptr(i, j), &dcp_v[0], m);
      }
    }

    return m_out;
  }

  template <CPX_Integral Tint>
  [[nodiscard]] DenseMatrix<Scalar, -1> get_block_view(Tint i, Tint j, size_t m,
                                                       size_t n) const {
    return get_block_copy(i, j, m, n);
  }
  // Decompress vectors:1 ends here

  // [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Compress vector][Compress vector:1]]
  template <CPX_Integral Tint>
  void set_vector(const Vector<Scalar>& v, Tint j) {
    _cpr_vector(j) =
        Compressor(const_cast<Scalar*>(get_ptr(v)), n_rows(v), _param_compr);
  }

  template <CPX_Integral Tint> void set_vector(Vector<Scalar>&& v, Tint j) {
    _cpr_vector(j) =
        Compressor(const_cast<Scalar*>(get_ptr(v)), n_rows(v), _param_compr);
    v = Vector<Scalar>();
  }
  // Compress vector:1 ends here

  // [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Get a view subset of consecutive vectors][Get a view subset of consecutive vectors:1]]
  template <CPX_Integral Tint>
  [[nodiscard]] CompressedBasis get_columns_view(Tint j_beg, Tint j_end) {
    return CompressedBasis(*this, j_beg, j_end);
  }

  template <CPX_Integral Tint>
  [[nodiscard]] friend CompressedBasis
  get_columns_view(CompressedBasis& b, Tint j_beg, Tint j_end) {
    return b.get_columns_view(j_beg, j_end);
  }
  // Get a view subset of consecutive vectors:1 ends here

  // [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Matrix vector product][Matrix vector product:1]]
  [[nodiscard]] Vector<Scalar> mat_vect(const Vector<Scalar> v_in) const {
    if (_n == 0)
      return Vector<Scalar>();

    size_t M = n_rows(get_vect(0));
    Vector<Scalar> v_out(M);

    for (size_t j = 0; j < _n; ++j) {
      Vector<Scalar> vj = get_vect(j);
      for (size_t i = 0; i < M; ++i) {
        v_out[i] += vj[i] * v_in[j];
      }
    }

    return v_out;
  }
  // Matrix vector product:1 ends here

  // [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Getters][Getters:1]]
  template <CPX_Integral Tint>
  [[nodiscard]] size_t get_storage_bytes(Tint j) const {
    return _cpr_vector(j).get_compressed_bytes();
  }

  [[nodiscard]] size_t get_n_rows() const {
    if (_n == 0)
      return 0;
    return _cpr_vector(0).get_n_elts();
  }
  [[nodiscard]] size_t get_n_cols() const { return _n; }
  // Getters:1 ends here

  // [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Getters][Getters:2]]
}; // class CompressedBasis
// Getters:2 ends here

// [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Getters][Getters:3]]
template <typename Compressor>
[[nodiscard]] size_t n_rows(const CompressedBasis<Compressor>& basis) {
  return basis.get_n_rows();
}
template <typename Compressor>
[[nodiscard]] size_t n_cols(const CompressedBasis<Compressor>& basis) {
  return basis.get_n_cols();
}

template <typename Compressor>
[[nodiscard]] size_t get_storage_bytes(const CompressedBasis<Compressor>& basis,
                                       int j) {
  return basis.get_storage_bytes(j);
}
// Getters:3 ends here

// [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Operators][Operators:1]]
template <CPX_Scalar Scalar, typename Compressor>
Vector<Scalar> operator*(const CompressedBasis<Compressor>& basis,
                         const Vector<Scalar>& RHS) {
  return basis.mat_vect(RHS);
}
// Operators:1 ends here

// [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Traits][Traits:1]]
template <typename Compressor>
struct vector_type<CompressedBasis<Compressor>> : public std::true_type {
  using type = typename CompressedBasis<Compressor>::vector_type;
};

template <typename Compressor>
struct sparse_type<CompressedBasis<Compressor>> : public std::true_type {
  using type = SparseMatrixCSC<typename Compressor::value_type>;
};

template <typename Compressor>
struct dense_type<CompressedBasis<Compressor>> : public std::true_type {
  using type = DenseMatrix<typename Compressor::value_type>;
};

template <typename Compressor>
struct diag_type<CompressedBasis<Compressor>> : public std::true_type {
  using type = DiagonalMatrix<typename Compressor::value_type>;
};

template <typename Compressor>
struct scalar_type<CompressedBasis<Compressor>> : public std::true_type {
  using type = typename Compressor::value_type;
};
// Traits:1 ends here

// [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
