// [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Header and namespace][Header and namespace:1]]
#pragma once

#include <memory>
#include <cassert>
#include <limits>
#include <iostream>
#include <iomanip>
#include <functional>
#include <numeric>
#include <string>

namespace composyx {
// Header and namespace:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Exception when trying to access zero value][Exception when trying to access zero value:1]]
struct ComposyxAccessZeroValue : public std::runtime_error {
  ComposyxAccessZeroValue(const std::string& s) : std::runtime_error(s) {}
};
// Exception when trying to access zero value:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Concept for functions of type "foreach_ijv"][Concept for functions of type "foreach_ijv":1]]
template <typename Func, typename Index, typename Scalar>
concept CPX_FuncIJV = CPX_Integral<Index> && CPX_Scalar<Scalar> &&
                      requires(Func f, Index i, Index j, Scalar v) {
                        { f(i, j, v) };
                      };
// Concept for functions of type "foreach_ijv":1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Forward delcaration][Forward delcaration:1]]
template <typename, typename> class SparseMatrixBase;
// Forward delcaration:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Concept for composyx sparse matrices][Concept for composyx sparse matrices:1]]
template <typename matrix, typename Scalar = matrix::scalar_type,
          typename Index = matrix::index_type>
concept CPX_IJV_SparseMatrix =
    std::derived_from<matrix, SparseMatrixBase<Scalar, Index>>;
// Concept for composyx sparse matrices:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Cast][Cast:1]]
//mattype = SparseMatrixCOO, SparseMatrixCSR, SparseMatrixCSC
#define COMPOSYX_SPARSE_MATRIX_CAST_FUNCTION(mattype)                          \
  template <CPX_Scalar OtherScalar>                                            \
  [[nodiscard]] mattype<OtherScalar, Index> cast() const {                     \
    std::vector<OtherScalar> vcast(_v.size());                                 \
    for (size_t k = 0; k < _v.size(); ++k)                                     \
      vcast[k] = static_cast<OtherScalar>(_v[k]);                              \
    mattype<OtherScalar, Index> out(_m, _n, _nnz, _i, _j, std::move(vcast));   \
    out.copy_properties(*this);                                                \
    return out;                                                                \
  }
// Cast:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Header][Header:1]]
} // namespace composyx

#include "composyx/utils/Arithmetic.hpp"
#include "composyx/IO/MatrixMarketLoader.hpp"
#include "composyx/utils/MatrixProperties.hpp"
#include "composyx/utils/Error.hpp"
#include "composyx/utils/ArrayAlgo.hpp"
#include "composyx/solver/LinearOperator.hpp"
#include "composyx/interfaces/implicit_sparse_solver.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Attributes][Attributes:1]]
template <typename Scalar, typename Index>
class SparseMatrixBase : public MatrixProperties<Scalar> {

public:
  using scalar_type = Scalar;
  using value_type = Scalar;
  using real_type = typename arithmetic_real<Scalar>::type;
  using index_type = Index;

protected:
  using Real = real_type;
  using Idx_arr = std::vector<Index>;
  using Scal_arr = std::vector<Scalar>;

  size_t _m = 0;
  size_t _n = 0;
  size_t _nnz = 0;

  Idx_arr _i;
  Idx_arr _j;
  Scal_arr _v;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Constructors][Constructors:1]]
public:
  SparseMatrixBase() {}

  SparseMatrixBase(const size_t m, const size_t n, const size_t nnz,
                   const Idx_arr& i, const Idx_arr& j, const Scal_arr& v)
      : _m{m}, _n{n}, _nnz{nnz}, _i{i}, _j{j}, _v{v} {}

  SparseMatrixBase(const size_t m, const size_t n, const size_t nnz,
                   Idx_arr&& i, Idx_arr&& j, Scal_arr&& v)
      : _m{m}, _n{n}, _nnz{nnz}, _i{std::move(i)}, _j{std::move(j)},
        _v{std::move(v)} {}

  // Generic constructor
  template <CPX_IntArray I, CPX_Arrayof<Scalar> A>
  SparseMatrixBase(const size_t m, const size_t n, const size_t nnz, const I& i,
                   const I& j, const A& v)
      : _m{m}, _n{n}, _nnz{nnz} {
    _i = Idx_arr(i.size());
    _j = Idx_arr(j.size());
    _v = Scal_arr(v.size());
    // this->check_dimensions();
    for (size_t k = 0; k < i.size(); ++k) {
      _i[k] = i[k];
    }
    for (size_t k = 0; k < j.size(); ++k) {
      _j[k] = j[k];
    }
    for (size_t k = 0; k < v.size(); ++k) {
      _v[k] = v[k];
    }
  }

  // Copy constructor
  SparseMatrixBase(const SparseMatrixBase& spmat) = default;

  // Move constructor
  SparseMatrixBase(SparseMatrixBase&& spmat)
      : _m{std::exchange(spmat._m, 0)}, _n{std::exchange(spmat._n, 0)},
        _nnz{std::exchange(spmat._nnz, 0)}, _i{std::move(spmat._i)},
        _j{std::move(spmat._j)}, _v{std::move(spmat._v)} {
    this->copy_properties(spmat);
    spmat.set_default_properties();
  }

  // Copy
  SparseMatrixBase& operator=(const SparseMatrixBase& copy) {
    _m = copy._m;
    _n = copy._n;
    _nnz = copy._nnz;

    _i = copy._i;
    _j = copy._j;
    _v = copy._v;
    this->copy_properties(copy);
    return *this;
  }

  // Move assignment operator
  SparseMatrixBase& operator=(SparseMatrixBase&& copy) {
    copy.swap(*this);
    return *this;
  }
  // Constructors:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Check dimensions][Check dimensions:1]]
  virtual void check_dimensions() = 0;
  // Check dimensions:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Swap functions][Swap functions:1]]
  void swap(SparseMatrixBase& other) {
    std::swap(_m, other._m);
    std::swap(_n, other._n);
    std::swap(_nnz, other._nnz);
    std::swap(_i, other._i);
    std::swap(_j, other._j);
    std::swap(_v, other._v);
    this->MatrixProperties<Scalar>::swap(other);
  }

  friend void swap(SparseMatrixBase& m1, SparseMatrixBase& m2) { m1.swap(m2); }
  // Swap functions:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Diagonal extraction][Diagonal extraction:1]]
  virtual DiagonalMatrix<Scalar> diag() const = 0;
  virtual Vector<Scalar> diag_vect() const = 0;
  // Diagonal extraction:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Break symmetry][Break symmetry:1]]
  virtual void fill_half_to_full_storage() = 0;

  void break_symmetry() {
    this->fill_half_to_full_storage();
    this->set_property(MatrixSymmetry::general);
  }
  // Break symmetry:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Frobenius norm][Frobenius norm:1]]
  [[nodiscard]] Real frobenius_norm() const {
    auto square_v = [](const Real& sum, const Scalar& scal) {
      return sum + std::abs(scal * scal);
    };
    return std::sqrt(std::accumulate(_v.begin(), _v.end(), Real{0}, square_v));
  }

  [[nodiscard]] Real norm() const { return frobenius_norm(); }
  // Frobenius norm:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Getters][Getters:1]]
  // Getters
  [[nodiscard]] size_t get_n_rows() const { return _m; }
  [[nodiscard]] size_t get_n_cols() const { return _n; }
  [[nodiscard]] size_t get_nnz() const { return _nnz; }

  [[nodiscard]] const Index* get_i_ptr() const { return _i.data(); }
  [[nodiscard]] const Index* get_j_ptr() const { return _j.data(); }
  [[nodiscard]] const Scalar* get_v_ptr() const { return _v.data(); }

  [[nodiscard]] Index* get_i_ptr() { return _i.data(); }
  [[nodiscard]] Index* get_j_ptr() { return _j.data(); }
  [[nodiscard]] Scalar* get_v_ptr() { return _v.data(); }
  // Getters:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Operators][Operators:1]]
  // Scalar multiplication
  SparseMatrixBase& operator*=(const Scalar& scal) {
    std::ranges::for_each(_v, [&scal](Scalar& s) { s *= scal; });
    return *this;
  }

  SparseMatrixBase& operator/=(const Scalar& scal) {
    COMPOSYX_ASSERT(scal != Scalar{0}, "SparseMatrix:: division by 0 !");
    const auto inv = Scalar{1.0} / scal;
    std::ranges::for_each(_v, [&inv](Scalar& s) { s *= inv; });
    return *this;
  }
  // Operators:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Display function][Display function:1]]
  virtual void display(const std::string& name = "",
                       std::ostream& out = std::cout) const = 0;
}; // class SparseMatrixBase
// Display function:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Interface functions][Interface functions:1]]
template <CPX_Scalar Scalar, CPX_Integral Index>
[[nodiscard]] size_t n_rows(const SparseMatrixBase<Scalar, Index>& mat) {
  return mat.get_n_rows();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
[[nodiscard]] size_t n_cols(const SparseMatrixBase<Scalar, Index>& mat) {
  return mat.get_n_cols();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
[[nodiscard]] size_t n_nonzero(const SparseMatrixBase<Scalar, Index>& mat) {
  return mat.get_nnz();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
[[nodiscard]] [[nodiscard]] DiagonalMatrix<Scalar>
diagonal(const SparseMatrixBase<Scalar, Index>& mat) {
  return mat.diag();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
[[nodiscard]] [[nodiscard]] Vector<Scalar>
diagonal_as_vector(const SparseMatrixBase<Scalar, Index>& mat) {
  return mat.diag_vect();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
[[nodiscard]] const Index*
get_i_ptr(const SparseMatrixBase<Scalar, Index>& mat) {
  return mat.get_i_ptr();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
[[nodiscard]] const Index*
get_j_ptr(const SparseMatrixBase<Scalar, Index>& mat) {
  return mat.get_j_ptr();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
[[nodiscard]] const Scalar*
get_v_ptr(const SparseMatrixBase<Scalar, Index>& mat) {
  return mat.get_v_ptr();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
[[nodiscard]] Index* get_i_ptr(SparseMatrixBase<Scalar, Index>& mat) {
  return mat.get_i_ptr();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
[[nodiscard]] Index* get_j_ptr(SparseMatrixBase<Scalar, Index>& mat) {
  return mat.get_j_ptr();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
[[nodiscard]] Scalar* get_v_ptr(SparseMatrixBase<Scalar, Index>& mat) {
  return mat.get_v_ptr();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
void display(const SparseMatrixBase<Scalar, Index>& v,
             const std::string& name = "", std::ostream& out = std::cout) {
  v.display(name, out);
}
// Interface functions:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Common SparseMatrix functions][Common SparseMatrix functions:1]]
template <CPX_IJV_SparseMatrix Matrix>
auto sparse_matrix_diag(const Matrix& mat) {
  using Scalar = Matrix::scalar_type;
  using Index = Matrix::index_type;

  DiagonalMatrix<Scalar> diag(n_rows(mat), n_cols(mat));
  const size_t size = std::min(n_rows(mat), n_cols(mat));

  mat.foreach_ijv_max_i(
      [&](Index i, Index j, Scalar v) {
        if (i == j)
          diag(i) = v;
      },
      size);

  return diag;
}

template <CPX_IJV_SparseMatrix Matrix>
auto sparse_matrix_diag_vect(const Matrix& mat) {
  using Scalar = Matrix::scalar_type;
  using Index = Matrix::index_type;

  const size_t size = std::min(n_rows(mat), n_cols(mat));
  Vector<Scalar> diag(size);

  mat.foreach_ijv_max_i(
      [&](Index i, Index j, Scalar v) {
        if (i == j)
          diag(i) = v;
      },
      size);

  return diag;
}

template <CPX_IJV_SparseMatrix Matrix>
auto sparse_matrix_to_dense(const Matrix& mat) {
  using Scalar = Matrix::scalar_type;
  using Index = Matrix::index_type;

  DenseMatrix<Scalar> dense(n_rows(mat), n_cols(mat));
  mat.foreach_ijv([&](Index i, Index j, Scalar v) { dense(i, j) = v; });
  dense.copy_properties(mat);

  return dense;
}

} // namespace composyx
#include "composyx/loc_data/SparseMatrixLIL.hpp"
namespace composyx {

template <CPX_IJV_SparseMatrix Matrix>
void sparse_matrix_fill_half_to_full_storage(Matrix& mat) {
  if (mat.is_storage_full()) {
    return;
  }
  if (mat.is_general()) {
    return;
  }

  using Scalar = Matrix::scalar_type;
  using Index = Matrix::index_type;

  SparseMatrixLIL<Scalar, Index> lilmat(n_rows(mat), n_rows(mat));
  lilmat.copy_properties(mat);
  lilmat.insert(mat);
  lilmat.fill_half_to_full_storage();
  mat.from_lil(lilmat);
}

template <CPX_IJV_SparseMatrix Matrix>
void sparse_matrix_to_storage_half(Matrix& mat, MatrixStorage storage) {
  COMPOSYX_ASSERT(
      mat.is_symmetric() || mat.is_hermitian(),
      "SparseMatrix: asking for half storage with non symmetric matrix");
  if (mat.get_storage_type() == storage) {
    return;
  }
  if ((mat.get_storage_type() == MatrixStorage::lower &&
       storage == MatrixStorage::upper) ||
      (mat.get_storage_type() == MatrixStorage::upper &&
       storage == MatrixStorage::lower)) {
    auto sym = mat.get_symmetry();
    mat.set_property(MatrixStorage::full, MatrixSymmetry::general);
    mat.transpose();
    mat.set_property(sym, storage);
    return;
  }

  using Scalar = Matrix::scalar_type;
  using Index = Matrix::index_type;

  SparseMatrixLIL<Scalar, Index> lilmat(mat.get_n_rows(), mat.get_n_cols());

  foreach_ijv([&](Index i, Index j, Scalar v) {
    if (storage == MatrixStorage::lower && i >= j)
      lilmat.insert(j, i, v);
    else if (storage == MatrixStorage::upper && i <= j)
      lilmat.insert(j, i, v);
  });

  mat.set_property(storage);
  mat.from_lil(lilmat);
}

template <CPX_IJV_SparseMatrix Matrix1, CPX_IJV_SparseMatrix Matrix2>
void sparse_matrix_spmm(Matrix1& lhs, const Matrix2& rhs) {
  COMPOSYX_DIM_ASSERT(n_cols(lhs), n_rows(rhs), "SparseMatrix x SparseMatrix");

  using Scalar = Matrix1::scalar_type;
  using Index = Matrix1::index_type;

  SparseMatrixLIL<Scalar, Index> lilmat(n_rows(lhs), n_cols(rhs));

  auto insert_nnz = [&lilmat](Index i1, Index j1, Index i2, Index j2, Scalar v1,
                              Scalar v2) {
    if (j1 == i2) {
      lilmat.insert(i1, j2, v1 * v2);
    }
  };

  const bool tri_storage_1 = !(lhs.is_storage_full());
  const bool tri_storage_2 = !(rhs.is_storage_full());

  lhs.foreach_ijv([&](Index i1, Index j1, Scalar v1) {
    rhs.foreach_ijv([&](Index i2, Index j2, Scalar v2) {
      // A(i1 j1) x B(i1 j2)
      insert_nnz(i1, j1, i2, j2, v1, v2);

      if (tri_storage_1 && (i1 != j1)) {
        // A(j1 i1) x B(i1 j2)
        insert_nnz(j1, i1, i2, j2, v1, v2);

        if (tri_storage_2 && (j2 != i2)) {
          // A(j1 i1) x B(j2 i2)
          insert_nnz(j1, i1, j2, i2, v1, v2);
        }
      }

      if (tri_storage_2 && (j2 != i2)) {
        // A(i1 j1) x B(j2 i2)
        insert_nnz(i1, j1, j2, i2, v1, v2);
      }
    });
  });

  lilmat.drop();
  lhs.from_lil(lilmat);
}

// A <- A * D
template <CPX_IJV_SparseMatrix Matrix, CPX_Scalar Scalar = Matrix::scalar_type>
void sparse_matrix_right_diagmm(Matrix& mat,
                                const DiagonalMatrix<Scalar>& other) {
  using Index = Matrix::index_type;

  size_t M = n_rows(mat);
  size_t N = n_cols(other);
  COMPOSYX_DIM_ASSERT(n_cols(mat), n_rows(other),
                      "SparseMatrix * DiagonalMatrix");

  mat.break_symmetry();
  SparseMatrixLIL<Scalar, Index> lilmat(M, N);
  lilmat.copy_properties(mat);
  const Scalar* diag = other.get_ptr();

  // Multiplication by diagonal on the right is scaling the columns
  const Index dim = static_cast<Index>(std::min(N, n_cols(mat)));

  mat.foreach_ijv_max_j(
      [&](Index i, Index j, Scalar v) { lilmat.insert(i, j, v * diag[j]); },
      dim);

  lilmat.drop();
  mat.from_lil(lilmat);
}

// A <- D * A
template <CPX_IJV_SparseMatrix Matrix, CPX_Scalar Scalar = Matrix::scalar_type>
void sparse_matrix_left_diagmm(Matrix& mat,
                               const DiagonalMatrix<Scalar>& other) {
  using Index = Matrix::index_type;

  size_t M = n_rows(other);
  size_t N = n_cols(mat);
  COMPOSYX_DIM_ASSERT(n_rows(mat), n_cols(other),
                      "DiagonalMatrix * SparseMatrix");

  mat.break_symmetry();
  SparseMatrixLIL<Scalar> lilmat(M, N);
  lilmat.copy_properties(mat);
  const Scalar* diag = other.get_ptr();

  // Multiplication by diagonal on the right is scaling the rows
  const Index dim = static_cast<Index>(std::min(M, n_rows(mat)));

  mat.foreach_ijv_max_i(
      [&](Index i, Index j, Scalar v) { lilmat.insert(i, j, v * diag[i]); },
      dim);

  lilmat.drop();
  mat.from_lil(lilmat);
}

// A = A + alpha B
template <CPX_IJV_SparseMatrix Matrix1, CPX_IJV_SparseMatrix Matrix2,
          typename Scalar = Matrix1::scalar_type>
void sparse_matrix_addition(Matrix1& lhs, const Matrix2& rhs,
                            Scalar alpha = Scalar{1}) {
  COMPOSYX_DIM_ASSERT(n_rows(lhs), n_rows(rhs),
                      "SparseMatrix + SparseMatrix: different nb rows.");
  COMPOSYX_DIM_ASSERT(n_cols(lhs), n_cols(rhs),
                      "SparseMatrix + SparseMatrix: different nb cols.");
  SparseMatrixLIL<Scalar> lilmat(n_rows(lhs), n_cols(lhs));

  // If storage types are different, sum in full storage
  if (lhs.get_storage_type() != rhs.get_storage_type()) {
    lilmat.insert(rhs);
    lilmat.copy_properties(rhs);
    lilmat.fill_half_to_full_storage();
    if (alpha != Scalar{1}) {
      lilmat *= alpha;
    }
    lhs.fill_half_to_full_storage();
    lilmat.insert(lhs);
  } else {
    lilmat.copy_properties(lhs);
    lilmat.insert(rhs);
    if (alpha != Scalar{1}) {
      lilmat *= alpha;
    }
    lilmat.insert(lhs);
  }

  lilmat.drop();
  lhs.from_lil(lilmat);
}

// A <- A + alpha D
template <CPX_IJV_SparseMatrix Matrix, CPX_Scalar Scalar = Matrix::scalar_type>
void sparse_matrix_diagonal_addition(Matrix& mat,
                                     const DiagonalMatrix<Scalar>& other,
                                     Scalar alpha = Scalar{1}) {
  COMPOSYX_ASSERT(n_rows(mat) == n_rows(other),
                  "SparseMatrix + DiagonalMatrix: different nb rows.");
  COMPOSYX_ASSERT(n_cols(mat) == n_cols(other),
                  "SparseMatrix + DiagonalMatrix: different nb cols.");
  SparseMatrixLIL<Scalar> lilmat(n_rows(mat), n_cols(mat));
  lilmat.copy_properties(mat);
  lilmat.insert(other);
  if (alpha != Scalar{1}) {
    lilmat *= alpha;
  }
  lilmat.insert(mat);
  lilmat.drop(); // Drop zeros
  mat.from_lil(lilmat);
}
// Common SparseMatrix functions:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixBase.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
