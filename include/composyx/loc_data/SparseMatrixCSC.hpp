// [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Header][Header:1]]
#pragma once

namespace composyx {
template <CPX_Scalar, CPX_Integral = int> class SparseMatrixCSC;

template <CPX_Scalar, CPX_Integral, Op> struct OpSpMatCSC;

template <Op op, CPX_Scalar Scalar, CPX_Integral Index>
inline void apply_op(SparseMatrixCSC<Scalar, Index>& mat) {
  if constexpr (op == Op::NoTrans) {
    return;
  } else if constexpr (op == Op::Trans) {
    mat.transpose();
  } else if constexpr (op == Op::ConjTrans) {
    mat.conj_transpose();
  }
}

template <CPX_Scalar Scalar, CPX_Integral Index>
SparseMatrixCSC<Scalar, Index>
CSC_from_CSR(std::span<const Index> csri, std::span<const Index> csrj,
             std::span<const Scalar> csrv, size_t csrm, size_t csrn);

template <CPX_Scalar Scalar, CPX_Integral Index>
SparseMatrixCSC<Scalar, Index>
CSC_from_CSR(std::initializer_list<Index> i, std::initializer_list<Index> j,
             std::initializer_list<Scalar> v, const size_t m, const size_t n);
} // namespace composyx

#include "composyx/loc_data/SparseMatrixCOO.hpp"
#include "composyx/loc_data/SparseMatrixLIL.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Attributes][Attributes:1]]
template <CPX_Scalar Scalar, CPX_Integral Index>
class SparseMatrixCSC : public SparseMatrixBase<Scalar, Index> {

public:
  using local_type = SparseMatrixCSC<Scalar, Index>;

private:
  using BaseT = SparseMatrixBase<Scalar, Index>;

  using Real = typename BaseT::Real;
  using Idx_arr = typename BaseT::Idx_arr;
  using Scal_arr = typename BaseT::Scal_arr;

  using BaseT::_m;
  using BaseT::_n;
  using BaseT::_nnz;

  using BaseT::_i;
  using BaseT::_j;
  using BaseT::_v;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Check consistency for attributes][Check consistency for attributes:1]]
  // Check that values in i < M, in j <= NNZ
  void check_values_range() {
    COMPOSYX_ASSERT(elts_lower_than(_i, static_cast<Index>(_m)),
                    "CSC: value in i >= m");
    COMPOSYX_ASSERT(elts_lower_than(_j, static_cast<Index>(_nnz + 1)),
                    "CSC: value in j > nnz");
    //check_indices_order();
  }

  // Check order of I and J arrays
  void check_indices_order() {

    if (_n == 0)
      return;
    COMPOSYX_ASSERT(_j[0] == 0, "CSC: j[0] must be 0");

    int j_prev = 0;
    for (Index j = 0; j < static_cast<Index>(_n); ++j) {

      COMPOSYX_ASSERT(_j[j] >= j_prev, "CSC: Error in j array order");

      int i_prev = -1;
      for (Index k = _j[j]; k < _j[j + 1]; ++k) {
        COMPOSYX_ASSERT(static_cast<int>(_i[k]) > i_prev,
                        "CSC: Error in I array order");
        i_prev = static_cast<int>(_i[k]);
      }
    }

    COMPOSYX_ASSERT(_j[_n] == static_cast<Index>(_nnz),
                    "CSC: j last value must be nnz");
  }
  // Check consistency for attributes:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Check dimensions][Check dimensions:1]]
  void check_dimensions() {
    COMPOSYX_DIM_ASSERT(
        _i.size(), _nnz,
        "SparseMatrixCSC:: size(i) should be the number of non-zero elements");
    COMPOSYX_DIM_ASSERT(_j.size(), _n + 1,
                        "SparseMatrixCSC:: size(j) should be N + 1");
    COMPOSYX_DIM_ASSERT(
        _v.size(), _nnz,
        "SparseMatrixCSC:: size(v) should be the number of non-zero elements");
  }
  // Check dimensions:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Constructors][Constructors:1]]
public:
  explicit SparseMatrixCSC() : BaseT() {}
  explicit SparseMatrixCSC(const size_t m, const size_t n) : BaseT() {
    _m = m;
    _n = n;
  }

  explicit SparseMatrixCSC(const size_t m, const size_t n, const size_t nnz,
                           const Index* i, const Index* j, const Scalar* v)
      : BaseT{m,
              n,
              nnz,
              std::span(i, nnz),
              std::span(j, n + 1),
              std::span(v, nnz)} {
    check_values_range();
  }

  explicit SparseMatrixCSC(const size_t m, const size_t n, const size_t nnz,
                           const Idx_arr& i, const Idx_arr& j,
                           const Scal_arr& v)
      : BaseT{m, n, nnz, i, j, v} {
    check_dimensions();
    check_values_range();
  }

  explicit SparseMatrixCSC(const size_t m, const size_t n, const size_t nnz,
                           Idx_arr&& i, Idx_arr&& j, Scal_arr&& v)
      : BaseT{m, n, nnz, std::move(i), std::move(j), std::move(v)} {
    check_dimensions();
    check_values_range();
  }

  // Initialize with a triplet of vectors ({i}, {j}, {v})
  // Guess m and n as maximums of i and j (if not given)
  // To be used for small matrices (quick testing)
  explicit SparseMatrixCSC(std::initializer_list<Index> i,
                           std::initializer_list<Index> j,
                           std::initializer_list<Scalar> v, const size_t m = 0,
                           const size_t n = 0) {
    _m = m;
    _n = n;
    _nnz = v.size();
    COMPOSYX_ASSERT(_nnz == i.size(),
                    "Initialize sparse matrix with size(i) != size(v)");
    // Auto-detect matrix dimensions with max (when not given by the user)
    if (_n == 0 && _nnz != 0) {
      _n = j.size() - 1;
    } else {
      COMPOSYX_ASSERT(j.size() == _n + 1,
                      "Initialize sparse matrix with size(j) != n + 1");
    }
    if (_m == 0 && _nnz != 0) {
      for (Index k : i) {
        _m = static_cast<size_t>((std::max(k, static_cast<Index>(_m))));
      }
      ++_m; // Because starting at 0
    }
    _i = Idx_arr(_nnz);
    _j = Idx_arr(_n + 1);
    _v = Scal_arr(_nnz);
    std::ranges::copy(i, _i.begin());
    std::ranges::copy(j, _j.begin());
    std::ranges::copy(v, _v.begin());
    check_values_range();
  }

  // From a dense matrix
  template <typename DMat>
    requires requires(DMat d, int i, int j) {
      { n_rows(d) } -> std::integral;
      { n_cols(d) } -> std::integral;
      { d(i, j) } -> std::convertible_to<Scalar>;
    }
  SparseMatrixCSC(const DMat& dm, Real drop = 1e-15) {
    _m = n_rows(dm);
    _n = n_cols(dm);
    _nnz = 0;

    // Count nnz
    for (size_t j = 0; j < _n; ++j) {
      for (size_t i = 0; i < _m; ++i) {
        if (std::abs(dm(i, j)) > drop)
          ++_nnz;
      }
    }

    // Create and fill ijv
    _i = Idx_arr(_nnz);
    _j = Idx_arr(_n + 1);
    _v = Scal_arr(_nnz);
    int k = 0;
    _j[0] = 0;
    for (size_t j = 0; j < _n; ++j) {
      for (size_t i = 0; i < _m; ++i) {
        if (std::abs(dm(i, j)) > drop) {
          _i[k] = i;
          _v[k] = dm(i, j);
          k++;
        }
      }
      _j[j + 1] = k;
    }
    this->copy_properties(dm);
  }
  // Constructors:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Apply function on IJV][Apply function on IJV:1]]
  template <CPX_FuncIJV<Index, Scalar> Func> void foreach_ijv(Func f) const {
    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      for (Index k = _j[j]; k < _j[j + 1]; ++k) {
        f(_i[k], j, _v[k]);
      }
    }
  }

  template <CPX_FuncIJV<Index, Scalar> Func>
  void foreach_ijv_max_i(Func f, size_t max_i) const {
    const Index mi = static_cast<Index>(max_i);
    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      for (Index k = _j[j]; k < _j[j + 1]; ++k) {
        if (_i[k] >= mi)
          break;
        f(_i[k], j, _v[k]);
      }
    }
  }

  template <CPX_FuncIJV<Index, Scalar> Func>
  void foreach_ijv_max_j(Func f, size_t max_j) const {
    for (Index j = 0; j < static_cast<Index>(max_j); ++j) {
      for (Index k = _j[j]; k < _j[j + 1]; ++k) {
        f(_i[k], j, _v[k]);
      }
    }
  }
  // Apply function on IJV:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Factories][Factories:1]]
  static SparseMatrixCSC eye(const size_t M, const size_t N) {
    size_t nnz = std::min(M, N);
    Scal_arr v(nnz);
    std::ranges::fill(v, Scalar{1});
    SparseMatrixCSC out(M, N, nnz, arange<Idx_arr>(nnz), arange<Idx_arr>(N + 1),
                        std::move(v));
    return out;
  }

  static SparseMatrixCSC identity(const size_t M) {
    return SparseMatrixCSC::eye(M, M);
  }

  static SparseMatrixCSC from_matrix_market(const std::string& filename) {
    MatrixSymmetry sym;
    MatrixStorage stor;
    size_t m, n, nnz;
    Idx_arr i, j;
    Scal_arr v;
    matrix_market::load(filename, i, j, v, m, n, nnz, sym, stor);

    // Sort in j direction
    std::vector<size_t> idx = multilevel_argsort<std::vector<size_t>>(j, i);
    apply_permutation(idx, i, j, v);

    Idx_arr jout(n + 1);

    jout[0] = 0;
    Index cur_j = 0;

    for (size_t k = 0; k < nnz; ++k) {
      while (j[k] > cur_j) {
        cur_j++;
        jout[cur_j] = k;
      }
    }
    cur_j++;
    while (cur_j < static_cast<Index>(n + 1)) {
      jout[cur_j] = nnz;
      cur_j++;
    }

    SparseMatrixCSC out(m, n, nnz, std::move(i), std::move(jout), std::move(v));
    out.set_property(sym);
    out.set_property(stor);
    return out;
  }
  // Factories:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Generic SparseMatrix methods][Generic SparseMatrix methods:1]]
  [[nodiscard]] DenseMatrix<Scalar> to_dense() const {
    return sparse_matrix_to_dense(*this);
  }

  [[nodiscard]] DiagonalMatrix<Scalar> diag() const {
    return sparse_matrix_diag(*this);
  }

  [[nodiscard]] Vector<Scalar> diag_vect() const {
    return sparse_matrix_diag_vect(*this);
  }

  void fill_half_to_full_storage() {
    sparse_matrix_fill_half_to_full_storage(*this);
  }

  void to_storage_half(MatrixStorage storage = MatrixStorage::lower) {
    sparse_matrix_to_storage_half(*this, storage);
  }

  SparseMatrixCSC& operator*=(const SparseMatrixCSC& other) {
    sparse_matrix_spmm(*this, other);
    return *this;
  }

  SparseMatrixCSC& operator*=(const SparseMatrixCOO<Scalar, Index>& other) {
    sparse_matrix_spmm(*this, other);
    return *this;
  }

  SparseMatrixCSC& operator*=(const SparseMatrixCSR<Scalar, Index>& other) {
    sparse_matrix_spmm(*this, other);
    return *this;
  }

  void right_diagmm(const DiagonalMatrix<Scalar>& other) {
    sparse_matrix_right_diagmm(*this, other);
  }

  void left_diagmm(const DiagonalMatrix<Scalar>& other) {
    sparse_matrix_left_diagmm(*this, other);
  }

  COMPOSYX_SPARSE_MATRIX_CAST_FUNCTION(SparseMatrixCSC)
  // Generic SparseMatrix methods:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Conversion functions][Conversion functions:1]]
  // Conversion
  void from_coo(const SparseMatrixCOO<Scalar, Index>& mat) {

    _m = n_rows(mat);
    _n = n_cols(mat);
    _nnz = n_nonzero(mat);

    _i = Idx_arr(_nnz);
    _j = Idx_arr(_n + 1);
    _v = Scal_arr(_nnz);

    Idx_arr i(mat.get_i_ptr(), mat.get_i_ptr() + _nnz);
    Idx_arr j(mat.get_j_ptr(), mat.get_j_ptr() + _nnz);
    Scal_arr v(mat.get_v_ptr(), mat.get_v_ptr() + _nnz);

    // Sort in j direction
    std::vector<size_t> idx = multilevel_argsort<std::vector<size_t>>(j, i);
    apply_permutation(idx, i, j, v);

    _j[0] = 0;
    Index cur_j = 0;

    for (size_t k = 0; k < _nnz; ++k) {
      _i[k] = i[k];
      _v[k] = v[k];
      while (j[k] > cur_j) {
        cur_j++;
        _j[cur_j] = k;
      }
    }
    cur_j++;
    while (cur_j < static_cast<Index>(_n + 1)) {
      _j[cur_j] = _nnz;
      cur_j++;
    }

    this->copy_properties(mat);
  }

  // Conversion
  void from_lil(const SparseMatrixLIL<Scalar, Index>& lilmat) {
    (*this) = std::move(lilmat.to_csc());
  }

  template <typename OutIndex = Index>
  SparseMatrixCOO<Scalar, OutIndex> to_coo() const {
    SparseMatrixCOO<Scalar, OutIndex> out;
    out.template from_csc<Index>(*this);
    out.copy_properties(*this);
    return out;
  }

  void convert(SparseMatrixCSC& out_mat) const { out_mat = *this; }
  void convert(SparseMatrixCOO<Scalar>& out_mat) const {
    out_mat = this->to_coo();
  }
  void convert(DenseMatrix<Scalar>& out_mat) const {
    out_mat = this->to_dense();
  }
  // Conversion functions:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Load matrix from a matrix market file][Load matrix from a matrix market file:1]]
  // From matrix_market file
  void from_matrix_market_file(const std::string& filename) {
    SparseMatrixCOO<Scalar> sp_mat;
    sp_mat.from_matrix_market_file(filename);
    this->from_coo(sp_mat);
  }
  // Load matrix from a matrix market file:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Comparison functions][Comparison functions:1]]
  bool operator==(const SparseMatrixCSC& spmat2) const {
    if (this->_m != spmat2._m)
      return false;
    if (this->_n != spmat2._n)
      return false;
    if (this->_nnz != spmat2._nnz)
      return false;
    if (this->_i != spmat2._i)
      return false;
    if (this->_j != spmat2._j)
      return false;
    if (this->_v != spmat2._v)
      return false;
    return true;
  }

  bool operator!=(const SparseMatrixCSC& spmat2) const {
    return !(*this == spmat2);
  }
  // Comparison functions:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Re-indexing][Re-indexing:1]]
  template <CPX_IntArray I> void reindex(const I& new_indices, int new_m = -1) {
    SparseMatrixCOO<Scalar, Index> s = this->to_coo();
    s.reindex(new_indices, new_m);
    (*this) = s.to_csc();
  }

  template <CPX_IntArray I>
  void reindex(const I& new_indices, const I& old_indices, int new_m = -1) {
    SparseMatrixCOO<Scalar, Index> s = this->to_coo();
    s.reindex(new_indices, old_indices, new_m);
    (*this) = s.to_csc();
  }
// Re-indexing:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Element accessor][Element accessor:1]]
#define SPMATCSC_COEFF_ACCESSOR                                                \
  do {                                                                         \
    if (this->is_storage_lower() && (i < j)) {                                 \
      std::swap(i, j);                                                         \
    } else if (this->is_storage_upper() && (i > j)) {                          \
      std::swap(i, j);                                                         \
    }                                                                          \
                                                                               \
    for (Index k = _j[j]; k < _j[j + 1]; ++k) {                                \
      if (_i[k] == i) {                                                        \
        return _v[k];                                                          \
      }                                                                        \
    }                                                                          \
  } while (0)

  [[nodiscard]] Scalar coeff(Index i, Index j) const {
    SPMATCSC_COEFF_ACCESSOR;
    return Scalar{0};
  }

  // Element accessor (access is linear in m)
  [[nodiscard]] const Scalar& coeffRef(Index i, Index j) const {
    SPMATCSC_COEFF_ACCESSOR;
    throw ComposyxAccessZeroValue(
        "SparseMatrixCSC(i,j) does not point to a non zero value");
    return _v[0];
  }

  [[nodiscard]] Scalar& coeffRef(Index i, Index j) {
    SPMATCSC_COEFF_ACCESSOR;
    throw ComposyxAccessZeroValue(
        "SparseMatrixCSC(i,j) does not point to a non zero value");
    return _v[0];
  }

  [[nodiscard]] const Scalar& operator()(Index i, Index j) const {
    return this->coeffRef(i, j);
  }
  [[nodiscard]] Scalar& operator()(Index i, Index j) {
    return this->coeffRef(i, j);
  }
  [[nodiscard]] std::tuple<Scalar*, Index*, size_t> get_col(Index j) {
    return std::make_tuple(&_v[_j[j]], &_i[_j[j]], _j[j + 1] - _j[j]);
  }
  // Element accessor:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Memory cost][Memory cost:1]]
  [[nodiscard]] inline size_t memcost() {
    constexpr size_t elt_size = (sizeof(size_t) + sizeof(Scalar));
    return elt_size * _nnz + (_n + 1) * sizeof(Index);
  }
  // Memory cost:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Serialization][Serialization:1]]
  std::vector<char> serialize() const {
    // M, N, values
    int M = static_cast<int>(_m);
    int N = static_cast<int>(_n);
    int NNZ = static_cast<int>(_nnz);
    std::vector<char> buffer((3 + NNZ + N + 1) * sizeof(int) +
                             NNZ * sizeof(Scalar));
    int cnt = 0;

    std::memcpy(&buffer[cnt], &M, sizeof(int));
    cnt += sizeof(int);
    std::memcpy(&buffer[cnt], &N, sizeof(int));
    cnt += sizeof(int);
    std::memcpy(&buffer[cnt], &NNZ, sizeof(int));
    cnt += sizeof(int);
    std::memcpy(&buffer[cnt], &_i[0], _nnz * sizeof(int));
    cnt += _nnz * sizeof(int);
    std::memcpy(&buffer[cnt], &_j[0], (_n + 1) * sizeof(int));
    cnt += (_n + 1) * sizeof(int);
    std::memcpy(&buffer[cnt], &_v[0], _nnz * sizeof(Scalar));

    return buffer;
  }

  void deserialize(const std::vector<char>& buffer) {
    int M, N, NNZ;
    int cnt = 0;
    std::memcpy(&M, &buffer[cnt], sizeof(int));
    cnt += sizeof(int);
    std::memcpy(&N, &buffer[cnt], sizeof(int));
    cnt += sizeof(int);
    std::memcpy(&NNZ, &buffer[cnt], sizeof(int));
    cnt += sizeof(int);
    _m = static_cast<size_t>(M);
    _n = static_cast<size_t>(N);
    _nnz = static_cast<size_t>(NNZ);
    _i = Idx_arr(_nnz);
    _j = Idx_arr(_n + 1);
    _v = Scal_arr(_nnz);

    std::memcpy(&_i[0], &buffer[cnt], _nnz * sizeof(int));
    cnt += _nnz * sizeof(int);
    std::memcpy(&_j[0], &buffer[cnt], (_n + 1) * sizeof(int));
    cnt += (_n + 1) * sizeof(int);
    std::memcpy(&_v[0], &buffer[cnt], _nnz * sizeof(Scalar));
  }
  // Serialization:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Operators][Operators:1]]
  // Scalar multiplication
  using SparseMatrixBase<Scalar, Index>::operator*=;

  template <CPX_IJV_SparseMatrix OtherMatrix>
  SparseMatrixCSC& operator+=(const OtherMatrix& other) {
    sparse_matrix_addition(*this, other);
    return *this;
  }

  template <CPX_IJV_SparseMatrix OtherMatrix>
  SparseMatrixCSC& operator-=(const OtherMatrix& other) {
    sparse_matrix_addition(*this, other, Scalar{-1});
    return *this;
  }

  SparseMatrixCSC& operator+=(const DiagonalMatrix<Scalar>& other) {
    sparse_matrix_diagonal_addition(*this, other);
    return *this;
  }

  SparseMatrixCSC& operator-=(const DiagonalMatrix<Scalar>& other) {
    sparse_matrix_diagonal_addition(*this, other, Scalar{-1});
    return *this;
  }

  SparseMatrixCSC& operator*=(const DiagonalMatrix<Scalar>& other) {
    this->right_diagmm(other);
    return *this;
  }
  // Operators:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Display function][Display function:1]]
  // Pretty printing
  void display(const std::string& name = "",
               std::ostream& out = std::cout) const {
    if (!name.empty())
      out << name << '\n';
    out << "m: " << _m << " "
        << "n: " << _n << " "
        << "nnz: " << _nnz << '\n'
        << this->properties_str() << '\n'
        << '\n';

    out << "i\tv" << '\n';
    for (size_t k = 0; k < _n; ++k) {
      out << "--- Col " << k << '\n';
      for (Index k2 = _j[k]; k2 < _j[k + 1]; ++k2) {
        out << _i[k2] << "\t" << _v[k2] << '\n';
      }
    }

    out << '\n';
  }

  friend std::ostream& operator<<(std::ostream& out,
                                  const SparseMatrixCSC& spcsc) {
    spcsc.display("", out);
    return out;
  }
  // Display function:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Transposition][Transposition:1]]
  void transpose() {
    if (this->is_symmetric())
      return;

    Idx_arr tr_j(_m + 1);
    tr_j[0] = 0;

    foreach_ijv([&](Index i, Index, Scalar) { tr_j[i + 1]++; });

    for (size_t k = 1; k < tr_j.size(); ++k) {
      tr_j[k] += tr_j[k - 1];
    }

    Idx_arr tr_i(_nnz);
    Scal_arr tr_v(_nnz);
    Idx_arr offset_j(_m, 0);

    foreach_ijv([&](Index i, Index j, Scalar v) {
      auto pos = tr_j[i] + offset_j[i];
      tr_i[pos] = j;
      tr_v[pos] = v;
      offset_j[i]++;
    });

    _i = std::move(tr_i);
    _j = std::move(tr_j);
    _v = std::move(tr_v);

    std::swap(_m, _n);
  }

  void conj_transpose() {
    if (this->is_hermitian())
      return;
    this->transpose();
    if constexpr (is_complex<Scalar>::value) {
      std::ranges::for_each(_v, [](Scalar& s) { s = conj<Scalar>(s); });
    }
  }

  [[nodiscard]] auto t() const {
    return OpSpMatCSC<Scalar, Index, Op::Trans>(*this);
  }

  [[nodiscard]] auto h() const {
    return OpSpMatCSC<Scalar, Index, Op::ConjTrans>(*this);
  }

  template <Op op>
  SparseMatrixCSC(const OpSpMatCSC<Scalar, Index, op>& opmat)
      : SparseMatrixCSC(opmat.mat) {
    debug_log_ET::log("SparseMatrixCSC = OpSpMatCSC");
    apply_op<op>(*this);
  }

  template <Op op>
  SparseMatrixCSC& operator=(const OpSpMatCSC<Scalar, Index, op>& opmat) {
    debug_log_ET::log("SparseMatrixCSC = OpSpMatCSC");
    if (this != &(opmat.mat))
      *this = opmat.mat;
    apply_op<op>(*this);
    return *this;
  }
  // Transposition:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Out of class operators][Out of class operators:1]]
}; // class SparseMatrixCSC
// Out of class operators:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Out of class operators][Out of class operators:2]]
// Scalar multiplication
template <class Scalar>
inline SparseMatrixCSC<Scalar> operator*(SparseMatrixCSC<Scalar> sp_mat,
                                         const Scalar& scal) {
  sp_mat *= scal;
  return sp_mat;
}

template <class Scalar>
inline SparseMatrixCSC<Scalar>
operator*(Scalar scal, const SparseMatrixCSC<Scalar>& sp_mat) {
  return sp_mat * scal;
}

template <class Scalar>
inline SparseMatrixCSC<Scalar> operator/(SparseMatrixCSC<Scalar> sp_mat,
                                         const Scalar& scal) {
  sp_mat /= scal;
  return sp_mat;
}

template <CPX_IJV_SparseMatrix OtherMatrix,
          CPX_Scalar Scalar = OtherMatrix::scalar_type>
inline SparseMatrixCSC<Scalar> operator+(SparseMatrixCSC<Scalar> sp_mat,
                                         const OtherMatrix& other) {
  sp_mat += other;
  return sp_mat;
}

template <CPX_IJV_SparseMatrix OtherMatrix,
          CPX_Scalar Scalar = OtherMatrix::scalar_type>
inline SparseMatrixCSC<Scalar> operator-(SparseMatrixCSC<Scalar> sp_mat,
                                         const OtherMatrix& other) {
  sp_mat -= other;
  return sp_mat;
}

template <CPX_IJV_SparseMatrix OtherMatrix,
          CPX_Scalar Scalar = OtherMatrix::scalar_type>
inline SparseMatrixCSC<Scalar> operator*(SparseMatrixCSC<Scalar> sp_mat_l,
                                         const OtherMatrix& sp_mat_r) {
  sp_mat_l *= sp_mat_r;
  return sp_mat_l;
}
// Out of class operators:2 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Matrix inverse operator][Matrix inverse operator:1]]
template <CPX_Scalar Scalar, CPX_Integral Index>
auto operator~(const SparseMatrixCSC<Scalar, Index>& A) {
  return COMPOSYX_SPARSE_SOLVER(A);
}

template <CPX_Scalar Scalar, CPX_Integral Index>
Vector<Scalar> operator%(const SparseMatrixCSC<Scalar, Index>& A,
                         const Vector<Scalar>& b) {
  return ~A * b;
}
// Matrix inverse operator:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Traits][Traits:1]]
template <typename Scalar, CPX_Integral Index>
struct scalar_type<SparseMatrixCSC<Scalar, Index>> : public std::true_type {
  using type = Scalar;
};

template <typename Scalar, CPX_Integral Index>
struct vector_type<SparseMatrixCSC<Scalar, Index>> : public std::true_type {
  using type = Vector<Scalar>;
};

template <typename Scalar, CPX_Integral Index>
struct dense_type<SparseMatrixCSC<Scalar, Index>> : public std::true_type {
  using type = DenseMatrix<Scalar>;
};

template <typename Scalar, CPX_Integral Index>
struct sparse_type<SparseMatrixCSC<Scalar, Index>> : public std::true_type {
  using type = SparseMatrixCSC<Scalar, Index>;
};

template <typename Scalar, CPX_Integral Index>
struct diag_type<SparseMatrixCSC<Scalar, Index>> : public std::true_type {
  using type = DiagonalMatrix<Scalar>;
};

template <typename Scalar, CPX_Integral Index>
struct is_sparse<SparseMatrixCSC<Scalar, Index>> : public std::true_type {};
// Traits:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Transposed or adjoint sparse matrix structure][Transposed or adjoint sparse matrix structure:1]]
template <CPX_Scalar Scalar, CPX_Integral Index, Op op> struct OpSpMatCSC {
  using Mat = SparseMatrixCSC<Scalar, Index>;
  const Mat& mat;

  OpSpMatCSC(const Mat& m) : mat{m} { debug_log_ET::log("OpSpMatCSC"); }

  auto eval() const {
    if constexpr (op == Op::NoTrans) {
      return mat;
    } else if constexpr (op == Op::Trans) {
      Mat emat(mat);
      emat.transpose();
      return emat;
    } else if constexpr (op == Op::ConjTrans) {
      Mat emat(mat);
      emat.conj_transpose();
      return emat;
    }
  }

  auto t() const {
    // A.h().t()
    static_assert(op != Op::ConjTrans, "Unsupported A.h().t() expression");

    if constexpr (op == Op::NoTrans) {
      return OpSpMatCSC<Scalar, Index, Op::Trans>(mat);
    } else {
      return OpSpMatCSC<Scalar, Index, Op::NoTrans>(mat);
    }
  }

  auto h() const {
    // A.t().h()
    static_assert(op != Op::Trans, "Unsupported A.t().h() expression");

    if constexpr (op == Op::NoTrans) {
      return OpSpMatCSC<Scalar, Index, Op::ConjTrans>(mat);
    } else {
      return OpSpMatCSC<Scalar, Index, Op::NoTrans>(mat);
    }
  }
};
// Transposed or adjoint sparse matrix structure:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*SpMV structure][SpMV structure:1]]
template <CPX_Scalar Scalar, CPX_Integral Index, Op opA, typename Rhs>
struct SpCSCMV {
  const SparseMatrixCSC<Scalar, Index>& sp_mat;
  const Rhs& rhs;

  SpCSCMV(const SparseMatrixCSC<Scalar, Index>& m, const Rhs& r)
      : sp_mat{m}, rhs{r} {
    debug_log_ET::log("SpCSCMV");
  }

  inline Rhs eval() const;
};

template <CPX_Scalar Scalar, CPX_Integral Index, Op opA, typename Rhs>
Rhs operator*(Scalar s, const SpCSCMV<Scalar, Index, opA, Rhs>& spmv) {
  return s * spmv.eval();
}
// SpMV structure:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Interface functions][Interface functions:1]]
template <typename Scalar, CPX_Integral Index = int>
SparseMatrixCSC<Scalar, Index>
transpose(const SparseMatrixCSC<Scalar, Index>& mat) {
  SparseMatrixCSC<Scalar, Index> tr = mat.t();
  return tr;
}

template <typename Scalar, CPX_Integral Index = int>
SparseMatrixCSC<Scalar, Index>
adjoint(const SparseMatrixCSC<Scalar, Index>& mat) {
  SparseMatrixCSC<Scalar, Index> adjmat = mat.h();
  return adjmat;
}

template <typename Scalar>
void build_matrix(SparseMatrixCSC<Scalar>& mat, const int M, const int N,
                  const int nnz, int* i, int* j, Scalar* v,
                  const bool fill_symmetry = false) {
  SparseMatrixCOO<Scalar> coo_mat;
  build_matrix(coo_mat, M, N, nnz, i, j, v, fill_symmetry);
  mat.from_coo(coo_mat);
}

template <CPX_Scalar Scalar, CPX_IntArray I>
void reindex(SparseMatrixCSC<Scalar>& mat, const I& new_indices,
             int new_m = -1) {
  mat.reindex(new_indices, new_m);
}
// Interface functions:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Construction with CSR format][Construction with CSR format:1]]
template <CPX_Scalar Scalar, CPX_Integral Index>
SparseMatrixCSC<Scalar, Index>
CSC_from_CSR(std::span<const Index> csri, std::span<const Index> csrj,
             std::span<const Scalar> csrv, size_t csrm, size_t csrn) {
  SparseMatrixCSC<Scalar, Index> out(csrn, csrm, csrv.size(), csrj.data(),
                                     csri.data(), csrv.data());
  out.transpose();
  return out;
  //return COO_from_CSR<Scalar, Index>(csri, csrj, csrv, csrm, csrn).to_csc();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
SparseMatrixCSC<Scalar, Index>
CSC_from_CSR(std::initializer_list<Index> i, std::initializer_list<Index> j,
             std::initializer_list<Scalar> v, const size_t m, const size_t n) {
  return CSC_from_CSR<Scalar, Index>(std::span(i.begin(), i.end()),
                                     std::span(j.begin(), j.end()),
                                     std::span(v.begin(), v.end()), m, n);
}
// Construction with CSR format:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
