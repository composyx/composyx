// [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Header][Header:1]]
#pragma once

#include <forward_list>

namespace composyx {
template <class, class = int> class SparseMatrixLIL;
}

#include "composyx/utils/Arithmetic.hpp"
#include "composyx/utils/MatrixProperties.hpp"
#include "composyx/utils/Error.hpp"
#include "composyx/utils/ArrayAlgo.hpp"
#include "composyx/loc_data/SparseMatrixCOO.hpp"
#include "composyx/loc_data/SparseMatrixCSR.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Attributes][Attributes:1]]
template <class Scalar, class Index>
class SparseMatrixLIL : public MatrixProperties<Scalar> {

public:
  using local_type = SparseMatrixLIL<Scalar, Index>;
  using scalar_type = Scalar;
  using real_type = typename arithmetic_real<Scalar>::type;

private:
  using Real = real_type;

  size_t _m = 0;
  size_t _n = 0;
  size_t _nnz = 0;

  using Pair = std::pair<Index, Scalar>;
  using List = std::forward_list<Pair>;
  // Array of N lists of pairs (row index, value)
  std::vector<List> _data;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Constructors][Constructors:1]]
  // Check that n and are positive
  void check_dims_positive() {
    COMPOSYX_ASSERT_POSITIVE(_m, "M, in initialization SparseMatrixLIL");
    COMPOSYX_ASSERT_POSITIVE(_n, "N, in initialization SparseMatrixLIL");
  }

public:
  explicit SparseMatrixLIL(const size_t m, const size_t n) : _m{m}, _n{n} {
    check_dims_positive();
    _data = std::vector<List>(n);
  }
  // Constructors:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Checking order for debugging][Checking order for debugging:1]]
  // Check order of I and J arrays
  void check_indices_order() const {
    if (_n == 0)
      return;

    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      int i_prev = -1;
      for (auto& [i, _] : _data[j]) {
        COMPOSYX_ASSERT(static_cast<int>(i) > i_prev,
                        "LIL: Error in I array order");
        i_prev = static_cast<int>(i);
      }
    }
  }
  // Checking order for debugging:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Apply function on IJV][Apply function on IJV:1]]
  template <CPX_FuncIJV<Index, Scalar> Func> void foreach_ijv(Func f) const {
    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      for (auto& [i, v] : _data[j]) {
        f(i, j, v);
      }
    }
  }

  template <CPX_FuncIJV<Index, Scalar> Func>
  void foreach_ijv_max_i(Func f, size_t max_i) const {
    const Index mi = static_cast<Index>(max_i);
    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      for (auto& [i, v] : _data[j]) {
        if (i >= mi)
          break;
        f(i, j, v);
      }
    }
  }

  template <CPX_FuncIJV<Index, Scalar> Func>
  void foreach_ijv_max_j(Func f, size_t max_j) const {
    for (Index j = 0; j < static_cast<Index>(max_j); ++j) {
      for (auto& [i, v] : _data[j]) {
        f(i, j, v);
      }
    }
  }
  // Apply function on IJV:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Factories][Factories:1]]
  static SparseMatrixLIL eye(const size_t M, const size_t N) {
    size_t nnz = std::min(M, N);
    SparseMatrixLIL out(M, N);
    for (size_t k = 0; k < nnz; ++k) {
      out.insert(k, k, Scalar{1});
    }
    return out;
  }

  static SparseMatrixLIL identity(const size_t M) {
    return SparseMatrixLIL::eye(M, M);
  }

  static SparseMatrixLIL from_matrix_market(const std::string& filename) {
    MatrixSymmetry sym;
    MatrixStorage stor;
    size_t m, n, nnz;
    std::vector<Index> i, j;
    std::vector<Scalar> v;
    matrix_market::load(filename, i, j, v, m, n, nnz, sym, stor);
    SparseMatrixLIL out(m, n);

    for (size_t k = 0; k < nnz; ++k) {
      out.insert(i[k], j[k], v[k]);
    }
    out.set_property(sym);
    out.set_property(stor);
    return out;
  }
  // Factories:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Insertion of an element][Insertion of an element:1]]
private:
  static void sum_duplicates(Scalar& v1, const Scalar& v2) { v1 += v2; }

public:
  void insert(Index i, Index j, const Scalar v,
              void (*treat_duplicates)(Scalar&,
                                       const Scalar&) = sum_duplicates) {

    if (this->is_storage_lower() && (i < j)) {
      std::swap(i, j);
    } else if (this->is_storage_upper() && (i > j)) {
      std::swap(i, j);
    }

    List& colj = _data[j];

    // Iterator to one before the first element
    auto prev_it = colj.before_begin();
    for (auto curr_it = colj.begin(); curr_it != colj.end();
         ++curr_it, ++prev_it) {
      auto& [li, lv] = *curr_it;
      if (li == i) {
        treat_duplicates(lv, v);
        return;
      }
      if (li > i) {
        colj.emplace_after(prev_it, Pair(i, v));
        _nnz++;
        return;
      }
    }

    // If i > all elements
    colj.emplace_after(prev_it, Pair(i, v));
    _nnz++;
  }
  // Insertion of an element:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Grouped insertion of a sparse matrix][Grouped insertion of a sparse matrix:1]]
  template <typename Matrix>
  void insert(const Matrix& spmat,
              void (*treat_duplicates)(Scalar&,
                                       const Scalar&) = sum_duplicates) {
    spmat.foreach_ijv([&](Index i, Index j, Scalar v) {
      this->insert(i, j, v, treat_duplicates);
    });
  }
  // Grouped insertion of a sparse matrix:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Grouped insertion of a dense matrix][Grouped insertion of a dense matrix:1]]
  void insert(const DenseMatrix<Scalar>& dm,
              void (*treat_duplicates)(Scalar&,
                                       const Scalar&) = sum_duplicates) {
    for (size_t j = 0; j < n_cols(dm); ++j) {
      for (size_t i = 0; i < n_rows(dm); ++i) {
        if (dm(i, j) != Scalar{0}) {
          this->insert(i, j, dm(i, j), treat_duplicates);
        }
      }
    }
  }
  // Grouped insertion of a dense matrix:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Grouped insertion of a diagonal matrix][Grouped insertion of a diagonal matrix:1]]
  void insert(const DiagonalMatrix<Scalar>& diagmat,
              void (*treat_duplicates)(Scalar&,
                                       const Scalar&) = sum_duplicates) {
    const Scalar* diag = diagmat.get_ptr();
    const size_t dim = std::min(n_rows(diagmat), n_cols(diagmat));
    for (size_t k = 0; k < dim; ++k) {
      this->insert(k, k, diag[k], treat_duplicates);
    }
  }
  // Grouped insertion of a diagonal matrix:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Drop zero elements][Drop zero elements:1]]
  void drop(Real drop) {

    auto rm_fct = [this, drop](const Pair& p) {
      if (std::abs(p.second) < drop) {
        _nnz--;
        return true;
      }
      return false;
    };

    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      _data[j].remove_if(rm_fct);
    }
  }

  void drop() {

    auto rm_fct = [this](const Pair& p) {
      if (p.second == Scalar{0}) {
        _nnz--;
        return true;
      }
      return false;
    };

    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      _data[j].remove_if(rm_fct);
    }
  }
  // Drop zero elements:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Fill to full storage][Fill to full storage:1]]
  void fill_half_to_full_storage() {
    if (this->is_storage_full()) {
      return;
    }
    if (this->is_general()) {
      return;
    }

    SparseMatrixLIL new_coeffs(_m, _n);

    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      for (auto [i, v] : _data[j]) {
        if (i != j) {
          if constexpr (is_complex<Scalar>::value) {
            if (this->is_hermitian()) {
              v = conj<Scalar>(v);
            }
          }
          new_coeffs.insert(j, i, v);
        }
      }
    }

    this->set_property(MatrixStorage::full);
    this->insert(new_coeffs);
  }
  // Fill to full storage:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Conversion functions][Conversion functions:1]]
  template <typename OutIndex = Index>
  SparseMatrixCOO<Scalar, OutIndex> to_coo() const {
    std::vector<OutIndex> i_arr(_nnz);
    std::vector<OutIndex> j_arr(_nnz);
    std::vector<Scalar> v_arr(_nnz);

    Index k = 0;
    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      for (const auto& [i, v] : _data[j]) {
        i_arr[k] = static_cast<OutIndex>(i);
        j_arr[k] = static_cast<OutIndex>(j);
        v_arr[k] = v;
        k++;
      }
    }

    SparseMatrixCOO<Scalar, OutIndex> coo(_m, _n, _nnz, std::move(i_arr),
                                          std::move(j_arr), std::move(v_arr));
    coo.copy_properties(*this);
    return coo;
  }

  template <typename OutIndex = Index>
  SparseMatrixCSC<Scalar, OutIndex> to_csc() const {
    std::vector<OutIndex> i_a(_nnz);
    std::vector<OutIndex> j_a(_n + 1);
    std::vector<Scalar> v_a(_nnz);

    j_a[0] = 0;
    OutIndex k = 0;
    for (OutIndex j = 0; j < static_cast<OutIndex>(_n); ++j) {
      for (const auto& [i, v] : _data[j]) {
        i_a[k] = static_cast<OutIndex>(i);
        v_a[k] = v;
        k++;
      }
      j_a[j + 1] = k;
    }

    SparseMatrixCSC<Scalar, OutIndex> csc(_m, _n, _nnz, std::move(i_a),
                                          std::move(j_a), std::move(v_a));
    csc.copy_properties(*this);
    return csc;
  }

  template <typename OutIndex = Index>
  SparseMatrixCSR<Scalar, OutIndex> to_csr() const {
    // Construct from CSC
    std::vector<OutIndex> i_a(_nnz);
    std::vector<OutIndex> j_a(_n + 1);
    std::vector<Scalar> v_a(_nnz);

    j_a[0] = 0;
    OutIndex k = 0;
    for (OutIndex j = 0; j < static_cast<OutIndex>(_n); ++j) {
      for (const auto& [i, v] : _data[j]) {
        i_a[k] = static_cast<OutIndex>(i);
        v_a[k] = v;
        k++;
      }
      j_a[j + 1] = k;
    }

    SparseMatrixCSR<Scalar, OutIndex> csr =
        CSR_from_CSC<Scalar, OutIndex>(i_a, j_a, v_a, _m, _n);
    csr.copy_properties(*this);
    return csr;
  }

  DenseMatrix<Scalar> to_dense() const {
    DenseMatrix<Scalar> dm(_m, _n);
    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      const List& l = _data[j];
      for (const Pair& iv : l) {
        dm(iv.first, j) = iv.second;
      }
    }
    dm.copy_properties(*this);
    return dm;
  }

  template <typename OutIndex = Index>
  void convert(SparseMatrixCOO<Scalar>& out_mat) const {
    out_mat = this->to_coo<OutIndex>();
  }
  template <typename OutIndex = Index>
  void convert(SparseMatrixCSC<Scalar>& out_mat) const {
    out_mat = this->to_csc<OutIndex>();
  }
  template <typename OutIndex = Index>
  void convert(SparseMatrixCSR<Scalar>& out_mat) const {
    out_mat = this->to_csr<OutIndex>();
  }
  void convert(DenseMatrix<Scalar>& out_mat) const {
    out_mat = this->to_dense();
  }
  // Conversion functions:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Comparison operators][Comparison operators:1]]
  bool operator==(const SparseMatrixLIL<Scalar>& other) const {
    if (_m != other._m)
      return false;
    if (_n != other._n)
      return false;
    if (_nnz != other._nnz)
      return false;

    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      const List& l1 = _data[j];
      const List& l2 = other._data[j];
      if (l1 != l2)
        return false;
    }
    return true;
  }

  bool operator!=(const SparseMatrixLIL<Scalar>& other) const {
    return !(*this == other);
  }
  // Comparison operators:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Getters][Getters:1]]
  // Getters
  [[nodiscard]] inline size_t get_n_rows() const { return _m; }
  [[nodiscard]] inline size_t get_n_cols() const { return _n; }
  [[nodiscard]] inline size_t get_nnz() const { return _nnz; }
  // Getters:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Element accessor][Element accessor:1]]
  // Element accessor (access is linear in m)
  template <CPX_Integral Tint> Scalar& operator()(const Tint i, const Tint j) {
    Index i_t = static_cast<Index>(i);
    Index j_t = static_cast<Index>(j);
    List& l = _data[j_t];
    for (Pair& iv : l) {
      if (iv.first == i_t)
        return iv.second;
    }
    throw ComposyxAccessZeroValue(
        "SparseMatrixLIL(i,j) does not point to a non zero value");
    return l.front().second;
  }

  template <CPX_Integral Tint>
  const Scalar& operator()(const Tint i, const Tint j) const {
    Index i_t = static_cast<Index>(i);
    Index j_t = static_cast<Index>(j);
    const List& l = _data[j_t];
    for (const Pair& iv : l) {
      if (iv.first == i_t)
        return iv.second;
    }
    throw ComposyxAccessZeroValue(
        "SparseMatrixLIL(i,j) does not point to a non zero value");
    return l.front().second;
  }

  [[nodiscard]] Scalar coeff(Index i, Index j) const {
    Index i_t = static_cast<Index>(i);
    Index j_t = static_cast<Index>(j);
    const List& l = _data[j_t];
    for (const Pair& iv : l) {
      if (iv.first == i_t)
        return iv.second;
    }
    return Scalar{0};
    ;
  }

  [[nodiscard]] const Scalar& coeffRef(Index i, Index j) const {
    return (*this)(i, j);
  }

  [[nodiscard]] Scalar& coeffRef(Index i, Index j) { return (*this)(i, j); }
  // Element accessor:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Operators][Operators:1]]
  // Addition and substraction
  SparseMatrixLIL& operator*=(const Scalar scal) {
    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      for (Pair& p : _data[j]) {
        p.second *= scal;
      }
    }
    return *this;
  }

  SparseMatrixLIL& operator+=(const SparseMatrixLIL& mat) {
    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      const List& l = mat._data[j];
      for (const auto& [i, v] : l) {
        this->insert(i, j, v, sum_duplicates);
      }
    }
    this->drop();
    return *this;
  }

  SparseMatrixLIL& operator-=(const SparseMatrixLIL& mat) {
    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      const List& l = mat._data[j];
      for (const auto& [i, v] : l) {
        this->insert(i, j, Scalar{-1} * v, sum_duplicates);
      }
    }
    this->drop();
    return *this;
  }
  // Operators:1 ends here

  // [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Display function][Display function:1]]
  // Pretty printing
  void display(const std::string& name = "",
               std::ostream& out = std::cout) const {
    if (!name.empty())
      out << name << '\n';
    out << "m: " << _m << " "
        << "n: " << _n << " "
        << "nnz: " << _nnz << '\n'
        << this->properties_str() << '\n'
        << '\n';

    out << "i\tv" << '\n';
    for (Index j = 0; j < static_cast<Index>(_n); ++j) {
      out << "--- Col " << j << '\n';
      const List& l = _data[j];
      for (const auto& [i, v] : l) {
        out << i << "\t" << v << '\n';
      }
    }

    out << '\n';
  }

  friend std::ostream& operator<<(std::ostream& out,
                                  const SparseMatrixLIL& lilm) {
    lilm.display("", out);
    return out;
  }
}; // class SparseMatrixLIL
// Display function:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Traits][Traits:1]]
template <typename Scalar>
struct vector_type<SparseMatrixLIL<Scalar>> : public std::true_type {
  using type = Vector<Scalar>;
};

template <typename Scalar>
struct dense_type<SparseMatrixLIL<Scalar>> : public std::true_type {
  using type = DenseMatrix<Scalar>;
};

template <typename Scalar>
struct scalar_type<SparseMatrixLIL<Scalar>> : public std::true_type {
  using type = Scalar;
};

template <typename Scalar>
struct is_sparse<SparseMatrixLIL<Scalar>> : public std::true_type {};
// Traits:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Out of class operators][Out of class operators:1]]
template <class Scalar, class Index = int>
[[nodiscard]]
SparseMatrixLIL<Scalar, Index>
operator+(SparseMatrixLIL<Scalar, Index> A,
          const SparseMatrixLIL<Scalar, Index>& B) {
  A += B;
  return A;
}

template <class Scalar, class Index = int>
[[nodiscard]]
SparseMatrixLIL<Scalar, Index>
operator-(SparseMatrixLIL<Scalar, Index> A,
          const SparseMatrixLIL<Scalar, Index>& B) {
  A -= B;
  return A;
}
// Out of class operators:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Interface functions][Interface functions:1]]
template <CPX_Scalar Scalar, CPX_Integral Index>
inline size_t n_rows(const SparseMatrixLIL<Scalar, Index>& mat) {
  return mat.get_n_rows();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
inline size_t n_cols(const SparseMatrixLIL<Scalar, Index>& mat) {
  return mat.get_n_cols();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
inline size_t n_nonzero(const SparseMatrixLIL<Scalar, Index>& mat) {
  return mat.get_nnz();
}

template <CPX_Scalar Scalar, CPX_Integral Index>
void display(const SparseMatrixLIL<Scalar, Index>& v,
             const std::string& name = "", std::ostream& out = std::cout) {
  v.display(name, out);
}
// Interface functions:1 ends here

// [[file:../../../org/composyx/loc_data/SparseMatrixLIL.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
