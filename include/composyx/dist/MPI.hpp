// [[file:../../../org/composyx/dist/MPI.org::*Header][Header:1]]
#pragma once

#include <mpi.h>
#include <memory>
#include <cassert>
#include <iostream>
#include <vector>
#include <tuple>
#include <cstring>
#include <utility>

#include "composyx/utils/Arithmetic.hpp"
#include "composyx/utils/Error.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/dist/MPI.org::*Message structure][Message structure:1]]
template <class T> struct Message {
  T* buffer = nullptr;
  int size = 0;
  int destination = -1;
  int tag = 1993;

  Message() = default;
  Message(T* b, int s, int d, int t = 1993)
      : buffer{b}, size{s}, destination{d}, tag{t} {}
  Message(const T* b, int s, int d, int t = 1993)
      : buffer{const_cast<T*>(b)}, size{s}, destination{d}, tag{t} {}
};
// Message structure:1 ends here

// [[file:../../../org/composyx/dist/MPI.org::*MMPI namespace][MMPI namespace:1]]
namespace MMPI {

static int current_tag = 0; // To be used for isendMessages
static int provided;
template <class T> inline MPI_Datatype mpi_type() { return MPI_DATATYPE_NULL; }

#define CPX_DEFINE_MPI_DATATYPE(_c_type, _mpi_type)                            \
  template <> inline MPI_Datatype mpi_type<_c_type>() { return _mpi_type; }
CPX_DEFINE_MPI_DATATYPE(char, MPI_CHAR)
CPX_DEFINE_MPI_DATATYPE(int, MPI_INT)
CPX_DEFINE_MPI_DATATYPE(long int, MPI_LONG)
CPX_DEFINE_MPI_DATATYPE(unsigned int, MPI_UNSIGNED)
CPX_DEFINE_MPI_DATATYPE(float, MPI_FLOAT)
CPX_DEFINE_MPI_DATATYPE(double, MPI_DOUBLE)
CPX_DEFINE_MPI_DATATYPE(std::complex<float>, MPI_C_FLOAT_COMPLEX)
CPX_DEFINE_MPI_DATATYPE(std::complex<double>, MPI_C_DOUBLE_COMPLEX)

template <> inline MPI_Datatype mpi_type<std::size_t>() {
  if constexpr (sizeof(std::size_t) == 2) {
    return MPI_UINT16_T;
  } else if constexpr (sizeof(std::size_t) == 4) {
    return MPI_UINT32_T;
  } else {
    return MPI_UINT64_T;
  }
}

inline int init(int thread_required = MPI_THREAD_MULTIPLE) {
  int flag = 0;
  int res_mpi_init = 0;
  MPI_Initialized(&flag);
  if (!flag) {
    res_mpi_init = MPI_Init_thread(NULL, NULL, thread_required, &provided);
    if (provided < thread_required) {
      COMPOSYX_WARNING(
          "The threading support level is less than that demanded.\n");
    }
  }
  return res_mpi_init;
}
inline int finalize() {
  int flag = 0;
  int res_mpi_final = 0;
  MPI_Finalized(&flag);
  if (!flag) {
    res_mpi_final = MPI_Finalize();
  }
  return res_mpi_final;
}

inline int rank(const MPI_Comm comm = MPI_COMM_WORLD) {
  int rank;
  MPI_Comm_rank(comm, &rank);
  return rank;
}

inline int size(const MPI_Comm comm = MPI_COMM_WORLD) {
  int size;
  MPI_Comm_size(comm, &size);
  return size;
}

inline MPI_Comm comm_dup(const MPI_Comm comm) {
  MPI_Comm duplicated;
  MPI_Comm_dup(comm, &duplicated);
  return duplicated;
}

template <class T>
inline void send(const T* buffer, const int count, const int tag,
                 const int dest, const MPI_Comm comm = MPI_COMM_WORLD) {
  MPI_Send(buffer, count, mpi_type<T>(), dest, tag, comm);
}

template <class T>
inline MPI_Status recv(T* buffer, const int count, const int tag,
                       const int source, const MPI_Comm comm = MPI_COMM_WORLD) {
  MPI_Status stat;
  MPI_Recv(buffer, count, mpi_type<T>(), source, tag, comm, &stat);
  return stat;
}

template <class T>
inline void isend(const T* buffer, const int count, const int tag,
                  const int dest, MPI_Request& req,
                  const MPI_Comm comm = MPI_COMM_WORLD) {
  MPI_Isend(buffer, count, mpi_type<T>(), dest, tag, comm, &req);
}

template <class T>
inline void irecv(T* buffer, const int count, const int tag, const int source,
                  MPI_Request& req, const MPI_Comm comm = MPI_COMM_WORLD) {
  MPI_Irecv(buffer, count, mpi_type<T>(), source, tag, comm, &req);
}

//----
template <class T>
inline void gather(const T* send_buffer, T* recv_buffer, const int count,
                   const int root, const MPI_Comm comm = MPI_COMM_WORLD) {
  MPI_Gather(send_buffer, count, mpi_type<T>(), recv_buffer, count,
             mpi_type<T>(), root, comm);
}

template <class T, class IntArray>
inline void gatherv(const T* send_buffer, T* recv_buffer, const int count,
                    const IntArray& recvcounts, const IntArray& displ,
                    const int root, const MPI_Comm comm = MPI_COMM_WORLD) {
  MPI_Gatherv(send_buffer, count, mpi_type<T>(), recv_buffer, recvcounts.data(),
              displ.data(), mpi_type<T>(), root, comm);
}

template <class T>
inline void reduce(const T* send_buffer, T* recv_buffer, const int count,
                   const MPI_Op operation, const int root,
                   const MPI_Comm comm = MPI_COMM_WORLD) {
  MPI_Reduce(send_buffer, recv_buffer, count, mpi_type<T>(), operation, root,
             comm);
}
//----

template <class T>
inline void allgather(const T* send_buffer, T* recv_buffer, const int count,
                      const MPI_Comm comm = MPI_COMM_WORLD) {
  MPI_Allgather(send_buffer, count, mpi_type<T>(), recv_buffer, count,
                mpi_type<T>(), comm);
}

template <class T, class IntArray>
inline void allgatherv(const T* send_buffer, T* recv_buffer, const int count,
                       const IntArray& recvcounts, const IntArray& displ,
                       const MPI_Comm comm = MPI_COMM_WORLD) {
  MPI_Allgatherv(send_buffer, count, mpi_type<T>(), recv_buffer,
                 recvcounts.data(), displ.data(), mpi_type<T>(), comm);
}

template <class T>
inline void allreduce(const T* send_buffer, T* recv_buffer, const int count,
                      const MPI_Op operation,
                      const MPI_Comm comm = MPI_COMM_WORLD) {
  MPI_Allreduce(send_buffer, recv_buffer, count, mpi_type<T>(), operation,
                comm);
}

template <class T>
inline void bcast(T* buffer, const int count, const int root,
                  const MPI_Comm comm = MPI_COMM_WORLD) {
  MPI_Bcast(buffer, count, mpi_type<T>(), root, comm);
}

inline MPI_Status probe(const int source, const int tag,
                        const MPI_Comm comm = MPI_COMM_WORLD) {
  MPI_Status stat;
  MPI_Probe(source, tag, comm, &stat);
  return stat;
}

inline bool test(MPI_Request& r, MPI_Status& status) {
  int flag;
  MPI_Test(&r, &flag, &status);
  return (flag != 0);
}

inline void barrier(const MPI_Comm comm = MPI_COMM_WORLD) { MPI_Barrier(comm); }

inline void wait(MPI_Request& req, MPI_Status& stat) { MPI_Wait(&req, &stat); }

inline int waitall(const int count, MPI_Request* reqs, MPI_Status* stats) {
  return MPI_Waitall(count, reqs, stats);
}

inline int waitall(const int count, MPI_Request* reqs) {
  return MPI_Waitall(count, reqs, MPI_STATUSES_IGNORE);
}

inline int waitall(std::vector<MPI_Request>& reqs) {
  return MPI_Waitall(static_cast<int>(reqs.size()), reqs.data(),
                     MPI_STATUSES_IGNORE);
}

inline int tag() { return current_tag; }

template <class T> inline int get_count(MPI_Status& stat) {
  int count;
  MPI_Get_count(&stat, mpi_type<T>(), &count);
  return count;
}

inline int get_source(MPI_Status& stat) { return stat.MPI_SOURCE; }

inline int get_tag(MPI_Status& stat) { return stat.MPI_TAG; }

inline int get_provided() { return provided; }

// Use .display() function on an object and pretty print it in the rank order
template <class T>
inline void ordered_display(const T& obj, std::ostream& out = std::cout,
                            const MPI_Comm comm = MPI_COMM_WORLD) {
  const int lrank = rank(comm);
  const int lsize = size(comm);

  int token_s = 0, token_r = 0;
  if (lrank == 0) {
    out << "---Ordered display---\nRank: 0\n";
    obj.display("", out);
    if (lsize > 1) {
      send<int>(&token_s, 1, 10, 1, comm);
    }
  } else {
    recv<int>(&token_r, 1, 10, lrank - 1, comm);
    out << "Rank: " << lrank << "\n";
    obj.display("", out);
    if (lrank < (lsize - 1)) {
      send<int>(&token_s, 1, 10, lrank + 1, comm);
    }
  }
  barrier(comm);
}

} // namespace MMPI
// MMPI namespace:1 ends here

// [[file:../../../org/composyx/dist/MPI.org::*=MPI_Receptor= class][=MPI_Receptor= class:1]]
template <class T> struct MPI_Receptor {
  MPI_Comm _comm;
  std::vector<MPI_Request> _reqs;
  int _n_recv;
  int _tag;

  MPI_Receptor(MPI_Comm comm) : _comm{comm} {}

  ~MPI_Receptor() { wait(); }

  MPI_Receptor(const MPI_Receptor&) = delete; // Non copyable

  MPI_Receptor(MPI_Receptor&& other) {
    _comm = std::exchange(other._comm, MPI_COMM_NULL);
    _reqs = std::move(other._reqs);
    _n_recv = std::exchange(other._n_recv, 0);
    _tag = std::exchange(other._tag, 0);
  }

  MPI_Receptor& operator=(const MPI_Receptor&) = delete;

  MPI_Receptor& operator=(MPI_Receptor&& other) {
    if (&other == this)
      return *this;
    _comm = std::exchange(other._comm, MPI_COMM_NULL);
    _reqs = std::move(other._reqs);
    _n_recv = std::exchange(other._n_recv, 0);
    _tag = std::exchange(other._tag, 0);
    return *this;
  }

  std::vector<T> operator()(int& source, int& tag, bool force_tag = false,
                            bool force_source = false) {
    if (_n_recv == 0) {
      source = -1;
      return std::vector<T>();
    }

    MPI_Status stat;
    if (force_tag and force_source) {
      stat = MMPI::probe(source, tag, _comm);
    } else if (force_tag) {
      stat = MMPI::probe(MPI_ANY_SOURCE, tag, _comm);
      source = MMPI::get_source(stat);
    } else if (force_source) {
      stat = MMPI::probe(source, MPI_ANY_TAG, _comm);
      tag = MMPI::get_tag(stat);
    } else {
      stat = MMPI::probe(MPI_ANY_SOURCE, MPI_ANY_TAG, _comm);
      tag = MMPI::get_tag(stat);
      source = MMPI::get_source(stat);
    }

    int count = MMPI::get_count<T>(stat);
    std::vector<T> out(count);

    MMPI::recv<T>(&out[0], count, tag, source, _comm);

    _n_recv--;
    return out;
  }

  void probe_no_recv(int& source, int& tag) {
    MPI_Status stat;
    stat = MMPI::probe(MPI_ANY_SOURCE, MPI_ANY_TAG, _comm);
    tag = MMPI::get_tag(stat);
    source = MMPI::get_source(stat);
  }

  void wait() {
    int n_sent = static_cast<int>(_reqs.size());
    std::vector<MPI_Status> stats(n_sent);
    MMPI::waitall(n_sent, _reqs.data(), &stats[0]);
  }

  int get_n_recv() const { return _n_recv; }
}; // class MPI_Receptor
// =MPI_Receptor= class:1 ends here

// [[file:../../../org/composyx/dist/MPI.org::*=MPI_Receptor= class][=MPI_Receptor= class:2]]
template <class T> struct MPI_IReceptor {
  MPI_Comm _comm;
  std::vector<MPI_Request> _s_reqs;
  std::vector<MPI_Request> _r_reqs;
  int _n_recv;

  MPI_IReceptor(MPI_Comm comm) : _comm{comm} {}

  MPI_IReceptor(const MPI_IReceptor&) = delete; // Non copyable

  MPI_IReceptor(MPI_IReceptor&& other) {
    _comm = std::exchange(other._comm, MPI_COMM_NULL);
    _s_reqs = std::move(other._s_reqs);
    _r_reqs = std::move(other._r_reqs);
    _n_recv = std::exchange(other._n_recv, 0);
  }

  MPI_IReceptor& operator=(const MPI_IReceptor&) = delete;

  MPI_IReceptor& operator=(MPI_IReceptor&& other) {
    if (&other == this)
      return *this;
    _comm = std::exchange(other._comm, MPI_COMM_NULL);
    _s_reqs = std::move(other._s_reqs);
    _r_reqs = std::move(other._r_reqs);
    _n_recv = std::exchange(other._n_recv, 0);
    return *this;
  }

  template <class IntArray>
  std::vector<T>& operator()(int& source,
                             const std::vector<IntArray>& recv_buffers) {
    if (_n_recv == 0) {
      source = -1;
      return std::vector<T>();
    }
    MPI_Status stat;
    bool no_receive = true;
    int k;
    while (no_receive) {
      k = 0;
      for (auto& r : _r_reqs) {
        if (MMPI::test(r, stat)) {
          no_receive = false;
          break;
        }
        k++;
      }
    }

    source = MMPI::get_source(stat);
    _n_recv--;
    return recv_buffers[k];
  }

  void wait() {
    MMPI::waitall(static_cast<int>(_s_reqs.size()), _s_reqs.data());
    MMPI::waitall(static_cast<int>(_r_reqs.size()), _r_reqs.data());
  }

  void wait(std::vector<MPI_Status>& s_statuses,
            std::vector<MPI_Status>& r_statuses) {
    s_statuses = std::vector<MPI_Status>(static_cast<int>(_s_reqs.size()));
    r_statuses = std::vector<MPI_Status>(static_cast<int>(_r_reqs.size()));
    MMPI::waitall(static_cast<int>(_s_reqs.size()), _s_reqs.data(),
                  s_statuses.data());
    MMPI::waitall(static_cast<int>(_r_reqs.size()), _r_reqs.data(),
                  r_statuses.data());
  }

  int get_n_recv() const { return _n_recv; }
}; // class MPI_IReceptor
// =MPI_Receptor= class:2 ends here

// [[file:../../../org/composyx/dist/MPI.org::*isendMessages function][isendMessages function:1]]
namespace MMPI {
template <class T>
inline MPI_Receptor<T> isendMessages(const std::vector<Message<T>>& messages,
                                     const int n_recv,
                                     const MPI_Comm comm = MPI_COMM_WORLD) {
  MPI_Receptor<T> receptor(comm);
  int n_mess = static_cast<int>(messages.size());

  receptor._reqs = std::vector<MPI_Request>(n_mess);
  std::vector<MPI_Request>& reqs = receptor._reqs;

  int i = 0;
  for (auto mess : messages) {
    MMPI::isend<T>(mess.buffer, mess.size, mess.tag, mess.destination,
                   reqs.at(i), comm);
    i++;
  }

  receptor._tag = current_tag;
  receptor._n_recv = n_recv;

  return receptor;
}
} // namespace MMPI
// isendMessages function:1 ends here

// [[file:../../../org/composyx/dist/MPI.org::*isendRecvMessages function][isendRecvMessages function:1]]
namespace MMPI {
template <class T>
inline MPI_IReceptor<T>
isendRecvMessages(const std::vector<Message<T>>& send_messages,
                  const std::vector<Message<T>>& recv_messages,
                  const MPI_Comm comm = MPI_COMM_WORLD) {
  MPI_IReceptor<T> receptor(comm);
  int n_mess = static_cast<int>(send_messages.size());

  receptor._s_reqs = std::vector<MPI_Request>(n_mess);
  std::vector<MPI_Request>& send_reqs = receptor._s_reqs;

  int i = 0;
  for (auto& mess : send_messages) {
    MMPI::isend<T>(mess.buffer, mess.size, mess.tag, mess.destination,
                   send_reqs.at(i), comm);
    i++;
  }

  n_mess = static_cast<int>(recv_messages.size());

  receptor._r_reqs = std::vector<MPI_Request>(n_mess);
  std::vector<MPI_Request>& recv_reqs = receptor._r_reqs;

  i = 0;
  for (auto& mess : recv_messages) {
    MMPI::irecv<T>(mess.buffer, mess.size, mess.tag, mess.destination,
                   recv_reqs.at(i), comm);
    i++;
  }

  receptor._n_recv = i;

  return receptor;
}
} // namespace MMPI
// isendRecvMessages function:1 ends here

// [[file:../../../org/composyx/dist/MPI.org::*Data serialization][Data serialization:1]]
namespace MMPI {
template <class TupleType>
inline std::vector<char> serialize(const std::vector<TupleType>& data) {
  int total_size = sizeof(TupleType) * data.size();
  std::vector<char> serialized(total_size);
  std::memset(serialized.data(), 0, total_size);
  std::memcpy(static_cast<void*>(serialized.data()), data.data(), total_size);
  return serialized;
}

template <class TupleType>
inline std::vector<TupleType> deserialize(const std::vector<char>& serialized) {
  int nb_elems = serialized.size() / sizeof(TupleType);
  std::vector<TupleType> deserialized(nb_elems);
  std::memcpy(static_cast<void*>(deserialized.data()), serialized.data(),
              serialized.size());
  return deserialized;
}
} // namespace MMPI
// Data serialization:1 ends here

// [[file:../../../org/composyx/dist/MPI.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
