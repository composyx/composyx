// [[file:../../../org/composyx/dist/Subdomain.org::*Header][Header:1]]
#pragma once

#include <algorithm>
#include <iomanip>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "composyx/IO/DomainDecompositionIO.hpp"
#include "composyx/dist/MPI.hpp"
#include "composyx/utils/Arithmetic.hpp"
#include "composyx/utils/CrossProduct.hpp"
#include "composyx/utils/Error.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/dist/Subdomain.org::*Enumeration of reduction operators][Enumeration of reduction operators:1]]
enum class Reduction {
  twoways_sum,
  owner_value,
};
// Enumeration of reduction operators:1 ends here

// [[file:../../../org/composyx/dist/Subdomain.org::*Attributes][Attributes:1]]
class Subdomain {
private:
  int _id = -1;
  bool _on_intrf;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Attributes][Attributes:2]]
  int _n_loc;
  int _n_glob = -1;
  // Attributes:2 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Attributes][Attributes:3]]
  std::vector<bool> _part_unity;
  // Attributes:3 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Attributes][Attributes:4]]
  using Nei_map = std::map<int, std::vector<int>>;
  Nei_map _nei2dof;
  std::vector<int> _interface;
  // Attributes:4 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Attributes][Attributes:5]]
  std::vector<int> _global_indices;
  // Attributes:5 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Attributes][Attributes:6]]
  std::map<int, int> _nei2tag;
  // Attributes:6 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Constructors][Constructors:1]]
public:
  Subdomain(const int subdomain_id, const int n_loc, Nei_map&& nei2dof,
            const bool on_intrf = false)
      : _id{subdomain_id}, _on_intrf{on_intrf}, _n_loc{n_loc},
        _nei2dof{std::move(nei2dof)} {
    compute_part_unity(on_intrf);
  }

  Subdomain(const int subdomain_id, const int n_loc, const Nei_map& nei2dof,
            const bool on_intrf = false)
      : _id{subdomain_id}, _on_intrf{on_intrf}, _n_loc{n_loc},
        _nei2dof{nei2dof} {
    compute_part_unity(on_intrf);
  }
  // Constructors:1 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Loading a domain][Loading a domain:1]]
  static std::pair<Subdomain, Subdomain>
  load_subdomain(const int id, const std::string& filename) {
    Nei_map nei2dof;
    int n_tot, n_g;
    read_subdomain(filename, n_tot, n_g, nei2dof, id);

    Subdomain sd_full(id, n_tot, nei2dof, false);
    Subdomain sd_intrf(id, n_g, nei2dof, true);
    return std::make_pair(sd_full, sd_intrf);
  }
  // Loading a domain:1 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Dumping a subdomain][Dumping a subdomain:1]]
  void dump(const std::string& filename, bool mppformat = true) const {
    if (mppformat) {
      write_subdomain_mpp(filename, _n_loc, _nei2dof);
    } else {
      write_subdomain_mf(filename, _n_loc, static_cast<int>(_interface.size()),
                         _nei2dof, _interface, _global_indices);
    }
  }
  // Dumping a subdomain:1 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Compute partition of unity and interface nodes][Compute partition of unity and interface nodes:1]]
  void compute_part_unity(const bool on_intrf) {
    for (const auto& neimap : _nei2dof) {
      const std::vector<int>& nei_intrf = neimap.second;
      union_with(_interface, nei_intrf);
    }

    if (on_intrf) {
      Nei_map nei2intrf_dof;
      for (const auto& neimap : _nei2dof) {
        const int nei = neimap.first;
        const std::vector<int>& nei_intrf = neimap.second;
        searchsorted(_interface, nei_intrf, nei2intrf_dof[nei]);
      }

      _nei2dof = std::move(nei2intrf_dof);
    }

    _part_unity = std::vector<bool>(_n_loc, 1);

    for (const auto& [nei, nei_intrf] : _nei2dof) {
      if (nei < _id) {
        for (size_t k = 0; k < nei_intrf.size(); ++k) {
          _part_unity[nei_intrf[k]] = false;
        }
      }
    }
  }
  // Compute partition of unity and interface nodes:1 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Assemble global indices][Assemble global indices:1]]
  MPI_Receptor<int> compute_global_idx_send(
      const std::vector<int>& sd2rank, const std::vector<int>& n_per_subdomain,
      std::vector<std::vector<int>>& send_buffers,
      const std::map<int, int>& nei2tag, const MPI_Comm comm) {
    _n_glob =
        std::accumulate(n_per_subdomain.begin(), n_per_subdomain.end(), 0);
    int offset = 0;
    for (auto i = 0; i < _id; ++i) {
      offset += n_per_subdomain[i];
    }

    _global_indices = std::vector<int>(_n_loc, 0);
    int j = offset;
    for (auto i = 0; i < _n_loc; ++i) {
      if (_part_unity[i]) {
        _global_indices[i] = j;
        j++;
      }
    }

    _nei2tag = nei2tag;
    return neighbor_send<int>(sd2rank, _global_indices, send_buffers,
                              Reduction::owner_value, comm);
  }

  void compute_global_idx_recv(const std::vector<int>& received,
                               const int nei) {
    const std::vector<int>& nei_intrf = _nei2dof[nei];
    for (size_t k = 0; k < nei_intrf.size(); ++k)
      _global_indices[nei_intrf[k]] += received[k];
  }
  // Assemble global indices:1 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Reduce operations][Reduce operations:1]]
  // Set values not owned to 0
  template <class T> void disassemble(std::vector<T>& local_values) const {
    for (int i = 0; i < _n_loc; ++i) {
      if (!_part_unity[i]) {
        local_values[i] = 0;
      }
    }
  }

  template <class Array> void disassemble(Array& local_values) const {
    for (size_t j = 0; j < n_cols(local_values); ++j) {
      for (int i = 0; i < _n_loc; ++i) {
        if (!_part_unity[i]) {
          local_values(i, j) = 0;
        }
      }
    }
  }
  // Reduce operations:1 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Reduce operations][Reduce operations:2]]
private:
  // 2D send buffer
  // Assume matrix elements can be accessed with matrix(i, j) (dense matrix)
  template <class T, class M, bool has_vector_comm_pattern = false>
  std::vector<T> _sendbuf_2D(const M& local_buf, const std::vector<int>& idx,
                             int exp_size) const {
    std::vector<int> idy;
    int exp_sizey = 0;
    if constexpr (has_vector_comm_pattern) {
      exp_sizey = n_cols(local_buf);
      idy = arange<std::vector<int>>(exp_sizey);
    } else {
      idy = idx;
      exp_sizey = exp_size;
    }

    COMPOSYX_ASSERT(static_cast<int>(n_rows(local_buf) * n_cols(local_buf)) ==
                        exp_size * exp_sizey,
                    "Subdomain assemble (2D) : wrong buffer size");
    CrossProduct cp(idx, idy);
    std::vector<T> sendbuf(cp.size());
    int k = 0;
    for (auto pair : cp) {
      sendbuf[k++] = local_buf(pair.first, pair.second);
    }
    return sendbuf;
  }

  // 1D send buffer
  template <class T>
  std::vector<T> _sendbuf_1D(const std::vector<T>& local_buf,
                             const std::vector<int>& idx, int exp_size) const {
    COMPOSYX_DIM_ASSERT(local_buf.size(), exp_size,
                        "Subdomain assemble (1D) : wrong buffer size");
    return subarray(local_buf, idx);
  }

  // Get indices to send (interface only or whole subdomain) returns 1D or 2D
  // send buffer corresponding.
  template <class T, class M, bool has_vector_comm_pattern = false>
  std::vector<T> _compute_send_buffer(const M& local_buf, const int nei) const {
    const std::vector<int>& indices = _nei2dof.at(nei);

    if constexpr (std::is_same<std::vector<T>, M>::value) {
      return _sendbuf_1D<T>(local_buf, indices, _n_loc);
    } else {
      return _sendbuf_2D<T, M, has_vector_comm_pattern>(local_buf, indices,
                                                        _n_loc);
    }
  }

public:
  template <class T, class M, bool has_vector_comm_pattern = false>
  MPI_Receptor<T>
  neighbor_send(const std::vector<int>& sd2rank, M& local_values,
                std::vector<std::vector<T>>& send_buffers,
                const Reduction reduction, const MPI_Comm comm) const {
    if (reduction == Reduction::owner_value) {
      disassemble(local_values);
    }

    int n_recv = 0;
    std::vector<Message<T>> messages;
    for (const auto& neimap : _nei2dof) {
      const int nei = neimap.first;
      // For owner_value reduction,
      // only the processes with higher rank send, the others receive
      if ((reduction == Reduction::owner_value) && (_id > nei)) {
        n_recv++;
        continue;
      }
      send_buffers.emplace_back(
          _compute_send_buffer<T, M, has_vector_comm_pattern>(local_values,
                                                              nei));

      std::vector<T>& sb = send_buffers.back();
      // std::cout << "display sb from " << _id << " to " << nei << ' ';
      // sb.display();
      messages.emplace_back(sb.data(), static_cast<int>(sb.size()),
                            sd2rank[nei], _nei2tag.at(nei));
      if (reduction != Reduction::owner_value)
        n_recv++;
    }
    return MMPI::isendMessages(messages, n_recv, comm);
  }

  // 2D reception
  template <class T, class M, bool has_vector_comm_pattern = false>
  void neighbor_recv(const std::vector<T>& received, M& matrix, const int nei,
                     MatrixStorage storage = MatrixStorage::full) const {
    const std::vector<int>& nei_intrf = _nei2dof.at(nei);
    std::vector<int> nei_intrf_y;
    if constexpr (has_vector_comm_pattern) {
      nei_intrf_y = arange<std::vector<int>>(static_cast<int>(n_cols(matrix)));
    } else {
      nei_intrf_y = nei_intrf;
    }

    CrossProduct cp(nei_intrf, nei_intrf_y);
    int k = 0;

    for (auto pair : cp) {
      if (storage == MatrixStorage::upper && pair.first > pair.second) {
        matrix(pair.second, pair.first) += received[k++];
      } else if (storage == MatrixStorage::lower && pair.first < pair.second) {
        matrix(pair.second, pair.first) += received[k++];
      } else {
        matrix(pair.first, pair.second) += received[k++];
      }
    }
  }

  // 1D reception
  template <class T, class M = std::vector<T>,
            bool has_vector_comm_pattern = false>
  void neighbor_recv(const std::vector<T>& received,
                     std::vector<T>& local_values, const int nei,
                     MatrixStorage) const {
    const std::vector<int>& nei_intrf = _nei2dof.at(nei);
    // std::cout << "display received in " << _id << " for " <<' ';
    // nei_intrf.display();
    // std::cout << "received display ";
    // received.display();
    for (size_t k = 0; k < nei_intrf.size(); ++k)
      local_values[nei_intrf[k]] += received[k];
  }
  // Reduce operations:2 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Reduce operations][Reduce operations:3]]
  template <class M>
  MPI_Receptor<char>
  neighbor_send_sparse(const std::vector<int>& sd2rank, M& local_values,
                       std::vector<std::vector<char>>& send_buffers,
                       const Reduction reduction, const MPI_Comm comm) const {
    using Scalar = typename scalar_type<M>::type;
    using Index = int;
    using Triplet = std::tuple<Index, Index, Scalar>;

    // For owner_value reduction, we set values not owned to 0
    COMPOSYX_ASSERT(
        reduction != Reduction::owner_value,
        "Reduction::owner_value not implemented for sparse matrices");

    int n_recv = 0;
    std::vector<Message<char>> messages;
    for (const auto& neimap : _nei2dof) {
      const int nei = neimap.first;
      // For owner_value reduction,
      // only the processes with higher rank send, the others receive
      if ((reduction == Reduction::owner_value) && (_id > nei)) {
        n_recv++;
        continue;
      }

      const std::vector<int>& indices = _nei2dof.at(nei);

      // First cross product with the local i and j
      CrossProduct cp(indices, indices);

      // Second cross product: the indices of i and j in the "indices" array
      std::vector<int> range =
          arange<std::vector<int>>(static_cast<int>(indices.size()));
      CrossProduct cp_range(range, range);

      std::vector<Triplet> to_send(n_nonzero(local_values));
      int k = 0;
      for (auto [i, j] : cp) {
        Scalar v = local_values.coeff(i, j);
        if (v != Scalar{0}) {
          auto [idx_i, idx_j] = *cp_range;
          to_send[k++] = std::make_tuple(idx_i, idx_j, v);
        }
        ++cp_range;
      }
      to_send.resize(k);

      send_buffers.emplace_back(MMPI::serialize(to_send));
      std::vector<char>& sb = send_buffers.back();

      messages.emplace_back(sb.data(), static_cast<int>(sb.size()),
                            sd2rank[nei], _nei2tag.at(nei));
      if (reduction != Reduction::owner_value)
        n_recv++;
    }

    return MMPI::isendMessages(messages, n_recv, comm);
  }

  template <class M>
  void neighbor_recv_sparse(const std::vector<char>& received, M& matrix,
                            const int nei,
                            MatrixStorage storage = MatrixStorage::full) const {
    using Scalar = typename scalar_type<M>::type;
    using Index = int;
    using Triplet = std::tuple<Index, Index, Scalar>;

    std::vector<Triplet> tri_received = MMPI::deserialize<Triplet>(received);

    const std::vector<int>& indices = _nei2dof.at(nei);

    std::vector<Index> ri(tri_received.size());
    std::vector<Index> rj(tri_received.size());
    std::vector<Scalar> rv(tri_received.size());
    int k = 0;
    for (const auto& [i, j, v] : tri_received) {
      if (storage == MatrixStorage::upper && indices[i] > indices[j])
        continue;
      if (storage == MatrixStorage::lower && indices[i] < indices[j])
        continue;
      ri[k] = indices[i];
      rj[k] = indices[j];
      rv[k] = v;
      ++k;
    }
    ri.resize(k);
    rj.resize(k);
    rv.resize(k);

    M received_mat;
    build_matrix(received_mat, n_rows(matrix), n_cols(matrix), k, ri.data(),
                 rj.data(), rv.data());
    received_mat.set_property(storage);
    matrix += received_mat;
  }
  // Reduce operations:3 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Local to global indexing][Local to global indexing:1]]
  template <class IntArray> void local_to_global(IntArray& array) {
    for (int i = 0; i < static_cast<int>(array.size()); ++i) {
      array[i] = _global_indices[array[i]];
    }
  }

  template <class IntArray> void global_to_local(IntArray& array) {
    const int not_found = static_cast<int>(array.size());
    for (int i = 0; i < static_cast<int>(array.size()); ++i) {
      // Search index in sorted array
      int loc_idx = std::distance(
          _global_indices.begin(),
          std::find(_global_indices.begin(), _global_indices.end(), array[i]));
      if (loc_idx != not_found) {
        array[i] = loc_idx;
      } else {
        array[i] = -1;
      }
    }
  }
  // Local to global indexing:1 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Check consistency][Check consistency:1]]
  bool check_global_indices(const int n_glob,
                            std::ostream& out = std::cerr) const {
    // Check we do not have duplicates
    std::vector<int> unique_glob_idx = _global_indices;
    unique(unique_glob_idx);
    if (unique_glob_idx.size() != _global_indices.size()) {
      out << "SD " << _id << ": Global index contains duplicates" << '\n';
      // out << _id << " : [ ";
      // for(auto& g : _global_indices){
      //   out << g << ", ";
      // }
      // out << "]" << '\n';
      return false;
    }

    auto out_of_range = [n_glob](const int& i) {
      return (i < 0 || i >= n_glob);
    };

    using std::ranges::views::filter;
    auto err_pts = filter(_global_indices, out_of_range);

    for (auto ep : err_pts) {
      out << "global_index array contains values out of range ( negative or > "
             "n_glob = "
          << n_glob << ")" << '\n';
      out << ep << '\n';
      return false;
    }

    return true;
  }
  // Check consistency:1 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Getters][Getters:1]]
  [[nodiscard]] int get_n_responsible() const {
    int n_resp = std::ranges::count(_part_unity, true);
    return n_resp;
  }

  [[nodiscard]] inline int get_id() const { return _id; }
  [[nodiscard]] inline int get_n_loc() const { return _n_loc; }
  [[nodiscard]] inline int get_n_glob() const { return _n_glob; }
  [[nodiscard]] inline int* get_glob_idx_ptr() {
    return _global_indices.data();
  }

  [[nodiscard]] std::vector<int> get_neighbors() const {
    std::vector<int> neighbors;
    for (const auto& n : _nei2dof) {
      neighbors.push_back(n.first);
    }
    return neighbors;
  }

  template <class T>
  [[nodiscard]] T sum(const std::vector<T>& local_data) const {
    auto filtered_data = local_data[_part_unity];
    return filtered_data.sum();
  }

  [[nodiscard]] inline std::vector<int> get_global_indices() const {
    return _global_indices;
  }

  [[nodiscard]] inline std::vector<int>
  get_interface_with(const int sd_id) const {
    return _nei2dof.at(sd_id);
  }

  [[nodiscard]] inline const std::vector<int>& get_interface() const {
    return _interface;
  }

  [[nodiscard]] const Nei_map& get_nei2dof() const { return _nei2dof; }
  // Getters:1 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Print function (for debugging)][Print function (for debugging):1]]
  void display(const std::string& name = "",
               std::ostream& out = std::cout) const {
    if (!name.empty())
      out << "--------- " << name << '\n';

    out << "--------- Subdomain ID: " << _id << '\n';
    out << "On interface: " << _on_intrf << '\n';
    out << "ndofs: local / global " << _n_loc << " / " << _n_glob << '\n';

    out << "Partition of unity: " << '\n';
    for (auto p : _part_unity)
      out << p;
    out << "\n\n";

    out << "Neighbor map: " << '\n';
    for (auto& v : _nei2dof) {
      out << " - " << v.first << " : [";
      auto idx_tab = v.second;
      for (auto& k : idx_tab) {
        out << k << " ";
      }
      out << "]" << '\n';
    }
    out << '\n';

    out << "Global indices:" << '\n' << "[";
    for (auto gi : _global_indices) {
      out << gi << ", ";
    }
    out << "]" << '\n';
  }

  friend std::ostream& operator<<(std::ostream& out, const Subdomain& s) {
    s.display("", out);
    return out;
  }
  // Print function (for debugging):1 ends here

  // [[file:../../../org/composyx/dist/Subdomain.org::*Footer][Footer:1]]
}; // class Subdomain

} // namespace composyx
// Footer:1 ends here
