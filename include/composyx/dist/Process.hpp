// [[file:../../../org/composyx/dist/Process.org::*Header][Header:1]]
#pragma once

#include <array>
#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <thread>
#include <mutex>

namespace composyx {
class Process;
}

#include "composyx/dist/MPI.hpp"
#include "composyx/dist/Subdomain.hpp"
#include "composyx/interfaces/linalg_concepts.hpp"
#include "composyx/loc_data/DenseMatrix.hpp"
#include "composyx/utils/Error.hpp"
#include "composyx/utils/ArrayAlgo.hpp"

namespace composyx {

using Subdomain_map =
    std::vector<std::pair<std::vector<int>,
                          int>>; // number of subdomains is the size of the
                                 // map, thus map between sd -> pair(vector of
                                 // procs, master proc) for the sd, the vector
                                 // of procs may contain the procs that do
                                 // nothing (workers)
using Thread_map = std::vector<std::pair<std::thread, std::vector<int>>>;
// Header:1 ends here

// [[file:../../../org/composyx/dist/Process.org::*Attributes][Attributes:1]]
class Process {
private:
  int _rank;
  int _rank_master;
  MPI_Comm _init_comm = MPI_COMM_NULL;
  Subdomain_map _map;
  std::mutex _proc_map_mutex;
  std::map<int, std::pair<Subdomain, Subdomain>>
      _subdomains; // Full subdomain + subdomain interface
  std::map<std::pair<int, int>, std::pair<int, int>> _tag2sd;
  std::map<std::pair<int, int>, std::pair<int, int>> _sd2tag;
  int _tag_intrf_offset;
  MPI_Comm _worker_comm = MPI_COMM_NULL;
  MPI_Comm _master_comm = MPI_COMM_NULL;
  int _master;
  std::vector<int> _sd2rank; // Subdomain to master rank vector, the size of
  // vector is the number of subdomains, thus map
  // between sd -> num proc (not used yet?)
  std::vector<int> _sd2rank_master; // Subdomain to master rank vector
  std::vector<int> _workers;        // worker ranks
  bool _interface_only = false;     // Process defined only on the interface
  unsigned int _n_threads;          // number of threads used for shared memory
  mutable Thread_map _thread2sds;
  // Attributes:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Constructor][Constructor:1]]
public:
  Process(MPI_Comm initial_comm, Subdomain_map&& map, const int master,
          const std::vector<int>& sd2rank, const std::vector<int>& master_ranks,
          const std::vector<int>& worker_ranks,
          const unsigned int n_threads = 1)
      : _master{master}, _sd2rank{sd2rank}, _workers{worker_ranks},
        _n_threads{n_threads} {
    _rank = MMPI::rank(initial_comm);
    _init_comm = MMPI::comm_dup(initial_comm);
    _map = std::move(map);

    int size = MMPI::size(initial_comm);

    // Bind subdomains to threads
    int id_sd = 0;

    if (_n_threads > std::thread::hardware_concurrency()) {
      COMPOSYX_WARNING(
          "Input _n_threads exceeds the maximum number of concurrent threads "
          "std::thread::hardware_concurrency().");
    }

    if (_n_threads > 1 && MMPI::get_provided() != MPI_THREAD_MULTIPLE) {
      COMPOSYX_WARNING(
          "Using multiple threads per MPI process but MPI_THREAD_MULTIPLE is "
          "not enabled.");
    }

    std::map<int, std::vector<int>>
        rank2sds; // reversed map used to compute sd2threads
    std::vector<int> _thread2n_sds;
    for (const auto& p : _map)
      rank2sds[p.second].push_back(id_sd++);

    for (const auto& [rank, sds] : rank2sds) {
      if (rank != _rank)
        continue;
      if (sds.size() <= _n_threads) {
        for (const auto& sd_id : sds) {
          _thread2n_sds.push_back(1);
          _thread2sds.emplace_back(std::thread(), std::vector(1, sd_id));
        }
      } else {
        const int sds_per_thread = sds.size() / _n_threads;
        const int sds_per_thread_r = sds.size() % _n_threads;
        _thread2n_sds.resize(
            _n_threads,
            sds_per_thread); // number of subdomains for each thread
        if (sds_per_thread_r != 0) {
          for (int i = 0; i < sds_per_thread_r; i++)
            _thread2n_sds[i]++;
        }
        int k = 0; // index for sds (value of map rank2sds)

        for (unsigned int i = 0; i < _n_threads; i++) {
          _thread2sds.emplace_back(std::thread(),
                                   std::vector(_thread2n_sds[i], 0));
          for (int j = 0; j < _thread2n_sds[i]; j++) {
            _thread2sds[i].second[j] = sds[k++];
          }
        }
      }
    }

    // Create initial comm group
    MPI_Group init_group;
    MPI_Comm_group(initial_comm, &init_group);

    if (_master == _rank) {
      // Create master group
      MPI_Group master_group;
      MPI_Group_incl(init_group, static_cast<int>(master_ranks.size()),
                     master_ranks.data(), &master_group);

      // Create master comm
      int master_tag = size + 1;
      MPI_Comm_create_group(initial_comm, master_group, master_tag,
                            &_master_comm);

      _rank_master = MMPI::rank(_master_comm);
      MPI_Group_free(&master_group);
    }

    if (worker_ranks.size() > 0) {
      // Create worker group
      MPI_Group worker_group;
      MPI_Group_incl(init_group, static_cast<int>(worker_ranks.size()),
                     worker_ranks.data(), &worker_group);

      // Create worker comm
      int worker_tag = _master; // Rank of the master as tag
      MPI_Comm_create_group(initial_comm, worker_group, worker_tag,
                            &_worker_comm);
      MPI_Group_free(&worker_group);
    } else {
      _worker_comm = MMPI::comm_dup(MPI_COMM_SELF);
    }
    MPI_Group_free(&init_group);
  }
  // Constructor:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Destructor to free communicators][Destructor to free communicators:1]]
  ~Process() {
    if (_init_comm != MPI_COMM_NULL)
      MPI_Comm_free(&_init_comm);
    if (_worker_comm != MPI_COMM_NULL)
      MPI_Comm_free(&_worker_comm);
    if (_master_comm != MPI_COMM_NULL)
      MPI_Comm_free(&_master_comm);
  }
  // Destructor to free communicators:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Load subdomains][Load subdomains:1]]
  void load_subdomains(std::string path_to_part) {
    if (_master != _rank)
      return;
    const std::vector<int> sd_ids = get_sd_ids();

    if (!(path_to_part.empty()) && path_to_part.back() != '/') {
      path_to_part += '/';
    }

    auto check_file_exists = [](const std::string& fname) {
      std::ifstream f(fname.c_str());
      return f.good();
    };

    bool composyx_format;
    if (check_file_exists(path_to_part +
                          std::string("composyx_local_domain0.yaml"))) {
      composyx_format = true;
    } else if (check_file_exists(path_to_part +
                                 std::string("maphys_local_domain1.dom"))) {
      composyx_format = false;
    } else {
      std::string strerror =
          std::string("Process::load_subdomains: could not find any of files: "
                      "'composyx_local_domain0.yaml' or "
                      "'maphys_local_domain1.dom' in ") +
          path_to_part;
      COMPOSYX_ASSERT(false, strerror);
    }

    for (const auto& sd_id : sd_ids) {
      std::string fname_domain = path_to_part;
      if (composyx_format) {
        fname_domain +=
            "composyx_local_domain" + std::to_string(sd_id) + ".yaml";
      } else {
        fname_domain +=
            "maphys_local_domain" + std::to_string(sd_id + 1) + ".dom";
      }
      std::pair<Subdomain, Subdomain> sds(
          Subdomain::load_subdomain(sd_id, fname_domain));
      _subdomains.emplace(sd_id, std::move(sds));
    }
    assemble_subdomains();
  }

  void load_baton_subdomains(std::string path_to_part) {
    if (_master != _rank)
      return;
    const std::vector<int> sd_ids = get_sd_ids();
    int n_subdomains = this->get_n_sd_total();

    if (!(path_to_part.empty()) && path_to_part.back() != '/')
      path_to_part += '/';

    for (const auto& sd_id : sd_ids) {
      std::string piece_to_load("middle");
      if (sd_id == 0)
        piece_to_load = std::string("first");
      if (sd_id == n_subdomains - 1)
        piece_to_load = std::string("last");

      const std::string fname_domain = path_to_part +
                                       std::string("baton_domain_") +
                                       piece_to_load + std::string(".yaml");

      std::pair<Subdomain, Subdomain> sds(
          Subdomain::load_subdomain(sd_id, fname_domain));
      _subdomains.emplace(sd_id, std::move(sds));
    }
    assemble_subdomains();
  }

  void load_subdomains(const std::vector<Subdomain>& subdomains) {
    if (_master != _rank)
      return;
    _subdomains.clear();
    const std::vector<int> sd_ids = get_sd_ids();
    int n_found = 0;
    _interface_only = false;

    std::vector<Subdomain> subdomains_intrf;
    for (const auto& sd : subdomains) {
      int n_g = static_cast<int>(sd.get_interface().size());
      const auto n2d = sd.get_nei2dof();
      subdomains_intrf.emplace_back(sd.get_id(), n_g, n2d, true);
    }

    for (const int sd_id : sd_ids) {
      for (size_t k = 0; k < subdomains.size(); ++k) {
        const auto& sd = subdomains[k];
        if (sd.get_id() == sd_id) {
          const auto& sd_i = subdomains_intrf[k];
          _subdomains.emplace(sd_id, std::make_pair(sd, sd_i));
          n_found++;
          break;
        }
      }
    }

    COMPOSYX_DIM_ASSERT(n_found, static_cast<int>(sd_ids.size()),
                        "Loading subdomains: could not find every subdomains "
                        "in the provided subdomains");

    assemble_subdomains();
  }

  void
  load_subdomains_interface(const std::vector<Subdomain>& subdomains_intrf) {
    if (_master != _rank)
      return;
    _subdomains.clear();
    const std::vector<int> sd_ids = get_sd_ids();
    int n_found = 0;
    _interface_only = true;

    for (const int sd_id : sd_ids) {
      for (size_t k = 0; k < subdomains_intrf.size(); ++k) {
        const auto& sd_i = subdomains_intrf[k];

        if (sd_i.get_id() == sd_id) {
          const Subdomain dummy_sd(sd_id, 0, std::map<int, std::vector<int>>(),
                                   false);
          _subdomains.emplace(sd_id, std::make_pair(dummy_sd, sd_i));
          n_found++;
          break;
        }
      }
    }

    COMPOSYX_DIM_ASSERT(n_found, static_cast<int>(sd_ids.size()),
                        "Loading subdomains: could not find every subdomains "
                        "in the provided subdomains");
    assemble_subdomains();
  }

  void load_subdomain(const Subdomain& subdomain) {
    std::vector<Subdomain> vs{subdomain};
    load_subdomains(vs);
  }

  void load_subdomain_interface(const Subdomain& subdomain) {
    std::vector<Subdomain> vs{subdomain};
    load_subdomains_interface(vs);
  }
  // Load subdomains:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Computing and getting MPI tags][Computing and getting MPI tags:1]]
private:
  void compute_tags() {
    int current_tag = 1;
    std::vector<std::vector<int>> send_buffers;
    std::vector<Message<int>> smess;

    for (const auto& my_sds : _subdomains) {
      const int sd_local = my_sds.first;
      // We take neighbors from the interface sd in case the process is only
      // defined on the interface
      const std::vector<int> neighbors = my_sds.second.second.get_neighbors();
      for (const int& sd_nei : neighbors) {
        const int nei_proc = _sd2rank_master.at(sd_nei);
        // For sending, we set the tag to be sent and fill SD -> TAG map
        send_buffers.emplace_back(
            std::vector<int>({sd_nei, sd_local, _rank_master, current_tag}));
        smess.emplace_back(send_buffers.back().data(), 4, nei_proc);
        std::pair<int, int> dest_tag(nei_proc, current_tag);
        std::pair<int, int> sd_local_nei(sd_local, sd_nei);
        _sd2tag.emplace(std::make_pair(sd_local_nei, dest_tag));
        current_tag++;
      }
    }

    auto receptor = MMPI::isendMessages(
        smess, static_cast<int>(send_buffers.size()), _master_comm);

    // For receiving, we receive the tag to be received and fill TAG -> SD map
    while (receptor.get_n_recv()) {
      int source, tag;
      std::vector<int> rbuf = receptor(source, tag);
      std::pair<int, int> sd_local_nei(rbuf[0], rbuf[1]);
      std::pair<int, int> source_tag(rbuf[2], rbuf[3]);
      _tag2sd.emplace(std::make_pair(source_tag, sd_local_nei));
    }

    // For the interface subdomains, we use the same tag + on offet
    MMPI::allreduce(&current_tag, &_tag_intrf_offset, 1, MPI_SUM, _master_comm);
    MMPI::barrier(_master_comm);
  }

  void tag_from_sd_id(const int sd_local, const int sd_neighbor, int& dest,
                      int& tag, const bool on_intrf) const {
    // ( sd local, sd neighbor ) -> ( destination proc, tag)
    const std::pair<int, int> sd_loc_nei(sd_local, sd_neighbor);
    const std::pair<int, int>& dest_tag = _sd2tag.at(sd_loc_nei);
    dest = dest_tag.first;
    tag = dest_tag.second;
    if (on_intrf)
      tag += _tag_intrf_offset;
  }

  void sd_id_from_tag(const int source, const int tag, int& sd_local,
                      int& sd_neighbor, bool& on_intrf) const {
    // ( source proc, tag_recv) -> ( sd local, sd neighbor )
    int check_tag = tag;
    on_intrf = false;
    if (tag >= _tag_intrf_offset) {
      on_intrf = true;
      check_tag = tag - _tag_intrf_offset;
    }
    const std::pair<int, int> source_tag(source, check_tag);
    if (_tag2sd.count(source_tag)) {
      const std::pair<int, int>& subdoms = _tag2sd.at(source_tag);
      sd_local = subdoms.first;
      sd_neighbor = subdoms.second;
    } else {
      sd_local = -1;
      sd_neighbor = -1;
    }
  }
  // Computing and getting MPI tags:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Assemble subdomains][Assemble subdomains:1]]
private:
  void assemble_subdomains() {
    if (_master != _rank)
      return;

    // Translate sd2rank to set ranks of the master communicator
    _sd2rank_master = std::vector<int>(_sd2rank.size(), 0);
    int k = 0;
    for (int i = 1; i < static_cast<int>(_sd2rank.size()); ++i) {
      if (_sd2rank[i] != _sd2rank[i - 1])
        k++;
      _sd2rank_master[i] = k;
    }

    // Debug
    /*
	if (_rank == 0) {
	std::cout << "_sd2rank_master: ";
	for (auto i: _sd2rank_master) std::cout << i << ' ';
	std::cout << '\n';
	} */

    compute_tags();

    // We want to allgather for each subdomain the number of
    // dof it is responsible for
    const int n_masters = MMPI::size(_master_comm);
    std::vector<int> recvcounts(n_masters, 0);
    for (auto m_rank : _sd2rank_master) {
      recvcounts[m_rank]++;
    }

    std::vector<int> send_n_sd(_subdomains.size(), 0);
    std::vector<int> send_n_intrf_sd(_subdomains.size(), 0);
    k = 0;
    for (auto& my_sds : _subdomains) {
      send_n_sd[k] = my_sds.second.first.get_n_responsible();
      send_n_intrf_sd[k] = my_sds.second.second.get_n_responsible();
      k++;
    }

    std::vector<int> n_per_subdomain =
        std::vector(subdomain_allgather(send_n_sd, recvcounts));
    std::vector<int> n_per_subdomain_intrf =
        std::vector(subdomain_allgather(send_n_intrf_sd, recvcounts));
    std::vector<std::vector<int>> send_buffers;
    std::vector<MPI_Receptor<int>> receptors;

    for (auto& my_sds : _subdomains) {
      const int sd_id = my_sds.first;
      Subdomain& my_sd = my_sds.second.first;
      Subdomain& my_sd_intrf = my_sds.second.second;
      std::map<int, int> nei2tag;
      std::map<int, int> nei2tag_intrf;
      // We take neighbors from the interface sd in case the process is only
      // defined on the interface
      const std::vector<int> neighbors = my_sd_intrf.get_neighbors();
      for (auto& sd_nei : neighbors) {
        int dest, tag;
        if (!_interface_only) {
          tag_from_sd_id(sd_id, sd_nei, dest, tag, /*on_intrf*/ false);
          nei2tag[sd_nei] = tag;
        }
        tag_from_sd_id(sd_id, sd_nei, dest, tag, /*on_intrf*/ true);
        nei2tag_intrf[sd_nei] = tag;
      }

      if (!_interface_only)
        receptors.emplace_back(
            my_sd.compute_global_idx_send(_sd2rank_master, n_per_subdomain,
                                          send_buffers, nei2tag, _master_comm));
      receptors.emplace_back(my_sd_intrf.compute_global_idx_send(
          _sd2rank_master, n_per_subdomain_intrf, send_buffers, nei2tag_intrf,
          _master_comm));
    }

    for (auto& receptor : receptors) {
      int source = -1, tag = -1;
      while (receptor.get_n_recv()) {
        std::vector<int> received(receptor(source, tag));
        int sd_local, sd_nei;
        bool on_intrf;
        sd_id_from_tag(source, tag, sd_local, sd_nei, on_intrf);
        Subdomain& sd = (on_intrf) ? _subdomains.at(sd_local).second
                                   : _subdomains.at(sd_local).first;
        sd.compute_global_idx_recv(received, sd_nei);
      }
    }

    for (auto& receptor : receptors) {
      receptor.wait();
    }
    MMPI::barrier(_master_comm);
  }
  // Assemble subdomains:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Disassembly, local to global operations][Disassembly, local to global operations:1]]
public:
  template <class ValueArray>
  void disassemble(std::map<int, ValueArray>& local_data,
                   bool on_intrf = false) {
    if (_interface_only)
      on_intrf = true;
    for (auto& loc_d : local_data) {
      get_subdomain(loc_d.first, on_intrf).disassemble(loc_d.second);
    }
  }

  template <class IntArray>
  void local_to_global(std::map<int, IntArray>& local_data,
                       bool on_intrf = false) {
    if (_interface_only)
      on_intrf = true;
    for (auto& loc_d : local_data) {
      get_subdomain(loc_d.first, on_intrf).local_to_global(loc_d.second);
    }
  }

  template <class IntArray>
  void local_to_global(const int sd_id, IntArray& local_data,
                       bool on_intrf = false) {
    if (_interface_only)
      on_intrf = true;
    get_subdomain(sd_id, on_intrf).local_to_global(local_data);
  }

  template <class IntArray>
  void global_to_local(std::map<int, IntArray>& local_data,
                       bool on_intrf = false) {
    if (_interface_only)
      on_intrf = true;
    for (auto& loc_d : local_data) {
      get_subdomain(loc_d.first, on_intrf).global_to_local(loc_d.second);
    }
  }

  template <class IntArray>
  void global_to_local(const int sd_id, IntArray& local_data,
                       bool on_intrf = false) {
    if (_interface_only)
      on_intrf = true;
    get_subdomain(sd_id, on_intrf).global_to_local(local_data);
  }
  // Disassembly, local to global operations:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Change domain decomposition][Change domain decomposition:1]]
  void set_domain_decomposition(Subdomain_map&& map) {
    _map = std::move(map);
    assemble_subdomains();
  }
  // Change domain decomposition:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Check subdomains][Check subdomains:1]]
  bool check_subdomains_part_of_unity() const {
    if (_master != _rank)
      return true;
    std::map<int, std::vector<int>> check_po_unity;
    std::map<int, std::vector<int>> check_po_unity_intrf;

    for (const auto& sd : _subdomains) {
      const int id = sd.first;
      check_po_unity[id] = std::vector<int>(sd.second.first.get_n_loc(), 1);
      check_po_unity_intrf[id] =
          std::vector<int>(sd.second.second.get_n_loc(), 1);
    }
    subdomain_assemble<int>(check_po_unity, false, Reduction::owner_value);
    subdomain_assemble<int>(check_po_unity_intrf, true, Reduction::owner_value);

    for (auto& check : check_po_unity) {
      for (auto i : check.second) {
        if (i != 1) {
          return false;
        }
      }
    }

    return true;
  }
  // Check subdomains:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Display method for debugging][Display method for debugging:1]]
  void display(const std::string& name = "",
               std::ostream& out = std::cout) const {
    if (!name.empty())
      out << "--------- " << name << '\n';
    if (_master == _rank) {
      out << "Process: " << _rank << ", master of [";
      for (auto& w : _workers)
        out << w << ",";
      out << "]" << '\n';
      out << "Owns subdomains [";
      for (auto& s : _subdomains)
        out << s.first << ",";
      out << "]" << '\n';
      display_subdomains("", out);
    } else {
      out << "Process: " << _rank << ", worker of " << _master << '\n';
    }
  }

  friend std::ostream& operator<<(std::ostream& out, const Process& p) {
    p.display("", out);
    return out;
  }

  void display_subdomains(const std::string& name = "",
                          std::ostream& out = std::cout) const {
    if (_master != _rank)
      return;
    if (!name.empty())
      out << "--------- " << name << '\n';
    for (const auto& sd : _subdomains) {
      sd.second.first.display("Full subdomain", out);
      sd.second.second.display("Subdomain interface", out);
    }
  }
  // Display method for debugging:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Display connectivity graph][Display connectivity graph:1]]
  void display_connectivity(const std::string& name = "",
                            std::ostream& out = std::cout) const {
    MPI_Comm comm = _master_comm;
    if (comm == MPI_COMM_NULL) {
      std::cerr << "Process::display_graph: master_comm is MPI_COMM_NULL\n";
      return;
    }

    int rank = MMPI::rank(comm);
    bool is_root = (rank == 0);
    int size = MMPI::size(comm);

    if (is_root) {
      if (!name.empty()) {
        out << "graph " << name << " {\n" << std::flush;
      } else {
        out << "graph subdomain_connectivity {\n" << std::flush;
      }
    }

    for (int k = 0; k < size; ++k) {
      if (k == MMPI::rank()) {
        for (const auto& [sd_id, my_sd] : _subdomains) {
          const std::vector<int> neighbors = my_sd.second.get_neighbors();
          for (const int sd_nei : neighbors) {
            if (sd_nei < sd_id)
              out << "  " << sd_id << " -- " << sd_nei << ";\n";
          }
        }
        out << std::flush;
      }
      MMPI::barrier(comm);
    }

    if (is_root) {
      out << "}\n" << std::flush;
    }
  }
  // Display connectivity graph:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Dump domain decomposition][Dump domain decomposition:1]]
  void dump_subdomains(const std::string& path,
                       bool composyx_format = true) const {
    for (const auto& [sd_id, sds] : _subdomains) {
      std::string filename(path);
      if (!(filename.empty()) && filename.back() != '/')
        filename += '/';

      if (composyx_format) {
        filename += "composyx_local_domain";
        filename += std::to_string(sd_id);
        filename += std::string(".yaml");
      } else {
        filename += "maphys_local_domain";
        filename += std::to_string(sd_id + 1);
        filename += std::string(".dom");
      }

      if (_interface_only) {
        sds.second.dump(filename);
      } else {
        sds.first.dump(filename);
      }
    }
  }
  // Dump domain decomposition:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Getters][Getters:1]]
  [[nodiscard]] inline bool is_master() const { return _master == _rank; }
  [[nodiscard]] inline int get_master() const { return _master; }
  [[nodiscard]] inline MPI_Comm init_comm() { return _init_comm; }
  [[nodiscard]] inline MPI_Comm master_comm() { return _master_comm; }
  [[nodiscard]] inline MPI_Comm worker_comm() { return _worker_comm; }
  [[nodiscard]] inline int get_n_sd_total() const {
    return static_cast<int>(_sd2rank.size());
  }
  [[nodiscard]] inline int get_n_glob(bool on_intrf = false) const {
    if (_interface_only)
      on_intrf = true;
    for (const auto& sds : _subdomains) {
      if (on_intrf)
        return sds.second.second.get_n_glob();
      return sds.second.first.get_n_glob();
    }
    return -1;
  }

  [[nodiscard]] std::vector<int> get_sd_ids() const {
    if (_master != _rank)
      return std::vector<int>();
    std::vector<int> subdomain_ids;
    for (int sd = 0; sd < static_cast<int>(_sd2rank.size()); ++sd) {
      if (_sd2rank[sd] == _rank) {
        subdomain_ids.push_back(sd);
      }
    }
    return subdomain_ids;
  }

  [[nodiscard]] inline std::vector<int> get_sd2rank() const { return _sd2rank; }
  bool owns_subdomain(const int id) const {
    if (_master != _rank)
      return false;

    for (const int sd : get_sd_ids()) {
      if (sd == id)
        return true;
    }
    return false;
  }

  [[nodiscard]] int rank() const { return _rank; }

  [[nodiscard]] int rank_master() const { return _rank_master; }

  [[nodiscard]] inline int get_n_dofs(const int sd_id,
                                      bool on_intrf = false) const {
    if (_interface_only)
      on_intrf = true;
    return on_intrf ? _subdomains.at(sd_id).second.get_n_loc()
                    : _subdomains.at(sd_id).first.get_n_loc();
  }

  [[nodiscard]] std::vector<int> get_sd_neighbors(const int sd_id) const {
    // We take neighbors from the interface sd in case the process is only
    // defined on the interface
    return _subdomains.at(sd_id).second.get_neighbors();
  }

  [[nodiscard]] std::map<int, std::vector<int>>
  get_global_indices(bool on_intrf = false) const {
    if (_interface_only)
      on_intrf = true;
    if (_rank != _master)
      return std::map<int, std::vector<int>>();
    std::map<int, std::vector<int>> glob_idx;
    for (const auto& [sd_id, my_sd] : _subdomains) {
      if (on_intrf) {
        glob_idx[sd_id] = my_sd.second.get_global_indices();
      } else {
        glob_idx[sd_id] = my_sd.first.get_global_indices();
      }
    }
    return glob_idx;
  }

  [[nodiscard]] std::map<int, std::vector<int>> get_interface() const {
    if (_rank != _master)
      return std::map<int, std::vector<int>>();
    std::map<int, std::vector<int>> interface;
    for (const auto& [sd_id, my_sd] : _subdomains) {
      interface[sd_id] = my_sd.first.get_interface();
    }
    return interface;
  }

  [[nodiscard]] std::vector<int>
  get_global_indices(const int sd_id, bool on_intrf = false) const {
    if (_rank != _master)
      return std::vector<int>();
    if (_interface_only)
      on_intrf = true;
    if (on_intrf)
      return _subdomains.at(sd_id).second.get_global_indices();
    return _subdomains.at(sd_id).first.get_global_indices();
  }

  [[nodiscard]] std::vector<int> get_interface(const int sd_id,
                                               bool on_intrf = false) const {
    if (_interface_only)
      on_intrf = true;
    if (_rank != _master)
      return std::vector<int>();
    if (on_intrf)
      return _subdomains.at(sd_id).second.get_interface();
    return _subdomains.at(sd_id).first.get_interface();
  }

  [[nodiscard]] std::vector<int>
  get_interface_between(const int sd_id, const int nei_id,
                        bool on_intrf = false) const {
    if (_interface_only)
      on_intrf = true;
    if (_rank != _master)
      return std::vector<int>();
    if (on_intrf)
      return _subdomains.at(sd_id).second.get_interface_with(nei_id);
    return _subdomains.at(sd_id).first.get_interface_with(nei_id);
  }

  [[nodiscard]] Thread_map& get_thread2sds() { return _thread2sds; }

  [[nodiscard]] Subdomain& get_subdomain(const int sd_id,
                                         bool on_intrf = false) {
    if (_interface_only)
      on_intrf = true;
    if (on_intrf)
      return _subdomains.at(sd_id).second;
    return _subdomains.at(sd_id).first;
  }

  [[nodiscard]] int get_nb_threads() const { return _thread2sds.size(); }

  [[nodiscard]] const std::vector<int>& get_sds_by_th(int th_id) const {
    return _thread2sds.at(th_id).second;
  }

  [[nodiscard]] const Subdomain& get_subdomain(const int sd_id,
                                               bool on_intrf = false) const {
    if (_interface_only)
      on_intrf = true;
    if (on_intrf)
      return _subdomains.at(sd_id).second;
    return _subdomains.at(sd_id).first;
  }
  // Getters:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Communication between subdomains][Communication between subdomains:1]]
  template <class T>
  std::vector<T> subdomain_allgather(const std::vector<T>& sendbuf,
                                     const std::vector<int>& recvcounts) {
    if (_master != _rank)
      return std::vector<T>();

    const int total_size =
        std::accumulate(recvcounts.begin(), recvcounts.end(), 0);
    std::vector<T> recv_buffer(total_size);

    const int n_masters = MMPI::size(_master_comm);
    std::vector<int> displ(n_masters + 1, 0);

    for (int i = 1; i <= static_cast<int>(n_masters); ++i) {
      displ[i] = displ[i - 1] + recvcounts[i - 1];
    }

    MMPI::allgatherv<T, std::vector<int>>(sendbuf.data(), recv_buffer.data(),
                                          static_cast<int>(sendbuf.size()),
                                          recvcounts, displ, _master_comm);
    return recv_buffer;
  }
  // Communication between subdomains:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Communication between subdomains][Communication between subdomains:2]]
private:
  template <typename T>
  T _subdomain_allreduce(const std::vector<T>& sendbufs, MPI_Op op = MPI_SUM) {
    if (_master != _rank)
      return T();

    T in_val;
    T out_val = T{0};
    if (op == MPI_SUM) {
      in_val = T{0};
      for (const auto& s : sendbufs)
        in_val += s;
    } else if (op == MPI_MAX || op == MPI_MIN) {
      if constexpr (is_complex<T>::value) {
        COMPOSYX_ASSERT(
            false, "Allreduce with MPI_MAX or MPI_MIN with complex arithmetic");
      } else {
        in_val = sendbufs[0];
        if (op == MPI_MAX) {
          for (const auto& s : sendbufs)
            in_val = std::max(in_val, s);
        } else { //(op == MPI_MIN)
          for (const auto& s : sendbufs)
            in_val = std::min(in_val, s);
        }
      }
    } else {
      COMPOSYX_ASSERT(
          false, "Subdomain allreduce not implemented with this operation");
    }

    MMPI::allreduce<T>(&in_val, &out_val, 1, op, _master_comm);

    return out_val;
  }

  template <typename T, int NbCol>
  DenseMatrix<T, NbCol> _subdomain_allreduce_dense_matrix(
      const std::vector<DenseMatrix<T, NbCol>>& sendbufs, MPI_Op op = MPI_SUM) {
    using Mat = DenseMatrix<T, NbCol>;
    if (_master != _rank)
      return Mat();
    auto m = sendbufs[0].get_n_rows();
    auto n = sendbufs[0].get_n_cols();
    Mat in_val; //(m, n);
    Mat out_val(m, n);
    if (op == MPI_SUM) {
      in_val = sendbufs[0];
      // for(const auto& s : sendbufs) in_val += s;
    } else if (op == MPI_MAX || op == MPI_MIN) {
      COMPOSYX_ASSERT(false,
                      "Allreduce with MPI_MAX or MPI_MIN on DenseMatrix");
    } else {
      COMPOSYX_ASSERT(
          false, "Subdomain allreduce not implemented with this operation");
    }

    MMPI::allreduce<T>(in_val.get_ptr(), out_val.get_ptr(),
                       static_cast<int>(m * n), op, _master_comm);

    return out_val;
  }

public:
  template <typename T, bool has_vector_comm_pattern = false>
  T subdomain_allreduce(const std::vector<T>& sendbufs, MPI_Op op = MPI_SUM) {
    if constexpr (has_vector_comm_pattern)
      return _subdomain_allreduce_dense_matrix(sendbufs, op);
    else
      return _subdomain_allreduce(sendbufs, op);
  }

  /*template<typename T, int NbCol, bool has_vector_comm_pattern = false>
    DenseMatrix<T, NbCol> subdomain_allreduce(const std::vector<DenseMatrix<T,
    NbCol>>& sendbufs, MPI_Op op = MPI_SUM){ if constexpr
    (has_vector_comm_pattern) return _subdomain_allreduce_dense_matrix(sendbufs,
    op); else return _subdomain_allreduce(sendbufs, op);
    }*/
// Communication between subdomains:2 ends here

// [[file:../../../org/composyx/dist/Process.org::*Communication between subdomains][Communication between subdomains:3]]
#if defined(COMPOSYX_USE_MULTITHREAD)
  template <class T, class M = std::vector<T>,
            bool has_vector_comm_pattern = false>
  void
  subdomain_assemble(std::map<int, M>& local_data, bool on_intrf,
                     const Reduction& reduction = Reduction::twoways_sum,
                     const MatrixStorage& storage = MatrixStorage::full) const {
    if (_master != _rank)
      return;
    if (_interface_only)
      on_intrf = true;
    COMPOSYX_ASSERT(local_data.size() == _subdomains.size(),
                    "Error in subdomain assemble: number of buffers should be "
                    "the same as number of subdomains.");
    if constexpr (is_sparse<M>::value) {
      subdomain_assemble_sparse<M>(local_data, on_intrf, reduction, storage);
    } else {
      std::vector<std::vector<MPI_Receptor<T>>> receptors(_thread2sds.size());
      std::vector<std::vector<std::vector<T>>> send_buffers(_thread2sds.size());
      parallel_run([&, this](int th_id, std::vector<int>& sds) {
        for (const auto& sd_id : sds) {
          const auto& my_sds = _subdomains.at(sd_id);
          const auto& my_sd = on_intrf ? my_sds.second : my_sds.first;
          receptors[th_id].push_back(
              my_sd.neighbor_send<T, M, has_vector_comm_pattern>(
                  _sd2rank_master, local_data.at(sd_id), send_buffers[th_id],
                  reduction, _master_comm));
        }
      });
      parallel_run([&, this](int th_id, std::vector<int>& sds) {
        for (auto& receptor : receptors[th_id]) {
          bool on_intrf_check;
          while (receptor.get_n_recv()) {
            int source, tag = -1;
            receptor.probe_no_recv(source, tag);
            int sd_local, sd_nei;
            sd_id_from_tag(source, tag, sd_local, sd_nei, on_intrf_check);
            for (const auto& sd_id : sds) {
              if (sd_id == sd_local) {
                std::vector<T> received(
                    std::move(receptor(source, tag, true, true)));
                get_subdomain(sd_id, on_intrf)
                    .neighbor_recv<T, M, has_vector_comm_pattern>(
                        received, local_data.at(sd_id), sd_nei, storage);
                break;
              }
            }
          }
        }
        for (auto& receptor : receptors[th_id]) {
          receptor.wait();
        }
      });
      MMPI::barrier(_master_comm);
    }
  }
#else
  template <class T, class M, bool has_vector_comm_pattern = false>
  void subdomain_assemble_start(
      std::map<int, M>& local_data, std::vector<std::vector<T>>& send_buffers,
      std::vector<MPI_Receptor<T>>& receptors, bool on_intrf,
      const Reduction& reduction = Reduction::twoways_sum) const {

    if (_master != _rank)
      return;
    if (_interface_only)
      on_intrf = true;

    COMPOSYX_DIM_ASSERT(local_data.size(), _subdomains.size(),
                        "Error in subdomain assemble: number of buffers should "
                        "be the same as number of subdomains.");

    for (const auto& my_sd : _subdomains) {
      const int id = my_sd.first;
      receptors.push_back(get_subdomain(id, on_intrf)
                              .neighbor_send<T, M, has_vector_comm_pattern>(
                                  _sd2rank_master, local_data.at(id),
                                  send_buffers, reduction, _master_comm));
    }
  }

  template <class T, class M, bool has_vector_comm_pattern = false>
  void subdomain_assemble_finalize(
      std::map<int, M>& local_data, std::vector<MPI_Receptor<T>>& receptors,
      const MatrixStorage storage = MatrixStorage::full) const {

    if (_master != _rank)
      return;
    bool on_intrf;
    for (auto& receptor : receptors) {
      int source = -1, tag = -1;
      while (receptor.get_n_recv()) {
        std::vector<T> received(receptor(source, tag));
        int sd_local, sd_nei;
        sd_id_from_tag(source, tag, sd_local, sd_nei, on_intrf);
        for (const auto& my_sd : _subdomains) {
          const int id = my_sd.first;
          if (id == sd_local) {
            get_subdomain(id, on_intrf)
                .neighbor_recv<T, M, has_vector_comm_pattern>(
                    received, local_data[id], sd_nei, storage);
            break;
          }
        }
      }
    }

    for (auto& receptor : receptors) {
      receptor.wait();
    }
    MMPI::barrier(_master_comm);
  }

  template <class T, class M = std::vector<T>,
            bool has_vector_comm_pattern = false>
  void
  subdomain_assemble(std::map<int, M>& local_data, bool on_intrf,
                     const Reduction& reduction = Reduction::twoways_sum,
                     const MatrixStorage storage = MatrixStorage::full) const {
    if (_master != _rank)
      return;
    if (_interface_only)
      on_intrf = true;
    if constexpr (is_sparse<M>::value) {
      subdomain_assemble_sparse<M>(local_data, on_intrf, reduction, storage);
    } else {
      std::vector<std::vector<T>> send_buffers;
      std::vector<MPI_Receptor<T>> receptors;
      subdomain_assemble_start<T, M, has_vector_comm_pattern>(
          local_data, send_buffers, receptors, on_intrf, reduction);
      subdomain_assemble_finalize<T, M, has_vector_comm_pattern>(
          local_data, receptors, storage);
    }
  }
#endif

  template <class T, class ValueArray>
  void subdomain_disassemble(std::map<int, ValueArray>& local_data,
                             bool on_intrf = false) const {
    if (_master != _rank)
      return;
    if (_interface_only)
      on_intrf = true;

    for (const auto& my_sd : _subdomains) {
      const int sd_id = my_sd.first;
      get_subdomain(sd_id, on_intrf).disassemble(local_data.at(sd_id));
    }
  }
// Communication between subdomains:3 ends here

// [[file:../../../org/composyx/dist/Process.org::*Communication between subdomains][Communication between subdomains:4]]
#if defined(COMPOSYX_USE_MULTITHREAD)
  template <CPX_SparseMatrix Matrix>
  void subdomain_assemble_sparse(
      std::map<int, Matrix>& local_data, bool on_intrf,
      const Reduction& reduction = Reduction::twoways_sum,
      const MatrixStorage storage = MatrixStorage::full) const {
    if (_master != _rank)
      return;
    if (_interface_only)
      on_intrf = true;
    COMPOSYX_ASSERT(local_data.size() == _subdomains.size(),
                    "Error in subdomain assemble: number of buffers should be "
                    "the same as number of subdomains.");
    std::vector<std::vector<MPI_Receptor<char>>> receptors(_thread2sds.size());
    std::vector<std::vector<std::vector<char>>> send_buffers(
        _thread2sds.size());
    parallel_run([&, this](int th_id, std::vector<int>& sds) {
      for (const auto& sd_id : sds) {
        const auto& my_sds = _subdomains.at(sd_id);
        const auto& my_sd = on_intrf ? my_sds.second : my_sds.first;
        receptors[th_id].push_back(my_sd.neighbor_send_sparse(
            _sd2rank_master, local_data.at(sd_id), send_buffers[th_id],
            reduction, _master_comm));
      }
    });
    parallel_run([&, this](int th_id, std::vector<int>& sds) {
      for (auto& receptor : receptors[th_id]) {
        bool on_intrf_check;
        while (receptor.get_n_recv()) {
          int source, tag = -1;
          receptor.probe_no_recv(source, tag);
          int sd_local, sd_nei;
          sd_id_from_tag(source, tag, sd_local, sd_nei, on_intrf_check);
          COMPOSYX_ASSERT(on_intrf == on_intrf_check,
                          "Process::subdomain_assemble_sparse error in comm");
          for (const auto& sd_id : sds) {
            if (sd_id == sd_local) {
              std::vector<char> received(receptor(source, tag, true, true));
              get_subdomain(sd_id, on_intrf)
                  .neighbor_recv_sparse<Matrix>(received, local_data.at(sd_id),
                                                sd_nei, storage);
              break;
            }
          }
        }
      }
      for (auto& receptor : receptors[th_id]) {
        receptor.wait();
      }
    });
    MMPI::barrier(_master_comm);
  }
#else
  template <CPX_SparseMatrix Matrix>
  void subdomain_assemble_sparse(
      std::map<int, Matrix>& local_data, bool on_intrf,
      const Reduction& reduction = Reduction::twoways_sum,
      const MatrixStorage storage = MatrixStorage::full) const {
    if (_master != _rank)
      return;
    if (_interface_only)
      on_intrf = true;
    std::vector<std::vector<char>> send_buffers;
    std::vector<MPI_Receptor<char>> receptors;

    COMPOSYX_ASSERT(local_data.size() == _subdomains.size(),
                    "Error in subdomain assemble: number of buffers should be "
                    "the same as number of subdomains.");

    for (const auto& [id, my_sds] : _subdomains) {
      const auto& my_sd = on_intrf ? my_sds.second : my_sds.first;
      receptors.push_back(
          my_sd.neighbor_send_sparse(_sd2rank_master, local_data.at(id),
                                     send_buffers, reduction, _master_comm));
    }

    for (auto& receptor : receptors) {
      int source, tag = -1;
      bool on_intrf_check;
      while (receptor.get_n_recv()) {
        std::vector<char> received(receptor(source, tag));
        int sd_local, sd_nei;
        sd_id_from_tag(source, tag, sd_local, sd_nei, on_intrf_check);
        COMPOSYX_ASSERT(on_intrf == on_intrf_check,
                        "Process::subdomain_assemble_sparse error in comm");
        for (const auto& my_sd : _subdomains) {
          const int id = my_sd.first;
          if (id == sd_local) {
            get_subdomain(id, on_intrf)
                .neighbor_recv_sparse<Matrix>(received, local_data.at(id),
                                              sd_nei, storage);
            break;
          }
        }
      }
    }

    for (auto& receptor : receptors) {
      receptor.wait();
    }
    MMPI::barrier(_master_comm);
  }
#endif
  // Communication between subdomains:4 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Exchange simple buffer between neighbor subdomains][Exchange simple buffer between neighbor subdomains:1]]
  template <class T>
  std::map<std::pair<int, int>, std::vector<T>> exchange_with_neighbors(
      const std::map<std::pair<int, int>, std::vector<T>>& sendbuffers) {
    std::map<std::pair<int, int>, std::vector<T>> received;

    if (_master != _rank)
      return received;
    std::vector<Message<T>> messages;
    int n_sent = 0; // = nb of reception
    for (const auto& [sd_pair, sendbuf] : sendbuffers) {
      int sd_loc = sd_pair.first;
      int sd_nei = sd_pair.second;
      int tag;
      int dest = _sd2rank_master[sd_nei];
      tag_from_sd_id(sd_loc, sd_nei, dest, tag, false);
      messages.emplace_back(sendbuf.data(), static_cast<int>(sendbuf.size()),
                            dest, tag);
      n_sent++;
    }

    MPI_Receptor<T> receptor =
        MMPI::isendMessages(messages, n_sent, _master_comm);

    while (receptor.get_n_recv()) {
      int source, sd_loc, sd_nei, tag = -1;
      bool on_intrf_dummy;
      std::vector<T> rvd(receptor(source, tag));
      sd_id_from_tag(source, tag, sd_loc, sd_nei, on_intrf_dummy);
      received[std::make_pair(sd_loc, sd_nei)] = std::move(rvd);
    }
    receptor.wait();
    MMPI::barrier(_master_comm); // Not sure why but this barrier is needed

    return received;
  }
// Exchange simple buffer between neighbor subdomains:1 ends here

// [[file:../../../org/composyx/dist/Process.org::*Centralization of vectors][Centralization of vectors:1]]
#if defined(COMPOSYX_USE_MULTITHREAD)
  template <typename T>
  std::vector<T> subdomain_centralize(std::map<int, std::vector<T>>& local_data,
                                      int root = -1, bool on_intrf = false) {
    if (_rank != _master)
      return std::vector<T>();
    if (_interface_only)
      on_intrf = true;
    const int n_glob = this->get_n_glob(on_intrf);

    bool bcast_vector = false;
    if (root < 0) {
      root = 0;
      bcast_vector = true;
    }
    std::vector<T> out_vector;

    parallel_run([&, this](int th_id, std::vector<int>& sds) {
      std::vector<std::vector<char>> send_buffers;
      std::vector<Message<char>> messages;

      for (const auto& sd_id : sds) {
        // std:: cout << "checking "  << sd_id << '\n';
        std::vector<T>& buffer = local_data.at(sd_id);
        auto& my_sds = _subdomains.at(sd_id);
        auto& my_sd = on_intrf ? my_sds.second : my_sds.first;
        int buffer_size = my_sd.get_n_loc();
        int* idx_ptr = my_sd.get_glob_idx_ptr();

        COMPOSYX_ASSERT(
            buffer_size == static_cast<int>(buffer.size()),
            "Error in subdomain_centralize_on_root: wrong buffer size");

        {
          std::vector<std::pair<int, T>> pair_buf(buffer_size);
          int l = 0; // Don't send zeros
          for (int k = 0; k < buffer_size; ++k) {
            // std::cerr << "Idx / buf : " << idx_ptr[k] << " / " << buffer[k]
            // << '\n';
            if (buffer[k] != T{0}) {
              pair_buf[l++] = std::make_pair(idx_ptr[k], buffer[k]);
            }
          }
          pair_buf.resize(l);
          send_buffers.emplace_back(
              MMPI::serialize<std::pair<int, T>>(pair_buf));
        }
        auto& sendb = send_buffers[send_buffers.size() - 1];
        messages.emplace_back(sendb.data(), sendb.size(), root, sd_id + 1);
        // std::cout << "msg size "  << messages.size() << '\n';
      }

      const int n_sd = static_cast<int>(_map.size());
      int n_recv = 0;
      if (_rank_master == root && th_id == 0) {
        n_recv = n_sd;
        out_vector = std::vector<T>(n_glob);
      } else if (bcast_vector && th_id == 0) {
        out_vector = std::vector<T>(n_glob);
      }

      MPI_Receptor<char> receptor =
          MMPI::isendMessages(messages, n_recv, _master_comm);

      while (receptor.get_n_recv()) {
        int source, tag;
        std::vector<char> brut_recv = receptor(source, tag);
        std::vector<std::pair<int, T>> pair_recv =
            MMPI::deserialize<std::pair<int, T>>(brut_recv);
        for (const auto& [idx, val] : pair_recv) {
          out_vector[idx] += val;
        }
      }
    });
    /*
    for (auto& [th, sds] : _thread2sds)
    th = std::thread(comm_centralization, th_id++, std::ref(sds));
    for (auto& [th, sds] : _thread2sds)
    th.join(); */

    if (bcast_vector) {
      MMPI::bcast(out_vector.data(), n_glob, root, _master_comm);
    } else {
      MMPI::barrier(_master_comm);
    }
    return out_vector;
  }

  template <typename T>
  DenseMatrix<T> subdomain_centralize(std::map<int, DenseMatrix<T>>& local_data,
                                      int root = -1, bool on_intrf = false) {
    if (_rank != _master)
      return DenseMatrix<T>();
    if (_interface_only)
      on_intrf = true;
    const int n_glob = this->get_n_glob(on_intrf);

    bool bcast_vector = false;
    if (root < 0) {
      root = 0;
      bcast_vector = true;
    }
    DenseMatrix<T> out_vector;
    int nb_col = 1;
    parallel_run([&, this](int th_id, std::vector<int>& sds) {
      std::vector<std::vector<char>> send_buffers;
      std::vector<Message<char>> messages;
      for (const auto& sd_id : sds) {
        DenseMatrix<T>& buffer = local_data.at(sd_id);
        nb_col = static_cast<int>(n_cols(buffer));
        int nb_row = static_cast<int>(n_rows(buffer));
        auto& my_sds = _subdomains.at(sd_id);
        auto& my_sd = on_intrf ? my_sds.second : my_sds.first;
        int buffer_size = my_sd.get_n_loc() * nb_col;
        int* idx_ptr = my_sd.get_glob_idx_ptr();

        COMPOSYX_ASSERT(
            buffer_size == (nb_row * nb_col),
            "Error in subdomain_centralize_on_root: wrong buffer size");

        {
          std::vector<std::tuple<int, int, T>> pair_buf(buffer_size);
          int l = 0; // Don't send zeros
          for (int j = 0; j < nb_col; ++j) {
            for (int i = 0; i < nb_row; ++i) {
              // std::cerr << "Idx / buf : " << idx_ptr[k] << " / " << buffer[k]
              // << '\n';
              if (buffer(i, j) != T{0}) {
                pair_buf[l++] = std::make_tuple(idx_ptr[i], j, buffer(i, j));
                // if(_rank_master == 0) std::cerr << idx_ptr[i] << ',' << j <<
                // ',' << buffer(i, j) << '\n';
              }
            }
          }
          pair_buf.resize(l);
          send_buffers.emplace_back(
              MMPI::serialize<std::tuple<int, int, T>>(pair_buf));
        }

        auto& sendb = send_buffers[send_buffers.size() - 1];
        messages.emplace_back(sendb.data(), sendb.size(), root,
                              my_sd.get_id() + 1);
      }

      const int n_sd = static_cast<int>(_map.size());
      int n_recv = 0;
      if (_rank_master == root && th_id == 0) {
        n_recv = n_sd;
        out_vector = DenseMatrix<T>(n_glob, nb_col);
      } else if (bcast_vector && th_id == 0) {
        out_vector = DenseMatrix<T>(n_glob, nb_col);
      }

      MPI_Receptor<char> receptor =
          MMPI::isendMessages(messages, n_recv, _master_comm);

      while (receptor.get_n_recv()) {
        int source, tag;
        std::vector<char> brut_recv = receptor(source, tag);
        std::vector<std::tuple<int, int, T>> pair_recv =
            MMPI::deserialize<std::tuple<int, int, T>>(brut_recv);
        for (const auto& [idx_i, idx_j, val] : pair_recv) {
          out_vector(idx_i, idx_j) += val;
        }
      }
    });

    if (bcast_vector) {
      MMPI::bcast(out_vector.get_ptr(), n_glob * nb_col, root, _master_comm);
    } else {
      MMPI::barrier(_master_comm);
    }
    return out_vector;
  }
#else
  template <typename T>
  std::vector<T> subdomain_centralize(std::map<int, std::vector<T>>& local_data,
                                      int root = -1, bool on_intrf = false) {
    if (_rank != _master)
      return std::vector<T>();
    if (_interface_only)
      on_intrf = true;
    const int n_glob = this->get_n_glob(on_intrf);

    bool bcast_vector = false;
    if (root < 0) {
      root = 0;
      bcast_vector = true;
    }

    std::vector<std::vector<char>> send_buffers;
    std::vector<Message<char>> messages;

    for (auto& [id, my_sds] : _subdomains) {
      std::vector<T>& buffer = local_data.at(id);
      auto& my_sd = on_intrf ? my_sds.second : my_sds.first;
      int buffer_size = my_sd.get_n_loc();
      int* idx_ptr = my_sd.get_glob_idx_ptr();

      COMPOSYX_ASSERT(
          buffer_size == static_cast<int>(buffer.size()),
          "Error in subdomain_centralize_on_root: wrong buffer size");

      {
        std::vector<std::pair<int, T>> pair_buf(buffer_size);
        int l = 0; // Don't send zeros
        for (int k = 0; k < buffer_size; ++k) {
          //std::cerr << "Idx / buf : " << idx_ptr[k] << " / " << buffer[k] << '\n';
          if (buffer[k] != T{0}) {
            pair_buf[l++] = std::make_pair(idx_ptr[k], buffer[k]);
          }
        }
        pair_buf.resize(l);
        send_buffers.emplace_back(MMPI::serialize<std::pair<int, T>>(pair_buf));
      }

      auto& sendb = send_buffers[send_buffers.size() - 1];
      messages.emplace_back(sendb.data(), sendb.size(), root, id + 1);
    }

    const int n_sd = static_cast<int>(_map.size());
    int n_recv = 0;
    std::vector<T> out_vector;
    if (_rank_master == root) {
      n_recv = n_sd;
      out_vector = std::vector<T>(n_glob);
    } else if (bcast_vector) {
      out_vector = std::vector<T>(n_glob);
    }

    MPI_Receptor<char> receptor =
        MMPI::isendMessages(messages, n_recv, _master_comm);

    while (receptor.get_n_recv()) {
      int source, tag;
      std::vector<char> brut_recv = receptor(source, tag);
      std::vector<std::pair<int, T>> pair_recv =
          MMPI::deserialize<std::pair<int, T>>(brut_recv);
      for (const auto& [idx, val] : pair_recv) {
        out_vector[idx] += val;
      }
    }

    if (bcast_vector) {
      MMPI::bcast(out_vector.data(), n_glob, root, _master_comm);
    } else {
      MMPI::barrier(_master_comm);
    }

    return out_vector;
  }

  template <typename T>
  DenseMatrix<T> subdomain_centralize(std::map<int, DenseMatrix<T>>& local_data,
                                      int root = -1, bool on_intrf = false) {
    if (_rank != _master)
      return DenseMatrix<T>();
    if (_interface_only)
      on_intrf = true;
    const int n_glob = this->get_n_glob(on_intrf);

    bool bcast_vector = false;
    if (root < 0) {
      root = 0;
      bcast_vector = true;
    }

    std::vector<std::vector<char>> send_buffers;
    std::vector<Message<char>> messages;
    int nb_col = 1;
    for (auto& [id, my_sds] : _subdomains) {
      DenseMatrix<T>& buffer = local_data.at(id);
      nb_col = static_cast<int>(n_cols(buffer));
      int nb_row = static_cast<int>(n_rows(buffer));
      auto& my_sd = on_intrf ? my_sds.second : my_sds.first;
      int buffer_size = my_sd.get_n_loc() * nb_col;
      int* idx_ptr = my_sd.get_glob_idx_ptr();

      COMPOSYX_ASSERT(
          buffer_size == (nb_row * nb_col),
          "Error in subdomain_centralize_on_root: wrong buffer size");

      {
        std::vector<std::tuple<int, int, T>> pair_buf(buffer_size);
        int l = 0; // Don't send zeros
        for (int j = 0; j < nb_col; ++j) {
          for (int i = 0; i < nb_row; ++i) {
            //std::cerr << "Idx / buf : " << idx_ptr[k] << " / " << buffer[k] << '\n';
            if (buffer(i, j) != T{0}) {
              pair_buf[l++] = std::make_tuple(idx_ptr[i], j, buffer(i, j));
              //if(_rank_master == 0) std::cerr << idx_ptr[i] << ',' << j << ',' << buffer(i, j) << '\n';
            }
          }
        }
        pair_buf.resize(l);
        send_buffers.emplace_back(
            MMPI::serialize<std::tuple<int, int, T>>(pair_buf));
      }

      auto& sendb = send_buffers[send_buffers.size() - 1];
      messages.emplace_back(sendb.data(), sendb.size(), root,
                            my_sd.get_id() + 1);
    }

    const int n_sd = static_cast<int>(_map.size());
    int n_recv = 0;
    DenseMatrix<T> out_vector;
    if (_rank_master == root) {
      n_recv = n_sd;
      out_vector = DenseMatrix<T>(n_glob, nb_col);
    } else if (bcast_vector) {
      out_vector = DenseMatrix<T>(n_glob, nb_col);
    }

    MPI_Receptor<char> receptor =
        MMPI::isendMessages(messages, n_recv, _master_comm);

    while (receptor.get_n_recv()) {
      int source, tag;
      std::vector<char> brut_recv = receptor(source, tag);
      std::vector<std::tuple<int, int, T>> pair_recv =
          MMPI::deserialize<std::tuple<int, int, T>>(brut_recv);
      for (const auto& [idx_i, idx_j, val] : pair_recv) {
        out_vector(idx_i, idx_j) += val;
      }
    }

    if (bcast_vector) {
      MMPI::bcast(out_vector.get_ptr(), n_glob * nb_col, root, _master_comm);
    } else {
      MMPI::barrier(_master_comm);
    }

    return out_vector;
  }
#endif
  // Centralization of vectors:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Thread level parallel run engine][Thread level parallel run engine:1]]
  template <class Fn, typename... Args>
  void parallel_run(Fn fn, Args&... args) const {
    int th_id = 0;
    //Timer<1> t("Fork time");
    for (auto& [th, sds] : _thread2sds) {
      th = std::thread(fn, th_id++, std::ref(sds), std::ref(args)...);
    }
    //t.stop();
    for (auto& [th, sds] : _thread2sds) {
      th.join();
    }
  }
  // Thread level parallel run engine:1 ends here

  // [[file:../../../org/composyx/dist/Process.org::*Footer class Process][Footer class Process:1]]
}; // class Process
// Footer class Process:1 ends here

// [[file:../../../org/composyx/dist/Process.org::*Compute the binding map][Compute the binding map:1]]
inline Subdomain_map compute_map(const int n_sd, const int n_procs) {
  Subdomain_map map;

  if (n_procs == 0 or n_sd == 0)
    return map;

  const int sds_per_proc = n_sd / n_procs;
  const int sds_per_proc_r = n_sd % n_procs;
  const int procs_per_sd = n_procs / n_sd;
  const int procs_per_sd_r = n_procs % n_sd;

  int proc_rank = 0;
  int id_in_group = 0;

  for (int sd = 0; sd < n_sd; ++sd) {
    // Outputs
    std::vector<int> procs;
    int master;

    if (sds_per_proc == 0) { // More processes than sd
      int group_size = procs_per_sd;

      // First groups take one more if the division is not exact
      if (sd < procs_per_sd_r) {
        group_size++;
      }

      // Add the processes in the group with the first as master
      master = proc_rank;
      for (int j = 0; j < group_size; ++j) {
        procs.push_back(proc_rank++);
      }
    }

    else { // More sd than processes
      int sd_group_size = sds_per_proc;

      // First groups take one more if the division is not exact
      if (proc_rank < sds_per_proc_r) {
        sd_group_size++;
      }

      master = proc_rank;
      procs.push_back(master);

      // Last of my group => increment proc_rank
      id_in_group++;
      if (id_in_group == sd_group_size) {
        proc_rank++;
        id_in_group = 0;
      }
    }
    map.emplace_back(std::make_pair(std::move(procs), master));
  }

  return map;
}
// Compute the binding map:1 ends here

// [[file:../../../org/composyx/dist/Process.org::*Create the process][Create the process:1]]
inline std::shared_ptr<Process>
bind_subdomains(const int n_sd, const unsigned int n_threads = 1,
                const MPI_Comm comm = MPI_COMM_WORLD) {
  const int rank = MMPI::rank(comm);
  const int n_procs = MMPI::size(comm);
  Subdomain_map map = compute_map(n_sd, n_procs);

  std::vector<int> master_ranks;
  std::vector<int> worker_ranks;
  std::vector<int> sd2rank;
  int my_master = -1;

  for (auto& p : map) {
    const std::vector<int>& procs = p.first;
    const int master = p.second;
    sd2rank.push_back(master);
    if (master_ranks.size() == 0 || master_ranks.back() != master) {
      master_ranks.push_back(master);
    }
    if (rank == master) {
      worker_ranks = procs;
      my_master = master;
    } else if (my_master == -1) {
      for (auto& pp : procs) {
        if (rank == pp) {
          my_master = master;
          worker_ranks = procs;
          break;
        }
      }
    }
  }

  return std::make_shared<Process>(comm, std::move(map), my_master, sd2rank,
                                   master_ranks, worker_ranks, n_threads);
}
// Create the process:1 ends here

// [[file:../../../org/composyx/dist/Process.org::*Footer namespace][Footer namespace:1]]
} // namespace composyx
// Footer namespace:1 ends here
