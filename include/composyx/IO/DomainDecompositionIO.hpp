// [[file:../../../org/composyx/IO/DomainDecompositionIO.org::*Header][Header:1]]
#pragma once

#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "composyx/IO/MatrixMarketLoader.hpp"
#include "composyx/utils/Error.hpp"
#include "composyx/utils/ArrayAlgo.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/IO/DomainDecompositionIO.org::*Read][Read:1]]
inline void read_subdomain_mf(const std::string& filename, int& n_tot, int& n_g,
                              std::map<int, std::vector<int>>& nei2dof) {
  std::ifstream f{filename, std::ios::in};
  std::string line;
  std::vector<std::string> s;

  if (!f.is_open()) {
    COMPOSYX_ASSERT(f, filename + ": file not opened");
  }

  // 1st line: ndofs
  std::getline(f, line);
  s = split(line, '=');
  n_tot = std::stoi(s[1]);

  // 2nd line: n_g
  std::getline(f, line);
  s = split(line, '=');
  n_g = std::stoi(s[1]);

  // 5th line: number of neighbors
  std::getline(f, line);
  std::getline(f, line);
  std::getline(f, line);
  s = split(line, '=');
  int n_neighbors = std::stoi(s[1]);

  // 6th line: indexVi -> indices of the neighbors
  std::getline(f, line);
  s = split(line);
  std::vector<int> neighbors(n_neighbors);
  for (auto i = 1; i <= n_neighbors; ++i) {
    neighbors[i - 1] = std::stoi(s[i]);
  }

  //7th line: ptrIndexVi -> pointers to the following list
  std::getline(f, line);
  s = split(line);
  std::vector<int> ptrIndexVi(n_neighbors + 1);
  for (auto i = 0; i <= n_neighbors; ++i) {
    ptrIndexVi[i] = std::stoi(s[i + 1]) - 1;
  }

  //8th line: indexIntrf -> list of indices in the interface for each neighbor
  nei2dof.clear();
  std::getline(f, line);
  s = split(line);
  std::string* sptr = &s[1];
  for (auto i = 0; i < static_cast<int>(neighbors.size()); ++i) {
    int nei = neighbors[i];
    nei2dof[nei] = std::vector<int>(ptrIndexVi[i + 1] - ptrIndexVi[i]);
    for (auto j = 0; j < ptrIndexVi[i + 1] - ptrIndexVi[i]; ++j) {
      nei2dof[nei][j] = n_tot - n_g + std::stoi(*(sptr++)) - 1;
    }
  }
}
// Read:1 ends here

// [[file:../../../org/composyx/IO/DomainDecompositionIO.org::*Write][Write:1]]
inline void write_subdomain_mf(const std::string& filename, const int n_tot,
                               const int n_g,
                               const std::map<int, std::vector<int>>& nei2dof,
                               const std::vector<int>& interface,
                               const std::vector<int>& glob_idx) {
  std::ofstream f{filename, std::ios::out};
  if (!f.is_open()) {
    COMPOSYX_ASSERT(f, filename + ": file not opened");
  }

  f << "myndof= " << n_tot << '\n';
  f << "sizeIntrf= " << interface.size() << '\n';

  f << "myInterface= ";
  {
    std::vector<int> glob_intrf = subarray(glob_idx, interface);
    for (auto idx : glob_intrf) {
      f << idx << ' ';
    }
    f.seekp(-1, std::ios_base::end);
    f << "\n\n";
  }

  f << "nbVi= " << nei2dof.size() << '\n';

  {
    std::vector<int> ptrIndexVi(nei2dof.size() + 1);
    f << "indexVi= ";
    int ptrvi = 1;
    int k = 0;
    for (const auto& [idx, dofs] : nei2dof) {
      f << idx << ' ';
      ptrIndexVi[k++] = ptrvi;
      ptrvi += dofs.size();
    }
    f.seekp(-1, std::ios_base::end);
    f << '\n';
    ptrIndexVi[k] = ptrvi;

    f << "ptrIndexVi= ";
    for (const auto idx : ptrIndexVi) {
      f << idx << ' ';
    }
    f.seekp(-1, std::ios_base::end);
    f << '\n';
  }

  f << "indexIntrf= ";
  for (const auto& n2f : nei2dof) {
    const auto& dofs = n2f.second;
    for (const auto i : dofs) {
      for (int k = 0; k < n_g; ++k) {
        if (interface[k] == i) {
          f << k + 1 << ' ';
          break;
        }
      }
    }
  }
  f.seekp(-1, std::ios_base::end);
  f << '\n';
}
// Write:1 ends here

// [[file:../../../org/composyx/IO/DomainDecompositionIO.org::*Tiny arithmetic parser][Tiny arithmetic parser:1]]
inline int tiny_i_parser(const int I_val, const std::string& s) {
  auto pos_plus = s.find('+');
  auto pos_minus = s.find('-');
  auto pos_times = s.find('*');
  auto pos_slash = s.find('/');

  if (pos_plus != std::string::npos) {
    return tiny_i_parser(I_val, s.substr(0, pos_plus)) +
           tiny_i_parser(I_val, s.substr(pos_plus + 1, s.size()));
  } else if (pos_minus != std::string::npos) {
    return tiny_i_parser(I_val, s.substr(0, pos_minus)) -
           tiny_i_parser(I_val, s.substr(pos_minus + 1, s.size()));
  } else if (pos_times != std::string::npos) {
    return tiny_i_parser(I_val, s.substr(0, pos_times)) *
           tiny_i_parser(I_val, s.substr(pos_times + 1, s.size()));
  } else if (pos_slash != std::string::npos) {
    return tiny_i_parser(I_val, s.substr(0, pos_slash)) /
           tiny_i_parser(I_val, s.substr(pos_slash + 1, s.size()));
  } else {
    if (s.find('I') != std::string::npos)
      return I_val;
    return std::stoi(s);
  }
}
// Tiny arithmetic parser:1 ends here

// [[file:../../../org/composyx/IO/DomainDecompositionIO.org::*Read][Read:1]]
inline void read_subdomain_mpp(const std::string& filename, int& n_tot,
                               int& n_g,
                               std::map<int, std::vector<int>>& nei2dof,
                               const int sd_id) {
  std::ifstream f{filename, std::ios::in};
  std::string line;
  std::vector<std::string> s;

  if (!f.is_open()) {
    COMPOSYX_ASSERT(f, filename + ": file not opened");
  }

  n_tot = -1;
  nei2dof.clear();

  std::vector<int> interface;

  auto rm_comments = [](std::string& str) {
    std::vector<std::string> v_str;
    v_str = split(str, '#');
    str = v_str[0];
  };

  while (std::getline(f, line)) {
    if (line.empty())
      continue;
    rm_comments(line);

    s = split(line, ':');
    if (!s.empty() && s[0] == "ndofs")
      n_tot = std::stoi(s[1]);

    if (!s.empty() && s[0] == "nei2dof") {

      int nei_id = -1;
      while (std::getline(f, line)) {
        if (line.empty())
          continue;
        if (line == std::string("..."))
          break;

        rm_comments(line);
        s = split(line, ':');

        COMPOSYX_ASSERT(s.size() >= 2, "DomainDecompositionIO::read_subdomain_"
                                       "mpp: error parsing indices in " +
                                           filename);

        nei_id = tiny_i_parser(sd_id, s[0]);

        const std::string& s_array = s[1];

        size_t pos_beg = 0;
        size_t pos_end = s_array.size() - 1;
        // Go after '['
        for (const auto c : s_array) {
          if (c == '[')
            break;
          ++pos_beg;
        }
        // Stay before ']'
        for (auto cp = s_array.crbegin(); cp != s_array.crend(); ++cp) {
          if (*cp == ']')
            break;
          --pos_beg;
        }

        const std::string s_parse =
            s_array.substr(pos_beg + 1, pos_end - pos_beg - 1);
        std::vector<std::string> s_indices = split(s_parse, ',');

        size_t n_g_loc = s_indices.size() - 1;
        std::vector<int> nei_intrf(n_g_loc);

        for (size_t k = 0; k < n_g_loc; ++k) {
          nei_intrf[k] = std::stoi(s_indices[k]);
        }
        union_with(interface, nei_intrf);
        nei2dof[nei_id] = std::move(nei_intrf);
      }

      COMPOSYX_ASSERT(nei_id >= 0, "DomainDecompositionIO::read_subdomain_mpp: "
                                   "error parsing indices in " +
                                       filename);
    }
  }
  n_g = static_cast<int>(interface.size());

  COMPOSYX_ASSERT(n_tot >= 0, "DomainDecompositionIO::read_subdomain_mpp: "
                              "cound not find key 'ndofs' in " +
                                  filename);
}
// Read:1 ends here

// [[file:../../../org/composyx/IO/DomainDecompositionIO.org::*Write][Write:1]]
inline void
write_subdomain_mpp(const std::string& filename, const int n_tot,
                    const std::map<int, std::vector<int>>& nei2dof) {
  std::ofstream f{filename, std::ios::out};
  if (!f.is_open()) {
    COMPOSYX_ASSERT(f, filename + ": file not opened");
  }

  f << "---\n";
  f << "ndofs: " << n_tot << '\n';
  f << "nei2dof:\n";

  for (const auto& [nei, indices] : nei2dof) {
    f << "  " << nei << ": [";
    for (auto idx : indices) {
      f << idx << ',';
    }
    f.seekp(-1, std::ios_base::end);
    f << "]\n";
  }

  f << "...\n";
}
// Write:1 ends here

// [[file:../../../org/composyx/IO/DomainDecompositionIO.org::*General reader][General reader:1]]
inline void read_subdomain(const std::string& filename, int& n_tot, int& n_g,
                           std::map<int, std::vector<int>>& nei2dof,
                           const int sd_id) {
  std::ifstream f{filename, std::ios::in};
  std::string line;
  std::vector<std::string> s;

  if (!f.is_open()) {
    COMPOSYX_ASSERT(f, filename + ": file not opened");
  }

  // 1st line: "ndofs" or "---"
  std::getline(f, line);
  s = split(line, '=');
  f.close();
  if (header_match(s, "myndof")) {
    read_subdomain_mf(filename, n_tot, n_g, nei2dof);
  } else {
    read_subdomain_mpp(filename, n_tot, n_g, nei2dof, sd_id);
  }
}
// General reader:1 ends here

// [[file:../../../org/composyx/IO/DomainDecompositionIO.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
