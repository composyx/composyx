// [[file:../../../org/composyx/IO/MatrixMarketLoader.org::*Header][Header:1]]
#pragma once

#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <iostream>
#include <fstream>
#include <sstream>
#include <type_traits>
#include <complex>
#include <vector>
#include <algorithm>

#include "composyx/utils/Error.hpp"
#include "composyx/utils/Arithmetic.hpp"
#include "composyx/utils/MatrixProperties.hpp"
#include "composyx/loc_data/SparseMatrixCOO.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/IO/MatrixMarketLoader.org::*Split string][Split string:1]]
inline std::vector<std::string> split(const std::string& line,
                                      const char sep = ' ') {
  std::stringstream ss{line};
  std::string token;
  std::vector<std::string> res;

  while (std::getline(ss, token, sep)) {
    // empty token each time there is several separators following each other :
    if (token != "") {
      res.emplace_back(token);
    }
  }
  return res;
}
// Split string:1 ends here

// [[file:../../../org/composyx/IO/MatrixMarketLoader.org::*Find a sub-string][Find a sub-string:1]]
inline bool header_match(const std::vector<std::string>& header,
                         std::string token) {
  return (std::find(header.begin(), header.end(), token) != header.end());
}
// Find a sub-string:1 ends here

// [[file:../../../org/composyx/IO/MatrixMarketLoader.org::*String to real conversion][String to real conversion:1]]
inline void str_to_real(const std::string& s, float& f) { f = std::stof(s); }
inline void str_to_real(const std::string& s, double& d) { d = std::stod(s); }
// String to real conversion:1 ends here

// [[file:../../../org/composyx/IO/MatrixMarketLoader.org::*Matrix market namespace][Matrix market namespace:1]]
namespace matrix_market {
// Matrix market namespace:1 ends here

// [[file:../../../org/composyx/IO/MatrixMarketLoader.org::*Load a matrix market line][Load a matrix market line:1]]
template <typename Scalar>
inline Scalar read_scalar(const std::vector<std::string>& line, int index) {
  if constexpr (is_real<Scalar>::value) {
    Scalar v;
    str_to_real(line[index], v);
    return v;
  } else {
    using Real = typename arithmetic_real<Scalar>::type;
    Real real, imag;
    str_to_real(line[index], real);
    str_to_real(line[index + 1], imag);
    return Scalar{real, imag};
  }
}

template <typename Scalar>
inline void read_ijv(const std::vector<std::string>& line, int& i, int& j,
                     Scalar& v) {
  i = std::stoi(line[0]) - 1;
  j = std::stoi(line[1]) - 1;
  v = read_scalar<Scalar>(line, 2);
}
// Load a matrix market line:1 ends here

// [[file:../../../org/composyx/IO/MatrixMarketLoader.org::*Read header][Read header:1]]
inline bool is_real_matrix(const std::string& filename) {

  std::ifstream file{filename, std::ios::in};
  COMPOSYX_ASSERT(file, filename + ": could not be opened");

  std::string line;

  getline(file, line);
  auto header = split(line);

  // Check MatrixMarket
  COMPOSYX_ASSERT(
      (header[0] == "%%MatrixMarket") or (header[0] == "%MatrixMarket"),
      filename + ": error in matrix market header. (%MatrixMarket not found)");

  if (header_match(header, "complex")) {
    return false;
  }
  if (header_match(header, "real")) {
    return true;
  }
  if (header_match(header, "double")) {
    return true;
  }

  COMPOSYX_ASSERT(!header_match(header, "pattern"),
                  filename + ": error in matrix market header. Reader does not "
                             "support 'pattern' matrices";);
  COMPOSYX_ASSERT(false,
                  filename +
                      ": error in matrix market header. Unrecognized 'format' "
                      "type (real, double, complex, integer or pattern)";);
  return false;
}

template <class Scalar>
inline void read_header(std::ifstream& file, const std::string& filename,
                        size_t& M, size_t& N, size_t& NNZ,
                        MatrixSymmetry& symmetry, bool& is_format_array) {

  if (!file.is_open()) {
    COMPOSYX_ASSERT(file, filename + ": file not opened");
  }

  bool commentLine;
  std::string line;
  symmetry = MatrixSymmetry::general;

  getline(file, line);
  auto header = split(line);

  // %MatrixMarket object format field symmetry
  // %MatrixMarket or %%MatrixMarket
  // object -> matrix or vector
  // format -> coordinate or array
  // field -> real, double, complex, integer or pattern
  // symmetry -> general, symmetric, skew-symmetric or hermitian

  COMPOSYX_ASSERT(header.size() > 3, "MatrixMarket reader: ill-formed header");

  // Check MatrixMarket
  COMPOSYX_ASSERT(
      (header[0] == "%%MatrixMarket") or (header[0] == "%MatrixMarket"),
      filename + ": error in matrix market header. (%MatrixMarket not found)");

  // Check matrix or vector
  COMPOSYX_ASSERT(
      (header[1] == "matrix") or (header[1] == "vector"),
      filename +
          ": error in matrix market header. (matrix / vector not found)");

  // Array / coordinate format
  is_format_array = (header[2] == "array");
  COMPOSYX_ASSERT(
      is_format_array or (header[2] == "coordinate"),
      filename +
          ": error in matrix market header. (array / coordinate not found)");

  // Get symmetry
  if (header_match(header, "symmetric") or
      header_match(header, "skew-symmetric")) {
    symmetry = MatrixSymmetry::symmetric;
  } else if (header_match(header, "hermitian")) {
    symmetry = MatrixSymmetry::hermitian;
  }

  // Check type is corresponding
  if (is_complex<Scalar>::value && !header_match(header, "complex")) {

    COMPOSYX_ASSERT(header_match(header, "complex"),
                    "This is a complex parser but file "
                    "header does not have \"complex\" tag");

  } else if (!is_complex<Scalar>::value) {
    COMPOSYX_ASSERT(header_match(header, "real") or
                        header_match(header, "double"),
                    "This is a real parser but file "
                    "header does not have \"real\" tag");
  }

  do {
    getline(file, line);
    commentLine = (line.length() == 0 || line[0] == '%');
  } while (commentLine);

  auto size = split(line);
  M = std::stoi(size[0]);
  N = std::stoi(size[1]);
  NNZ = is_format_array ? M * N : std::stoi(size[2]);
}
// Read header:1 ends here

// [[file:../../../org/composyx/IO/MatrixMarketLoader.org::*Loading the file][Loading the file:1]]
template <class IntArray, class ScalArray>
inline void load(const std::string& filename, IntArray& i, IntArray& j,
                 ScalArray& values, size_t& m, size_t& n, size_t& nnz,
                 MatrixSymmetry& symmetry, MatrixStorage& storage) {

  using Scalar = typename ScalArray::value_type;

  std::ifstream file{filename, std::ios::in};
  COMPOSYX_ASSERT(file, filename + ": could not be opened");

  bool is_format_array;
  read_header<Scalar>(file, filename, m, n, nnz, symmetry, is_format_array);
  std::string line;

  i = IntArray(nnz);
  j = IntArray(nnz);
  values = ScalArray(nnz);

  if (is_format_array) {
    size_t k = 0;
    for (size_t jj = 0; jj < n; ++jj) {
      for (size_t ii = 0; ii < m; ++ii) {
        getline(file, line);
        auto lv = split(line);
        i[k] = ii;
        j[k] = jj;
        values[k] = read_scalar<Scalar>(lv, 0);
        k++;
      }
    }
    storage = MatrixStorage::full;
  }

  else { // Format coordinate
    for (size_t k = 0; k < nnz; ++k) {
      getline(file, line);
      auto lv = split(line);
      read_ijv<Scalar>(lv, i[k], j[k], values[k]);
    }

    // When symmetric, trying to find storage
    storage = MatrixStorage::full;
    if (symmetry != MatrixSymmetry::general) {
      bool upper = false;
      bool lower = false;
      for (size_t k = 0; k < nnz; ++k) {
        if (i[k] > j[k]) {
          lower = true;
        } else if (i[k] < j[k]) {
          upper = true;
        }
        if (lower && upper)
          return;
      }
      storage = lower ? MatrixStorage::lower : MatrixStorage::upper;
    }
  }
}

template <class ScalArray>
inline void load(const std::string& filename, ScalArray& values, size_t& m,
                 size_t& n) {

  using Scalar = typename ScalArray::value_type;

  std::ifstream file{filename, std::ios::in};
  COMPOSYX_ASSERT(file, filename + ": could not be opened");

  bool is_format_array;
  size_t dum_nnz;
  MatrixSymmetry dum_sym;
  read_header<Scalar>(file, filename, m, n, dum_nnz, dum_sym, is_format_array);

  COMPOSYX_ASSERT(is_format_array,
                  "matrix_market::load function specific for array format, but "
                  "file is not in this format.");
  std::string line;

  values = ScalArray(m * n);

  size_t k = 0;
  for (size_t jj = 0; jj < n; ++jj) {
    for (size_t ii = 0; ii < m; ++ii) {
      getline(file, line);
      auto lv = split(line);
      values[k] = read_scalar<Scalar>(lv, 0);
      k++;
    }
  }
}
// Loading the file:1 ends here

// [[file:../../../org/composyx/IO/MatrixMarketLoader.org::*Dumping to file][Dumping to file:1]]
template <CPX_Real Scal> inline std::string scalar_to_str(Scal r) {
  return std::to_string(r);
}

template <CPX_Complex Scal> inline std::string scalar_to_str(Scal c) {
  return std::to_string(std::real(c)) + std::string(" ") +
         std::to_string(std::imag(c));
}

template <class IntArray, class ScalArray>
inline void dump(const std::string& filename, const IntArray& i,
                 const IntArray& j, const ScalArray& values, const size_t m,
                 const size_t n, const MatrixSymmetry symmetry,
                 const MatrixStorage storage) {

  using Scalar = typename ScalArray::value_type;

  // Only lower part must be provided if not general
  if (symmetry != MatrixSymmetry::general and storage != MatrixStorage::lower) {
    using Index = typename IntArray::value_type;
    SparseMatrixCOO<Scalar, Index> out(m, n, i.size(), i, j, values);
    out.set_property(symmetry, storage);
    out.to_storage_half(MatrixStorage::lower);
    out.dump_to_matrix_market(filename);
    return;
  }

  std::ofstream file{filename, std::ios::out};
  COMPOSYX_ASSERT(file, filename + ": could not be opened");

  file << "%%MatrixMarket matrix coordinate ";

  if constexpr (is_real<Scalar>::value) {
    file << "real ";
  } else {
    file << "complex ";
  }

  switch (symmetry) {
  case MatrixSymmetry::general:
    file << "general";
    break;
  case MatrixSymmetry::symmetric:
    file << "symmetric";
    break;
  case MatrixSymmetry::hermitian:
    file << "hermitian";
    break;
  }
  file << '\n';

  file << "%generated from composyx\n";

  file << std::to_string(m) << ' ' << std::to_string(n) << ' '
       << std::to_string(i.size()) << '\n';

  for (size_t k = 0; k < i.size(); ++k) {
    file << i[k] + 1 << ' ' << j[k] + 1 << ' ' << scalar_to_str(values[k])
         << '\n';
  }
}

template <class ScalArray>
inline void dump(const std::string& filename, const ScalArray& values,
                 const size_t m, const size_t n) {
  using Scalar = typename ScalArray::value_type;

  std::ofstream file{filename, std::ios::out};
  COMPOSYX_ASSERT(file, filename + ": could not be opened");

  file << "%%MatrixMarket matrix array ";

  if constexpr (is_real<Scalar>::value) {
    file << "real ";
  } else {
    file << "complex ";
  }

  file << "general\n";
  file << "%generated from composyx\n";
  file << std::to_string(m) << ' ' << std::to_string(n) << '\n';

  for (auto& v : values) {
    file << scalar_to_str(v) << '\n';
  }
}
// Dumping to file:1 ends here

// [[file:../../../org/composyx/IO/MatrixMarketLoader.org::*Footer][Footer:1]]
} // end namespace matrix_market
} // end namespace composyx
// Footer:1 ends here
