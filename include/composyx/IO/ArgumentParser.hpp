// [[file:../../../org/composyx/IO/ArgumentParser.org::*Header][Header:1]]
#pragma once

#include <iostream>
#include <map>
#include <any>
#include <string>

using std::string;

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/IO/ArgumentParser.org::*Parse arguments][Parse arguments:1]]
std::map<string, string> parse_arguments(int argc, char** argv) {

  std::map<string, string> arguments;

  if (argc <= 1)
    return arguments;

  // Ex: --parameter
  auto dashdash = [](const string& s) {
    return s.size() >= 3 and s[0] == '-' && s[1] == '-';
  };

  // Ex: -P
  auto onedash = [](const string& s) {
    return s.size() == 2 and s[0] == '-' && s[1] != '-';
  };

  string key;
  for (int i = 1; i < argc; ++i) {
    string word(argv[i]);

    if (dashdash(word)) {
      key = word.substr(2, word.size());
      arguments[key] = string("");
    } else if (onedash(word)) {
      key = word.substr(1, 2);
      arguments[key] = string("");
    } else if (!key.empty()) {
      arguments[key] = word;
      key.clear();
    }
  }

  return arguments;
}
// Parse arguments:1 ends here

// [[file:../../../org/composyx/IO/ArgumentParser.org::*Argument class][Argument class:1]]
struct Argument {
  string key;
  string description;
  char char_key = '-';

  Argument() : key{string()}, description{string()}, char_key{'-'} {}
  Argument(string _key, string _description, char _char_key)
      : key{_key}, description{_description}, char_key{_char_key} {}
}; // struct Argument
// Argument class:1 ends here

// [[file:../../../org/composyx/IO/ArgumentParser.org::*ArgumentList class][ArgumentList class:1]]
enum class ArgumentType { t_int, t_real, t_str, t_dir, t_file, t_bool };

string ArgumentType_strval(const ArgumentType& type, const std::any val) {
  string out;

  switch (type) {
  case ArgumentType::t_int:
    out = std::to_string(std::any_cast<int>(val));
    break;
  case ArgumentType::t_real:
    out = std::to_string(std::any_cast<double>(val));
    break;
  case ArgumentType::t_str:
    out = std::any_cast<string>(val);
    break;
  case ArgumentType::t_dir:
    out = std::any_cast<string>(val);
    break;
  case ArgumentType::t_file:
    out = std::any_cast<string>(val);
    break;
  case ArgumentType::t_bool:
    break;
  }

  return out;
}

string ArgumentType_strtype(const ArgumentType& type) {
  string out;

  switch (type) {
  case ArgumentType::t_int:
    out = string("int ");
    break;
  case ArgumentType::t_real:
    out = string("real");
    break;
  case ArgumentType::t_str:
    out = string("str ");
    break;
  case ArgumentType::t_dir:
    out = string("dir ");
    break;
  case ArgumentType::t_file:
    out = string("file");
    break;
  case ArgumentType::t_bool:
    out = string("bool");
    break;
  }

  return out;
}

struct ArgumentList {
  std::string command_descr;

  std::map<string, ArgumentType> arg_types;
  std::map<string, std::any> arg_values;
  std::map<string, std::vector<string>> possible_values;

  std::map<string, Argument> argument_dict;
  std::vector<string> argument_in_order; // To pring beautiful help

  void command_description(const std::string& cd) { command_descr = cd; }

  void command_description(const char* cd) { command_descr = std::string(cd); }

  void add_argument_int(const char* key, const char* description,
                        int default_value, char char_key = '-') {
    const string skey(key);
    argument_in_order.push_back(key);
    arg_values[skey] = default_value;
    arg_types[skey] = ArgumentType::t_int;
    argument_dict[skey] = Argument(key, std::string(description), char_key);
  }

  void add_argument_str(const char* key, const char* description,
                        string default_value, char char_key = '-') {
    const string skey(key);
    argument_in_order.push_back(key);
    arg_values[skey] = default_value;
    arg_types[skey] = ArgumentType::t_str;
    argument_dict[skey] = Argument(key, std::string(description), char_key);
  }

  void add_argument_dir(const char* key, const char* description,
                        string default_value, char char_key = '-') {
    const string skey(key);
    argument_in_order.push_back(key);
    arg_values[skey] = default_value;
    arg_types[skey] = ArgumentType::t_dir;
    argument_dict[skey] = Argument(key, std::string(description), char_key);
  }

  void add_argument_file(const char* key, const char* description,
                         string default_value, char char_key = '-') {
    const string skey(key);
    argument_in_order.push_back(key);
    arg_values[skey] = default_value;
    arg_types[skey] = ArgumentType::t_file;
    argument_dict[skey] = Argument(key, std::string(description), char_key);
  }

  void add_argument_real(const char* key, const char* description,
                         double default_value, char char_key = '-') {
    const string skey(key);
    argument_in_order.push_back(key);
    arg_values[skey] = default_value;
    arg_types[skey] = ArgumentType::t_real;
    argument_dict[skey] = Argument(key, std::string(description), char_key);
  }

  void add_argument_bool(const char* key, const char* description,
                         char char_key = '-') {
    const string skey(key);
    argument_in_order.push_back(key);
    arg_values[skey] = false;
    arg_types[skey] = ArgumentType::t_bool;
    argument_dict[skey] = Argument(key, std::string(description), char_key);
  }

  void add_possible_values(const char* key,
                           const std::vector<string>& possible_vals) {
    const string skey(key);
    possible_values[key] = possible_vals;
  }

  void add_possible_values(const char* key,
                           std::vector<string>&& possible_vals) {
    const string skey(key);
    possible_values[key] = std::move(possible_vals);
  }
  // ArgumentList class:1 ends here

  // [[file:../../../org/composyx/IO/ArgumentParser.org::*Generate usage information][Generate usage information:1]]
  string usage() const {
    auto descr = [this](const Argument& arg, const string& def_val) {
      string out_line("");
      if (arg.char_key != '-') {
        out_line += string("-") + string(1, arg.char_key) + string(", ");
      }
      out_line += string("--") + arg.key + string(" : ");
      out_line += arg.description;

      if (possible_values.contains(arg.key)) {
        out_line += string("\n -> Possible values: [ ");
        for (const auto& pv : possible_values.at(arg.key)) {
          out_line += string(pv) + string(" ");
        }
        out_line.pop_back(); // Remove last semi-column
        out_line += string(" ]\n");
      }

      if (!def_val.empty()) {
        out_line += string(" (default ") + def_val + string(")");
      }

      out_line += string("\n");
      return out_line;
    };

    string out_str;
    out_str += std::string("\n") + command_descr + std::string("\n");

    for (auto& key : argument_in_order) {
      if (arg_values.contains(key)) {
        const Argument& arg = argument_dict.at(key);
        const string type_str = ArgumentType_strtype(arg_types.at(key));
        const string val_str =
            ArgumentType_strval(arg_types.at(key), arg_values.at(key));
        out_str += string("<") + type_str + string("> ");
        out_str += descr(arg, val_str);
      }
    }

    return out_str;
  }
  // Generate usage information:1 ends here

  // [[file:../../../org/composyx/IO/ArgumentParser.org::*Generate template for the parameter file][Generate template for the parameter file:1]]
  void generate_template_file(const string& filename,
                              const std::vector<string>& hide_keys) const {
    std::ofstream f{filename, std::ios::out};
    if (!f.is_open()) {
      COMPOSYX_ASSERT(f, filename + ": file not opened");
    }

    auto comment_description = [this](const string& desc, const string& key) {
      std::stringstream ssin{desc};
      std::stringstream ssout;
      const string type_str = ArgumentType_strtype(arg_types.at(key));
      for (std::string line; std::getline(ssin, line, '\n');) {
        ssout << "# (" << type_str << ") " << line << '\n';
      }
      return ssout.str();
    };

    for (auto& key : argument_in_order) {
      if (std::ranges::find(hide_keys, key) != hide_keys.end())
        continue;

      if (arg_values.contains(key)) {
        const Argument& arg = argument_dict.at(key);
        const string val_str =
            ArgumentType_strval(arg_types.at(key), arg_values.at(key));
        f << comment_description(arg.description, key);
        f << key << ": " << val_str << "\n\n";
      }
    }
  }
  // Generate template for the parameter file:1 ends here

  // [[file:../../../org/composyx/IO/ArgumentParser.org::*Fill values][Fill values:1]]
  template <typename T>
  void _generic_fill_value(const std::map<string, string>& parsed_args,
                           const char* ckey, T& v,
                           std::function<T(const string&)> stoT) const {

    const string key = std::string(ckey);
    if ((!argument_dict.contains(key)) or (!arg_values.contains(key))) {
      COMPOSYX_WARNING("ArgumentList::fill_value - unknown parameter: " + key);
      return;
    }
    const auto& arg = argument_dict.at(key);

    if (parsed_args.contains(arg.key)) {
      v = stoT(parsed_args.at(arg.key));
      return;
    }
    if (arg.char_key != '-' &&
        (parsed_args.count(std::string(1, arg.char_key)))) {
      v = stoT(parsed_args.at(std::string(1, arg.char_key)));
      return;
    }
    v = std::any_cast<T>(arg_values.at(key));
  }

  void fill_value(const std::map<string, string>& parsed_args, const char* ckey,
                  int& v) const {
    try {
      _generic_fill_value<int>(parsed_args, ckey, v,
                               [](const string& s) { return std::stoi(s); });
    } catch (std::invalid_argument&) {
      const auto key = string(ckey);
      const auto& value = parsed_args.at(key);
      COMPOSYX_WARNING(
          "Parameter - failed to convert to integer, key / value: " + key +
          string(" / ") + value);
    }
  }

  void fill_value(const std::map<string, string>& parsed_args, const char* ckey,
                  string& v) const {
    _generic_fill_value<string>(parsed_args, ckey, v,
                                [](const string& s) { return s; });
  }

  void fill_value(const std::map<string, string>& parsed_args, const char* ckey,
                  double& v) const {
    try {
      _generic_fill_value<double>(parsed_args, ckey, v,
                                  [](const string& s) { return std::stod(s); });
    } catch (std::invalid_argument&) {
      const auto key = string(ckey);
      const auto& value = parsed_args.at(key);
      COMPOSYX_WARNING(
          "Parameter - failed to convert to real (double), key / value: " +
          key + string(" / ") + value);
    }
  }

  // For booleans, the presence of the key means true, unless the value given is "0" or "false"
  void fill_value(const std::map<string, string>& parsed_args, const char* ckey,
                  bool& v) const {
    v = false;
    auto str_to_bool = [](const string& s) {
      if (s == string("false")) {
        return false;
      } else if (s == string("0")) {
        return false;
      }
      return true;
    };
    _generic_fill_value<bool>(parsed_args, ckey, v, str_to_bool);
  }
  // Fill values:1 ends here

  // [[file:../../../org/composyx/IO/ArgumentParser.org::*Checking unknown parameters][Checking unknown parameters:1]]
  void unknown_parameters(const std::map<string, string>& parsed_args) const {
    for (const auto& [parsed_key, parsed_value] : parsed_args) {
      bool found = false;
      for (const auto& [dict_key, dict_value] : argument_dict) {
        if (parsed_key == dict_key or
            parsed_key == string(1, dict_value.char_key)) {
          found = true;
          break;
        }
      }
      if (!found)
        COMPOSYX_WARNING("Unknown parameter: \"" + parsed_key +
                         string("\" - value: " + parsed_value));
    }
  }
  // Checking unknown parameters:1 ends here

  // [[file:../../../org/composyx/IO/ArgumentParser.org::*Generate bash completion][Generate bash completion:2]]
  static bool generate_bash_completion(int argc, char** argv,
                                       const ArgumentList& arglist,
                                       const string& command_name) {
    if (argc == 3 and
        std::string(argv[1]) == std::string("--bash-completion")) {
      std::string bash_comp_fname = std::string(argv[2]);
      std::ofstream out{bash_comp_fname, std::ios::out};
      if (!out.is_open()) {
        COMPOSYX_ASSERT(out, bash_comp_fname + " : file not opened");
      }

      out << '_' << command_name << "_completion() {\n";
      out << "  " << "local cur prev file_args dir_args opts";

      // Local variables for possible values
      for (const auto& [pv_key, _] : arglist.possible_values) {
        out << " pv_" << pv_key;
      }

      out << "\n";
      out << "  cur=\"${COMP_WORDS[COMP_CWORD]}\"\n";
      out << "  prev=\"${COMP_WORDS[COMP_CWORD-1]}\"\n";

      std::vector<string> fileargs;
      std::vector<string> dirargs;
      std::vector<string> otherargs;

      for (auto& key : arglist.argument_in_order) {
        if (arglist.arg_types.at(key) == ArgumentType::t_file) {
          fileargs.push_back(key);
        } else if (arglist.arg_types.at(key) == ArgumentType::t_dir) {
          dirargs.push_back(key);
        } else {
          otherargs.push_back(key);
        }
      }

      auto write_arg_list = [&](const char* var_name,
                                const std::vector<string>& keys) {
        out << "  " << var_name << "=\"";
        for (auto& key : keys) {
          const Argument& arg = arglist.argument_dict.at(key);
          if (arg.char_key != '-') {
            out << '-' << arg.char_key << ' ';
          }
          out << "--" << arg.key << ' ';
        }
        out << "\"\n";
      };

      write_arg_list("file_args", fileargs);
      write_arg_list("dir_args", dirargs);
      write_arg_list("other_args", otherargs);

      out << "  " << "opts=\"${file_args} ${dir_args} ${other_args}\"\n";

      // Variables for parameter with possible values
      for (const auto& [pv_key, pv_vals] : arglist.possible_values) {
        out << "  pv_" << pv_key << "=\"";
        for (const string& pv : pv_vals) {
          out << pv << " ";
        }
        out << "\"\n";
      }

      out << "  if [[ \" ${file_args} \" =~ \" ${prev} \" ]]; then\n";
      out << "      COMPREPLY=( $(compgen -f -- \"${cur}\") )\n";
      out << "  elif [[ \" ${dir_args} \" =~ \" ${prev} \" ]]; then\n";
      out << "      COMPREPLY=( $(compgen -d -- \"${cur}\") )\n";

      for (const auto& [pv_key, pv_vals] : arglist.possible_values) {
        const Argument& arg = arglist.argument_dict.at(pv_key);
        string var_name("pv_");
        var_name += pv_key;
        out << "  " << "elif [[ \"${prev}\" == \"--" << arg.key << "\" ";
        if (arg.char_key != '-') {
          out << "|| \"${prev}\" == \"-" << arg.char_key << "\" ";
        }
        out << "]]; then\n";
        out << "      " << "COMPREPLY=( $(compgen -W \"${" << var_name
            << "}\" -- \"${cur}\") )\n";
      }

      out << "  else\n";
      out << "      COMPREPLY=( $(compgen -W \"${opts}\" -- \"${cur}\") )\n";
      out << "  fi\n";
      out << "}\n";
      out << "complete -F _" << command_name << "_completion " << command_name
          << '\n';

      return true;
    }

    return false;
  }
  // Generate bash completion:2 ends here

  // [[file:../../../org/composyx/IO/ArgumentParser.org::*Clear content][Clear content:1]]
  void clear() {
    command_descr.clear();
    arg_types.clear();
    arg_values.clear();
    argument_dict.clear();
    argument_in_order.clear();
    possible_values.clear();
  }
  // Clear content:1 ends here

  // [[file:../../../org/composyx/IO/ArgumentParser.org::*Footer][Footer:1]]
}; //struct ArgumentList
} // namespace composyx
// Footer:1 ends here
