// [[file:../../../org/composyx/interfaces/basic_concepts.org::*Header][Header:1]]
#pragma once

#include "Common.hpp"

#include <type_traits>
#include <complex>
#include "../utils/Arithmetic.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/interfaces/basic_concepts.org::*Basic types][Basic types:1]]
// Integral type
template <class T>
concept CPX_Integral = std::is_integral<T>::value;
// Basic types:1 ends here

// [[file:../../../org/composyx/interfaces/basic_concepts.org::*Basic types][Basic types:2]]
// Scalar type
template <typename S>
concept CPX_Scalar = std::is_same_v<S, float> || std::is_same_v<S, double> ||
                     std::is_same_v<S, std::complex<float>> ||
                     std::is_same_v<S, std::complex<double>>;

// Real type
template <typename S>
concept CPX_Real = std::is_same_v<S, float> || std::is_same_v<S, double>;

// Complex type
template <typename S>
concept CPX_Complex = std::is_same_v<S, std::complex<float>> ||
                      std::is_same_v<S, std::complex<double>>;
// Basic types:2 ends here

// [[file:../../../org/composyx/interfaces/basic_concepts.org::*Linear object][Linear object:1]]
/*
  CPX_LinearObject
  Concept of an object that can be scaled, added substracted
  such as a vector, matrix, tensor...
*/
template <typename object>
concept CPX_LinearObject =
    requires { typename scalar_type<object>::type; } &&
    CPX_Scalar<typename scalar_type<object>::type> &&
    requires(object obj, typename scalar_type<object>::type scal) {
      { obj *= scal };
      { obj += obj };
      { obj -= obj };
      { obj* scal }; //-> std::convertible_to<object>;
      { scal* obj }; //-> std::convertible_to<object>;
      { obj + obj }; //-> std::convertible_to<object>;
      { obj - obj }; //-> std::convertible_to<object>;
    };
// Linear object:1 ends here

// [[file:../../../org/composyx/interfaces/basic_concepts.org::*Array of integer][Array of integer:1]]
template <typename T>
concept CPX_IntRef =
    requires() { requires std::integral<std::remove_reference_t<T>>; };

template <typename A>
concept CPX_IntArray = requires(A arr, int i) {
  { arr.size() } -> std::unsigned_integral;
  { arr[i] } -> CPX_IntRef;
};
// Array of integer:1 ends here

// [[file:../../../org/composyx/interfaces/basic_concepts.org::*Array of constrained type][Array of constrained type:1]]
template <typename T, typename S>
concept CPX_Refof = requires() {
  requires std::is_same_v<S, std::remove_cv_t<std::remove_reference_t<T>>>;
};

template <typename A, typename S>
concept CPX_Arrayof = requires(A arr, int i) {
  { arr.size() } -> std::unsigned_integral;
  { arr[i] } -> CPX_Refof<S>;
};
// Array of constrained type:1 ends here

// [[file:../../../org/composyx/interfaces/basic_concepts.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
