// [[file:../../../org/composyx/interfaces/implicit_sparse_solver.org::*Header][Header:1]]
#pragma once
#include <variant>
#include <optional>

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_sparse_solver.org::*Default dynamic selection][Default dynamic selection:1]]
enum class sparse_solver_type { NONE, MUMPS, PASTIX, QRMUMPS };

namespace default_sparse_solver {

#if defined(COMPOSYX_USE_MUMPS)
static const sparse_solver_type package = sparse_solver_type::MUMPS;
#elif defined(COMPOSYX_USE_PASTIX)
static const sparse_solver_type package = sparse_solver_type::PASTIX;
#elif defined(COMPOSYX_USE_QRMUMPS)
static const sparse_solver_type package = sparse_solver_type::QRMUMPS;
#else
static const sparse_solver_type package = sparse_solver_type::NONE;
#endif

} // namespace default_sparse_solver

namespace select_sparse_solver {
static std::optional<sparse_solver_type> package;
}

} // namespace composyx
// Default dynamic selection:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_sparse_solver.org::*Include available solvers][Include available solvers:1]]
#if defined(COMPOSYX_USE_MUMPS)
#include "composyx/solver/Mumps.hpp"
#endif // COMPOSYX_USE_MUMPS

#if defined(COMPOSYX_USE_PASTIX)
#include "composyx/solver/Pastix.hpp"
#endif // COMPOSYX_USE_PASTIX

#if defined(COMPOSYX_USE_QRMUMPS)
#include "composyx/solver/QrMumps.hpp"
#endif // COMPOSYX_USE_QRMUMPS
// Include available solvers:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_sparse_solver.org::*Checking compile-time available solvers][Checking compile-time available solvers:1]]
namespace composyx {

// Cross product (mumps x pastix x qrmumps)

// 000 No solver
#if !defined(COMPOSYX_USE_MUMPS) && !defined(COMPOSYX_USE_PASTIX) &&           \
    !defined(COMPOSYX_USE_QRMUMPS)
template <typename Matrix, typename Vector>
using sparse_sol_var = std::variant<composyx::Identity<Matrix, Vector>>;
#define COMPOSYX_STATIC_SPARSE_SOLVER_TYPE "NONE"
#endif

// 001 QRMUMPS only
#if !defined(COMPOSYX_USE_MUMPS) && !defined(COMPOSYX_USE_PASTIX) &&           \
    defined(COMPOSYX_USE_QRMUMPS)
template <typename Matrix, typename Vector>
using sparse_sol_var = std::variant<composyx::QrMumps<Matrix, Vector>>;
#define COMPOSYX_STATIC_SPARSE_SOLVER_TYPE "QRMUMPS"
#endif

// 010 PASTIX only
#if !defined(COMPOSYX_USE_MUMPS) && defined(COMPOSYX_USE_PASTIX) &&            \
    !defined(COMPOSYX_USE_QRMUMPS)
template <typename Matrix, typename Vector>
using sparse_sol_var = std::variant<composyx::Pastix<Matrix, Vector>>;
#define COMPOSYX_STATIC_SPARSE_SOLVER_TYPE "PASTIX"
#endif

// 011 PASTIX + QRMUMPS
#if !defined(COMPOSYX_USE_MUMPS) && defined(COMPOSYX_USE_PASTIX) &&            \
    defined(COMPOSYX_USE_QRMUMPS)
template <typename Matrix, typename Vector>
using sparse_sol_var = std::variant<composyx::Pastix<Matrix, Vector>,
                                    composyx::QrMumps<Matrix, Vector>>;
#endif

// 100 MUMPS only
#if defined(COMPOSYX_USE_MUMPS) && !defined(COMPOSYX_USE_PASTIX) &&            \
    !defined(COMPOSYX_USE_QRMUMPS)
template <typename Matrix, typename Vector>
using sparse_sol_var = std::variant<composyx::Mumps<Matrix, Vector>>;
#define COMPOSYX_STATIC_SPARSE_SOLVER_TYPE "QRMUMPS"
#endif

// 101 MUMPS + QRMUMPS
#if defined(COMPOSYX_USE_MUMPS) && !defined(COMPOSYX_USE_PASTIX) &&            \
    defined(COMPOSYX_USE_QRMUMPS)
template <typename Matrix, typename Vector>
using sparse_sol_var = std::variant<composyx::Mumps<Matrix, Vector>,
                                    composyx::QrMumps<Matrix, Vector>>;
#endif

// 110 MUMPS + PASTIX
#if defined(COMPOSYX_USE_MUMPS) && defined(COMPOSYX_USE_PASTIX) &&             \
    !defined(COMPOSYX_USE_QRMUMPS)
template <typename Matrix, typename Vector>
using sparse_sol_var = std::variant<composyx::Mumps<Matrix, Vector>,
                                    composyx::Pastix<Matrix, Vector>>;
#endif

// 111 All possibilities
#if defined(COMPOSYX_USE_MUMPS) && defined(COMPOSYX_USE_PASTIX) &&             \
    defined(COMPOSYX_USE_QRMUMPS)
template <typename Matrix, typename Vector>
using sparse_sol_var = std::variant<composyx::Mumps<Matrix, Vector>,
                                    composyx::Pastix<Matrix, Vector>,
                                    composyx::QrMumps<Matrix, Vector>>;
#endif
// Checking compile-time available solvers:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_sparse_solver.org::*Solvers interface][Solvers interface:1]]
template <typename Matrix, typename Vector = vector_type<Matrix>::type>
class COMPOSYX_SPARSE_SOLVER {
  sparse_sol_var<Matrix, Vector> solver;

public:
  using matrix_type = Matrix;
  using vector_type = Vector;
  using scalar_type = typename Vector::value_type;

  COMPOSYX_SPARSE_SOLVER() {}

  COMPOSYX_SPARSE_SOLVER([[maybe_unused]] const Matrix& A) {

    sparse_solver_type sp = (select_sparse_solver::package)
                                ? select_sparse_solver::package.value()
                                : default_sparse_solver::package;

#if defined(COMPOSYX_USE_MUMPS)
    if (sp == sparse_solver_type::MUMPS) {
      solver = composyx::Mumps<Matrix, Vector>(A);
    }
#endif // defined(COMPOSYX_USE_MUMPS)

#if defined(COMPOSYX_USE_PASTIX)
    if (sp == sparse_solver_type::PASTIX) {
      solver = composyx::Pastix<Matrix, Vector>(A);
    }
#endif // defined(COMPOSYX_USE_PASTIX)

#if defined(COMPOSYX_USE_QRMUMPS)
    if (sp == sparse_solver_type::QRMUMPS) {
      solver = composyx::QrMumps<Matrix, Vector>(A);
    }
#endif // defined(COMPOSYX_USE_QRMUMPS)

    COMPOSYX_ASSERT(sp != sparse_solver_type::NONE,
                    "No sparse direct solver available");
  }

  void setup(const Matrix& A) {
    return std::visit([&](auto& s) { return s.setup(A); }, this->solver);
  }

  void setup(Matrix&& A) {
    return std::visit([&](auto& s) { return s.setup(std::move(A)); },
                      this->solver);
  }

  Vector apply(const Vector& rhs) {
    return std::visit([&](auto& s) { return s * rhs; }, this->solver);
  }

  friend Vector operator*(const COMPOSYX_SPARSE_SOLVER& cpx_solver,
                          const Vector& rhs) {
    return std::visit([&](auto& s) { return s * rhs; }, cpx_solver.solver);
  }
}; // COMPOSYX_SPARSE_SOLVER

template <typename Matrix, typename Vector>
struct matrix_type<COMPOSYX_SPARSE_SOLVER<Matrix, Vector>> {
  using type = Matrix;
};
template <typename Matrix, typename Vector>
struct vector_type<COMPOSYX_SPARSE_SOLVER<Matrix, Vector>> {
  using type = Vector;
};
template <typename Matrix, typename Vector>
struct scalar_type<COMPOSYX_SPARSE_SOLVER<Matrix, Vector>> {
  using type = typename Vector::value_type;
};
// Solvers interface:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_sparse_solver.org::*Info on the current solver / solvers][Info on the current solver / solvers:1]]
#if defined(COMPOSYX_STATIC_SPARSE_SOLVER_TYPE)

inline std::string _value_current_implicit_sparse_solver() {
  return std::string(COMPOSYX_STATIC_SPARSE_SOLVER_TYPE);
}

inline void info_implicit_sparse_solver(std::ostream& sout = std::cout) {
  sout << "Sparse solver defined at compilation time to: "
       << COMPOSYX_STATIC_SPARSE_SOLVER_TYPE << '\n';
}

#else

inline std::string _value_current_implicit_sparse_solver() {

  sparse_solver_type sp = (select_sparse_solver::package)
                              ? select_sparse_solver::package.value()
                              : default_sparse_solver::package;

  if (sp == sparse_solver_type::MUMPS) {
    return std::string("MUMPS");
  } else if (sp == sparse_solver_type::PASTIX) {
    return std::string("PASTIX");
  } else if (sp == sparse_solver_type::QRMUMPS) {
    return std::string("QRMUMPS");
  }

  return std::string("NONE");
}

inline void info_implicit_sparse_solver(std::ostream& sout = std::cout) {
  sout << "Sparse solver current value: "
       << _value_current_implicit_sparse_solver() << '\n';
}
#endif // defined(COMPOSYX_STATIC_SPARSE_SOLVER_TYPE)
// Info on the current solver / solvers:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_sparse_solver.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
