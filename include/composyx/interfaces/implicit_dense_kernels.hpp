// [[file:../../../org/composyx/interfaces/implicit_dense_kernels.org::*Header][Header:1]]
#pragma once
#include <optional>
#include <variant>

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_dense_kernels.org::*Composyx BLAS kernel interface][Composyx BLAS kernel interface:1]]
struct COMPOSYX_BLAS {
  template <typename Matrix, typename Vect1, typename Vect2,
            CPX_Scalar Scalar = typename Matrix::scalar_type>
  static void gemv(const Matrix& A, const Vect1& X, Vect2& Y, char opA = 'N',
                   Scalar alpha = 1, Scalar beta = 0);

  template <typename Matrix, typename Vect1, typename Vect2,
            CPX_Scalar Scalar = typename Matrix::scalar_type>
  static void geru(Matrix& A, const Vect1& X, const Vect2& Y, Scalar alpha = 1);

  template <typename Matrix, typename Vect1, typename Vect2,
            CPX_Scalar Scalar = typename Matrix::scalar_type>
  static void ger(Matrix& A, const Vect1& X, const Vect2& Y, Scalar alpha = 1);

  template <typename Vector, CPX_Scalar Scalar>
  static void scal(Vector& X, Scalar alpha);

  template <typename Vector, CPX_Scalar Scalar>
  static void axpy(const Vector& X, Vector& Y, Scalar alpha);

  template <typename Vect1, typename Vect2,
            CPX_Scalar Scalar = typename Vect1::scalar_type>
  static Scalar dot(const Vect1& X, const Vect2& Y);

  template <typename Vect1, typename Vect2,
            CPX_Scalar Scalar = typename Vect1::scalar_type>
  static Scalar dotu(const Vect1& X, const Vect2& Y);

  template <typename Vector, typename Real = typename Vector::real_type>
  static Real norm2_squared(const Vector& X);

  template <typename Vector, typename Real = typename Vector::real_type>
  static Real norm2(const Vector& X);

  template <typename Mat1, typename Mat2, typename Mat3,
            CPX_Scalar Scalar = typename Mat1::scalar_type>
  static void gemm(const Mat1& A, const Mat2& B, Mat3& C, char opA = 'N',
                   char opB = 'N', Scalar alpha = 1, Scalar beta = 0);

  template <typename Matrix,
            typename ComplexMatrix = typename complex_type<Matrix>::type,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Complex Complex = typename ComplexMatrix::scalar_type>
  [[nodiscard]] static std::pair<std::vector<Complex>, ComplexMatrix>
  eig(const Matrix& A);

  template <typename Matrix,
            typename ComplexMatrix = typename complex_type<Matrix>::type,
            CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Complex Complex = typename ComplexMatrix::scalar_type>
  [[nodiscard]] static std::vector<Complex> eigvals(const Matrix& A);

  template <typename Matrix, CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  eigh(const Matrix& A);

  template <typename Matrix, CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::vector<Real> eigvalsh(const Matrix& A);

  template <typename Matrix, CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, DenseMatrix<Scalar>>
  eigh_select(const Matrix& A, int first_idx, int last_idx);

  template <typename Matrix, CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, DenseMatrix<Scalar>>
  eigh_smallest(const Matrix& A, int n_v);

  template <typename Matrix, CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, DenseMatrix<Scalar>>
  eigh_largest(const Matrix& A, int n_v);

  template <typename Matrix, CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  gen_eigh_select(const Matrix& A, const Matrix& B, int first_idx,
                  int last_idx);

  template <typename Matrix, CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  gen_eigh_smallest(const Matrix& A, const Matrix& B, int n_v);

  template <typename Matrix, CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  gen_eigh_largest(const Matrix& A, const Matrix& B, int n_v);

  template <typename Matrix, CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::tuple<Matrix, std::vector<Real>, Matrix>
  svd(const Matrix& A, bool reduced = true);

  // Returns U and sigmas only
  template <typename Matrix, CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<Matrix, std::vector<Real>>
  svd_left_sv(const Matrix& A, bool reduced = true);
  // Returns sigmas and Vt only
  template <typename Matrix, CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  svd_right_sv(const Matrix& A, bool reduced = true);
  // Returns only singular values
  template <typename Matrix, CPX_Scalar Scalar = typename Matrix::scalar_type,
            CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::vector<Real> svdvals(const Matrix& A);
}; // COMPOSYX_BLAS
// Composyx BLAS kernel interface:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_dense_kernels.org::*Default dynamic selection][Default dynamic selection:1]]
constexpr bool static_strings_equal(char const* a, char const* b) {
  return std::string_view(a) == b;
}

enum class dense_kernel_type { LAPACKPP, TLAPACK, CHAMELEON };

namespace default_dense_kernel {

#if defined(COMPOSYX_USE_LAPACKPP)
static const dense_kernel_type package = dense_kernel_type::LAPACKPP;
#else
static const dense_kernel_type package = dense_kernel_type::TLAPACK;
#endif

} // namespace default_dense_kernel

namespace select_dense_kernel {
static std::optional<dense_kernel_type> package;
}

} // namespace composyx
// Default dynamic selection:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_dense_kernels.org::*Include available solvers][Include available solvers:1]]
#include "composyx/loc_data/DenseData.hpp"

#if defined(COMPOSYX_USE_LAPACKPP)

#include "composyx/kernel/BlasKernels.hpp"
#include "composyx/solver/BlasSolver.hpp"

#endif // COMPOSYX_USE_LAPACKPP undefined

#if defined(COMPOSYX_USE_TLAPACK)

#include "composyx/kernel/TlapackKernels.hpp"
#include "composyx/solver/TlapackSolver.hpp"

#endif // COMPOSYX_USE_LAPACKPP

#if defined(COMPOSYX_USE_CHAMELEON)

#include "composyx/kernel/ChameleonKernels.hpp"
#include "composyx/solver/ChameleonSolver.hpp"

#endif // COMPOSYX_USE_CHAMELEON
// Include available solvers:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_dense_kernels.org::*Checking compile-time available kernels][Checking compile-time available kernels:1]]
namespace composyx {

// Cross product (lapackpp x chameleon x tlapack)

// 000 No solver
#if !defined(COMPOSYX_USE_LAPACKPP) && !defined(COMPOSYX_USE_CHAMELEON) &&     \
    !defined(COMPOSYX_USE_TLAPACK)
static_assert(false, "No dense blas/lapack found (LAPACKPP/CHAMELEON/TLAPACK)");
#endif

// 001 Tlapack only
#if !defined(COMPOSYX_USE_LAPACKPP) && !defined(COMPOSYX_USE_CHAMELEON) &&     \
    defined(COMPOSYX_USE_TLAPACK)
using dense_ker_var = std::variant<composyx::tlapack_kernels>;
template <typename Matrix, typename Vector>
using dense_sol_var = std::variant<composyx::TlapackSolver<Matrix, Vector>>;
#define COMPOSYX_STATIC_DENSE_KERNEL_TYPE "TLAPACK"
#endif

// 010 Chameleon only (not valid)
#if !defined(COMPOSYX_USE_LAPACKPP) && defined(COMPOSYX_USE_CHAMELEON) &&      \
    !defined(COMPOSYX_USE_TLAPACK)
static_assert(false, "COMPOSYX_USE_CHAMELEON requires COMPOSYX_USE_LAPACKPP");
#endif

// 011 Chameleon + Tlapack (not valid)
#if !defined(COMPOSYX_USE_LAPACKPP) && defined(COMPOSYX_USE_CHAMELEON) &&      \
    defined(COMPOSYX_USE_TLAPACK)
static_assert(false, "COMPOSYX_USE_CHAMELEON requires COMPOSYX_USE_LAPACKPP");
#endif

// 100 Lapackpp only
#if defined(COMPOSYX_USE_LAPACKPP) && !defined(COMPOSYX_USE_CHAMELEON) &&      \
    !defined(COMPOSYX_USE_TLAPACK)
using dense_ker_var = std::variant<composyx::blas_kernels>;
template <typename Matrix, typename Vector>
using dense_sol_var = std::variant<composyx::BlasSolver<Matrix, Vector>>;
#define COMPOSYX_STATIC_DENSE_KERNEL_TYPE "LAPACKPP"
#endif

// 101 No chameleon
#if defined(COMPOSYX_USE_LAPACKPP) && !defined(COMPOSYX_USE_CHAMELEON) &&      \
    defined(COMPOSYX_USE_TLAPACK)
using dense_ker_var =
    std::variant<composyx::blas_kernels, composyx::tlapack_kernels>;
template <typename Matrix, typename Vector>
using dense_sol_var = std::variant<composyx::BlasSolver<Matrix, Vector>,
                                   composyx::TlapackSolver<Matrix, Vector>>;
#endif

// 110 No tlapack
#if defined(COMPOSYX_USE_LAPACKPP) && defined(COMPOSYX_USE_CHAMELEON) &&       \
    !defined(COMPOSYX_USE_TLAPACK)
using dense_ker_var =
    std::variant<composyx::blas_kernels, composyx::chameleon_kernels>;
template <typename Matrix, typename Vector>
using dense_sol_var = std::variant<composyx::BlasSolver<Matrix, Vector>,
                                   composyx::ChameleonSolver<Matrix, Vector>>;
#endif

// 111 All possibilities
#if defined(COMPOSYX_USE_LAPACKPP) && defined(COMPOSYX_USE_CHAMELEON) &&       \
    defined(COMPOSYX_USE_TLAPACK)
using dense_ker_var =
    std::variant<composyx::blas_kernels, composyx::chameleon_kernels,
                 composyx::tlapack_kernels>;
template <typename Matrix, typename Vector>
using dense_sol_var = std::variant<composyx::BlasSolver<Matrix, Vector>,
                                   composyx::ChameleonSolver<Matrix, Vector>,
                                   composyx::TlapackSolver<Matrix, Vector>>;
#endif
// Checking compile-time available kernels:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_dense_kernels.org::*Static selection][Static selection:1]]
// Special case
// - Only one solver available
// - We force to use only one with COMPOSYX_STATIC_DENSE_KERNEL_TYPE
#if defined(COMPOSYX_STATIC_DENSE_KERNEL_TYPE)
consteval auto static_select_implicit_dense_kernels() {

#if defined(COMPOSYX_USE_LAPACKPP)
  if constexpr (static_strings_equal(COMPOSYX_STATIC_DENSE_KERNEL_TYPE,
                                     "LAPACKPP")) {
    return composyx::blas_kernels();
  }
#endif // defined(COMPOSYX_USE_LAPACKPP)

#if defined(COMPOSYX_USE_CHAMELEON)
  if constexpr (static_strings_equal(COMPOSYX_STATIC_DENSE_KERNEL_TYPE,
                                     "CHAMELEON")) {
    return composyx::chameleon_kernels();
  }
#endif // defined(COMPOSYX_USE_CHAMELEON)

#if defined(COMPOSYX_USE_TLAPACK)
  if constexpr (static_strings_equal(COMPOSYX_STATIC_DENSE_KERNEL_TYPE,
                                     "TLAPACK")) {
    return composyx::tlapack_kernels();
  }
#endif // defined(COMPOSYX_USE_TLAPACK)
}

static constexpr const dense_ker_var DK_var =
    static_select_implicit_dense_kernels();
inline void select_variant() {}
// Static selection:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_dense_kernels.org::*Dynamic selection][Dynamic selection:1]]
#else // Dynamic selection

static dense_ker_var DK_var;

inline void select_variant() {
  dense_kernel_type kt = (select_dense_kernel::package)
                             ? select_dense_kernel::package.value()
                             : default_dense_kernel::package;

#if defined(COMPOSYX_USE_LAPACKPP)
  if (kt == dense_kernel_type::LAPACKPP) {
    DK_var = composyx::blas_kernels();
    return;
  }
#endif // defined(COMPOSYX_USE_LAPACKPP)

#if defined(COMPOSYX_USE_CHAMELEON)
  if (kt == dense_kernel_type::CHAMELEON) {
    DK_var = composyx::chameleon_kernels();
    return;
  }
#endif // defined(COMPOSYX_USE_CHAMELEON)

#if defined(COMPOSYX_USE_TLAPACK)
  if (kt == dense_kernel_type::TLAPACK) {
    DK_var = composyx::tlapack_kernels();
    return;
  }
#endif // defined(COMPOSYX_USE_TLAPACK)

  std::cerr << "No kernel found !\n";
}
#endif // defined(COMPOSYX_STATIC_DENSE_KERNEL_TYPE)
// Dynamic selection:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_dense_kernels.org::*Kernels interface][Kernels interface:1]]
#define call_fct_DK(FCT, ...)                                                  \
  std::visit(                                                                  \
      [&](auto arg) {                                                          \
        using T = decltype(arg);                                               \
        T::FCT(__VA_ARGS__);                                                   \
      },                                                                       \
      DK_var)
#define call_fct_ret_DK(FCT, ...)                                              \
  std::visit(                                                                  \
      [&](auto arg) {                                                          \
        using T = decltype(arg);                                               \
        return T::FCT(__VA_ARGS__);                                            \
      },                                                                       \
      DK_var)

template <typename Matrix, typename Vect1, typename Vect2, CPX_Scalar Scalar>
void COMPOSYX_BLAS::gemv(const Matrix& A, const Vect1& X, Vect2& Y, char opA,
                         Scalar alpha, Scalar beta) {
  select_variant();
  call_fct_DK(gemv, A, X, Y, opA, alpha, beta);
}

template <typename Matrix, typename Vect1, typename Vect2, CPX_Scalar Scalar>
void COMPOSYX_BLAS::geru(Matrix& A, const Vect1& X, const Vect2& Y,
                         Scalar alpha) {
  select_variant();
  call_fct_DK(geru, A, X, Y, alpha);
}

template <typename Matrix, typename Vect1, typename Vect2, CPX_Scalar Scalar>
void COMPOSYX_BLAS::ger(Matrix& A, const Vect1& X, const Vect2& Y,
                        Scalar alpha) {
  select_variant();
  call_fct_DK(ger, A, X, Y, alpha);
}

template <typename Vector, CPX_Scalar Scalar>
void COMPOSYX_BLAS::scal(Vector& X, Scalar alpha) {
  select_variant();
  call_fct_DK(scal, X, alpha);
}

template <typename Vector, CPX_Scalar Scalar>
void COMPOSYX_BLAS::axpy(const Vector& X, Vector& Y, Scalar alpha) {
  select_variant();
  return call_fct_ret_DK(axpy, X, Y, alpha);
}

template <typename Vect1, typename Vect2, CPX_Scalar Scalar>
Scalar COMPOSYX_BLAS::dot(const Vect1& X, const Vect2& Y) {
  select_variant();
  return call_fct_ret_DK(dot, X, Y);
}

template <typename Vect1, typename Vect2, CPX_Scalar Scalar>
Scalar COMPOSYX_BLAS::dotu(const Vect1& X, const Vect2& Y) {
  select_variant();
  return call_fct_ret_DK(dotu, X, Y);
}

template <typename Vector, typename Real>
Real COMPOSYX_BLAS::norm2_squared(const Vector& X) {
  select_variant();
  return call_fct_ret_DK(norm2_squared, X);
}

template <typename Vector, typename Real>
Real COMPOSYX_BLAS::norm2(const Vector& X) {
  select_variant();
  return call_fct_ret_DK(norm2, X);
}

template <typename Mat1, typename Mat2, typename Mat3, CPX_Scalar Scalar>
void COMPOSYX_BLAS::gemm(const Mat1& A, const Mat2& B, Mat3& C, char opA,
                         char opB, Scalar alpha, Scalar beta) {
  select_variant();
  return call_fct_ret_DK(gemm, A, B, C, opA, opB, alpha, beta);
}

template <typename Matrix, typename ComplexMatrix, CPX_Scalar Scalar,
          CPX_Complex Complex>
[[nodiscard]] std::pair<std::vector<Complex>, ComplexMatrix>
COMPOSYX_BLAS::eig(const Matrix& A) {
  select_variant();
  return std::move(call_fct_ret_DK(eig, A));
}

template <typename Matrix, typename ComplexMatrix, CPX_Scalar Scalar,
          CPX_Complex Complex>
[[nodiscard]] std::vector<Complex> COMPOSYX_BLAS::eigvals(const Matrix& A) {
  select_variant();
  return call_fct_ret_DK(eigvals, A);
}

template <typename Matrix, CPX_Scalar Scalar, CPX_Real Real>
[[nodiscard]] std::pair<std::vector<Real>, Matrix>
COMPOSYX_BLAS::eigh(const Matrix& A) {
  select_variant();
  return std::move(call_fct_ret_DK(eigh, A));
}

template <typename Matrix, CPX_Scalar Scalar, CPX_Real Real>
[[nodiscard]] std::vector<Real> COMPOSYX_BLAS::eigvalsh(const Matrix& A) {
  select_variant();
  return call_fct_ret_DK(eigvalsh, A);
}

template <typename Matrix, CPX_Scalar Scalar, CPX_Real Real>
[[nodiscard]] std::pair<std::vector<Real>, DenseMatrix<Scalar>>
COMPOSYX_BLAS::eigh_select(const Matrix& A, int first_idx, int last_idx) {
  select_variant();
  return std::move(call_fct_ret_DK(eigh_select, A, first_idx, last_idx));
}

template <typename Matrix, CPX_Scalar Scalar, CPX_Real Real>
[[nodiscard]] std::pair<std::vector<Real>, DenseMatrix<Scalar>>
COMPOSYX_BLAS::eigh_smallest(const Matrix& A, int n_v) {
  select_variant();
  return std::move(call_fct_ret_DK(eigh_smallest, A, n_v));
}

template <typename Matrix, CPX_Scalar Scalar, CPX_Real Real>
[[nodiscard]] std::pair<std::vector<Real>, DenseMatrix<Scalar>>
COMPOSYX_BLAS::eigh_largest(const Matrix& A, int n_v) {
  select_variant();
  return std::move(call_fct_ret_DK(eigh_largest, A, n_v));
}

template <typename Matrix, CPX_Scalar Scalar, CPX_Real Real>
[[nodiscard]] std::pair<std::vector<Real>, Matrix>
COMPOSYX_BLAS::gen_eigh_select(const Matrix& A, const Matrix& B, int first_idx,
                               int last_idx) {
  select_variant();
  return std::move(call_fct_ret_DK(gen_eigh_select, A, B, first_idx, last_idx));
}

template <typename Matrix, CPX_Scalar Scalar, CPX_Real Real>
[[nodiscard]] std::pair<std::vector<Real>, Matrix>
COMPOSYX_BLAS::gen_eigh_smallest(const Matrix& A, const Matrix& B, int n_v) {
  select_variant();
  return std::move(call_fct_ret_DK(gen_eigh_select, A, B, n_v));
}

template <typename Matrix, CPX_Scalar Scalar, CPX_Real Real>
[[nodiscard]] std::pair<std::vector<Real>, Matrix>
COMPOSYX_BLAS::gen_eigh_largest(const Matrix& A, const Matrix& B, int n_v) {
  select_variant();
  return std::move(call_fct_ret_DK(gen_eigh_largest, A, B, n_v));
}

template <typename Matrix, CPX_Scalar Scalar, CPX_Real Real>
[[nodiscard]] std::tuple<Matrix, std::vector<Real>, Matrix>
COMPOSYX_BLAS::svd(const Matrix& A, bool reduced) {
  select_variant();
  return std::move(call_fct_ret_DK(svd, A, reduced));
}

// Returns U and sigmas only
template <typename Matrix, CPX_Scalar Scalar, CPX_Real Real>
[[nodiscard]] std::pair<Matrix, std::vector<Real>>
COMPOSYX_BLAS::svd_left_sv(const Matrix& A, bool reduced) {
  select_variant();
  return std::move(call_fct_ret_DK(svd_left_sv, A, reduced));
}

// Returns sigmas and Vt only
template <typename Matrix, CPX_Scalar Scalar, CPX_Real Real>
[[nodiscard]] std::pair<std::vector<Real>, Matrix>
COMPOSYX_BLAS::svd_right_sv(const Matrix& A, bool reduced) {
  select_variant();
  return std::move(call_fct_ret_DK(svd_right_sv, A, reduced));
}

// Returns only singular values
template <typename Matrix, CPX_Scalar Scalar, CPX_Real Real>
[[nodiscard]] std::vector<Real> COMPOSYX_BLAS::svdvals(const Matrix& A) {
  select_variant();
  return call_fct_ret_DK(svdvals, A);
}
// Kernels interface:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_dense_kernels.org::*Solvers interface][Solvers interface:1]]
//#define call_fct_DK(FCT, ...) std::visit([&](auto arg){ using T = decltype(arg); T::FCT(__VA_ARGS__); }, DK_var);
template <typename Matrix, typename Vector> class COMPOSYX_SOLVER_BLAS {
  dense_sol_var<Matrix, Vector> solver;

public:
  using matrix_type = Matrix;
  using vector_type = Vector;
  using scalar_type = typename Vector::value_type;

  COMPOSYX_SOLVER_BLAS() {}

  COMPOSYX_SOLVER_BLAS(const Matrix& A) {

    dense_kernel_type kt = (select_dense_kernel::package)
                               ? select_dense_kernel::package.value()
                               : default_dense_kernel::package;

#if defined(COMPOSYX_USE_LAPACKPP)
    if (kt == dense_kernel_type::LAPACKPP) {
      solver = composyx::BlasSolver<Matrix, Vector>(A);
    }
#endif // defined(COMPOSYX_USE_LAPACKPP)

#if defined(COMPOSYX_USE_CHAMELEON)
    if (kt == dense_kernel_type::CHAMELEON) {
      solver = composyx::ChameleonSolver<Matrix, Vector>(A);
    }
#endif // defined(COMPOSYX_USE_CHAMELEON)

#if defined(COMPOSYX_USE_TLAPACK)
    if (kt == dense_kernel_type::TLAPACK) {
      solver = composyx::TlapackSolver<Matrix, Vector>(A);
    }
#endif // defined(COMPOSYX_USE_TLAPACK)
  }

  void setup(const Matrix& A) {
    return std::visit([&](auto& s) { return s.setup(A); }, this->solver);
  }

  void setup(Matrix&& A) {
    return std::visit([&](auto& s) { return s.setup(std::move(A)); },
                      this->solver);
  }

  Vector apply(const Vector& rhs) {
    return std::visit([&](auto& s) { return s * rhs; }, this->solver);
  }

  friend Vector operator*(const COMPOSYX_SOLVER_BLAS& cpx_solver,
                          const Vector& rhs) {
    return std::visit([&](auto& s) { return s * rhs; }, cpx_solver.solver);
  }
}; // COMPOSYX_SOLVER_BLAS

template <typename Matrix, typename Vector>
struct matrix_type<COMPOSYX_SOLVER_BLAS<Matrix, Vector>> {
  using type = Matrix;
};
template <typename Matrix, typename Vector>
struct vector_type<COMPOSYX_SOLVER_BLAS<Matrix, Vector>> {
  using type = Vector;
};
template <typename Matrix, typename Vector>
struct scalar_type<COMPOSYX_SOLVER_BLAS<Matrix, Vector>> {
  using type = typename Vector::value_type;
};
// Solvers interface:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_dense_kernels.org::*Info on the current solver / kernels][Info on the current solver / kernels:1]]
#if defined(COMPOSYX_STATIC_DENSE_KERNEL_TYPE)

inline std::string _value_current_implicit_dense_kernels() {
  return std::string(COMPOSYX_STATIC_DENSE_KERNEL_TYPE);
}

inline void info_implicit_dense_kernels(std::ostream& sout = std::cout) {
  sout << "Blas kernels defined at compilation to: "
       << COMPOSYX_STATIC_DENSE_KERNEL_TYPE << '\n';
}

#else

inline std::string _value_current_implicit_dense_kernels() {

  dense_kernel_type kt = (select_dense_kernel::package)
                             ? select_dense_kernel::package.value()
                             : default_dense_kernel::package;

  if (kt == dense_kernel_type::LAPACKPP) {
    return std::string("LAPACKPP");
  } else if (kt == dense_kernel_type::CHAMELEON) {
    return std::string("CHAMELEON");
  } else if (kt == dense_kernel_type::TLAPACK) {
    return std::string("TLAPACK");
  }
  return std::string("Error");
}

inline void info_implicit_dense_kernels(std::ostream& sout = std::cout) {
  sout << "Blas kernels current value: "
       << _value_current_implicit_dense_kernels() << '\n';
}
#endif // defined(COMPOSYX_STATIC_DENSE_KERNEL_TYPE)
// Info on the current solver / kernels:1 ends here

// [[file:../../../org/composyx/interfaces/implicit_dense_kernels.org::*Footer][Footer:1]]
} // namespace composyx
// Footer:1 ends here
