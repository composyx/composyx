// [[file:../../../org/composyx/interfaces/linalg_concepts.org::*Header][Header:1]]
#pragma once

#include "basic_concepts.hpp"

namespace composyx {
// Header:1 ends here

// [[file:../../../org/composyx/interfaces/linalg_concepts.org::*Vector][Vector:1]]
template <typename vector>
concept CPX_Vector = CPX_LinearObject<vector> && requires(vector v1,
                                                          vector v2) {
  {
    dot(v1, v2)
  } -> std::convertible_to<typename scalar_type<vector>::type>; // Dot product
  { size(v1) } -> std::integral;
};
// Vector:1 ends here

// [[file:../../../org/composyx/interfaces/linalg_concepts.org::*Vector][Vector:2]]
template <typename vector>
concept CPX_Vector_Multiple =
    CPX_LinearObject<vector> && requires(vector v1, vector v2) {
      { n_rows(v1) } -> std::integral;
      { n_cols(v1) } -> std::integral;
      {
        cwise_dot(v1, v2)
      } -> std::convertible_to<std::vector<
          typename scalar_type<vector>::type>>; // Column-wise dot product
    };
// Vector:2 ends here

// [[file:../../../org/composyx/interfaces/linalg_concepts.org::*Vector][Vector:3]]
template <typename vector>
concept CPX_Vector_Single_Or_Multiple =
    (CPX_Vector<vector> || CPX_Vector_Multiple<vector>);
// Vector:3 ends here

// [[file:../../../org/composyx/interfaces/linalg_concepts.org::*Dense vector][Dense vector:1]]
template <typename vector>
concept CPX_DenseVector =
    CPX_Vector<vector> && requires(vector v1, vector v2, int i) {
      {
        vector(i)
      } -> std::convertible_to<vector>; // Constructor with size only
      {
        v1[i]
      } -> std::convertible_to<
          typename scalar_type<vector>::type>; // Element accessor
      {
        v1(i)
      } -> std::convertible_to<
          typename scalar_type<vector>::type>; // Element accessor
      {
        get_ptr(v1)
      } -> std::convertible_to<typename scalar_type<vector>::type*>;
    };
// Dense vector:1 ends here

// [[file:../../../org/composyx/interfaces/linalg_concepts.org::*Operator][Operator:1]]
template <typename operat, typename vector = typename vector_type<operat>::type>
concept CPX_LinearOperator =
    (!std::is_same_v<
        vector,
        std::
            false_type>) && // "typename vector_type<operat>::type" is undefined
    CPX_Vector<vector> &&
    requires(operat m, vector v) {
      { m* v } -> std::convertible_to<vector>;
    };
// Operator:1 ends here

// [[file:../../../org/composyx/interfaces/linalg_concepts.org::*Matrix][Matrix:1]]
template <typename matrix>
concept CPX_Matrix = CPX_LinearOperator<matrix> && requires(matrix m) {
  { n_rows(m) } -> std::integral;
  { n_cols(m) } -> std::integral;
};
// Matrix:1 ends here

// [[file:../../../org/composyx/interfaces/linalg_concepts.org::*Dense matrix][Dense matrix:1]]
template <typename matrix>
concept CPX_DenseMatrix =
    CPX_Matrix<matrix> &&
    requires(matrix mat, typename scalar_type<matrix>::type scal, int i,
             int j) {
      {
        get_ptr(mat)
      } -> std::convertible_to<typename scalar_type<matrix>::type*>;
      {
        mat(i, j)
      } -> std::convertible_to<
          typename scalar_type<matrix>::type>; // Element accessor
    };

template <typename matrix>
concept CPX_PartDenseMatrix =
    CPX_Matrix<matrix> && CPX_DenseMatrix<typename matrix::local_type>;
// Dense matrix:1 ends here

// [[file:../../../org/composyx/interfaces/linalg_concepts.org::*Dense matrix with BLAS/lapack format][Dense matrix with BLAS/lapack format:1]]
template <typename matrix>
concept CPX_DenseBlasMatrix = CPX_DenseMatrix<matrix> && requires(matrix mat) {
  { get_leading_dim(mat) } -> std::integral;
};
// Dense matrix with BLAS/lapack format:1 ends here

// [[file:../../../org/composyx/interfaces/linalg_concepts.org::*Sparse matrix][Sparse matrix:1]]
template <typename matrix>
concept CPX_SparseMatrix = CPX_Matrix<matrix> && requires(matrix mat) {
  { n_nonzero(mat) } -> std::integral;
};

template <typename matrix>
concept CPX_PartSparseMatrix =
    CPX_Matrix<matrix> && CPX_SparseMatrix<typename matrix::local_type>;
// Sparse matrix:1 ends here

// [[file:../../../org/composyx/interfaces/linalg_concepts.org::*Sparse matrix with I, J, V access][Sparse matrix with I, J, V access:1]]
template <typename T>
concept CPX_PointerToIntegral =
    std::is_pointer_v<T> && std::is_integral_v<std::remove_pointer_t<T>>;

template <typename T>
concept CPX_PointerToScalar =
    std::is_pointer_v<T> && CPX_Scalar<std::remove_pointer_t<T>>;

template <typename matrix>
concept CPX_SparseMatrixIJV = CPX_SparseMatrix<matrix> && requires(matrix mat) {
  { get_i_ptr(mat) } -> CPX_PointerToIntegral;
  { get_j_ptr(mat) } -> CPX_PointerToIntegral;
  { get_v_ptr(mat) } -> CPX_PointerToScalar;
};
// Sparse matrix with I, J, V access:1 ends here

// [[file:../../../org/composyx/interfaces/linalg_concepts.org::*Solver][Solver:1]]
/*
  template<typename solver, typename matrix, typename vector = typename vector_type<matrix>::type>
  concept CPX_Solver = requires(solver s, vector v, matrix mat)
  {
  { s.setup(mat) } -> std::convertible_to<void>;
  { s.solve(v) } ->std::convertible_to<vector>;
  { s * v } -> std::convertible_to<vector>;
  };
*/
// Solver:1 ends here

// [[file:../../../org/composyx/interfaces/linalg_concepts.org::*No concept version][No concept version:1]]
} // namespace composyx
// No concept version:1 ends here
