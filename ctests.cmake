#--------------------------------------------------------------------
# BUILDNAME variable construction
# This variable will be used to set the build name which will appear
# on the MaPHyS dashboard http://cdash-ci.inria.fr/
#--------------------------------------------------------------------
# Start with the short system name, e.g. "Linux", "FreeBSD" or "Windows"
if(NOT BUILDNAME)
  set(BUILDNAME "${CMAKE_SYSTEM_NAME}")

  # Add compiler name
  get_filename_component(CMAKE_CXX_COMPILER_NAME ${CMAKE_CXX_COMPILER} NAME)
  set(BUILDNAME "${BUILDNAME}-${CMAKE_CXX_COMPILER_NAME}")

  # Add the build type, e.g. "Debug, Release..."
  if(CMAKE_BUILD_TYPE)
    set(BUILDNAME "${BUILDNAME}-${CMAKE_BUILD_TYPE}")
  endif(CMAKE_BUILD_TYPE)
endif()

enable_testing()
include(CTest)
