#!/usr/bin/env sh

set -ex

if command -v clang-format
then
    echo "Found clang-format"
    emacs --batch --load publish.el --eval '(org-publish "generate-source-code")'
else
    echo "Missing command: clang-format"
    echo "Necessary to format tangled source files. Please add clang to your environment."
    exit 1
fi

