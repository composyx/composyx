# Script to format all sources in composyx

# Need clang
# Ex:
# guix shell clang-toolchain -- ./clangformatsrc.sh

SRC="$(find include | grep hpp$) $(find src | grep cpp$) $(find bench | grep cpp$)"

for f in $SRC; do
    echo "$f"
    clang-format -i $f
done

echo "--- clang-format DONE ---"
