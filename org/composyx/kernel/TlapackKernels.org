label:solver:tlapackkernels

* Class
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../include/composyx/kernel/TlapackKernels.hpp :comments link
:END:

** Header

#+begin_src c++
  #pragma once
  #include <cctype>

  // Use span as a sliceable vector of int for tlapack
  #include <span>

  #include <tlapack/base/arrayTraits.hpp>
  #include <tlapack/LegacyMatrix.hpp>

  namespace tlapack {
    template<typename T>
    const std::span<T> slice(const std::span<T>& v, std::pair<int, int> idxs){
      auto& [i, f] = idxs;
      return v.subspan(i, f - i);
    }

    template<typename T>
    std::span<T> slice(std::span<T>& v, std::pair<int, int> idxs){
      auto& [i, f] = idxs;
      return v.subspan(i, f - i);
    }
  } // namespace tlapack

  #include <composyx/interfaces/linalg_concepts.hpp>

  template <composyx::CPX_DenseBlasMatrix Mat>
  constexpr auto legacy_matrix(const Mat& A) noexcept
  {
    using Scalar = typename Mat::value_type;
    return tlapack::LegacyMatrix<Scalar>{n_rows(A), n_cols(A), (const Scalar*) get_ptr(A), get_leading_dim(A)};
  }

  template <composyx::CPX_DenseBlasMatrix Mat>
  constexpr auto legacy_matrix(Mat& A) noexcept
  {
    using Scalar = typename Mat::value_type;
    return tlapack::LegacyMatrix<Scalar>{n_rows(A), n_cols(A), (Scalar*) get_ptr(A), get_leading_dim(A)};
  }

  #include <tlapack.hpp>
  #include <tlapack/lapack/gesvd.hpp>

  namespace composyx {
#+end_src

** Concepts

Definition of the concepts of the object interfaces (matrix,
vector...) necessary to call the tlapack functions.

#+begin_src c++
  template<typename matrix>
  concept TlapackKernelMatrix = tlapack::concepts::Matrix<matrix> &&
    requires(matrix m)
  {
    { is_storage_full(m) }  -> std::same_as<bool>;
    { is_storage_lower(m) } -> std::same_as<bool>;
    { is_storage_upper(m) } -> std::same_as<bool>;
    { is_symmetric(m) }     -> std::same_as<bool>;
    { is_hermitian(m) }     -> std::same_as<bool>;
  };
#+end_src

#+begin_src c++
  struct tlapack_kernels {
#+end_src

** Blas 1

#+begin_src c++
  // X <- alpha * X (vector or matrix)
  template<tlapack::concepts::Vector Vector, CPX_Scalar Scalar>
  static void scal(Vector& X, Scalar alpha){
    tlapack::scal(alpha, X);
  }

  // Y <- alpha * X + Y
  template<tlapack::concepts::Vector Vector, CPX_Scalar Scalar>
  static void axpy(const Vector& X, Vector& Y, Scalar alpha){
    tlapack::axpy(alpha, X, Y);
  }

  // Return X dot Y
  template<tlapack::concepts::Vector Vector>
  [[nodiscard]] static CPX_Scalar auto dot(const Vector& X, const Vector& Y){
    return tlapack::dot(X, Y);
  }

  // Return X dot Y
  template<tlapack::concepts::Vector Vector>
  [[nodiscard]] static CPX_Scalar auto dotu(const Vector& X, const Vector& Y){
    return tlapack::dotu(X, Y);
  }

  template<tlapack::concepts::Vector Vector>
  [[nodiscard]] static CPX_Real auto norm2_squared(const Vector& X){
    return std::real(tlapack::dot(X, X));
  }

  template<tlapack::concepts::Vector Vector>
  [[nodiscard]] static CPX_Real auto norm2(const Vector& X){
    return tlapack::nrm2(X);
  }
#+end_src

** Blas 2

#+begin_src c++
  // Y = alpha * op(A) * X + beta * Y
  template<TlapackKernelMatrix Matrix, tlapack::concepts::Vector Vector, CPX_Scalar Scalar = typename Vector::scalar_type>
  static void gemv(const Matrix& A, const Vector& X, Vector& Y, char opA = 'N', Scalar alpha = 1, Scalar beta = 0){
    const size_t M = tlapack::nrows(A);
    const size_t N = tlapack::ncols(A);
    opA = std::toupper(opA);

    const size_t opa_rows = (opA == 'N') ? M : N;
    const size_t opa_cols = (opA == 'N') ? N : M;

    COMPOSYX_DIM_ASSERT(tlapack::nrows(X), opa_cols, "TlapackSolver::gemv dimensions of A and X do not match");
    if(tlapack::nrows(Y) != opa_rows){
      COMPOSYX_ASSERT(beta == Scalar{0}, "TlapackSolver::gemv wrong dimensions for Y (and beta != 0)");
      Y = Vector(opa_rows); // Not captured by the concept of tlapack Vector
    }

    if(is_storage_full(A)){
      tlapack::Op op = tlapack::Op::NoTrans;
      if(opA == 'T') op = tlapack::Op::Trans;
      if(opA == 'C') op = tlapack::Op::ConjTrans;
      tlapack::gemv(op, alpha, A, X, beta, Y);
    } else {
      tlapack::Uplo uplo = (is_storage_lower(A)) ? tlapack::Uplo::Lower : tlapack::Uplo::Upper;
      if(is_symmetric(A)){
	tlapack::symv(uplo, alpha, A, X, beta, Y);
      }
      else if(is_hermitian(A)){
	if constexpr(is_complex<Scalar>::value){
	  tlapack::hemv(uplo, alpha, A, X, beta, Y);
	}
      }
      else {
	COMPOSYX_ASSERT(false, "TlapackSolver::gemv matrix A is half stored but not sym/herm");
      }
    }
  }

  // A += alpha * x * y.t()
  template<TlapackKernelMatrix Matrix, tlapack::concepts::Vector Vect1, tlapack::concepts::Vector Vect2, CPX_Scalar Scalar = typename Matrix::scalar_type>
  static void geru(Matrix& A, const Vect1& X, const Vect2& Y, Scalar alpha = 1){
    tlapack::geru(alpha, X, Y, A);
  }

  // A += alpha * x * y.h()
  template<TlapackKernelMatrix Matrix, tlapack::concepts::Vector Vect1, tlapack::concepts::Vector Vect2, CPX_Scalar Scalar = typename Matrix::scalar_type>
  static void ger(Matrix& A, const Vect1& X, const Vect2& Y, Scalar alpha = 1){
    tlapack::ger(alpha, X, Y, A);
  }
#+end_src

** Blas 3

#+begin_src c++
  // C <- alpha * opA(A) * opB(B) + beta * C
  template<TlapackKernelMatrix Matrix, CPX_Scalar Scalar = typename Matrix::scalar_type>
  static void gemm(const Matrix& A, const Matrix& B, Matrix& C, char opA = 'N', char opB = 'N', Scalar alpha = 1, Scalar beta = 0){
    opA = std::toupper(opA);
    opB = std::toupper(opB);
    const size_t opa_rows = (opA == 'N') ? tlapack::nrows(A) : tlapack::ncols(A);
    const size_t opa_cols = (opA == 'N') ? tlapack::ncols(A) : tlapack::nrows(A);
    const size_t opb_rows = (opB == 'N') ? tlapack::nrows(B) : tlapack::ncols(B);
    const size_t opb_cols = (opB == 'N') ? tlapack::ncols(B) : tlapack::nrows(B);

    COMPOSYX_DIM_ASSERT(opa_cols, opb_rows, "TlapackSolver::gemm dimensions of A and B do not match");
    if(tlapack::nrows(C) != opa_rows or tlapack::ncols(C) != opb_cols){
      COMPOSYX_ASSERT(beta == Scalar{0}, "TlapackSolver::gemm wrong dimensions for C (and beta != 0)");
      C = Matrix(opa_rows, opb_cols);
    }

    if(is_storage_full(A) and is_storage_full(B)){
      auto op_blas = [](char op){
	if(op == 'T') return tlapack::Op::Trans;
	if(op == 'C') return tlapack::Op::ConjTrans;
	return tlapack::Op::NoTrans;
      };

      tlapack::gemm(op_blas(opA), op_blas(opB), alpha, A, B, beta, C);
    }
    else {

      const Matrix& sym_mat  = (is_storage_full(A)) ? B : A;
      const Matrix& full_mat = (is_storage_full(A)) ? A : B;
      tlapack::Side side     = (is_storage_full(A)) ? tlapack::Side::Right : tlapack::Side::Left;
      tlapack::Uplo uplo     = (is_storage_lower(sym_mat)) ? tlapack::Uplo::Lower : tlapack::Uplo::Upper;

      if(is_symmetric(sym_mat)){
	tlapack::symm(side, uplo, alpha, sym_mat, full_mat, beta, C);
      }
      else if(is_hermitian(sym_mat)){
	if constexpr(is_complex<Scalar>::value){
	  tlapack::hemm(side, uplo, alpha, sym_mat, full_mat, beta, C);
	}
      }
      else {
	COMPOSYX_ASSERT(false, "TlapackSolver::gemm matrix is half stored but not sym/herm");
      }
    }
  }
#+end_src

** Eigenvalue decomposition


#+begin_src c++
  template<TlapackKernelMatrix Matrix,
	   TlapackKernelMatrix ComplexMatrix = typename complex_type<Matrix>::type,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Complex Complex = typename ComplexMatrix::scalar_type>
  [[nodiscard]] static std::pair<std::vector<Complex>, ComplexMatrix>
  eig(const Matrix&) {
    COMPOSYX_ASSERT(false, "Tlapack::eig not implemented");
    return std::pair<std::vector<Complex>, ComplexMatrix>();
  }

  template<TlapackKernelMatrix Matrix,
	   TlapackKernelMatrix ComplexMatrix = typename complex_type<Matrix>::type,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Complex Complex = typename ComplexMatrix::scalar_type>
  [[nodiscard]] static std::vector<Complex>
  eigvals(const Matrix&) {
    COMPOSYX_ASSERT(false, "Tlapack::eigvals not implemented");
    return std::vector<Complex>();
  }

  template<TlapackKernelMatrix Matrix,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  eigh(const Matrix&) {
    COMPOSYX_ASSERT(false, "Tlapack::eigh not implemented");
    return std::pair<std::vector<Real>, Matrix>();
  }

  template<TlapackKernelMatrix Matrix,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::vector<Real>
  eigvalsh(const Matrix&) {
    COMPOSYX_ASSERT(false, "Tlapack::eigvalsh not implemented");
    return std::vector<Real>();
  }

  template<TlapackKernelMatrix Matrix,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, DenseMatrix<Scalar>>
  eigh_select(const Matrix&, int, int) {
    COMPOSYX_ASSERT(false, "Tlapack::eigh_select not implemented");
    return std::pair<std::vector<Real>, DenseMatrix<Scalar>>();
  }

  template<TlapackKernelMatrix Matrix,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, DenseMatrix<Scalar>>
  eigh_smallest(const Matrix&, int) {
    COMPOSYX_ASSERT(false, "Tlapack::eigh_smallest not implemented");
    return std::pair<std::vector<Real>, DenseMatrix<Scalar>>();
  }

  template<TlapackKernelMatrix Matrix,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, DenseMatrix<Scalar>>
  eigh_largest(const Matrix&, int) {
    COMPOSYX_ASSERT(false, "Tlapack::eigh_largest not implemented");
    return std::pair<std::vector<Real>, DenseMatrix<Scalar>>();
  }

  template<TlapackKernelMatrix Matrix,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  gen_eigh_select(const Matrix&, const Matrix&, int, int) {
    COMPOSYX_ASSERT(false, "Tlapack::gen_eigh_select not implemented");
    return std::pair<std::vector<Real>, Matrix>();
  }

  template<TlapackKernelMatrix Matrix,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  gen_eigh_smallest(const Matrix&, const Matrix&, int) {
    COMPOSYX_ASSERT(false, "Tlapack::gen_eigh_smallest not implemented");
    return std::pair<std::vector<Real>, Matrix>();
  }

  template<TlapackKernelMatrix Matrix,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  gen_eigh_largest(const Matrix&, const Matrix&, int) {
    COMPOSYX_ASSERT(false, "Tlapack::gen_eigh_largest not implemented");
    return std::pair<std::vector<Real>, Matrix>();
  }

  template<TlapackKernelMatrix Matrix,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static  std::tuple<Matrix, std::vector<Real>, Matrix>
  _svd_gesvd(Matrix A, bool compute_u = true, bool compute_vt = true, bool reduced = true){
    size_t M = n_rows(A);
    size_t N = n_cols(A);
    size_t K = std::min(M, N);

    // !!!! Seems like reduced is not implemented

    Matrix U;
    if(compute_u){
      U = Matrix(M, M);
      // if(reduced){
      //   U = Matrix(M, K);
      // } else {
      //   U = Matrix(M, M);
      // }
    }

    Matrix Vt;
    if(compute_vt){
      Vt = Matrix(N, N);
      //if(reduced){
      //  Vt = Matrix(K, N);
      //} else {
      //  Vt = Matrix(N, N);
      //}
    }

    std::vector<Real> svalues(K);
    std::span<Real> span_sv(svalues);

    auto tlpA = legacy_matrix(A);
    auto tlpU = legacy_matrix(U);
    auto tlpVt = legacy_matrix(Vt);

    tlapack::gesvd(compute_u, compute_vt, tlpA, span_sv, tlpU, tlpVt);

    // Truncate the result if reduced form is demanded
    if(reduced){
      Matrix U_hat;
      Matrix Vt_hat;
      if(compute_u){
	U_hat = Matrix(M, K);
	for(size_t j = 0; j < K; ++j){
	  for(size_t i = 0; i < M; ++i){
	    U_hat(i, j) = U(i, j);
	  }
	}
      }
      if(compute_vt){
	Vt_hat = Matrix(K, N);
	for(size_t j = 0; j < N; ++j){
	  for(size_t i = 0; i < K; ++i){
	    Vt_hat(i, j) = Vt(i, j);
	  }
	}
      }
      return std::make_tuple(std::move(U_hat), std::move(svalues), std::move(Vt_hat));
    }

    // Else return full SVD
    return std::make_tuple(std::move(U), std::move(svalues), std::move(Vt));
  }

  template<TlapackKernelMatrix Matrix,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::tuple<Matrix, std::vector<Real>, Matrix>
  svd(const Matrix& A, bool reduced = true) {
    return _svd_gesvd<Matrix, Scalar, Real>(A, true, true, reduced);
  }

  // Returns U and sigmas only
  template<TlapackKernelMatrix Matrix,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<Matrix, std::vector<Real>>
  svd_left_sv(const Matrix& A, bool reduced = true) {
    auto [U, sigmas, Vt] = _svd_gesvd<Matrix, Scalar, Real>(A, true, false, reduced);
    return std::move(std::make_pair(std::move(U), std::move(sigmas)));
  }

  // Returns sigmas and Vt only
  template<TlapackKernelMatrix Matrix,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::pair<std::vector<Real>, Matrix>
  svd_right_sv(const Matrix& A, bool reduced = true) {
    auto [U, sigmas, Vt] = _svd_gesvd<Matrix, Scalar, Real>(A, false, true, reduced);
    return std::move(std::make_pair(std::move(sigmas), std::move(Vt)));
  }

  // Returns only singular values
  template<TlapackKernelMatrix Matrix,
	   CPX_Scalar Scalar = typename Matrix::scalar_type,
	   CPX_Real Real = typename Matrix::real_type>
  [[nodiscard]] static std::vector<Real>
  svdvals(const Matrix& A) {
    auto [U, sigmas, Vt] = _svd_gesvd<Matrix, Scalar, Real>(A, false, false);
    return sigmas;
  }
#+end_src

** Footer

#+begin_src c++
  }; // struct tlapack_kernels
  } // namespace composyx
#+end_src

* Tests
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../src/test/unittest/test_tlapack_kernels.cpp :comments link
:END:

#+begin_src c++
  #include <composyx.hpp>
  #include <composyx/kernel/TlapackKernels.hpp>
  #include <composyx/loc_data/DenseMatrix.hpp>
  #include <composyx/testing/TestMatrix.hpp>
  #include <catch2/catch_test_macros.hpp>
  #include <catch2/catch_template_test_macros.hpp>
  #include <composyx/testing/Catch2DenseMatrixMatchers.hpp>

  using namespace composyx;

  TEMPLATE_TEST_CASE("TlapackKernels", "[dense][sequential][tlapack][kernel]", float, double, std::complex<float>, std::complex<double>){
    using Scalar = TestType;
    using Real = typename arithmetic_real<Scalar>::type;
    //using Complex = typename std::conditional<is_complex<Scalar>::value, Scalar, std::complex<Real>>::type;

    using EqualsVectorPW = composyx::EqualsVectorPW<Vector<Scalar>>;
    using EqualsMatrixPW = composyx::EqualsMatrixPW<DenseMatrix<Scalar>>;

    constexpr const double blas_tol = (is_precision_double<Scalar>::value) ? 1e-15 : 1e-6;
    constexpr const double ev_tol = (is_precision_double<Scalar>::value) ? 1e-12 : 1e-5;

    auto equals_fp = [](auto s1, auto s2, double tol){
      return std::abs(s1 - s2) < tol;
    };
#+end_src

** Blas 1

#+begin_src c++
  SECTION("Blas 1 - SCAL"){
    const Vector<Scalar> x({1, -1, 3});
    const DenseMatrix<Scalar> mat({1, 2, 3, 4}, 2, 2);

    // Scal (*2)
    const Vector<Scalar> x_scal({2, -2, 6});

    Vector<Scalar> x2(x);
    tlapack_kernels::scal(x2, Scalar{2});
    REQUIRE_THAT(x2, EqualsVectorPW(x_scal));

    // Test with increment
    DenseMatrix<Scalar> mat2(mat);
    Vector<Scalar> y = mat2.get_row_view(0);

    const Vector<Scalar> y_scal({2, 6});
    tlapack_kernels::scal(y, Scalar{2});

    REQUIRE_THAT(y, EqualsVectorPW(y_scal));
  }

  SECTION("Blas 1 - AXPY"){
    const Vector<Scalar> x({1, -1, 3});

    // AXPY (*2 + (-1,-1,-1)T)
    Vector<Scalar> y({-1, -1, -1});
    const Vector<Scalar> x2py({1, -3, 5});
    tlapack_kernels::axpy(x, y, Scalar{2});
    REQUIRE_THAT(y, EqualsVectorPW(x2py));
  }

  // dot
  SECTION("Blas 1 - DOT"){
    const Vector<Scalar> x({1, -1, 3});

    const Vector<Scalar> y({2, -4, -1});
    const Scalar xdoty{3};
    REQUIRE(equals_fp(tlapack_kernels::dot(x, y), xdoty, blas_tol));
  }

  // norm2
  SECTION("Blas 1 - NORM2"){
    const Vector<Scalar> x({1, -1, 3});

    const Real normx_sq = 11;
    const Real normx = std::sqrt(normx_sq);
    REQUIRE(equals_fp(tlapack_kernels::norm2_squared(x), normx_sq, blas_tol));
    REQUIRE(equals_fp(tlapack_kernels::norm2(x), normx, blas_tol));
  }
#+end_src

** Blas 2

#+begin_src c++
  SECTION("Blas 2 -GEMV"){
    //static void gemv(const Matrix& A, const Vector& X, Vector& Y, Scalar alpha = 1, Scalar beta = 0)

    // 1 4        3
    // 2 5 x -1 = 3
    // 3 6    1   3
    const DenseMatrix<Scalar> MA({1, 2, 3, 4, 5, 6}, 3, 2);
    const Vector<Scalar> x({-1, 1});
    const Vector<Scalar> sol_check({3, 3, 3});
    Vector<Scalar> y(composyx::n_rows(MA));

    tlapack_kernels::gemv(MA, x, y);

    REQUIRE_THAT(sol_check, EqualsVectorPW(y));
  }

  SECTION("Blas 2 - Gemv sym/herm"){
    Vector<Scalar> X({3, 2, 1});
    DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
    L.set_property(composyx::MatrixStorage::lower, composyx::MatrixSymmetry::symmetric);

    Vector<Scalar> LX({-2, 5, -2});
    if constexpr(composyx::is_complex<Scalar>::value){
      X += Scalar{0, 1} * Vector<Scalar>({1, 2, -1});
      L += Scalar{0, 1} * DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
      L.set_property(composyx::MatrixStorage::lower, composyx::MatrixSymmetry::hermitian);
      LX = Vector<Scalar>({{-5, 7}, {8, -2}, {1, -7}});
    }

    Vector<Scalar> my_LX(3);
    tlapack_kernels::gemv(L, X, my_LX);
    REQUIRE_THAT(my_LX, EqualsVectorPW(LX));
  }

#+end_src

** Blas 3

#+begin_src c++
  SECTION("Blas 3 - GEMM - general"){
    const DenseMatrix<Scalar> A({1.0, -1.0, 2.0, -2.0, 3.0, -3.0}, 2, 3);
    const DenseMatrix<Scalar> B({1.0,  2.0, 3.0,  4.0, 5.0,  6.0}, 3, 2);

    //            1 4
    //  1  2  3 x 2 5 =  14  32
    // -1 -2 -3   3 6   -14 -32

    const DenseMatrix<Scalar> C_check({14.0, -14.0, 32.0, -32.0}, 2, 2);
    DenseMatrix<Scalar> C(composyx::n_rows(A), composyx::n_cols(B));

    tlapack_kernels::gemm(A, B, C);
    REQUIRE_THAT(C, EqualsMatrixPW(C_check));
  }

  SECTION("Blas 3 - GEMM - symmetric / hermitian"){
    DenseMatrix<Scalar> M({1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 3);
    DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
    L.set_property(composyx::MatrixStorage::lower, composyx::MatrixSymmetry::symmetric);

    DenseMatrix<Scalar> ML({-14,-16,-18, 5, 7, 9, 36, 39, 42}, 3,3);
    DenseMatrix<Scalar> LM({-6, 3, 14, -12, 9, 23, -18, 15, 32}, 3, 3);
    if constexpr(composyx::is_complex<Scalar>::value){
      M += Scalar{0, 1} * DenseMatrix<Scalar>({1, -1, 1,
	  -1, 2, -2,
	  1, -1, -0.5}, 3, 3);
      L += Scalar{0, 1} * DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
      L.set_property(composyx::MatrixStorage::lower, composyx::MatrixSymmetry::hermitian);
      ML = DenseMatrix<Scalar>({{-16, -3}, {-13, 11}, {-19.5, -0.5}, {6, -9}, {6, -19}, {7, -11.5}, {39, 8}, {34, 8}, {47, 1.5}}, 3, 3);
      LM = DenseMatrix<Scalar>({{-4, -7}, {2, 9}, {11, -2}, {-16, 10}, {12, 2}, {28, -11}, {-17.5, -2.5}, {17, 13.5}, {29, -15.5}}, 3, 3);
    }

    DenseMatrix<Scalar> my_ML(3, 3);
    tlapack_kernels::gemm(M, L, my_ML);
    REQUIRE_THAT(my_ML, EqualsMatrixPW(ML));

    DenseMatrix<Scalar> my_LM(3, 3);
    tlapack_kernels::gemm(L, M, my_LM);
    REQUIRE_THAT(my_LM, EqualsMatrixPW(LM));
  }
#+end_src

** SVD

#+begin_src c++
  SECTION("Singular value decomposition"){
    const size_t M = 3;
    const size_t N = 2;
    const size_t K = 2;

    DenseMatrix<Scalar> A({1, 2, 3, -4, -5, 6}, M, N);
    if constexpr(is_complex<Scalar>::value){
      // Add some imaginary part for fun
      A += Scalar{0, 1} * DenseMatrix<Scalar>({1, -1, 2, 1, 0, 0.5}, M, N);
    }

    auto check_svd = [&A](const DenseMatrix<Scalar>& l_U,
			  const std::vector<Real>& l_sigmas,
			  const DenseMatrix<Scalar>& l_Vt){
      // Construct the diagonal matrix Sigma for checking
      DenseMatrix<Scalar> Sigma(n_cols(l_U), n_rows(l_Vt));
      auto l_k = std::min(n_cols(l_U), n_rows(l_Vt));
      for(size_t i = 0; i < l_k; ++i) Sigma(i, i) = Scalar{l_sigmas[i]};
      DenseMatrix<Scalar> Acheck = l_U * Sigma * l_Vt;
      return ((A - Acheck).norm() < ev_tol);
    };

    auto check_singvals = [](const std::vector<Real>& s1,
			     const std::vector<Real>& s2){
      if(s1.size() != s2.size()) return false;
      for(size_t i = 0; i < s1.size(); ++i){
	if(std::abs(s1[i] - s2[i]) > ev_tol) return false;
      }
      return true;
    };

    // Reduced svd
    const bool reduced = true;
    const bool full = false;

    auto [U, sigmas, Vt] = tlapack_kernels::svd(A, reduced);
    REQUIRE(n_rows(U) == M);
    REQUIRE(n_cols(U) == K);
    REQUIRE(n_rows(Vt) == K);
    REQUIRE(n_cols(Vt) == N);
    REQUIRE(check_svd(U, sigmas, Vt));

    // Full svd
    auto [U2, sigmas2, Vt2] = tlapack_kernels::svd(A, full);
    REQUIRE(n_rows(U2) == M);
    REQUIRE(n_cols(U2) == M);
    REQUIRE(n_rows(Vt2) == N);
    REQUIRE(n_cols(Vt2) == N);
    REQUIRE(check_singvals(sigmas2, sigmas));
    REQUIRE(check_svd(U2, sigmas2, Vt2));

    // Full, only left singular vectors U
    auto [U3, sigmas3] = tlapack_kernels::svd_left_sv(A, full);
    REQUIRE(n_rows(U3) == M);
    REQUIRE(n_cols(U3) == M);
    REQUIRE(check_singvals(sigmas3, sigmas));

    // Full, only right singular vectors Vt
    auto [sigmas4, Vt4] = tlapack_kernels::svd_right_sv(A, full);
    REQUIRE(n_rows(Vt4) == N);
    REQUIRE(n_cols(Vt4) == N);
    REQUIRE(check_singvals(sigmas4, sigmas));

    // Only singular values
    auto sigmas5 = tlapack_kernels::svdvals(A);
    REQUIRE(check_singvals(sigmas5, sigmas));
  }
#+end_src

** Footer

#+begin_src c++
  }
#+end_src
