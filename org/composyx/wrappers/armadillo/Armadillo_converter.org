label:wrappers:armadillo_converter

#+PROPERTY: header-args: c++ :results silent

* Armadillo converter
  :PROPERTIES:
  :header-args: c++ :tangle ../../../../include/composyx/wrappers/armadillo/Armadillo_converter.hpp :comments link
  :END:

  Functions to convert =Armadillo= and =Composyx= matrices into one
  another.

#+begin_src c++
  #pragma once

  #include <composyx/loc_data/DenseMatrix.hpp>
  #include <composyx/wrappers/armadillo/Armadillo.hpp>

  namespace composyx{
    template<typename Scalar>
    inline DenseMatrix<Scalar> to_composyx_dense(const A_DenseMatrix<Scalar>& a_mat){
      DenseMatrix<Scalar> c_mat(n_rows(a_mat), n_cols(a_mat));
      for(size_t i = 0; i < n_rows(c_mat); ++i){
        for(size_t j = 0; j < n_cols(c_mat); ++j){
  	c_mat(i, j) = a_mat(i, j);
        }
      }

      return c_mat;
    }

    template<typename Scalar>
    inline Vector<Scalar> to_composyx_dense(const A_Vector<Scalar>& a_vect){
      Vector<Scalar> c_vect(a_vect.size());
      for(size_t i = 0; i < n_rows(c_vect); ++i){
        c_vect[i] = a_vect[i];
      }
      return c_vect;
    }

    template<typename Scalar>
    inline A_DenseMatrix<Scalar> to_armadillo_dense(const DenseMatrix<Scalar>& c_mat){
      A_DenseMatrix<Scalar> a_mat(n_rows(c_mat), n_cols(c_mat));
      for(size_t i = 0; i < n_rows(c_mat); ++i){
        for(size_t j = 0; j < n_cols(c_mat); ++j){
  	a_mat(i, j) = c_mat(i, j);
        }
      }

      return a_mat;
    }

    template<typename Scalar>
    inline A_Vector<Scalar> to_armadillo_dense(const Vector<Scalar>& c_vect){
      A_Vector<Scalar> a_vect(n_rows(c_vect));
      for(size_t i = 0; i < n_rows(c_vect); ++i){
        a_vect[i] = c_vect[i];
      }
      return a_vect;
    }

    template<typename Scalar>
    inline A_SparseMatrix<Scalar> to_armadillo_sparse(const DenseMatrix<Scalar>& c_mat){
      std::vector<int> ei;
      std::vector<int> ej;
      std::vector<Scalar> ev;
      for(size_t i = 0; i < n_rows(c_mat); ++i){
        for(size_t j = 0; j < n_cols(c_mat); ++j){
  	if(c_mat(i, j) != Scalar{0}){
  	  ei.push_back(i);
  	  ej.push_back(j);
  	  ev.push_back(c_mat(i, j));
  	}
        }
      }

      A_SparseMatrix<Scalar> a_mat(n_rows(c_mat), n_cols(c_mat));
      build_matrix(a_mat, n_rows(c_mat), n_cols(c_mat), static_cast<int>(ei.size()), ei.data(), ej.data(), ev.data());

      return a_mat;
    }
  }
#+end_src
