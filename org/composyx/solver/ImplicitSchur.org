label:solver:implschur

#+PROPERTY: header-args: c++ :results silent

* Method

  The numerical method is described in the
  {{{link(solver:schursolver,SchurSolver.html,Schur solver)}}} file.

  In this file we implement linear operator which allows to perform
  the product of the Schur complement with a vector without computing
  explicitly the Schur complement.

  NB: at the moment it is not possible to call an iterative solver
  using a preconditioner with ImplicitSchur, for it will try to
  evaluate the ImplicitSchur as a matrix, which we want to avoid in
  general.

* ImplicitSchur class
:PROPERTIES:
:header-args: c++ :tangle ../../../include/composyx/solver/ImplicitSchur.hpp :comments link
:END:

** Header

#+begin_src c++
  #pragma once

  #include <memory>
  #include <cassert>
  #include <limits>
  #include <iostream>
  #include <iomanip>
  #include <functional>
  #include <string>
  #include <type_traits>
  #include <cmath>

  #include "composyx/utils/Arithmetic.hpp"
  #include "composyx/utils/MatrixProperties.hpp"
  #include "composyx/utils/Error.hpp"
  #include "composyx/utils/Macros.hpp"
  #include "composyx/loc_data/SparseMatrixLIL.hpp"
  #include "composyx/solver/LinearOperator.hpp"
  #include "composyx/solver/IterativeSolver.hpp"

  namespace composyx {
#+end_src

** Attributes

#+begin_src c++
  template <CPX_Matrix Matrix, CPX_Vector Vector, typename SolverK>
  class ImplicitSchur : public LinearOperator<Matrix, Vector> {
  private:
    using Scalar = typename LinearOperator<Matrix, Vector>::scalar_type;
    using Real = typename arithmetic_real<Scalar>::type;

  public:
    using matrix_type = Matrix;
    using scalar_type = Scalar;
    using vector_type = Vector;
    using real_type = Real;

  private:
    std::vector<int> _schurlist;

    const Matrix* _K = nullptr;
    SolverK _Kii_inv;
    Matrix _Kig;
    Matrix _Kgi;
    Matrix _Kgg;
    Vector _Fi;

    std::vector<int> _perm;
    std::vector<int> _invperm;
    size_t _n_i;
    size_t _n_g;

    bool _splitted = false;
#+end_src

** Constructors

#+begin_src c++
  public:
    ImplicitSchur() {}
    ImplicitSchur(const Matrix& K, const std::vector<int>& schurlist)
	   : _K{&K}, _schurlist{schurlist} {}

    ImplicitSchur(const ImplicitSchur&) = default;
    ImplicitSchur& operator=(const ImplicitSchur&) = default;
    ImplicitSchur(ImplicitSchur&&) = default;
    ImplicitSchur& operator=(ImplicitSchur&&) = default;
#+end_src

** Split matrix

   After permuting the variables so that the interface unknowns are at
   the end, we split the input matrix $\K$ into $(\KII, \KIG, \KGI,
   \KGG)$.

   The properties of $\K$ are transmitted to $\KII$ and $\KII$
   (symmetry, positivity, definiteness and storage).

   In case of half storage, one of $\KIG, \KGI$ is fully stored and
   the other empty. Therefore we fill the empty one as the conjugate
   transpose of the full one.

#+begin_src c++
  public:
  void split_matrix() {
    COMPOSYX_ASSERT(_K != nullptr, "ImplicitSchur: K is null pointer");
    COMPOSYX_ASSERT(_schurlist.size() > 0,
  		  "ImplicitSchur: size of the schurlist must be not null");

    const size_t n_tot = n_rows(*_K);

    compute_permutations();
    Matrix K_ord = *_K;

    reindex(K_ord, _perm);
    COMPOSYX_ASSERT(
  		  _n_g < n_tot,
  		  "ImplicitSchur: size of the schurlist is larget than matrix size");
    const int ni = static_cast<int>(_n_i);
    const int ng = static_cast<int>(_n_g);

    SparseMatrixLIL<Scalar> Kii(static_cast<size_t>(ni),
  			      static_cast<size_t>(ni));
    SparseMatrixLIL<Scalar> Kig(static_cast<size_t>(ni),
  			      static_cast<size_t>(ng));
    SparseMatrixLIL<Scalar> Kgi(static_cast<size_t>(ng),
  			      static_cast<size_t>(ni));
    SparseMatrixLIL<Scalar> Kgg(static_cast<size_t>(ng),
  			      static_cast<size_t>(ng));

    Kii.copy_properties(*_K);
    Kgg.copy_properties(*_K);

    K_ord.foreach_ijv([&](int i, int j, Scalar v) {
      if (i >= ni) {
        if (j >= ni) {
  	Kgg.insert(i - ni, j - ni, v);
        } else {
  	Kgi.insert(i - ni, j, v);
        }
      } else {
        if (j >= ni) {
  	Kig.insert(i, j - ni, v);
        } else {
  	Kii.insert(i, j, v);
        }
      }
    });

    auto convert_to_matrix = [](SparseMatrixLIL<Scalar>&& matrix_in,
  			      Matrix& matrix_out) {
      SparseMatrixCOO<Scalar> coomat(matrix_in.to_coo());
      matrix_in = SparseMatrixLIL<Scalar>(0, 0);
      build_matrix(matrix_out, coomat.get_n_rows(), coomat.get_n_cols(),
  		 coomat.get_nnz(), coomat.get_i_ptr(), coomat.get_j_ptr(),
  		 coomat.get_v_ptr());
      matrix_out.copy_properties(coomat);
    };


    {
      Matrix Kii_conv;
      convert_to_matrix(std::move(Kii), Kii_conv);
      if constexpr (std::derived_from<SolverK, IterativeSolver<Matrix, Vector>>) {
        _Kii_inv.setup(parameters::copy_A<bool>{true});
       }
      _Kii_inv.setup(Kii_conv);
    }

    convert_to_matrix(std::move(Kig), _Kig);
    convert_to_matrix(std::move(Kgi), _Kgi);
    convert_to_matrix(std::move(Kgg), _Kgg);

    // Dealing with half storage issues
    if (_K->is_storage_lower()) {
      _Kig = adjoint(_Kgi);
    } else if (_K->is_storage_upper()) {
      _Kgi = adjoint(_Kig);
    }

    _splitted = true;
  }
#+end_src

** Permutations

#+begin_src c++
  private:
    // Compute perm and invperm to set schur unknowns at the end
    void compute_permutations() {
	 const auto M = n_rows(*_K);
	 _perm = std::vector<int>(M);
	 _invperm = std::vector<int>(M);

	 std::ranges::sort(_schurlist);

	 _n_g = _schurlist.size();
	 _n_i = M - _n_g;

	 size_t idx_g = 0;
	 size_t idx_i = 0;

	 // ex: M = 4; schurlist = [0, 2]
	 // perm = [2, 0, 3, 1] (B -> (Fi, Fg)^T)
	 // invperm = [1, 3, 0, 2] ((Ui, Ug)^T -> U)
	 for (int k = 0; k < static_cast<int>(M); ++k) {
	   if ((idx_g >= _n_g) || (_schurlist[idx_g] != k)) {
	     _perm[k] = idx_i;
	     _invperm[idx_i] = k;
	     idx_i++;
	   } else {
	     _perm[k] = _n_i + idx_g;
	     _invperm[_n_i + idx_g] = k;
	     idx_g++;
	   }
	 }
    }
#+end_src

** Setup

#+begin_src c++
  public:
    void setup(const Matrix& K) { _K = &K; }

    void setup_schurlist(const std::vector<int>& schurlist) {
	 _schurlist = schurlist;
    }
#+end_src

** Apply

#+begin_src c++
  Vector apply(const Vector& x) {
    if (!_splitted) {
	 split_matrix();
    }

    Vector y = _Kig * x;
    y = _Kii_inv * y;
    y = _Kgi * y;
    return _Kgg * x - y;
  }
#+end_src

** Get Schur system RHS

#+begin_src c++
  Vector compute_rhs(const Vector& B) {
    if (!_splitted) {
	 split_matrix();
    }
    _Fi = Vector(_n_i);
    Vector Fg(_n_g);

    for (size_t k = 0; k < (_n_i + _n_g); ++k) {
	 if (_perm[k] >= static_cast<int>(_n_i)) {
	   Fg[_perm[k] - _n_i] = B[k];
	 } else {
	   _Fi[_perm[k]] = B[k];
	 }
    }

    return Fg - _Kgi * (_Kii_inv * _Fi);
  }
#+end_src

** Get solution

   #+begin_src c++
     void get_solution(const Vector& Ug, Vector& U) const {
       Vector Ui = _Kii_inv * (_Fi - _Kig * Ug);

       for (size_t k = 0; k < _n_i; ++k) {
	 U[_invperm[k]] = Ui[k];
       }
       for (size_t k = 0; k < _n_g; ++k) {
	 U[_invperm[_n_i + k]] = Ug[k];
       }
     }

     Vector get_solution(const Vector& Ug) const {
       Vector U(_n_i + _n_g);
       get_solution(Ug, U);
       return U;
     }
   #+end_src

** Compute explicit Schur complement

   #+begin_src c++
     Matrix get_schur() { return _Kgg - _Kgi * (_Kii_inv * _Kig); }

     using DMat = typename dense_type<Matrix>::type;

     DMat get_dense_schur() {
       if constexpr (is_solver_direct<SolverK>::value) {
	 SolverK schur_solver(*_K);
	 return schur_solver.get_schur(_schurlist);
       }
       if (!_splitted) {
	 split_matrix();
       }
       DMat dense_Kig;
       _Kig.convert(dense_Kig);
       DMat S;
       _Kgg.convert(S);
       for (size_t k = 0; k < _n_g; ++k) {
	 auto S_col_k = S.get_vect_view(k);
	 auto Kig_col_k = dense_Kig.get_vect_view(k);
	 S_col_k -= _Kgi * (_Kii_inv * Kig_col_k);
       }
       return S;
     }
   #+end_src

** Getters

#+begin_src c++
  [[nodiscard]] SolverK& get_solver_K() { return _Kii_inv; }

  void display(const std::string& name = "",
  	     std::ostream& out = std::cout) const {
    if (!name.empty())
      out << "ImplicitSchur solver name: " << name << '\n';

    if (_K) {
      out << "Input matrix K (" << n_rows(*_K) << ", " << n_cols(*_K) << ")"
  	<< '\n';
    } else {
      out << "Input matrix K: unset" << '\n';
    }

    if (!_schurlist.empty()) {
      composyx::display<std::vector<int>>(_schurlist, "Schur list", out);
    } else {
      out << "Schur list: unset" << '\n';
    }

    out << '\n';
  }

  }; // class ImplicitSchur
#+end_src

** Traits

#+begin_src c++
  template <CPX_Matrix Matrix, CPX_Vector Vector, typename SolverK>
  struct vector_type<ImplicitSchur<Matrix, Vector, SolverK>>
    : public std::true_type {
    using type = Vector;
  };

  template <CPX_Matrix Matrix, CPX_Vector Vector, typename SolverK>
  struct scalar_type<ImplicitSchur<Matrix, Vector, SolverK>>
    : public std::true_type {
    using type = typename ImplicitSchur<Matrix, Vector, SolverK>::scalar_type;
  };
#+end_src

** Footer

#+begin_src c++
} //namespace composyx
#+end_src

* Tests
:PROPERTIES:
:header-args: c++ :tangle ../../../src/test/unittest/test_implicitschur.cpp :comments link
:END:

  #+begin_src c++
    #include <iostream>
    #include <vector>

    #ifdef COMPOSYX_USE_EIGEN
    #include <Eigen/Core>
    #include <Eigen/Sparse>
    #include <composyx/wrappers/Eigen/Eigen.hpp>
    #endif

    #ifdef COMPOSYX_USE_ARMA
    #include <armadillo>
    #include <composyx/wrappers/armadillo/Armadillo.hpp>
    #endif

    #include <composyx.hpp>
    #include <composyx/loc_data/SparseMatrixCOO.hpp>
    #include <composyx/solver/Pastix.hpp>
    #include <composyx/solver/ImplicitSchur.hpp>
    #include <composyx/testing/TestMatrix.hpp>
    #include <catch2/catch_test_macros.hpp>
    #include <catch2/catch_template_test_macros.hpp>

    TEMPLATE_TEST_CASE("ImplicitSchur", "[schur][sequential]", float, double, std::complex<float>, std::complex<double>){
      using namespace composyx;
      using Scalar = TestType;
      using Real = typename arithmetic_real<Scalar>::type;
      using SpMat = SparseMatrixCOO<Scalar>;
      using SolverK = Pastix<SpMat, Vector<Scalar>>;

      Real tol = 1.0e-5;
      SpMat K = test_matrix::general_matrix_4_4<SpMat>();

      SECTION("Test implicit schur VS real schur vector multiplication"){
        // Schur complement of the general matrix on the 2 last unknowns
        DenseMatrix<Scalar> schur;
        if constexpr(is_real<Scalar>::value){
          schur = DenseMatrix<Scalar>({6.,  0.23076922, 1., 7.61538458}, 2, 2, /*row_major*/ true);
        }
        else{
          schur = DenseMatrix<Scalar>({{6.35051546, -0.28865979}, {0.27835052, 0.12371134},
    				   {1.08247423, 1.81443299}, {7.53608247, 1.79381443}},
    	2, 2, /*row_major*/ true);
        }

        ImplicitSchur<SpMat, Vector<Scalar>, SolverK> impschur;
        impschur.setup(K);
        impschur.setup_schurlist(std::vector<int>{2, 3});

        Vector<Scalar> u{-1, 3};
        Vector<Scalar> res_exp = schur * u;
        Vector<Scalar> res = impschur * u;
        REQUIRE((res_exp - res).norm() < tol);
      }

      SECTION("Test implicit schur - half stored input matrix"){

        MatrixProperties<Scalar> prop;

        if constexpr(is_complex<Scalar>::value){
          prop.set_hpd(MatrixStorage::upper);
        }
        else{
          prop.set_spd(MatrixStorage::lower);
        }

        DenseMatrix<Scalar> fullmatrix = test_matrix::generate_matrix<DenseMatrix<Scalar>>(4, 4, prop, 0.8, true);
        fullmatrix.display("fullmatrix half");
        SpMat Kspd(fullmatrix);
        Kspd.display("Kspd");

        fullmatrix.to_storage_full();
        fullmatrix.display("fullmatrix");

        auto A11 = fullmatrix.get_block_view(0, 0, 2, 2);
        auto A12 = fullmatrix.get_block_view(0, 2, 2, 2);
        auto A21 = fullmatrix.get_block_view(2, 0, 2, 2);
        auto A22 = fullmatrix.get_block_view(2, 2, 2, 2);

        BlasSolver<DenseMatrix<Scalar>, DenseMatrix<Scalar>> A11_inv(A11);
        const DenseMatrix<Scalar> schur = A22 - A21 * (A11_inv * A12);

        ImplicitSchur<SpMat, Vector<Scalar>, SolverK> impschur;
        impschur.setup(Kspd);
        impschur.setup_schurlist(std::vector<int>{2, 3});

        const DenseMatrix<Scalar> schur_from_impl = impschur.get_dense_schur();
        REQUIRE((schur_from_impl - schur).norm() < tol);

        schur.display("schur");
        schur_from_impl.display("schur_from_impl");

        const Vector<Scalar> u{-1, 3};
        u.display("uA");
        const Vector<Scalar> res_exp = schur * u;
        u.display("uB");
        const Vector<Scalar> res = impschur * u;
        res_exp.display("res_exp");
        res.display("res");
        REQUIRE((res_exp - res).norm() < tol);
      }
    }
  #+end_src
