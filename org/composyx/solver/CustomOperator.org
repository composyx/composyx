label:solver:customoperator

* Lite operator
  :PROPERTIES:
  :header-args: c++ :tangle ../../../include/composyx/solver/CustomOperator.hpp :comments link
  :END:

  This class allows the user to implement a =composyx= minimalistic
  operator where only an =apply= function must be provided.

** Header

#+begin_src c++
  #pragma once

  #include <functional>

  namespace composyx {
#+end_src

** Class

#+begin_src c++
  template<typename Operand>
  class CustomOperator{
    using ApplyType = std::function<Operand(const Operand&)>;

    static Operand default_apply(const Operand&in){ return in; }

    ApplyType apply_fct = CustomOperator::default_apply;

  public:
    using vector_type = Operand;
    using apply_function_type = ApplyType;

    CustomOperator(){}
    CustomOperator(ApplyType f): apply_fct{f} {}
    void setup(auto){}
    Operand apply(const Operand& B){ return apply_fct(B); }

    void set_apply_fct(ApplyType afct){ apply_fct = afct; }
  }; //class CustomOperator

  template<typename Operand> [[nodiscard]]
  Operand operator* (CustomOperator<Operand>& op, const Operand& rhs){
    return op.apply(rhs);
  }
#+end_src

** Footer

 #+begin_src c++
 } // namespace composyx
 #+end_src
