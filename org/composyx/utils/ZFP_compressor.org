label:utils:zfpcompressor

* ZFP compressor
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../include/composyx/utils/ZFP_compressor.hpp :comments link
:END:

https://zfp.io/

** Header

#+begin_src c++
  #pragma once

  #include <zfp.hpp>
  #include <zfp/array1.hpp>

  #include "Arithmetic.hpp"

  namespace composyx {
#+end_src

** Compression modes

#+begin_src c++
  enum class ZFP_CompressionMode : int { ACCURACY, PRECISION, RATE };
#+end_src

** ZFP compressor class

  #+begin_src c++
    template<CPX_Scalar Scalar, ZFP_CompressionMode ComprMode>
    class ZFP_compressor{
    private:
      zfp_stream * _stream = nullptr;
      bitstream * _bit_stream = nullptr;
      std::vector<char> _compressed;
      size_t _n_elts = 0;
      size_t _compressed_bytes = 0;

      using Real = typename arithmetic_real<Scalar>::type;
      // Compression parameter is an integer for ZFP_PRECISION or ZFP_RATE, otherwise double
      using ComprParam = std::conditional_t<ComprMode == ZFP_CompressionMode::PRECISION ||
					    ComprMode == ZFP_CompressionMode::RATE,
					    unsigned int, double>;

    public:
      using Scalar_type = Scalar;
      using value_type = Scalar;
      using compression_param_type = ComprParam;
      constexpr static const ZFP_CompressionMode compression_mode = ComprMode;

      ZFP_compressor(){
	_stream = zfp_stream_open(NULL);
      }

      ZFP_compressor(std::span<const Scalar> data, ComprParam param){
	_stream = zfp_stream_open(NULL);
	compress(data, param);
      }

      ZFP_compressor(Scalar * data, int size, ComprParam param){
	_stream = zfp_stream_open(NULL);
	std::span<Scalar> spdata(data, data + size);
	compress(spdata, param);
      }

      // Avoid implicit conversion (double/int)
      template<typename T> ZFP_compressor(std::span<const Scalar>, T) = delete;
      template<typename T> ZFP_compressor(Scalar *, int, T) = delete;

      ~ZFP_compressor(){
	if(_bit_stream) stream_close(_bit_stream);
	if(_stream) zfp_stream_close(_stream);
      }

      // Copy is not OK
      ZFP_compressor(const ZFP_compressor&) = delete;
      ZFP_compressor& operator=(const ZFP_compressor&) = delete;

      // Move is OK
    private:
      void _move(ZFP_compressor&& other){
	if(_stream) zfp_stream_close(_stream);
	_stream = std::exchange(other._stream, nullptr);
	if(_bit_stream) stream_close(_bit_stream);
	_bit_stream = std::exchange(other._bit_stream, nullptr);
	_n_elts = std::exchange(other._n_elts, 0);
	_compressed_bytes = std::exchange(other._compressed_bytes, 0);
	_compressed = std::move(other._compressed);
      }

    public:
      ZFP_compressor(ZFP_compressor&& other){
	_move(std::move(other));
      }

      ZFP_compressor& operator=(ZFP_compressor&& other){
	_move(std::move(other));
	return *this;
      }

    private:
      [[nodiscard]] size_t _n_reals() const {
	constexpr const size_t real_per_scalar = is_complex<Scalar>::value ? 2 : 1;
	return _n_elts * real_per_scalar;
      }

      [[nodiscard]] static constexpr zfp_type _get_datatype(){
	if constexpr(is_precision_double<Scalar>::value){
	  return zfp_type_double;
	}
	else{
	  return zfp_type_float;
	}
      }
  #+end_src

** Set compression parameters

*** Fixed-accuracy compression

A first option is to set a tolerance for the compression. The absolute
value of the difference between the initial vector and the
decompressed vector are bounded by this tolerance (point-wise).

The actual tolerance set by zfp is returned.

#+begin_src c++
  double _set_accuracy(double tolerance){
    return zfp_stream_set_accuracy(_stream, tolerance);
  }
#+end_src

*** Fixed-precision compression

Fix the number of bits used to store each element of the compressed
vector.

#+begin_src c++
  unsigned int _set_precision(unsigned int precision){
    return zfp_stream_set_precision(_stream, precision);
  }
#+end_src

*** Fixed-rate compression

This number of compressed bits per block is amortized over the $4^d$
values to give a rate in bits per value, where $d$ is the dimension of
the array:

$$rate = \frac{maxbits}{4^d}$$

#+begin_src c++
  double _set_rate(unsigned int rate){
    return zfp_stream_set_rate(_stream, rate, _get_datatype(), 1, zfp_false);
  }
#+end_src

** Compression

  #+begin_src c++
    private:
    void _compress(const Scalar * data, size_t size, ComprParam param){
      _n_elts = size;

      if constexpr(ComprMode == ZFP_CompressionMode::ACCURACY){
	_set_accuracy(param);
      }
      else if constexpr(ComprMode == ZFP_CompressionMode::PRECISION){
	_set_precision(param);
      }
      else if constexpr(ComprMode == ZFP_CompressionMode::RATE){
	_set_rate(param);
      }

      zfp_field * field = zfp_field_1d((void *) data, _get_datatype(), _n_reals());

      // Get a maximal size for the buffer
      size_t max_bufsize = zfp_stream_maximum_size(_stream, field);

      // Allocate buffer and attach a "bitstream" to it
      _compressed = std::vector<char>(max_bufsize);
      _bit_stream = stream_open(_compressed.data(), max_bufsize);
      zfp_stream_set_bit_stream(_stream, _bit_stream);

      // Write metadata in the header of the bitsteam (return size of the header)
      _compressed_bytes = zfp_write_header(_stream, field, ZFP_HEADER_MAGIC);

      // rewind stream to beginning
      zfp_stream_rewind(_stream);
      // return value is byte size of compressed stream
      _compressed_bytes += zfp_compress(_stream, field);
      _compressed.resize(_compressed_bytes);

      zfp_field_free(field);
    }

    public:
    void compress(std::span<const Scalar> values, ComprParam param){
      _compress(values.data(), values.size(), param);
    }

    template<typename T> void compress(std::span<const Scalar>, T) = delete;
  #+end_src

** Decompress

  #+begin_src c++
    void decompress(Scalar * ptr) const {
      zfp_field * re_field = zfp_field_1d(ptr, _get_datatype(), _n_reals());

      // read header
      zfp_read_header(_stream, re_field, ZFP_HEADER_MAGIC); // read metadata

      zfp_stream_rewind(_stream); // rewind stream to beginning
      zfp_decompress(_stream, re_field);

      zfp_field_free(re_field);
    }

    std::vector<Scalar> decompress() const {
      std::vector<Scalar> out_vector(_n_elts);
      decompress(out_vector.data());
      return out_vector;
    }

    void decompress_and_free(Scalar * ptr){
      decompress(ptr);
      _compressed.clear();
      _n_elts = 0;
    }

    std::vector<Scalar> decompress_and_free(){
      std::vector<Scalar> out_vector(_n_elts);
      decompress_and_free(out_vector.data());
      return out_vector;
    }
   #+end_src

** Getters

   #+begin_src c++
     [[nodiscard]] double get_ratio() const {
       if(_compressed_bytes == 0) { return 0; }

       const double input_bytes = static_cast<double>(sizeof(Scalar) * _n_elts);
       return (input_bytes / static_cast<double>(_compressed_bytes));
     }

     [[nodiscard]] size_t get_compressed_bytes() const { return _compressed_bytes; }

     [[nodiscard]] size_t get_n_elts() const { return _n_elts; }
     }; // class ZFP_compressor
     } // namespace composyx
   #+end_src

* Tests

  #+begin_src c++ :results silent :tangle ../../../src/test/unittest/test_zfp_compressor.cpp :comments link
    #include <iostream>
    #include <vector>
    #include <map>

    #include <random>
    #include <composyx.hpp>
    #include <catch2/catch_test_macros.hpp>
    #include <catch2/catch_template_test_macros.hpp>
    #include <composyx/utils/ZFP_compressor.hpp>

    TEMPLATE_TEST_CASE("ZFP_compressor", "[compressor]", float, double, std::complex<float>, std::complex<double>){
      using namespace composyx;

      using Scalar = TestType;
      using Real = typename arithmetic_real<Scalar>::type;

      std::random_device rd;
      std::mt19937 gen(rd());
      Real min = -10;
      Real max = 10;
      std::uniform_real_distribution<> dis(min, max);
      auto random = [&gen,&dis](){ return static_cast<Real>(dis(gen)); };

      const size_t n_elts = 1000;
      const double zeta = double{1e-4};
      const unsigned int precision = 28;
      const unsigned int rate = 22;

      std::cout << "Vector of size: " << n_elts << '\n';

      auto generate_elts = [random](int n){
	std::vector<Scalar> cpr(n);
	for(size_t i = 0; i < cpr.size(); ++i){
	  if constexpr(is_complex<Scalar>::value){
	    cpr[i] = Scalar{random(), random()};
	  }
	  else{
	    cpr[i] = Scalar{random()};
	  }
	}
	return cpr;
      };

      std::vector<Scalar> to_compress = generate_elts(n_elts);

      SECTION("Compress/Decompress std::vector"){
	ZFP_compressor<Scalar, ZFP_CompressionMode::ACCURACY> compressor(to_compress, zeta);
	double ratio = compressor.get_ratio();
	std::cout << "Accuracy: " << zeta << '\n';
	std::cout << "Compression ratio (" << arithmetic_name<Scalar>::name << "): " << ratio << '\n';
	REQUIRE(ratio > double{1});

	std::vector<Scalar> decompressed = compressor.decompress();
	REQUIRE(decompressed.size() == n_elts);

	std::vector<Scalar> decompressed_2 = compressor.decompress();
	REQUIRE(decompressed_2 == decompressed);
      }

      SECTION("Compress/Decompress with pointers"){
	ZFP_compressor<Scalar, ZFP_CompressionMode::ACCURACY> compressor(to_compress.data(), to_compress.size(), zeta);
	double ratio = compressor.get_ratio();
	std::cout << "Accuracy: " << zeta << '\n';
	std::cout << "Compression ratio (" << arithmetic_name<Scalar>::name << "): " << ratio << '\n';
	REQUIRE(ratio > double{1});

	std::vector<Scalar> decompressed(compressor.get_n_elts());
	compressor.decompress(decompressed.data());
	REQUIRE(decompressed.size() == n_elts);
      }

      SECTION("Compress/Decompress with fixed rate"){
	ZFP_compressor<Scalar, ZFP_CompressionMode::RATE> compressor;
	compressor.compress(to_compress, rate);
	double ratio = compressor.get_ratio();
	std::cout << "Rate: " << rate << '\n';
	std::cout << "Compression ratio (" << arithmetic_name<Scalar>::name << "): " << ratio << '\n';
	REQUIRE(ratio > double{1});

	std::vector<Scalar> decompressed = compressor.decompress();
	REQUIRE(decompressed.size() == n_elts);

	std::vector<Scalar> decompressed_2 = compressor.decompress();
	REQUIRE(decompressed_2 == decompressed);
      }

      SECTION("Compress/Decompress with fixed precision"){
	ZFP_compressor<Scalar, ZFP_CompressionMode::PRECISION> compressor;
	compressor.compress(to_compress, precision);
	double ratio = compressor.get_ratio();
	std::cout << "Precision: " << precision << '\n';
	std::cout << "Compression ratio (" << arithmetic_name<Scalar>::name << "): " << ratio << '\n';
	REQUIRE(ratio > double{1});

	std::vector<Scalar> decompressed = compressor.decompress();
	REQUIRE(decompressed.size() == n_elts);

	std::vector<Scalar> decompressed_2 = compressor.decompress();
	REQUIRE(decompressed_2 == decompressed);
      }

    }
  #+end_src
