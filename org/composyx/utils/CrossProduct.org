label:utils:crossproduct

* Purpose

This class is an input iterator generating the cross
product of two arrays element by element.

The implementation allows range for loop, and returns a pair.

Example:

#+begin_src c++ :results silent
std::vector<int> v1{0, 2, 3};
std::vector<int> v2{4, 5};
for(auto& [first, second] : CrossProduct(v1, v2)){
  std::cout << first << ", " << second << '\n';
}
#+end_src

Must return:
#+begin_src
0, 4
0, 5
2, 4
2, 5
3, 4
3, 5
#+end_src

* CrossProduct class
:PROPERTIES:
:header-args: c++ :results silent :tangle ../../../include/composyx/utils/CrossProduct.hpp :comments link
:END:

** Header

#+begin_src c++
#pragma once
#include <vector>
#include <algorithm>
#include "Error.hpp"

namespace composyx {
#+end_src

** Attributes and constructor

We keep constant references to the two arrays for which we
compute the cross product =_a1= and =_a2=. We keep an iterator
for each array: =_i1= and =_i2=.

#+begin_src c++
  template<class T1, class T2>
  class CrossProduct{

  private:
    const T1& _a1;
    const T2& _a2;
    typename T1::const_iterator _i1;
    typename T2::const_iterator _i2;

  public:
    CrossProduct(const T1& a1, const T2& a2):
      _a1{a1}, _a2{a2}, _i1{a1.begin()}, _i2{a2.begin()}
    {}

    CrossProduct(const CrossProduct&) = default;
#+end_src

** Traits

#+begin_src c++

  using value_type = std::pair<typename T1::value_type, typename T2::value_type>;

#+end_src

** Input iterator methods

We implement =operator++= to access the next element and
=operator*= to access the current element.

To access the next element, the second index is increased.
If it exceeds the size of the second array, it is reset to 0
and the first index is increased. If the last element
was produced, we don't update the pair anymore to avoid
array reading after the array.

#+begin_src c++
  CrossProduct& operator++(){
    ++_i2;
    if (_i2 == _a2.end()) {
      _i2 = _a2.begin();
      ++_i1;
    }
    return *this;
  }

  // Post-increment (x++)
  CrossProduct operator++(int){
    CrossProduct p = *this;
    this->operator++();
    return p;
  }

  value_type operator*() const {
    return std::make_pair(*_i1, *_i2);
  }

  size_t size(){ return _a1.size() * _a2.size(); }
#+end_src

** Range methods

We implement our range loop functions to identify the first
and last element of the iterator.

Notice that the end element is defined with the first index
is equal to the size of the first array and the second index is 0.

#+begin_src c++
  CrossProduct begin(){
    if(size() == 0) return end();
    CrossProduct p(_a1, _a2);
    p._i1 = _a1.begin();
    p._i2 = _a2.begin();
    return p;
  }

  CrossProduct end() const {
    CrossProduct p(_a1, _a2);
    p._i1 = _a1.end();
    p._i2 = _a2.begin();
    return p;
  }
#+end_src

** Comparison functions

Necessary to identify the last element (in the range for loop).

#+begin_src c++
  bool operator!=(const CrossProduct& other) const {
    return (_i1 != other._i1) or (_i2 != other._i2);
  }
#+end_src

** Footer

#+begin_src c++
};// CrossProduct
} // end namespace composyx
#+end_src

* Tests

#+begin_src c++ :results silent :tangle ../../../src/test/unittest/test_cross_product.cpp :comments link

  #include <composyx.hpp>
  #include "composyx/utils/CrossProduct.hpp"
  #include <composyx/testing/TestMatrix.hpp>

  #include <catch2/catch_test_macros.hpp>

  TEST_CASE("CrossProduct", "[cross_product]"){

    using namespace composyx;

    SECTION("Basic cross product test"){
      std::vector<int> v1{0, 2, 3};
      std::vector<int> v2{4, 5};

      std::vector<std::pair<int,int>> vals({{0, 4},{0, 5},{2, 4},{2, 5},{3, 4},{3, 5}});
      std::size_t k = 0;

      for(auto [first, second] : CrossProduct(v1, v2)){
        REQUIRE(first == vals[k].first);
        REQUIRE(second == vals[k].second);
        k++;
      }
      REQUIRE(k == vals.size());
    }

    SECTION("0 iteration test (v2 size 0)"){
      std::vector<int> v1{0, 2, 3};
      std::vector<int> v2;
      int passed_in_the_loop = 0;
      for(auto cp : CrossProduct(v1, v2)){
        std::cout << cp.first << ", " << cp.second << '\n';
        passed_in_the_loop++;
      }
      REQUIRE(passed_in_the_loop == 0);
    }

    SECTION("0 iteration test (v1 size 0)"){
      std::vector<int> v1;
      std::vector<int> v2{0,1,2};
      int passed_in_the_loop = 0;
      for(auto cp : CrossProduct(v1, v2)){
        std::cout << cp.first << ", " << cp.second << '\n';
        passed_in_the_loop++;
      }
      REQUIRE(passed_in_the_loop == 0);
    }
  }
#+end_src
