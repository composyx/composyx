label:testing:testiterativesolver

* Sequential

** Header

#+begin_src c++ :results silent :tangle ../../../src/test/unittest/test_iterative_solver.hpp :comments link
#pragma once
#include <composyx.hpp>
#include <catch2/catch_test_macros.hpp>

namespace composyx{

template<typename Scalar, typename Matrix, typename Vector, typename Solver>
void test_solver(const Matrix& A, const Vector& X, const int max_iter = 100, const bool verbose = false){
  using Real = typename arithmetic_real<Scalar>::type;

  Vector B = A * X;

  Real tol = arithmetic_tolerance<Scalar>::value;

  Solver s;
  s.setup(parameters::A<Matrix>{A},
          parameters::tolerance<Real>{tol},
          parameters::max_iter<int>{max_iter},
          parameters::verbose<bool>{verbose});

  Vector sol = s * B;

  REQUIRE( s.get_n_iter() != -1 );
  Real Bsq = std::real(dot(B, B));
  Real Rsq = s.get_residual() * s.get_residual();
  Real backward_error;
  if(Bsq == Real{0}){
    backward_error = 0;
  }
  else{
    backward_error = Rsq/Bsq;
  }
  REQUIRE( backward_error < tol );
}
} //namespace composyx
#+end_src
