#define FABULOUS_USE_COMPOSYX_TIMER
#define TIMER_LEVEL_MAX 201
#define TIMER_LEVEL_MIN 0

#ifdef COMPOSYX_USE_RSB_SPBLAS
#include <rsb.h>         /* for rsb_lib_init */
#include <blas_sparse.h> /* Sparse BLAS on the top of librsb */
#else
#ifdef COMPOSYX_USE_RSB
#include <rsb.hpp>
#include <composyx/wrappers/librsb/Librsb.hpp>
#endif
#endif

#include <fstream>
#include <iostream>
#include <vector>
#include <stdlib.h>

// #include <fabulous.hpp>

#include <composyx.hpp>
#include <composyx/solver/Fabulous.hpp>
#include <composyx/solver/GCR.hpp>
#include <composyx/solver/ConjugateGradient.hpp>
#include <composyx/IO/ReadParam.hpp>
#include <composyx/loc_data/SparseMatrixCOO.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <composyx/testing/catch.hpp>

#include <composyx/precond/Dummy.hpp>
#include <composyx/precond/TruncateColumns.hpp>
#include <composyx/precond/DiagonalPrecond.hpp>
#include <composyx/solver/CastOperator.hpp>
#include <composyx/solver/Pastix.hpp>
// #include <composyx/solver/Mumps.hpp>

#include "Timer.hpp"

const std::string blas_name =
#if defined COMPOSYX_USE_RSB_SPBLAS

#if defined COMPOSYX_USE_MKL
    "mkl/rsb_blas";
#else
    "openblas/rsb_blas";
#endif

#else // COMPOSYX_USE_RSB_SPBLAS

#if defined COMPOSYX_USE_RSB

#if defined COMPOSYX_USE_MKL
    "mkl/rsb";
#else
    "openblas/rsb";
#endif

#else // COMPOSYX_USE_RSB

#if defined COMPOSYX_USE_MKL_SPBLAS
    "mkl/mkl";
#else
    "openblas/m++";
#endif

#endif

#endif

using namespace composyx;

#define FAB_BENCH(SOLVER_NAME, SETUP, K, ...)                                  \
  {                                                                            \
    Timer<0> t_setup("Setup");                                                 \
    __VA_ARGS__ fab;                                                           \
    SETUP(K)                                                                   \
    t_setup.stop();                                                            \
    const double setup = Timer<0>::get_event_cumul_time("Setup");              \
    multi_run<__VA_ARGS__>(fab, A, X, B, setup, SOLVER_NAME, NB_FAMILY, bench, \
                           ortho_type);                                        \
  }

#define FAB_SETUP(X)                                                           \
  fab.setup(fabulous_params::A<SpMat>{A},                                      \
            fabulous_params::tolerances<std::vector<Real>>{tol},               \
            fabulous_params::max_iter<int>{max_iter},                          \
            fabulous_params::max_krylov_space<int>{X},                         \
            fabulous_params::ortho_type{fabulous::OrthoType::ORTHO_METHOD},    \
            fabulous_params::ortho_scheme{ortho_type},                         \
            fabulous_params::verbose<bool>{verbose});

#define FAB_SETUP_GCRO(X)                                                      \
  fab.setup(fabulous_params::A<SpMat>{A},                                      \
            fabulous_params::tolerances<std::vector<Real>>{tol},               \
            fabulous_params::max_iter<int>{max_iter},                          \
            fabulous_params::max_krylov_space<int>{X},                         \
            fabulous_params::ortho_type{fabulous::OrthoType::ORTHO_METHOD},    \
            fabulous_params::ortho_scheme{ortho_type},                         \
            fabulous_params::verbose<bool>{verbose});
// fabulous_params::Uk{Uk});

#define FAB_SETUP_DR(X)                                                        \
  fab.setup(fabulous_params::A<SpMat>{A},                                      \
            fabulous_params::tolerances<std::vector<Real>>{tol},               \
            fabulous_params::max_iter<int>{max_iter},                          \
            fabulous_params::max_krylov_space<int>{X},                         \
            fabulous_params::ortho_type{fabulous::OrthoType::ORTHO_METHOD},    \
            fabulous_params::ortho_scheme{ortho_type},                         \
            fabulous_params::verbose<bool>{verbose},                           \
            fabulous_params::deflated_restart{                                 \
                fabulous::deflated_restart<Scalar>(k, Scalar{0})});

#define FAB_SETUP_GCRO_DR(X)                                                   \
  fab.setup(fabulous_params::A<SpMat>{A},                                      \
            fabulous_params::tolerances<std::vector<Real>>{tol},               \
            fabulous_params::max_iter<int>{max_iter},                          \
            fabulous_params::max_krylov_space<int>{X},                         \
            fabulous_params::ortho_type{fabulous::OrthoType::ORTHO_METHOD},    \
            fabulous_params::ortho_scheme{ortho_type},                         \
            fabulous_params::verbose<bool>{verbose},                           \
            fabulous_params::deflated_restart{                                 \
                fabulous::deflated_restart<Scalar>(k, Scalar{0})});
// #define ORTHO_METHOD BLOCK
#define ORTHO_METHOD RUHE
#define NB_FAMILY 3
#define TAG(SOLVER_NAME)                                                       \
  blas_name + ',' + type_to_str<Scalar>() + ',' + SOLVER_NAME + ",MGS," +      \
      std::to_string(i) + ',' + std::to_string(fab.get_n_iter()) + ','

int trace = 0;

template <typename Scalar> const std::string type_to_str(void) {
  static const std::string name[4] = {"real (simple)", "real (double)",
                                      "complex (simple)", "complex (double)"};
  if constexpr (std::is_same_v<Scalar, float>) {
    return name[0];
  } else if constexpr (std::is_same_v<Scalar, double>) {
    return name[1];
  } else if (std::is_same_v<Scalar, std::complex<float>>) {
    return name[2];
  } else if (std::is_same_v<Scalar, std::complex<double>>) {
    return name[3];
  }
}

template <typename SpMat, typename Scalar>
void display(const std::string name, const int n, const int n_iter,
             const SpMat& A, const DenseMatrix<Scalar>& B1,
             const DenseMatrix<Scalar>& X1_exp, const DenseMatrix<Scalar>& X1,
             const double t_setup, const int bench,
             const fabulous::OrthoScheme ortho_type) {

  const double X1_norm = X1_exp.frobenius_norm();
  // I'm a bugfix line
  auto C = A * X1_exp;
  // comment for crach with librsb spblas
  const double rr = (A * X1 - B1).frobenius_norm() / B1.frobenius_norm();
  const double t_solve = Timer<0>::get_event_cumul_time("Solve");
  std::string o_str = "MGS";
  if (ortho_type == fabulous::OrthoScheme::CGS)
    o_str = "CGS";

  if (bench == 1 && !trace) {
    std::cout << blas_name << "," << name << "," << o_str << "," << n << ","
              << n_iter << ",";
    std::cout << t_setup << "," << t_solve << ","
              << (X1_exp - X1).frobenius_norm() / X1_norm;
    std::cout << "," << type_to_str<Scalar>() << ',' << rr << ','
              << B1.get_n_cols() << '\n';
  } else if (bench == 0 && !trace) {
    std::cout << "[using" << blas_name << "]\n";
    std::cout << type_to_str<Scalar>() << "\n";
    std::cout << name << " + " << o_str << "\n";
    std::cout << "iterations : " << n_iter << "\n";
    std::cout << "Setup time : " << t_setup << "\n";
    std::cout << "Solve time : " << t_solve << "\n";
    std::cout << "||X_exp - X||_F / ||X_exp||_F = "
              << (X1_exp - X1).frobenius_norm() / X1_norm << "\n";
    std::cout << "||AX - B||_F / ||B||_F = " << rr << "\n";
    std::cout << "nrhs = " << B1.get_n_cols() << "\n";
  }
}

template <typename Scalar>
void print_cvg_fabulous(fabulous::Logger& log, const std::string name,
                        const int n, const fabulous::OrthoScheme ortho_type,
                        const int nrhs) {
  std::string o_str = "MGS";
  if (ortho_type == fabulous::OrthoScheme::CGS)
    o_str = "CGS";
  std::string blas_name_n = blas_name + ',' + type_to_str<Scalar>() + ',' +
                            name + ',' + o_str + ',' + std::to_string(n) + ',' +
                            std::to_string(nrhs) + ',';

  const double* data = log.write_down_array();
  double time_from_start = 0.0;
  auto iter = log.get_iterations();
  // std::cerr << log.get_nb_iterations() << '\n';
  for (int i = 0; i < log.get_nb_iterations(); ++i) {
    time_from_start += data[3 * i + 2];
    std::cout << blas_name_n << iter[i].nb_mvp << "," << data[3 * i + 0] << ","
              << data[3 * i + 1] << "," << time_from_start << "\n";
  }
}

std::vector<double> norm_list;
std::vector<double> time_list;

template <typename Scalar>
void print_norm_list(const std::string name, const int n) {
  std::string blas_name_n = blas_name + ',' + type_to_str<Scalar>() + ',' +
                            name + ",MGS," + std::to_string(n) + ',';

  for (size_t i = 0; i < norm_list.size(); i++) {
    std::cout << blas_name_n << i << ',' << norm_list[i] << ',' << norm_list[i]
              << ',' << time_list[i] << '\n';
  }
}

template <class FSolver, typename SpMat, typename Scalar>
void run(FSolver& fab, const SpMat& A, const DenseMatrix<Scalar>& X1_exp,
         const DenseMatrix<Scalar>& B1, const double t_setup,
         const std::string name, const int n, const int bench,
         const fabulous::OrthoScheme ortho_type) {

  Timer<0> t_solve("Solve");
  const DenseMatrix<Scalar> X1 = fab * B1;
  t_solve.stop();

  if (bench == 2 && !trace) {
    if constexpr (std::is_same<FSolver,
                               GCR<SpMat, composyx::Vector<Scalar>>>::value ||
                  std::is_same<FSolver,
                               ConjugateGradient<
                                   SpMat, composyx::Vector<Scalar>>>::value) {
      print_norm_list<Scalar>(name, n);
    } else {
      fabulous::Logger log = fab.get_last_log();
      print_cvg_fabulous<Scalar>(log, name, n, ortho_type, B1.get_n_cols());
    }
  } else {
    display(name, n, fab.get_n_iter(), A, B1, X1_exp, X1, t_setup, bench,
            ortho_type);
  }
  Timer<0>::reset();

  norm_list.clear();
  time_list.clear();
}

template <class FSolver, typename SpMat, typename Scalar>
void multi_run(FSolver& fab, const SpMat& A,
               const std::vector<DenseMatrix<Scalar>>& X,
               const std::vector<DenseMatrix<Scalar>>& B, const double t_setup,
               const std::string name, const int n_max, const int bench,
               const fabulous::OrthoScheme ortho_type) {

  for (int i = 0; i < n_max; i++) {
    run(fab, A, X[i], B[i], t_setup, name, i, bench, ortho_type);
    if (trace)
      std::cout << (TAG(name)) << '\n';
  }
}

template <typename SpMat, typename Scalar>
void callback(const SpMat&, const composyx::Vector<Scalar>& B,
              const composyx::Vector<Scalar>&,
              const composyx::Vector<Scalar>& R) {

  static double B_norm = B.norm();
  static int iter = 0;

  // reset for a new run
  if (static_cast<int>(time_list.size()) != iter) {
    iter = 0;
    B_norm = B.norm();
  }

  norm_list.push_back(R.norm() / B_norm);
  if (iter++ == 0) { // First iteration
    time_list.push_back(Timer<0>::get_event_cumul_time("GCR iteration 0"));
  } else {
    time_list.push_back(Timer<0>::get_event_cumul_time("GCR iteration"));
  }
}

template <typename Scalar, typename Scalar2 = float>
void experiments(const std::string path_A, int n, int bench, int is_sym,
                 const fabulous::OrthoScheme ortho_type) {
  using Real = typename arithmetic_real<Scalar>::type;

  SparseMatrixCOO<Scalar> A1;
  A1.from_matrix_market_file(path_A);

  // std::cerr << A.properties_str() << "\n";
  if (is_sym)
    A1.fill_half_to_full_storage();

  // #ifdef COMPOSYX_USE_RSB
  //      using SpMat = RsbSparseMatrix<Scalar>;
  //      SpMat A = convert(A1);
  //      rsb_real_t sf{-1};
  //      const Scalar alpha {1.0}, beta {2.0};
  //      const auto nrhs { n / 2 };
  //      // use autotuning for nrhs=n/2 (assuming n decreases..)
  //      // note: librsb-1.3's SpMM is better with
  //      RSB_FLAG_WANT_ROW_MAJOR_ORDER
  //      A.tune_spmm(sf,RSB_TRANSPOSITION_N,alpha,nrhs,RSB_FLAG_WANT_COLUMN_MAJOR_ORDER,nullptr,beta,nullptr);
  //      std::cerr << "autotuning expects speedup: "  << sf << "x\n";
  // #else
  using SpMat = SparseMatrixCOO<Scalar>;
  SpMat A = A1;
  // #endif

  const Real tol_0 = 1.0e-8;
  const std::vector<Real> tol{tol_0};
  const bool verbose = !true;
  /*const*/ int restart_iter = 250;
  const int m = A.get_n_rows(), nnz_max = m * n;
  const Real max = 10000.0;

  // Iter max basically
  // the number of matrix vector products for n=10 -> at least 4 spmm calls
  const int max_iter = 120;
  // number before restart
  // Can be anything less than max_iter
  // const int max_krylow_space = 16;//restart_iter;
  // Number of vector keep
  // Can be anything less than max_krylow_space
  const int k = 5; // Deflation space's size

  /*
  SparseMatrixCOO<Scalar> Xf;
  Xf.from_matrix_market_file("../matrices/rhs_bad.mtx");
  const DenseMatrix<Scalar> X1_exp = Xf.to_dense();// =
  test_matrix::random_matrix<Scalar, DenseMatrix<Scalar>>(nnz_max, -max, max, m,
  n).matrix;
  //*/
  std::vector<DenseMatrix<Scalar>> X;
  std::vector<DenseMatrix<Scalar>> B;
  for (int i = 0; i < NB_FAMILY; i++) {
    DenseMatrix<Scalar> Xi =
        test_matrix::random_matrix<Scalar, DenseMatrix<Scalar>>(nnz_max, -max,
                                                                max, m, n)
            .matrix;
    X.push_back(Xi);
    B.push_back(A * Xi);
  }

  using DMat = DenseMatrix<Scalar>;

  // using Pastix32 = Pastix<SparseMatrixCOO<Scalar2>, DenseMatrix<Scalar2>,
  // false>; using Pastix64 = Pastix<SparseMatrixCOO<Scalar>, DMat, false>;
  // using DirSolver = Mumps<SparseMatrixCOO<Scalar2>, DenseMatrix<Scalar2>>;
  // using DirSolver = DiagonalPrecond<SparseMatrixCOO<Scalar2>,
  // DenseMatrix<Scalar2>>; using CO = CastOperator<SparseMatrixCOO<Scalar>,
  // DMat, Pastix32>;

  // Preconditionneer uncomment to select
  using Precond = Identity<SpMat, DMat>;
  // using Precond = DummyPrecond<SpMat, DMat, NoPrecond>;
  // using Precond = DiagonalPrecond<SpMat, DMat>;
  // using Precond = DummyPrecond<SpMat, DMat, CO>;
  // using Precond = TruncateColumns<SpMat, DMat, Pastix64, 1, 2>;
  // using Precond = TruncateColumns<SpMat, DMat, 1, 3, DummyPrecond<SpMat,
  // DMat, CO>>; using Precond = Pastix64;

  // int n_iter = -1;
  //  Test for k = 0
  // FAB_BENCH("IB-BGCRO (\u221E) k = 0", FAB_SETUP, max_iter, Fabulous<SpMat,
  // DMat, Precond, fabulous::bgcro::ARNOLDI>) FAB_BENCH("IB-BGCRO (\u221E) k =
  // " + std::to_string(k), FAB_SETUP_GCRO, max_iter, Fabulous<SpMat, DMat,
  // Precond, fabulous::bgcro::ARNOLDI>) FAB_BENCH("IB-BGMRES (\u221E)",
  // FAB_SETUP, max_iter, Fabulous<SpMat, DMat, Precond,
  // fabulous::bgmres::ARNOLDI_IB>)

  // Test for k > 0
  // FAB_BENCH("IB-BGCRO (" + std::to_string(restart_iter) + ") k = 0",
  // FAB_SETUP, restart_iter, Fabulous<SpMat, DMat, Precond,
  // fabulous::bgcro::ARNOLDI>) FAB_BENCH("IB-BGCRO (" +
  // std::to_string(restart_iter) + ") k = " + std::to_string(k),
  // FAB_SETUP_GCRO, restart_iter, Fabulous<SpMat, DMat, Precond,
  // fabulous::bgcro::ARNOLDI>) FAB_BENCH("IB-BGMRES (" +
  // std::to_string(restart_iter) + ")", FAB_SETUP, restart_iter,
  // Fabulous<SpMat, DMat, Precond, fabulous::bgmres::ARNOLDI_IB>)

  FAB_BENCH("IB-BGMRES-DR (" + std::to_string(restart_iter - k) +
                ") k = " + std::to_string(k),
            FAB_SETUP_DR, restart_iter,
            Fabulous<SpMat, DMat, Precond, fabulous::bgmres::ARNOLDI_IBDR,
                     fabulous::DeflatedRestart<Scalar>>)
  restart_iter -= k;
  FAB_BENCH("IB-BGCRO-DR (" + std::to_string(restart_iter) +
                ") k = " + std::to_string(k),
            FAB_SETUP_DR, restart_iter,
            Fabulous<SpMat, DMat, Precond, fabulous::bgcro::ARNOLDI,
                     fabulous::DeflatedRestart<Scalar>>)

  // FAB_BENCH("IB-BGCR (\u221E)", FAB_SETUP, max_iter, Fabulous<SpMat, DMat,
  // Precond, fabulous::bgcr::ARNOLDI>) FAB_BENCH("IB-BGCR (" +
  // std::to_string(restart_iter) + ")", FAB_SETUP, max_krylow_space,
  // Fabulous<SpMat, DMat, Precond, fabulous::bgcr::ARNOLDI>)

  // FAB_BENCH("BGMRES (\u221E)", FAB_SETUP, max_iter, Fabulous<SpMat, DMat>)
  // FAB_BENCH("BGMRES (" + std::to_string(restart_iter) + ")", FAB_SETUP,
  // max_krylow_space, Fabulous<SpMat, DMat, Precond>)

  // FAB_BENCH("IB-BGMRES-DR (\u221E)", FAB_SETUP_DR, max_iter, Fabulous<SpMat,
  // DMat, Precond,fabulous::bgmres::ARNOLDI_IBDR,
  // fabulous::DeflatedRestart<Scalar>>)
}

int main(int argc, char** argv) {
  MMPI::init();
#ifdef COMPOSYX_USE_RSB_SPBLAS
  if (rsb_lib_init(RSB_NULL_INIT_OPTIONS) != RSB_ERR_NO_ERROR) {
    std::cerr << "Not able to start librsb\n";
    MMPI::finalize();
    return -1;
  }
#else
#ifdef COMPOSYX_USE_RSB
  if (rsb_lib_init(RSB_NULL_INIT_OPTIONS) != RSB_ERR_NO_ERROR) {
    std::cerr << "Not able to start librsb\n";
    MMPI::finalize();
    return -1;
  }
#endif
#endif
  // std::ofstream outfile;
  // outfile.open("trace.csv");
  // Timer<0>::set_trace_output(&outfile);

  std::string path_A = "";
  int n = 0;
  int bench = 0;
  int is_sym = 0;
  auto ortho_type = fabulous::OrthoScheme::MGS;
  try {
    // name of file of matrix A
    path_A = argv[1];
    // number of columns of B1
    n = atoi(argv[2]);
    // bench type
    const std::string s(argv[3]);
    bench = (s == "-b") + (s == "-p") * 2 + (s == "-ps") * 2;
    is_sym = (s == "-ps") + (s == "-s");
    trace = (s == "-t");
    // Orthogonalisation process
    if (argc > 4) {
      const std::string ortho_string(argv[4]);
      if (ortho_string == "-CGS")
        ortho_type = fabulous::OrthoScheme::CGS;
    }
  } catch (const std::out_of_range& e) {
    std::cerr << "invalid usage\n";
    return 0;
  }
  Timer<0>::set_trace(trace);
  std::cout.precision(3);
  std::cout << std::scientific;
  //  std::cerr.precision(3);
  //  std::cerr << std::scientific;

  srand(1);

  if (matrix_market::is_real_matrix(path_A)) {
    experiments<double>(path_A, n, bench, is_sym, ortho_type);
  } else {
    experiments<std::complex<double>, std::complex<float>>(path_A, n, bench,
                                                           is_sym, ortho_type);
  }

//*/
// outfile.close();
#ifdef COMPOSYX_USE_RSB_SPBLAS
  if (rsb_lib_exit(RSB_NULL_EXIT_OPTIONS) != RSB_ERR_NO_ERROR) {
    std::cerr << "Not able to exit librsb\n";
    MMPI::finalize();
    return -1;
  }
#ifdef COMPOSYX_USE_RSB
  if (rsb_lib_exit(RSB_NULL_EXIT_OPTIONS) != RSB_ERR_NO_ERROR) {
    std::cerr << "Not able to exit librsb\n";
    MMPI::finalize();
    return -1;
  }
#endif
#endif
  MMPI::finalize();
  return 0;
}
