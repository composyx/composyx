#include <iostream>
#include <fstream>

#define TIMER_LEVEL_MAX 1001
#define TIMER_LEVEL_MIN 0

#include <composyx.hpp>
#include <composyx/IO/ReadParam.hpp>
#include <composyx/part_data/PartMatrix.hpp>
#include <composyx/solver/ConjugateGradient.hpp>
#include <composyx/solver/BlasSolver.hpp>
#include <composyx/solver/PartSchurSolver.hpp>
#include <composyx/precond/AbstractSchwarz.hpp>
#if defined(COMPOSYX_USE_MUMPS)
#include <composyx/solver/Mumps.hpp>
#endif
#if defined(COMPOSYX_USE_PASTIX)
#include <composyx/solver/Pastix.hpp>
#endif

using namespace composyx;
using Scalar = double;
using Real = double;

using LocMat = SparseMatrixCOO<Scalar>;
using LocVect = Vector<Scalar>;

using SpMat = PartMatrix<LocMat>;
using Vect = PartVector<LocVect>;

const bool root_verbose = true;

void usage(char** argv) {
  if (MMPI::rank() != 0)
    return;
  std::cerr << "Usage: " << std::string(argv[0])
            << " subdomains_path n_subdomains dir_solver precision\n";
  std::cerr << "- subdomains_path: path to partitions to load\n";
  std::cerr << "- n_subdomains: number of partitions\n";
  std::cerr << "- dir_solver: must be 'pastix' or 'mumps'\n";
  std::cerr << "- precisions: low rank precision (e.g. '1e-5'), use a negative "
               "value for full rank\n";
}

template <typename DirectSolver>
void bench(const SpMat& A, const Vect& b, const std::string& benchname,
           std::map<std::string, int>& n_iterations, double precision) {

  using DenseSolver = BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>>;
  using Precond = AdditiveSchwarz<PartMatrix<DenseMatrix<Scalar>>,
                                  PartVector<Vector<Scalar>>, DenseSolver>;
  using CGType = ConjugateGradient<PartMatrix<DenseMatrix<Scalar>>,
                                   PartVector<Vector<Scalar>>, Precond>;
  using Solver = PartSchurSolver<SpMat, Vect, DirectSolver, CGType>;

  // if(MMPI::rank() == 0) Timer<0>::set_debug(true);

  bool verbose = (root_verbose && MMPI::rank() == 0);

  Solver solver;

  if (precision > double{0}) {
    auto setup_dir_solver = [precision](DirectSolver& dsolver) {
      // dsolver.set_verbose(2);
      dsolver.set_low_rank(precision);
    };
    solver.setup(parameters::setup_solver_K<std::function<void(DirectSolver&)>>{
        setup_dir_solver});
  }

  solver.setup(A);
  CGType& cg = solver.get_solver_S();

  cg.setup(parameters::max_iter{1000}, parameters::tolerance{1e-8},
           parameters::verbose{verbose});

  if (MMPI::rank() == 0)
    std::cout << "##### Bench: " << benchname << '\n';

  Timer<1> t("Bench time");
  Vect X = solver * b;
  if (MMPI::rank() == 0)
    cg.display("CG");
  n_iterations[benchname] = cg.get_n_iter();
}

int main(int argc, char** argv) {

  Timer<0> tt("Total time");

  MMPI::init();
  {
    if (argc != 5) {
      usage(argv);
      MMPI::finalize();
      return 1;
    }

    const std::string mat_filename = std::string(argv[1]);
    const int n_subdomains = std::stoi(argv[2]);
    const std::string solver_name = std::string(argv[3]);
    const double precision = std::stod(argv[4]);

    if (solver_name != "pastix" && solver_name != "mumps") {
      usage(argv);
      MMPI::finalize();
      return 1;
    }

    if (MMPI::rank() == 0) {
      std::cout << "Matrix partitions: " << mat_filename << '\n';
      std::cout << "Number of subdomains: " << n_subdomains << '\n';
      std::cout << "Direct solver: " << solver_name << '\n';
      std::cout << "Low-rank precision: " << precision << "\n\n";
    }

    auto p = bind_subdomains(n_subdomains);
    SpMat Acoo(p);

    Vect b(p);
    load_subdomains_and_data(mat_filename, n_subdomains, p, Acoo, b);
    b.assemble();

    Acoo.apply_on_data([](LocMat& m) {
      m.set_spd(MatrixStorage::lower);
      m.order_indices();
    });

    double inv_normA = 1. / Acoo.frobenius_norm();
    if (MMPI::rank() == 0)
      std::cout << "1/||A||: " << inv_normA << '\n';
    Acoo *= inv_normA;
    b *= inv_normA;

    std::map<std::string, int> n_iterations;

#if defined(COMPOSYX_USE_PASTIX)
    if (solver_name == "pastix") {
      bench<Pastix<LocMat, LocVect>>(Acoo, b, "Pastix", n_iterations,
                                     precision);
    }
#endif
#if defined(COMPOSYX_USE_MUMPS)
    if (solver_name == "mumps") {
      bench<Mumps<LocMat, LocVect>>(Acoo, b, "Mumps", n_iterations, precision);
    }
#endif

    MMPI::barrier();
    if (MMPI::rank() == 0) {
      tt.results(std::cout, /*detailed*/ false);
    }

    tt.stop();

    std::vector<std::string> keys = Timer<1>::get_event_keys();
    std::sort(keys.begin(), keys.end());
    std::map<std::string, double> output_times;
    for (std::string& key : keys) {
      double time = Timer<1>::get_event_cumul_time(key);
      double max_time;
      MMPI::reduce(&time, &max_time, 1, MPI_MAX, 0, MPI_COMM_WORLD);
      output_times[key] = max_time;
    }

    if (MMPI::rank() == 0) {
      std::cout << "\n-----------------------------\n";
      for (const auto& [key, time] : output_times) {
        std::cout << key << ": " << time << '\n';
      }
      std::cout << "\n-----------------------------\n";
      std::cout << "Number of iterations\n";
      for (const auto& [solver, niter] : n_iterations) {
        std::cout << "Niter-" << solver << ": " << niter << '\n';
      }
    }
  } // MMPI::init()
  MMPI::finalize();

  return 0;
}
