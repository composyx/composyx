#include <iostream>
#include <fstream>

#define TIMER_RECORD_CUMULATIVE
#define TIMER_LEVEL_MAX 1011
#define TIMER_LEVEL_MIN 0

#include <composyx.hpp>
#include <composyx/solver/GMRES.hpp>
#include <composyx/solver/Mumps.hpp>
#include <composyx/IO/ReadParam.hpp>
#include <composyx/IO/ArgumentParser.hpp>
#include <composyx/linalg/MatrixNorm.hpp>
#include <random>

using namespace composyx;

struct bench_param {

  ArgumentList arglist;

  bool help = false;
  std::string mat_filename;
  std::string rhs_filename;
  std::string pcd_filename;
  double tol;
  double zeta;
  int precision;
  std::string compressor;
  int max_iter;
  int restart;
  Ortho ortho = Ortho::CGS;
  bool trace_be;
  std::string scalar_type;
  std::string template_filename;

  bench_param() {
    arglist.add_argument_bool("help", "Print command help", 'h');
    arglist.add_argument_str("matrix", "(required) Path to matrix file", "",
                             'A');
    arglist.add_argument_str("rhs", "(required) Path to rhs matrix file", "",
                             'b');
    arglist.add_argument_str("precond",
                             "(required) Path to matrix file for precondioner "
                             "(to be factorized by direct solver)",
                             "", 'M');
    arglist.add_argument_str("ortho", "Orthogonalization scheme", "MGS", 'O');
    arglist.add_argument_real("tol", "Tolerance", 1e-6, 't');
    arglist.add_argument_real("zeta", "Compression parameter zeta", 1e-6, 'z');
    arglist.add_argument_int(
        "precision", "Compression parameter for ZFP precision", 26, 'p');
    arglist.add_argument_str(
        "compressor", "Compressor to use (NONE,SZ_NW,SZ_CW,ZFP_ACC,ZFP_PREC)",
        "SZ_NW", 'c');
    arglist.add_argument_int("max-iter", "Maximal number of iterations", 500,
                             'i');
    arglist.add_argument_int("restart", "Number of iterations between restarts",
                             250, 'r');
    arglist.add_argument_bool("trace-be", "Trace backward error", 'e');
    arglist.add_argument_str("scalar", "Scalar type (s or d)", "d", 's');
    arglist.add_argument_str("template-file",
                             "Generate template parameter file", "");
  }

  void display() {
    std::cout << std::boolalpha << "------\n"
              << " - Matrix file: " << mat_filename << '\n'
              << " - Rhs file: " << rhs_filename << '\n'
              << " - Precond file: " << pcd_filename << '\n'
              << " - Scalar type: " << scalar_type << '\n'
              << " - Tolerance: " << tol << '\n'
              << " - Zeta: " << zeta << '\n'
              << " - Precision: " << precision << '\n'
              << " - Compressor: " << compressor << '\n'
              << " - Max iterations: " << max_iter << '\n'
              << " - Restart: " << restart << '\n'
              << " - Trace backward error: " << trace_be << '\n'
              << " - Orthgonalization: ";
    if (ortho == Ortho::CGS) {
      std::cout << "CGS";
    } else if (ortho == Ortho::CGS2) {
      std::cout << "CGS2";
    } else if (ortho == Ortho::MGS) {
      std::cout << "MGS";
    } else if (ortho == Ortho::MGS2) {
      std::cout << "MGS2";
    }
    std::cout << '\n';

    std::cout << '\n';
  }

  void set_from_line_arguments(int argc, char** argv) {
    set_parameters(parse_arguments(argc, argv));
  }

  void set_from_parsed_file(const std::string& filename) {
    set_parameters(read_param_file(filename));
  }

  void set_parameters(const std::map<std::string, std::string>& keymap) {
    arglist.fill_value(keymap, "help", help);
    arglist.fill_value(keymap, "matrix", mat_filename);
    arglist.fill_value(keymap, "rhs", rhs_filename);
    arglist.fill_value(keymap, "precond", pcd_filename);
    arglist.fill_value(keymap, "tol", tol);
    arglist.fill_value(keymap, "zeta", zeta);
    arglist.fill_value(keymap, "precision", precision);
    if (precision <= 0) {
      std::cerr << "Error: precision must be a strictly positive integer, "
                   "setting default value";
      precision = 26;
    }
    arglist.fill_value(keymap, "compressor", compressor);

#if defined(COMPOSYX_USE_ZFP_COMPRESSOR)
    const std::array<std::string, 5> supported_vals{"NONE", "SZ_NW", "SZ_CW",
                                                    "ZFP_ACC", "ZFP_PREC"};
#else
    const std::array<std::string, 5> supported_vals{"NONE", "SZ_NW", "SZ_CW"};
#endif
    if (std::ranges::find(supported_vals, compressor) == supported_vals.end()) {
      std::cerr << "Error: Compressor not recognized: " << compressor << '\n';
      std::cerr << "Using SZ_NW\n";
      compressor = std::string("SZ_NW");
    }

    arglist.fill_value(keymap, "max-iter", max_iter);
    arglist.fill_value(keymap, "restart", restart);

    std::string ortho_str;
    arglist.fill_value(keymap, "ortho", ortho_str);
    if (ortho_str == std::string("CGS")) {
      ortho = Ortho::CGS;
    } else if (ortho_str == std::string("CGS2")) {
      ortho = Ortho::CGS2;
    } else if (ortho_str == std::string("MGS")) {
      ortho = Ortho::MGS;
    } else if (ortho_str == std::string("MGS2")) {
      ortho = Ortho::MGS2;
    } else {
      std::cerr << "Error: Orthogonalization not recognized: " << ortho_str
                << '\n';
      std::cerr << "Using MGS\n";
      ortho = Ortho::MGS;
    }

    arglist.fill_value(keymap, "trace-be", trace_be);
    arglist.fill_value(keymap, "scalar", scalar_type);
    arglist.fill_value(keymap, "template-file", template_filename);

    arglist.unknown_parameters(keymap);
  }
};

void usage(char** argv, const ArgumentList& arglist) {
  std::cerr << "Usage: " << std::string(argv[0]) << " input_file\n"
            << "   or: " << std::string(argv[0]) << " [parameters]\n"
            << arglist.usage() << '\n';
  // << "  Ex: " << std::string(argv[0]) << " -m 1024 -n 1024 -k 1024 -b 128 -l
  // 3 -H\n\n";
}

template <typename Matrix, typename Vector, typename Precond>
struct MatPcdOperator : LinearOperator<Matrix, Vector> {
  const Matrix* mat;
  const Precond* pcd;
  MatPcdOperator(const Matrix& m, const Precond& p) {
    mat = &m;
    pcd = &p;
  }
  void setup(const Matrix& m) { mat = &m; }
  Vector apply(const Vector& rhs) { return (*mat) * ((*pcd) * rhs); }
};
template <typename Matrix, typename Vector, typename Precond>
size_t n_rows(const MatPcdOperator<Matrix, Vector, Precond>& m) {
  return n_rows(*m.mat);
}

template <typename Basis> void test(bench_param& params) {

  using Scalar = typename Basis::scalar_type;
  using Real = typename arithmetic_real<Scalar>::type;
  using Precond = Mumps<SparseMatrixCSC<Scalar>, Vector<Scalar>>;

  auto get_mat = [](const std::string& fname) {
    SparseMatrixCOO<Scalar> mat;
    mat.from_matrix_market_file(fname);
    return mat;
  };

  const SparseMatrixCSC<Scalar> A(get_mat(params.mat_filename).to_csc());
  const SparseMatrixCSC<Scalar> M(get_mat(params.pcd_filename).to_csc());
  const Vector<Scalar> b(get_mat(params.rhs_filename).to_dense());

  std::cout << "Matrix dimensions: " << n_rows(A) << " x " << n_cols(A) << '\n';

  Real normAM;
  {
    Precond precond(M);
    MatPcdOperator<SparseMatrixCSC<Scalar>, Vector<Scalar>, Precond> AM_ope(
        A, precond);
    normAM =
        approximate_mat_norm<decltype(AM_ope), Vector<Scalar>, Scalar, Real>(
            AM_ope, 20);
  }
  // Evaluate A norm
  Real normA = approximate_mat_norm(A, 20);
  Real normAfr = A.frobenius_norm();
  Real normB = b.norm();

  std::cout << "A norm: " << normA << '\n';
  std::cout << "A(frobenius) norm: " << normAfr << '\n';
  std::cout << "A.M norm: " << normAM << '\n';
  std::cout << "b norm: " << normB << '\n';

  auto setup_inv_M = [&M](const SparseMatrixCSC<Scalar>&, Precond& dir_solver) {
    dir_solver.setup(M);
  };

  SZ_compressor_init();

  auto show_eta_AM_b = [&normAM, &normB](const SparseMatrixCSC<Scalar>& lA,
                                         const Vector<Scalar>& lx,
                                         const Vector<Scalar>& lb,
                                         const Vector<Scalar>&) {
    auto num = (lb - lA * lx).norm();
    auto denom = normAM * lx.norm() + normB;
    // ||b - AX|| / (||AM|| || X || + || b ||
    std::cout << "eta_AM_b = " << (num / denom) << '\n';
  };

  using Feedback_ite_type =
      std::function<void(const SparseMatrixCSC<Scalar>&, const Vector<Scalar>&,
                         const Vector<Scalar>&, const Vector<Scalar>&)>;

  GMRES<SparseMatrixCSC<Scalar>, Vector<Scalar>, Precond, Basis> gmres(A);
  gmres.setup(
      parameters::tolerance<Real>{static_cast<Real>(params.tol)},
      parameters::max_iter<int>{params.max_iter},
      parameters::restart<int>{params.restart},
      parameters::orthogonalization<Ortho>{params.ortho},
      parameters::verbose_mem<bool>{true},
      parameters::verbose_ortho_loss_norm2<bool>{true},
      parameters::setup_pcd<
          std::function<void(const SparseMatrixCSC<Scalar>&, Precond&)>>{
          setup_inv_M},
      parameters::verbose<bool>{true});

  if (params.trace_be) {
    gmres.setup(
        parameters::always_true_residual<bool>{params.trace_be},
        parameters::feedback_iteration<Feedback_ite_type>{show_eta_AM_b});
  }

  if (params.compressor == std::string("ZFP_PREC")) {
    std::cout << "Setting compression precision to: " << params.precision
              << " bits\n";
    gmres.setup(parameters::compression_precision<uint>{
        static_cast<uint>(params.precision)});
  } else {
    std::cout << "Setting compression accuracy to: " << params.zeta << '\n';
    gmres.setup(parameters::compression_accuracy<double>{params.zeta});
  }

  Vector<Scalar> X = gmres * b;

  gmres.display();

  std::vector<std::string> keys{
      "GMRES: compute_W",  "GMRES: orthogonalization", "GMRES: least_squares",
      "GMRES: update_sol", "GMRES iteration 0",        "GMRES iteration",
      "Iterative solve"};
  for (auto k : keys) {
    std::cout << k << ": " << Timer<0>::get_event_cumul_time(k) << '\n';
  }

  SZ_compressor_finalize();
}

int main(int argc, char** argv) {

  MMPI::init();
  bench_param params;

  // Load parameters from file
  bool from_file = false;
  std::string filename;

  auto check_file_exists = [](const std::string& fname) {
    std::ifstream f(fname.c_str());
    return f.good();
  };

  if (argc == 2) {
    filename = std::string(argv[1]);
    from_file = check_file_exists(filename);
  }

  if (from_file) {
    params.set_from_parsed_file(filename);
  } else { // Load parameters command line
    params.set_from_line_arguments(argc, argv);
  }

  if (params.template_filename.empty()) {
    if (!check_file_exists(params.mat_filename)) {
      std::cerr << "Error: cannot find matrix file";
      usage(argv, params.arglist);
      return 1;
    } else if (!check_file_exists(params.rhs_filename)) {
      std::cerr << "Error: cannot find rhs file";
      usage(argv, params.arglist);
      return 1;
    } else if (!check_file_exists(params.pcd_filename)) {
      std::cerr << "Error: cannot find precond file";
      usage(argv, params.arglist);
      return 1;
    }
  }

  if (params.help) {
    usage(argv, params.arglist);
  } else if (!params.template_filename.empty()) {
    const std::vector<std::string> hide_keys{"help", "template-file"};
    params.arglist.generate_template_file(params.template_filename, hide_keys);
  } else {

    using Basis_NoCompr_S = DenseMatrix<float>;
    using Basis_NoCompr_D = DenseMatrix<double>;
    using Basis_SZ_S_CW =
        CompressedBasis<SZ_compressor<float, SZ_CompressionMode::POINTWISE>>;
    using Basis_SZ_S_NW =
        CompressedBasis<SZ_compressor<float, SZ_CompressionMode::NORMWISE>>;
    using Basis_SZ_D_CW =
        CompressedBasis<SZ_compressor<double, SZ_CompressionMode::POINTWISE>>;
    using Basis_SZ_D_NW =
        CompressedBasis<SZ_compressor<double, SZ_CompressionMode::NORMWISE>>;
#if defined(COMPOSYX_USE_ZFP_COMPRESSOR)
    using Basis_ZFP_S_ACC =
        CompressedBasis<ZFP_compressor<float, ZFP_CompressionMode::ACCURACY>>;
    using Basis_ZFP_S_PREC =
        CompressedBasis<ZFP_compressor<float, ZFP_CompressionMode::PRECISION>>;
    using Basis_ZFP_D_ACC =
        CompressedBasis<ZFP_compressor<double, ZFP_CompressionMode::ACCURACY>>;
    using Basis_ZFP_D_PREC =
        CompressedBasis<ZFP_compressor<double, ZFP_CompressionMode::PRECISION>>;
#endif

    params.display();
    auto basis_selector = [&params](const std::string& scal,
                                    const std::string& compr) {
      return (params.scalar_type == std::string(scal) and
              params.compressor == std::string(compr));
    };

    if (basis_selector("s", "NONE")) {
      test<Basis_NoCompr_S>(params);
    } else if (basis_selector("d", "NONE")) {
      test<Basis_NoCompr_D>(params);
    } else if (basis_selector("s", "SZ_CW")) {
      test<Basis_SZ_S_CW>(params);
    } else if (basis_selector("s", "SZ_NW")) {
      test<Basis_SZ_S_NW>(params);
    } else if (basis_selector("d", "SZ_CW")) {
      test<Basis_SZ_D_CW>(params);
    } else if (basis_selector("d", "SZ_NW")) {
      test<Basis_SZ_D_NW>(params);
    }
#if defined(COMPOSYX_USE_ZFP_COMPRESSOR)
    else if (basis_selector("s", "ZFP_ACC")) {
      test<Basis_ZFP_S_ACC>(params);
    } else if (basis_selector("s", "ZFP_PREC")) {
      test<Basis_ZFP_S_PREC>(params);
    } else if (basis_selector("d", "ZFP_ACC")) {
      test<Basis_ZFP_D_ACC>(params);
    } else if (basis_selector("d", "ZFP_PREC")) {
      test<Basis_ZFP_D_PREC>(params);
    }
#endif
  }

  MMPI::finalize();
  return 0;
}
