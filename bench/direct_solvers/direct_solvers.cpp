#include <iostream>
#include <fstream>

#define TIMER_LEVEL_MAX 1001
#define TIMER_LEVEL_MIN 0

#if defined(COMPOSYX_USE_EIGEN)
#include <composyx/wrappers/Eigen/Eigen_header.hpp>
#include <composyx/wrappers/Eigen/Eigen.hpp>
#include <composyx/wrappers/Eigen/EigenSparseSolver.hpp>
#endif
#include <composyx.hpp>
#include <composyx/IO/ReadParam.hpp>
#include <composyx/dist/MPI.hpp>
#if defined(COMPOSYX_USE_QRMUMPS)
#include <composyx/solver/QrMumps.hpp>
#endif
#if defined(COMPOSYX_USE_MUMPS)
#include <composyx/solver/Mumps.hpp>
#endif
#if defined(COMPOSYX_USE_PASTIX)
#include <composyx/solver/Pastix.hpp>
#endif

#include <cstdlib>

using namespace composyx;
using Scalar = double;
using Real = double;

void usage(char** argv) {
  std::cerr << "Usage: " << std::string(argv[0])
            << " matrix_file [n_threads=1] [rhs_file]\n";
}

template <typename DirectSolver, typename Vect>
void bench(const Vect& b, DirectSolver& solver, const std::string& benchname) {
  std::cout << "##### Bench: " << benchname << '\n';
  Timer<1> t(benchname);
  Vect X = solver * b;
  t.stop();
  t.results(std::cout, false);
  t.reset();

  std::cout << "First 10 values\n";
  for (int k = 0; k < 10; ++k) {
    std::cout << X[k] << '\n';
  }
  std::cout << '\n';
}

int main(int argc, char** argv) {

  if (argc != 2 && argc != 3 && argc != 4) {
    usage(argv);
    return 1;
  }

  const std::vector<std::string> env_var_check{
      "MKL_NUM_THREADS", "OMP_NUM_THREADS", "GOTO_NUM_THREADS", "QRM_NCPU"};
  std::cout << "Environment variable checking:\n";
  for (const std::string& env_var : env_var_check) {
    const char* env_p = env_var.c_str();
    const char* env_val = std::getenv(env_p);
    if (env_val) {
      std::cout << "- " << env_var << " = " << env_val << '\n';
    } else {
      std::cout << "- " << env_var << " not set\n";
    }
  }

  MMPI::init();
  {

    const std::string mat_filename = std::string(argv[1]);
    auto get_A = [mat_filename]() {
      SparseMatrixCOO<Scalar> mat;
      mat.from_matrix_market_file(mat_filename);
      mat.set_spd(MatrixStorage::lower);
      return mat;
    };

    auto get_n_threads = [&argc, &argv]() {
      if (argc != 3) {
        return 1;
      } else {
        return std::stoi(std::string(argv[2]));
      }
    };
    const int n_threads = get_n_threads();
    std::cout << "Using number of threads: " << n_threads << '\n';

    SparseMatrixCOO<Scalar> A(get_A());

    Vector<Scalar> b;
    if (argc == 4) {
      std::cout << "Loading RHS file\n";
      const std::string rhs_filename = std::string(argv[3]);
      SparseMatrixCOO<Scalar> Bsp;
      Bsp.from_matrix_market_file(rhs_filename);
      b = Bsp.to_dense();
    } else {
      std::cout << "Generation RHS, with solution = (1, ..., 1)^T\n";
      Vector<Scalar> x_gen(n_rows(A));
      for (size_t k = 0; k < n_rows(A); ++k)
        x_gen[k] = Scalar{1};
      b = A * x_gen;
    }

#if defined(COMPOSYX_USE_MUMPS)
    {
      Mumps<SparseMatrixCOO<Scalar>, Vector<Scalar>> mumps;
      mumps.set_n_threads(n_threads);
      mumps.setup(A);
      bench<decltype(mumps), Vector<Scalar>>(b, mumps, "Mumps");
    }
#endif
#if defined(COMPOSYX_USE_PASTIX)
    {
      Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>> pastix;
      pastix.set_n_threads(n_threads);
      pastix.setup(A);
      bench<decltype(pastix), Vector<Scalar>>(b, pastix, "Pastix");
    }
#endif
#if defined(COMPOSYX_USE_QRMUMPS)
    {
      QrMumps<SparseMatrixCOO<Scalar>, Vector<Scalar>> qrmumps;
      qrmumps.set_n_cpu(n_threads);
      qrmumps.setup(A);
      bench<decltype(qrmumps), Vector<Scalar>>(b, qrmumps, "QrMumps - LLT");
    }

    {
      A.fill_half_to_full_storage();
      A.set_default_properties(); // No symmetry to use QR facto
      QrMumps<SparseMatrixCOO<Scalar>, Vector<Scalar>> qrmumps;
      qrmumps.set_n_cpu(n_threads);
      qrmumps.setup(A);
      bench<decltype(qrmumps), Vector<Scalar>>(b, qrmumps, "QrMumps - QR");
      A.set_spd(MatrixStorage::lower);
      A.to_triangular(MatrixStorage::lower);
    }
#endif
#if defined(COMPOSYX_USE_EIGEN)
    if (n_threads == 1) {
      Eigen::SparseMatrix<Scalar> A_eigen;
      build_matrix<Scalar>(A_eigen, n_rows(A), n_cols(A), n_nonzero(A),
                           A.get_i_ptr(), A.get_j_ptr(), A.get_v_ptr(),
                           /*fill_symmetry*/ false);
      A = SparseMatrixCOO<Scalar>(); // Free memory
      MatrixProperties<Scalar> prop;
      prop.set_spd(MatrixStorage::lower);

      using E_Vector = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;
      E_Vector b_e(n_rows(b));
      std::memcpy(get_ptr(b_e), get_ptr(b), sizeof(Scalar) * n_rows(b));
      b = Vector<Scalar>();

      EigenSparseSolver<Eigen::SparseMatrix<Scalar>, E_Vector> eigen_solver(
          A_eigen, prop);
      bench<decltype(eigen_solver), E_Vector>(b_e, eigen_solver, "Eigen");
    } else {
      std::cout << "Eigen test not supported for with number of threads >1\n";
    }
#endif
  }
  MMPI::finalize();

  return 0;
}
