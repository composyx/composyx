#+AUTHOR: Concace
#+EMAIL: gilles.marait@inria.fr
#+TITLE: Composyx weekly benchmark on plafrim

#+begin_src xml :tangle mt_bench.xml
<jube>
  <!-- set benchmark name and output folder -->
  <benchmark name="M++_direct_solvers" outpath="composyx_mt_results">
    <comment>Bench to test composyx direct solvers</comment>

    <!-- 888888888888888888888888888888888888888888888888888888888888888888888888888 -->

    <parameterset name="execute_param">
      <parameter name="exe_path">
        ${HOME}/composyx/bench/direct_solvers
      </parameter>
      <parameter name="exe">
        execcomposyx_mt.sh
      </parameter>
      <parameter name="guix_env_openblas">
        composyx --ad-hoc hwloc slurm eigen armadillo scalapack zlib qr_mumps bash --with-git-url=qr_mumps=${HOME}/qr_mumps --with-branch=qr_mumps=master
      </parameter>
      <parameter name="guix_env_mkl">
	composyx --ad-hoc hwloc slurm eigen armadillo scalapack zlib qr_mumps bash --with-git-url=qr_mumps=${HOME}/qr_mumps --with-branch=qr_mumps=master --with-input=openblas=mkl --with-input=mumps-openmpi=mumps-mkl-openmpi
      </parameter>
      <parameter name="path_bench_openblas">
	${HOME}/composyx/build_qrmumps/bench/direct_solvers
      </parameter>
      <parameter name="path_bench_mkl">
	${HOME}/composyx/build_mkl_qrmumps/bench/direct_solvers
      </parameter>
    </parameterset>

    <!-- load tag mode handling for the execute step -->
    <parameterset name="systemParameter" init_with="platform.xml">
      <!-- Miriel: 24 cores -->
      <!-- Bora: 36 cores -->
      <parameter name="threadspertask" type="int"> 1,16,36 </parameter>
      <parameter name="tasks" type="int"> 1 </parameter>
      <parameter name="corepernode" type="int"> 36 </parameter>
      <!-- <parameter name="nodes" type="int" mode="python"> int(${tasks} / ${corepernode} + 1 if ${tasks} % ${corepernode} else ${tasks} / ${corepernode}) </parameter> -->
      <parameter name="nodes" type="int" mode="python">1</parameter>
      <parameter name="taskspernode" type="int" mode="python"> min(${tasks},${corepernode}) </parameter>
      <parameter name="is_mt" type="int" mode="python"> 0 if $threadspertask == 1 else 1 </parameter>
      <parameter name="executable"> ./$exe </parameter>
      <parameter name="args_exec"></parameter>
      <parameter name="timelimit">01:00:00</parameter>

      <parameter name="preprocess" separator="@">
	echo "SLURM_CPUS_PER_TASK: ${SLURM_CPUS_PER_TASK}"
	sleep ${SLURM_CPUS_PER_TASK}
	cnt=0
	while [ -f "${HOME}/qr_mumps/token" ]
	do
	    echo "Waiting for token"
	    sleep $(($RANDOM % 10 + 5 ))
	    $cnt=$(($cnt+1))
	    if [ $cnt == 10 ]; then
	        echo "Waited 10 times, breaking loop"
	        break
	    fi
	done
	touch ${HOME}/qr_mumps/token
	<!-- For some reason the system doesn't like several jobs to access the qr_mumps repository at the same time. Hence this sleep-->
      </parameter>

      <parameter name="postprocess">
      </parameter>
      <parameter name="queue"></parameter>
    </parameterset>

    <parameterset name="executeset" init_with="platform.xml">
      <parameter name="qrm_mb" mode="python">1024 if 2 >= ${threadspertask} else 512</parameter>
      <parameter name="blas">openblas,mkl</parameter> <!-- openblas,wmkl -->
      <parameter name="guix_env" mode="python">{'mkl':'${guix_env_mkl}', 'openblas':'${guix_env_openblas}'}['${blas}']</parameter>
      <parameter name="bin_path" mode="python">{'mkl':'${path_bench_mkl}', 'openblas':'${path_bench_openblas}'}['${blas}']</parameter>
      <parameter name="submit_script">submit.job</parameter>
      <parameter name="starter">export BENCH_BIN_PATH=${bin_path}
      guix environment --pure --preserve=BENCH_BIN_PATH --preserve=^SLURM ${guix_env} -- </parameter>
    </parameterset>

    <fileset name="copy_exe">
      <copy> ${exe_path}/${exe} </copy>
    </fileset>

    <!-- job execution -->
    <step name="execute"> <!-- depend="compile"> -->
      <use>executeset</use> <!-- platform configuration -->
      <use from="platform.xml">executesub</use> <!-- substitution of job script template placeholders -->
      <use from="platform.xml">jobfiles</use> <!-- job script template -->
      <use>systemParameter</use> <!-- platform configuration -->
      <use>execute_param</use>
      <use>copy_exe</use>
      <do done_file="$done_file">$submit $submit_script</do>
    </step>

    <!-- 888888888888888888888888888888888888888888888888888888888888888888888888888 -->
    <!-- postprocessing -->
    <!-- Regex pattern -->
    <patternset name="pattern">
      <pattern name="MumpsFacto" type="float">Mumps facto, 1, $jube_pat_fp</pattern>
      <pattern name="MumpsSolve" type="float">Mumps solve, 1, $jube_pat_fp</pattern>
      <pattern name="MumpsTot" type="float">Mumps, 1, $jube_pat_fp</pattern>

      <pattern name="PastixFacto" type="float">Pastix facto, 1, $jube_pat_fp</pattern>
      <pattern name="PastixSolve" type="float">Pastix solve, 1, $jube_pat_fp</pattern>
      <pattern name="PastixTot" type="float">Pastix, 1, $jube_pat_fp</pattern>

      <pattern name="QrMumpsLLTSolve" type="float">QrMumps LLT-solve, 1, $jube_pat_fp</pattern>
      <pattern name="QrMumpsLLTFacto" type="float">QrMumps LLT-facto, 1, $jube_pat_fp</pattern>
      <pattern name="QrMumpsLLTTot" type="float">QrMumps - LLT, 1, $jube_pat_fp</pattern>

      <pattern name="QrMumpsQRSolve" type="float">QrMumps QR-solve, 1, $jube_pat_fp</pattern>
      <pattern name="QrMumpsQRFacto" type="float">QrMumps QR-facto, 1, $jube_pat_fp</pattern>
      <pattern name="QrMumpsQRTot" type="float">QrMumps - QR, 1, $jube_pat_fp</pattern>
    </patternset>

    <step name="postprocess" depend="execute">
      <do>
	ln -s ${exe_path}/pt.py
	ln -s ./execute/job.out
	python pt.py
      </do>
    </step>

    <!-- Analyse -->
    <analyser name="analyse">
      <use>pattern</use> <!-- use existing patternset -->
      <analyse step="postprocess">
        <file>job-pt.out</file> <!-- file which should be scanned -->
      </analyse>
    </analyser>

    <!-- Create result table -->
    <result>
      <use>analyse</use> <!-- use csvng analyser -->
      <table name="result" style="csv" sort="blas">
	<column>threadspertask</column>
	<column>blas</column>
	<column>qrm_mb</column>
	<column>MumpsFacto</column>
	<column>MumpsSolve</column>
	<column>MumpsTot</column>
	<column>PastixFacto</column>
	<column>PastixSolve</column>
	<column>PastixTot</column>
	<column>QrMumpsLLTFacto</column>
	<column>QrMumpsLLTSolve</column>
	<column>QrMumpsLLTTot</column>
	<column>QRMumpsQRFacto</column>
	<column>QrMumpsQRSolve</column>
	<column>QrMumpsQRTot</column>
      </table>
    </result>
    <!-- 888888888888888888888888888888888888888888888888888888888888888888888888888 -->

  </benchmark>
</jube>
#+end_src

#+begin_src xml :tangle platform.xml
<?xml version="1.0" encoding="UTF-8"?>
<jube>
    <!-- Default plafrim sets -->
    <parameterset name="executeset">
        <!-- Jobscript handling -->
        <parameter name="submit">sbatch</parameter>
        <parameter name="submit_script">submit.job</parameter>
        <parameter name="done_file">ready</parameter>
        <parameter name="starter">mpirun -np </parameter>
        <parameter name="args_starter"></parameter>
        <!-- Chainjob handling -->
        <parameter name="shared_folder">shared</parameter>
        <parameter name="shared_job_info">${shared_folder}/jobid</parameter>
        <parameter name="chainjob_script">./jureca-chainJobs.sh</parameter>
        <parameter name="chainjob_needs_submit">false</parameter>
    </parameterset>

    <parameterset name="systemParameter">
        <!-- Default jobscript parameter  -->
        <parameter name="nodes" type="int">1</parameter>
        <parameter name="taskspernode" type="int">1</parameter>
        <parameter name="threadspertask" type="int">1</parameter>
        <parameter name="tasks" mode="python" type="int">
            $nodes * $taskspernode
        </parameter>
        <!-- <parameter name="OMP_NUM_THREADS" type="int" export="true">
            $threadspertask
          </parameter> -->
        <parameter name="queue">batch</parameter>
        <parameter name="executable"></parameter>
        <parameter name="args_exec"></parameter>
        <parameter name="mail"></parameter>
        <parameter name="env" separator=";">$jube_wp_envstr</parameter>
        <parameter name="notification">ALL</parameter>
        <parameter name="outlogfile">job.out</parameter>
        <parameter name="errlogfile">job.err</parameter>
        <parameter name="timelimit">00:30:00</parameter>
        <parameter name="preprocess"></parameter>
        <parameter name="postprocess"></parameter>
        <parameter name="measurement"></parameter>
    </parameterset>

    <substituteset name="executesub">
        <!-- Default jobscript substitution -->
        <iofile in="${submit_script}.in" out="$submit_script" />
        <sub source="#ENV#" dest="$env" />
        <sub source="#NOTIFY_EMAIL#" dest="$mail" />
        <sub source="#NOTIFICATION_TYPE#" dest="$notification" />
        <sub source="#BENCHNAME#"
             dest="${jube_benchmark_name}_${jube_step_name}_${jube_wp_id}" />
        <sub source="#NODES#" dest="$nodes" />
        <sub source="#TASKS#" dest="$tasks" />
        <sub source="#NCPUS#" dest="$taskspernode" />
        <sub source="#NTHREADS#" dest="$threadspertask" />
        <sub source="#TIME_LIMIT#" dest="$timelimit" />
        <sub source="#PREPROCESS#" dest="$preprocess" />
        <sub source="#POSTPROCESS#" dest="$postprocess" />
        <sub source="#QUEUE#" dest="$queue" />
        <sub source="#STARTER#" dest="$starter" />
        <sub source="#ARGS_STARTER#" dest="$args_starter" />
        <sub source="#MEASUREMENT#" dest="$measurement" />
        <sub source="#STDOUTLOGFILE#" dest="$outlogfile" />
        <sub source="#STDERRLOGFILE#" dest="$errlogfile" />
        <sub source="#EXECUTABLE#" dest="$executable" />
        <sub source="#ARGS_EXECUTABLE#" dest="$args_exec" />
        <sub source="#FLAG#" dest="touch $done_file" />
    </substituteset>

    <substituteset name="chainsub">
        <!-- Default chainjob substitution -->
    </substituteset>

    <fileset name="jobfiles">
        <!-- Default jobscript access -->
        <copy>${submit_script}.in</copy>
    </fileset>

    <fileset name="chainfiles">
        <!-- Chainjob script access -->
        <copy>$chainjob_script</copy>
    </fileset>
</jube>
#+end_src

#+begin_src sh :tangle compile.sh
#!/bin/bash

cd ../.. # (composyx root)
rm -rf build_qrmumps
mkdir build_qrmumps
cd build_qrmumps
guix environment --pure composyx --ad-hoc hwloc slurm eigen armadillo scalapack zlib qr_mumps bash --with-git-url=qr_mumps=${HOME}/qr_mumps --with-branch=qr_mumps=master -- cmake .. -DCOMPOSYX_CXX_CONCEPTS=OFF -DCOMPOSYX_DEV_WARNINGS=ON -DCOMPOSYX_DEV_TANGLE=OFF -DCOMPOSYX_USE_QRMUMPS=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../install -DCOMPOSYX_COMPILE_BENCH=ON -DCOMPOSYX_USE_EIGEN=OFF
cd bench
guix environment --pure composyx --ad-hoc hwloc slurm eigen armadillo scalapack zlib qr_mumps bash --with-git-url=qr_mumps=${HOME}/qr_mumps --with-branch=qr_mumps=master -- make

cd ${HOME}/composyx
rm -rf build_mkl_qrmumps
mkdir build_mkl_qrmumps
cd build_mkl_qrmumps
guix environment --pure composyx --ad-hoc hwloc slurm eigen armadillo scalapack zlib qr_mumps bash --with-git-url=qr_mumps=${HOME}/qr_mumps --with-branch=qr_mumps=master --with-input=openblas=mkl --with-input=mumps-openmpi=mumps-mkl-openmpi -- cmake .. -DCOMPOSYX_CXX_CONCEPTS=OFF -DCOMPOSYX_DEV_WARNINGS=ON -DCOMPOSYX_DEV_TANGLE=OFF -DCOMPOSYX_USE_QRMUMPS=ON -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../install -DCOMPOSYX_COMPILE_BENCH=ON -DCOMPOSYX_USE_EIGEN=OFF
cd bench
guix environment --pure composyx --ad-hoc hwloc slurm eigen armadillo scalapack zlib qr_mumps bash --with-git-url=qr_mumps=${HOME}/qr_mumps --with-branch=qr_mumps=master --with-input=openblas=mkl --with-input=mumps-openmpi=mumps-mkl-openmpi -- make
#+end_src

#+begin_src sh :tangle submit.job.in
#!/bin/bash
#SBATCH --job-name=#BENCHNAME#
#SBATCH --nodes=#NODES#
#SBATCH --ntasks=#TASKS#
#SBATCH --ntasks-per-node=#NCPUS#
#SBATCH --cpus-per-task=#NTHREADS#
#SBATCH --time=#TIME_LIMIT#
#SBATCH --output=#STDOUTLOGFILE#
#SBATCH --error=#STDERRLOGFILE#
#SBATCH --exclusive
#SBATCH --constraint bora

#ENV#

#PREPROCESS#

#MEASUREMENT# #STARTER# #ARGS_STARTER# #EXECUTABLE# #ARGS_EXECUTABLE#

#POSTPROCESS#

JUBE_ERR_CODE=$?
if [ $JUBE_ERR_CODE -ne 0 ]; then
    exit $JUBE_ERR_CODE
fi

#FLAG#
#+end_src

#+begin_src sh :tangle execcomposyx_mt.sh
rm ${HOME}/qr_mumps/token

export MKL_NUM_THREADS=1
export OMP_NUM_THREADS=1
export GOTO_NUM_THREADS=1
export QRM_MB=512

echo "SLURM_CPUS_PER_TASK / SLURM_CPUS_ON_NODE : ${SLURM_CPUS_PER_TASK} / ${SLURM_CPUS_ON_NODE}"

mpirun --map-by ppr:1:node:pe=${SLURM_CPUS_ON_NODE} -np ${SLURM_NTASKS} ${BENCH_BIN_PATH}/direct_solvers /beegfs/gmarait/Flan_1565.mtx ${SLURM_CPUS_PER_TASK}
#+end_src
