#include <catch2/catch_session.hpp>
#include <composyx/dist/MPI.hpp>

int main(int argc, char* argv[]) {
  composyx::MMPI::init(MPI_THREAD_SERIALIZED);
  int result = Catch::Session().run(argc, argv);
  composyx::MMPI::finalize();
  return result;
}
