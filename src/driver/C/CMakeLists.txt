function(driver_c_gen_prec SCAL CPP_SCALAR_TYPE CPP_REAL_TYPE C_SCALAR_TYPE C_REAL_TYPE)
  configure_file(${CMAKE_CURRENT_SOURCE_DIR}/driver_x.h.in ${CMAKE_CURRENT_BINARY_DIR}/driver_composyx_${SCAL}.h)
  configure_file(${CMAKE_CURRENT_SOURCE_DIR}/driver_x.cpp.in ${CMAKE_CURRENT_BINARY_DIR}/driver_${SCAL}.cpp)
  configure_file(${CMAKE_CURRENT_SOURCE_DIR}/generated_interface_x.hpp ${CMAKE_CURRENT_BINARY_DIR}/generated_interface_${SCAL}.hpp)
  install(FILES ${CMAKE_CURRENT_BINARY_DIR}/driver_composyx_${SCAL}.h DESTINATION include)
endfunction()

driver_c_gen_prec("s" "float" "float" "float" "float")
driver_c_gen_prec("d" "double" "double" "double" "double")
driver_c_gen_prec("c" "std::complex<float>" "float" "float _Complex" "float")
driver_c_gen_prec("z" "std::complex<double>" "double" "double _Complex" "double")

set(driver_sources
   ${CMAKE_CURRENT_BINARY_DIR}/driver_s.cpp
   ${CMAKE_CURRENT_BINARY_DIR}/driver_d.cpp
   ${CMAKE_CURRENT_BINARY_DIR}/driver_c.cpp
   ${CMAKE_CURRENT_BINARY_DIR}/driver_z.cpp)

add_library(composyx_driver_c ${driver_sources})

# Hack to avoid multiple definition of some static variables
# We add a compile definition to all source files but one

set(sources_but_one_file ${driver_sources})
list(POP_FRONT sources_but_one_file)

set_source_files_properties(${sources_but_one_file}
                            PROPERTIES COMPILE_DEFINITIONS "COMPOSYX_STATIC_VARIABLES_ALREADY_DECLARED")

target_link_libraries(composyx_driver_c PUBLIC ${CMAKE_PROJECT_NAME})
target_compile_options(composyx_driver_c PRIVATE "$<$<CONFIG:Debug>:-g>" "$<$<CONFIG:Debug>:-Wall>")

if(COMPOSYX_USE_ARPACK)
  target_compile_definitions(composyx_driver_c PUBLIC COMPOSYX_USE_ARPACK)
endif()
if( COMPOSYX_USE_MUMPS )
  target_compile_definitions(composyx_driver_c PUBLIC COMPOSYX_USE_MUMPS)
endif()
if( COMPOSYX_USE_PASTIX )
  target_compile_definitions(composyx_driver_c PUBLIC COMPOSYX_USE_PASTIX)
endif()

# Install library
install(EXPORT composyx_driver_cTargets
  NAMESPACE COMPOSYX::
  DESTINATION lib/cmake/composyx
  )

install(TARGETS composyx_driver_c
  EXPORT composyx_driver_cTargets
  RUNTIME DESTINATION bin
  ARCHIVE DESTINATION lib
  LIBRARY DESTINATION lib)

set(tests
  test_driver
  test_driver_mpi
  test_complex
  test_driver_read_param
  test_tr_mat)

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/params.in
  DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

foreach(_test ${tests})
  add_executable( ${_test} ${_test}.c )
  target_compile_options(${_test} PUBLIC "$<$<CONFIG:Debug>:-Wall>")
  target_link_libraries(${_test} PUBLIC composyx_driver_c MPI::MPI_C)
  target_include_directories( ${_test} PUBLIC ${CMAKE_CURRENT_BINARY_DIR})
  set_target_properties(${_test} PROPERTIES LINKER_LANGUAGE "CXX")
endforeach()
