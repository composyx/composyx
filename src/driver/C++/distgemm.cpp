#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <random>
#include <sstream>

#define TIMER_RECORD_CUMULATIVE
#define TIMER_LEVEL_MAX 1001
#define TIMER_LEVEL_MIN 0
#include <composyx.hpp>
#include <composyx/IO/ReadParam.hpp>
#include <composyx/part_data/PartDenseMatrix.hpp>
#include <composyx/IO/ArgumentParser.hpp>

using namespace composyx;
using Scalar = double;
using Real = double;

enum class Datadist { BC_2D, row_1D, col_1D, random };
enum class TaskMapType { Astat, Bstat, Cstat, random };

struct bench_param {

  ArgumentList arglist;

  bool help = false;
  bool human_readable = false;
  bool column_out = false;

  bool check = false;
  bool check_centr = false;

  int niter = 1;
  int nthreads = 1;
  int ngpus = 0;

  int gemm_algo = 1;

  int m = 512;
  int n = 512;
  int k = 512;

  int seedA = -1;
  int seedB = -1;
  int seedC = -1;
  int seed_dist = -1;
  int seed_task = -1;

  double alpha = double{1.5};
  double beta = double{0.5};

  int inbmin = -1;
  int inbmax = -1;
  int jnbmin = -1;
  int jnbmax = -1;
  int knbmin = -1;
  int knbmax = -1;

  int P_grid = 1;
  int Q_grid = 1;
  Datadist data_dist_A = Datadist::BC_2D;
  Datadist data_dist_B = Datadist::BC_2D;
  Datadist data_dist_C = Datadist::BC_2D;

  TaskMapType taskmap_type = TaskMapType::Cstat;

  std::string report_datamap;
  std::string report_taskmap;
  std::string template_filename;

  bench_param() {
    arglist.command_description(
        "Perform a distributed matrix-matrix multiplication with block "
        "partitioned matrices");

    arglist.add_argument_bool("help", "Print command help", 'h');
    arglist.add_argument_bool("human", "Enable human readable mode", 'H');
    arglist.add_argument_bool("column-output",
                              "Output in column format 'key: value'", 'j');

    arglist.add_argument_bool("check",
                              "Enable checking of the result (parallel)", 'c');
    arglist.add_argument_bool(
        "check-centr",
        "Enable checking of the result (centralized, for small cases)");

    arglist.add_argument_int("niter", "Perform multiple iteration per test", 1,
                             'l');
    arglist.add_argument_int("threads", "Number of CPU workers per node", 1,
                             't');
    arglist.add_argument_int("gpus", "Number of GPU workers per node", 0, 'g');

    arglist.add_argument_int("algo", "Gemm algorithm", 1, 'a');
    arglist.add_possible_values("algo", std::vector<std::string>{"1", "2"});

    arglist.add_argument_int(
        "dim_square", "Sets all matrices square of dimension N (m=n=k=N)", -1,
        'N');
    arglist.add_argument_int("m", "Dimension M of the operation", 512, 'm');
    arglist.add_argument_int("n", "Dimension N of the operation", 512, 'n');
    arglist.add_argument_int("k", "Dimension K of the operation", 512, 'k');

    arglist.add_argument_real("alpha", "Value of the scalar alpha", double{1.5},
                              'x');
    arglist.add_argument_real("beta", "Value of the scalar beta", double{0.5},
                              'y');

    std::string nb_descr("Tile size, by default fixed and square. For finer "
                         "configuration of the blocksizes:\n");
    nb_descr += "    nbmin and nbmax : to obtain random sized tiles (same in "
                "all directions)\n";
    nb_descr += "To set the tile sizes direction by direction, set:\n";
    nb_descr += "    inb (or inbmin and inbmax): tile size in A-row and C-row "
                "direction\n";
    nb_descr += "    jnb (or jnbmin and jnbmax): tile size in B-column and "
                "C-column direction\n";
    nb_descr += "    knb (or knbmin and knbmax): tile size in A-column and "
                "B-row direction\n";

    arglist.add_argument_int("nb", nb_descr.c_str(), -1, 'b');
    arglist.add_argument_int("nbmin", "Random tile size minimal value", -1,
                             'r');
    arglist.add_argument_int("nbmax", "Random tile size maximal value", -1,
                             'R');

    arglist.add_argument_int(
        "inb", "Tile size in A-row and C-row direction (fixed)", -1);
    arglist.add_argument_int("inbmin", "Random tile size inb minimal value",
                             128);
    arglist.add_argument_int("inbmax", "Random tile size inb maximal value",
                             128);

    arglist.add_argument_int(
        "jnb", "Tile size in B-column and C-column direction (fixed)", -1);
    arglist.add_argument_int("jnbmin", "Random tile size jnb minimal value",
                             128);
    arglist.add_argument_int("jnbmax", "Random tile size jnb maximal value",
                             128);

    arglist.add_argument_int(
        "knb", "Tile size in A-column and B-row  direction (fixed)", -1);
    arglist.add_argument_int("knbmin", "Random tile size knb minimal value",
                             128);
    arglist.add_argument_int("knbmax", "Random tile size knb maximal value",
                             128);

    std::string taskmap_descr("Taskmap: where local matrix products are "
                              "performed. Possible values are:\n");
    taskmap_descr += "    A : A stationary\n";
    taskmap_descr += "    B : B stationary\n";
    taskmap_descr += "    C : C stationary (default)\n";
    taskmap_descr += "    R : random";
    arglist.add_argument_str("taskmap", taskmap_descr.c_str(), "C", 's');
    arglist.add_possible_values("taskmap",
                                std::vector<std::string>{"A", "B", "C", "R"});

    std::string distrib_descr(
        "Data distribution for matrix A. Possible values are:\n");
    distrib_descr += "    2DBC : 2D block cyclic (default)\n";
    distrib_descr += "    row  : 1D row distribution\n";
    distrib_descr += "    col  : 1D column distribution\n";
    distrib_descr += "    R    : random";
    arglist.add_argument_str("dataA", distrib_descr.c_str(), "2DBC", 'A');
    arglist.add_argument_str(
        "dataB", "Data distribution for matrix B (same as for A)", "2DBC", 'B');
    arglist.add_argument_str(
        "dataC", "Data distribution for matrix C (same as for A)", "2DBC", 'C');

    arglist.add_possible_values(
        "dataA", std::vector<std::string>{"2DBC", "row", "col", "R"});
    arglist.add_possible_values(
        "dataB", std::vector<std::string>{"2DBC", "row", "col", "R"});
    arglist.add_possible_values(
        "dataC", std::vector<std::string>{"2DBC", "row", "col", "R"});

    arglist.add_argument_int(
        "P", "Rows (P) in the PxQ process grid for 2DBC distribution", 1, 'P');

    arglist.add_argument_int("seedA", "Seed for the matrix A random generation",
                             -1, 'X');
    arglist.add_argument_int("seedB", "Seed for the matrix A random generation",
                             -1, 'Y');
    arglist.add_argument_int("seedC", "Seed for the matrix A random generation",
                             -1, 'Z');
    arglist.add_argument_int("seeddist", "Seed for random data distribution",
                             -1, 'S');
    arglist.add_argument_int("seedtask", "Seed for random taskmap", -1, 'D');

    arglist.add_argument_file(
        "report-datamap",
        "Generate a report with dimensions and datamap (filename)", "");
    arglist.add_argument_file(
        "report-taskmap", "Generate a report of the taskmap (filename)", "");
    arglist.add_argument_file("template-file",
                              "Generate template parameter file", "");
  }

  void display() {
    std::cout << std::boolalpha << "------\n"
              << " - Number of iterations: " << niter << '\n'
              << " - Number of threads: " << nthreads << '\n'
              << " - Number of GPUs: " << ngpus << '\n'
              << " - Gemm algorithm: " << gemm_algo << '\n'
              << '\n'
              << " - M: " << m << '\n'
              << " - N: " << n << '\n'
              << " - K: " << k << '\n'
              << " - alpha: " << alpha << '\n'
              << " - beta: " << beta << '\n'
              << '\n'
              << " - tile size ( i x j x k ): " << block_size_str() << '\n';

    auto datamap_desc = [&](const Datadist& d) {
      std::string out;
      switch (d) {
      case Datadist::BC_2D:
        out += std::string("2D block cyclic, (PxQ) with P = ");
        out += std::to_string(P_grid);
        out += std::string(" and Q = ");
        out += std::to_string(Q_grid);
        break;
      case Datadist::row_1D:
        out += std::string("1D row");
        break;
      case Datadist::col_1D:
        out += std::string("1D column");
        break;
      case Datadist::random:
        out += std::string("Random");
        break;
      }
      return out;
    };

    std::cout << '\n'
              << " - Data distriution for A: " << datamap_desc(data_dist_A)
              << '\n'
              << " - Data distriution for B: " << datamap_desc(data_dist_B)
              << '\n'
              << " - Data distriution for C: " << datamap_desc(data_dist_C)
              << '\n'
              << '\n'
              << " - Taskmap: ";
    switch (taskmap_type) {
    case TaskMapType::Astat:
      std::cout << "A stationnary\n";
      break;
    case TaskMapType::Bstat:
      std::cout << "B stationnary\n";
      break;
    case TaskMapType::Cstat:
      std::cout << "C stationnary\n";
      break;
    case TaskMapType::random:
      std::cout << "Random\n";
      break;
    }

    auto print_seed = [](int value) {
      std::string out;
      if (value < 0) {
        out += "random";
      } else {
        out += std::to_string(value);
      }
      return out;
    };

    std::cout << '\n'
              << " - seed for A: " << print_seed(seedA) << '\n'
              << " - seed for B: " << print_seed(seedB) << '\n'
              << " - seed for C: " << print_seed(seedC) << '\n'
              << " - seed for distribution map: " << print_seed(seed_dist)
              << '\n'
              << " - seed for task map: " << print_seed(seed_task) << '\n';

    std::cout << "\n------\n";
  }

  void set_parameters(const std::map<std::string, std::string>& keymap) {
    arglist.fill_value(keymap, "help", help);
    arglist.fill_value(keymap, "human", human_readable);
    arglist.fill_value(keymap, "column-output", column_out);
    arglist.fill_value(keymap, "check", check);
    arglist.fill_value(keymap, "check-centr", check_centr);
    arglist.fill_value(keymap, "niter", niter);
    arglist.fill_value(keymap, "threads", nthreads);
    arglist.fill_value(keymap, "gpus", ngpus);
    arglist.fill_value(keymap, "algo", gemm_algo);
    arglist.fill_value(keymap, "dim_square", m);
    if (m != -1) {
      n = m;
      k = m;
    } else {
      arglist.fill_value(keymap, "m", m);
      arglist.fill_value(keymap, "n", n);
      arglist.fill_value(keymap, "k", k);
    }

    arglist.fill_value(keymap, "P", P_grid);
    const int rank = MMPI::rank(MPI_COMM_WORLD);
    const int nprocs = MMPI::size(MPI_COMM_WORLD);
    COMPOSYX_ASSERT(P_grid > 0, "Parameter P must be positive");
    COMPOSYX_ASSERT(P_grid <= nprocs, "Parameter P <= nprocs");
    Q_grid = std::max(nprocs / P_grid, 1);
    if (Q_grid * P_grid != nprocs and rank == 0)
      COMPOSYX_WARNING("2D block cyclic with PxQ != nprocs");

    arglist.fill_value(keymap, "seedA", seedA);
    arglist.fill_value(keymap, "seedB", seedB);
    arglist.fill_value(keymap, "seedC", seedC);
    arglist.fill_value(keymap, "seeddist", seed_dist);
    arglist.fill_value(keymap, "seedtask", seed_task);
    arglist.fill_value(keymap, "alpha", alpha);
    arglist.fill_value(keymap, "beta", beta);

    arglist.fill_value(keymap, "inbmin", inbmin);
    arglist.fill_value(keymap, "inbmax", inbmax);
    arglist.fill_value(keymap, "jnbmin", jnbmin);
    arglist.fill_value(keymap, "jnbmax", jnbmax);
    arglist.fill_value(keymap, "knbmin", knbmin);
    arglist.fill_value(keymap, "knbmax", knbmax);

    int tmp;
    arglist.fill_value(keymap, "inb", tmp);
    if (tmp != -1) {
      inbmin = tmp;
      inbmax = tmp;
    }
    arglist.fill_value(keymap, "jnb", tmp);
    if (tmp != -1) {
      jnbmin = tmp;
      jnbmax = tmp;
    }
    arglist.fill_value(keymap, "knb", tmp);
    if (tmp != -1) {
      knbmin = tmp;
      knbmax = tmp;
    }

    arglist.fill_value(keymap, "nbmin", tmp);
    if (tmp != -1) {
      inbmin = tmp;
      jnbmin = tmp;
      knbmin = tmp;
    }

    arglist.fill_value(keymap, "nbmax", tmp);
    if (tmp != -1) {
      inbmax = tmp;
      jnbmax = tmp;
      knbmax = tmp;
    }

    arglist.fill_value(keymap, "nb", tmp);
    if (tmp != -1) {
      inbmin = tmp;
      inbmax = tmp;
      jnbmin = tmp;
      jnbmax = tmp;
      knbmin = tmp;
      knbmax = tmp;
    }

    if (inbmin > inbmax) {
      std::cerr << "Error: REQUIRES inbmin <= inbmax\n";
      inbmin = 128;
      inbmax = 128;
      return;
    }
    if (jnbmin > jnbmax) {
      std::cerr << "Error: REQUIRES jnbmin <= jnbmax\n";
      jnbmin = 128;
      jnbmax = 128;
      return;
    }
    if (knbmin > knbmax) {
      std::cerr << "Error: REQUIRES knbmin <= knbmax\n";
      knbmin = 128;
      knbmax = 128;
      return;
    }

    std::string data_A_str, data_B_str, data_C_str;
    arglist.fill_value(keymap, "dataA", data_A_str);
    arglist.fill_value(keymap, "dataB", data_B_str);
    arglist.fill_value(keymap, "dataC", data_C_str);

    auto str_to_datamap = [](const std::string& data_str) {
      if (data_str == std::string("2DBC"))
        return Datadist::BC_2D;
      if (data_str == std::string("row"))
        return Datadist::row_1D;
      if (data_str == std::string("col"))
        return Datadist::col_1D;
      if (data_str == std::string("R"))
        return Datadist::random;
      return Datadist::BC_2D;
    };

    data_dist_A = str_to_datamap(data_A_str);
    data_dist_B = str_to_datamap(data_B_str);
    data_dist_C = str_to_datamap(data_C_str);

    std::string taskmap_str;
    arglist.fill_value(keymap, "taskmap", taskmap_str);

    auto str_to_taskmap = [](const std::string& tm_str) {
      if (tm_str == std::string("A"))
        return TaskMapType::Astat;
      if (tm_str == std::string("B"))
        return TaskMapType::Bstat;
      if (tm_str == std::string("C"))
        return TaskMapType::Cstat;
      if (tm_str == std::string("R"))
        return TaskMapType::random;
      return TaskMapType::Cstat;
    };

    taskmap_type = str_to_taskmap(taskmap_str);

    arglist.fill_value(keymap, "report-datamap", report_datamap);
    arglist.fill_value(keymap, "report-taskmap", report_taskmap);
    arglist.fill_value(keymap, "template-file", template_filename);

    arglist.unknown_parameters(keymap);
  }

  void set_from_line_arguments(int argc, char** argv) {
    set_parameters(parse_arguments(argc, argv));
  }

  void set_from_parsed_file(const std::string& filename) {
    set_parameters(read_param_file(filename));
  }

  std::string block_size_str() const {
    std::stringstream strs;

    if (inbmin == inbmax and inbmax == jnbmin and jnbmin == jnbmax and
        jnbmax == knbmin and knbmin == knbmax) {
      strs << inbmin;
    } else {
      if (inbmin == inbmax) {
        strs << inbmin;
      } else {
        strs << '(' << inbmin << '-' << inbmax << ')';
      }
      strs << " x ";
      if (jnbmin == jnbmax) {
        strs << jnbmin;
      } else {
        strs << '(' << jnbmin << '-' << jnbmax << ')';
      }
      strs << " x ";
      if (knbmin == knbmax) {
        strs << knbmin;
      } else {
        strs << '(' << knbmin << '-' << knbmax << ')';
      }
    }

    return strs.str();
  }
};

void usage(char** argv, const ArgumentList& arglist) {
  if (MMPI::rank() == 0) {
    std::cerr << "Usage: " << std::string(argv[0]) << " input_file\n"
              << "   or: " << std::string(argv[0]) << " [parameters]\n"
              << arglist.usage() << '\n'
              << "  Ex: " << std::string(argv[0])
              << " -m 1024 -n 1024 -k 1024 -b 128 -l 3 -H\n\n";
  }
}

void pick_random_seed(bench_param& params, MPI_Comm comm) {
  std::random_device rd;
  const int seed = rd();
  std::mt19937 gen(seed);
  std::uniform_int_distribution<int> dis(1, 1e8);
  int rank = MMPI::rank(comm);

  auto pick_seed = [&gen, &dis, rank, comm](int& s) {
    if (s < 0) {
      if (rank == 0) {
        s = dis(gen);
      }
      MMPI::bcast(&s, 1, 0, comm);
    }
  };

  pick_seed(params.seedA);
  pick_seed(params.seedB);
  pick_seed(params.seedC);
  pick_seed(params.seed_dist);
  pick_seed(params.seed_task);
}

template <typename DataFct>
void distgemm(const bench_param& params, MPI_Comm comm) {

  using LocMat = DenseMatrix<Scalar>;

  using PartMat = PartDenseMatrix<LocMat, DataFct>;

  const int nprocs = MMPI::size(comm);
  const int rank = MMPI::rank(comm);

  const int max_nblocks = 1000000;

  auto get_datamap_fct = [&](const Datadist& d, const int seed) {
    std::shared_ptr<DataFct> fct_ptr;
    if constexpr (!std::is_same<DataFct,
                                canonical_datamap::TwoD_block_cyclic>::value) {
      switch (d) {
      case Datadist::BC_2D:
        fct_ptr = std::make_shared<DataFct>(
            canonical_datamap::TwoD_block_cyclic(params.P_grid, params.Q_grid));
        break;
      case Datadist::row_1D:
        fct_ptr =
            std::make_shared<DataFct>(canonical_datamap::OneD_row(nprocs));
        break;
      case Datadist::col_1D:
        fct_ptr =
            std::make_shared<DataFct>(canonical_datamap::OneD_col(nprocs));
        break;
      case Datadist::random:
        fct_ptr = std::make_shared<DataFct>(
            canonical_datamap::pseudorandom(nprocs, seed));
        break;
      }
    } else {
      fct_ptr = std::make_shared<DataFct>(
          canonical_datamap::TwoD_block_cyclic(params.P_grid, params.Q_grid));
    }
    return Datamap<DataFct>(fct_ptr, max_nblocks, max_nblocks);
  };

  auto get_taskmap_fct = [&](const TaskMapType& t, const int seed) {
    std::function<int(int, int, int)> taskmap;
    switch (t) {
    case TaskMapType::Astat:
      taskmap = canonical_taskmap::A_stationary(
          get_datamap_fct(params.data_dist_A, params.seedA));
      break;
    case TaskMapType::Bstat:
      taskmap = canonical_taskmap::B_stationary(
          get_datamap_fct(params.data_dist_B, params.seedB));
      break;
    case TaskMapType::Cstat:
      taskmap = canonical_taskmap::C_stationary(
          get_datamap_fct(params.data_dist_C, params.seedC));
      break;
    case TaskMapType::random:
      taskmap = canonical_taskmap::pseudorandom(nprocs, seed);
      break;
    }
    return taskmap;
  };

  auto make_cuts = [](const int dim, int xnbmin, int xnbmax, int seed) {
    std::vector<int> v;
    if (xnbmin == xnbmax) {
      for (int i = 0; i < dim; i += xnbmin) {
        v.emplace_back(i);
      }
    } else {
      std::mt19937 gen(seed);
      std::uniform_int_distribution<int> dis(xnbmin, xnbmax);
      int i = 0;
      while (i < dim) {
        v.emplace_back(i);
        i += dis(gen);
      }
    }
    v.emplace_back(dim);
    return v;
  };

  std::vector<int> cuts_i =
      make_cuts(params.m, params.inbmin, params.inbmax, params.seedA);
  std::vector<int> cuts_j =
      make_cuts(params.n, params.jnbmin, params.jnbmax, params.seedB);
  std::vector<int> cuts_k =
      make_cuts(params.k, params.knbmin, params.knbmax, params.seedC);

  auto make_part_matrix = [&](const Datadist& d, const int seed,
                              const std::vector<int>& cuts_row,
                              const std::vector<int>& cuts_col) {
    std::map<std::pair<int, int>, LocMat> mat_map;
    auto datamap = get_datamap_fct(d, seed);

    const size_t nblocks_row = cuts_row.size() - 1;
    const size_t nblocks_col = cuts_col.size() - 1;

    std::mt19937 gen_real(seed);
    std::uniform_real_distribution<double> dis_real(double{-1}, double{1});

    for (size_t ib = 0; ib < nblocks_row; ++ib) {
      for (size_t jb = 0; jb < nblocks_col; ++jb) {
        if (datamap(ib, jb) == rank) {
          int m = cuts_row[ib + 1] - cuts_row[ib];
          int n = cuts_col[jb + 1] - cuts_col[jb];
          LocMat lm(m, n);
          for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
              lm(i, j) = dis_real(gen_real);
            }
          }
          mat_map.emplace(std::make_pair(ib, jb), std::move(lm));
        }
      }
    }
    return PartMat(comm, std::move(mat_map), datamap);
  };

  Timer<1> tgA("generate_A");
  auto A = make_part_matrix(params.data_dist_A, params.seedA, cuts_i, cuts_k);
  tgA.stop();

  Timer<1> tgB("generate_B");
  auto B = make_part_matrix(params.data_dist_B, params.seedB, cuts_k, cuts_j);
  tgB.stop();

  Timer<1> tgC("generate_C");
  auto C = make_part_matrix(params.data_dist_C, params.seedC, cuts_i, cuts_j);
  tgC.stop();

  // Report datamaps
  if (!params.report_datamap.empty() and rank == 0) {
    std::cout << "Writing datamap report in " << params.report_datamap
              << " ...\n";
    std::ofstream f{params.report_datamap, std::ios::out};
    if (!f.is_open())
      COMPOSYX_ASSERT(f, params.report_datamap + ": file not opened");

    f << "A (" << params.m << ',' << params.k << ") with blocks (";
    f << A.get_n_rows_block() << ',' << A.get_n_cols_block() << ")\n";
    f << "A_row_blocks_sizes: " << A.blocksizes_row_str() << '\n';
    f << "A_col_blocks_sizes: " << A.blocksizes_col_str() << '\n';
    f << "Datamap:\n" << A.datamap_str(',') << '\n';
    f << "B (" << params.k << ',' << params.n << ") with blocks (";
    f << B.get_n_rows_block() << ',' << B.get_n_cols_block() << ")\n";
    f << "B_row_blocks_sizes: " << B.blocksizes_row_str() << '\n';
    f << "B_col_blocks_sizes: " << B.blocksizes_col_str() << '\n';
    f << "Datamap:\n" << B.datamap_str(',') << '\n';
    f << "C (" << params.m << ',' << params.n << ") with blocks (";
    f << C.get_n_rows_block() << ',' << C.get_n_cols_block() << ")\n";
    f << "C_row_blocks_sizes: " << C.blocksizes_row_str() << '\n';
    f << "C_col_blocks_sizes: " << C.blocksizes_col_str() << '\n';
    f << "Datamap:\n" << C.datamap_str(',');
  }

  cuts_i.clear();
  cuts_j.clear();
  cuts_k.clear();

  // A.display_centralized(0, "A");
  // B.display_centralized(0, "B");
  // C.display_centralized(0, "C");

  auto taskmap = get_taskmap_fct(params.taskmap_type, params.seed_task);

  // Report taskamp
  if (!params.report_taskmap.empty() and rank == 0) {
    std::cout << "Writing taskmap report in " << params.report_taskmap
              << " ...\n";
    std::ofstream f{params.report_taskmap, std::ios::out};
    if (!f.is_open())
      COMPOSYX_ASSERT(f, params.report_taskmap + ": file not opened");
    f << "# Showing taskmap (i, j, k) as the matrix (i, j) for consecutive "
         "values of k\n";
    for (size_t k = 0; k < A.get_n_cols_block(); ++k) {
      f << "k = " << k << "\n";
      for (size_t j = 0; j < B.get_n_cols_block(); ++j) {
        for (size_t i = 0; i < A.get_n_rows_block(); ++i) {
          f << taskmap(i, j, k) << ',';
        }
        f << '\n';
      }
      f << '\n';
    }
  }

  if (rank == 0) {
    double tA = Timer<1>::get_event_cumul_time("generate_A");
    double tB = Timer<1>::get_event_cumul_time("generate_B");
    double tC = Timer<1>::get_event_cumul_time("generate_C");
    std::cout << "Time to generate A (s)   : tA=" << tA << '\n';
    std::cout << "Time to generate B (s)   : tB=" << tB << '\n';
    std::cout << "Time to generate C (s)   : tC=" << tC << '\n';
    std::cout << "Total generation time (s): tGen=" << tA + tB + tC << '\n';
    std::cout << '\n';
    std::cout << "Performing gemm...\n\n";
  }

  std::cout << std::flush;
  MMPI::barrier(comm);

  std::vector<double> check_results;
  std::vector<double> check_centr_results;

  for (int iter = 0; iter < params.niter; ++iter) {
    // Fill C with random values
    {
      std::mt19937 gen_real(params.seedC);
      std::uniform_real_distribution<double> dis_real(double{-1000},
                                                      double{1000});
      for (size_t ib = 0; ib < C.get_n_rows_block(); ++ib) {
        for (size_t jb = 0; jb < C.get_n_cols_block(); ++jb) {
          if (C.get_datamap()(ib, jb) == rank) {
            auto& lm = C.get_local_matrix(ib, jb);
            for (size_t i = 0; i < n_rows(lm); ++i) {
              for (size_t j = 0; j < n_cols(lm); ++j) {
                lm(i, j) = dis_real(gen_real);
              }
            }
          }
        }
      }
    }

    std::unique_ptr<PartMat> x0_check;
    std::unique_ptr<PartMat> x1_check;

    if (params.check) {
      // For checking, pick a random vector x0
      std::mt19937 gen_real(params.seedC);
      std::uniform_real_distribution<double> dis_real(double{-1}, double{1});

      x0_check =
          std::make_unique<PartMat>(compute_matvect_distribution<DataFct>(C));
      for (size_t ib = 0; ib < x0_check->get_n_rows_block(); ++ib) {
        if (x0_check->get_datamap()(ib, 0) == rank) {
          auto& lm = x0_check->get_local_matrix(ib, 0);
          for (size_t i = 0; i < n_rows(lm); ++i) {
            lm(i, 0) = dis_real(gen_real);
          }
        }
      }

      // Compute x1 <- alpha * A * (B * x0) + beta * C * x0
      x1_check = std::make_unique<PartMat>(
          params.alpha * A * (B * (*x0_check)) + params.beta * C * (*x0_check));
    }

    std::unique_ptr<LocMat> C_centr_check;
    if (params.check_centr) {
      auto A_centr = A.centralize(0);
      auto B_centr = B.centralize(0);
      auto C_centr = C.centralize(0);
      if (rank == 0) {
        C_centr_check = std::make_unique<LocMat>(
            params.alpha * A_centr * B_centr + params.beta * C_centr);
      }
    }

    MMPI::barrier(comm);
    Timer<1> tgemm("gemm");

    if (params.gemm_algo == 1) {
      dense_dist_gemm(A, B, C, taskmap, params.alpha, params.beta);
    } else if (params.gemm_algo == 2) {
      serialize_gemm(A, B, C, taskmap, params.alpha, params.beta);
    } else if (params.gemm_algo == 3) {
#if defined(COMPOSYX_USE_CHAMELEON) && defined(CHAMELEON_USE_MPI)
      if constexpr (std::is_same<DataFct,
                                 canonical_datamap::TwoD_block_cyclic>::value) {
        chameleon_gemm(A, B, C, taskmap, params.alpha, params.beta);
      }
#endif
    }

    MMPI::barrier(comm);
    tgemm.stop();

    if (rank == 0) {
      double time = Timer<1>::get_event_cumul_time("gemm");
      auto n_flop = flops_gemm(A, B);
      double gflops = static_cast<double>(n_flop) / 1e9 / time;

      std::vector<std::string> output_keys{
          "Id", "threads", "gpus", "nb", "m", "n", "k", "time", "gflops"};
      std::vector<int> output_sizes{4, 7, 4, 4, 6, 6, 6, 12, 12};
      std::vector<std::string> output_values;
      output_values.push_back(std::to_string(iter));
      output_values.push_back(std::to_string(params.nthreads));
      output_values.push_back(std::to_string(params.ngpus));
      output_values.push_back(params.block_size_str());
      output_values.push_back(std::to_string(params.m));
      output_values.push_back(std::to_string(params.n));
      output_values.push_back(std::to_string(params.k));

      std::stringstream strs;
      strs << std::setprecision(6) << std::scientific << time;
      output_values.push_back(strs.str());
      strs = std::stringstream();
      strs << std::setprecision(6) << std::scientific << gflops;
      output_values.push_back(strs.str());

      if (params.column_out) {
        std::cout << '\n';
        for (size_t i = 0; i < output_keys.size(); ++i) {
          std::cout << output_keys.at(i) << ": " << output_values.at(i) << '\n';
        }
        std::cout << '\n';
      }

      auto format_print = [&](const std::string& val, const int size,
                              const bool last) {
        if (params.human_readable) {
          std::cout << std::setw(size) << val << '\t';
        } else {
          std::cout << val;
          if (!last)
            std::cout << ';';
        }
      };

      if (iter == 0) {
        for (size_t i = 0; i < output_keys.size(); ++i) {
          format_print(output_keys.at(i), output_sizes.at(i),
                       (i == output_keys.size() - 1));
        }
        std::cout << '\n';
      }

      for (size_t i = 0; i < output_values.size(); ++i) {
        format_print(output_values.at(i), output_sizes.at(i),
                     (i == output_keys.size() - 1));
      }

      std::cout << '\n';
    }

    if (params.check) {
      const PartMat& x1 = (*x1_check);
      PartMat x2 = C * (*x0_check);
      Real diff = (x1 - x2).norm() / x1.norm();

      if (rank == 0) {
        check_results.push_back(diff);
      }
    }

    if (params.check_centr) {
      auto C_final = C.centralize(0);

      if (rank == 0) {
        const auto& C_check = (*C_centr_check);
        Real diff = (C_final - C_check).norm() / C_check.norm();
        check_centr_results.push_back(diff);
      }
    }

    Timer<0>::reset();
  } // iter

  if (rank == 0) {
    if (params.check) {
      std::cout << "\nCheck (parallel) result relative error\n";
      std::cout << "Id;rel_err_mv\n";
      int iter = 0;
      for (const auto& res : check_results)
        std::cout << iter++ << ';' << std::setprecision(6) << std::scientific
                  << res << '\n';
      if (params.column_out) {
        for (const auto& res : check_results)
          std::cout << "rel_err_mv: " << std::setprecision(6) << std::scientific
                    << res << '\n';
      }
    }
    if (params.check_centr) {
      std::cout << "\nCheck (centralized) result relative error:\n";
      std::cout << "Id;rel_err_centr\n";
      int iter = 0;
      for (const auto& res : check_centr_results)
        std::cout << iter++ << ';' << std::setprecision(6) << std::scientific
                  << res << '\n';
      if (params.column_out) {
        for (const auto& res : check_centr_results)
          std::cout << "rel_err_centr: " << std::setprecision(6)
                    << std::scientific << res << '\n';
      }
    }
  }
}

int main(int argc, char** argv) {

  MMPI::init(MPI_THREAD_MULTIPLE);
  {
    Timer<0> tt("Total time");
    MPI_Comm comm = MPI_COMM_WORLD;
    const int rank = MMPI::rank(comm);

    bench_param params;

    if (ArgumentList::generate_bash_completion(argc, argv, params.arglist,
                                               "composyx_distgemm"))
      return 0;

    // Load parameters from file
    bool from_file = false;
    std::string filename;
    if (argc == 2) {
      filename = std::string(argv[1]);
      auto check_file_exists = [](const std::string& fname) {
        std::ifstream f(fname.c_str());
        return f.good();
      };
      from_file = check_file_exists(filename);
    }

    if (from_file) {
      params.set_from_parsed_file(filename);
    } else { // Load parameters command line
      params.set_from_line_arguments(argc, argv);
    }

    if (params.help) {
      if (rank == 0)
        usage(argv, params.arglist);
    } else if (!params.template_filename.empty()) {
      const std::vector<std::string> hide_keys{"help", "template-file"};
      if (rank == 0)
        params.arglist.generate_template_file(params.template_filename,
                                              hide_keys);
    } else {
      if (rank == 0)
        params.display();
      pick_random_seed(params, comm);
      initialize(params.nthreads, params.ngpus);
      if (params.gemm_algo == 3) { // Chameleon
#if defined(COMPOSYX_USE_CHAMELEON) && defined(CHAMELEON_USE_MPI)
        setChameleonParameters(params.inbmax);
        distgemm<canonical_datamap::TwoD_block_cyclic>(params, comm);
#else
        COMPOSYX_ASSERT(false,
                        "gemm_algo=3 i.e. call chameleon_gemm but either "
                        "COMPOSYX_USE_CHAMELEON or CHAMELEON_USE_MPI not set");
#endif
      } else {
        distgemm<std::function<int(int, int)>>(params, comm);
      }
      finalize();
    }
  }
  MMPI::finalize();

  return 0;
}
