#include <composyx.hpp>
#include <composyx/IO/ReadParam.hpp>
#include <composyx/loc_data/SparseMatrixCOO.hpp>
#include <composyx/solver/GMRES.hpp>
#include <composyx/graph/Paddle.hpp>
#include <composyx/solver/Pastix.hpp>
#include <composyx/solver/PartSchurSolver.hpp>
#include <composyx/IO/ArgumentParser.hpp>

using namespace composyx;

struct driver_params {
  std::string matrix = "";
  bool help;
  std::string template_filename = "";

  ArgumentList arglist;

  driver_params() {
    arglist.command_description(
        "Partition a matrix and calls GMRES to solve a linear system with the "
        "matrix. Input matrix must be in matrix market format.");

    arglist.add_argument_bool("help", "Print command help", 'h');
    arglist.add_argument_file(
        "matrix", "Path to the matrix file (matrix market format), mandatory",
        std::string(), 'A');
    arglist.add_argument_file("template-file",
                              "Generate template parameter file", "");
  }

  void set_parameters(const std::map<std::string, std::string>& keymap) {
    arglist.fill_value(keymap, "help", help);
    arglist.fill_value(keymap, "matrix", matrix);
    arglist.fill_value(keymap, "template-file", template_filename);
    arglist.unknown_parameters(keymap);
  }

  void set_from_line_arguments(int argc, char** argv) {
    set_parameters(parse_arguments(argc, argv));
  }

  void set_from_parsed_file(const std::string& filename) {
    set_parameters(read_param_file(filename));
  }
};

template <typename Scalar>
void solve_with_paddle(const std::string& matrix_filename) {

  int rank = MMPI::rank();

  SparseMatrixCOO<Scalar> a_in;
  Vector<Scalar> b_in;

  if (rank == 0) {
    a_in.from_matrix_market_file(matrix_filename);

    b_in = Vector<Scalar>(n_rows(a_in));
    Scalar rhs_val;
    if constexpr (is_complex<Scalar>::value) {
      rhs_val = Scalar{1.0, 0.5};
    } else {
      rhs_val = Scalar{1.0};
    }

    for (size_t k = 0; k < n_rows(b_in); ++k) {
      b_in[k] = rhs_val;
    }
    b_in *= (Scalar{1} / b_in.norm());
  }

  Paddle<Scalar> paddle;
  paddle.set_verbose();

  auto [p_A, p_rhs] = paddle.centralized_input(a_in, b_in);

  using Solver_K = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
  using Solver_S =
      GMRES<PartMatrix<DenseMatrix<Scalar>>, PartVector<Vector<Scalar>>>;
  using Solver =
      PartSchurSolver<decltype(p_A), decltype(p_rhs), Solver_K, Solver_S>;

  Solver solver;
  solver.setup(parameters::verbose{(rank == 0)}, parameters::A{p_A});

  Solver_S& iter_solver = solver.get_solver_S();

  iter_solver.setup(parameters::max_iter{1000}, parameters::restart{10},
                    parameters::tolerance{1e-5},
                    parameters::always_true_residual{false},
                    parameters::orthogonalization{Ortho::CGS},
                    parameters::verbose{(rank == 0)});

  auto p_x = solver * p_rhs;

  auto ctr_x = paddle.solution_redistribution(p_x);
}

void usage(char** argv, const ArgumentList& arglist) {
  if (MMPI::rank() == 0) {
    std::cerr << "Usage: " << std::string(argv[0]) << " input_file\n"
              << "   or: " << std::string(argv[0]) << " [parameters]\n"
              << arglist.usage() << '\n'
              << "  Ex: mpirun -np 2 " << std::string(argv[0])
              << " -A my_matrix.mtx\n\n";
  }
}

int main(int argc, char** argv) {

  MMPI::init();
  auto rank = MMPI::rank();

  driver_params params;
  if (ArgumentList::generate_bash_completion(argc, argv, params.arglist,
                                             "composyx_driver_paddle"))
    return 0;

  // Load parameters from file
  if (argc == 2) {
    std::string filename = std::string(argv[1]);
    auto check_file_exists = [](const std::string& fname) {
      std::ifstream f(fname.c_str());
      return f.good();
    };
    if (check_file_exists(filename)) {
      params.set_from_parsed_file(filename);
    }
  }
  // Load parameters command line
  else {
    params.set_from_line_arguments(argc, argv);
  }

  // Print help only
  if (params.help) {
    if (rank == 0)
      usage(argv, params.arglist);
    MMPI::finalize();
    return 0;
  } else if (!params.template_filename.empty()) {
    const std::vector<std::string> hide_keys{"help", "template-file"};
    if (rank == 0)
      params.arglist.generate_template_file(params.template_filename,
                                            hide_keys);
    MMPI::finalize();
    return 0;
  }

  // Check required keys
  {
    bool error = false;
    if (params.matrix.empty()) {
      if (rank == 0)
        std::cerr << "Required key not found: partitions\n";
      error = true;
    }
    if (error) {
      usage(argv, params.arglist);
      MMPI::finalize();
      return 1;
    }
  }

  const std::string matrix_filename(params.matrix);

  if (matrix_market::is_real_matrix(matrix_filename)) {
    if (rank == 0)
      std::cout << "Found matrix with real coefficients\n";
    solve_with_paddle<double>(matrix_filename);
  } else {
    if (rank == 0)
      std::cout << "Found matrix with complex coefficients\n";
    solve_with_paddle<std::complex<double>>(matrix_filename);
  }

  MMPI::finalize();
}
