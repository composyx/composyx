#include <iostream>
#include <vector>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <Python.h>
#include <composyx.hpp>
#include <composyx/solver/ConjugateGradient.hpp>
#include <composyx/precond/AbstractSchwarz.hpp>
#include <composyx/part_data/PartMatrix.hpp>
#include <composyx/solver/Pastix.hpp>
#include <composyx/solver/ConjugateGradient.hpp>
#include <composyx/solver/PartSchurSolver.hpp>

using namespace std;
namespace py = pybind11;
using namespace composyx;

py::array_t<double> multiply_dense (py::array_t <double> a, py::array_t <double> b){
  auto a_arr = a.request(); //return a buffer_info to the python object
  auto b_arr = b.request();

  unsigned int shape_a_row = a_arr.shape[0];
  unsigned int shape_a_col = a_arr.shape[1];

  double* a_arr_ptr = (double*)a_arr.ptr; //cast the pointers of the python object to a double*
  double* b_arr_ptr = (double*)b_arr.ptr;

  double* x_arr_ptr = new double[shape_a_row]; //initialize a new double* of size shape_a_row

  const DenseMatrix<double> A(DenseData<double>(shape_a_row,shape_a_col,a_arr_ptr),true);
  const Vector<double> B(DenseData<double,1>(shape_a_row,b_arr_ptr),true);
  Vector<double> X(DenseData<double,1>(shape_a_row,x_arr_ptr),true);
  X = A * B;
  return py::array_t<double>({shape_a_row} ,x_arr_ptr); // construct the py::array_t to be returned (size,data)
}

py::array_t<double> solve_dense(py::array_t <double> a, py::array_t <double> b){
  auto a_arr = a.request();
  auto b_arr = b.request();

  unsigned int m = a_arr.shape[0];

  double* a_ptr = (double*)a_arr.ptr;
  double* b_ptr = (double*)b_arr.ptr;
  double* x_ptr = new double[m];
  const DenseMatrix<double> A(DenseData<double>(m, m, a_ptr), true);
  const Vector<double> B(DenseData<double, 1>(m, b_ptr), true);
  Vector<double> X(DenseData<double, 1>(m, x_ptr), true);
  ConjugateGradient<DenseMatrix<double>, Vector<double>> solver;
  solver.setup(parameters::A{A},parameters::verbose{true},parameters::max_iter{100},parameters::tolerance{1e-8});
  X = solver * B;
  return py::array_t<double>({m},x_ptr);
}

SparseMatrixCOO<double> bind_coo(py::object a){
  //data
    py::object src_data = a.attr("data"); // data array
    vector <double> data_ = src_data.cast<vector<double>>(); // cast to an std::vector<double>
    double* data = data_.data();
    //col
    py::object src_col = a.attr("col"); //col array
    vector <int> col_ = src_col.cast<vector<int>>();
    int* col = col_.data();
    //row
    py::object src_row = a.attr("row"); //row array
    vector <int> row_ = src_row.cast<vector<int>>();
    int* row = row_.data();
    //shape
    py::object src_shape = a.attr("shape"); //shape tuple
    vector <int> shape = src_shape.cast<vector<int>>();
    int shape_a_row = shape[0];
    int shape_a_col = shape[1];
    //NNZ
    py::object src_nnz = a.attr("nnz");
    int nnz = src_nnz.cast<int>();
    const SparseMatrixCOO<double> A(shape_a_row,shape_a_col,nnz,row,col,data);
    return A;
}

int get_coo_nrows(py::object a){
  py::object src_shape = a.attr("shape"); //shape tuple
  vector <int> shape = src_shape.cast<vector<int>>();
  return shape[0];
}

py::array_t<double> multiply_coo(py::object a, py::array_t<double> b){
  const SparseMatrixCOO<double> A = bind_coo(a);
  auto b_arr = b.request();
  double * b_arr_ptr = (double*)b_arr.ptr;
  unsigned int shape_b_row = b_arr.shape[0];

  const Vector<double> B(DenseData<double,1>(shape_b_row,b_arr_ptr),true);

  double* x_arr_ptr = new double[shape_b_row];

  Vector<double> X(DenseData<double,1>(shape_b_row,x_arr_ptr),true);

  X = A * B;

  return py::array_t<double>({shape_b_row} ,x_arr_ptr);
}

SparseMatrixCSC<double> bind_csc(py::object a){
  //data
  py::object src_data = a.attr("data"); // data array
  vector <double> data_ = src_data.cast<vector<double>>(); // cast to an std::vector<double>
  double* data = data_.data();
  //col
  py::object src_indices = a.attr("indices"); //col array
  vector <int> indices_ = src_indices.cast<vector<int>>();
  int* indices = indices_.data();
  //row
  py::object src_indptr = a.attr("indptr"); //row array
  vector <int> indptr_ = src_indptr.cast<vector<int>>();
  int* indptr = indptr_.data();
  //shape
  py::object src_shape = a.attr("shape"); //shape tuple
  vector <int> shape = src_shape.cast<vector<int>>();
  int shape_a_row = shape[0];
  int shape_a_col = shape[1];
  //NNZ
  py::object src_nnz = a.attr("nnz");
  int nnz = src_nnz.cast<int>();

  const SparseMatrixCSC<double> A(shape_a_row,shape_a_col,nnz,indices,indptr,data);
  return A;
}

py::array_t <double> multiply_csc(py::object a, py::array_t<double> b){
  const SparseMatrixCSC<double> A = bind_csc(a);
  auto b_arr = b.request();
  double * b_arr_ptr = (double*)b_arr.ptr;
  unsigned int shape_b_row = b_arr.shape[0];

  //A.set_spd(MatrixStorage::lower);
  const Vector<double> B(DenseData<double,1>(shape_b_row,b_arr_ptr),true);

  double* x_arr_ptr = new double[shape_b_row];

  Vector<double> X(DenseData<double,1>(shape_b_row,x_arr_ptr),true);

  X = A * B;

  return py::array_t<double>({shape_b_row} ,x_arr_ptr);
}

py::array_t<double> solve_coo(py::object a, py::array_t<double> b){
  py::object scipy = py::module_::import("scipy");
  auto b_arr = b.request();
  double * b_arr_ptr = (double*)b_arr.ptr;
  unsigned int shape_b_row = b_arr.shape[0];

  const SparseMatrixCOO<double> A = bind_coo(a);
  const Vector<double> B(DenseData<double,1>(shape_b_row,b_arr_ptr),true);

  double* x_arr_ptr = new double[shape_b_row];

  Vector<double> X(DenseData<double,1>(shape_b_row,x_arr_ptr),true);
  //A.set_spd(MatrixStorage::lower);
  ConjugateGradient<SparseMatrixCOO<double>,Vector<double>> solver;
  solver.setup(parameters::A{A},
               parameters::verbose{true},
               parameters::max_iter{100},
               parameters::tolerance{1e-8});
  X = solver * B;
  return py::array_t<double>({shape_b_row} ,x_arr_ptr);
}

py::array_t<double> solve_csc (py::object a, py::array_t<double> b){
  py::object scipy = py::module_::import("scipy");
  auto b_arr = b.request();
  double * b_arr_ptr = (double*)b_arr.ptr;
  unsigned int shape_b_row = b_arr.shape[0];

  const SparseMatrixCSC<double> A = bind_csc(a);
  const Vector<double> B(DenseData<double,1>(shape_b_row,b_arr_ptr),true);

  double* x_arr_ptr = new double[shape_b_row];

  Vector<double> X(DenseData<double,1>(shape_b_row,x_arr_ptr),true);
  //A.set_spd(MatrixStorage::lower);
  ConjugateGradient<SparseMatrixCSC<double>,Vector<double>> solver;
  solver.setup(parameters::A{A},
               parameters::verbose{true},
               parameters::max_iter{100},
               parameters::tolerance{1e-8});
  X = solver * B;
  return py::array_t<double>({shape_b_row} ,x_arr_ptr);
}

map<int, std::vector<int>> convert_dict_to_map(py::dict dictionary)
{
    map<int, std::vector<int>> result;
    for (std::pair<py::handle, py::handle> item : dictionary)
    {
        auto key = item.first.cast<int>();
        auto value = item.second.cast<vector<int>>();
        std::vector<int> value_ = std::vector(value);
        //cout << key << " : " << value;
        result[key] = value_;
    }
    return result;
}

void pybind_subdomain_bindings(py::object a, std::shared_ptr<Process> p){
  py::object src_a = a.attr("dd");
  py::dict neighbors = src_a.attr("neighbors");

  py::object n_dd = src_a.attr("n");

  int n_glob = n_dd.cast<int>(); //dd.n
  py::object N_sub_dd = src_a.attr("n_subdomains");
  py::object n_dofs_dd = src_a.attr("ni");
  const int N_SUBDOMAINS = N_sub_dd.cast<int>();
  const int N_DOFS = n_dofs_dd.cast<int>();

  map<int,std::vector<int>> res = convert_dict_to_map(neighbors);
  std::vector<Subdomain> sd;
  for (int i = 0; i < N_SUBDOMAINS; ++i) if(p->owns_subdomain(i)) sd.emplace_back(i, N_DOFS, std::move(res));
  p->load_subdomains(sd);
}

int get_n_subdomains(py::object a){
  py::object src_a = a.attr("dd");
  py::object N_sub_dd = src_a.attr("n_subdomains");
  int N_SUBDOMAINS = N_sub_dd.cast<int>();
  return N_SUBDOMAINS;
}

int get_n_glob(py::object a){
  py::object src_a = a.attr("dd");
  py::object n_dd = src_a.attr("n");
  int n_glob = n_dd.cast<int>();
  return n_glob;
}

py::object multiply_dist(py::object a, py::object b){
  //const int rank = MMPI::rank();
  py::object src_a = a.attr("dd");
  const int N_SUBDOMAINS = get_n_subdomains(a);
  std::shared_ptr<Process> p = bind_subdomains(N_SUBDOMAINS);
  pybind_subdomain_bindings(a,p);
  //local matrix
  py::array a_ = a.attr("local");
  auto a_arr = a_.request();
  double * a_ptr = (double*)a_arr.ptr; //ptr to the binded matrix
  const int M = a_arr.shape[0];

  const DenseMatrix<double> A_loc(DenseData<double>(M,M,a_ptr),true); //Densematrix from local matr

  // Create the partitioned matrix
  std::map<int, DenseMatrix<double>> A_map;
  for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) A_map[k] = A_loc;
  const PartMatrix<DenseMatrix<double>> A(p, std::move(A_map));
  //local vector
  py::object src_b = b.attr("local");
  vector <double> data_b = src_b.cast<vector<double>>();
  double* b_arr_ptr = data_b.data(); //ptr to the binded vector

  Vector<double> b_loc(DenseData<double,1>(M,b_arr_ptr),true); //vector from b_arr_ptr

  // Create the partitioned vector
  std::map<int, Vector<double>> b_map;
  for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) b_map[k] = b_loc;
  const PartVector<Vector<double>> X(p, b_map);

  PartVector<Vector<double>> b_vect(p, b_map);
  b_vect = A.matvect(X);

  Vector<double> b_tmp;

  for (int i = 0; i < N_SUBDOMAINS; ++i) if(p->owns_subdomain(i)) b_tmp = b_vect.get_local_vector(i);

  //Construct the distVect to return
  double * b_arr_ret = b_tmp.get_ptr();
  py::object ddmpy =  py::module_::import("ddmpy");
  py::array_t<double> b_ret = py::array_t<double>({M},b_arr_ret);
  py::object ret = ddmpy.attr("DistVector")(b_ret,src_a);
  return ret;
}



py::object multiply_dist_coo(py::object a, py::object b){
  //const int rank = MMPI::rank();

  py::object src_a = a.attr("dd");
  const int N_SUBDOMAINS = get_n_subdomains(a);
  std::shared_ptr<Process> p = bind_subdomains(N_SUBDOMAINS);
  pybind_subdomain_bindings(a,p);

  //local matrix
  py::object a_ = a.attr("local");

  SparseMatrixCOO<double, int> A_loc = bind_coo(a_); //Sparse matrix from the binded data
  int M = get_coo_nrows(a_);
  //A_loc.set_spd(MatrixStorage::upper);

  // Create the partitioned matrix
  std::map<int, SparseMatrixCOO<double, int>> A_map;
  for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) A_map[k] = A_loc;
  const PartMatrix<SparseMatrixCOO<double>> A(p, std::move(A_map));


  py::object src_b = b.attr("local");
  vector <double> data_b = src_b.cast<vector<double>>();
  double* b_arr_ptr = data_b.data();

  Vector<double> b_loc(DenseData<double,1>(M,b_arr_ptr),true);

  // Create the partitioned vector
  std::map<int, Vector<double>> b_map;
  for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) b_map[k] = b_loc;
  const PartVector<Vector<double>> X(p, b_map);

  PartVector<Vector<double>> b_vect(p, std::move(b_map));
  b_vect = A.matvect(X);

  //A.gemv(X,b_vect);

  Vector<double> b_tmp;

  if(p->owns_subdomain(0)) b_tmp = b_vect.get_local_vector(0);
  if(p->owns_subdomain(1)) b_tmp = b_vect.get_local_vector(1);
  if(p->owns_subdomain(2)) b_tmp = b_vect.get_local_vector(2);

  double * b_arr_ret = b_tmp.get_ptr();
  py::object ddmpy =  py::module_::import("ddmpy");
  py::array_t<double> b_ret = py::array_t<double>({M},b_arr_ret);
  py::object ret = ddmpy.attr("DistVector")(b_ret,src_a);
  return ret;
}

py::object schur_coo(py::object a, py::object b){
  //const int rank = MMPI::rank();

   py::object src_a = a.attr("dd");
  const int N_SUBDOMAINS = get_n_subdomains(a);
  std::shared_ptr<Process> p = bind_subdomains(N_SUBDOMAINS);
  pybind_subdomain_bindings(a,p);

  py::object rank_dd = src_a.attr("rank");
  const int rank = rank_dd.cast<int>(); //dd.rank
  //local matrix
  py::object a_ = a.attr("local");


  SparseMatrixCOO<double, int> A_loc = bind_coo(a_); //Sparse matrix from the binded data
  int M = get_coo_nrows(a_);
  //A_loc.set_spd(MatrixStorage::upper);

  // Create the partitioned matrix
  std::map<int, SparseMatrixCOO<double, int>> A_map;
  for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) A_map[k] = A_loc;
  const PartMatrix<SparseMatrixCOO<double>> A(p, std::move(A_map));

  //local vector
  py::object src_b = b.attr("local");
  vector <double> data_b = src_b.cast<vector<double>>();
  double* b_arr_ptr = data_b.data(); //ptr to the binded vector

  Vector<double> b_loc(DenseData<double,1>(M,b_arr_ptr),true); //vector from b_arr_ptr

  // Create the partitioned vector
  std::map<int, Vector<double>> b_map;
  for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) b_map[k] = b_loc;
  const PartVector<Vector<double>> b_vect(p, b_map);

  using Solver_K = Pastix<SparseMatrixCOO<double>, Vector<double>>;
  using Solver_S = ConjugateGradient<PartMatrix<DenseMatrix<double>>, PartVector<Vector<double>>>;
  using Solver = PartSchurSolver<PartMatrix<SparseMatrixCOO<double>>, PartVector<Vector<double>>, Solver_K, Solver_S>;

  // Setup
  Solver schur_solver;
  schur_solver.setup(A);

  // Set parameters for the solver S (ConjugateGradient)
  Solver_S& iter_solver = schur_solver.get_solver_S();
  iter_solver.setup(parameters::max_iter{50},
                    parameters::tolerance{1e-8},
                    parameters::verbose{(rank == 0)});

  // Solve

  PartVector<Vector<double>> X(p, b_map);

  X = schur_solver * b_vect;

  Vector<double> b_tmp;

  for (int i = 0; i < N_SUBDOMAINS; ++i) if(p->owns_subdomain(i)) b_tmp = X.get_local_vector(i);

  //Construct the distVect to return
  double * b_arr_ret = b_tmp.get_ptr();
  py::object ddmpy =  py::module_::import("ddmpy");
  py::array_t<double> b_ret = py::array_t<double>({M},b_arr_ret);
  py::object ret = ddmpy.attr("DistVector")(b_ret,src_a);
  return ret;
}

py::object cg_dense(py::object a, py::object b){
  //const int rank = MMPI::rank();

  py::object src_a = a.attr("dd");
  const int N_SUBDOMAINS = get_n_subdomains(a);
  std::shared_ptr<Process> p = bind_subdomains(N_SUBDOMAINS);
  pybind_subdomain_bindings(a,p);

  py::object rank_dd = src_a.attr("rank");
  const int rank = rank_dd.cast<int>(); //dd.rank
  //local matrix
  py::array a_ = a.attr("local");
  auto a_arr = a_.request();
  double * a_ptr = (double*)a_arr.ptr; //ptr to the binded matrix
  int M = a_arr.shape[0];

  const DenseMatrix<double> A_loc(DenseData<double>(M,M,a_ptr),true);

  // Create the partitioned matrix
  std::map<int, DenseMatrix<double>> A_map;
  for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) A_map[k] = A_loc;
  const PartMatrix<DenseMatrix<double>> A(p, std::move(A_map));


  //local vector
  py::object src_b = b.attr("local");
  vector <double> data_b = src_b.cast<vector<double>>();
  double* b_arr_ptr = data_b.data(); //ptr to the binded vector

  Vector<double> b_loc(DenseData<double,1>(M,b_arr_ptr),true); //vector from b_arr_ptr

  // Create the partitioned vector
  std::map<int, Vector<double>> b_map;
  for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) b_map[k] = b_loc;
  const PartVector<Vector<double>> b_vect(p, b_map);

  using Solver_S = ConjugateGradient<PartMatrix<DenseMatrix<double>>, PartVector<Vector<double>>>;

  // Setup
  Solver_S cg_solver;
  cg_solver.setup(parameters::A{A},
                  parameters::max_iter{50},
                  parameters::tolerance{1e-8},
                  parameters::verbose{(rank == 0)});

  // Solve

  PartVector<Vector<double>> X(p, b_map);

  X = cg_solver * b_vect;

  Vector<double> b_tmp;

  for (int i = 0; i < N_SUBDOMAINS; ++i) if(p->owns_subdomain(i)) b_tmp = X.get_local_vector(i);

  //Construct the distVect to return
  double * b_arr_ret = b_tmp.get_ptr();
  py::object ddmpy =  py::module_::import("ddmpy");
  py::array_t<double> b_ret = py::array_t<double>({M},b_arr_ret);
  py::object ret = ddmpy.attr("DistVector")(b_ret,src_a);
  return ret;
}

py::object pycentralize_dense(py::object a){
       //const int rank = MMPI::rank();

    py::object src_a = a.attr("dd");
    const int N_SUBDOMAINS = get_n_subdomains(a);
    std::shared_ptr<Process> p = bind_subdomains(N_SUBDOMAINS);
    pybind_subdomain_bindings(a,p);
    int n_glob = get_n_glob(a);
    //local matrix
    py::array a_ = a.attr("local");
    auto a_arr = a_.request();
    double * a_ptr = (double*)a_arr.ptr; //ptr to the binded matrix
    int M = a_arr.shape[0];
    const DenseMatrix<double> A_loc(DenseData<double>(M,M,a_ptr),true);

    // Create the partitioned matrix
    std::map<int, DenseMatrix<double>> A_map;
    for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) A_map[k] = A_loc;
    const PartMatrix<DenseMatrix<double>> A(p, std::move(A_map));

    SparseMatrixCOO<double,int> A_centr = A.centralize(); //centralize the partmatrix


    DenseMatrix<double> A_dense = A_centr.to_dense(); //transform it to a dense matrix
    double * a_ret = A_dense.get_ptr();
    py::array_t<double> ret = py::array_t<double>({n_glob,n_glob},a_ret); //construct the  py::array_t to return
    return ret;
   }

py::object pycentralize_coo(py::object a){
    //const int rank = MMPI::rank();

    py::object src_a = a.attr("dd");
    const int N_SUBDOMAINS = get_n_subdomains(a);
    std::shared_ptr<Process> p = bind_subdomains(N_SUBDOMAINS);
    pybind_subdomain_bindings(a,p);
    int n_glob = get_n_glob(a);

    py::object a_ = a.attr("local");
    SparseMatrixCOO<double, int> A_loc = bind_coo(a_);//Sparse matrix from the binded data
    //A_loc.set_spd(MatrixStorage::upper);

    // Create the partitioned matrix
    std::map<int, SparseMatrixCOO<double, int>> A_map;
    for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) A_map[k] = A_loc;
    const PartMatrix<SparseMatrixCOO<double>> A(p, std::move(A_map));
    SparseMatrixCOO<double,int> A_centr = A.centralize(); //centralize the partmatrix


    DenseMatrix<double> A_dense = A_centr.to_dense(); //transform it to a dense matrix
    double * a_ret = A_dense.get_ptr();
    py::object scipy =  py::module_::import("scipy.sparse");
    py::array_t<double> A_ret = py::array_t<double>({n_glob,n_glob},a_ret); //construct a py::array from the DenseMatrix
    py::object ret = scipy.attr("coo_matrix")(A_ret); //construct the  py::array_t to return
    return ret;
}

py::object pycentralize_v(py::object a){
    //const int rank = MMPI::rank();

   py::object src_a = a.attr("dd");
    const int N_SUBDOMAINS = get_n_subdomains(a);
    std::shared_ptr<Process> p = bind_subdomains(N_SUBDOMAINS);
    pybind_subdomain_bindings(a,p);
    int n_glob = get_n_glob(a);

     //local matrix
    py::array a_ = a.attr("local");
    auto a_arr = a_.request();
    double * a_ptr = (double*)a_arr.ptr;
    int M = a_arr.shape[0];

    const Vector<double> b_loc(DenseData<double,1>(M,a_ptr),true);

    // Create the partitioned matrix
    std::map<int, Vector<double>> b_map;
    for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) b_map[k] = b_loc;
    const PartVector<Vector<double>> b(p, std::move(b_map));

    Vector<double> b_centr = b.centralize();

    double * b_arr_ret = b_centr.get_ptr();
    py::array_t<double> ret = py::array_t<double>({n_glob},b_arr_ret);
    return ret;
}

using SpMat = PartMatrix<SparseMatrixCOO<double>>;
using DMat = PartMatrix<DenseMatrix<double>>;
using Vect = PartVector<Vector<double>>;
using Precond = AdditiveSchwarz<DMat, Vect, Pastix<SparseMatrixCOO<double>, Vector<double>>>;
using Solver_K = Pastix<SparseMatrixCOO<double>, Vector<double>>;
using Solver_S = ConjugateGradient<DMat, Vect, Precond>;
using Solver_cg = ConjugateGradient<DMat, Vect>;
using Solver = PartSchurSolver<DMat, Vect, Solver_K, Solver_S>;

template <typename Solver1>
py::object solve_gen(py::object a, py::object b, py::object tol, py::object iter){
  //const int rank = MMPI::rank();

  py::object src_a = a.attr("dd");
  py::dict neighbors = src_a.attr("neighbors");

  py::object n_dd = src_a.attr("n");
  py::object rank_dd = src_a.attr("rank");

  int n_glob = n_dd.cast<int>(); //dd.n
  const int rank = rank_dd.cast<int>(); //dd.rank
  py::object N_sub_dd = src_a.attr("n_subdomains");
  py::object n_dofs_dd = src_a.attr("ni");
  const int N_SUBDOMAINS = N_sub_dd.cast<int>();
  const int N_DOFS = n_dofs_dd.cast<int>();
  std::shared_ptr<Process> p = bind_subdomains(N_SUBDOMAINS);
  map<int,std::vector<int>> res = convert_dict_to_map(neighbors);
  std::vector<Subdomain> sd;
  for (int i = 0; i < N_SUBDOMAINS; ++i) if(p->owns_subdomain(i)) sd.emplace_back(i, N_DOFS, std::move(res));
  p->load_subdomains(sd);

  //local matrix
  py::array a_ = a.attr("local");
  auto a_arr = a_.request();
  const int M = a_arr.shape[0];
  double * a_ptr = (double*)a_arr.ptr; //ptr to the binded matrix

  const DenseMatrix<double> A_loc(DenseData<double>(M,M,a_ptr),true);

  // Create the partitioned matrix
  std::map<int, DenseMatrix<double>> A_map;
  for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) A_map[k] = A_loc;
  const PartMatrix<DenseMatrix<double>> A(p, std::move(A_map));


  //local vector
  py::object src_b = b.attr("local");
  vector <double> data_b = src_b.cast<vector<double>>();
  double* b_arr_ptr = data_b.data(); //ptr to the binded vector

  Vector<double> b_loc(DenseData<double,1>(M,b_arr_ptr),true); //vector from b_arr_ptr

  // Create the partitioned vector
  std::map<int, Vector<double>> b_map;
  for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) b_map[k] = b_loc;
  const PartVector<Vector<double>> b_vect(p, b_map);

  double tol_ = tol.cast<double>();
  int maxiter = iter.cast<int>();

  PartVector<Vector<double>> X(p, b_map);

  Solver1 cg_as;
  cg_as.setup(parameters::A{A},
                  parameters::tolerance{tol_},
                  parameters::max_iter{maxiter});

  X = cg_as * b_vect;

  Vector<double> b_tmp;

  for (int i = 0; i < N_SUBDOMAINS; ++i) if(p->owns_subdomain(i)) b_tmp = X.get_local_vector(i);

  //Construct the distVect to return
  double * b_arr_ret = b_tmp.get_ptr();
  py::object ddmpy =  py::module_::import("ddmpy");
  py::array_t<double> b_ret = py::array_t<double>({M},b_arr_ret);
  py::object ret = ddmpy.attr("DistVector")(b_ret,src_a);
  return ret;
}
py::object cg_dense_solve(py::object a, py::object b, py::object tol, py::object iter){
  //const int rank = MMPI::rank();

  py::object src_a = a.attr("dd");
  py::dict neighbors = src_a.attr("neighbors");

  py::object n_dd = src_a.attr("n");
  py::object rank_dd = src_a.attr("rank");

  int n_glob = n_dd.cast<int>(); //dd.n
  const int rank = rank_dd.cast<int>(); //dd.rank
  py::object N_sub_dd = src_a.attr("n_subdomains");
  py::object n_dofs_dd = src_a.attr("ni");
  const int N_SUBDOMAINS = N_sub_dd.cast<int>();
  const int N_DOFS = n_dofs_dd.cast<int>();
  std::shared_ptr<Process> p = bind_subdomains(N_SUBDOMAINS);
  map<int,std::vector<int>> res = convert_dict_to_map(neighbors);
  std::vector<Subdomain> sd;
  for (int i = 0; i < N_SUBDOMAINS; ++i) if(p->owns_subdomain(i)) sd.emplace_back(i, N_DOFS, std::move(res));
  p->load_subdomains(sd);

  //local matrix
  py::array a_ = a.attr("local");
  auto a_arr = a_.request();
  const int M = a_arr.shape[0];
  double * a_ptr = (double*)a_arr.ptr; //ptr to the binded matrix

  const DenseMatrix<double> A_loc(DenseData<double>(M,M,a_ptr),true);

  // Create the partitioned matrix
  std::map<int, DenseMatrix<double>> A_map;
  for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) A_map[k] = A_loc;
  const PartMatrix<DenseMatrix<double>> A(p, std::move(A_map));


  //local vector
  py::object src_b = b.attr("local");
  vector <double> data_b = src_b.cast<vector<double>>();
  double* b_arr_ptr = data_b.data(); //ptr to the binded vector

  Vector<double> b_loc(DenseData<double,1>(M,b_arr_ptr),true); //vector from b_arr_ptr

  // Create the partitioned vector
  std::map<int, Vector<double>> b_map;
  for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) b_map[k] = b_loc;
  const PartVector<Vector<double>> b_vect(p, b_map);

  using Solver_S = ConjugateGradient<PartMatrix<DenseMatrix<double>>, PartVector<Vector<double>>>;

  // Setup
  Solver_S cg_solver;
  cg_solver.setup(parameters::A{A},
                  parameters::max_iter{50},
                  parameters::tolerance{1e-8},
                  parameters::verbose{(rank == 0)});

  // Solve

  PartVector<Vector<double>> X(p, b_map);

  X = cg_solver * b_vect;

  Vector<double> b_tmp;

  for (int i = 0; i < N_SUBDOMAINS; ++i) if(p->owns_subdomain(i)) b_tmp = X.get_local_vector(i);

  //Construct the distVect to return
  double * b_arr_ret = b_tmp.get_ptr();
  py::object ddmpy =  py::module_::import("ddmpy");
  py::array_t<double> b_ret = py::array_t<double>({M},b_arr_ret);
  py::object ret = ddmpy.attr("DistVector")(b_ret,src_a);
  return ret;
}

struct cg {
    cg(py::object a, py::object tol, py::object iter): a(a), tol(tol), iter(iter) {

    }
    py::object solve(py::object b){
        return solve_gen<Solver_cg>(a,b,tol,iter);
    }
    py::object a;
    py::object tol;
    py::object iter;
};


template<typename Matrix, typename Vect, typename SolverS, typename HybSolver>
Vect hybrid_solve(const Vect& B, const Matrix& K, double tol_, int maxiter){
  HybSolver hybrid_solver;

  bool verbose = false;
  if(MMPI::rank() == 0) verbose = true;

  SolverS& iter_solver = hybrid_solver.get_solver_S();
  if constexpr(is_solver_iterative<SolverS>::value){
    iter_solver.setup(parameters::max_iter{maxiter},
                      parameters::tolerance{tol_},
                      parameters::verbose{verbose});
  }

  hybrid_solver.setup(parameters::A{K},
                      parameters::verbose{verbose});

  using SolverK = typename HybSolver::solver_on_K;
  if constexpr(is_solver_iterative<SolverK>::value){
    auto setup_solverk = [verbose,maxiter](SolverK& solver){
                           solver.setup(parameters::max_iter{maxiter},
                                        parameters::verbose{verbose});
                         };
    hybrid_solver.setup(parameters::setup_solver_K<std::function<void(SolverK&)>>{setup_solverk});
  }

  return hybrid_solver * B;
}

py::object dss_dense_solve(py::object a, py::object b, py::object tol, py::object iter){
  //const int rank = MMPI::rank();

  py::object src_a = a.attr("dd");
  py::dict neighbors = src_a.attr("neighbors");

  py::object n_dd = src_a.attr("n");
  py::object rank_dd = src_a.attr("rank");

  int n_glob = n_dd.cast<int>(); //dd.n
  const int rank = rank_dd.cast<int>(); //dd.rank
  py::object N_sub_dd = src_a.attr("n_subdomains");
  py::object n_dofs_dd = src_a.attr("ni");
  const int N_SUBDOMAINS = N_sub_dd.cast<int>();
  const int N_DOFS = n_dofs_dd.cast<int>();
  std::shared_ptr<Process> p = bind_subdomains(N_SUBDOMAINS);
  map<int,std::vector<int>> res = convert_dict_to_map(neighbors);
  std::vector<Subdomain> sd;
  for (int i = 0; i < N_SUBDOMAINS; ++i) if(p->owns_subdomain(i)) sd.emplace_back(i, N_DOFS, std::move(res));
  p->load_subdomains(sd);

  //local matrix
  py::array a_ = a.attr("local");
  auto a_arr = a_.request();
  const int M = a_arr.shape[0];
  double * a_ptr = (double*)a_arr.ptr; //ptr to the binded matrix

  const DenseMatrix<double> A_loc(DenseData<double>(M,M,a_ptr),true);

  // Create the partitioned matrix
  std::map<int, DenseMatrix<double>> A_map;
  for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) A_map[k] = A_loc;
  const PartMatrix<DenseMatrix<double>> A(p, std::move(A_map));


  //local vector
  py::object src_b = b.attr("local");
  vector <double> data_b = src_b.cast<vector<double>>();
  double* b_arr_ptr = data_b.data(); //ptr to the binded vector

  Vector<double> b_loc(DenseData<double,1>(M,b_arr_ptr),true); //vector from b_arr_ptr

  // Create the partitioned vector
  std::map<int, Vector<double>> b_map;
  for(int k = 0; k < N_SUBDOMAINS; ++k) if(p->owns_subdomain(k)) b_map[k] = b_loc;
  const PartVector<Vector<double>> b_vect(p, b_map);

  double tol_ = tol.cast<double>();
  int maxiter = iter.cast<int>();

  PartVector<Vector<double>> X(p, b_map);

  X = hybrid_solve<DMat, Vect, Solver_S, Solver>(b_vect, A,tol_,maxiter);

  Vector<double> b_tmp;

  for (int i = 0; i < N_SUBDOMAINS; ++i) if(p->owns_subdomain(i)) b_tmp = X.get_local_vector(i);

  //Construct the distVect to return
  double * b_arr_ret = b_tmp.get_ptr();
  py::object ddmpy =  py::module_::import("ddmpy");
  py::array_t<double> b_ret = py::array_t<double>({M},b_arr_ret);
  py::object ret = ddmpy.attr("DistVector")(b_ret,src_a);
  return ret;
}

struct dss {
    dss(py::object a, py::object tol, py::object iter): a(a), tol(tol), iter(iter) {

    }
    py::object solve(py::object b){
        return dss_dense_solve(a,b,tol,iter);
    }
    py::object a;
    py::object tol;
    py::object iter;
};

struct pcg {
    pcg(py::object a, py::object tol, py::object iter): a(a), tol(tol), iter(iter) {

    }
    py::object solve(py::object b){
        return solve_gen<Solver_S>(a,b,tol,iter);
    }
    py::object a;
    py::object tol;
    py::object iter;
};

PYBIND11_MODULE(pycompose_core,m){
  py::class_<cg>(m,"ConjGrad")
    .def(py::init<py::object, py::object, py::object>())
    .def("solve", &cg::solve);
  py::class_<dss>(m,"dss_p_cg")
    .def(py::init<py::object, py::object, py::object>())
    .def("solve", &dss::solve);
  py::class_<pcg>(m,"pcg")
    .def(py::init<py::object, py::object, py::object>())
    .def("solve", &pcg::solve);
  m.def("multiply_dense",&multiply_dense);
  m.def("solve_dense", &solve_dense);
  m.def("multiply_coo",&multiply_coo);
  m.def("multiply_csc",&multiply_csc);
  m.def("solve_coo",&solve_coo);
  m.def("solve_csc",&solve_csc);
  m.def("multiply_dist",&multiply_dist);
  m.def("multiply_dist_coo",&multiply_dist_coo);
  m.def("pycentralize_dense",&pycentralize_dense);
  m.def("pycentralize_coo",&pycentralize_coo);
  m.def("pycentralize_v",&pycentralize_v);
  m.def("schur_coo", &schur_coo);
  m.def("cg_dense", &cg_dense);
}
