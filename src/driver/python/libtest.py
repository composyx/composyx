import numpy as np
from ctypes import *
import os
import hashlib

CXX="g++"
CPP_FLAGS = ["--std=c++17", "-DCOMPOSYXPP_HEADER_ONLY"]
CPP_INCLUDES=["/home/gmarait/Repo/composyx/include", "/home/gmarait/Repo/blaspp/install/include"]
CPP_LIBS=[]

def compile_load(code):
    hashcode = hashlib.sha224(code.encode('utf-8')).hexdigest()[:16]
    fname = hashcode + ".cpp"
    libname = hashcode + ".so"

    if os.path.exists(libname):
        print("> Found: " + libname)
        return cdll.LoadLibrary("./" + libname)

    with open(fname, 'w') as f:
        f.write("#include <iostream>\n")
        f.write("#include <composyx.hpp>\n")
        f.write("#include <composyx/solver/ConjugateGradient.hpp>\n")
        f.write("using namespace composyx;\n")
        f.write('extern "C"{\n')
        f.write(code)
        f.write("\n}")

    compile_line = CXX + " -shared "
    for flag in CPP_FLAGS:
        compile_line += flag + " "
    for idir in CPP_INCLUDES:
        compile_line += "-I " + idir + " "
    for ldir in CPP_LIBS:
        compile_line += "-L " + ldir + " "
    compile_line += " -o " + libname + " -fPIC " + fname
    print("> " + compile_line)
    os.system(compile_line)
    return cdll.LoadLibrary("./" + libname)

def mpp_multiply(A, b):
    cpp_str = "void driver(double * a_ptr, double * b_ptr, double * x_ptr, int m){\n"

    # Matrix A
    cpp_str += "const DenseMatrix<double> A(DenseData<double>(m, m, a_ptr), true);\n";

    # Vector b
    cpp_str += "const Vector<double> B(DenseData<double, 1>(m, b_ptr), true);\n";

    # Vector x
    cpp_str += "Vector<double> X(DenseData<double, 1>(m, x_ptr), true);\n";
    
    # Multiplication
    cpp_str += "X = A * B;\n"

    cpp_str += "}\n"

    lib = compile_load(cpp_str)
    x = np.zeros(b.shape, dtype='d')

    driver = lib.driver
    driver.argstypes = [POINTER(c_double), POINTER(c_double), POINTER(c_double), c_int]
    a_ptr = A.ctypes.data_as(POINTER(c_double))
    b_ptr = b.ctypes.data_as(POINTER(c_double))
    x_ptr = x.ctypes.data_as(POINTER(c_double))
    driver(a_ptr, b_ptr, x_ptr, c_int(A.shape[0]))

    return x

def mpp_solve(A, b):
    cpp_str = "void driver(double * a_ptr, double * b_ptr, double * x_ptr, int m){\n"

    # Matrix A
    cpp_str += "const DenseMatrix<double> A(DenseData<double>(m, m, a_ptr), true);\n"

    # Vector b
    cpp_str += "const Vector<double> B(DenseData<double, 1>(m, b_ptr), true);\n"

    # Vector x
    cpp_str += "Vector<double> X(DenseData<double, 1>(m, x_ptr), true);\n"
    #cpp_str += "X.display(\"x\");\n"

    # Solver
    cpp_str += "ConjugateGradient<DenseMatrix<double>, Vector<double>> solver;\n"
    cpp_str += "solver.setup(parameters::A{A},"
    cpp_str += "parameters::verbose{true},"
    cpp_str += "parameters::max_iter{10},"
    cpp_str += "parameters::tolerance{1e-8});\n"
    cpp_str += "X = solver * B;\n"

    cpp_str += "}\n"

    lib = compile_load(cpp_str)
    x = np.zeros(b.shape, dtype='d')

    driver = lib.driver
    driver.argstypes = [POINTER(c_double), POINTER(c_double), POINTER(c_double), c_int]
    a_ptr = A.ctypes.data_as(POINTER(c_double))
    b_ptr = b.ctypes.data_as(POINTER(c_double))
    x_ptr = x.ctypes.data_as(POINTER(c_double))
    driver(a_ptr, b_ptr, x_ptr, c_int(A.shape[0]))

    return x

A = np.array([[4., -1, 0.],
              [-1, 3., -0.5],
              [0., -0.5, 4.]], dtype='d')
u = np.array([1., 2., 3.], dtype='d')
b = mpp_multiply(A, u)

print("A", A)
print("u", u)
print("b = A * u", b)
print("With python:")
print(A @ u)

x = mpp_solve(A, b)
print("x = solve(A, b)", x)
