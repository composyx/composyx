from pycompose import *
import matplotlib.pyplot as plt
import time


A = np.array([[2.,0.,3.],[0.,2.,0.],[1.,0.,3.]],dtype='float64')
u = np.array([1., 2., 3.], dtype='d')

b_coo = multiply(coo_matrix(A),u)

print("coo(A)",coo_matrix(A))
print("u", u)
print("b_coo = A * u", b_coo)
x = solve(coo_matrix(A),b_coo)
print("x_coo = solve(A, b)", x)

b_csc = multiply(csc_matrix(A),u)
print("csc(A)",csr_matrix(A))
print("u", u)
print("b_csc = A * u", b_csc)
x = solve(csc_matrix(A),b_csc)
print("x_csc = solve(A, b)", x)




#time_pycmp = []
#time_np = []
#for i in range(1,101):
#    A = random(i,i)
#    u = np.random.random(i)
#    then = time.time()
#    b = multiply(A,u)
#    now = time.time()
#    time_pycmp.append(now-then)
#
#for i in range(1,101):
#    A = random(i,i)
#    u = np.random.random(i)
#    then = time.time()
#    b = A.multiply(u)
#    now = time.time()
#    time_np.append(now-then)
#
#
#plt.plot(np.linspace(1,100,100),time_pycmp,'b*',label="pycompose sparse multiply")
#plt.plot(np.linspace(1,100,100),time_np,'r*',label="scipy sparse multiply")
#plt.yscale('log')
#plt.title("Time difference between pycompose and scipy sparse multiplication")
#plt.xlabel("Matrix size")
#plt.ylabel("Time")
#plt.legend()
#plt.show()
#