import numpy as np
import scipy.sparse.linalg as sla
import pycompose
import matplotlib.pyplot as plt
import time
import random


A = np.array([[0., -1, 0.],
              [-1, 3., 0.],
              [0., -0.5, 4.]], dtype='d')
u = np.array([1., 2., 3.], dtype='d')

b = pycompose.multiply(A,u)
print(b)
#print("A", A)
#print("u", u)
#print("b = A * u", b)
#x = pycompose.solve(A,b)
#print("x = solve(A, b)", x)


time_pycmp = []
time_np = []

norm_pycmp = []
norm_np = []
#for i in range(1,101):
#    A = np.random.random((i*100,i*100))
#    u = np.random.random(i*100)
#    then = time.time()
#    b = pycompose.multiply(A,u)
#    now = time.time()
#    time_pycmp.append(now-then)
#
#for i in range(1,101):
#    A = np.random.random((i*100,i*100))
#    u = np.random.random(i*100)
#    then = time.time()
#    b = A @ u   
#    now = time.time()
#    time_np.append(now-then)
for i in range(1,101):
    A = np.array([[j*5 for j in range(i)] for k in range(i)],dtype='d')
    u = np.array([j for j in range(i)],dtype='d')
    b = pycompose.multiply(A,u)
    then = time.time()
    x = pycompose.solve(A,b)
    now = time.time()
    time_pycmp.append(now-then)
    norm_pycmp.append(np.linalg.norm(x - u))

for i in range(1,101):
    A = np.array([[j*5 for j in range(i)] for k in range(i)],dtype='d')
    u = np.array([j for j in range(i)],dtype='d')
    b = A @ u 
    then = time.time()
    x = sla.cg(A,b)
    x = np.array(x)
    now = time.time()
    time_np.append(now-then)

#plt.plot(np.linspace(1,101,100),time_pycmp,'b*',label="pycompose cg")
#plt.plot(np.linspace(1,101,100),time_np,'r*',label="scipy cg")
##plt.plot(np.linspace(1,101,100),norm_pycmp,'b',label="pycompose norm cg")
#plt.yscale('log')
#plt.title("Time difference between pycompose and scipy matrix-vector multiplication")
#plt.xlabel("Matrix size")
#plt.ylabel("Time")
#plt.legend()
#plt.show()
