import ddmpy
from scipy.sparse import *
import scipy.sparse.linalg as sla
import numpy as np
import time
import sys
sys.path.append('/home/abibi/Desktop/compose/compose/src/driver/python/build')
import pycompose
from multipledispatch import dispatch
#


comm = ddmpy.MPI.COMM_WORLD
rank = comm.Get_rank()

### neighbors :
dd = ddmpy.DomainDecomposition.test()

### mutiplication : 
Ki = np.array([[3.,-1.,0.,-1.],[-1.,4.,-1.,-1.],[0.,-1.,3.,-1.],[-1.,-1.,-1.,4.]])
ki_ = coo_matrix(Ki)
A = ddmpy.DistMatrix(Ki,dd) #DistMatrix  with dense local
B = ddmpy.DistMatrix(ki_,dd) #DistMatrix with sparse_coo local

x = ddmpy.DistVector(np.ones((4,1)),dd) #DistVector


b = multiply(A,x) #pycompose matvect of DistMatrix @ DistVector


A_ = pycentralize(A) # pyceompose centralize of ddmpy DistMatrix
B_ = pycentralize(B)
b_ = pycentralize(b) 
x_ = pycentralize(x)

if dd.rank == 0:
    print("Shown results are computed with the pycompose centralize :")
    print("Multiplication :")
    print("\n")
    print("A",A_)
    print("\n")
    print("x",x_)
    print("\n")
    print("b = A @ x",b_)


####### solvers
x, i = ddmpy.cg(B, b) #ddmpy cg for matrices that were multiplied in pycompose

X_dense = pycompose.cg_dense(A,b) # cg solver of pycompose
X_coo = pycompose.schur_coo(B,b) # schur solver of pycompose
X_centr_dense = X_dense.centralize() #ddmpy centralize of an objected returned with pycompose
X_centr_coo = X_coo.centralize() # ddmpy centralize of an objected returned with pycompose
x_centr = x.centralize() #ddmpy centralize
 
if dd.rank == 0:
    x_,i_ = sla.cg(B_,b_)
    print("pycompose cg solver :")
    print(X_centr_dense)
    print("\n")
    print("ddmpy cg solver : ")
    print(x_centr)
    print("\n")
    print("pycompose schur solver :")
    print(X_centr_coo)
    print("\n")
    print("scipy cg solver :")
    print(x_)



# ConjGrad ddmpy
S = ddmpy.ConjGrad(A,tol=1e-6)
x_cg = S.dot(b)
print("conjGrad ddmpy :", pycentralize(x_cg))


## ConjGrad pycompose
S1 = pycompose.ConjGrad(A,100,1e-6)
x_cmp = S1 @ b
print("conjGrad pycompose :", pycentralize(x_cmp))
