import numpy as np
import scipy.sparse as sp
import pycompose
import ddmpy


comm = ddmpy.MPI.COMM_WORLD
rank = comm.Get_rank()
# neighbors :
dd = ddmpy.DomainDecomposition.test()
A = np.array([[0., -1, 0.],
            [-1, 3., 0.],
            [0., -0.5, 4.]], dtype='d')
u = np.array([1., 2., 3.], dtype='d')

b = A @ u
print(b)

#b = pycompose.multiply(A,u)
#print(b)

#x = pycompose.solve(A,b)
#print(x)

A_coo = sp.coo_matrix(A)
A_csc = sp.csc_matrix(A)
b_coo = A_coo.dot(u)
b_csc = A_csc.dot(u)
print("b_coo",b_coo)
print("b_csc",b_csc)

b_coo = pycompose.multiply(A_coo,u)
b_csc = pycompose.multiply(A_csc,u)
print("b_csc", b_csc)
print("b_coo", b_coo)

x_csc = pycompose.solve(A_csc,b_csc)
x_coo = pycompose.solve(A_coo,b_coo)

print("x_csc", x_csc)
print("x_coo", x_coo)


# mutiplication :
Ki = np.array([[3.,-1.,0.,-1.],[-1.,4.,-1.,-1.],[0.,-1.,3.,-1.],[-1.,-1.,-1.,4.]])
A = ddmpy.DistMatrix(Ki,dd) #DistMatrix  with dense local

u = ddmpy.DistVector(np.ones((4,1)),dd) #DistVector

b_ddmpy = A @ u
#b_ddmpy = b_ddmpy.centralize()
if dd.rank == 0:
    print("b_ddmpy", b_ddmpy)

b_cmp = pycompose.multiply(A,u) #pycompose matvect of DistMatrix @ DistVector
#b_cmp = b_cmp.centralize()

if dd.rank == 0:
    print("b_cmp", b_cmp)

####### solvers
x_ddmpy, i = ddmpy.cg(A, b_cmp) #ddmpy cg for matrices that were multiplied in pycompose
x_cmp = pycompose.cg(A,b_cmp)
#if dd.rank == 0:
    #print("x_ddmpy",pycompose.pycentralize(x_ddmpy))
    #print("x_cmp", pycompose.pycentralize(x_cmp))

# ConjGrad ddmpy
S = ddmpy.ConjGrad(A,tol=1e-6)
x_cg = S.dot(b)
#print("conjGrad ddmpy :", pycentralize(x_cg))


## ConjGrad pycompose
S1 = pycompose.ConjGrad(A,100,1e-6)
x_cmp = S1 @ b
#print("conjGrad pycompose :", pycentralize(x_cmp))