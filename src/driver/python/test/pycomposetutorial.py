import numpy as np
import scipy.sparse as sp
import pycompose
import ddmpy
A = np.array([[4., -1, 0.],
              [-1, 3., -0.5],
              [0., -0.5, 4.]], dtype='d')
u = np.array([1., 2., 3.], dtype='d')

b_np = A @ u
print("b_np",b_np)

b_cmp = pycompose.multiply(A,u)
print("b_cmp",b_cmp)

x = pycompose.solve(A,b_cmp)
print("u",x)


A_coo = sp.coo_matrix(A)
A_csc = sp.csc_matrix(A)
b_coo = A_coo.dot(u)
b_csc = A_csc.dot(u)
print("b_coo",b_coo)
print("b_csc",b_csc)

b_coo = pycompose.multiply(A_coo,u)
b_csc = pycompose.multiply(A_csc,u)
print("b_csc", b_csc)
print("b_coo", b_coo)

x_csc = pycompose.solve(A_csc,b_csc)
x_coo = pycompose.solve(A_coo,b_coo)

print("x_csc", x_csc)
print("x_coo", x_coo)

#comm = ddmpy.MPI.COMM_WORLD
#rank = comm.Get_rank()
#
## neighbors :
#dd = ddmpy.DomainDecomposition.test()
#
## mutiplication :
#Ki = np.array([[3.,-1.,0.,-1.],[-1.,4.,-1.,-1.],[0.,-1.,3.,-1.],[-1.,-1.,-1.,4.]])
#A = ddmpy.DistMatrix(Ki,dd) #DistMatrix  with dense local
#
#u = ddmpy.DistVector(np.ones((4,1)),dd) #DistVector
##
#b_ddmpy = A @ u
##b_dd = b_ddmpy.centralize()
##if dd.rank == 0:
#    print("b_dd",b_dd)
#
#b_cmp = pycompose.multiply(A,u) #pycompose matvect of DistMatrix @ DistVector
#b_cp = b_cmp.centralize() 
#if dd.rank == 0:
#     print("b_cp",b_cp)
#
######## solvers
#x_ddmpy, i = ddmpy.cg(A, b_cmp) #ddmpy cg for matrices that were multiplied in pycompose
#x_cmp = pycompose.cg(A,b_cmp)
#x_dd = pycompose.pycentralize(x_ddmpy)
#x_cp = pycompose.pycentralize(x_cmp)
#
#if dd.rank == 0:
#    print("x_ddmpy", x_dd)
#    print("x_cmp", x_cp)
#
## ConjGrad ddmpy
#S = ddmpy.ConjGrad(A,tol=1e-6)
#x_cg = S.dot(b_ddmpy)
#print("conjGrad ddmpy :", pycompose.pycentralize(x_cg))


# ConjGrad pycompose
#S1 = pycompose.ConjGrad(A)
#x_cmp = S1 @ b_ddmpy
#print("conjGrad pycompose :", pycompose.pycentralize(x_cmp))

#S = ddmpy.DistSchurSolver(A,
#                    interface = [0, 1, 2, 3],
#                    interface_solver = ddmpy.ConjGrad())
#x_dd = S @ b_ddmpy
#x_ddmpy = x_dd.centralize()
#

#if dd.rank == 0:
#    print("x_ddmpy",x_ddmpy)
#    
#S1 = ddmpy.DistSchurSolver(A,
#                    interface = [0, 1, 2, 3],
#                    interface_solver = pycompose.ConjGrad())
#x_cp = S1 @ b_ddmpy
#x_cmp = x_cp.centralize()
#
#if dd.rank == 0:
#    print("x_ddmpy",x_ddmpy)
#    print("s_cmp", x_cmp)