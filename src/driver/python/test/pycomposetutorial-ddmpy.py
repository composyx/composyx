import numpy as np
import scipy.sparse as sp
import pycompose
import ddmpy

comm = ddmpy.MPI.COMM_WORLD
rank = comm.Get_rank()

# neighbors :
dd = ddmpy.DomainDecomposition.test()

# mutiplication :
Ki = np.array([[3.,-1.,0.,-1.],[-1.,4.,-1.,-1.],[0.,-1.,3.,-1.],[-1.,-1.,-1.,4.]])
A = ddmpy.DistMatrix(Ki,dd) #DistMatrix  with dense local
u = ddmpy.DistVector(np.ones((4,1)),dd) #DistVector
b_ddmpy = A @ u

b_dd = b_ddmpy.centralize()
if dd.rank == 0:
    print("ddmpy multiply: b_dd",b_dd)

b_cmp = pycompose.multiply(A,u) #pycompose matvect of DistMatrix @ DistVector
b_cp = b_cmp.centralize()
if dd.rank == 0:
     print("pycompose multiply: b_cp",b_cp)

####### solvers
x_ddmpy, i = ddmpy.cg(A, b_cmp) #ddmpy cg for matrices that were multiplied in pycompose
x_cmp = pycompose.cg(A,b_cmp)
x_dd = pycompose.pycentralize(x_ddmpy)
x_cp = pycompose.pycentralize(x_cmp)

if dd.rank == 0:
    print("ddmpy solve x_ddmpy", x_dd)
    print("pycompose solve x_cmp", x_cp)

####### solvers
x_ddmpy, i = ddmpy.cg(A, b_cmp) #ddmpy cg for matrices that were multiplied in pycompose
x_cmp = pycompose.cg(A,b_cmp)
x_dd = pycompose.pycentralize(x_ddmpy)
x_cp = pycompose.pycentralize(x_cmp)

if dd.rank == 0:
    print("ddmpy solve x_ddmpy", x_dd)
    print("pycompose solve x_cmp", x_cp)

# ConjGrad ddmpy
S = ddmpy.ConjGrad()
S.setup(A,tol=1e-6,maxiter=100)
x_cg = S.dot(b_cmp)

x_cg_centr = x_cg.centralize()
if dd.rank == 0:
    print("conjGrad ddmpy :", x_cg_centr)

# ConjGrad pycompose
S1 = pycompose.ConjGrad()
S1.setup(A,tol=1e-5,max_iter=120)
x_cmp = S1.solve(b_ddmpy)
x_cmp_centr = x_cmp.centralize()
if dd.rank == 0:
    print("conjGrad pycompose :", x_cmp_centr)

S2 = ddmpy.DistSchurSolver(interface_solver = pycompose.ConjGrad())
S2.setup(A)

x__ = S2.solve(b_ddmpy)
x__centr = x__.centralize()

if dd.rank == 0:
    print("ddmpy DistSchur with an interface solver pycomposeConjGrad :", x__centr)


S3 = pycompose.dss_pastix_pcg()
S3.setup(A,tol=1e-6,max_iter=100)

x = S3.solve(b_ddmpy)
x_centr = x.centralize()

if dd.rank == 0:
    print("distschur_pastix_cg_addSchwartz all configured with compose :", x_centr)

S4 = ddmpy.DistSchurSolver(interface_solver = pycompose.pcg())
S4.setup(A,tol=1e-6,max_iter=100)

x = S4.solve(b_ddmpy)
x_centr = x.centralize()

if dd.rank == 0:
    print("ddmpy DistSchur with an interface solver pycomposeConjGrad preconditioned with pycomposeAS :", x_centr)