from ddmpy import *
import numpy as np

comm = MPI.COMM_WORLD
rank = comm.Get_rank()


dd = DomainDecomposition.test()
Ki = np.array([[3,-1,0,-1],[-1,4,-1,-1],[0,-1,3,-1],[-1,-1,-1,4]],dtype=float)
A = DistMatrix(Ki,dd)
b = DistVector(np.arange(4),dd,assemble=True)

def cg(A,b,x0=None, tol=1e-5, maxiter=None, xtype=None, M=None, callback=None):
    bb = b.T @ b
    maxiter = len(b) if maxiter is None else maxiter
    x = 0 * b if x0 is None else x0
    r = b - A @ x
    if r.T @ r <= tol * tol * bb : 
        return x, 0
    z = r if M is None else M @ rank
    p = z.copy()
    rz = r.T @ z
    for i in range(maxiter):
        Ap = A @ p
        pAp = p.T @ Ap
        alpha = (rz / pAp)[0,0]
        x += alpha * p
        r -= alpha * Ap
        if callback is not None:
            callback(x)
        if r.T @ r <= tol * tol * bb:
            return x,0
        z = r if M is None else M @ r
        rz, rzold = r.T @ z, rz
        beta = (rz / rzold)[0,0]
        p = z + beta * p
    return x, i


print(A.local)
#b_ = A @ cg(A,b)[0]
#print(b.local,"\n")
#print(b_.local)
#
#print(np.linalg.norm(b_.local)/np.linalg.norm(b.local) <= 1e-5)
