import numpy as np
import pycompose_core
from scipy.sparse import *
import ddmpy
from multipledispatch import dispatch

@dispatch(np.ndarray,np.ndarray)
def multiply(A,u):
    return pycompose_core.multiply_dense(A,u)

@dispatch(np.ndarray,np.ndarray)
def solve(A,u):
    return pycompose_core.solve_dense(A,u)

@dispatch(coo_matrix,np.ndarray)
def multiply(A,u):
    return pycompose_core.multiply_coo(A,u)

@dispatch(coo_matrix,np.ndarray)
def solve(A,u):
    return pycompose_core.solve_coo(A,u)

@dispatch(csc_matrix,np.ndarray)
def multiply(A,u):
    return pycompose_core.multiply_csc(A,u)

@dispatch(csc_matrix,np.ndarray)
def solve(A,u):
    return pycompose_core.solve_csc(A,u)

@dispatch(ddmpy.DistMatrix)
def pycentralize(A):
    if(type(A.local) is np.ndarray):
        return pycompose_core.pycentralize_dense(A)
    elif(type(A.local) is coo_matrix):
        return pycompose_core.pycentralize_coo(A)

@dispatch(ddmpy.DistVector)
def pycentralize(b):
    return pycompose_core.pycentralize_v(b)

@dispatch(ddmpy.DistMatrix,ddmpy.DistVector)
def multiply(A,x):
    if(type(A.local) is np.ndarray):
        return pycompose_core.multiply_dist(A,x)
    elif(type(A.local) is coo_matrix):
        return pycompose_core.multiply_dist_coo(A,x)

@dispatch(ddmpy.DistMatrix,ddmpy.DistVector)
def solve(A,x):
    if(type(A.local) is np.ndarray):
        return pycompose_core.cg_dense(A,x)
    elif(type(A.local) is coo_matrix):
        return pycompose_core.schur_coo(A,x)

def cg(A,b):
    return solve(A,b)

#def ConjGrad(A):
#    return pycompose_core.ConjGrad(A)

class ConjGrad(ddmpy.LinearOperator):
    def setup(self,A,tol=1e-5,max_iter=100):
        ddmpy.LinearOperator.setup(self,A)
        self.A = A
        self.tol = tol
        self.max_iter = max_iter

    def solve(self,b):
        S = pycompose_core.ConjGrad(self.A,self.tol,self.max_iter)
        return S.solve(b)

class dss_pastix_pcg(ddmpy.LinearOperator):
    def setup(self,A,tol=1e-5,max_iter=100):
        ddmpy.LinearOperator.setup(self,A)
        self.A = A
        self.tol = tol
        self.max_iter = max_iter

    def solve(self,b):
        S = pycompose_core.dss_p_cg(self.A,self.tol,self.max_iter)
        return S.solve(b)

class pcg(ddmpy.LinearOperator):
    def setup(self,A,tol=1e-5,max_iter=100):
        ddmpy.LinearOperator.setup(self,A)
        self.A = A
        self.tol = tol
        self.max_iter = max_iter

    def solve(self,b):
        S = pycompose_core.pcg(self.A,self.tol,self.max_iter)
        return S.solve(b)