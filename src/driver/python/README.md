### Bind compose to python

## Pycompose:
A python module that binds the C++ library compose to python

The binding is done in the cpp file. The pycompose module is created using cmake, and used to call the wrapped functions using pybind11 from C++ library into a python environnement.


# ddmpy binding test :
$ mkdir build && cd build
$ cmake ..
$ cd .. && mpirun --np 3 --oversubscribe pyhton3 testddmpy.py


# Numpy test:
change in CMakeLists.txt:
pybind11_add_module(pycompose ddmpycompose.cpp)
with : 
pybind11_add_module(pycompose pycomposebind.cpp)
$ mkdir build && cd build
$ cmake ..
$ cd .. && pyhton3 pycomposebind.py


# Scipy test:
change in CMakeLists.txt:
pybind11_add_module(pycompose ddmpycompose.cpp)
with : 
pybind11_add_module(pycompose scipytest.cpp)

$ mkdir build && cd build
$ cmake ..
$ cd .. && pyhton3 pycomposebind.py

