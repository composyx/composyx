#include <iostream>
// the full Eigen wrappers for matrix and vector
#include <composyx/wrappers/Eigen/Eigen.hpp>

///  Type definition
using value_type = double;

using my_vector_type = composyx::E_Vector<value_type>;
using my_matrix_type = composyx::E_DenseMatrix<value_type>;

// Constraints for using Eigen vector in CPX iterative solver
namespace Eigen {
auto dot(my_vector_type const& v1,
         my_vector_type const& v2) -> my_vector_type::value_type {
  return v1.dot(v2);
}
[[nodiscard]] auto size(my_vector_type const& v) -> size_t {
  return static_cast<size_t>(v.size());
}
} // namespace Eigen

#include "composyx.hpp"
#include <composyx/solver/ConjugateGradient.hpp>

int main() {
  my_matrix_type A(3, 3);
  A << 4, -1, 0, -1, 3, -0.5, 0, -0.5, 4;
  my_vector_type u(3);
  u << 1, 2, 3;

  my_vector_type b = A * u;
  //
  composyx::ConjugateGradient<my_matrix_type, my_vector_type> solver;
  solver.setup(composyx::parameters::A{A}, composyx::parameters::verbose{true},
               composyx::parameters::max_iter{20},
               composyx::parameters::tolerance{1e-8});

  my_vector_type x = solver * b;

  composyx::display(x, "x:\n", std::cout);

  return 0;
}
