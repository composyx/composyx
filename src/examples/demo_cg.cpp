/*
 * In this example we use the ConjugateGradient with different operators and
 * vector types
 * - composyx internal dense and sparse matrices, with composyx vectors
 * - composyx 1D-laplacian operator, with composyx vectors
 * - eigen dense and sparse matrices, with eigen vectors
 * - armadillo dense and sparse matrices, with armadillo vectors
 * The system solved is :
 * ( 2 -1  0  0 )   ( 3 )   (  1 )
 * (-1  2 -1  0 )   ( 5 )   (  0 )
 * ( 0 -1  2 -1 ) x ( 7 ) = (  0 )
 * ( 0  0 -1  2 )   ( 9 )   ( 11 )
 */

#include <iostream>
#include <vector>
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <armadillo>

namespace composyx {
double dot(const Eigen::VectorXd&, const Eigen::VectorXd&);
auto size(const Eigen::Matrix<double, -1, 1, 0>&);
auto n_rows(const Eigen::Matrix<double, -1, 1, 0>&);
auto n_rows(const Eigen::SparseMatrix<double>&);
auto size(const arma::Col<double>&);
auto n_rows(const arma::Mat<double>&);
auto n_rows(const arma::SpMat<double>&);
} // namespace composyx

#include <composyx.hpp>
#include <composyx/solver/ConjugateGradient.hpp>
#include <composyx/loc_data/Laplacian.hpp>

namespace composyx {
template <> struct scalar_type<Eigen::VectorXd> {
  using type = double;
};
double dot(const Eigen::VectorXd& v1, const Eigen::VectorXd& v2) {
  return v1.dot(v2);
}

auto n_rows(const Eigen::Matrix<double, -1, 1, 0>& m) { return m.size(); }
auto size(const Eigen::Matrix<double, -1, 1, 0>& m) { return m.size(); }
template <> struct vector_type<Eigen::MatrixXd> {
  using type = Eigen::VectorXd;
};
template <> struct scalar_type<Eigen::MatrixXd> {
  using type = double;
};

auto n_rows(const Eigen::SparseMatrix<double>& m) { return m.size(); }
template <> struct vector_type<Eigen::SparseMatrix<double>> {
  using type = Eigen::VectorXd;
};
template <> struct scalar_type<Eigen::SparseMatrix<double>> {
  using type = double;
};

template <> struct scalar_type<arma::Col<double>> {
  using type = double;
};

auto n_rows(const arma::Mat<double>& m) { return m.size(); }
auto size(const arma::Col<double>& m) { return m.size(); }
template <> struct vector_type<arma::Mat<double>> {
  using type = arma::Col<double>;
};
template <> struct scalar_type<arma::Mat<double>> {
  using type = double;
};

auto n_rows(const arma::SpMat<double>& m) { return m.size(); }
template <> struct vector_type<arma::SpMat<double>> {
  using type = arma::Col<double>;
};
template <> struct scalar_type<arma::SpMat<double>> {
  using type = double;
};
} // namespace composyx

int main() {

  using Scalar = double;
  using Real = double;

  // Compose classes
  using CPX_D_mat = composyx::DenseMatrix<Scalar>;
  using CPX_SP_mat = composyx::SparseMatrixCOO<Scalar>;
  using CPX_Laplacian = composyx::Laplacian<Scalar>;
  using CPX_vect = composyx::Vector<Scalar>;

  // CG parameters
  const int max_iter = 10; // 4 should be enough
  const Real tolerance = Real{1e-5};
  const bool verbose = true;

  const int m = 4;
  const CPX_vect X_expected{3.0, 5.0, 7.0, 9.0};

#if defined(COMPOSYX_USE_RSB) || defined(COMPOSYX_USE_RSB_SPBLAS)
  if (rsb_lib_init(RSB_NULL_INIT_OPTIONS) != RSB_ERR_NO_ERROR)
    return -1;
#endif

  /************************************************************************/
  /************************************************************************/
  std::cout << "Solving with composyx dense matrix" << std::endl << std::endl;

  CPX_D_mat A1({2.0, -1.0, 0.0, 0.0, -1.0, 2.0, -1.0, 0.0, 0.0, -1.0, 2.0, -1.0,
                0.0, 0.0, -1.0, 2.0},
               m, m);
  A1.set_spd(composyx::MatrixStorage::full);

  const CPX_vect B{1.0, 0.0, 0.0, 11.0};

  composyx::ConjugateGradient<CPX_D_mat, CPX_vect> cg_mph_dense;
  cg_mph_dense.setup(composyx::parameters::A<CPX_D_mat>{A1},
                     composyx::parameters::max_iter<int>{max_iter},
                     composyx::parameters::tolerance<Real>{tolerance},
                     composyx::parameters::verbose<bool>{verbose});

  CPX_vect X1 = cg_mph_dense * B;
  X1.display("X1");

  cg_mph_dense.display("CG composyx dense");

  /************************************************************************/
  /************************************************************************/
  std::cout << "Solving with composyx sparse matrix" << std::endl << std::endl;

  CPX_SP_mat A2({0, 0, 1, 1, 2, 2, 3}, {0, 1, 1, 2, 2, 3, 3},
                {2.0, -1.0, 2.0, -1.0, 2.0, -1.0, 2.0}, 4, 4);
  A2.set_spd(composyx::MatrixStorage::lower);

  composyx::ConjugateGradient<CPX_SP_mat, CPX_vect> cg_mph_sparse;
  cg_mph_sparse.setup(composyx::parameters::A<CPX_SP_mat>{A2},
                      composyx::parameters::max_iter<int>{max_iter},
                      composyx::parameters::tolerance<Real>{tolerance},
                      composyx::parameters::verbose<bool>{verbose});

  CPX_vect X2 = cg_mph_sparse * B;
  X1.display("X2");

  cg_mph_sparse.display("CG composyx sparse");

  /************************************************************************/
  /************************************************************************/
  std::cout << "Solving with composyx 1D-laplacian function" << std::endl
            << std::endl;
  CPX_Laplacian lap;
  composyx::ConjugateGradient<CPX_Laplacian, CPX_vect> cg_mph_lap;
  cg_mph_lap.setup(composyx::parameters::A<CPX_Laplacian>{lap},
                   composyx::parameters::max_iter<int>{max_iter},
                   composyx::parameters::tolerance<Real>{tolerance},
                   composyx::parameters::verbose<bool>{verbose});

  CPX_vect X3 = cg_mph_lap * B;
  X3.display("X3");

  cg_mph_lap.display("CG composyx laplacian");

  /************************************************************************/
  /************************************************************************/
  // Eigen classes
  using E_mat = Eigen::MatrixXd;
  using E_vect = Eigen::VectorXd;

  std::cout << "Solving with eigen matrix" << std::endl << std::endl;

  E_mat A3(m, m);
  A3 << 2.0, -1.0, 0.0, 0.0, -1.0, 2.0, -1.0, 0.0, 0.0, -1.0, 2.0, -1.0, 0.0,
      0.0, -1.0, 2.0;

  E_vect B2(m);
  B2 << 1.0, 0.0, 0.0, 11.0;

  composyx::ConjugateGradient<E_mat, E_vect> cg_eigen;
  cg_eigen.setup(composyx::parameters::A<E_mat>{A3},
                 composyx::parameters::max_iter<int>{max_iter},
                 composyx::parameters::tolerance<Real>{tolerance},
                 composyx::parameters::verbose<bool>{verbose});

  E_vect X4 = cg_eigen * B2;
  std::cout << "X4\t" << X4.transpose() << std::endl;

  cg_eigen.display("CG with eigen matrix");

  // Eigen sparse class
  using E_SP_mat = Eigen::SparseMatrix<double>;
  using E_tri = Eigen::Triplet<double>;

  E_SP_mat A3_sp(4, 4);

  std::vector<E_tri> A3_sp_data{
      E_tri(0, 0, 2.0), E_tri(0, 1, -1.0), E_tri(1, 1, 2.0), E_tri(1, 2, -1.0),
      E_tri(2, 2, 2.0), E_tri(2, 3, -1.0), E_tri(3, 3, 2.0)};

  A3_sp.setFromTriplets(A3_sp_data.begin(), A3_sp_data.end());

  composyx::ConjugateGradient<decltype(A3_sp), E_vect> cg_sp_eigen;
  cg_sp_eigen.setup(composyx::parameters::A<decltype(A3_sp)>{A3_sp},
                    composyx::parameters::max_iter<int>{max_iter},
                    composyx::parameters::tolerance<Real>{tolerance},
                    composyx::parameters::verbose<bool>{verbose});

  E_vect X4_bis = cg_sp_eigen * B2;
  std::cout << "X4_bis\t" << X4_bis.transpose() << std::endl;

  /************************************************************************/
  /************************************************************************/
  // Armadillo classes
  using ARMA_mat = arma::Mat<Scalar>;
  using ARMA_vect = arma::Col<Scalar>;

  std::cout << "Solving with armadillo matrix" << std::endl << std::endl;

  const ARMA_mat A4 = {{2.0, -1.0, 0.0, 0.0},
                       {-1.0, 2.0, -1.0, 0.0},
                       {0.0, -1.0, 2.0, -1.0},
                       {0.0, 0.0, -1.0, 2.0}};

  const ARMA_vect B3 = {1.0, 0.0, 0.0, 11.0};

  composyx::ConjugateGradient<ARMA_mat, ARMA_vect> cg_arma;
  cg_arma.setup(composyx::parameters::A<ARMA_mat>{A4},
                composyx::parameters::max_iter<int>{max_iter},
                composyx::parameters::tolerance<Real>{tolerance},
                composyx::parameters::verbose<bool>{verbose});

  ARMA_vect X5 = cg_arma * B3;
  X5.print("X5");

  cg_arma.display("CG with armadillo matrix");

  using ARMA_SPmat = arma::SpMat<Scalar>;

  const ARMA_SPmat A5(A4);

  composyx::ConjugateGradient<ARMA_SPmat, ARMA_vect> cg_arma_sp;
  cg_arma_sp.setup(composyx::parameters::A<ARMA_SPmat>{A5},
                   composyx::parameters::max_iter<int>{max_iter},
                   composyx::parameters::tolerance<Real>{tolerance},
                   composyx::parameters::verbose<bool>{verbose});

  ARMA_vect X6 = cg_arma_sp * B3;
  X6.print("X6");

  cg_arma_sp.display("CG with armadillo sparse matrix");

#if defined(COMPOSYX_USE_RSB) || defined(COMPOSYX_USE_RSB_SPBLAS)
  if (rsb_lib_exit(RSB_NULL_EXIT_OPTIONS) != RSB_ERR_NO_ERROR)
    return -1;
#endif

  return 0;
}
