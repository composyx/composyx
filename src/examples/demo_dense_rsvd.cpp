/*
 * In this example we call dense matrices randomized svd or evd (for
 * symmetric/hermitian case)
 */

#include <iostream>
#include <string>
#include <composyx.hpp>
#ifndef COMPOSYX_NO_MPI
#include <composyx/dist/MPI.hpp>
#endif

using namespace composyx;

template <BlasKernelMatrix Matrix,
          CPX_Scalar Scalar = typename Matrix::scalar_type,
          CPX_Real Real = typename Matrix::real_type>
void check_svd(Matrix A, Matrix U, std::vector<Real> sigmas, Matrix Vt,
               int BlasOrChameleon = 0) {
  constexpr const double ev_tol =
      (is_precision_double<Scalar>::value) ? 1e-12 : 1e-5;
  Matrix Sigma(n_cols(U), n_rows(Vt));
  auto k = std::min(n_cols(U), n_rows(Vt));
  for (size_t i = 0; i < k; ++i)
    Sigma(i, i) = Scalar{sigmas[i]};
  // Matrix Acheck = U * Sigma * Vt;
  Matrix Acheck(n_rows(A), n_cols(A));
  Matrix SVt(k, n_cols(Vt));
  if (BlasOrChameleon == 0) {
    blas_kernels::gemm(Sigma, Vt, SVt);
    blas_kernels::gemm(U, SVt, Acheck);
  } else {
    chameleon_kernels::gemm(Sigma, Vt, SVt);
    chameleon_kernels::gemm(U, SVt, Acheck);
  }
  Matrix DiffSVD = A - Acheck;
  // DiffSVD.display("DiffSVD");
  std::cout << "Norm diff SVD = " << DiffSVD.norm() << std::endl;
  bool successSVD = (DiffSVD.norm() < ev_tol * A.norm());
  if (successSVD) {
    std::cout << "SVD success !" << std::endl;
  } else {
    std::cout << "SVD Failure..." << std::endl;
  }
}

int main(int argc, char* argv[]) {

  using Scalar = double;
  using Real = typename arithmetic_real<Scalar>::type;

  /* Choose between Blas and Chameleon kernels/solver */
  int BlasOrChameleon = 0;
  if (argc > 1) {
    BlasOrChameleon = std::stoi(argv[1]);
  }
  if (BlasOrChameleon == 0) {
    std::cout << "BlasOrChameleon = " << BlasOrChameleon << " -> blas is used"
              << std::endl;
  } else {
    std::cout << "BlasOrChameleon = " << BlasOrChameleon
              << " -> chameleon is used" << std::endl;
  }

  /* Set size M of the problem (number of rows in the matrices) */
  const int Defaultsize = 10;
  int M = Defaultsize;
  if (argc > 2) {
    std::cout << "M = " << argv[2] << std::endl;
    M = std::stoi(argv[2]);
  }

  /* Set size N of the problem (number of columns in the rectangular matrices)
   */
  int N = Defaultsize;
  if (argc > 3) {
    std::cout << "N = " << argv[3] << std::endl;
    N = std::stoi(argv[3]);
  }
  if (N > M) {
    std::cout << "N cannot be larger than M, exit." << std::endl;
    return -1;
  }

  /* Set rank of the matrix */
  int R = Defaultsize / 2;
  if (argc > 4) {
    std::cout << "R = " << argv[4] << std::endl;
    R = std::stoi(argv[4]);
  }
  if (R > N) {
    std::cout << "R cannot be larger than N, exit." << std::endl;
    return -1;
  }
  if (R <= 0) {
    std::cout << "R cannot be <= 0, exit." << std::endl;
    return -1;
  }

  /* Chameleon parameters: tile size, number of threads, gpus */
  int NB = std::min(320, N);
  if (argc > 5) {
    std::cout << "NB = " << argv[5] << std::endl;
    NB = std::stoi(argv[5]);
  }

  int NT = 4;
  if (argc > 6) {
    std::cout << "NT = " << argv[6] << std::endl;
    NT = std::stoi(argv[6]);
  }

  int NG = 0;
  if (argc > 7) {
    std::cout << "NG = " << argv[7] << std::endl;
    NG = std::stoi(argv[7]);
  }

  int CHECK = 0;
  if (argc > 8) {
    std::cout << "CHECK = " << argv[8] << std::endl;
    CHECK = std::stoi(argv[8]);
  }

  MMPI::init(MPI_THREAD_SERIALIZED);

  /* Initialize Chameleon context */
  initialize(NT, NG);
  setChameleonParameters(NB);
  if (BlasOrChameleon == 0) {
    // chameleon init set num threads to 1, force to use all threads for blas
    setBlasThreads(getMaxBlasThreads());
  }

  /* RSVD */

  /* Generate the matrix A or rank R */
  Timer<0> tbuild("Matrix building time");
  DenseMatrix<Scalar> Al(M, R);
  DenseMatrix<Scalar> Ar(R, N);
  DenseMatrix<Scalar> A(M, N);
  if (BlasOrChameleon == 0) {
    Al = blas_kernels::random_matrix_normal<DenseMatrix<Scalar>>(M, R);
    Ar = blas_kernels::random_matrix_normal<DenseMatrix<Scalar>>(R, N);
    blas_kernels::gemm(Al, Ar, A);
  } else {
    Al = chameleon_kernels::random_matrix_normal<DenseMatrix<Scalar>>(M, R);
    Ar = chameleon_kernels::random_matrix_normal<DenseMatrix<Scalar>>(R, N);
    chameleon_kernels::gemm(Al, Ar, A);
  }
  if (M <= Defaultsize and N <= Defaultsize)
    A.display("A");
  tbuild.stop();

  Timer<0> trsvd("RSVD time");
  std::vector<Real> Sigmas;
  DenseMatrix<Scalar> U, Vt;
  if (BlasOrChameleon == 0) {
    auto [Ui, sigmasi, Vti] = blas_kernels::rsvd(A, R, 0, false);
    U = std::move(Ui);
    Sigmas = std::move(sigmasi);
    Vt = std::move(Vti);
  } else {
    auto [Ui, sigmasi, Vti] = chameleon_kernels::rsvd(A, R, 0, false);
    U = std::move(Ui);
    Sigmas = std::move(sigmasi);
    Vt = std::move(Vti);
  }
  trsvd.stop();

  if (CHECK == 1) {
    Timer<0> trsvdcheck("Check time");
    check_svd(A, U, Sigmas, Vt, BlasOrChameleon);
    trsvdcheck.stop();
    trsvdcheck.summary_results();
  } else {
    trsvd.summary_results();
  }

  finalize();
  MMPI::finalize();
  return 0;
}
