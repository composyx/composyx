#include <iostream>
// the Eigen wrappers for matrix and vector
#include "composyx/wrappers/Eigen/EigenMatrix.hpp"

// The composyx matrix/vector include
#include "composyx/loc_data/DenseMatrix.hpp"

///  Type definition
using value_type = double;
// Composyx vector type
using my_vector_type = composyx::Vector<value_type>;
// Eigen matrix type (we use the composyx Eigen wrapper)
using my_matrix_type = composyx::E_DenseMatrix<value_type>;
//

/// Composability
namespace composyx {
// For concepts
// We specify that the vector type for an Eigen matrix is a composyx vector type
template <CPX_Scalar Scalar>
struct vector_type<E_DenseMatrix<Scalar>> : public std::true_type {
  using type = my_vector_type;
};
// The Eigen matrix * composyx vector operator
auto operator*(my_matrix_type const& A,
               my_vector_type const& x) -> my_vector_type {
  using value_type = my_matrix_type::value_type;
  using C_matrix_type = composyx::DenseMatrix<value_type>;
  // The eigen matrix is encapsulated (without copying) in a composyx matrix
  C_matrix_type AA(A.rows(), A.cols(), const_cast<value_type*>(A.data()), true);
  return AA * x;
}
} // namespace composyx

#include <composyx/solver/ConjugateGradient.hpp>

int main() {
  my_matrix_type A({{4, -1, 0}, {-1, 3, -0.5}, {0, -0.5, 4}});

  my_vector_type u({1, 2, 3}, 3);
  my_vector_type b = A * u;
  //
  composyx::ConjugateGradient<my_matrix_type, my_vector_type> solver;
  solver.setup(composyx::parameters::A{A}, composyx::parameters::verbose{true},
               composyx::parameters::max_iter{20},
               composyx::parameters::tolerance{1e-8});

  my_vector_type x = solver * b;

  composyx::display(x, "x:\n", std::cout);

  return 0;
}
