/*!
 * Example strongly inspired from fabulous' example1.cpp
 * \brief Example for fabulous use; dense matrix, not distributed
 */

#include <composyx.hpp>
#include <composyx/solver/Fabulous.hpp>
#include <utility>
#include <string>

using Scalar = std::complex<double>;
using DM = composyx::DenseMatrix<Scalar>; // fa::Block<Scalar>;
using SparseMat = composyx::SparseMatrixCOO<Scalar>;

/*! \brief example1 matrix */

int main(int argc, char** argv) {

  std::string mat_path("../matrices/young1c.mtx");
  if (argc == 2) {
    mat_path = std::string(argv[1]);
  }

  std::cout << "Search for matrix (complex) in: " << mat_path << '\n';

  SparseMat A;
  A.from_matrix_market_file(mat_path);
  std::vector<double> epsilon{1e-6};
  const int dim = A.get_n_cols();
  const int max_mvp = 1000;
  const int nrhs = 6;

  DM X(dim, nrhs);
  for (auto i = 0; i < dim; ++i)
    for (auto j = 0; j < nrhs; ++j)
      X(i, j) = Scalar{(double)i + 1, ((double)j + 1) / 2.0};

  // Choose the fabulous algorithm
  auto algo = fabulous::bgmres::qribdr();

  composyx::Fabulous<SparseMat, DM, composyx::Identity<SparseMat, DM>,
                     decltype(algo), fabulous::DeflatedRestart<Scalar>>
      fabulous;
  fabulous.setup(composyx::fabulous_params::A{A},
                 composyx::fabulous_params::max_iter{max_mvp},
                 composyx::fabulous_params::tolerances{epsilon},
                 // composyx::fabulous_params::preconditioner{M},
                 composyx::fabulous_params::verbose{true},
                 composyx::fabulous_params::algo{algo},
                 composyx::fabulous_params::deflated_restart{
                     fabulous::deflated_restart<Scalar>(20, Scalar{0.0})});

  DM B = A * X;
  DM sol = fabulous * B;

  std::cout << "Found\tExpected\n";
  for (auto i = 0; i < 10; ++i) {
    std::cout << sol.at(i, 0) << "\t\t" << X.at(i, 0) << std::endl;
  }

  fabulous.display("Fabulous");

  return 0;
}
