/*
 * In this example we call dense matrices operations (product and linear solver)
 * with MPI and Chameleon
 */

#include <iostream>
#include <string>
#include <chameleon.h>
#include <composyx.hpp>
#include <composyx/part_data/PartDenseMatrix.hpp>
#include <composyx/testing/TestMatrix.hpp>

using namespace composyx;

using Scalar = double;
using LocMatrix = DenseMatrix<Scalar>;
using MapFct = canonical_datamap::TwoD_block_cyclic;
using PartMat = PartDenseMatrix<LocMatrix, MapFct>;

void* getaddr(const CHAM_desc_t* desc, int ib, int jb) {
  PartMat* partmat = (PartMat*)desc->mat;
  return (void*)get_ptr(partmat->get_local_matrix(ib, jb));
}
int getrank(const CHAM_desc_t* desc, int ib, int jb) {
  PartMat* partmat = (PartMat*)desc->mat;
  return partmat->datamap(ib, jb);
}

int main(int argc, char* argv[]) {

  const int Defaultsize = 10;

  /* Set size M of the problem (number of rows in the A and C matrices) */
  int M = Defaultsize;
  if (argc > 1) {
    M = std::stoi(argv[1]);
  }

  /* Set size N of the problem (number of columns in the B and C matrices) */
  int N = Defaultsize;
  if (argc > 2) {
    N = std::stoi(argv[2]);
  }

  /* Set size K of the problem (number of columns in A and of rows in B) */
  int K = Defaultsize;
  if (argc > 3) {
    K = std::stoi(argv[3]);
  }

  /* Chameleon parameters: tile size, number of threads, gpus */
  int NB = std::min(2, N);
  if (argc > 4) {
    NB = std::stoi(argv[4]);
  }

  int NT = 4;
  if (argc > 5) {
    NT = std::stoi(argv[5]);
  }

  int NG = 0;
  if (argc > 6) {
    NG = std::stoi(argv[6]);
  }

  /* tiles mapping over MPI processes: 2D block-cyclic parameters P and Q */
  int P = 0;
  if (argc > 7) {
    P = std::stoi(argv[7]);
  }

  int Q = 0;
  if (argc > 8) {
    Q = std::stoi(argv[8]);
  }

  /* Initialize MPI, Composyx, Chameleon contexts */
  MMPI::init(MPI_THREAD_SERIALIZED);
  initialize(NT, NG);
  setChameleonParameters(NB);

  const int nprocs = MMPI::size();
  const int rank = MMPI::rank();

  /* Generate data (duplicated global matrices) */
  auto make_glob_mat = [rank](size_t lM, size_t lN) {
    LocMatrix glob(lM, lN);
    if (rank == 0) {
      glob = test_matrix::random_matrix<Scalar, LocMatrix>(lM * lN, -1.0, 1.0,
                                                           lM, lN)
                 .matrix;
    }
    MMPI::bcast(get_ptr(glob), lM * lN, 0, MPI_COMM_WORLD);
    return glob;
  };

  const LocMatrix glob_A = make_glob_mat(M, K);
  const LocMatrix glob_B = make_glob_mat(K, N);
  const LocMatrix glob_C = make_glob_mat(M, N);

  /* Define data mapping (2d block-cyclic) */
  if (P * Q != nprocs) {
    P = 1;
    Q = nprocs;
  }
  auto dmapfunc2dbc = std::make_shared<MapFct>(MapFct(P, Q));
  Datamap<MapFct> dmapA(dmapfunc2dbc, M, K);
  Datamap<MapFct> dmapB(dmapfunc2dbc, K, N);
  Datamap<MapFct> dmapC(dmapfunc2dbc, M, N);

  /* Define partitionned matrix */
  auto make_part_matrix = [rank, NB](const LocMatrix& glob_mat,
                                     Datamap<MapFct> ldmap) {
    std::map<std::pair<int, int>, LocMatrix> mat_map;
    size_t lM = glob_mat.get_n_rows();
    size_t lN = glob_mat.get_n_cols();
    size_t lMT = (lM + NB - 1) / NB;
    size_t lNT = (lN + NB - 1) / NB;
    for (size_t ib = 0; ib < lMT; ++ib) {
      for (size_t jb = 0; jb < lNT; ++jb) {
        size_t NR = (ib == (lMT - 1)) ? lM - ib * NB : NB;
        size_t NC = (jb == (lNT - 1)) ? lN - jb * NB : NB;
        if (ldmap(ib, jb) == rank) {
          mat_map.emplace(std::make_pair(ib, jb),
                          glob_mat.get_block_copy(ib * NB, jb * NB, NR, NC));
        }
      }
    }
    return PartMat(MPI_COMM_WORLD, mat_map, ldmap);
  };

  const auto mat_A = make_part_matrix(glob_A, dmapA);

  /* default */
  mat_A._gemm_algo = gemm_algo::chameleon_gemm;
  const auto mat_B = make_part_matrix(glob_B, dmapB);
  auto mat_C = make_part_matrix(glob_C, dmapC);
  mat_C = mat_A * mat_B;
  mat_C.display_centralized(0, "C");

  /* Finalize MPI, Composyx, Chameleon contexts */
  finalize();
  MMPI::finalize();

  return 0;
}
