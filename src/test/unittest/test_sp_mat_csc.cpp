// [[file:../../../org/composyx/loc_data/SparseMatrixCSC.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>

#include <composyx.hpp>

#include <composyx/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

using namespace composyx;

TEMPLATE_TEST_CASE("SparseMatrixCSC", "[sparse][csc_matrix]", float, double,
                   std::complex<float>, std::complex<double>) {
  using Scalar = TestType;
  using Index = int;
  using Spmat = SparseMatrixCSC<Scalar, Index>;
  using Vect = Vector<Scalar>;
  using Dmat = DenseMatrix<Scalar>;
  using Diag = DiagonalMatrix<Scalar>;

  SECTION("Empty constructor") {
    Spmat M;
    REQUIRE(M.get_n_rows() == 0);
    REQUIRE(M.get_n_cols() == 0);
    REQUIRE(M.get_nnz() == 0);
  }

  SECTION("size_t constructor") {
    Spmat M(3, 4);
    REQUIRE(M.get_n_rows() == 3);
    REQUIRE(M.get_n_cols() == 4);
    REQUIRE(M.get_nnz() == 0);
  }

  SECTION("Pointer constructor VS list constructor") {
    // ( 0  1  1  0)
    // ( 0  0  0  2)

    std::vector<int> M_i_data{0, 0, 1};
    std::vector<int> M_j_data{0, 0, 1, 2, 3};
    std::vector<Scalar> M_v_data{1.0, 1.0, 2.0};

    int* M_i = M_i_data.data();
    int* M_j = M_j_data.data();
    Scalar* M_v = M_v_data.data();
    Spmat M(2, 4, 3, M_i, M_j, M_v);

    Spmat M_check({0, 0, 1}, {0, 0, 1, 2, 3}, {1.0, 1.0, 2.0}, 2, 4);
    REQUIRE(M_check == M);
  }

  SECTION("Constructor from dense matrix") {
    const Spmat M_check({0, 0, 1}, {0, 0, 1, 2, 3}, {1.0, 1.0, 2.0}, 2, 4);
    const DenseMatrix<Scalar> dm({0, 0, 1, 0, 1, 0, 0, 2}, 2, 4);
    const Spmat M_c(dm);
    REQUIRE(M_c == M_check);
  }

  const Spmat CSCM({0, 0, 1}, {0, 0, 1, 2, 3}, {1.0, 1.0, 2.0}, 2, 4);
  SECTION("Copy constructor") {
    Spmat M(CSCM);
    REQUIRE(CSCM == M);
  }

  SECTION("Move constructor") {
    Spmat CSCM_bis(CSCM);
    Spmat M(std::move(CSCM_bis));
    REQUIRE(CSCM == M);
    REQUIRE(CSCM_bis.get_n_rows() == 0);
    REQUIRE(CSCM_bis.get_n_cols() == 0);
    REQUIRE(CSCM_bis.get_nnz() == 0);
  }

  /*#####################
    ## Test assignment ##
    #####################*/

  SECTION("Copy assignment") {
    Spmat M;
    M = CSCM;
    REQUIRE(M == CSCM);
  }

  SECTION("Move assignment") {
    Spmat CSCM_bis(CSCM);
    Spmat M;
    M = std::move(CSCM_bis);
    REQUIRE(M == CSCM);
  }

  /*#######################
    ## Matrix arithmetic ##
    #######################*/

  SECTION("Scalar multiplication") {
    Spmat M = CSCM;
    Spmat Mover2({0, 0, 1}, {0, 0, 1, 2, 3}, {0.5, 0.5, 1.0}, 2, 4);

    REQUIRE((M * Scalar{0.5}) == Mover2);
    REQUIRE((Scalar{0.5} * M) == Mover2);
    REQUIRE((M / Scalar{2.0}) == Mover2);
  }

  const Spmat CSCM2({0, 1, 0, 0, 1}, {0, 1, 2, 3, 5},
                    {1.0, 2.0, -3.0, 4.0, 4.0});
  const Vect V1{1.0, 2.0, -1.0, 1.0};
  SECTION("Matrix vector multiplication (full storage)") {

    //                 1
    //                 2
    // ( 1 0 -3 4 ) x -1 = 8
    // ( 0 2  0 4 )    1   8

    Vect CSCM2timesV1 = CSCM2 * V1;
    Vect V1_check{8.0, 8.0};
  }

  SECTION("Matrix vector multiplication (half storage)") {

    // ( 1 3 0 0 )    1    7
    // ( . 2 0 2 )    2    9
    // ( . . 1 0 ) x -1 = -1
    // ( . . . 2 )    1    6

    Spmat M({0, 0, 1, 2, 1, 3}, {0, 1, 3, 4, 6},
            {1.0, 3.0, 2.0, 1.0, 2.0, 2.0});
    M.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
    Vect MtimesV1 = M * V1;
    Vect V1_check2{7.0, 9.0, -1.0, 6.0};

    REQUIRE(MtimesV1 == V1_check2);
  }

  SECTION("Matrix vector multiplication with hermitian matrix (half storage)") {

    if constexpr (is_complex<Scalar>::value) {
      // ( 1,0 3,3  0,0 0,0  )    1, 1    10,4
      // (   . 2,0  0,0 2,1  )    2,-1    14,-5
      // (   .   . -1,0 1,-1 ) x  0, 1  = -1,-4
      // (   .   .    . 2,0  )    1,-2     4,-7

      Spmat M({0, 0, 1, 2, 1, 2, 3}, {0, 1, 3, 4, 7},
              {{1, 0}, {3, 3}, {2, 0}, {-1, 0}, {2, 1}, {1, -1}, {2, 0}}, 4, 4);
      M.set_property(MatrixSymmetry::hermitian, MatrixStorage::upper);
      const Vect V1c({{1, 1}, {2, -1}, {0, 1}, {1, -2}});
      const Vect MtimesV1c = M * V1c;
      const Vect V_check({{10, 4}, {14, -5}, {-1, -4}, {4, -7}});

      REQUIRE(MtimesV1c == V_check);
    }
  }

  SECTION("Matrix vector multiplication 2 (full storage)") {
    // ( 1 4 7 )    1   30
    // ( 2 5 8 ) x  2 = 36
    // ( 0 6 9 )    3   39

    Spmat M({0, 1, 0, 1, 2, 0, 1, 2}, {0, 2, 5, 8}, {1, 2, 4, 5, 6, 7, 8, 9});
    Vect X{1, 2, 3};
    Vect V_exp{30, 36, 39};
    Vect V = M * X;

    REQUIRE(V == V_exp);
  }

  SECTION("Matrix vector multiplication consistent with COO (random matrices, "
          "full)") {
    const auto tol = arithmetic_tolerance<Scalar>::value * 10;
    const MatrixProperties<Scalar> prop_default;
    const double density = 0.7;

    for (int k = 0; k < 10; ++k) {
      const int seed = 102 + k;
      Spmat csc = test_matrix::generate_matrix<Spmat>(6, 6, prop_default,
                                                      density, false, seed);
      SparseMatrixCOO<Scalar> coo;
      csc.convert(coo);
      //Vect v = test_matrix::random_matrix<Scalar, Vect>(6, -10.0, 10.0, 6).vector;
      Vect v = test_matrix::random_vector<Vect>(-10.0, 10.0, 6);

      Vect v_csc = csc * v;
      Vect v_coo = coo * v;

      REQUIRE((v_csc - v_coo).norm() < tol);
    }
  }

  SECTION("Matrix vector multiplication consistent with COO (random matrices, "
          "upper)") {
    const auto tol = arithmetic_tolerance<Scalar>::value * 10;
    for (int k = 0; k < 10; ++k) {
      Spmat csc =
          test_matrix::random_matrix<Scalar, Spmat>(15, -10.0, 10.0, 6, 6, true)
              .matrix;
      SparseMatrixCOO<Scalar> coo;
      csc.convert(coo);
      Vect v =
          test_matrix::random_matrix<Scalar, Vect>(6, -10.0, 10.0, 6).vector;

      Vect v_csc = csc * v;
      Vect v_coo = coo * v;

      REQUIRE((v_csc - v_coo).norm() < tol);
    }
  }

  SECTION("Matrix x diagonal matrix multiplication (half storage)") {

    // ( 1 3 0 0 )    ( 1.5  .    .     . )   ( 1.5 1.5  0    0  )
    // ( . 2 0 2 )    ( .    0.5  .     . )   ( 4.5  1   0    4  )
    // ( . . 1 0 ) x -( .    .    0.25  . ) = (  0   0  0.25  0  )
    // ( . . . 2 )    ( .    .    .     2 )   (  0   1   0    4  )

    Spmat SM11({0, 0, 1, 2, 1, 3}, {0, 1, 3, 4, 6},
               {1.0, 3.0, 2.0, 1.0, 2.0, 2.0});
    SM11.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
    Diag D({1.5, 0.5, 0.25, 2});
    SM11 = SM11 * D;
    const Spmat SM11D_check({0, 1, 0, 1, 3, 2, 1, 3}, {0, 2, 5, 6, 8},
                            {1.5, 4.5, 1.5, 1, 1, 0.25, 4, 4});
    REQUIRE(SM11 == SM11D_check);
  }

  SECTION("Sparse matrix times dense matrix") {
    //                 ( 1  2)
    //                 ( 2  3)
    // ( 1 0 -3 4 ) x  (-2 -4) = (11 34)
    // ( 0 2  0 4 )    ( 1  5)   (8  26)

    Dmat D1({1.0, 2.0, -2.0, 1.0, 2.0, 3.0, -4.0, 5.0}, 4, 2);
    Dmat CSCM2timesD1 = CSCM2 * D1;
    Dmat D1_check({11.0, 8.0, 34.0, 26.0}, 2, 2);

    REQUIRE(CSCM2timesD1 == D1_check);
  }

  SECTION("Sparse matrix with transposition times dense matrix") {
    //                              (-2 -4)
    //                              ( 2 -8)
    // ( 1 0 -3 4 ) ^T x  (-2 -4) = ( 6 12)
    // ( 0 2  0 4 )       ( 1  5)   (-4  4)

    Dmat D1({-2, 1, -4, 5}, 2, 2);
    Dmat CSCM2timesD1 = CSCM2.t() * D1;
    Dmat D1_check({-2, 2, 6, -4, -4, 10, 12, 4}, 4, 2);

    REQUIRE(CSCM2timesD1 == D1_check);
  }

  SECTION("COO conversion") {
    Spmat csc1 = CSCM;
    SparseMatrixCOO<Scalar> coo1 = csc1.to_coo();
    Spmat csc2;
    csc2.from_coo(coo1);
    REQUIRE(csc1 == csc2);

    // Central block matrix
    // 0  0  0  0  0  0
    // 0  0  0  0  0  0
    // 0  0  1  3  0  0
    // 0  0  2  4  0  0
    // 0  0  0  0  0  0
    // 0  0  0  0  0  0
    SparseMatrixCOO<Scalar> coo2({2, 2, 3, 3}, {2, 3, 2, 3}, {1, 3, 2, 4}, 6,
                                 6);
    Spmat csc3;
    csc3.from_coo(coo2);
    Spmat csc_exp({2, 3, 2, 3}, {0, 0, 0, 2, 4, 4, 4}, {1, 2, 3, 4}, 6, 6);
    REQUIRE(csc3 == csc_exp);
  }

  SECTION("Diagonal extraction") {
    //          ( 1 4 7 )    1
    // diagonal ( 2 5 8 )  = 5
    //          ( 3 6 9 )    9

    Spmat M({0, 1, 2, 0, 1, 2, 0, 1, 2}, {0, 3, 6, 9},
            {1, 2, 3, 4, 5, 6, 7, 8, 9});
    Vect d = diagonal_as_vector(M);
    Vect d_expected{1, 5, 9};

    REQUIRE(d == d_expected);
  }

  SECTION("Accessor") {
    // ( 0  1  1  0)
    // ( 0  0  0  2)
    REQUIRE(CSCM.coeff(0, 0) == Scalar{0});
    REQUIRE(CSCM.coeff(0, 1) == Scalar{1});
    REQUIRE(CSCM.coeff(0, 2) == Scalar{1});
    REQUIRE(CSCM.coeff(0, 3) == Scalar{0});
    REQUIRE(CSCM.coeff(1, 0) == Scalar{0});
    REQUIRE(CSCM.coeff(1, 1) == Scalar{0});
    REQUIRE(CSCM.coeff(1, 2) == Scalar{0});
    REQUIRE(CSCM.coeff(1, 3) == Scalar{2});
  }

  SECTION("Transpose") {
    Spmat orig = CSCM;
    Spmat transposed = adjoint(orig);
    const Spmat CSCM_transposed({1, 2, 3}, {0, 2, 3}, {1.0, 1.0, 2.0}, 4, 2);
    REQUIRE(transposed == CSCM_transposed);
  }

  SECTION("Test with integer 64: CSC x Dense mat") {
    const SparseMatrixCSC<Scalar, long int> M({0, 1, 0, 0, 1}, {0, 1, 2, 3, 5},
                                              {1.0, 2.0, -3.0, 4.0, 4.0});

    Dmat D1({1.0, 2.0, -2.0, 1.0, 2.0, 3.0, -4.0, 5.0}, 4, 2);
    Dmat MtimesD1 = M * D1;
    Dmat D1_check({11.0, 8.0, 34.0, 26.0}, 2, 2);

    REQUIRE(MtimesD1 == D1_check);
  }

  const Spmat second_term({1, 0, 1, 1}, {0, 1, 1, 3, 4}, {-1, 1, 2, -2}, 2, 4);
  SECTION("Addition spm + spm") {
    // ( 0  1  1  0)    ( 0  0  1  0 )   ( 0  1  2  0 )
    // ( 0  0  0  2) +  (-1  0  2 -2 ) = (-1  0  2  0 )
    const Spmat orig = CSCM;
    const Spmat expected({1, 0, 0, 1}, {0, 1, 2, 4, 4}, {-1, 1, 2, 2}, 2, 4);

    Spmat found = orig + second_term;
    REQUIRE(found == expected);
  }

  SECTION("Substraction spm - spm") {
    // ( 0  1  1  0)    ( 0  0  1  0 )   ( 0  1  0  0 )
    // ( 0  0  0  2) -  (-1  0  2 -2 ) = ( 1  0 -2  4 )
    const Spmat orig = CSCM;
    const Spmat expected({1, 0, 1, 1}, {0, 1, 2, 3, 4}, {1, 1, -2, 4}, 2, 4);

    Spmat found = orig - second_term;
    REQUIRE(found == expected);
  }

  SECTION("Multiplication spm x spm") {
    // ( 1 0 2 ) ( 1  -2 0 )     (-9  10  14 )
    // ( 0 3 0 ) ( 3  -4 0 )  =  ( 9 -12   0 )
    // ( 4 5 0 ) (-5   6 7 )     (19 -28   0 )
    const Spmat lmat({0, 2, 1, 2, 0}, {0, 2, 4, 5}, {1, 4, 3, 5, 2}, 3, 3);
    const Spmat rmat({0, 1, 2, 0, 1, 2, 2}, {0, 3, 6, 7},
                     {1, 3, -5, -2, -4, 6, 7}, 3, 3);
    const Spmat expected({0, 1, 2, 0, 1, 2, 0}, {0, 3, 6, 7},
                         {-9, 9, 19, 10, -12, -28, 14}, 3, 3);

    Spmat found = lmat * rmat;
    REQUIRE(found == expected);
  }

  SECTION("Multiplication spm (half) x spm") {
    // ( 1 - - ) ( 1  -2 0 )     (-19  22  28 )
    // ( 0 3 - ) ( 3  -4 0 )  =  (-16  18  35 )
    // ( 4 5 2 ) (-5   6 7 )     ( 9  -16  14 )
    Spmat lmat({0, 2, 1, 2, 2}, {0, 2, 4, 5}, {1, 4, 3, 5, 2}, 3, 3);
    lmat.set_property(MatrixSymmetry::symmetric, MatrixStorage::lower);
    const Spmat rmat({0, 1, 2, 0, 1, 2, 2}, {0, 3, 6, 7},
                     {1, 3, -5, -2, -4, 6, 7}, 3, 3);
    const Spmat expected({0, 1, 2, 0, 1, 2, 0, 1, 2}, {0, 3, 6, 9},
                         {-19, -16, 9, 22, 18, -16, 28, 35, 14}, 3, 3);

    Spmat found = lmat * rmat;
    REQUIRE(found == expected);
  }

  SECTION("Multiplication spm x spm (half)") {
    //  ( 1  -2 0 ) ( 1 - - )     ( 1   -6  -6 )
    //  ( 3  -4 0 ) ( 0 3 - )  =  ( 3  -12  -8 )
    //  (-5   6 7 ) ( 4 5 2 )     (23   53   24)
    const Spmat lmat({0, 1, 2, 0, 1, 2, 2}, {0, 3, 6, 7},
                     {1, 3, -5, -2, -4, 6, 7}, 3, 3);
    Spmat rmat({0, 2, 1, 2, 2}, {0, 2, 4, 5}, {1, 4, 3, 5, 2}, 3, 3);
    rmat.set_property(MatrixSymmetry::symmetric, MatrixStorage::lower);
    const Spmat expected({0, 1, 2, 0, 1, 2, 0, 1, 2}, {0, 3, 6, 9},
                         {1, 3, 23, -6, -12, 53, -6, -8, 24}, 3, 3);

    Spmat found = lmat * rmat;
    REQUIRE(found == expected);
  }

  SECTION("Multiplication spm (half) x spm (half)") {
    //  ( 1  3  0 ) ( 1 - - )     ( 1   9  19 )
    //  ( - -4  5 ) ( 0 3 - )  =  (23  13   2 )
    //  ( -  -  7 ) ( 4 5 2 )     (28  50  39)
    Spmat lmat({0, 0, 1, 1, 2}, {0, 1, 3, 5}, {1, 3, -4, 5, 7}, 3, 3);
    lmat.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
    Spmat rmat({0, 2, 1, 2, 2}, {0, 2, 4, 5}, {1, 4, 3, 5, 2}, 3, 3);
    rmat.set_property(MatrixSymmetry::symmetric, MatrixStorage::lower);
    const Spmat expected({0, 1, 2, 0, 1, 2, 0, 1, 2}, {0, 3, 6, 9},
                         {1, 23, 28, 9, 13, 50, 19, 2, 39}, 3, 3);

    Spmat found = lmat * rmat;
    REQUIRE(found == expected);
  }

  SECTION("Serialization / deserialization") {
    const Spmat M = CSCM;
    std::vector<char> buf = M.serialize();
    Spmat O;
    O.deserialize(buf);
    REQUIRE(O == M);
  }

  SECTION("Constructor with CSR format") {
    //  ( 1  -2 0 )
    //  ( 3  -4 0 )
    //  (-5   6 7 )
    const Spmat lmat({0, 1, 2, 0, 1, 2, 2}, {0, 3, 6, 7},
                     {1, 3, -5, -2, -4, 6, 7}, 3, 3);
    const Spmat csrmat = CSC_from_CSR<Scalar, int>(
        {0, 2, 4, 7}, {0, 1, 0, 1, 0, 1, 2}, {1, -2, 3, -4, -5, 6, 7}, 3, 3);

    REQUIRE(lmat == csrmat);

    const std::vector<int> csr_i{0, 2, 4, 7};
    const std::vector<int> csr_j{0, 1, 0, 1, 0, 1, 2};
    const std::vector<Scalar> csr_v{1, -2, 3, -4, -5, 6, 7};

    const Spmat csrmat2 = CSC_from_CSR<Scalar, int>(csr_i, csr_j, csr_v, 3, 3);
    REQUIRE(lmat == csrmat2);
  }
} // TEMPLATE_TEST_CASE
// Tests:1 ends here
