// [[file:../../../org/composyx/utils/SZ_compressor.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>
#include <map>

#include <random>
#include <composyx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include "composyx/utils/SZ_compressor.hpp"

TEMPLATE_TEST_CASE("SZ_compressor", "[compressor]", float, double,
                   std::complex<float>, std::complex<double>) {
  using namespace composyx;

  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;

  std::random_device rd;
  std::mt19937 gen(rd());
  Real min = -10;
  Real max = 10;
  std::uniform_real_distribution<> dis(min, max);
  auto random = [&gen, &dis]() { return static_cast<Real>(dis(gen)); };

  const size_t n_elts = 1000;
  const double zeta = double{1e-4};

  std::cout << "Vector of size: " << n_elts << ", zeta = " << zeta << '\n';

  auto generate_elts = [random](int n) {
    std::vector<Scalar> cpr(n);
    for (size_t i = 0; i < cpr.size(); ++i) {
      if constexpr (is_complex<Scalar>::value) {
        cpr[i] = Scalar{random(), random()};
      } else {
        cpr[i] = Scalar{random()};
      }
    }
    return cpr;
  };

  std::vector<Scalar> to_compress = generate_elts(n_elts);

  SZ_compressor_init();

  SECTION("Compress/Decompress std::vector pointwise") {
    SZ_compressor<Scalar, SZ_CompressionMode::POINTWISE> compressor(to_compress,
                                                                    zeta);
    double ratio = compressor.get_ratio();
    std::cout << "Compression ratio (" << arithmetic_name<Scalar>::name
              << "): " << ratio << '\n';
    REQUIRE(ratio > double{1});

    std::vector<Scalar> decompressed = compressor.decompress();
    REQUIRE(decompressed.size() == n_elts);

    std::vector<Scalar> decompressed_2 = compressor.decompress();
    REQUIRE(decompressed_2 == decompressed);
  }

  SECTION("Compress/Decompress std::vector normwise") {
    SZ_compressor<Scalar, SZ_CompressionMode::NORMWISE> compressor(to_compress,
                                                                   zeta);
    double ratio = compressor.get_ratio();
    std::cout << "Compression ratio (" << arithmetic_name<Scalar>::name
              << "): " << ratio << '\n';
    REQUIRE(ratio > double{1});

    std::vector<Scalar> decompressed = compressor.decompress();
    REQUIRE(decompressed.size() == n_elts);

    std::vector<Scalar> decompressed_2 = compressor.decompress();
    REQUIRE(decompressed_2 == decompressed);
  }

  SECTION("Compress/Decompress with pointers") {
    SZ_compressor<Scalar, SZ_CompressionMode::POINTWISE> compressor(
        to_compress.data(), to_compress.size(), zeta);
    double ratio = compressor.get_ratio();
    std::cout << "Compression ratio (" << arithmetic_name<Scalar>::name
              << "): " << ratio << '\n';
    REQUIRE(ratio > double{1});

    std::vector<Scalar> decompressed(compressor.get_n_elts());
    compressor.decompress(decompressed.data());
    REQUIRE(decompressed.size() == n_elts);
  }

  SECTION("Test with small size (no compression)") {
    std::vector<Scalar> small = generate_elts(10);

    SZ_compressor<Scalar, SZ_CompressionMode::POINTWISE> compressor(
        small.data(), small.size(), zeta);
    double ratio = compressor.get_ratio();
    std::cout << "Compression ratio (" << arithmetic_name<Scalar>::name
              << "): " << ratio << '\n';
    REQUIRE(ratio == double{1});

    std::vector<Scalar> decompressed(compressor.get_n_elts());
    compressor.decompress(decompressed.data());
    REQUIRE(decompressed == small);
  }

  SZ_compressor_finalize();
}
// Tests:1 ends here
