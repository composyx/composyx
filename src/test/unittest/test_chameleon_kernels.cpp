// [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Tests][Tests:1]]
#include <composyx.hpp>
#include <composyx/kernel/ChameleonKernels.hpp>
#include <composyx/loc_data/DenseMatrix.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <composyx/testing/Catch2DenseMatrixMatchers.hpp>

using namespace composyx;

TEMPLATE_TEST_CASE("ChameleonKernels", "[dense][sequential][chameleon][kernel]",
                   float, double, std::complex<float>, std::complex<double>) {
  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;
  //using Complex = typename arithmetic_complex<Scalar>::type;
  using EqualsMatrixPW = composyx::EqualsMatrixPW<DenseMatrix<Scalar>>;
  constexpr const Real ev_tol =
      (is_precision_double<Scalar>::value) ? 1e-12 : 1e-5;

  // initialize chameleon context
  // optional: set number of workers used by Chameleon (ncpus, ngpus and tile size)
  initialize(2, 0);
  setChameleonParameters(2);
  // Tests:1 ends here

  // [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Blas 3][Blas 3:1]]
  SECTION("Blas 3 - GEMM - general") {
    const DenseMatrix<Scalar> A({1.0, -1.0, 2.0, -2.0, 3.0, -3.0}, 2, 3);
    const DenseMatrix<Scalar> B({1.0, 2.0, 3.0, 4.0, 5.0, 6.0}, 3, 2);

    //            1 4
    //  1  2  3 x 2 5 =  14  32
    // -1 -2 -3   3 6   -14 -32

    const DenseMatrix<Scalar> C_check({14.0, -14.0, 32.0, -32.0}, 2, 2);
    DenseMatrix<Scalar> C(composyx::n_rows(A), composyx::n_cols(B));

    chameleon_kernels::gemm(A, B, C);
    REQUIRE_THAT(C_check, EqualsMatrixPW(C));
  }

  SECTION("Blas 3 - GEMM - symmetric / hermitian") {
    DenseMatrix<Scalar> M({1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 3);
    DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
    L.set_property(composyx::MatrixStorage::lower,
                   composyx::MatrixSymmetry::symmetric);

    DenseMatrix<Scalar> ML({-14, -16, -18, 5, 7, 9, 36, 39, 42}, 3, 3);
    DenseMatrix<Scalar> LM({-6, 3, 14, -12, 9, 23, -18, 15, 32}, 3, 3);
    if constexpr (composyx::is_complex<Scalar>::value) {
      M += Scalar{0, 1} *
           DenseMatrix<Scalar>({1, -1, 1, -1, 2, -2, 1, -1, -0.5}, 3, 3);
      L += Scalar{0, 1} *
           DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
      L.set_property(composyx::MatrixStorage::lower,
                     composyx::MatrixSymmetry::hermitian);
      ML = DenseMatrix<Scalar>({{-16, -3},
                                {-13, 11},
                                {-19.5, -0.5},
                                {6, -9},
                                {6, -19},
                                {7, -11.5},
                                {39, 8},
                                {34, 8},
                                {47, 1.5}},
                               3, 3);
      LM = DenseMatrix<Scalar>({{-4, -7},
                                {2, 9},
                                {11, -2},
                                {-16, 10},
                                {12, 2},
                                {28, -11},
                                {-17.5, -2.5},
                                {17, 13.5},
                                {29, -15.5}},
                               3, 3);
    }

    DenseMatrix<Scalar> my_ML(3, 3);
    chameleon_kernels::gemm(M, L, my_ML);
    REQUIRE_THAT(ML, EqualsMatrixPW(my_ML));

    DenseMatrix<Scalar> my_LM(3, 3);
    chameleon_kernels::gemm(L, M, my_LM);
    REQUIRE_THAT(LM, EqualsMatrixPW(my_LM));
  }
  // Blas 3:1 ends here

  // [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Randomized singular value decomposition][Randomized singular value decomposition:1]]
  SECTION("Randomized Singular value decomposition") {
    const size_t m = 10;
    const size_t n = 8;
    const size_t r = 3;

    // generate a (m, n) rank-r matrix
    // matrix (m, r) x (r, n)
    auto make_rank_r_mat = [=](int seed) {
      DenseMatrix<Scalar> mat_l(m, r);
      test_matrix::fill_random(mat_l, seed);
      DenseMatrix<Scalar> mat_r(r, n);
      test_matrix::fill_random(mat_r, seed + 1234);
      DenseMatrix<Scalar> mat = mat_l * mat_r;
      return mat;
    };

    const DenseMatrix<Scalar> A = make_rank_r_mat(96348);

    auto check_svd = [&A](const DenseMatrix<Scalar>& l_U,
                          const std::vector<Real>& l_sigmas,
                          const DenseMatrix<Scalar>& l_Vt) {
      // Construct the diagonal matrix Sigma for checking
      DenseMatrix<Scalar> Sigma(n_cols(l_U), n_rows(l_Vt));
      auto l_k = std::min(n_cols(l_U), n_rows(l_Vt));
      for (size_t i = 0; i < l_k; ++i)
        Sigma(i, i) = Scalar{l_sigmas[i]};

      DenseMatrix<Scalar> Acheck = l_U * Sigma * l_Vt;
      DenseMatrix<Scalar> DiffA = A - Acheck;
      //DiffA.display("DiffA");
      //std::cout << " my_ev_tol = " << my_ev_tol  << std::endl;
      //std::cout << "Norm DiffA = " << DiffA.norm() << std::endl;
      return (DiffA.norm() < ev_tol * A.norm());
    };

    auto check_singvals = [](const std::vector<Real>& s1,
                             const std::vector<Real>& s2) {
      if (s1.size() != s2.size())
        return false;
      Real normvec = 0;
      for (size_t i = 0; i < s1.size(); ++i) {
        normvec += s1[i] * s1[i];
      }
      normvec = sqrt(normvec);
      for (size_t i = 0; i < s1.size(); ++i) {
        if (std::abs(s1[i] - s2[i]) > ev_tol * normvec)
          return false;
      }
      return true;
    };

    // using direct svd method
    auto [U, sigmas, Vt] = chameleon_kernels::rsvd(A, r);
    REQUIRE(n_rows(U) == m);
    REQUIRE(n_cols(U) == r);
    REQUIRE(n_rows(Vt) == r);
    REQUIRE(n_cols(Vt) == n);
    REQUIRE(check_svd(U, sigmas, Vt));

    // using 0 subspace iteration and two-stages svd method (direct=false)
    auto [U1, sigmas1, Vt1] = chameleon_kernels::rsvd(A, r, 0, false);
    REQUIRE(check_svd(U1, sigmas1, Vt1));

    // only left singular vectors U
    auto [U2, sigmas2] = chameleon_kernels::rsvd_left_sv(A, r);
    REQUIRE(n_rows(U2) == m);
    REQUIRE(n_cols(U2) == r);
    REQUIRE(check_singvals(sigmas2, sigmas));

    // only right singular vectors Vt
    auto [sigmas3, Vt3] = chameleon_kernels::rsvd_right_sv(A, r);
    REQUIRE(n_rows(Vt3) == r);
    REQUIRE(n_cols(Vt3) == n);
    REQUIRE(check_singvals(sigmas3, sigmas));

    // Only singular values
    auto sigmas4 = chameleon_kernels::rsvdvals(A, r);
    REQUIRE(check_singvals(sigmas4, sigmas));
  }
  // Randomized singular value decomposition:1 ends here

  // [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Randomized eigen value decomposition][Randomized eigen value decomposition:1]]
  SECTION("Randomized Eigen value decomposition") {
    const size_t n = 10;
    const size_t r = 3;

    auto make_rank_r_hpd_mat = [=](int seed) {
      DenseMatrix<Scalar> mat(n, r);
      test_matrix::fill_random(mat, seed);
      for (size_t k = 0; k < std::min(n, r); ++k) {
        mat(k, k) += static_cast<Scalar>(n);
      }
      mat *= mat.h();
      mat.set_spd(MatrixStorage::full);
      if constexpr (is_complex<Scalar>::value) {
        mat.set_hpd(MatrixStorage::full);
      }
      return mat;
    };

    // generate a rank-K matrix symmetric/hermitian
    const DenseMatrix<Scalar> A = make_rank_r_hpd_mat(5372);

    Real scal_norm = A.norm();
    Real my_ev_tol = ev_tol * scal_norm;

    auto check_evd = [&A, my_ev_tol](const std::vector<Real>& l_lambdas,
                                     const DenseMatrix<Scalar>& l_V) {
      bool success_evd = true;
      for (size_t i = 0; i < n_cols(l_V); ++i) {
        Vector<Scalar> AV = A * l_V.get_vect_view(i);
        Vector<Scalar> lamV;
        if constexpr (composyx::is_complex<Scalar>::value) {
          Scalar cplx_lam = Scalar{l_lambdas[i], 0};
          lamV = cplx_lam * l_V.get_vect_view(i);
        } else {
          lamV = l_lambdas[i] * l_V.get_vect_view(i);
        }
        Vector<Scalar> DiffA = AV - lamV;
        //DiffA.display("DiffA");
        //std::cout << "Norm diff " << i << " = " << DiffA.norm() << std::endl;
        if (DiffA.norm() > my_ev_tol)
          success_evd = false;
      }
      return success_evd;
    };

    auto check_eigvals = [](const std::vector<Real>& e1,
                            const std::vector<Real>& e2) {
      if (e1.size() != e2.size())
        return false;
      Real normvec = 0;
      for (size_t i = 0; i < e1.size(); ++i) {
        normvec += e1[i] * e1[i];
      }
      normvec = sqrt(normvec);
      for (size_t i = 0; i < e1.size(); ++i) {
        if (std::abs(e1[i] - e2[i]) > ev_tol * normvec)
          return false;
      }
      return true;
    };

    // using direct evd method
    auto [lambdas, V] = chameleon_kernels::revd(A, r);
    REQUIRE(n_rows(V) == n);
    REQUIRE(n_cols(V) == r);
    REQUIRE(check_evd(lambdas, V));

    // using 0 subspace iteration
    auto [lambdas1, V1] = chameleon_kernels::revd(A, r, 0);
    REQUIRE(check_evd(lambdas1, V1));

    // Only eigen values
    auto lambdas2 = chameleon_kernels::revdvals(A, r);
    REQUIRE(check_eigvals(lambdas2, lambdas));
  }
  // Randomized eigen value decomposition:1 ends here

  // [[file:../../../org/composyx/kernel/ChameleonKernels.org::*Footer][Footer:1]]
  finalize();
}
// Footer:1 ends here
