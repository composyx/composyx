// [[file:../../../org/composyx/linalg/EigenArpackSolver.org::*Tests][Tests:1]]
#include <iostream>
#include <composyx.hpp>
#include <composyx/loc_data/DenseMatrix.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <composyx/solver/BlasSolver.hpp>
#include <composyx/solver/Pastix.hpp>
#include <composyx/linalg/EigenArpackSolver.hpp>

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

TEMPLATE_TEST_CASE("EigenSolver", "[eigen_problem][arpack]", float, double) {
  using namespace composyx;

  using Scalar = TestType;
  using Real = typename composyx::arithmetic_real<Scalar>::type;
  using DenseMat = DenseMatrix<Scalar>;
  using SparseMat = SparseMatrixCOO<Scalar>;
  using SolverSparse = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
  using SolverDense = BlasSolver<DenseMat, Vector<Scalar>>;

  const int M = 100;
  const int n_v = 5;

  // We want to find U so that A U = LAM B U
  // With U the n_v eigenvectors associated to the smallest eigenvalues
  auto build_mat = []() {
    DenseMat mat = test_matrix::random_matrix<Scalar, DenseMat>(3 * M, Real{-1},
                                                                Real{1}, M, M)
                       .matrix;
    for (int i = 0; i < M; ++i)
      mat(i, i) += (5 + i);
    mat *= mat.t();
    for (int j = 1; j < M; ++j) {
      for (int i = 0; i < j; ++i) {
        mat(i, j) = 0;
      }
    }
    mat.set_spd(MatrixStorage::lower);
    return mat;
  };

  Real tol = is_precision_double<Scalar>::value ? 1e-12 : 1e-5;

  SECTION("Test with A and B dense") {
    std::cout << "---dense---\n";
    const DenseMat A = build_mat();
    const DenseMat B = build_mat();

    auto [lambda, U] =
        EigenArpackSolver<DenseMat, DenseMat, Vector<Scalar>,
                          SolverDense>::gen_eigh_smallest(A, B, n_v);

    // In case A or B is too big, we multiply tolerance by (norm of A + norm of B) (Frobenius norm)
    Real scal_norm = A.norm() + B.norm();
    tol *= scal_norm;

    Real prev_lambda = Real{0};
    for (int j = 0; j < n_v; ++j) {
      // Check lambdas are in asending order
      REQUIRE(lambda[j] > prev_lambda);
      prev_lambda = lambda[j];
      Vector<Scalar> Uj = U.get_vect_view(j);
      REQUIRE(Uj.norm() > 0);
      Vector<Scalar> A_u = A * Uj;
      Vector<Scalar> l_B_u = static_cast<Scalar>(lambda[j]) * (B * Uj);

      std::cout << "Lambda : " << lambda[j] << '\n';
      Vector<Scalar> diff = A_u - l_B_u;
      std::cout << "Diff norm: " << diff.norm() << '\n';
      REQUIRE(diff.norm() < tol);
    }
  }

  SECTION("Test with A and B sparse") {
    std::cout << "---sparse---\n";
    SparseMat A = build_mat();
    SparseMat B = build_mat();
    A.to_storage_half(MatrixStorage::lower);
    B.to_storage_half(MatrixStorage::lower);

    auto [lambda, U] =
        EigenArpackSolver<DenseMat, SparseMat, Vector<Scalar>,
                          SolverSparse>::gen_eigh_smallest(A, B, n_v);

    // In case A or B is too big, we multiply tolerance by (norm of A + norm of B) (Frobenius norm)
    Real scal_norm = A.norm() + B.norm();
    tol *= scal_norm;

    Real prev_lambda = Real{0};
    for (int j = 0; j < n_v; ++j) {
      // Check lambdas are in asending order
      REQUIRE(lambda[j] > prev_lambda);
      prev_lambda = lambda[j];
      Vector<Scalar> Uj = U.get_vect_view(j);
      REQUIRE(Uj.norm() > 0);
      Vector<Scalar> A_u = A * Uj;
      Vector<Scalar> l_B_u = static_cast<Scalar>(lambda[j]) * (B * Uj);

      std::cout << "Lambda : " << lambda[j] << '\n';
      Vector<Scalar> diff = A_u - l_B_u;
      std::cout << "Diff norm: " << diff.norm() << '\n';
      REQUIRE(diff.norm() < tol);
    }
  }
}
// Tests:1 ends here
