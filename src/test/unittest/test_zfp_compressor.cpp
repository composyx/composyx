// [[file:../../../org/composyx/utils/ZFP_compressor.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>
#include <map>

#include <random>
#include <composyx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <composyx/utils/ZFP_compressor.hpp>

TEMPLATE_TEST_CASE("ZFP_compressor", "[compressor]", float, double,
                   std::complex<float>, std::complex<double>) {
  using namespace composyx;

  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;

  std::random_device rd;
  std::mt19937 gen(rd());
  Real min = -10;
  Real max = 10;
  std::uniform_real_distribution<> dis(min, max);
  auto random = [&gen, &dis]() { return static_cast<Real>(dis(gen)); };

  const size_t n_elts = 1000;
  const double zeta = double{1e-4};
  const unsigned int precision = 28;
  const unsigned int rate = 22;

  std::cout << "Vector of size: " << n_elts << '\n';

  auto generate_elts = [random](int n) {
    std::vector<Scalar> cpr(n);
    for (size_t i = 0; i < cpr.size(); ++i) {
      if constexpr (is_complex<Scalar>::value) {
        cpr[i] = Scalar{random(), random()};
      } else {
        cpr[i] = Scalar{random()};
      }
    }
    return cpr;
  };

  std::vector<Scalar> to_compress = generate_elts(n_elts);

  SECTION("Compress/Decompress std::vector") {
    ZFP_compressor<Scalar, ZFP_CompressionMode::ACCURACY> compressor(
        to_compress, zeta);
    double ratio = compressor.get_ratio();
    std::cout << "Accuracy: " << zeta << '\n';
    std::cout << "Compression ratio (" << arithmetic_name<Scalar>::name
              << "): " << ratio << '\n';
    REQUIRE(ratio > double{1});

    std::vector<Scalar> decompressed = compressor.decompress();
    REQUIRE(decompressed.size() == n_elts);

    std::vector<Scalar> decompressed_2 = compressor.decompress();
    REQUIRE(decompressed_2 == decompressed);
  }

  SECTION("Compress/Decompress with pointers") {
    ZFP_compressor<Scalar, ZFP_CompressionMode::ACCURACY> compressor(
        to_compress.data(), to_compress.size(), zeta);
    double ratio = compressor.get_ratio();
    std::cout << "Accuracy: " << zeta << '\n';
    std::cout << "Compression ratio (" << arithmetic_name<Scalar>::name
              << "): " << ratio << '\n';
    REQUIRE(ratio > double{1});

    std::vector<Scalar> decompressed(compressor.get_n_elts());
    compressor.decompress(decompressed.data());
    REQUIRE(decompressed.size() == n_elts);
  }

  SECTION("Compress/Decompress with fixed rate") {
    ZFP_compressor<Scalar, ZFP_CompressionMode::RATE> compressor;
    compressor.compress(to_compress, rate);
    double ratio = compressor.get_ratio();
    std::cout << "Rate: " << rate << '\n';
    std::cout << "Compression ratio (" << arithmetic_name<Scalar>::name
              << "): " << ratio << '\n';
    REQUIRE(ratio > double{1});

    std::vector<Scalar> decompressed = compressor.decompress();
    REQUIRE(decompressed.size() == n_elts);

    std::vector<Scalar> decompressed_2 = compressor.decompress();
    REQUIRE(decompressed_2 == decompressed);
  }

  SECTION("Compress/Decompress with fixed precision") {
    ZFP_compressor<Scalar, ZFP_CompressionMode::PRECISION> compressor;
    compressor.compress(to_compress, precision);
    double ratio = compressor.get_ratio();
    std::cout << "Precision: " << precision << '\n';
    std::cout << "Compression ratio (" << arithmetic_name<Scalar>::name
              << "): " << ratio << '\n';
    REQUIRE(ratio > double{1});

    std::vector<Scalar> decompressed = compressor.decompress();
    REQUIRE(decompressed.size() == n_elts);

    std::vector<Scalar> decompressed_2 = compressor.decompress();
    REQUIRE(decompressed_2 == decompressed);
  }
}
// Tests:1 ends here
