// [[file:../../../org/composyx/kernel/TlapackKernels.org::*Tests][Tests:1]]
#include <composyx.hpp>
#include <composyx/kernel/TlapackKernels.hpp>
#include <composyx/loc_data/DenseMatrix.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <composyx/testing/Catch2DenseMatrixMatchers.hpp>

using namespace composyx;

TEMPLATE_TEST_CASE("TlapackKernels", "[dense][sequential][tlapack][kernel]",
                   float, double, std::complex<float>, std::complex<double>) {
  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;
  //using Complex = typename std::conditional<is_complex<Scalar>::value, Scalar, std::complex<Real>>::type;

  using EqualsVectorPW = composyx::EqualsVectorPW<Vector<Scalar>>;
  using EqualsMatrixPW = composyx::EqualsMatrixPW<DenseMatrix<Scalar>>;

  constexpr const double blas_tol =
      (is_precision_double<Scalar>::value) ? 1e-15 : 1e-6;
  constexpr const double ev_tol =
      (is_precision_double<Scalar>::value) ? 1e-12 : 1e-5;

  auto equals_fp = [](auto s1, auto s2, double tol) {
    return std::abs(s1 - s2) < tol;
  };
  // Tests:1 ends here

  // [[file:../../../org/composyx/kernel/TlapackKernels.org::*Blas 1][Blas 1:1]]
  SECTION("Blas 1 - SCAL") {
    const Vector<Scalar> x({1, -1, 3});
    const DenseMatrix<Scalar> mat({1, 2, 3, 4}, 2, 2);

    // Scal (*2)
    const Vector<Scalar> x_scal({2, -2, 6});

    Vector<Scalar> x2(x);
    tlapack_kernels::scal(x2, Scalar{2});
    REQUIRE_THAT(x2, EqualsVectorPW(x_scal));

    // Test with increment
    DenseMatrix<Scalar> mat2(mat);
    Vector<Scalar> y = mat2.get_row_view(0);

    const Vector<Scalar> y_scal({2, 6});
    tlapack_kernels::scal(y, Scalar{2});

    REQUIRE_THAT(y, EqualsVectorPW(y_scal));
  }

  SECTION("Blas 1 - AXPY") {
    const Vector<Scalar> x({1, -1, 3});

    // AXPY (*2 + (-1,-1,-1)T)
    Vector<Scalar> y({-1, -1, -1});
    const Vector<Scalar> x2py({1, -3, 5});
    tlapack_kernels::axpy(x, y, Scalar{2});
    REQUIRE_THAT(y, EqualsVectorPW(x2py));
  }

  // dot
  SECTION("Blas 1 - DOT") {
    const Vector<Scalar> x({1, -1, 3});

    const Vector<Scalar> y({2, -4, -1});
    const Scalar xdoty{3};
    REQUIRE(equals_fp(tlapack_kernels::dot(x, y), xdoty, blas_tol));
  }

  // norm2
  SECTION("Blas 1 - NORM2") {
    const Vector<Scalar> x({1, -1, 3});

    const Real normx_sq = 11;
    const Real normx = std::sqrt(normx_sq);
    REQUIRE(equals_fp(tlapack_kernels::norm2_squared(x), normx_sq, blas_tol));
    REQUIRE(equals_fp(tlapack_kernels::norm2(x), normx, blas_tol));
  }
  // Blas 1:1 ends here

  // [[file:../../../org/composyx/kernel/TlapackKernels.org::*Blas 2][Blas 2:1]]
  SECTION("Blas 2 -GEMV") {
    //static void gemv(const Matrix& A, const Vector& X, Vector& Y, Scalar alpha = 1, Scalar beta = 0)

    // 1 4        3
    // 2 5 x -1 = 3
    // 3 6    1   3
    const DenseMatrix<Scalar> MA({1, 2, 3, 4, 5, 6}, 3, 2);
    const Vector<Scalar> x({-1, 1});
    const Vector<Scalar> sol_check({3, 3, 3});
    Vector<Scalar> y(composyx::n_rows(MA));

    tlapack_kernels::gemv(MA, x, y);

    REQUIRE_THAT(sol_check, EqualsVectorPW(y));
  }

  SECTION("Blas 2 - Gemv sym/herm") {
    Vector<Scalar> X({3, 2, 1});
    DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
    L.set_property(composyx::MatrixStorage::lower,
                   composyx::MatrixSymmetry::symmetric);

    Vector<Scalar> LX({-2, 5, -2});
    if constexpr (composyx::is_complex<Scalar>::value) {
      X += Scalar{0, 1} * Vector<Scalar>({1, 2, -1});
      L += Scalar{0, 1} *
           DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
      L.set_property(composyx::MatrixStorage::lower,
                     composyx::MatrixSymmetry::hermitian);
      LX = Vector<Scalar>({{-5, 7}, {8, -2}, {1, -7}});
    }

    Vector<Scalar> my_LX(3);
    tlapack_kernels::gemv(L, X, my_LX);
    REQUIRE_THAT(my_LX, EqualsVectorPW(LX));
  }
  // Blas 2:1 ends here

  // [[file:../../../org/composyx/kernel/TlapackKernels.org::*Blas 3][Blas 3:1]]
  SECTION("Blas 3 - GEMM - general") {
    const DenseMatrix<Scalar> A({1.0, -1.0, 2.0, -2.0, 3.0, -3.0}, 2, 3);
    const DenseMatrix<Scalar> B({1.0, 2.0, 3.0, 4.0, 5.0, 6.0}, 3, 2);

    //            1 4
    //  1  2  3 x 2 5 =  14  32
    // -1 -2 -3   3 6   -14 -32

    const DenseMatrix<Scalar> C_check({14.0, -14.0, 32.0, -32.0}, 2, 2);
    DenseMatrix<Scalar> C(composyx::n_rows(A), composyx::n_cols(B));

    tlapack_kernels::gemm(A, B, C);
    REQUIRE_THAT(C, EqualsMatrixPW(C_check));
  }

  SECTION("Blas 3 - GEMM - symmetric / hermitian") {
    DenseMatrix<Scalar> M({1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 3);
    DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
    L.set_property(composyx::MatrixStorage::lower,
                   composyx::MatrixSymmetry::symmetric);

    DenseMatrix<Scalar> ML({-14, -16, -18, 5, 7, 9, 36, 39, 42}, 3, 3);
    DenseMatrix<Scalar> LM({-6, 3, 14, -12, 9, 23, -18, 15, 32}, 3, 3);
    if constexpr (composyx::is_complex<Scalar>::value) {
      M += Scalar{0, 1} *
           DenseMatrix<Scalar>({1, -1, 1, -1, 2, -2, 1, -1, -0.5}, 3, 3);
      L += Scalar{0, 1} *
           DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
      L.set_property(composyx::MatrixStorage::lower,
                     composyx::MatrixSymmetry::hermitian);
      ML = DenseMatrix<Scalar>({{-16, -3},
                                {-13, 11},
                                {-19.5, -0.5},
                                {6, -9},
                                {6, -19},
                                {7, -11.5},
                                {39, 8},
                                {34, 8},
                                {47, 1.5}},
                               3, 3);
      LM = DenseMatrix<Scalar>({{-4, -7},
                                {2, 9},
                                {11, -2},
                                {-16, 10},
                                {12, 2},
                                {28, -11},
                                {-17.5, -2.5},
                                {17, 13.5},
                                {29, -15.5}},
                               3, 3);
    }

    DenseMatrix<Scalar> my_ML(3, 3);
    tlapack_kernels::gemm(M, L, my_ML);
    REQUIRE_THAT(my_ML, EqualsMatrixPW(ML));

    DenseMatrix<Scalar> my_LM(3, 3);
    tlapack_kernels::gemm(L, M, my_LM);
    REQUIRE_THAT(my_LM, EqualsMatrixPW(LM));
  }
  // Blas 3:1 ends here

  // [[file:../../../org/composyx/kernel/TlapackKernels.org::*SVD][SVD:1]]
  SECTION("Singular value decomposition") {
    const size_t M = 3;
    const size_t N = 2;
    const size_t K = 2;

    DenseMatrix<Scalar> A({1, 2, 3, -4, -5, 6}, M, N);
    if constexpr (is_complex<Scalar>::value) {
      // Add some imaginary part for fun
      A += Scalar{0, 1} * DenseMatrix<Scalar>({1, -1, 2, 1, 0, 0.5}, M, N);
    }

    auto check_svd = [&A](const DenseMatrix<Scalar>& l_U,
                          const std::vector<Real>& l_sigmas,
                          const DenseMatrix<Scalar>& l_Vt) {
      // Construct the diagonal matrix Sigma for checking
      DenseMatrix<Scalar> Sigma(n_cols(l_U), n_rows(l_Vt));
      auto l_k = std::min(n_cols(l_U), n_rows(l_Vt));
      for (size_t i = 0; i < l_k; ++i)
        Sigma(i, i) = Scalar{l_sigmas[i]};
      DenseMatrix<Scalar> Acheck = l_U * Sigma * l_Vt;
      return ((A - Acheck).norm() < ev_tol);
    };

    auto check_singvals = [](const std::vector<Real>& s1,
                             const std::vector<Real>& s2) {
      if (s1.size() != s2.size())
        return false;
      for (size_t i = 0; i < s1.size(); ++i) {
        if (std::abs(s1[i] - s2[i]) > ev_tol)
          return false;
      }
      return true;
    };

    // Reduced svd
    const bool reduced = true;
    const bool full = false;

    auto [U, sigmas, Vt] = tlapack_kernels::svd(A, reduced);
    REQUIRE(n_rows(U) == M);
    REQUIRE(n_cols(U) == K);
    REQUIRE(n_rows(Vt) == K);
    REQUIRE(n_cols(Vt) == N);
    REQUIRE(check_svd(U, sigmas, Vt));

    // Full svd
    auto [U2, sigmas2, Vt2] = tlapack_kernels::svd(A, full);
    REQUIRE(n_rows(U2) == M);
    REQUIRE(n_cols(U2) == M);
    REQUIRE(n_rows(Vt2) == N);
    REQUIRE(n_cols(Vt2) == N);
    REQUIRE(check_singvals(sigmas2, sigmas));
    REQUIRE(check_svd(U2, sigmas2, Vt2));

    // Full, only left singular vectors U
    auto [U3, sigmas3] = tlapack_kernels::svd_left_sv(A, full);
    REQUIRE(n_rows(U3) == M);
    REQUIRE(n_cols(U3) == M);
    REQUIRE(check_singvals(sigmas3, sigmas));

    // Full, only right singular vectors Vt
    auto [sigmas4, Vt4] = tlapack_kernels::svd_right_sv(A, full);
    REQUIRE(n_rows(Vt4) == N);
    REQUIRE(n_cols(Vt4) == N);
    REQUIRE(check_singvals(sigmas4, sigmas));

    // Only singular values
    auto sigmas5 = tlapack_kernels::svdvals(A);
    REQUIRE(check_singvals(sigmas5, sigmas));
  }
  // SVD:1 ends here

  // [[file:../../../org/composyx/kernel/TlapackKernels.org::*Footer][Footer:1]]
}
// Footer:1 ends here
