// [[file:../../../org/composyx/solver/BiCGSTAB.org::*Distributed][Distributed:1]]
#include <iostream>
#include <vector>

#ifdef COMPOSYX_USE_EIGEN
#include <composyx/wrappers/Eigen/Eigen_header.hpp>
#endif
#ifdef COMPOSYX_USE_ARMA
#include <composyx/wrappers/armadillo/Armadillo_header.hpp>
#endif
#ifdef COMPOSYX_USE_EIGEN
#include <composyx/wrappers/Eigen/Eigen.hpp>
#endif
#ifdef COMPOSYX_USE_ARMA
#include <composyx/wrappers/armadillo/Armadillo.hpp>
#endif

#include <composyx.hpp>
#include <composyx/solver/BiCGSTAB.hpp>
#include <composyx/precond/DiagonalPrecond.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include "test_iterative_solver.hpp"

#include <composyx/part_data/PartMatrix.hpp>
using namespace composyx;

template <typename Scalar, typename LocMatrix>
void test(const std::string& mat_type) {
  using Matrix = PartMatrix<LocMatrix>;
  using LocVect = typename vector_type<LocMatrix>::type;
  using Vect = PartVector<LocVect>;

  std::shared_ptr<Process> p = test_matrix::get_distr_process();

  SECTION(std::string("BiCGSTAB, distributed, no preconditioner, matrix ") +
          mat_type) {
    test_solver<Scalar, Matrix, Vect, BiCGSTAB<Matrix, Vect>>(
        test_matrix::dist_general_matrix<Scalar, LocMatrix>(p).matrix,
        test_matrix::dist_vector<Scalar, LocVect>(p).vector, 100,
        (MMPI::rank() == 0));
  }

  SECTION(
      std::string("BiCGSTAB, distributed, diagonal preconditioner, matrix ") +
      mat_type) {
    using PcdType = DiagonalPrecond<Matrix, Vect>;
    test_solver<Scalar, Matrix, Vect, BiCGSTAB<Matrix, Vect, PcdType>>(
        test_matrix::dist_general_matrix<Scalar, LocMatrix>(p).matrix,
        test_matrix::dist_vector<Scalar, LocVect>(p).vector);
  }
}

TEMPLATE_TEST_CASE(
    "BiCGSTAB", "[BiCGSTAB][iterative][distributed]",
    float) { //float, double, std::complex<float>, std::complex<double>){
  using Scalar = TestType;

  test<Scalar, DenseMatrix<Scalar>>("DenseMatrix");
  test<Scalar, SparseMatrixCOO<Scalar>>("SparseMatrixCOO");
  test<Scalar, SparseMatrixCSC<Scalar>>("SparseMatrixCSC");

#ifdef COMPOSYX_USE_EIGEN
  using EigenDenseMatrix =
      Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
  using EigenSparseMatrix = Eigen::SparseMatrix<Scalar>;

  test<Scalar, EigenDenseMatrix>("Eigen dense");
  test<Scalar, EigenSparseMatrix>("Eigen sparse");
#endif // COMPOSYX_USE_EIGEN

#ifdef COMPOSYX_USE_ARMA
  test<Scalar, arma::Mat<Scalar>>("Armadillo dense");
  test<Scalar, arma::SpMat<Scalar>>("Armadillo sparse");
#endif
} // TEMPLATE_TEST_CASE
// Distributed:1 ends here
