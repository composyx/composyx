// [[file:../../../org/composyx/solver/ImplicitSchur.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>

#ifdef COMPOSYX_USE_EIGEN
#include <Eigen/Core>
#include <Eigen/Sparse>
#include <composyx/wrappers/Eigen/Eigen.hpp>
#endif

#ifdef COMPOSYX_USE_ARMA
#include <armadillo>
#include <composyx/wrappers/armadillo/Armadillo.hpp>
#endif

#include <composyx.hpp>
#include <composyx/loc_data/SparseMatrixCOO.hpp>
#include <composyx/solver/Pastix.hpp>
#include <composyx/solver/ImplicitSchur.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

TEMPLATE_TEST_CASE("ImplicitSchur", "[schur][sequential]", float, double,
                   std::complex<float>, std::complex<double>) {
  using namespace composyx;
  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;
  using SpMat = SparseMatrixCOO<Scalar>;
  using SolverK = Pastix<SpMat, Vector<Scalar>>;

  Real tol = 1.0e-5;
  SpMat K = test_matrix::general_matrix_4_4<SpMat>();

  SECTION("Test implicit schur VS real schur vector multiplication") {
    // Schur complement of the general matrix on the 2 last unknowns
    DenseMatrix<Scalar> schur;
    if constexpr (is_real<Scalar>::value) {
      schur = DenseMatrix<Scalar>({6., 0.23076922, 1., 7.61538458}, 2, 2,
                                  /*row_major*/ true);
    } else {
      schur = DenseMatrix<Scalar>({{6.35051546, -0.28865979},
                                   {0.27835052, 0.12371134},
                                   {1.08247423, 1.81443299},
                                   {7.53608247, 1.79381443}},
                                  2, 2, /*row_major*/ true);
    }

    ImplicitSchur<SpMat, Vector<Scalar>, SolverK> impschur;
    impschur.setup(K);
    impschur.setup_schurlist(std::vector<int>{2, 3});

    Vector<Scalar> u{-1, 3};
    Vector<Scalar> res_exp = schur * u;
    Vector<Scalar> res = impschur * u;
    REQUIRE((res_exp - res).norm() < tol);
  }

  SECTION("Test implicit schur - half stored input matrix") {

    MatrixProperties<Scalar> prop;

    if constexpr (is_complex<Scalar>::value) {
      prop.set_hpd(MatrixStorage::upper);
    } else {
      prop.set_spd(MatrixStorage::lower);
    }

    DenseMatrix<Scalar> fullmatrix =
        test_matrix::generate_matrix<DenseMatrix<Scalar>>(4, 4, prop, 0.8,
                                                          true);
    fullmatrix.display("fullmatrix half");
    SpMat Kspd(fullmatrix);
    Kspd.display("Kspd");

    fullmatrix.to_storage_full();
    fullmatrix.display("fullmatrix");

    auto A11 = fullmatrix.get_block_view(0, 0, 2, 2);
    auto A12 = fullmatrix.get_block_view(0, 2, 2, 2);
    auto A21 = fullmatrix.get_block_view(2, 0, 2, 2);
    auto A22 = fullmatrix.get_block_view(2, 2, 2, 2);

    BlasSolver<DenseMatrix<Scalar>, DenseMatrix<Scalar>> A11_inv(A11);
    const DenseMatrix<Scalar> schur = A22 - A21 * (A11_inv * A12);

    ImplicitSchur<SpMat, Vector<Scalar>, SolverK> impschur;
    impschur.setup(Kspd);
    impschur.setup_schurlist(std::vector<int>{2, 3});

    const DenseMatrix<Scalar> schur_from_impl = impschur.get_dense_schur();
    REQUIRE((schur_from_impl - schur).norm() < tol);

    schur.display("schur");
    schur_from_impl.display("schur_from_impl");

    const Vector<Scalar> u{-1, 3};
    u.display("uA");
    const Vector<Scalar> res_exp = schur * u;
    u.display("uB");
    const Vector<Scalar> res = impschur * u;
    res_exp.display("res_exp");
    res.display("res");
    REQUIRE((res_exp - res).norm() < tol);
  }
}
// Tests:1 ends here
