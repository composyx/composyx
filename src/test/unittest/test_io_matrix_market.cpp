// [[file:../../../org/composyx/IO/MatrixMarketLoader.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>

#include <composyx.hpp>
#include <composyx/IO/MatrixMarketLoader.hpp>
#include <composyx/testing/TestMatrix.hpp>

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

TEMPLATE_TEST_CASE("MatrixMarketLoader", "[IO][sparse]", double,
                   std::complex<double>) {
  using namespace composyx;
  using Scalar = TestType;
  using Mat = SparseMatrixCOO<Scalar>;

  const std::string filename("tmp_test_matrix_market_load_dump.mtx");

  auto dump_load_compare = [&filename](const Mat& matrix) {
    matrix.dump_to_matrix_market(filename);
    Mat matrix2;
    matrix2.from_matrix_market_file(filename);
    if (matrix.is_general()) {
      return matrix == matrix2;
    } else {
      auto lower_mat = matrix;
      lower_mat.to_storage_half(MatrixStorage::lower);
      return lower_mat == matrix2;
    }
  };

  const Mat K = test_matrix::general_matrix_4_4<Mat>();
  Mat K_spd = K * static_cast<Mat>(K.h());

  auto set_hpd_or_spd = [](Mat& m) {
    if constexpr (is_complex<Scalar>::value) {
      m.set_hpd(MatrixStorage::full);
    } else {
      m.set_spd(MatrixStorage::full);
    }
  };

  // General
  REQUIRE(dump_load_compare(K));
  set_hpd_or_spd(K_spd);
  REQUIRE(dump_load_compare(K_spd));
  K_spd.to_storage_half(MatrixStorage::lower);
  REQUIRE(dump_load_compare(K_spd));
  K_spd = K * static_cast<Mat>(K.h());
  set_hpd_or_spd(K_spd);
  K_spd.to_storage_half(MatrixStorage::upper);
  REQUIRE(dump_load_compare(K_spd));

  // Check array dump / load
  size_t dmat_M = 2;
  size_t dmat_N = 3;
  std::vector<Scalar> dmat{1, 2, 3, 4, 5, 6};
  if constexpr (is_complex<Scalar>::value) {
    for (auto& z : dmat) {
      z += Scalar{0, 1} * z;
    }
  }
  matrix_market::dump<std::vector<Scalar>>(filename, dmat, dmat_M, dmat_N);
  std::vector<Scalar> dmat_loaded;
  size_t M_loaded, N_loaded;
  matrix_market::load<std::vector<Scalar>>(filename, dmat_loaded, M_loaded,
                                           N_loaded);
  REQUIRE(M_loaded == dmat_M);
  REQUIRE(N_loaded == dmat_N);
  REQUIRE(dmat_loaded.size() == dmat_M * dmat_N);
  REQUIRE(dmat_loaded == dmat);
}
// Tests:1 ends here
