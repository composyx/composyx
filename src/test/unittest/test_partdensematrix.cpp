// [[file:../../../org/composyx/part_data/PartDenseMatrix.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>

#include <composyx.hpp>
#include "composyx/part_data/PartDenseMatrix.hpp"

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <composyx/testing/Catch2DenseMatrixMatchers.hpp>
#include <composyx/testing/TestMatrix.hpp>

using namespace composyx;

TEST_CASE("partdensematrix", "[partmatrix][distributed][dense]") {

  using Scalar = double;
  using LocMatrix = DenseMatrix<Scalar>;
  using EqualsMatrixNW = composyx::EqualsMatrixNW<LocMatrix>;
  using EqualsMatrixPW = composyx::EqualsMatrixPW<LocMatrix>;

  const int nprocs = MMPI::size();
  const int rank = MMPI::rank();

  const Scalar alpha = 1.3;
  const Scalar beta = 0.7;

  auto make_glob_mat = [rank](size_t M, size_t N) {
    LocMatrix glob(M, N);
    if (rank == 0) {
      glob =
          test_matrix::random_matrix<Scalar, LocMatrix>(M * N, -1.0, 1.0, M, N)
              .matrix;
    }
    MMPI::bcast(get_ptr(glob), M * N, 0, MPI_COMM_WORLD);
    return glob;
  };

  const LocMatrix glob_A = make_glob_mat(8, 6);
  const LocMatrix glob_B = make_glob_mat(6, 3);
  const LocMatrix glob_Ci = make_glob_mat(8, 3);
  const LocMatrix glob_Cf = alpha * glob_A * glob_B + beta * glob_Ci;

  const std::vector<int> cuts_i{0, 3, 5, 8};
  const std::vector<int> cuts_j{0, 2, 3};
  const std::vector<int> cuts_k{0, 3, 5, 6};

  using MapFct = std::function<int(int, int)>;
  using PartMat = PartDenseMatrix<LocMatrix, MapFct>;

  const int seed = 1337;
  auto mapfctA_ptr =
      std::make_shared<MapFct>(canonical_datamap::pseudorandom(nprocs, seed));
  Datamap<MapFct> dmapA(mapfctA_ptr, 10, 10);
  auto mapfctB_ptr = std::make_shared<MapFct>(
      canonical_datamap::pseudorandom(nprocs, seed * 2 + 1));
  Datamap<MapFct> dmapB(mapfctB_ptr, 10, 10);
  auto mapfctC_ptr = std::make_shared<MapFct>(
      canonical_datamap::pseudorandom(nprocs, seed * 10 + 1));
  Datamap<MapFct> dmapC(mapfctC_ptr, 10, 10);

  auto make_part_matrix = [rank](const std::vector<int> cuts_row,
                                 const std::vector<int> cuts_col,
                                 const LocMatrix& glob_mat,
                                 Datamap<MapFct> ldmap) {
    std::map<std::pair<int, int>, LocMatrix> mat_map;
    for (size_t ib = 0; ib < cuts_row.size() - 1; ++ib) {
      for (size_t jb = 0; jb < cuts_col.size() - 1; ++jb) {
        size_t nrows = static_cast<size_t>(cuts_row[ib + 1] - cuts_row[ib]);
        size_t ncols = static_cast<size_t>(cuts_col[jb + 1] - cuts_col[jb]);
        if (ldmap(ib, jb) == rank) {
          mat_map.emplace(std::make_pair(ib, jb),
                          glob_mat.get_block_copy(cuts_row[ib], cuts_col[jb],
                                                  nrows, ncols));
        }
      }
    }
    return PartMat(MPI_COMM_WORLD, mat_map, ldmap);
  };

  // Needed to pass some tests in double precision
  const double tol_ten = composyx::arithmetic_tolerance<Scalar>::value * 10;

  const auto mat_A = make_part_matrix(cuts_i, cuts_k, glob_A, dmapA);
  const auto mat_B = make_part_matrix(cuts_k, cuts_j, glob_B, dmapB);
  auto mat_Ci = make_part_matrix(cuts_i, cuts_j, glob_Ci, dmapC);

  //glob_A.display("A");
  //mat_A.display_centralized(0, "A");
  //mat_B.display_centralized(0, "B");
  //mat_Ci.display_centralized(0, "C");

  SECTION("Centralization") {
    auto centr_A = mat_A.centralize(0);

    if (rank == 0) {
      //glob_A.display("glob_A");
      //centr_A.display("Matrix centralized");
      REQUIRE_THAT(centr_A, EqualsMatrixNW(glob_A));
    }
  }

  SECTION("Matrix matrix product (random taskmap)") {
    auto taskmap = canonical_taskmap::pseudorandom(nprocs, seed);
    dense_dist_gemm(mat_A, mat_B, mat_Ci, taskmap, alpha, beta);
    auto centr_C = mat_Ci.centralize(0);
    if (rank == 0) {
      REQUIRE_THAT(centr_C, EqualsMatrixNW(glob_Cf));
    }
  }

  SECTION("Matrix matrix product (A stat)") {
    auto taskmap = canonical_taskmap::A_stationary(mat_A.get_datamap());
    dense_dist_gemm(mat_A, mat_B, mat_Ci, taskmap, alpha, beta);
    auto centr_C = mat_Ci.centralize(0);
    if (rank == 0) {
      REQUIRE_THAT(centr_C, EqualsMatrixNW(glob_Cf));
    }
  }

  SECTION("Matrix matrix product (B stat)") {
    auto taskmap = canonical_taskmap::B_stationary(mat_B.get_datamap());
    dense_dist_gemm(mat_A, mat_B, mat_Ci, taskmap, alpha, beta);
    auto centr_C = mat_Ci.centralize(0);
    if (rank == 0) {
      REQUIRE_THAT(centr_C, EqualsMatrixNW(glob_Cf));
    }
  }

  SECTION("Matrix matrix product (C stat)") {
    auto taskmap = canonical_taskmap::C_stationary(mat_Ci.get_datamap());
    dense_dist_gemm(mat_A, mat_B, mat_Ci, taskmap, alpha, beta);
    auto centr_C = mat_Ci.centralize(0);
    if (rank == 0) {
      REQUIRE_THAT(centr_C, EqualsMatrixNW(glob_Cf));
    }
  }

  SECTION("Matrix matrix product (using serialize, taskmap random)") {
    auto taskmap = canonical_taskmap::pseudorandom(nprocs, seed);
    serialize_gemm(mat_A, mat_B, mat_Ci, taskmap, alpha, beta);
    auto centr_C = mat_Ci.centralize(0);
    if (rank == 0) {
      REQUIRE_THAT(centr_C, EqualsMatrixNW(glob_Cf));
    }
  }

  SECTION("Matrix matrix product with expression 'auto Cf = alpha * A * B + "
          "beta * Ci'") {
    auto Cf = alpha * mat_A * mat_B + beta * mat_Ci;
    auto centr_Cf = Cf.centralize(0);
    if (rank == 0) {
      REQUIRE_THAT(centr_Cf, EqualsMatrixNW(glob_Cf));
    }
  }

  SECTION("Column extraction") {
    for (int j = 0; j < 6; j++) {
      auto V = mat_A.get_col(j);
      auto centr_V = V.centralize(0);
      if (rank == 0) {
        auto Vj_glob = glob_A.get_vect_view(j);
        REQUIRE_THAT(centr_V, EqualsMatrixPW(Vj_glob));
      }
    }
  }

  SECTION("Row extraction") {
    for (int i = 0; i < 8; i++) {
      auto R = mat_A.get_row(i);
      auto centr_R = R.centralize(0);
      if (rank == 0) {
        auto Ri_glob = glob_A.get_row(i);
        REQUIRE_THAT(centr_R, EqualsMatrixPW(Ri_glob));
      }
    }
  }

  SECTION("Block extraction") {
    const std::vector<std::array<int, 4>> ijmn{{0, 0, 5, 5}, {1, 1, 5, 4},
                                               {6, 4, 2, 2}, {0, 4, 4, 2},
                                               {0, 4, 8, 1}, {4, 0, 1, 6}};

    for (const auto& [i, j, m, n] : ijmn) {
      //std::cout << "i, j, m, n: " << i<< ',' << j<< ',' << m<< ',' << n << '\n';
      auto B = mat_A.get_block(i, j, m, n);
      //B.display_centralized(0, "B");
      auto centr_B = B.centralize(0);
      if (rank == 0) {
        auto B_glob = glob_A.get_block_view(i, j, m, n);
        //centr_B.display("centr_B");
        //B_glob.display("B_glob");
        REQUIRE_THAT(centr_B, EqualsMatrixPW(B_glob));
      }
    }
  }

  SECTION("axpy") {
    const PartMat B_x3 = mat_B * Scalar{3};
    PartMat B_x7 = mat_B;
    // B_x7 = 2 * B_x3 + B
    axpy(B_x3, B_x7, Scalar{2});
    auto centr_B_x7 = B_x7.centralize(0);
    auto centr_B = mat_B.centralize(0);
    if (rank == 0) {
      auto check_B_x7 = Scalar{7} * centr_B;
      REQUIRE_THAT(check_B_x7, EqualsMatrixPW(centr_B_x7));
    }
  }

  SECTION("axpy with different datamaps") {
    // Same but B_x3 with mapC and B_x7 with mapA
    const PartMat B_x3 =
        make_part_matrix(cuts_k, cuts_j, glob_B, dmapA) * Scalar{3};
    PartMat B_x7 = make_part_matrix(cuts_k, cuts_j, glob_B, dmapC);

    // B_x7 = 2 * B_x3 + B
    axpy(B_x3, B_x7, Scalar{2});
    auto centr_B_x7 = B_x7.centralize(0);
    auto centr_B = mat_B.centralize(0);
    if (rank == 0) {
      auto check_B_x7 = Scalar{7} * centr_B;
      REQUIRE_THAT(check_B_x7, EqualsMatrixPW(centr_B_x7));
    }
  }

  SECTION("Gemv") {
    auto B0 = mat_B.get_col(0);
    auto AB0 = mat_A * B0;
    auto B0_centr = B0.centralize(0);
    auto AB0_centr = AB0.centralize(0);
    if (rank == 0) {
      auto AB0_check = glob_A * B0_centr;
      REQUIRE_THAT(AB0_check, EqualsMatrixPW(AB0_centr, tol_ten));
    }
  }

  SECTION("Gemv (bigger)") {
    // Matrix A is 65 x 65, block size 1x1, 2DBC P = 2, Q = 2, coeffs are 1
    // Vector x is 65 x 1, coeffs are 1
    std::vector<int> cuts(66);
    for (int k = 0; k < 66; ++k)
      cuts[k] = k;
    const LocMatrix block({1.0}, 1, 1);
    int P = 2;
    if (nprocs < 4)
      P = 1;
    int Q = nprocs / P;
    auto mapfctA65_ptr =
        std::make_shared<MapFct>(canonical_datamap::TwoD_block_cyclic(P, Q));
    Datamap<MapFct> dmapA65(mapfctA65_ptr, 65, 65);

    std::map<std::pair<int, int>, LocMatrix> mat_map;
    for (int i = 0; i < 65; ++i) {
      for (int j = 0; j < 65; ++j) {
        if (dmapA65(i, j) == rank)
          mat_map.emplace(std::make_pair(i, j), block);
      }
    }
    PartMat A65(MPI_COMM_WORLD, mat_map, dmapA65);
    PartMat x65 = compute_matvect_distribution(A65);
    for (size_t ib = 0; ib < x65.get_n_rows_block(); ++ib) {
      if (x65.get_datamap()(ib, 0) == rank) {
        auto& lm = x65.get_local_matrix(ib, 0);
        for (size_t i = 0; i < n_rows(lm); ++i) {
          lm(i, 0) = Scalar{1};
        }
      }
    }
    PartMat Ax65 = A65 * x65;

    auto Ax65_centr = Ax65.centralize(0);
    if (rank == 0) {
      LocMatrix Ax65_check(65, 1);
      for (int k = 0; k < 65; ++k) {
        Ax65_check(k, 0) = Scalar{65};
      }
      REQUIRE_THAT(Ax65_check, EqualsMatrixPW(Ax65_centr));
    }
  }

  SECTION("Gemm with sparse local matrices") {
    using SpLocMat = SparseMatrixCOO<Scalar>;

    std::map<std::pair<int, int>, SpLocMat> mat_map;
    std::map<std::pair<int, int>, LocMatrix> mat_map_dense;
    for (size_t ib = 0; ib < cuts_i.size() - 1; ++ib) {
      for (size_t jb = 0; jb < cuts_k.size() - 1; ++jb) {
        size_t nrows = static_cast<size_t>(cuts_i[ib + 1] - cuts_i[ib]);
        size_t ncols = static_cast<size_t>(cuts_k[jb + 1] - cuts_k[jb]);
        if (dmapA(ib, jb) == rank) {
          SpLocMat lA = test_matrix::random_matrix<Scalar, SpLocMat>(
                            nrows * ncols / 2, -1.0, 1.0, nrows, ncols)
                            .matrix;
          mat_map.emplace(std::make_pair(ib, jb), lA);
          mat_map_dense.emplace(std::make_pair(ib, jb), lA.to_dense());
        }
      }
    }

    PartDenseMatrix<SpLocMat, MapFct> Asp(MPI_COMM_WORLD, mat_map, dmapA);
    PartMat Adense(MPI_COMM_WORLD, mat_map_dense, dmapA);

    auto B0 = mat_B.get_col(0);
    auto AB0 = mat_Ci.get_col(0);
    auto taskmap = canonical_taskmap::C_stationary(AB0.get_datamap());
    serialize_gemm(Asp, B0, AB0, taskmap);

    auto A_centr = Adense.centralize(0);
    auto B0_centr = B0.centralize(0);
    auto AB0_centr = AB0.centralize(0);
    if (rank == 0) {
      auto AB0_check = A_centr * B0_centr;
      REQUIRE_THAT(AB0_check, EqualsMatrixPW(AB0_centr, tol_ten));
    }
  }
}
// Tests:1 ends here
