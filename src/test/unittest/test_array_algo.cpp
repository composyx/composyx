// [[file:../../../org/composyx/utils/ArrayAlgo.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>
#include <map>

#include <composyx.hpp>
#include "composyx/utils/ArrayAlgo.hpp"
#include <composyx/testing/TestMatrix.hpp>

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_range_equals.hpp>

TEST_CASE("ArrayAlgo", "[array_algo]") {
  using namespace composyx;
  using Catch::Matchers::RangeEquals;
  using std::vector;

  SECTION("Union with") {
    vector<double> a11{1.0, 2.0, -1.0};
    const vector<double> b11{2.0, 3.0, -1.0, 0.0};
    const vector<double> a11_exp{-1.0, 0.0, 1.0, 2.0, 3.0};
    union_with(a11, b11);
    REQUIRE_THAT(a11, RangeEquals(a11_exp));
  }

  SECTION("Search sorted") {
    vector<double> a{4.0, 1.0, 2.0, 1.5, 3.0};
    std::ranges::sort(a);
    // Sorted 1, 1.5, 2, 3, 4
    vector<double> values{1.5, 4.0, 2.0};
    const vector<int> expected{1, 4, 2};
    vector<int> found = searchsorted<vector<double>, vector<int>>(a, values);
    //searchsorted(a, values, found);
    REQUIRE_THAT(found, RangeEquals(expected));
  }

  SECTION("arange - int") {
    using TI = vector<int>;
    vector<int> a1 = arange<TI>(3);
    vector<int> a1_c{0, 1, 2};
    REQUIRE_THAT(a1, RangeEquals(a1_c));
    vector<int> a2 = arange<TI>(3, 7);
    vector<int> a2_c{3, 4, 5, 6};
    REQUIRE_THAT(a2, RangeEquals(a2_c));
    vector<int> a3 = arange<TI>(7, 3, -2);
    vector<int> a3_c{7, 5};
    REQUIRE_THAT(a3, RangeEquals(a3_c));
    vector<int> a4 = arange<TI>(7, 2, -2);
    vector<int> a4_c{7, 5, 3};
    REQUIRE_THAT(a4, RangeEquals(a4_c));
  }

  SECTION("arange - double") {
    using TI = vector<double>;
    auto comparator = [](double d1, double d2) {
      return std::abs(d1 - d2) < 1e-15;
    };

    vector<double> a1 = arange<TI>(3.1);
    vector<double> a1_c{0.0, 1.0, 2.0, 3.0};
    REQUIRE_THAT(a1, RangeEquals(a1_c, comparator));
    vector<double> a2 = arange<TI>(3.1, 7.0);
    vector<double> a2_c{3.1, 4.1, 5.1, 6.1};
    REQUIRE_THAT(a2, RangeEquals(a2_c, comparator));
    vector<double> a3 = arange<TI>(7.1, 3.0, -2.0);
    vector<double> a3_c{7.1, 5.1, 3.1};
    REQUIRE_THAT(a3, RangeEquals(a3_c, comparator));
    vector<double> a4 = arange<TI>(7.1, 3.1, -2.0);
    vector<double> a4_c{7.1, 5.1};
    REQUIRE_THAT(a4, RangeEquals(a4_c, comparator));
  }

  SECTION("argsort") {
    vector<int> a{4, 2, 1, 3, 6, 5};
    vector<int> idx{2, 1, 3, 0, 5, 4};
    vector<int> a_idxsort = argsort<vector<int>>(a);
    REQUIRE_THAT(idx, RangeEquals(a_idxsort));
  }

  SECTION("Multilevel argsort") {
    vector<int> a1{0, 1, 2, 1, 0, 0, 3, 4, 0, 3};
    vector<float> a2{12, 16, 10, 14, 10, 11, 12, 10, 11, 12};
    vector<int> a3{20, 21, 21, 20, 23, 23, 21, 20, 22, 20};
    const vector<int> idx_exp{4, 8, 5, 0, 3, 1, 2, 9, 6, 7};
    vector<int> idx = multilevel_argsort<vector<int>>(a1, a2, a3);
    REQUIRE_THAT(idx, RangeEquals(idx_exp));
  }

  SECTION("Permutation") {
    vector<double> a{1.0, 2.0, 3.0, 4.0};
    vector<int> b{3, 5, 1, 8};

    const vector<long int> perm{3, 1, 0, 2};
    const vector<double> a_permuted{4.0, 2.0, 1.0, 3.0};
    const vector<int> b_permuted{8, 5, 3, 1};

    apply_permutation(perm, a, b);
    REQUIRE_THAT(a, RangeEquals(a_permuted));
    REQUIRE_THAT(b, RangeEquals(b_permuted));
  }

  SECTION("Subarray") {
    const vector<double> a{1.0, 2.0, 3.0, 4.0, 5.0, 6.0};
    const vector<int> idxs{2, 1, 5, 0};
    const vector<double> res{3.0, 2.0, 6.0, 1.0};

    vector<double> sa = subarray(a, idxs);
    REQUIRE_THAT(sa, RangeEquals(res));
  }
}
// Tests:1 ends here
