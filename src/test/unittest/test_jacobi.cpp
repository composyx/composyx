// [[file:../../../org/composyx/solver/Jacobi.org::*Sequential][Sequential:1]]
#include <iostream>
#include <vector>

#ifdef COMPOSYX_USE_EIGEN
#include <composyx/wrappers/Eigen/Eigen_header.hpp>
#endif
#ifdef COMPOSYX_USE_ARMA
#include <composyx/wrappers/armadillo/Armadillo_header.hpp>
#endif
#ifdef COMPOSYX_USE_EIGEN
#include <composyx/wrappers/Eigen/Eigen.hpp>
#endif
#ifdef COMPOSYX_USE_ARMA
#include <composyx/wrappers/armadillo/Armadillo.hpp>
#endif

#include <composyx.hpp>
#include <composyx/solver/Jacobi.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include "test_iterative_solver.hpp"

using namespace composyx;

template <typename Scalar, typename Matrix>
void test(const std::string mat_type) {
  using Vect = typename vector_type<Matrix>::type;

  // Make a diagonal dominant matrix
  Matrix A = test_matrix::general_matrix_4_4<Matrix>();
  for (size_t i = 0; i < n_rows(A); ++i) {
    A(i, i) += 10;
  }

  SECTION(std::string("Jacobi, matrix ") + mat_type) {
    test_solver<Scalar, Matrix, Vect, Jacobi<Matrix, Vect>>(
        A, test_matrix::simple_vector<Vect>());
  }
}

TEMPLATE_TEST_CASE("Jacobi", "[Jacobi][iterative][sequential]", float, double,
                   std::complex<float>, std::complex<double>) {
  using Scalar = TestType;

  test<Scalar, DenseMatrix<Scalar>>("DenseMatrix");
#ifdef COMPOSYX_USE_EIGEN
  using EigenDenseMatrix =
      Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
  test<Scalar, EigenDenseMatrix>("Eigen dense");
#endif
#ifdef COMPOSYX_USE_ARMA
  test<Scalar, arma::Mat<Scalar>>("Armadillo dense");
#endif
} // TEMPLATE_TEST_CASE
// Sequential:1 ends here
