// [[file:../../../org/composyx/wrappers/Eigen/EigenSparseSolver.org::*Tests][Tests:1]]
#include <iostream>

#include <composyx/wrappers/Eigen/Eigen_header.hpp>
#include <composyx/wrappers/Eigen/EigenSparseSolver.hpp>
#include <composyx.hpp>
#include <composyx/wrappers/Eigen/Eigen.hpp>
#include <composyx/solver/ConjugateGradient.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>

using namespace composyx;

TEST_CASE("EigenSparseSolver", "[sparse][direct][Eigen]") {
  using Scalar = double;
  using Real = double;
  using Vect = Eigen::Matrix<Scalar, Eigen::Dynamic, 1>;
  using SpMat = Eigen::SparseMatrix<Scalar>;
  using DenseMat = Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;

  // A            X =  B
  //--------------------
  // 2  -  -  -   3    1
  //-1  2  -  -   5    0
  // 0 -1  2  - x 7 =  0
  // 0  0 -1  2   9   11
  std::vector<int> i{0, 1, 1, 2, 2, 3, 3};
  std::vector<int> j{0, 0, 1, 1, 2, 2, 3};
  std::vector<Scalar> v{2.0, -1.0, 2.0, -1.0, 2.0, -1.0, 2.0};

  Vect X_exp(4);
  X_exp << 3., 5., 7., 9.;
  const Real test_tol = 1e-14;
  MatrixProperties<Scalar> prop;

  SECTION("Eigen direct solve SPD") {
    SpMat spd_spA;
    build_matrix(spd_spA, 4, 4, 7, &i[0], &j[0], &v[0]);
    prop.set_spd(MatrixStorage::lower);
    Vect B(4);
    B << 1., 0., 0., 11.;
    EigenSparseSolver<SpMat, Vect> solver(spd_spA, prop);
    Vect X_found = solver * B;
    Real direct_err = (X_found - X_exp).norm();
    REQUIRE(direct_err < test_tol);
  }

  //SECTION("Eigen direct solve symmetric (LDLT)"){
  //  std::vector<Scalar> v_sym{3.0, 3.0, 1.0, -1.0, 10.0, 2.0, 4.0};
  //  SpMat sym_spA;
  //  build_matrix(sym_spA, 4, 4, 7, &i[0], &j[0], &v_sym[0]);
  //  prop.set_property(MatrixSymmetry::symmetric, MatrixStorage::lower);
  //  Vect B = sym_spA * X_exp;
  //  EigenSparseSolver<SpMat, Vect> solver(sym_spA, prop);
  //  Vect X_found = solver * B;
  //  Real direct_err = (X_found - X_exp).norm();
  //  REQUIRE(direct_err < test_tol);
  //}

  SpMat ge_spA = test_matrix::general_matrix_4_4<SpMat>();
  SECTION("Eigen direct solve general (LU)") {
    Vect B = ge_spA * X_exp;
    EigenSparseSolver<SpMat, Vect> solver(ge_spA);
    Vect X_found = solver * B;
    Real direct_err = (X_found - X_exp).norm();
    REQUIRE(direct_err < test_tol);
  }

  // Schur complement
  // Solving with Schur complement
  // S = Schur complement of A on (1, 3):
  // S        x Y = f
  //------------------------------------
  //   1  -1/2    5   1/2
  // -1/2  3/2  x 9 = 11
  SECTION("EigenSparseSolver solve with Schur complement") {
    SpMat spd_spA;
    build_matrix(spd_spA, 4, 4, 7, &i[0], &j[0], &v[0]);
    prop.set_spd(MatrixStorage::lower);
    EigenSparseSolver<SpMat, Vect> schur_solver(spd_spA, prop);

    std::vector<int> schurlist{1, 3};
    DenseMat schur_mat = schur_solver.get_schur(schurlist);
    display(schur_mat, "Schur matrix");

    Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic> es_exp(2, 2);
    es_exp << 1, -0.5, -0.5, 1.5;
    DenseMat schur_exp(std::move(es_exp));
    double schur_diff = (schur_mat - schur_exp).norm();
    REQUIRE(schur_diff < test_tol);

    Vect B(4);
    B << 1., 0., 0., 11.;
    Vect f = schur_solver.b2f(B);
    Vect f_exp(2);
    f_exp << 1. / 2., 11.;
    double f_diff = (f - f_exp).norm();
    display(f, "f");
    REQUIRE(f_diff < test_tol);

    ConjugateGradient<DenseMat, Vect> cg_on_schur(schur_mat);
    cg_on_schur.setup(parameters::max_iter{10});
    Vect y = cg_on_schur * f;
    Vect y_exp(2);
    y_exp << 5., 9.;
    double y_diff = (y - y_exp).norm();
    display(y, "y");
    REQUIRE(y_diff < test_tol);

    Vect X = schur_solver.y2x(y);
    display(X, "X");
    double direct_err = (X - X_exp).norm();
    std::cout << "|| X - X_exp ||_2 : " << direct_err << '\n';
    REQUIRE(direct_err < test_tol);
  }
}
// Tests:1 ends here
