// [[file:../../../org/composyx/solver/IRSolver.org::*Tests][Tests:1]]
#include <composyx.hpp>
#include <composyx/solver/BlasSolver.hpp>
#include <composyx/solver/IRSolver.hpp>
#include <composyx/solver/GMRES.hpp>
#include <composyx/precond/DiagonalPrecond.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>

using namespace composyx;

TEST_CASE("IR solver", "[IR][iterative]") {
  using Scalar = double;
  //using Real = double;
  using Matrix = DenseMatrix<Scalar>;
  using Vect = Vector<Scalar>;

  using InnerSolver = GMRES<Matrix, Vect, DiagonalPrecond<Matrix, Vect>>;

  Matrix A = test_matrix::general_matrix_4_4<Matrix>();
  Vect x_exp = test_matrix::simple_vector<Vect>();
  Vect b = A * x_exp;

  SECTION("Without ref solver") {
    using Solver = IRSolver<Matrix, Vect, InnerSolver>;

    Solver ir(A);
    auto& inner_s = ir.get_inner_solver();

    inner_s.setup(parameters::max_iter{10}, parameters::tolerance{1e-4},
                  parameters::verbose{true});
    inner_s.display("Inner solver");

    ir.setup(parameters::max_iter{5}, parameters::tolerance{1e-8},
             parameters::verbose{true});

    Vect x = ir * b;

    x_exp.display("x_exp");
    x.display("x");

    REQUIRE((x - x_exp).norm() < 1e-6);
  }

  SECTION("With a reference direct solver") {
    using RefSolver = BlasSolver<Matrix, Vect>;
    using Solver = IRSolver<Matrix, Vect, InnerSolver, RefSolver>;

    Solver ir(A);
    auto& inner_s = ir.get_inner_solver();
    auto& ref_s = ir.get_ref_solver();
    inner_s.display("Inner solver");
    ref_s.display("Reference solver");

    inner_s.setup(parameters::max_iter{10}, parameters::tolerance{1e-4},
                  parameters::verbose{true});

    ir.setup(parameters::max_iter{5}, parameters::tolerance{1e-8},
             parameters::verbose{true});

    Vect x = ir * b;

    x_exp.display("x_exp");
    x.display("x");

    REQUIRE((x - x_exp).norm() < 1e-6);
  }
}
// Tests:1 ends here
