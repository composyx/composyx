// [[file:../../../org/composyx/loc_data/SparseMatrixCOO.org::*Matrix market loading tests][Matrix market loading tests:1]]
#include <iostream>
#include <composyx.hpp>
#include <composyx/loc_data/SparseMatrixCOO.hpp>

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <composyx/testing/Catch2DenseMatrixMatchers.hpp>

using namespace composyx;

TEST_CASE("Matrix market loading", "[IO, matrix_market]") {

  const SparseMatrixCOO<double, int> ref(
      {0, 1, 2, 3, 0, 1, 3, 1, 2, 3, 0, 3, 4},
      {0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3},
      {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13});
  ref.dump_to_matrix_market("ref_matrix.mtx");

  SECTION("Load COO") {
    const auto test =
        SparseMatrixCOO<double, int>::from_matrix_market("ref_matrix.mtx");
    REQUIRE(test == ref);
  }

  SECTION("Load DenseMatrix") {
    const auto test = DenseMatrix<double>::from_matrix_market("ref_matrix.mtx");
    SparseMatrixCOO<double, int> tcoo;
    test.convert(tcoo);
    REQUIRE(tcoo == ref);
  }

  SECTION("Load CSC") {
    const auto test =
        SparseMatrixCSC<double, int>::from_matrix_market("ref_matrix.mtx");
    SparseMatrixCOO<double, int> tcoo;
    test.convert(tcoo);
    REQUIRE(tcoo == ref);
  }

  SECTION("Load LIL") {
    const auto test =
        SparseMatrixLIL<double, int>::from_matrix_market("ref_matrix.mtx");
    SparseMatrixCOO<double, int> tcoo;
    test.convert(tcoo);
    REQUIRE(tcoo == ref);
  }
}
// Matrix market loading tests:1 ends here
