// [[file:../../../org/composyx/utils/Chameleon.org::*Tests][Tests:1]]
#include <composyx.hpp>
#ifndef COMPOSYX_NO_MPI
#include <composyx/dist/MPI.hpp>
#endif
#include <composyx/utils/Chameleon.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>

TEST_CASE("Chameleon", "[chameleon]") {
  using namespace composyx;

#ifndef COMPOSYX_NO_MPI
  int nmpi = MMPI::size();
  int rmpi = MMPI::rank();
#else
  int nmpi = 1;
  int rmpi = 1;
#endif

  SECTION("Default constructor") {
    // initialize the chameleon context with default parameters
    initialize();
    REQUIRE(CHAMELEON_Comm_size() == nmpi);
    REQUIRE(CHAMELEON_Comm_rank() == rmpi);
  }

  SECTION("Setters (parallel parameter and tilesize") {
    const int nt = 3;
    const int ng = 0;
    const int nb = 3;

    // initialize the chameleon context with non default parameters
    initialize(nt, ng);
    setChameleonParameters(nb);

    REQUIRE(CHAMELEON_GetThreadNbr() == nt);
    int nb_check;
    CHAMELEON_Get(CHAMELEON_TILE_SIZE, &nb_check);
    REQUIRE(nb_check == nb);
  }

  finalize();
} //TEST_CASE
// Tests:1 ends here
