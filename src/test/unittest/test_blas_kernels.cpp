// [[file:../../../org/composyx/kernel/BlasKernels.org::*Tests][Tests:1]]
#include <composyx.hpp>
#include <composyx/kernel/BlasKernels.hpp>
#include <composyx/loc_data/DenseMatrix.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <composyx/testing/Catch2DenseMatrixMatchers.hpp>

using namespace composyx;

TEMPLATE_TEST_CASE("BlasKernels", "[dense][sequential][blas][kernel]", float,
                   double, std::complex<float>, std::complex<double>) {
  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;
  using Complex = typename arithmetic_complex<Scalar>::type;
  using EqualsVectorPW = composyx::EqualsVectorPW<Vector<Scalar>>;
  using EqualsVectorNW = composyx::EqualsVectorNW<Vector<Scalar>>;
  using EqualsMatrixPW = composyx::EqualsMatrixPW<DenseMatrix<Scalar>>;

  constexpr const double blas_tol =
      (is_precision_double<Scalar>::value) ? 1e-15 : 1e-6;
  constexpr const double ev_tol =
      (is_precision_double<Scalar>::value) ? 1e-12 : 1e-5;

  auto equals_fp = [](auto s1, auto s2, double tol) {
    return std::abs(s1 - s2) < tol;
  };
  // Tests:1 ends here

  // [[file:../../../org/composyx/kernel/BlasKernels.org::*Blas 1][Blas 1:1]]
  SECTION("Blas 1 - SCAL") {
    const Vector<Scalar> x({1, -1, 3});
    const DenseMatrix<Scalar> mat({1, 2, 3, 4}, 2, 2);

    // Scal (*2)
    const Vector<Scalar> x_scal({2, -2, 6});

    Vector<Scalar> x2(x);
    blas_kernels::scal(x2, Scalar{2});
    REQUIRE_THAT(x2, EqualsVectorPW(x_scal));

    // Test with increment
    DenseMatrix<Scalar> mat2(mat);
    Vector<Scalar> y = mat2.get_row_view(0);

    const Vector<Scalar> y_scal({2, 6});
    blas_kernels::scal(y, Scalar{2});

    REQUIRE_THAT(y, EqualsVectorPW(y_scal));
  }

  SECTION("Blas 1 - AXPY") {
    const Vector<Scalar> x({1, -1, 3});

    // AXPY (*2 + (-1,-1,-1)T)
    Vector<Scalar> y({-1, -1, -1});
    const Vector<Scalar> x2py({1, -3, 5});
    blas_kernels::axpy(x, y, Scalar{2});
    REQUIRE_THAT(y, EqualsVectorPW(x2py));
  }

  // dot
  SECTION("Blas 1 - DOT") {
    const Vector<Scalar> x({1, -1, 3});

    const Vector<Scalar> y({2, -4, -1});
    const Scalar xdoty{3};
    REQUIRE(equals_fp(blas_kernels::dot(x, y), xdoty, blas_tol));
  }

  // norm2
  SECTION("Blas 1 - NORM2") {
    const Vector<Scalar> x({1, -1, 3});

    const Real normx_sq = 11;
    const Real normx = std::sqrt(normx_sq);
    REQUIRE(equals_fp(blas_kernels::norm2_squared(x), normx_sq, blas_tol));
    REQUIRE(equals_fp(blas_kernels::norm2(x), normx, blas_tol));
  }
  // Blas 1:1 ends here

  // [[file:../../../org/composyx/kernel/BlasKernels.org::*Blas 2][Blas 2:1]]
  SECTION("Blas 2 -GEMV") {
    // 1 4        3
    // 2 5 x -1 = 3
    // 3 6    1   3
    const DenseMatrix<Scalar> MA({1, 2, 3, 4, 5, 6}, 3, 2);
    const Vector<Scalar> x({-1, 1});
    const Vector<Scalar> sol_check({3, 3, 3});
    Vector<Scalar> y(composyx::n_rows(MA));

    blas_kernels::gemv(MA, x, y);

    REQUIRE_THAT(sol_check, EqualsVectorPW(y));
  }

  SECTION("BLAS 2 - Gemv sym/herm") {
    Vector<Scalar> X({3, 2, 1});
    DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
    L.set_property(composyx::MatrixStorage::lower,
                   composyx::MatrixSymmetry::symmetric);

    Vector<Scalar> LX({-2, 5, -2});
    if constexpr (composyx::is_complex<Scalar>::value) {
      X += Scalar{0, 1} * Vector<Scalar>({1, 2, -1});
      L += Scalar{0, 1} *
           DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
      L.set_property(composyx::MatrixStorage::lower,
                     composyx::MatrixSymmetry::hermitian);
      LX = Vector<Scalar>({{-5, 7}, {8, -2}, {1, -7}});
    }

    Vector<Scalar> my_LX(3);
    blas_kernels::gemv(L, X, my_LX);
    REQUIRE_THAT(my_LX, EqualsVectorPW(LX));
  }
  // Blas 2:1 ends here

  // [[file:../../../org/composyx/kernel/BlasKernels.org::*Blas 3][Blas 3:1]]
  SECTION("Blas 3 - GEMM - general") {
    const DenseMatrix<Scalar> A({1.0, -1.0, 2.0, -2.0, 3.0, -3.0}, 2, 3);
    const DenseMatrix<Scalar> B({1.0, 2.0, 3.0, 4.0, 5.0, 6.0}, 3, 2);

    //            1 4
    //  1  2  3 x 2 5 =  14  32
    // -1 -2 -3   3 6   -14 -32

    const DenseMatrix<Scalar> C_check({14.0, -14.0, 32.0, -32.0}, 2, 2);
    DenseMatrix<Scalar> C(composyx::n_rows(A), composyx::n_cols(B));

    blas_kernels::gemm(A, B, C);
    REQUIRE_THAT(C, EqualsMatrixPW(C_check));
  }

  SECTION("Blas 3 - GEMM - symmetric / hermitian") {
    DenseMatrix<Scalar> M({1, 2, 3, 4, 5, 6, 7, 8, 9}, 3, 3);
    DenseMatrix<Scalar> L({-1, 2, -3, 0, -1, 1, 0, 0, 5}, 3, 3);
    L.set_property(composyx::MatrixStorage::lower,
                   composyx::MatrixSymmetry::symmetric);

    DenseMatrix<Scalar> ML({-14, -16, -18, 5, 7, 9, 36, 39, 42}, 3, 3);
    DenseMatrix<Scalar> LM({-6, 3, 14, -12, 9, 23, -18, 15, 32}, 3, 3);
    if constexpr (composyx::is_complex<Scalar>::value) {
      M += Scalar{0, 1} *
           DenseMatrix<Scalar>({1, -1, 1, -1, 2, -2, 1, -1, -0.5}, 3, 3);
      L += Scalar{0, 1} *
           DenseMatrix<Scalar>({0, -1, 1, 0, 0, -2, 0, 0, 0}, 3, 3);
      L.set_property(composyx::MatrixStorage::lower,
                     composyx::MatrixSymmetry::hermitian);
      ML = DenseMatrix<Scalar>({{-16, -3},
                                {-13, 11},
                                {-19.5, -0.5},
                                {6, -9},
                                {6, -19},
                                {7, -11.5},
                                {39, 8},
                                {34, 8},
                                {47, 1.5}},
                               3, 3);
      LM = DenseMatrix<Scalar>({{-4, -7},
                                {2, 9},
                                {11, -2},
                                {-16, 10},
                                {12, 2},
                                {28, -11},
                                {-17.5, -2.5},
                                {17, 13.5},
                                {29, -15.5}},
                               3, 3);
    }

    DenseMatrix<Scalar> my_ML(3, 3);
    blas_kernels::gemm(M, L, my_ML);
    REQUIRE_THAT(my_ML, EqualsMatrixPW(ML));

    DenseMatrix<Scalar> my_LM(3, 3);
    blas_kernels::gemm(L, M, my_LM);
    REQUIRE_THAT(my_LM, EqualsMatrixPW(LM));
  }
  // Blas 3:1 ends here

  // [[file:../../../org/composyx/kernel/BlasKernels.org::*Eigenvalue decomposition][Eigenvalue decomposition:1]]
  SECTION("Eigenvalue decomposition, general") {
    const DenseMatrix<Scalar> A({1, 2, 3, 4, -3, -13, 7, 8, 3.5}, 3, 3);
    // With this matrix: eigenvalues are
    //0.659471
    //0.420265 + i. 8.0168
    //0.420265 + i. -8.0168

    using EqVect = composyx::EqualsVectorNW<Vector<Complex>>;
    auto [lambdas, U] = blas_kernels::eig(A);

    auto A_cplx = A.template cast<Complex>();

    for (size_t i = 0; i < n_rows(A); ++i) {
      Vector<Complex> AU = A_cplx * U.get_vect_view(i);
      Vector<Complex> lamU = lambdas[i] * U.get_vect_view(i);
      REQUIRE_THAT(AU, EqVect(lamU, ev_tol * 10));
    }

    auto mus = blas_kernels::eigvals(A);

    for (size_t i = 0; i < n_rows(A); ++i) {
      REQUIRE(equals_fp(lambdas[i], mus[i], ev_tol));
    }
  }

  SECTION("Eigenvalue decomposition, symmetric/hermitian case") {
    DenseMatrix<Scalar> A({3, 6, 1, 6, 2, 2, 1, 2, 1}, 3, 3);
    // With this matrix: eigenvalues are
    //-3.667348
    //0.601550
    //9.065798

    if constexpr (is_complex<Scalar>::value) {
      // Add some imaginary part for fun (consistent with hermitian format)
      A += Scalar{0, 1} *
           DenseMatrix<Scalar>({0, -1, 2, 1, 0, 0.5, -2, -0.5, 0}, 3, 3);
    }

    auto [lambdas, U] = blas_kernels::eigh(A);

    for (size_t i = 0; i < n_rows(A); ++i) {
      Vector<Scalar> AU = A * U.get_vect_view(i);
      Vector<Scalar> lamU;
      if constexpr (is_complex<Scalar>::value) {
        Scalar cplx_lam = Scalar{lambdas[i], 0};
        lamU = cplx_lam * U.get_vect_view(i);
      } else {
        lamU = lambdas[i] * U.get_vect_view(i);
      }

      REQUIRE_THAT(AU, EqualsVectorNW(lamU, ev_tol * 10));
    }

    auto mus = blas_kernels::eigvalsh(A);

    for (size_t i = 0; i < n_rows(A); ++i) {
      REQUIRE(equals_fp(lambdas[i], mus[i], ev_tol));
    }
  }
  // Eigenvalue decomposition:1 ends here

  // [[file:../../../org/composyx/kernel/BlasKernels.org::*Eigenvalue decomposition for selected eigenvalues][Eigenvalue decomposition for selected eigenvalues:1]]
  SECTION("EVD (he/sym) with selected eigenvalues") {

    auto test_matrix_he_sym = []() {
      DenseMatrix<Scalar> M({3, 6, 1, 5, 6, 2, 2, 4, 1, 2, 4, 3, 5, 4, 3, 2}, 4,
                            4);
      // With this matrix: eigenvalues are
      //-3.667348
      //0.601550
      //9.065798

      if constexpr (is_complex<Scalar>::value) {
        // Add some imaginary part for fun (consistent with hermitian format)
        M += Scalar{0, 1} * DenseMatrix<Scalar>({0, 1, -2, -1, -1, 0, 2, 0.5, 2,
                                                 -2, 0, -1, 1, -0.5, 1, 0},
                                                4, 4);
      }
      return M;
    };

    const DenseMatrix<Scalar> A(test_matrix_he_sym());

    auto [evref, Uref] = blas_kernels::eigh(A);

    // Check A * U_i = lambda_i * U_i
    auto check_ev = [&A](Vector<Scalar> U_i, Real lambda_i) {
      const Vector<Scalar> AU_i = A * U_i;
      const Vector<Scalar> lam_U_i = Scalar{lambda_i} * U_i;
      return (AU_i - lam_U_i).norm() < ev_tol;
    };

    // Compute middle ev (idx 1 and 2)
    auto [evmid, Umid] = blas_kernels::eigh_select(A, 1, 2);
    REQUIRE(n_cols(Umid) == 2);
    REQUIRE(evmid.size() == 2);
    REQUIRE(equals_fp(evmid[0], evref[1], ev_tol));
    REQUIRE(equals_fp(evmid[1], evref[2], ev_tol));
    REQUIRE(check_ev(Umid.get_vect_view(0), evmid[0]));
    REQUIRE(check_ev(Umid.get_vect_view(1), evmid[1]));

    auto [evsmall, Usmall] = blas_kernels::eigh_smallest(A, 2);
    auto [evlarge, Ularge] = blas_kernels::eigh_largest(A, 2);

    REQUIRE(n_cols(Usmall) == 2);
    REQUIRE(n_cols(Ularge) == 2);
    REQUIRE(evsmall.size() == 2);
    REQUIRE(evlarge.size() == 2);

    REQUIRE(equals_fp(evsmall[0], evref[0], ev_tol));
    REQUIRE(equals_fp(evsmall[1], evref[1], ev_tol));
    REQUIRE(equals_fp(evlarge[0], evref[2], ev_tol));
    REQUIRE(equals_fp(evlarge[1], evref[3], ev_tol));

    REQUIRE(check_ev(Usmall.get_vect_view(0), evsmall[0]));
    REQUIRE(check_ev(Usmall.get_vect_view(1), evsmall[1]));
    REQUIRE(check_ev(Ularge.get_vect_view(0), evlarge[0]));
    REQUIRE(check_ev(Ularge.get_vect_view(1), evlarge[1]));
  }
  // Eigenvalue decomposition for selected eigenvalues:1 ends here

  // [[file:../../../org/composyx/kernel/BlasKernels.org::*Generalized eigenvalue decomposition][Generalized eigenvalue decomposition:1]]
  SECTION("Generalized EVD (he/sym) with selected eigenvalues") {
    const int M = 10;
    const int n_v = 6;

    // We want to find U so that A U = B U LAM
    // With U the n_v eigenvectors associated to the smallest eigenvalues
    auto make_hpd_mat = [=](int seed) {
      DenseMatrix<Scalar> mat(M, M);
      test_matrix::fill_random(mat, seed);
      for (int k = 0; k < M; ++k) {
        mat(k, k) += static_cast<Scalar>(M);
      }
      mat *= mat.h();
      mat.set_spd(MatrixStorage::full);
      if constexpr (is_complex<Scalar>::value) {
        mat.set_hpd(MatrixStorage::full);
      }
      return mat;
    };

    const DenseMatrix<Scalar> A = make_hpd_mat(12345);
    const DenseMatrix<Scalar> B = make_hpd_mat(54321);

    // In case A or B is too big, we multiply tolerance by (norm of A + norm of B) (Frobenius norm)
    Real scal_norm = A.norm() + B.norm();
    //std::cerr << scal_norm << '\n';
    Real my_ev_tol = ev_tol * scal_norm;

    // Check A * U_j = B * U_j * lambda_j
    auto check_gen_ev = [&A, &B, my_ev_tol](const Vector<Scalar>& U_j,
                                            Real lambda_j) {
      const Vector<Scalar> AU_j = A * U_j;
      const Vector<Scalar> lam_BU_j = Scalar{lambda_j} * B * U_j;
      return (AU_j - lam_BU_j).norm() < my_ev_tol;
    };

    auto [evref, Uref] = blas_kernels::gen_eigh_select(A, B, 0, M - 1);
    auto [evsmall, Usmall] = blas_kernels::gen_eigh_smallest(A, B, n_v);
    auto [evlarge, Ularge] = blas_kernels::gen_eigh_largest(A, B, n_v);

    for (int j = 0; j < n_v; ++j) {
      REQUIRE(check_gen_ev(Usmall.get_vect_view(j), evsmall[j]));
      REQUIRE(equals_fp(evsmall[j], evref[j], my_ev_tol));

      REQUIRE(check_gen_ev(Ularge.get_vect_view(j), evlarge[j]));
      REQUIRE(equals_fp(evlarge[j], evref[j + M - n_v], my_ev_tol));
    }
  }
  // Generalized eigenvalue decomposition:1 ends here

  // [[file:../../../org/composyx/kernel/BlasKernels.org::*SVD][SVD:1]]
  SECTION("Singular value decomposition") {
    const size_t M = 3;
    const size_t N = 2;
    const size_t K = 2;

    DenseMatrix<Scalar> A({1, 2, 3, -4, -5, 6}, M, N);
    if constexpr (is_complex<Scalar>::value) {
      // Add some imaginary part for fun
      A += Scalar{0, 1} * DenseMatrix<Scalar>({1, -1, 2, 1, 0, 0.5}, M, N);
    }

    auto check_svd = [&A](const DenseMatrix<Scalar>& l_U,
                          const std::vector<Real>& l_sigmas,
                          const DenseMatrix<Scalar>& l_Vt) {
      // Construct the diagonal matrix Sigma for checking
      DenseMatrix<Scalar> Sigma(n_cols(l_U), n_rows(l_Vt));
      auto l_k = std::min(n_cols(l_U), n_rows(l_Vt));
      for (size_t i = 0; i < l_k; ++i)
        Sigma(i, i) = Scalar{l_sigmas[i]};

      DenseMatrix<Scalar> Acheck = l_U * Sigma * l_Vt;
      return ((A - Acheck).norm() < ev_tol);
    };

    auto check_singvals = [](const std::vector<Real>& s1,
                             const std::vector<Real>& s2) {
      if (s1.size() != s2.size())
        return false;
      for (size_t i = 0; i < s1.size(); ++i) {
        if (std::abs(s1[i] - s2[i]) > ev_tol)
          return false;
      }
      return true;
    };

    // Reduced svd
    const bool reduced = true;
    const bool full = false;

    auto [U, sigmas, Vt] = blas_kernels::svd(A, reduced);
    REQUIRE(n_rows(U) == M);
    REQUIRE(n_cols(U) == K);
    REQUIRE(n_rows(Vt) == K);
    REQUIRE(n_cols(Vt) == N);
    REQUIRE(check_svd(U, sigmas, Vt));

    // Full svd
    auto [U2, sigmas2, Vt2] = blas_kernels::svd(A, full);
    REQUIRE(n_rows(U2) == M);
    REQUIRE(n_cols(U2) == M);
    REQUIRE(n_rows(Vt2) == N);
    REQUIRE(n_cols(Vt2) == N);
    REQUIRE(check_singvals(sigmas2, sigmas));
    REQUIRE(check_svd(U2, sigmas2, Vt2));

    // Full, only left singular vectors U
    auto [U3, sigmas3] = blas_kernels::svd_left_sv(A, full);
    REQUIRE(n_rows(U3) == M);
    REQUIRE(n_cols(U3) == M);
    REQUIRE(check_singvals(sigmas3, sigmas));

    // Full, only right singular vectors Vt
    auto [sigmas4, Vt4] = blas_kernels::svd_right_sv(A, full);
    REQUIRE(n_rows(Vt4) == N);
    REQUIRE(n_cols(Vt4) == N);
    REQUIRE(check_singvals(sigmas4, sigmas));

    // Only singular values
    auto sigmas5 = blas_kernels::svdvals(A);
    REQUIRE(check_singvals(sigmas5, sigmas));
  }
  // SVD:1 ends here

  // [[file:../../../org/composyx/kernel/BlasKernels.org::*Randomized singular value decomposition][Randomized singular value decomposition:1]]
  SECTION("Randomized Singular value decomposition") {
    const size_t m = 10;
    const size_t n = 8;
    const size_t r = 3;

    // generate a rank-K matrix
    DenseMatrix<Scalar> Al =
        test_matrix::random_matrix<Scalar, DenseMatrix<Scalar>>(m * r, Real{0},
                                                                Real{1}, m, r)
            .matrix;
    DenseMatrix<Scalar> Ar =
        test_matrix::random_matrix<Scalar, DenseMatrix<Scalar>>(r * n, Real{0},
                                                                Real{1}, r, n)
            .matrix;
    DenseMatrix<Scalar> A = Al * Ar;

    auto check_svd = [&A](const DenseMatrix<Scalar>& l_U,
                          const std::vector<Real>& l_sigmas,
                          const DenseMatrix<Scalar>& l_Vt) {
      // Construct the diagonal matrix Sigma for checking
      DenseMatrix<Scalar> Sigma(n_cols(l_U), n_rows(l_Vt));
      auto l_k = std::min(n_cols(l_U), n_rows(l_Vt));
      for (size_t i = 0; i < l_k; ++i)
        Sigma(i, i) = Scalar{l_sigmas[i]};

      DenseMatrix<Scalar> Acheck = l_U * Sigma * l_Vt;
      DenseMatrix<Scalar> DiffA = A - Acheck;
      //DiffA.display("DiffA");
      //std::cout << " my_ev_tol = " << my_ev_tol  << std::endl;
      //std::cout << "Norm DiffA = " << DiffA.norm() << std::endl;
      return (DiffA.norm() < ev_tol * A.norm());
    };

    auto check_singvals = [](const std::vector<Real>& s1,
                             const std::vector<Real>& s2) {
      if (s1.size() != s2.size())
        return false;
      Real normvec = 0;
      for (size_t i = 0; i < s1.size(); ++i) {
        normvec += s1[i] * s1[i];
      }
      normvec = sqrt(normvec);
      for (size_t i = 0; i < s1.size(); ++i) {
        if (std::abs(s1[i] - s2[i]) > ev_tol * normvec)
          return false;
      }
      return true;
    };

    // using direct svd method
    auto [U, sigmas, Vt] = blas_kernels::rsvd(A, r);
    REQUIRE(n_rows(U) == m);
    REQUIRE(n_cols(U) == r);
    REQUIRE(n_rows(Vt) == r);
    REQUIRE(n_cols(Vt) == n);
    REQUIRE(check_svd(U, sigmas, Vt));

    // using 0 subspace iteration and two-stages svd method (direct=false)
    auto [U1, sigmas1, Vt1] = blas_kernels::rsvd(A, r, 0, false);
    REQUIRE(check_svd(U1, sigmas1, Vt1));

    // only left singular vectors U
    auto [U2, sigmas2] = blas_kernels::rsvd_left_sv(A, r);
    REQUIRE(n_rows(U2) == m);
    REQUIRE(n_cols(U2) == r);
    REQUIRE(check_singvals(sigmas2, sigmas));

    // only right singular vectors Vt
    auto [sigmas3, Vt3] = blas_kernels::rsvd_right_sv(A, r);
    REQUIRE(n_rows(Vt3) == r);
    REQUIRE(n_cols(Vt3) == n);
    REQUIRE(check_singvals(sigmas3, sigmas));

    // Only singular values
    auto sigmas4 = blas_kernels::rsvdvals(A, r);
    REQUIRE(check_singvals(sigmas4, sigmas));
  }
  // Randomized singular value decomposition:1 ends here

  // [[file:../../../org/composyx/kernel/BlasKernels.org::*Randomized eigen value decomposition][Randomized eigen value decomposition:1]]
  SECTION("Randomized Eigen value decomposition") {
    const size_t n = 10;
    const size_t r = 3;

    // generate a rank-K matrix symmetric/hermitian
    DenseMatrix<Scalar> Al =
        test_matrix::random_matrix<Scalar, DenseMatrix<Scalar>>(n * r, Real{0},
                                                                Real{1}, n, r)
            .matrix;
    DenseMatrix<Scalar> Ar = Al.h();
    DenseMatrix<Scalar> A = Al * Ar;
    if constexpr (is_complex<Scalar>::value) {
      A.set_property(MatrixSymmetry::hermitian);
    } else {
      A.set_property(MatrixSymmetry::symmetric);
    }

    Real scal_norm = A.norm();
    Real my_ev_tol = ev_tol * scal_norm;

    auto check_evd = [&A, my_ev_tol](const std::vector<Real>& l_lambdas,
                                     const DenseMatrix<Scalar>& l_V) {
      bool success_evd = true;
      for (size_t i = 0; i < n_cols(l_V); ++i) {
        Vector<Scalar> AV = A * l_V.get_vect_view(i);
        Vector<Scalar> lamV;
        if constexpr (composyx::is_complex<Scalar>::value) {
          Scalar cplx_lam = Scalar{l_lambdas[i], 0};
          lamV = cplx_lam * l_V.get_vect_view(i);
        } else {
          lamV = l_lambdas[i] * l_V.get_vect_view(i);
        }
        Vector<Scalar> DiffA = AV - lamV;
        //DiffA.display("DiffA");
        //std::cout << "Norm diff " << i << " = " << DiffA.norm() << std::endl;
        if (DiffA.norm() > my_ev_tol)
          success_evd = false;
      }
      return success_evd;
    };

    auto check_eigvals = [](const std::vector<Real>& e1,
                            const std::vector<Real>& e2) {
      if (e1.size() != e2.size())
        return false;
      Real normvec = 0;
      for (size_t i = 0; i < e1.size(); ++i) {
        normvec += e1[i] * e1[i];
      }
      normvec = sqrt(normvec);
      for (size_t i = 0; i < e1.size(); ++i) {
        if (std::abs(e1[i] - e2[i]) > ev_tol * normvec)
          return false;
      }
      return true;
    };

    // using direct evd method
    auto [lambdas, V] = blas_kernels::revd(A, r);
    REQUIRE(n_rows(V) == n);
    REQUIRE(n_cols(V) == r);
    REQUIRE(check_evd(lambdas, V));

    // using 0 subspace iteration
    auto [lambdas1, V1] = blas_kernels::revd(A, r, 0);
    REQUIRE(check_evd(lambdas1, V1));

    // Only eigen values
    auto lambdas2 = blas_kernels::revdvals(A, r);
    REQUIRE(check_eigvals(lambdas2, lambdas));
  }
  // Randomized eigen value decomposition:1 ends here

  // [[file:../../../org/composyx/kernel/BlasKernels.org::*Footer][Footer:1]]
}
// Footer:1 ends here
