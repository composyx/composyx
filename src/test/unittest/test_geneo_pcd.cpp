// [[file:../../../org/composyx/precond/TwoLevelAbstractSchwarz.org::*Test GeneoPCD][Test GeneoPCD:1]]
#include <iostream>
#include <iomanip>

#include <composyx.hpp>
#include <composyx/precond/TwoLevelAbstractSchwarz.hpp>
#include <composyx/solver/BlasSolver.hpp>
#ifdef COMPOSYX_USE_PASTIX
#include <composyx/solver/Pastix.hpp>
#endif // COMPOSYX_USE_PASTIX
#include <composyx/solver/ConjugateGradient.hpp>
#include <composyx/solver/PartSchurSolver.hpp>
#include <composyx/solver/CentralizedSolver.hpp>
#include <composyx/testing/TestMatrix.hpp>

#include <catch2/catch_test_macros.hpp>

using namespace composyx;

using Scalar = double;
using Real = double;
static const Real test_alpha = 0;
static const Real test_beta = 0;
static const Real test_tol = arithmetic_tolerance<Real>::value * 1e11;
static const int test_max_vect = 3;

template <typename PcdType, typename Mat, typename Vect, typename Real>
void test_cg_geneo(const Mat& A, const Vect& b, int n_v) {
  ConjugateGradient<Mat, Vect, PcdType> cg;
  int max_it = std::max(1, static_cast<int>(n_rows(A)));
  max_it = std::min(100, max_it);

  auto setup_geneo = [n_v](const Mat& mat, PcdType& pcd) {
    if (MMPI::rank() == 0)
      std::cout << "NV = " << n_v << '\n';
    pcd.set_n_vect(n_v);
    pcd.set_alpha(test_alpha);
    pcd.set_beta(test_beta);
    pcd.setup(mat);
  };

  auto geneo_initguess = [](const Mat& mat, const Vect& rhs, PcdType& pcd) {
    (void)mat;
    return pcd.init_guess(rhs);
  };

  cg.setup(parameters::A{A}, parameters::max_iter{max_it},
           parameters::tolerance{test_tol},
           parameters::verbose{(MMPI::rank() == 0)},
           parameters::setup_pcd<std::function<void(const Mat&, PcdType&)>>{
               setup_geneo},
           parameters::setup_init_guess<
               std::function<Vect(const Mat&, const Vect&, PcdType&)>>{
               geneo_initguess});

  auto x = cg * b;

  REQUIRE(cg.get_n_iter() != -1);
}

TEST_CASE("GenEO preconditioner (dense)", "[precond][geneo][dense]") {
  using LocSolver = BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>>;
  using PMat = PartMatrix<DenseMatrix<Scalar>>;
  using PVect = PartVector<Vector<Scalar>>;
  using GlobSolver = CentralizedSolver<PMat, PVect, LocSolver>;
  using M0_type = CoarseSolve<DenseMatrix<Scalar>, Vector<Scalar>, GlobSolver>;
  //std::setprecision(std::numeric_limits<Scalar>::digits10 + 1);

  std::shared_ptr<Process> p = test_matrix::get_distr_process();
  const PMat A =
      test_matrix::dist_spd_matrix<Scalar, DenseMatrix<Scalar>>(p).matrix;
  PVect x_exp = test_matrix::dist_vector<Scalar, Vector<Scalar>>(p).vector;
  for (int sd_id : p->get_sd_ids()) {
    x_exp.get_local_vector(sd_id) = Vector<Scalar>{1, 1, 1, 1};
  }
  const PVect b = A * x_exp;

  SECTION("GenEO space generation") {
    auto sds = p->get_sd_ids();
    for (int n_v = 1; n_v <= test_max_vect; ++n_v) {
      NeumannNeumann<PMat, PVect, LocSolver> NN(A);
      PMat U1 = genEO_space<DenseMatrix<Scalar>, Vector<Scalar>, decltype(NN),
                            LocSolver>(A, NN, n_v);
      //U1.display("U1");
      REQUIRE(n_rows(U1) == n_rows(A));
      for (int sd_id : sds) {
        REQUIRE(n_cols(U1.get_local_matrix(sd_id)) == static_cast<size_t>(n_v));
      }

      AdditiveSchwarz<PMat, PVect, LocSolver> AS(A);
      PMat U2 = genEO_space<DenseMatrix<Scalar>, Vector<Scalar>, decltype(AS),
                            LocSolver>(A, AS, n_v);
      //U2.display("U2");
      REQUIRE(n_rows(U2) == n_rows(A));
      for (int sd_id : sds) {
        REQUIRE(n_cols(U2.get_local_matrix(sd_id)) == static_cast<size_t>(n_v));
      }

      RobinRobin<PMat, PVect, LocSolver> RR(A);
      PMat U3 = genEO_space<DenseMatrix<Scalar>, Vector<Scalar>, decltype(RR),
                            LocSolver>(A, RR, n_v);
      //U3.display("U3");
      REQUIRE(n_rows(U3) == n_rows(A));
      for (int sd_id : sds) {
        REQUIRE(n_cols(U3.get_local_matrix(sd_id)) == static_cast<size_t>(n_v));
      }
    }
  }

  SECTION("GenEO additive pcd AS +") {
    using M1_type = AdditiveSchwarz<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, false>;
    for (int n_v = 1; n_v <= test_max_vect; ++n_v) {
      test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
    }
  }

  SECTION("GenEO deflated pcd AS deflated") {
    using M1_type = AdditiveSchwarz<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
    for (int n_v = 1; n_v <= test_max_vect; ++n_v) {
      test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
    }
  }

  SECTION("GenEO deflated pcd NN deflated") {
    using M1_type = NeumannNeumann<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
    for (int n_v = 1; n_v <= test_max_vect; ++n_v) {
      test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
    }
  }

  SECTION("GenEO deflated pcd RR deflated") {
    using M1_type = RobinRobin<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
    for (int n_v = 1; n_v <= test_max_vect; ++n_v) {
      test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
    }
  }
}

// FIXME
/*
  #ifdef COMPOSYX_USE_PASTIX
  TEST_CASE("GenEO preconditioner on the Schur", "[precond][geneo][schur]"){
  using PMat = PartMatrix<SparseMatrixCOO<Scalar>>;
  using PMatDense = PartMatrix<DenseMatrix<Scalar>>;
  using PVect = PartVector<Vector<Scalar>>;
  using DenseSolver = BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>>;
  using SpSolver = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
  using GlobSolver = CentralizedSolver<PMatDense, PVect, DenseSolver>;

  using M0_type = CoarseSolve<DenseMatrix<Scalar>, Vector<Scalar>, GlobSolver>;
  using M1_type = AdditiveSchwarz<PMatDense, PVect, DenseSolver>;
  using Pcd = TwoLevelAbstractSchwarz<PMatDense, PVect, M0_type, M1_type, false>;

  using ItSolver = ConjugateGradient<PMatDense, PVect, Pcd>;
  using Solver = PartSchurSolver<PMat, PVect, SpSolver, ItSolver>;

  std::shared_ptr<Process> p = test_matrix::get_distr_process();
  PMat Atmp = test_matrix::dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
  Atmp.apply_on_data([](SparseMatrixCOO<Scalar>& m){ m.to_storage_half(); });
  const PMat A(std::move(Atmp));
  PVect x_exp = test_matrix::dist_vector<Scalar,Vector<Scalar>>(p).vector;
  for(int sd_id : p->get_sd_ids()){
  x_exp.get_local_vector(sd_id) = Vector<Scalar>{1,1,1,1};
  }
  const PVect b = A * x_exp;

  Solver solver;
  solver.setup(parameters::A{A});

  auto& cg = solver.get_solver_S();

  int max_it = std::max(1, static_cast<int>(n_rows(A)));
  max_it = std::min(100, max_it);
  const int n_v = 2;
  auto setup_geneo = [n_v](const PMatDense& mat, Pcd& pcd){
  if(MMPI::rank() == 0) std::cout << "NV = " << n_v << '\n';
  pcd.set_n_vect(n_v);
  pcd.set_alpha(test_alpha);
  pcd.set_beta(test_beta);
  pcd.setup(mat);
  };

  auto geneo_initguess = [](const PMatDense& mat, const PVect& rhs, Pcd& pcd){
  (void) mat;
  return pcd.init_guess(rhs);
  };

  cg.setup(parameters::max_iter{max_it},
  parameters::tolerance{test_tol},
  parameters::verbose{(MMPI::rank() == 0)},
  parameters::setup_pcd<std::function<void(const PMatDense&, Pcd&)>>{setup_geneo},
  parameters::setup_init_guess<std::function<PVect(const PMatDense&, const PVect&, Pcd&)>>{geneo_initguess});

  auto x = solver * b;

  REQUIRE(cg.get_n_iter() != -1);
  }
  #endif // COMPOSYX_USE_PASTIX
  // */

#ifdef COMPOSYX_USE_ARPACK
#ifdef COMPOSYX_USE_PASTIX
TEST_CASE("GenEO preconditioner (sparse)", "[precond][geneo][sparse]") {
  using PMat = PartMatrix<SparseMatrixCOO<Scalar>>;
  using PVect = PartVector<Vector<Scalar>>;
  using LocSolver = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
  using GlobSolver = CentralizedSolver<PMat, PVect, LocSolver>;
  using M0_type =
      CoarseSolve<SparseMatrixCOO<Scalar>, Vector<Scalar>, GlobSolver>;

  std::shared_ptr<Process> p = test_matrix::get_distr_process();
  PartMatrix<SparseMatrixCOO<Scalar>> Atmp =
      test_matrix::dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
  Atmp.apply_on_data([](SparseMatrixCOO<Scalar>& m) { m.to_storage_half(); });

  const PMat A(std::move(Atmp));
  PVect x_exp = test_matrix::dist_vector<Scalar, Vector<Scalar>>(p).vector;
  for (int sd_id : p->get_sd_ids()) {
    x_exp.get_local_vector(sd_id) = Vector<Scalar>{1, 1, 1, 1};
  }
  const PVect b = A * x_exp;

  SECTION("GenEO additive pcd AS +") {
    using M1_type = AdditiveSchwarz<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, false>;
    for (int n_v = 1; n_v <= test_max_vect; ++n_v) {
      test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
    }
  }

  SECTION("GenEO deflated pcd AS deflated") {
    using M1_type = AdditiveSchwarz<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
    for (int n_v = 1; n_v <= test_max_vect; ++n_v) {
      test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
    }
  }

  SECTION("GenEO deflated pcd NN deflated") {
    using M1_type = NeumannNeumann<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
    for (int n_v = 1; n_v <= test_max_vect; ++n_v) {
      test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
    }
  }

  SECTION("GenEO deflated pcd RR deflated") {
    using M1_type = RobinRobin<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
    for (int n_v = 1; n_v <= test_max_vect; ++n_v) {
      test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v);
    }
  }
}

TEST_CASE("GenEO preconditioner on the Schur", "[precond][geneo][schur]") {
  using PMat = PartMatrix<SparseMatrixCOO<Scalar>>;
  using PMatDense = PartMatrix<DenseMatrix<Scalar>>;
  using PVect = PartVector<Vector<Scalar>>;
  using DenseSolver = BlasSolver<DenseMatrix<Scalar>, Vector<Scalar>>;
  using SpSolver = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
  using GlobSolver = CentralizedSolver<PMatDense, PVect, DenseSolver>;

  using M1_type = AdditiveSchwarz<PMatDense, PVect, DenseSolver>;
  using M0_type = CoarseSolve<typename PMatDense::local_type,
                              typename PVect::local_type, GlobSolver>;
  using Pcd = TwoLevelAbstractSchwarz<PMatDense, PVect, M0_type, M1_type, true>;

  using ItSolver = ConjugateGradient<PMatDense, PVect, Pcd>;
  using Solver = PartSchurSolver<PMat, PVect, SpSolver, ItSolver>;

  std::shared_ptr<Process> p = test_matrix::get_distr_process();
  PMat Atmp =
      test_matrix::dist_spd_matrix<Scalar, SparseMatrixCOO<Scalar>>(p).matrix;
  Atmp.apply_on_data([](SparseMatrixCOO<Scalar>& m) {
    for (size_t k = 0; k < n_rows(m); ++k) {
      m(k, k) += Scalar{10};
    }
    m.to_storage_half();
  });
  const PMat A(std::move(Atmp));
  PVect x_exp = test_matrix::dist_vector<Scalar, Vector<Scalar>>(p).vector;
  for (int sd_id : p->get_sd_ids()) {
    x_exp.get_local_vector(sd_id) = Vector<Scalar>{1, 1, 1, 1};
  }
  const PVect b = A * x_exp;

  Solver solver;
  solver.setup(parameters::A{A});

  auto& cg = solver.get_solver_S();

  int max_it = std::max(1, static_cast<int>(n_rows(A)));
  max_it = std::min(100, max_it);
  const int n_v = 2;
  auto setup_geneo = [](const PMatDense& mat, Pcd& pcd) {
    if (MMPI::rank() == 0)
      std::cout << "NV = " << n_v << '\n';
    pcd.set_n_vect(n_v);
    pcd.set_alpha(test_alpha);
    pcd.set_beta(test_beta);
    pcd.set_tolerance(test_tol);
    pcd.setup(mat);
  };

  auto geneo_initguess = [](const PMatDense&, const PVect& rhs, Pcd& pcd) {
    return pcd.init_guess(rhs);
  };

  cg.setup(parameters::max_iter{max_it}, parameters::tolerance{test_tol},
           parameters::verbose{(MMPI::rank() == 0)},
           parameters::setup_pcd<std::function<void(const PMatDense&, Pcd&)>>{
               setup_geneo},
           parameters::setup_init_guess<
               std::function<PVect(const PMatDense&, const PVect&, Pcd&)>>{
               geneo_initguess});

  auto x = solver * b;

  REQUIRE(cg.get_n_iter() != -1);
}

TEST_CASE("GenEO preconditioner 8 subdomains (sparse)",
          "[precond][geneo][sparse]") {
  using PMat = PartMatrix<SparseMatrixCOO<Scalar>>;
  using PVect = PartVector<Vector<Scalar>>;
  using LocSolver = Pastix<SparseMatrixCOO<Scalar>, Vector<Scalar>>;
  using GlobSolver = CentralizedSolver<PMat, PVect, LocSolver>;
  using M0_type =
      CoarseSolve<SparseMatrixCOO<Scalar>, Vector<Scalar>, GlobSolver>;

  const std::string path_to_part = "@COMPOSYX_MATRIX_PARTITIONS_8@";
  const int n_subdomains = 8;
  //Timer<0>::set_debug(true);
  // Subdomain topology
  std::shared_ptr<Process> p = bind_subdomains(n_subdomains);

  // Load matrix and RHS
  PMat Atmp(p);
  PVect x_exp(p);
  load_subdomains_and_data(path_to_part, n_subdomains, p, Atmp, x_exp);
  const PMat A = std::move(Atmp);
  x_exp.apply_on_data([](Vector<Scalar>& v) {
    for (size_t k = 0; k < n_rows(v); ++k) {
      v[k] = Scalar{1};
    }
  });
  const PartVector<Vector<Scalar>> b = A * x_exp;
  const int n_v_big = 5;

  SECTION("GenEO additive pcd AS +") {
    using M1_type = AdditiveSchwarz<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, false>;
    test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v_big);
  }

  // Too long for unit tests
  /*
    SECTION("GenEO deflated pcd NN deflated"){
    using M1_type = NeumannNeumann<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M0_type, M1_type, true>;
    test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v_big);
    }

    SECTION("GenEO deflated pcd AS deflated"){
    using M1_type = AdditiveSchwarz<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M1_type, GlobSolver, LocSolver, true>;
    test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v_big);
    }

    SECTION("GenEO deflated pcd RR deflated"){
    using M1_type = RobinRobin<PMat, PVect, LocSolver>;
    using Pcd = TwoLevelAbstractSchwarz<PMat, PVect, M1_type, GlobSolver, LocSolver, true>;
    test_cg_geneo<Pcd, PMat, PVect, Real>(A, b, n_v_big);
    }*/
}
#endif // COMPOSYX_USE_PASTIX
#endif // COMPOSYX_USE_ARPACK
// Test GeneoPCD:1 ends here
