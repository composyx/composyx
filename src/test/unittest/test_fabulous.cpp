// [[file:../../../org/composyx/solver/Fabulous.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>

#include <composyx.hpp>
#include <composyx/solver/Fabulous.hpp>
#include <composyx/precond/DiagonalPrecond.hpp>
#include <composyx/loc_data/SparseMatrixCOO.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

using namespace composyx;
TEMPLATE_TEST_CASE("GCR", "[GCR][iterative][sequential]", double,
                   std::complex<float>, std::complex<double>) {
  using Scalar = TestType;
  using SpMat = composyx::SparseMatrixCOO<Scalar>;
  using Vector = Vector<Scalar>;
  using DMat = DenseMatrix<Scalar>;
  using Real = typename arithmetic_real<Scalar>::type;

  const std::vector<Real> tol{arithmetic_tolerance<Scalar>::value};
  const int max_iter = 1000;
  const bool verbose = true;

  const SpMat A = test_matrix::general_matrix_4_4<SpMat>();
  const Vector x_exp = test_matrix::simple_vector<Vector>();

  DMat X_exp;
  // Add an imaginary part if complex
  if constexpr (is_complex<Scalar>::value) {
    DMat X_exp_tmp({{1.0, 0.0},
                    {1.0, 1.0},
                    {1.0, 3.0},
                    {1.0, 2.0},
                    {1.0, 2.0},
                    {2.0, 0.0},
                    {1.0, 0.0},
                    {3.0, -1.0},
                    {-1.0, 0.0},
                    {0.0, 0.0},
                    {0.0, 0.0},
                    {10.0, 0.0}},
                   4, 3);
    X_exp = std::move(X_exp_tmp);
  } else {
    DMat X_exp_tmp(
        {1.0, 1.0, 1.0, 1.0, 1.0, 2.0, 1.0, 3.0, -1.0, 0.0, 0.0, 10.0}, 4, 3);
    X_exp = std::move(X_exp_tmp);
  }

  const Vector b = A * x_exp;
  const DMat B = A * X_exp;
  SECTION("composyx++ <-> fabulous, conserve data") {
    DMat Xcomposyx = X_exp;
    fabulous::Block<Scalar> Xf = convert_to_fabulous(Xcomposyx);
    for (int i = 0; i < Xf.get_nb_row(); i++) {
      for (int j = 0; j < Xf.get_nb_col(); j++) {
        REQUIRE(Xf(i, j) == X_exp(i, j));
        DMat Xcomposyx2 = convert_to_composyx<DMat>(Xf);
        REQUIRE(Xcomposyx2 == X_exp);
      }
    }
  }

  SECTION("composyx++ <-> fabulous, conserve modifications") {
    DMat Xcomposyx = Scalar{3.0} * X_exp;
    fabulous::Block<Scalar> Xf = convert_to_fabulous(Xcomposyx);
    for (int i = 0; i < Xf.get_nb_row(); i++) {
      for (int j = 0; j < Xf.get_nb_col(); j++) {
        REQUIRE(Xf(i, j) == Scalar{3.0} * X_exp(i, j));
        Xf(i, j) = Scalar{2.0} * Xf(i, j);
      }
    }

    DMat Xcomposyx2 = convert_to_composyx<DMat>(Xf);
    REQUIRE(Xcomposyx2 == static_cast<DMat>(Scalar{6.0} * X_exp));
  }

  SECTION("Fabulous with single rhs") {
    Fabulous<SpMat, Vector> fab;
    fab.setup(fabulous_params::A<SpMat>{A},
              fabulous_params::tolerances<std::vector<Real>>{tol},
              fabulous_params::max_iter<int>{max_iter},
              fabulous_params::verbose<bool>{verbose});
    auto x = fab * b;
    x.display("x");
    x_exp.display("x_exp");
    REQUIRE(fab.get_n_iter() > 0);
  }

  SECTION("Fabulous with multiple rhs") {
    Fabulous<SpMat, DMat> fab;
    fab.setup(fabulous_params::A<SpMat>{A},
              fabulous_params::tolerances<std::vector<Real>>{tol},
              fabulous_params::max_iter<int>{max_iter},
              fabulous_params::verbose<bool>{verbose});
    auto X = fab * B;
    X.display("X");
    X_exp.display("X_exp");
    REQUIRE(fab.get_n_iter() > 0);
  }

  SECTION("Fabulous with single rhs + diag pcd") {
    using PcdType = DiagonalPrecond<SpMat, Vector>;
    Fabulous<SpMat, Vector, PcdType> fab;

    fab.setup(fabulous_params::A<SpMat>{A},
              fabulous_params::tolerances<std::vector<Real>>{tol},
              fabulous_params::max_iter<int>{max_iter},
              fabulous_params::verbose<bool>{verbose});
    auto x = fab * b;
    x.display("x");
    x_exp.display("x_exp");
    REQUIRE(fab.get_n_iter() > 0);
  }

  SECTION("Fabulous with multiple rhs + diag pcd") {
    using PcdType = DiagonalPrecond<SpMat, DMat>;
    Fabulous<SpMat, DMat, PcdType> fab;

    fab.setup(fabulous_params::A<SpMat>{A},
              fabulous_params::tolerances<std::vector<Real>>{tol},
              fabulous_params::max_iter<int>{max_iter},
              fabulous_params::verbose<bool>{verbose});
    auto X = fab * B;
    REQUIRE(fab.get_n_iter() > 0);
  }

  SECTION("Fabulous with multiple rhs & deflated restart & ortho block & CGS") {
    Fabulous<SpMat, DMat, Identity<SpMat, DMat>, fabulous::bgmres::ARNOLDI_IBDR,
             fabulous::DeflatedRestart<Scalar>>
        fab;
    fab.setup(fabulous_params::A<SpMat>{A},
              fabulous_params::tolerances<std::vector<Real>>{tol},
              fabulous_params::max_iter<int>{max_iter},
              fabulous_params::verbose<bool>{verbose},
              fabulous_params::ortho_type{fabulous::OrthoType::BLOCK},
              fabulous_params::ortho_scheme{fabulous::OrthoScheme::CGS},
              fabulous_params::deflated_restart{
                  fabulous::deflated_restart<Scalar>(2, Scalar{0})});
    auto X = fab * B;
    X.display("X");
    X_exp.display("X_exp");
    REQUIRE(fab.get_n_iter() > 0);
  }
}
// Tests:1 ends here
