// [[file:../../../org/composyx/solver/GMRES.org::*Compression][Compression:1]]
#include <composyx.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#ifdef COMPOSYX_USE_SZ_COMPRESSOR
#include <composyx/utils/SZ_compressor.hpp>
#endif
#ifdef COMPOSYX_USE_ZFP_COMPRESSOR
#include <composyx/utils/ZFP_compressor.hpp>
#endif
#include <composyx/loc_data/CompressedBasis.hpp>
#include <composyx/solver/GMRES.hpp>

using namespace composyx;

template <typename Solver> void setup_gmres(Solver& solver) {
  solver.setup(parameters::tolerance<double>{1e-5},
               parameters::max_iter<int>{300}, parameters::restart<int>{300},
               parameters::compression_accuracy<double>{1e-7},
               parameters::orthogonalization<Ortho>{Ortho::MGS2},
               parameters::verbose<bool>{true});
}

TEST_CASE("GMRES", "[GMRES][iterative][compression]") {
  using Scalar = std::complex<double>;

  const std::string mat_path = "../../../../matrices/young1c.mtx";

  SparseMatrixCSC<Scalar> A;
  A.from_matrix_market_file(mat_path);
  const size_t M = n_rows(A);
  Vector<Scalar> x_exp(M);
  for (size_t k = 0; k < M; ++k)
    x_exp[k] = 1;

  Vector<Scalar> b = A * x_exp;

  using Precond = Identity<SparseMatrixCSC<Scalar>, Vector<Scalar>>;

#ifdef COMPOSYX_USE_SZ_COMPRESSOR
  SZ_compressor_init();

  SECTION("With SZ") {
    using SZ_Basis =
        CompressedBasis<SZ_compressor<Scalar, SZ_CompressionMode::POINTWISE>>;
    GMRES<SparseMatrixCSC<Scalar>, Vector<Scalar>, Precond, SZ_Basis> gmres(A);
    setup_gmres(gmres);

    Vector<Scalar> x = gmres * b;
    double diffnorm = (x - x_exp).norm();
    std::cout << "|| x* - x || = " << diffnorm << '\n';
    REQUIRE(gmres.get_n_iter() > 0);
  }

  SZ_compressor_finalize();
#endif

#ifdef COMPOSYX_USE_ZFP_COMPRESSOR
  SECTION("With ZFP") {
    using ZFP_Basis =
        CompressedBasis<ZFP_compressor<Scalar, ZFP_CompressionMode::ACCURACY>>;
    GMRES<SparseMatrixCSC<Scalar>, Vector<Scalar>, Precond, ZFP_Basis> gmres(A);
    setup_gmres(gmres);

    Vector<Scalar> x = gmres * b;
    double diffnorm = (x - x_exp).norm();
    std::cout << "|| x* - x || = " << diffnorm << '\n';
    REQUIRE(gmres.get_n_iter() > 0);
  }
#endif
}
// Compression:1 ends here
