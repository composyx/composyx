// [[file:../../../org/composyx/loc_data/ETDenseMatrix.org::*Tests][Tests:1]]
#include <iostream>
#include <vector>
#include <complex>

#include <composyx.hpp>
#include <composyx/loc_data/DenseMatrix.hpp>
#include <composyx/testing/TestMatrix.hpp>

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <composyx/testing/Catch2DenseMatrixMatchers.hpp>

TEMPLATE_TEST_CASE("Expression templates - dense matrices", "[densematrix, ET]",
                   float, double, std::complex<float>, std::complex<double>) {
  using namespace composyx;

  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;
  using Vector = composyx::Vector<Scalar>;
  using Matrix = composyx::DenseMatrix<Scalar, -1>;

  using EqualsVectorPW = composyx::EqualsVectorPW<Vector>;
  using EqualsMatrixPW = composyx::EqualsMatrixPW<Matrix>;

  const size_t M = 4;

  auto rand_mat = [=](const int seed) {
    Matrix mat(M, M);
    test_matrix::fill_random(mat, seed);
    return mat;
  };

  const Matrix A0 = rand_mat(123);
  const Matrix B0 = rand_mat(321);
  const Matrix C0 = rand_mat(12354);
  const Matrix D0 = rand_mat(123143);

  const Vector x0 = test_matrix::random_vector<Vector>(Real{-1}, Real{1}, M);
  const Vector y0 = test_matrix::random_vector<Vector>(Real{-1}, Real{1}, M);
  const Vector z0 = test_matrix::random_vector<Vector>(Real{-1}, Real{1}, M);

  const Vector abc = test_matrix::random_vector<Vector>(Real{-1}, Real{1}, 3);
  const Scalar alpha = abc[0];
  const Scalar beta = abc[1];
  const Scalar gamma = abc[2];

  // Needed to pass some tests in double precision
  const double tol_ten =
      composyx::arithmetic_tolerance<typename Matrix::scalar_type>::value * 10;

  // Transpose vectors and matrices
  SECTION("y = x.t()") {
    Vector xt = x0;
    xt.transpose();
    Vector y = x0.t();
    REQUIRE_THAT(y, EqualsVectorPW(xt));
  }

  SECTION("x = x.t()") {
    Vector xt = x0;
    xt.transpose();
    Vector x = x0;
    x = x.t();
    REQUIRE_THAT(x, EqualsVectorPW(xt));
  }

  SECTION("y = x.h()") {
    Vector xh = x0;
    xh.conj_transpose();
    Vector y = x0.h();
    REQUIRE_THAT(y, EqualsVectorPW(xh));
  }

  SECTION("B = A.t()") {
    Matrix At = A0;
    At.transpose();
    Matrix B = A0.t();
    REQUIRE_THAT(B, EqualsMatrixPW(At));
  }

  SECTION("B = A.h()") {
    Matrix Ah = A0;
    Ah.conj_transpose();
    Matrix B = A0.h();
    REQUIRE_THAT(B, EqualsMatrixPW(Ah));
  }

  // Nothing to do
  SECTION("y = x.t().t()") {
    const Vector y = x0.t().t();
    REQUIRE_THAT(y, EqualsVectorPW(x0));
  }
  SECTION("y = x.h().h()") {
    const Vector y = x0.t().t();
    REQUIRE_THAT(y, EqualsVectorPW(x0));
  }

  SECTION("B = A.t().t()") {
    const Vector B = A0.t().t();
    REQUIRE_THAT(B, EqualsMatrixPW(A0));
  }

  SECTION("B = A.h().h()") {
    const Vector B = A0.h().h();
    REQUIRE_THAT(B, EqualsMatrixPW(A0));
  }

  // Scaling
  SECTION("x = alpha * x") {
    Vector xref = x0;
    COMPOSYX_BLAS::scal(xref, alpha);
    Vector x = x0;
    x = alpha * x;
    REQUIRE_THAT(x, EqualsVectorPW(xref));
  }

  SECTION("y = alpha * x") {
    Vector xref = x0;
    COMPOSYX_BLAS::scal(xref, alpha);
    Vector y = alpha * x0;
    REQUIRE_THAT(y, EqualsVectorPW(xref));
  }

  SECTION("y = x * alpha") {
    Vector xref = x0;
    COMPOSYX_BLAS::scal(xref, alpha);
    Vector y = x0 * alpha;
    REQUIRE_THAT(y, EqualsVectorPW(xref));
  }

  SECTION("A = alpha * A") {
    Matrix Aref = A0;
    for (size_t j = 0; j < n_cols(A0); ++j) {
      auto Aj = Aref.get_vect_view(j);
      COMPOSYX_BLAS::scal(Aj, alpha);
    }
    Matrix A = A0;
    A = alpha * A;
    REQUIRE_THAT(A, EqualsMatrixPW(Aref));
  }

  SECTION("B = alpha * A") {
    Matrix Aref = A0;
    for (size_t j = 0; j < n_cols(A0); ++j) {
      auto Aj = Aref.get_vect_view(j);
      COMPOSYX_BLAS::scal(Aj, alpha);
    }

    Matrix B = alpha * A0;
    REQUIRE_THAT(B, EqualsMatrixPW(Aref));
  }

  SECTION("B = A * alpha") {
    Matrix Aref = A0;
    for (size_t j = 0; j < n_cols(A0); ++j) {
      auto Aj = Aref.get_vect_view(j);
      COMPOSYX_BLAS::scal(Aj, alpha);
    }

    Matrix B = A0 * alpha;
    REQUIRE_THAT(B, EqualsMatrixPW(Aref));
  }

  // Scaling and transpose
  SECTION("y = alpha * x.t()") {
    Vector xref = x0;
    COMPOSYX_BLAS::scal(xref, alpha);
    xref.transpose();
    Vector y = alpha * x0.t();
    REQUIRE_THAT(y, EqualsVectorPW(xref));
  }

  SECTION("B = alpha * A.t()") {
    Matrix Aref = A0;
    for (size_t j = 0; j < n_cols(A0); ++j) {
      auto Aj = Aref.get_vect_view(j);
      COMPOSYX_BLAS::scal(Aj, alpha);
    }

    Aref.transpose();
    Matrix B = alpha * A0.t();
    REQUIRE_THAT(B, EqualsMatrixPW(Aref));
  }

  SECTION("A *= A") {
    Matrix A1 = A0;
    Matrix A2 = A0;
    Matrix Aref = A0;
    Aref.gemm(A1, A2, Op::NoTrans, Op::NoTrans);

    Matrix A = A0;
    A *= A;

    REQUIRE_THAT(A, EqualsMatrixPW(Aref));
  }

  SECTION("A *= A.h()") {
    const Matrix A1 = A0;
    const Matrix A2 = A0;
    Matrix Aref = A0;
    Aref.gemm(A1, A2, Op::NoTrans, Op::ConjTrans);

    Matrix A = A0;
    A *= A.h();

    REQUIRE_THAT(A, EqualsMatrixPW(Aref));
  }

  // Addition, substraction
  SECTION("z = x + alpha * y") {
    Vector zref = x0;
    COMPOSYX_BLAS::axpy(y0, zref, alpha);

    Vector z = x0 + alpha * y0;
    REQUIRE_THAT(z, EqualsVectorPW(zref));
  }

  SECTION("z = x - alpha * y") {
    Vector zref = x0;
    COMPOSYX_BLAS::axpy(y0, zref, Scalar{-1} * alpha);

    Vector z = x0 - alpha * y0;
    REQUIRE_THAT(z, EqualsVectorPW(zref));
  }

  SECTION("z = alpha * x + y") {
    Vector zref = y0;
    COMPOSYX_BLAS::axpy(x0, zref, alpha);

    Vector z = alpha * x0 + y0;
    REQUIRE_THAT(z, EqualsVectorPW(zref));
  }

  SECTION("z = alpha * x - y") {
    Vector zref = y0;
    COMPOSYX_BLAS::axpy(x0, zref, Scalar{-1} * alpha);
    zref *= Scalar{-1};

    Vector z = alpha * x0 - y0;
    REQUIRE_THAT(z, EqualsVectorPW(zref));
  }

  SECTION("C = A + alpha * B") {
    Matrix Cref = A0;
    for (size_t j = 0; j < n_cols(A0); ++j) {
      const auto Bj = B0.get_vect_view(j);
      auto Cj = Cref.get_vect_view(j);
      COMPOSYX_BLAS::axpy(Bj, Cj, alpha);
    }

    Matrix C = A0 + alpha * B0;
    REQUIRE_THAT(C, EqualsMatrixPW(Cref, tol_ten));
  }

  SECTION("C = A - alpha * B") {
    Matrix Cref = A0;
    for (size_t j = 0; j < n_cols(A0); ++j) {
      const auto Bj = B0.get_vect_view(j);
      auto Cj = Cref.get_vect_view(j);
      COMPOSYX_BLAS::axpy(Bj, Cj, Scalar{-1} * alpha);
    }

    Matrix C = A0 - alpha * B0;
    REQUIRE_THAT(C, EqualsMatrixPW(Cref, tol_ten));
  }

  SECTION("C = alpha * B + A") {
    Matrix Cref = A0;
    for (size_t j = 0; j < n_cols(A0); ++j) {
      const auto Bj = B0.get_vect_view(j);
      auto Cj = Cref.get_vect_view(j);
      COMPOSYX_BLAS::axpy(Bj, Cj, alpha);
    }

    Matrix C = alpha * B0 + A0;
    REQUIRE_THAT(C, EqualsMatrixPW(Cref, tol_ten));
  }

  SECTION("C = alpha * B - A") {
    Matrix Cref = A0 * Scalar{-1};
    for (size_t j = 0; j < n_cols(A0); ++j) {
      const auto Bj = B0.get_vect_view(j);
      auto Cj = Cref.get_vect_view(j);
      COMPOSYX_BLAS::axpy(Bj, Cj, alpha);
    }

    Matrix C = alpha * B0 - A0;
    REQUIRE_THAT(C, EqualsMatrixPW(Cref, tol_ten));
  }

  // Implicit dot product
  SECTION("alpha = x.t() * y") {
    Scalar sref = x0.dotu(y0);
    Scalar s = x0.t() * y0;
    REQUIRE(sref == s);
  }

  SECTION("alpha = x.h() * y") {
    Scalar sref = x0.dot(y0);
    Scalar s = x0.h() * y0;
    REQUIRE(sref == s);
  }

  // Outer product (geru)
  SECTION("A = x * y.t()") {
    Matrix Aref = Matrix(x0) * Matrix(y0.t());
    Matrix A = x0 * y0.t();
    REQUIRE_THAT(A, EqualsMatrixPW(Aref));
  }

  SECTION("A += x * y.t()") {
    Matrix Aref = A0;
    Aref += Matrix(x0) * Matrix(y0.t());
    Matrix A = A0;
    A += x0 * y0.t();
    REQUIRE_THAT(A, EqualsMatrixPW(Aref));
  }

  // Matrix vector product
  SECTION("y = A * x") {
    Vector yref = x0;
    COMPOSYX_BLAS::gemv(A0, x0, yref);
    Vector y = A0 * x0;
    REQUIRE_THAT(y, EqualsVectorPW(yref));
  }

  SECTION("x = A * x") {
    Vector xref = x0;
    COMPOSYX_BLAS::gemv(A0, x0, xref);
    Vector x = x0;
    x = A0 * x;
    REQUIRE_THAT(x, EqualsVectorPW(xref));
  }

  SECTION("y = A.t() * x") {
    Vector yref = x0;
    COMPOSYX_BLAS::gemv(A0, x0, yref, 'T');
    Vector y = A0.t() * x0;
    REQUIRE_THAT(y, EqualsVectorPW(yref));
  }

  SECTION("y = A.h() * x") {
    Vector yref = x0;
    COMPOSYX_BLAS::gemv(A0, x0, yref, 'C');
    Vector y = A0.h() * x0;
    REQUIRE_THAT(y, EqualsVectorPW(yref));
  }

  // GEMV
  SECTION("y = A * x + y") {
    Vector yref = y0;
    COMPOSYX_BLAS::gemv(A0, x0, yref, 'N', Scalar{1}, Scalar{1});

    Vector y = A0 * x0 + y0;
    REQUIRE_THAT(y, EqualsVectorPW(yref));
  }

  SECTION("x = A * x + y") {
    Vector xref = y0;
    COMPOSYX_BLAS::gemv(A0, x0, xref, 'N', Scalar{1}, Scalar{1});

    Vector x = x0;
    x = A0 * x + y0;
    REQUIRE_THAT(x, EqualsVectorPW(xref));
  }

  SECTION("y = A.t() * x + y") {
    Vector yref = y0;
    COMPOSYX_BLAS::gemv(A0, x0, yref, 'T', Scalar{1}, Scalar{1});

    Vector y = A0.t() * x0 + y0;
    REQUIRE_THAT(y, EqualsVectorPW(yref));
  }

  SECTION("y = alpha * A * x + y") {
    Vector yref = y0;
    COMPOSYX_BLAS::gemv(A0, x0, yref, 'N', alpha, Scalar{1});

    Vector y = alpha * A0 * x0 + y0;
    REQUIRE_THAT(y, EqualsVectorPW(yref));
  }

  SECTION("y = A * x + beta y") {
    Vector yref = y0;
    COMPOSYX_BLAS::gemv(A0, x0, yref, 'N', Scalar{1}, beta);

    Vector y = A0 * x0 + beta * y0;
    REQUIRE_THAT(y, EqualsVectorPW(yref));
  }

  SECTION("y = alpha A.t() * x + beta y") {
    Vector yref = y0;
    COMPOSYX_BLAS::gemv(A0, x0, yref, 'T', alpha, beta);

    Vector y = alpha * A0.t() * x0 + beta * y0;
    REQUIRE_THAT(y, EqualsVectorPW(yref));
  }

  // Consecutive GEMVs
  SECTION("z = alpha A.t() * x + beta * B * y + gamma * C * z") {
    Vector zref = x0;
    COMPOSYX_BLAS::gemv(A0, x0, zref, 'T', alpha);           // alpha A.t() * x
    COMPOSYX_BLAS::gemv(B0, y0, zref, 'N', beta, Scalar{1}); // += beta * B * y
    COMPOSYX_BLAS::gemv(C0, z0, zref, 'N', gamma,
                        Scalar{1}); // += gamma * C * z

    Vector z = alpha * A0.t() * x0 + beta * B0 * y0 + gamma * C0 * z0;
    REQUIRE_THAT(z, EqualsVectorPW(zref));
  }

  SECTION("C = A * B") {
    Matrix Cref(n_rows(A0), n_cols(B0));
    Cref.gemm(A0, B0, Op::NoTrans, Op::NoTrans);

    Matrix C = A0 * B0;
    REQUIRE_THAT(C, EqualsMatrixPW(Cref));
  }

  SECTION("C = A.h() * B") {
    Matrix Cref(n_rows(A0), n_cols(B0));
    Cref.gemm(A0, B0, Op::ConjTrans, Op::NoTrans);

    Matrix C = A0.h() * B0;
    REQUIRE_THAT(C, EqualsMatrixPW(Cref));
  }

  SECTION("C = A * B.h()") {
    Matrix Cref(n_rows(A0), n_cols(B0));
    Cref.gemm(A0, B0, Op::NoTrans, Op::ConjTrans);

    Matrix C = A0 * B0.h();
    REQUIRE_THAT(C, EqualsMatrixPW(Cref));
  }

  SECTION("C = A * A.h()") {
    Matrix Cref(n_rows(A0), n_cols(A0));
    Cref.gemm(A0, A0, Op::NoTrans, Op::ConjTrans);

    Matrix C = A0 * A0.h();
    REQUIRE_THAT(C, EqualsMatrixPW(Cref));
  }

  SECTION("C = alpha A * B + beta C") {
    Matrix Cref = C0;
    Cref.gemm(A0, B0, Op::NoTrans, Op::NoTrans, alpha, beta);

    Matrix C = C0;
    C = alpha * A0 * B0 + beta * C;
    REQUIRE_THAT(C, EqualsMatrixPW(Cref));
  }

  SECTION("G = alpha A * B + beta C + D") {
    Matrix Gref = C0;
    Gref.gemm(A0, B0, Op::NoTrans, Op::NoTrans, alpha, beta);
    for (size_t j = 0; j < n_cols(Gref); ++j) {
      const auto Dj = D0.get_vect_view(j);
      auto Gj = Gref.get_vect_view(j);
      COMPOSYX_BLAS::axpy(Dj, Gj, Scalar{1});
    }

    Matrix G = alpha * A0 * B0 + beta * C0 + D0;
    REQUIRE_THAT(G, EqualsMatrixPW(Gref));
  }

  SECTION("G = alpha A * B + beta C * D + gamm * E * F") {
    const Matrix E0 =
        test_matrix::random_matrix<Scalar, Matrix>(16, Real{-1}, Real{1}, 4, 4)
            .matrix;
    const Matrix F0 =
        test_matrix::random_matrix<Scalar, Matrix>(16, Real{-1}, Real{1}, 4, 4)
            .matrix;

    Matrix Gref(n_rows(A0), n_cols(B0));
    Gref.gemm(A0, B0, Op::NoTrans, Op::NoTrans, alpha, Scalar{0});
    Gref.gemm(C0, D0, Op::NoTrans, Op::NoTrans, beta, Scalar{1});
    Gref.gemm(E0, F0, Op::NoTrans, Op::NoTrans, gamma, Scalar{1});

    Matrix G = alpha * A0 * B0 + beta * C0 * D0 + gamma * E0 * F0;
    REQUIRE_THAT(G, EqualsMatrixPW(Gref));
  }

  SECTION("D = A * B * C") {
    Matrix Dref(n_rows(A0), n_cols(C0));
    Matrix A0B0(n_rows(A0), n_cols(B0));
    A0B0.gemm(A0, B0, Op::NoTrans, Op::NoTrans);
    Dref.gemm(A0B0, C0, Op::NoTrans, Op::NoTrans);

    Matrix D = A0 * B0 * C0;
    REQUIRE_THAT(D, EqualsMatrixPW(Dref));
  }

  SECTION("D = \alpha * A.h() * B * C") {
    Matrix Dref(n_rows(A0), n_cols(C0));
    Matrix A0B0(n_rows(A0), n_cols(B0));
    A0B0.gemm(A0, B0, Op::ConjTrans, Op::NoTrans, alpha);
    Dref.gemm(A0B0, C0, Op::NoTrans, Op::NoTrans);

    Matrix D = alpha * A0.h() * B0 * C0;
    REQUIRE_THAT(D, EqualsMatrixPW(Dref));
  }
}
// Tests:1 ends here
