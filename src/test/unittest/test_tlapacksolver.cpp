// [[file:../../../org/composyx/solver/TlapackSolver.org::*Tests][Tests:1]]
#include <composyx.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include <composyx/loc_data/DenseMatrix.hpp>
#include <composyx/solver/TlapackSolver.hpp>

using namespace composyx;

TEMPLATE_TEST_CASE("TlapackSolver", "[dense][sequential][blas][lapack]", float,
                   double, std::complex<float>, std::complex<double>) {
  using Scalar = TestType;
  using Real = typename arithmetic_real<Scalar>::type;
  using Mat = DenseMatrix<Scalar>;
  using Vect = Vector<Scalar>;

  const Mat A({1.0, -1.0, 2.0, -2.0, 3.0, -3.0}, 2, 3);
  const Mat B({1.0, 2.0, 3.0, 4.0, 5.0, 6.0}, 3, 2);

  const Real tol = arithmetic_tolerance<Scalar>::value;

  SECTION("tlapack gemm") {
    //            1 4
    //  1  2  3 x 2 5 =  14  32
    // -1 -2 -3   3 6   -14 -32

    const Mat C_check({14.0, -14.0, 32.0, -32.0}, 2, 2);
    Mat C(n_rows(A), n_cols(B));

    tlapack_kernels::gemm(A, B, C);
    REQUIRE(C == C_check);
  }

  SECTION("tlapack gemv") {
    // 1 4        3
    // 2 5 x -1 = 3
    // 3 6    1   3
    const Vect x({-1, 1});
    const Vect sol_check({3, 3, 3});
    Vect sol(n_rows(B));

    tlapack_kernels::gemv(B, x, sol);
    REQUIRE(sol_check == sol);
  }

  SECTION("Solve square general system") {
    Mat A2 =
        test_matrix::random_matrix<Scalar, Mat>(16, Real{-2}, Real{2}, 4, 4)
            .matrix;
    for (int k = 0; k < static_cast<int>(n_rows(A2)); ++k)
      A2(k, k) += Scalar{5};
    const Mat A2_cpy = A2;

    const Vect rhs{1, 2, 3, 4};
    TlapackSolver<Mat, Vect> solver(A2);
    auto X = solver * rhs;

    Vect check(4);
    tlapack_kernels::gemv(A2_cpy, X, check);
    REQUIRE((rhs - check).norm() < tol);
  }

  SECTION("Solve square general system step by step") {
    Mat A2 =
        test_matrix::random_matrix<Scalar, Mat>(16, Real{-2}, Real{2}, 4, 4)
            .matrix;
    for (int k = 0; k < static_cast<int>(n_rows(A2)); ++k)
      A2(k, k) += Scalar{5};
    const Mat A2_cpy = A2;

    const Vect rhs{1, 2, 3, 4};

    TlapackSolver<Mat, Vect> solver(A2);
    solver.factorize();
    Vect X = solver.apply_facto_permutation(rhs);
    solver.triangular_solve(X, MatrixStorage::lower);
    solver.triangular_solve(X, MatrixStorage::upper);

    Vect check(4);
    tlapack_kernels::gemv(A2_cpy, X, check);
    REQUIRE((rhs - check).norm() < tol);
  }

  SECTION("Solve square spd") {
    Mat A2 =
        test_matrix::random_matrix<Scalar, Mat>(16, Real{-2}, Real{2}, 4, 4)
            .matrix;
    for (int k = 0; k < static_cast<int>(n_rows(A2)); ++k)
      A2(k, k) += Scalar{5};
    Mat A3 = A2 * A2.t();
    A3.set_spd(MatrixStorage::full);
    const Mat A3_cpy = A2;

    const Vect rhs{1, 2, 3, 4};
    TlapackSolver<Mat, Vect> solver(A2);
    auto X = solver * rhs;

    Vect check(4);
    tlapack_kernels::gemv(A3_cpy, X, check);
    REQUIRE((rhs - check).norm() < tol);
  }
  // Tests:1 ends here

  // [[file:../../../org/composyx/solver/TlapackSolver.org::*Overdetermined system][Overdetermined system:1]]
  SECTION("Least square solve (A m x n with m > n)") {
    const Mat Ar({1, 3, 5, 2, 4, 6}, 3, 2);
    const Vect b({-1, 0, 1});
    const Vect x_exp({2, -1.5});

    TlapackSolver<Mat, Vect> solver(Ar);
    Vect x = solver * b;

    const Real large_tol = (is_precision_double<Scalar>::value) ? 1e-6 : 1e-2;
    REQUIRE((x - x_exp).norm() < large_tol);
  }
  // Overdetermined system:1 ends here

  // [[file:../../../org/composyx/solver/TlapackSolver.org::*Underdetermined system][Underdetermined system:1]]
  SECTION("Minimal norm (A m x n with m < n)") {
    const Mat Ar({1, 2, 3, 4, 5, 6}, 2, 3);
    const Vect b({-1, 2});
    const Vect x_exp({3.5, 1, -1.5});

    TlapackSolver<Mat, Vect> solver(Ar);
    Vect x = solver * b;

    const Real large_tol = (is_precision_double<Scalar>::value) ? 1e-6 : 1e-1;
    REQUIRE((x - x_exp).norm() < large_tol);
  }
  // Underdetermined system:1 ends here

  // [[file:../../../org/composyx/solver/TlapackSolver.org::*Footer][Footer:1]]
}
// Footer:1 ends here
