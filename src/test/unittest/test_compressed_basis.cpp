// [[file:../../../org/composyx/loc_data/CompressedBasis.org::*Tests][Tests:1]]
#include <iostream>
#include <random>

#include <composyx.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

#include "composyx/loc_data/CompressedBasis.hpp"

using namespace composyx;

template <typename CprBasis> void test([[maybe_unused]] std::string desc) {

  using Scalar = CprBasis::scalar_type;
  using Real = typename arithmetic_real<Scalar>::type;
  using ComprParam = CprBasis::compression_param_type;

  const size_t M = 2000;
  ComprParam compression_param;
  const double tolerance = 1e-4;

  if constexpr (std::is_same_v<ComprParam, double>) {
    compression_param = 1e-4;
  }
  if constexpr (std::is_same_v<ComprParam, unsigned int>) {
    compression_param = 14;
  }

  using Real = typename arithmetic_real<Scalar>::type;
  std::random_device rd;
  std::mt19937 gen(rd());
  Real min = -1;
  Real max = 1;
  std::uniform_real_distribution<> dis(min, max);
  auto random = [&gen, &dis]() { return static_cast<Real>(dis(gen)); };

  auto generate_elts = [random](int n) {
    Vector<Scalar> cpr(n);
    for (size_t i = 0; i < n_rows(cpr); ++i) {
      if constexpr (is_complex<Scalar>::value) {
        cpr[i] = Scalar{random(), random()};
      } else {
        cpr[i] = Scalar{random()};
      }
    }
    return cpr;
  };

  auto pwdiff = [](const Vector<Scalar>& v_ex, const Vector<Scalar>& v_cpr) {
    Real max_diff = 0;
    for (size_t k = 0; k < n_rows(v_ex); ++k) {
      Real diff = std::abs(v_ex[k] - v_cpr[k]);
      //std::cout << v_ex[k] << " | " <<  v_cpr[k] << " -> " << diff << '\n';
      max_diff = std::max(max_diff, diff);
    }
    return max_diff;
  };

  SECTION("CTor with dense matrix") {
    const size_t N = 5;
    DenseMatrix<Scalar> mat(M, N);
    for (size_t j = 0; j < N; ++j) {
      mat.get_vect_view(j) = generate_elts(M);
    }
    CprBasis basis(mat, compression_param);

    for (size_t j = 0; j < N; ++j) {
      Vector<Scalar> vect = basis.get_vect(j);
      Vector<Scalar> matview = mat.get_vect_view(j);
      auto diff = pwdiff(matview, vect);
      std::cout << "Max diff:" << diff << '\n';
      REQUIRE(diff <= tolerance);
    }
  }

  SECTION("Test with individual vectors") {
    CprBasis basis(3, compression_param);

    Vector<Scalar> u1 = generate_elts(M);
    Vector<Scalar> u2 = generate_elts(M);
    Vector<Scalar> u3 = generate_elts(M);

    basis.set_vector(u1, 0);
    basis.set_vector(u2, 1);
    basis.set_vector(u3, 2);
    Vector<Scalar> ut1 = basis.get_vect(0);
    Vector<Scalar> ut2 = basis.get_vect_view(1);
    Vector<Scalar> ut3 = basis.get_vect_view(2);

    REQUIRE(pwdiff(u1, ut1) < tolerance);
    REQUIRE(pwdiff(u2, ut2) < tolerance);
    REQUIRE(pwdiff(u3, ut3) < tolerance);
  }

  SECTION("Basis x vector") {
    const size_t N = 10;

    DenseMatrix<Scalar> mat(M, N);
    for (size_t k = 0; k < N; ++k) {
      mat.get_vect_view(k) = generate_elts(M);
    }

    const CprBasis basis(mat, compression_param);
    const Vector<Scalar> vect = generate_elts(N);
    const Vector<Scalar> mv_nocomp = mat * vect;
    const Vector<Scalar> mv_comp = basis * vect;

    auto diff = (mv_nocomp - mv_comp).norm();
    std::cout << "Max diff:" << diff << '\n';
    REQUIRE(diff < (N * M * tolerance));
  }
}

TEMPLATE_TEST_CASE("Compressed basis", "[compressor]", float, double,
                   std::complex<float>, std::complex<double>) {
  using Scalar = TestType;

#if defined(COMPOSYX_USE_SZ_COMPRESSOR)
  SZ_compressor_init();

  test<CompressedBasis<SZ_compressor<Scalar, SZ_CompressionMode::POINTWISE>>>(
      "SZ PW");
  test<CompressedBasis<SZ_compressor<Scalar, SZ_CompressionMode::NORMWISE>>>(
      "SZ NW");
#endif

#if defined(COMPOSYX_USE_ZFP_COMPRESSOR)
  test<CompressedBasis<ZFP_compressor<Scalar, ZFP_CompressionMode::ACCURACY>>>(
      "ZFP ACC");
  test<CompressedBasis<ZFP_compressor<Scalar, ZFP_CompressionMode::PRECISION>>>(
      "ZFP PREC");
  test<CompressedBasis<ZFP_compressor<Scalar, ZFP_CompressionMode::RATE>>>(
      "ZFP RATE");
#endif

#if defined(COMPOSYX_USE_SZ_COMPRESSOR)
  SZ_compressor_finalize();
#endif
}
// Tests:1 ends here
