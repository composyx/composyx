// [[file:../../../org/composyx/linalg/MatrixNorm.org::*Test][Test:1]]
#include <iostream>
#include <lapack.hh>
#include <composyx.hpp>
#include <composyx/loc_data/DenseMatrix.hpp>
#include <composyx/loc_data/SparseMatrixCOO.hpp>
#include <composyx/solver/BlasSolver.hpp>
#include <composyx/kernel/BlasKernels.hpp>
#include <composyx/linalg/MatrixNorm.hpp>
#include <composyx/testing/TestMatrix.hpp>

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

using namespace composyx;

template <typename Scalar,
          typename Real = typename composyx::arithmetic_real<Scalar>::type>
Real matrix_norm_ref(const DenseMatrix<Scalar>& mat) {
  auto sing_vals = blas_kernels::svdvals(mat);
  return sing_vals[0];
}

TEMPLATE_TEST_CASE("MatrixNorm", "[matrix_norm]", float, double,
                   std::complex<float>, std::complex<double>) {

  using Scalar = TestType;
  //using Scalar = double;
  using Real = typename composyx::arithmetic_real<Scalar>::type;

  const int M = 1000;
  const int NNZ = 3333;
  const int NB_TESTS = 1;

  for (int i = 0; i < NB_TESTS; ++i) {

    DenseMatrix<Scalar> A =
        test_matrix::random_matrix<Scalar, DenseMatrix<Scalar>>(NNZ, Real{-10},
                                                                Real{10}, M, M)
            .matrix;
    const Real ref_norm = matrix_norm_ref(A);
    //std::cout << "-----------> " << ref_norm << '\n';
    Real norm = approximate_mat_norm(A);
    Real rel_err = (ref_norm - norm) / ref_norm;
    //std::cout << "Dense: Norm -> " << norm <<  '\n';
    std::cout << "Rel err -> " << rel_err << '\n';
    //REQUIRE(rel_err < 1); // Relative error lower than 1 -> correct order of magnitude

    SparseMatrixCOO<Scalar> Asparse(A);
    norm = approximate_mat_norm(Asparse);
    rel_err = (ref_norm - norm) / ref_norm;
    //std::cout << "Spars: Norm -> " << norm <<  '\n';
    std::cout << "Rel err -> " << rel_err << '\n';
    REQUIRE(rel_err < 1);
  }
}
// Test:1 ends here
