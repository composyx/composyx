// [[file:../../../org/composyx/solver/GMRES.org::*Sequential][Sequential:1]]
#include <iostream>
#include <vector>

#ifdef COMPOSYX_USE_EIGEN
#include <composyx/wrappers/Eigen/Eigen_header.hpp>
#endif
#ifdef COMPOSYX_USE_ARMA
#include <composyx/wrappers/armadillo/Armadillo_header.hpp>
#endif
#ifdef COMPOSYX_USE_EIGEN
#include <composyx/wrappers/Eigen/Eigen.hpp>
#endif
#ifdef COMPOSYX_USE_ARMA
#include <composyx/wrappers/armadillo/Armadillo.hpp>
#endif

#include <composyx.hpp>
#include <composyx/solver/GMRES.hpp>
#include <composyx/precond/DiagonalPrecond.hpp>
#include <composyx/testing/TestMatrix.hpp>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>
#include "test_iterative_solver.hpp"

using namespace composyx;

template <typename Scalar, typename Matrix>
void test(const std::string mat_type) {
  using Vect = typename vector_type<Matrix>::type;

  SECTION(std::string("GMRES, no preconditioner, matrix ") + mat_type) {
    test_solver<Scalar, Matrix, Vect, GMRES<Matrix, Vect>>(
        test_matrix::general_matrix_4_4<Matrix>(),
        test_matrix::simple_vector<Vect>(), 10, true);
  }

  SECTION(std::string("GMRES, diagonal preconditioner, matrix ") + mat_type) {
    using PcdType = DiagonalPrecond<Matrix, Vect>;
    test_solver<Scalar, Matrix, Vect, GMRES<Matrix, Vect, PcdType>>(
        test_matrix::general_matrix_4_4<Matrix>(),
        test_matrix::simple_vector<Vect>(), 10, true);
  }
}

TEMPLATE_TEST_CASE("GMRES", "[GMRES][iterative][sequential]", float, double,
                   std::complex<float>, std::complex<double>) {
  using Scalar = TestType;

  test<Scalar, DenseMatrix<Scalar>>("DenseMatrix");
  test<Scalar, SparseMatrixCOO<Scalar>>("SparseMatrixCOO");
  test<Scalar, SparseMatrixCSC<Scalar>>("SparseMatrixCSC");
  test<Scalar, SparseMatrixCSR<Scalar>>("SparseMatrixCSR");

#ifdef COMPOSYX_USE_EIGEN
  using EigenDenseMatrix =
      Eigen::Matrix<Scalar, Eigen::Dynamic, Eigen::Dynamic>;
  using EigenSparseMatrix = Eigen::SparseMatrix<Scalar>;

  test<Scalar, EigenDenseMatrix>("Eigen dense");
  test<Scalar, EigenSparseMatrix>("Eigen sparse");
#endif // COMPOSYX_USE_EIGEN

#ifdef COMPOSYX_USE_ARMA
  test<Scalar, arma::Mat<Scalar>>("Armadillo dense");
  test<Scalar, arma::SpMat<Scalar>>("Armadillo sparse");
#endif

} // TEMPLATE_TEST_CASE
// Sequential:1 ends here
