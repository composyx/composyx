#include <iostream>
#include <vector>

#include <composyx/wrappers/armadillo/Armadillo_header.hpp>
#include <composyx/wrappers/armadillo/Armadillo.hpp>

#include <composyx.hpp>
#include <composyx/solver/ConjugateGradient.hpp>

using namespace composyx;
using namespace arma;

int main() {

  Mat<double> A = {{2.0, -1.0, 0.0, 0.0},
                   {-1.0, 2.0, -1.0, 0.0},
                   {0.0, -1.0, 2.0, -1.0},
                   {0.0, 0.0, -1.0, 2.0}};

  Col<double> b = {1.0, 0.0, 0.0, 11.0};

  ConjugateGradient<Mat<double>, Col<double>> cg(A);
  Col<double> X = cg * b;

  X.print();

  return 0;
}
