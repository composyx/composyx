#include <iostream>
#include <fstream>
#include <vector>

#include <random>

#include <composyx.hpp>
#include <composyx/loc_data/SparseMatrixLIL.hpp>
#include <composyx/solver/Pastix.hpp>
#include <composyx/solver/Mumps.hpp>

using namespace composyx;
using Scalar = double;

SparseMatrixLIL<double> matrix_random(const int N, const int N_RANDOM_COEFF,
                                      bool only_lower = false) {
  std::random_device rd; // To get seed
  std::mt19937 gen(rd());
  std::uniform_real_distribution<> random_coeff(-1.0, 1.0);
  std::uniform_real_distribution<> random_index(0.0, static_cast<double>(N));

  SparseMatrixLIL<double> Alil(N, N);

  // Insert diagonal elements
  // + 1 extra-diagonal
  for (int k = 0; k < N; ++k) {
    double v = random_coeff(gen) + 10.0;
    Alil.insert(k, k, v);
    if (k < N - 1)
      Alil.insert(k + 1, k, v / 10);
  }

  for (int k = 0; k < N_RANDOM_COEFF; ++k) {
    int i = static_cast<int>(std::ceil(random_index(gen) - 1));
    int j = static_cast<int>(std::ceil(random_index(gen) - 1));
    double v = random_coeff(gen);
    if (only_lower) {
      Alil.insert(std::max(i, j), std::min(i, j), v);
    } else {
      Alil.insert(i, j, v);
    }
  }

  return Alil;
}

SparseMatrixCOO<double> matrix_general(const int N, const int N_RANDOM_COEFF) {
  return matrix_random(N, N_RANDOM_COEFF).to_coo();
}

SparseMatrixCOO<double> matrix_symmetric(const int N,
                                         const int N_RANDOM_COEFF) {
  SparseMatrixCOO<double> A = matrix_random(N, N_RANDOM_COEFF, true).to_coo();
  A.set_property(MatrixSymmetry::symmetric, MatrixStorage::lower);
  return A;
}

SparseMatrixCOO<double> matrix_spd(const int N, const int N_RANDOM_COEFF) {
  DenseMatrix<double> Adense =
      matrix_random(N, N_RANDOM_COEFF, true).to_dense();

  Adense *= Adense.t();

  SparseMatrixCOO<double> A(Adense);
  A.set_property(MatrixSymmetry::symmetric);
  A.to_storage_half(MatrixStorage::lower);
  A.set_spd(MatrixStorage::lower);

  return A;
}

int main() {

  const int N = 1000;
  const int N_RANDOM_COEFF = N * 3;

  const std::vector<int> schurlist{1, 3, 5, 7, 13};

#if defined(COMPOSYX_USE_RSB)
  if (rsb_lib_init(RSB_NULL_INIT_OPTIONS) != RSB_ERR_NO_ERROR)
    return -1;
  std::cout << "Solving with librsb sparse matrix\n\n";
#endif

  MMPI::init();
  {
    for (int k_test = 0; k_test < 3; ++k_test) {
      SparseMatrixCOO<double> A;

      if (k_test == 0) {
        A = matrix_general(N, N_RANDOM_COEFF);
        std::cout << "\n### General ###\n";
      } else if (k_test == 1) {
        A = matrix_symmetric(N, N_RANDOM_COEFF);
        std::cout << "\n### Symmetric ###\n";
      } else {
        A = matrix_spd(N, N_RANDOM_COEFF);
        std::cout << "\n### SPD ###\n";
      }

      const Vector<double> x_ones = Vector<double>::ones(N);
      Vector<double> B = A * x_ones;

      Pastix<SparseMatrixCOO<double>, Vector<double>> pastix(A);
      pastix.setup(A);
      pastix.spm_info();

      Mumps<SparseMatrixCOO<double>, Vector<double>> mumps(A);

      Vector<double> x_pastix = pastix * B;

      Vector<double> x_mumps = mumps * B;

      Vector<double> direct_diff = x_pastix - x_mumps;

      double be_pastix = (B - A * x_pastix).norm() / B.norm();
      double be_mumps = (B - A * x_mumps).norm() / B.norm();

      std::cout << "BE pastix: " << be_pastix << '\n';
      std::cout << "BE mumps: " << be_mumps << '\n';

      std::cout << "Diff: " << direct_diff.norm() << '\n';

      DenseMatrix<double> schur_pastix = pastix.get_schur(schurlist);
      DenseMatrix<double> schur_mumps = mumps.get_schur(schurlist);

      schur_pastix.display("schur_pastix");

      std::cout << "Diff schur: " << (schur_pastix - schur_mumps).norm()
                << '\n';
    }
  }
  MMPI::finalize();

#if defined(COMPOSYX_USE_RSB)
  if (rsb_lib_exit(RSB_NULL_EXIT_OPTIONS) != RSB_ERR_NO_ERROR)
    return -1;
#endif
}
