#include <iostream>

#include <composyx.hpp>
#include <composyx/loc_data/DenseMatrix.hpp>
#include <composyx/part_data/PartMatrix.hpp>

// Example with no interior dof
// With 3 subdomains, 3 dofs, all connected

int main() {

  using Scalar = double;
  // using Real = double;

  using namespace composyx;

  MMPI::init();
  {
    using Nei_map = std::map<int, std::vector<int>>;
    const int n_subdomains = 3;

    // SD 0
    Nei_map NM0{{1, {0, 1, 2}}, {2, {0, 1, 2}}};

    // SD 1
    Nei_map NM1{{0, {0, 1, 2}}, {2, {0, 1, 2}}};

    // SD 2
    Nei_map NM2{{0, {0, 1, 2}}, {1, {0, 1, 2}}};

    const int n_g = 3;

    std::vector<Subdomain> sd_intrf;
    sd_intrf.push_back(Subdomain(0, n_g, std::move(NM0), true));
    sd_intrf.push_back(Subdomain(1, n_g, std::move(NM1), true));
    sd_intrf.push_back(Subdomain(2, n_g, std::move(NM2), true));

    std::shared_ptr<Process> proc = bind_subdomains(n_subdomains);
    proc->load_subdomains_interface(sd_intrf);

    const bool row_major = true;
    /*
    DenseMatrix<Scalar> A_glob({1, 2, 3,
                                4, 5, 6,
                                7, 8, 9}, 3, 3, row_major);
    */

    Vector<Scalar> x_glob{5, 4, 3};

    std::map<int, Vector<Scalar>> m_vect;
    if (proc->owns_subdomain(0))
      m_vect.emplace(0, x_glob);
    if (proc->owns_subdomain(1))
      m_vect.emplace(1, x_glob);
    if (proc->owns_subdomain(2))
      m_vect.emplace(2, x_glob);
    PartVector<Vector<Scalar>> x(proc, m_vect, true);

    // For the sake of example, we put the diagonal term k on SD k
    // and extra-diagonal terms (k, l) shared between SD k and SD l
    std::map<int, DenseMatrix<Scalar>> m_mat;
    if (proc->owns_subdomain(0)) {
      m_mat.emplace(0, DenseMatrix<Scalar>(
                           {1, 2. / 2, 3. / 2, 4. / 2, 0, 0, 7. / 2, 0, 0}, 3,
                           3, row_major));
    }
    if (proc->owns_subdomain(1)) {
      m_mat.emplace(1, DenseMatrix<Scalar>(
                           {0, 2. / 2, 0, 4. / 2, 5, 6. / 2, 0, 8. / 2, 0}, 3,
                           3, row_major));
    }
    if (proc->owns_subdomain(2)) {
      m_mat.emplace(2, DenseMatrix<Scalar>(
                           {0, 0, 3. / 2, 0, 0, 6. / 2, 7. / 2, 8. / 2, 9}, 3,
                           3, row_major));
    }
    PartMatrix<DenseMatrix<Scalar>> A(proc, m_mat, true);

    x.display_centralized("x");
    A.display_centralized("A");

    auto y = A * x;

    y.display_centralized("y = A x");
  } // Make sure all created communicators are freed before MPI_Finalize

  MMPI::finalize();

  return 0;
}
