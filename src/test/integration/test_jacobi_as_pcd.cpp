#include <iostream>
#include <fstream>
#include <vector>

#include <random>

#include <composyx.hpp>
#include <composyx/solver/GMRES.hpp>
#include <composyx/solver/Jacobi.hpp>

using namespace composyx;
using Scalar = double;

int main() {

  const int N = 1000;

  // Create dense matrix
  // With bigger coeffs on the diagonal but not diagonal dominant

  // std::random_device rd;  //To get seed
  std::mt19937 gen(64125);
  std::uniform_real_distribution<> random_extra(0.1, 1.0);
  std::uniform_real_distribution<> random_diag(100.0, 1000.0);

  auto make_mat = [&]() {
    DenseMatrix<Scalar> mat(N, N);
    for (int i = 0; i < N; ++i) {
      for (int j = 0; j < N; ++j) {
        if (i == j) {
          mat(i, j) = random_diag(gen);
        } else {
          mat(i, j) = random_extra(gen);
        }
      }
    }
    return mat;
  };

  // Create random RHS
  auto make_rhs = [&]() {
    Vector<Scalar> rhs(N);
    for (int i = 0; i < N; ++i) {
      rhs(i) = random_extra(gen) * 10;
    }
    return rhs;
  };

  const DenseMatrix<Scalar> A = make_mat();
  const Vector<Scalar> b = make_rhs();

  // Use GMRES for solving
  GMRES<DenseMatrix<Scalar>, Vector<Scalar>> solver(A);
  solver.setup(parameters::max_iter{N}, parameters::tolerance{1e-8},
               parameters::verbose{false});

  auto X = solver * b;

  solver.display("Without precond");

  auto setup_jacobi = [](const DenseMatrix<Scalar>& mat,
                         Jacobi<DenseMatrix<Scalar>, Vector<Scalar>>& jac) {
    jac.setup(parameters::A{mat}, parameters::fixed_iter{3},
              parameters::verbose{true});
  };

  GMRES<DenseMatrix<Scalar>, Vector<Scalar>,
        Jacobi<DenseMatrix<Scalar>, Vector<Scalar>>>
      solver_J3(A);
  solver_J3.setup(
      parameters::max_iter<int>{4}, parameters::tolerance<double>{1e-8},
      parameters::verbose{true},
      parameters::setup_pcd<
          std::function<void(const DenseMatrix<Scalar>&,
                             Jacobi<DenseMatrix<Scalar>, Vector<Scalar>>&)>>{
          setup_jacobi});

  auto X2 = solver_J3 * b;

  solver_J3.display("With J3 as pcd");

  return 0;
}
