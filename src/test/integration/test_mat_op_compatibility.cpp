#include <iostream>
#include <fstream>
#include <vector>
#include <random>

#include <composyx.hpp>
#include <composyx/loc_data/DenseMatrix.hpp>
#include <composyx/loc_data/SparseMatrixLIL.hpp>
#include <composyx/loc_data/SparseMatrixCSR.hpp>
#include <composyx/loc_data/SparseMatrixCSC.hpp>
#include <composyx/loc_data/SparseMatrixCOO.hpp>
#include <composyx/testing/TestMatrix.hpp>

using namespace composyx;
using Scalar = double;

const size_t M = 8; //32;
const size_t N = 8; //24;

#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_template_test_macros.hpp>

using namespace composyx;

[[nodiscard]] DenseMatrix<Scalar> make_matrix(double sparsity_ratio){
  int nnz_max = static_cast<int>(static_cast<double>(M * N) * sparsity_ratio);
  DenseMatrix<Scalar> mat(test_matrix::random_matrix<Scalar, DenseMatrix<Scalar>>(nnz_max, double{-1}, double{1}, M, N).matrix);

  /*
  size_t nnz = 0;
  for(size_t j = 0; j < N; ++j){
    for(size_t i = 0; i < M; ++i){
      if(mat(i, j) != double{0}) nnz++;
    }
  }
  std::cout << "Sparsity: " << (double{100} * static_cast<double>(nnz) / double{M * N}) << "%\n";
  */

  return mat;
}

[[nodiscard]] DenseMatrix<Scalar> make_upper_matrix(double sparsity_ratio){
  DenseMatrix<Scalar> out = make_matrix(sparsity_ratio);
  for(size_t j = 0; j < N; ++j){
    for(size_t i = j; i < M; ++i){
      if(j < M and i < N) { out(j, i) = out(i, j); }
      out(i, j) = double{0};
    }
  }
  out.set_property(MatrixSymmetry::symmetric, MatrixStorage::upper);
  return out;
}

TEMPLATE_PRODUCT_TEST_CASE("Composyx matrix compatibility", "[sparse][matrix]", (SparseMatrixCOO, SparseMatrixCSC, SparseMatrixCSR), (Scalar)){
  using SpMat = TestType;

  const DenseMatrix<Scalar> DenseA = make_matrix(0.5);
  const DenseMatrix<Scalar> DenseB = make_matrix(0.3);
  const DenseMatrix<Scalar> DenseBt = DenseB.t();

  const DenseMatrix<Scalar> Dense_AplusB = DenseA + DenseB;
  const DenseMatrix<Scalar> Dense_AminusB = DenseA - DenseB;
  const DenseMatrix<Scalar> Dense_AtimesBt = DenseA * DenseBt;

  const SpMat A(DenseA);

  const DenseMatrix<Scalar> DenseB_up = make_upper_matrix(0.5);
  const DenseMatrix<Scalar> Dense_AplusB_up = DenseA + DenseB_up;
  const DenseMatrix<Scalar> Dense_AminusB_up = DenseA - DenseB_up;
  const DenseMatrix<Scalar> Dense_B_upminusA = DenseB_up - DenseA;
  const DenseMatrix<Scalar> Dense_2B_up = DenseB_up + DenseB_up;

  const SpMat Bup(DenseB_up);

  const double tolerance = double{1e-15};
  /*
  SECTION("Addition"){
    auto ApB_coo = A + SparseMatrixCOO<Scalar>(DenseB);
    REQUIRE((ApB_coo.to_dense() - Dense_AplusB).norm() < tolerance);
    auto ApB_csc = A + SparseMatrixCSC<Scalar>(DenseB);
    REQUIRE((ApB_csc.to_dense() - Dense_AplusB).norm() < tolerance);
    auto ApB_csr = A + SparseMatrixCSR<Scalar>(DenseB);
    REQUIRE((ApB_csr.to_dense() - Dense_AplusB).norm() < tolerance);
  }

  SECTION("Substraction"){
    auto ApB_coo = A - SparseMatrixCOO<Scalar>(DenseB);
    REQUIRE((ApB_coo.to_dense() - Dense_AminusB).norm() < tolerance);
    auto ApB_csc = A - SparseMatrixCSC<Scalar>(DenseB);
    REQUIRE((ApB_csc.to_dense() - Dense_AminusB).norm() < tolerance);
    auto ApB_csr = A - SparseMatrixCSR<Scalar>(DenseB);
    REQUIRE((ApB_csr.to_dense() - Dense_AminusB).norm() < tolerance);
  }

  SECTION("Multiplication"){
    auto ApB_coo = A * SparseMatrixCOO<Scalar>(DenseBt);
    REQUIRE((ApB_coo.to_dense() - Dense_AtimesBt).norm() < tolerance);
    auto ApB_csc = A * SparseMatrixCSC<Scalar>(DenseBt);
    REQUIRE((ApB_csc.to_dense() - Dense_AtimesBt).norm() < tolerance);
    auto ApB_csr = A * SparseMatrixCSR<Scalar>(DenseBt);
    REQUIRE((ApB_csr.to_dense() - Dense_AtimesBt).norm() < tolerance);
  }
  */
  SECTION("Addition with half storage rhs"){
    auto ApBup_coo = A + SparseMatrixCOO<Scalar>(DenseB_up);
    REQUIRE((ApBup_coo.to_dense() - Dense_AplusB_up).norm() < tolerance);
    auto ApBup_csc = A + SparseMatrixCSC<Scalar>(DenseB_up);
    REQUIRE((ApBup_coo.to_dense() - Dense_AplusB_up).norm() < tolerance);
    auto ApBup_csr = A + SparseMatrixCSR<Scalar>(DenseB_up);
    REQUIRE((ApBup_coo.to_dense() - Dense_AplusB_up).norm() < tolerance);
  }

  SECTION("Substraction with half storage rhs"){
    auto ApBup_coo = A - SparseMatrixCOO<Scalar>(DenseB_up);
    REQUIRE((ApBup_coo.to_dense() - Dense_AminusB_up).norm() < tolerance);
    auto ApBup_csc = A - SparseMatrixCSC<Scalar>(DenseB_up);
    REQUIRE((ApBup_coo.to_dense() - Dense_AminusB_up).norm() < tolerance);
    auto ApBup_csr = A - SparseMatrixCSR<Scalar>(DenseB_up);
    REQUIRE((ApBup_coo.to_dense() - Dense_AminusB_up).norm() < tolerance);
  }

  SECTION("Addition with half storage lhs"){
    auto AuppB_coo = Bup + SparseMatrixCOO<Scalar>(DenseA);
    REQUIRE((AuppB_coo.to_dense() - Dense_AplusB_up).norm() < tolerance);
    auto AuppB_csc = Bup + SparseMatrixCSC<Scalar>(DenseA);
    REQUIRE((AuppB_csc.to_dense() - Dense_AplusB_up).norm() < tolerance);
    auto AuppB_csr = Bup + SparseMatrixCSR<Scalar>(DenseA);
    REQUIRE((AuppB_csr.to_dense() - Dense_AplusB_up).norm() < tolerance);
  }

  SECTION("Substraction with half storage lhs"){
    auto AuppB_coo = Bup - SparseMatrixCOO<Scalar>(DenseA);
    REQUIRE((AuppB_coo.to_dense() - Dense_B_upminusA).norm() < tolerance);
    auto AuppB_csc = Bup - SparseMatrixCSC<Scalar>(DenseA);
    REQUIRE((AuppB_csc.to_dense() - Dense_B_upminusA).norm() < tolerance);
    auto AuppB_csr = Bup - SparseMatrixCSR<Scalar>(DenseA);
    REQUIRE((AuppB_csr.to_dense() - Dense_B_upminusA).norm() < tolerance);
  }

  SECTION("Addition upper + upper"){
    auto Bup2_coo = Bup + SparseMatrixCOO<Scalar>(DenseB_up);
    REQUIRE((Bup2_coo.to_dense() - Dense_2B_up).norm() < tolerance);
    REQUIRE(Bup2_coo.is_storage_upper());
    auto Bup2_csc = Bup + SparseMatrixCSC<Scalar>(DenseB_up);
    REQUIRE((Bup2_csc.to_dense() - Dense_2B_up).norm() < tolerance);
    REQUIRE(Bup2_csc.is_storage_upper());
    auto Bup2_csr = Bup + SparseMatrixCSR<Scalar>(DenseB_up);
    REQUIRE((Bup2_csr.to_dense() - Dense_2B_up).norm() < tolerance);
    REQUIRE(Bup2_csr.is_storage_upper());
  }
} // TEMPLATE_PRODUCT_TEST_CASE
