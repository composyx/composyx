#ifdef COMPOSYX_USE_RSB_SPBLAS
#include <rsb.h>         /* for rsb_lib_init */
#include <blas_sparse.h> /* Sparse BLAS on the top of librsb */
#endif

#include <stdio.h> /* for saying bye bye */
#include <composyx.hpp>
#include <composyx/kernel/spkernel.hpp>
#include <composyx/solver/ConjugateGradient.hpp>

#ifdef COMPOSYX_USE_RSB
#include <composyx/wrappers/librsb/Librsb.hpp>
#endif

int main() {

#ifdef COMPOSYX_USE_RSB
  if (rsb_lib_init(RSB_NULL_INIT_OPTIONS) != RSB_ERR_NO_ERROR)
    return -1;
  std::cout << "Solving with librsb sparse matrix\n\n";

#else
#ifdef COMPOSYX_USE_RSB_SPBLAS
  if (rsb_lib_init(RSB_NULL_INIT_OPTIONS) != RSB_ERR_NO_ERROR)
    return -1;

  std::cout << "Solving with librsb sparse blas\n\n";
#else
  std::cout << "Solving with sparse matrix\n\n";
#endif
#endif

  // Find X in AX = B system:
  //  2 -1  0  0    3     1
  // -1  2 -1  0    5     0
  //  0 -1  2 -1  x 7  =  0
  //  0  0 -1  2    9    11

  // AB =
  //  2
  // -1
  // -11
  //  22

  using Scalar = double;
  using Real = double;

  // Compose classes
#ifdef COMPOSYX_USE_RSB
  using MPH_SP_mat = composyx::RsbSparseMatrix<Scalar>;
#else
  using MPH_SP_mat = composyx::SparseMatrixCOO<Scalar>;
#endif
  using MPH_vect = composyx::Vector<Scalar>;

  // CG parameters
  const int max_iter = 10; // 4 should be enough
  const Real tolerance = Real{1e-5};
  const bool verbose = true;

  const MPH_vect X_expected{3.0, 5.0, 7.0, 9.0};
  const MPH_vect B{1.0, 0.0, 0.0, 11.0};

  /*  MPH_SP_mat A2({  0,    0,   1,    1,   2,    2,   3},
                {  0,    1,   1,    2,   2,    3,   3},
                {2.0, -1.0, 2.0, -1.0, 2.0, -1.0, 2.0},
                4, 4);
 */
  composyx::SparseMatrixCOO<Scalar> A1(
      {0, 0, 1, 1, 2, 2, 3}, {0, 1, 1, 2, 2, 3, 3},
      {2.0, -1.0, 2.0, -1.0, 2.0, -1.0, 2.0}, 4, 4);
  A1.set_spd(composyx::MatrixStorage::lower);
#ifdef COMPOSYX_USE_RSB
  MPH_SP_mat A2 = convert(A1);
#else
  MPH_SP_mat A2 = A1;
#endif

  /*  std::vector<int> I{  0,    0,   1,    1,   2,    2,   3};
  std::vector<int> J{  0,    1,   1,    2,   2,    3,   3};
  std::vector<Scalar> V{2.0, -1.0, 2.0, -1.0, 2.0, -1.0, 2.0};
  rsb::RsbMatrix<Scalar>test(I.data(), J.data(), V.data(), V.size(),
  RSB_FLAG_SYMMETRIC); std::cout << "yes\n"; MPH_SP_mat A2(I.data(), J.data(),
  V.data(), V.size(), RSB_FLAG_SYMMETRIC);*/

  // For composyx to be detected with the parameters
  A2.set_spd(composyx::MatrixStorage::lower);

  composyx::ConjugateGradient<MPH_SP_mat, MPH_vect> cg_mph_sparse;
  cg_mph_sparse.setup(composyx::parameters::A<MPH_SP_mat>{A2},
                      composyx::parameters::max_iter<int>{max_iter},
                      composyx::parameters::tolerance<Real>{tolerance},
                      composyx::parameters::verbose<bool>{verbose});

  const MPH_vect X1 = cg_mph_sparse * B;
  X1.display("X2");

  cg_mph_sparse.display("CG librsb sparse");

#ifdef COMPOSYX_USE_RSB_SPBLAS
  if (rsb_lib_exit(RSB_NULL_EXIT_OPTIONS) != RSB_ERR_NO_ERROR)
    return -1;
#else
#ifdef COMPOSYX_USE_RSB
  if (rsb_lib_exit(RSB_NULL_EXIT_OPTIONS) != RSB_ERR_NO_ERROR)
    return -1;
#endif
#endif
  return 0;
}
