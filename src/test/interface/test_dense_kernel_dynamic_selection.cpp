// [[file:../../../org/composyx/interfaces/implicit_dense_kernels.org::*Tests][Tests:1]]
#include <iostream>
#include <composyx.hpp>
#include <catch2/catch_test_macros.hpp>

// Static: only LAPACKPP
#if defined(COMPOSYX_USE_LAPACKPP) && !defined(COMPOSYX_USE_TLAPACK)
std::string expected_solver(const std::string&) {
  return std::string("LAPACKPP");
}
#endif

// Static: only TLAPACK
#if !defined(COMPOSYX_USE_LAPACKPP) && defined(COMPOSYX_USE_TLAPACK)
std::string expected_solver(const std::string&) {
  return std::string("TLAPACK");
}
#endif

// Static: forced with flag
#if defined(COMPOSYX_USE_LAPACKPP) && defined(COMPOSYX_USE_TLAPACK) &&         \
    defined(COMPOSYX_STATIC_DENSE_KERNEL_TYPE)
std::string expected_solver(const std::string&) {
  return std::string(COMPOSYX_STATIC_DENSE_KERNEL_TYPE);
}
#endif

// Dynamic
#if defined(COMPOSYX_USE_LAPACKPP) && defined(COMPOSYX_USE_TLAPACK) &&         \
    !defined(COMPOSYX_STATIC_DENSE_KERNEL_TYPE)
std::string expected_solver(const std::string& exp) { return exp; }
#endif

TEST_CASE("implicit_dense_kernels_dynamic_selection", "[dense][interface]") {

  using namespace composyx;

  info_implicit_dense_kernels(std::cout);

  std::cout << "Expected: " << expected_solver("LAPACKPP")
            << " - FOUND: " << _value_current_implicit_dense_kernels() << '\n';
  REQUIRE(_value_current_implicit_dense_kernels() ==
          expected_solver("LAPACKPP"));
  select_dense_kernel::package = dense_kernel_type::TLAPACK;
  std::cout << "Expected: " << expected_solver("TLAPACK")
            << " - FOUND: " << _value_current_implicit_dense_kernels() << '\n';
  REQUIRE(_value_current_implicit_dense_kernels() ==
          expected_solver("TLAPACK"));
  select_dense_kernel::package = dense_kernel_type::LAPACKPP;
  std::cout << "Expected: " << expected_solver("LAPACKPP")
            << " - FOUND: " << _value_current_implicit_dense_kernels() << '\n';
  REQUIRE(_value_current_implicit_dense_kernels() ==
          expected_solver("LAPACKPP"));
}
// Tests:1 ends here
