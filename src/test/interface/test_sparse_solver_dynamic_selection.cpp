// [[file:../../../org/composyx/interfaces/implicit_sparse_solver.org::*Tests][Tests:1]]
#include <iostream>
#include <composyx.hpp>
#include <catch2/catch_test_macros.hpp>

// Static: no solver available
#if !defined(COMPOSYX_USE_MUMPS) && !defined(COMPOSYX_USE_PASTIX) &&           \
    !defined(COMPOSYX_USE_QRMUMPS)
std::string expected_solver(const std::string&) { return std::string("NONE"); }
#endif

// Static: only MUMPS
#if defined(COMPOSYX_USE_MUMPS) && !defined(COMPOSYX_USE_PASTIX)
std::string expected_solver(const std::string&) { return std::string("MUMPS"); }
#endif

// Static: only PASTIX
#if !defined(COMPOSYX_USE_MUMPS) && defined(COMPOSYX_USE_PASTIX)
std::string expected_solver(const std::string&) {
  return std::string("PASTIX");
}
#endif

// Static: forced with flag
#if defined(COMPOSYX_USE_MUMPS) && defined(COMPOSYX_USE_PASTIX) &&             \
    defined(COMPOSYX_STATIC_SPARSE_SOLVER_TYPE)
std::string expected_solver(const std::string&) {
  return std::string(COMPOSYX_STATIC_SPARSE_SOLVER_TYPE);
}
#endif

// Dynamic
#if defined(COMPOSYX_USE_MUMPS) && defined(COMPOSYX_USE_PASTIX) &&             \
    !defined(COMPOSYX_STATIC_SPARSE_SOLVER_TYPE)
std::string expected_solver(const std::string& exp) { return exp; }
#endif

TEST_CASE("sparse_solvers_dynamic_selection", "[sparse][interface]") {

  using namespace composyx;

  info_implicit_sparse_solver(std::cout);

  std::cout << "Expected: " << expected_solver("MUMPS")
            << " - FOUND: " << _value_current_implicit_sparse_solver() << '\n';
  REQUIRE(_value_current_implicit_sparse_solver() == expected_solver("MUMPS"));
  select_sparse_solver::package = sparse_solver_type::PASTIX;
  std::cout << "Expected: " << expected_solver("PASTIX")
            << " - FOUND: " << _value_current_implicit_sparse_solver() << '\n';
  REQUIRE(_value_current_implicit_sparse_solver() == expected_solver("PASTIX"));
  select_sparse_solver::package = sparse_solver_type::MUMPS;
  std::cout << "Expected: " << expected_solver("MUMPS")
            << " - FOUND: " << _value_current_implicit_sparse_solver() << '\n';
  REQUIRE(_value_current_implicit_sparse_solver() == expected_solver("MUMPS"));
}
// Tests:1 ends here
