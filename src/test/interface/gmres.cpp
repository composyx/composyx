// [[file:../../../org/composyx/solver/GMRES.org::*Interface][Interface:1]]
#include <cstddef>

template <class Scalar> struct FakeVector;
namespace composyx {
template <class Scalar> std::size_t size(const FakeVector<Scalar>&);
template <class Scalar> Scalar* get_ptr(FakeVector<Scalar>&);
} // namespace composyx

#include <composyx.hpp>
#include <composyx/solver/GMRES.hpp>

// Vector type
template <class Scalar> struct FakeVector {
  Scalar dummy;
  FakeVector operator+=(const FakeVector&) { return *this; }
  FakeVector operator-=(const FakeVector&) { return *this; }
  FakeVector operator*=(Scalar) { return *this; }
  FakeVector() {}
  FakeVector(int) {}
  Scalar& operator[](int) { return dummy; }
  Scalar& operator()(int) { return dummy; }
};

template <typename T> struct composyx::scalar_type<FakeVector<T>> {
  using type = T;
};

template <class Scalar>
Scalar dot(const FakeVector<Scalar>&, const FakeVector<Scalar>&) {
  return Scalar{1};
}
template <class Scalar> size_t composyx::size(const FakeVector<Scalar>&) {
  return 1;
}
template <class Scalar> size_t n_rows(const FakeVector<Scalar>&) { return 1; }
template <class Scalar> size_t n_cols(const FakeVector<Scalar>&) { return 1; }
template <class Scalar> Scalar* composyx::get_ptr(FakeVector<Scalar>& v) {
  return &(v.dummy);
}

template <class Scalar>
FakeVector<Scalar> operator*(Scalar, FakeVector<Scalar> v) {
  return v;
}
template <class Scalar>
FakeVector<Scalar> operator*(FakeVector<Scalar> v, Scalar) {
  return v;
}
template <class Scalar>
FakeVector<Scalar> operator/(FakeVector<Scalar> v, Scalar) {
  return v;
}
template <class Scalar>
FakeVector<Scalar> operator-(const FakeVector<Scalar>&, FakeVector<Scalar> v) {
  return v;
}
template <class Scalar>
FakeVector<Scalar> operator+(const FakeVector<Scalar>&, FakeVector<Scalar> v) {
  return v;
}

// Matrix type
template <class Scalar> struct FakeMatrix {};
template <typename Scalar>
FakeVector<Scalar> operator*(const FakeMatrix<Scalar>&, FakeVector<Scalar> v) {
  return v;
}
template <typename T> struct composyx::scalar_type<FakeMatrix<T>> {
  using type = T;
};
template <typename T> struct composyx::vector_type<FakeMatrix<T>> {
  using type = FakeVector<T>;
};

int main() {

  FakeMatrix<double> A;
  FakeVector<double> b;

  composyx::GMRES<FakeMatrix<double>, FakeVector<double>> solver;
  solver.setup(composyx::parameters::A{A}, composyx::parameters::verbose{true},
               composyx::parameters::max_iter{5},
               composyx::parameters::tolerance{1e-8});

  [[maybe_unused]] FakeVector<double> x = solver * b;

  return 0;
}
// Interface:1 ends here
